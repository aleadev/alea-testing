from setuptools import setup, find_packages

setup(
    name='alea',
    version='0.1',
    description='Spectral Methods for Uncertainty Quantification',
    author='aleadev',
    author_email='e.zander@tu-bs.de',
    url='http://bitbucket.org/aleadev/alea',
    packages=find_packages(),
    long_description="""\
      alea is a library for performing uncertainty quantification using
      spectral methods in python
      """,
    classifiers=[
        "License :: OSI Approved :: GNU General Public License (GPL)",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering",
        "Topic :: Scientific/Engineering :: Mathematics",
        "Topic :: Software Development",
        "Topic :: Software Development :: Libraries",
        ],
    keywords='uncertainty quantification spectral method stochastic finite elements',
    license='GPL',
    install_requires=[
        'setuptools',
        'scipy',
        'numpy',
        'matplotlib',
        'joblib',
        'dolfin',
        'tt'
        ],
    )
