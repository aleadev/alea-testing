import numpy as np
from dolfin import grad, div, TestFunction, CellSize, assemble, FunctionSpace, cells, dx, dS, avg, project, jump, inner

def evaluate_residual_estimator(V, u_h, kappa, f, assemble_quad_degree=-1):
    # setup indicator
    mesh = V.mesh()
    h = CellSize(mesh)
    DG0 = FunctionSpace(mesh, 'DG', 0)
    DG0_dofs = dict([(c.index(),DG0.dofmap().cell_dofs(c.index())[0]) for c in cells(mesh)])
    dg0 = TestFunction(DG0)

    # define forms and assemble
    V2 = FunctionSpace(V.mesh(), V.ufl_element().family(), V.ufl_element().degree())
    kappa_h = project(kappa, V2)
    R_T = -(f + div(kappa_h * grad(u_h)))
    R_dT = kappa * grad(u_h)
    J = jump(R_dT)
    indicator = h ** 2 * (1 / kappa) * R_T ** 2 * dg0 * dx + avg(h) * avg(1 / kappa) * J **2 * 2 * avg(dg0) * dS

    # prepare indicators
    eta_res_local = assemble(indicator, form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
    eta_res_local = eta_res_local.array()[DG0_dofs.values()]
    eta_res_global = np.sum(eta_res_local)
    eta_res_local = np.sqrt(eta_res_local)

    return eta_res_local, eta_res_global
