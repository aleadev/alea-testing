"""
Demo for adaptive Poisson using a residual-based energy-norm error
estimator

  eta_h**2 = sum_T eta_T**2

with

  eta_T**2 = h_T**2 ||R_T||_T**2 + h_T ||R_dT||_dT**2

where

  R_T = - (f + div grad u_h)
  R_dT = jump(grad u_h * n)

and a 'maximal' marking strategy (refining those cells for
which the error indicator is greater than a certain fraction of the
largest error indicator)
"""

from dolfin import *
from numpy import array, sqrt
from residual_estimator import evaluate_residual_estimator

# Adaptive iterations
max_iterations = 10

# Refinement fraction
theta = 0.5
max_cells = 1e4

# Create initial mesh
square_domain = not True
mesh = UnitSquareMesh(4, 4) if square_domain else Mesh("lshape.xml")

for i in range(max_iterations):
    # Define variational problem
    V = FunctionSpace(mesh, "CG", 1)
    v = TestFunction(V)
    u = TrialFunction(V)
#    f = Expression("10*exp(-(pow(x[0] - 0.6, 2) + pow(x[1] - 0.4, 2)) / 0.02)",
#                   degree=3)
    f = Constant("1.0")
    a = inner(grad(v), grad(u)) * dx
    L = v * f * dx

    # Define boundary condition
    u0 = Constant(0.0)
    if square_domain:
        bc = DirichletBC(V, 0.0, "near(x[0], 0.0) || near(x[0], 1.0) || near(x[1], 0.0) || near(x[1], 1.0)")
    else:
        def u0_boundary(x, on_boundary):
            return on_boundary
#            return (x[0] <= DOLFIN_EPS - 1.0 or (x[0] >= 0.0 - DOLFIN_EPS and x[1] < 1.0 - DOLFIN_EPS)) and on_boundary
    #    return x[0] < DOLFIN_EPS or x[0] > 1.0 - DOLFIN_EPS
        bc = DirichletBC(V, u0, u0_boundary)

    # Compute solution
    u_h = Function(V)
    solve(a == L, u_h, bc)

    # Define cell and facet residuals
    R_T = -(f + div(grad(u_h)))
    nu = FacetNormal(mesh)
    R_dT = grad(u_h)

    # Will use space of constants to localize indicator form
    DG0 = FunctionSpace(mesh, 'DG', 0)
    DG0_dofs = dict([(c.index(),DG0.dofmap().cell_dofs(c.index())[0]) for c in cells(mesh)])
    w = TestFunction(DG0)
    h = CellSize(mesh)

    if True:
        indicators = evaluate_residual_estimator(V, u_h, Constant(1), f)[0]
    else:
        # Define form for assembling error indicators
        form = (h ** 2 * R_T ** 2 * w * dx + avg(h) * jump(R_dT,nu) ** 2 * 2 * avg(w) * dS)
#        form = (h ** 2 * R_T ** 2 * w * dx + avg(h) * jump(R_dT) ** 2 * 2 * avg(w) * dS)
#        form = avg(h) * jump(R_dT) ** 2 * 2 * avg(w) * dS

        # Assemble error indicators
        indicators = assemble(form).array()#[DG0_dofs.values()]

    # Calculate error
    error_estimate = sqrt(sum(indicators))
    print "error_estimate = ", error_estimate

    # Take sqrt of indicators
    print "----------", indicators
    indicators = sqrt(indicators)

    if mesh.num_cells() >= max_cells:
        break

    from operator import itemgetter
    sorted_indicators = sorted([(i,v) for i,v in enumerate(indicators)], key=itemgetter(1), reverse=True)
    cell_markers = MeshFunction("bool", mesh, mesh.topology().dim())
    cell_markers.set_all(False)
    marked_ind = 0
    while marked_ind < theta * error_estimate**2 and len(sorted_indicators) > 0:
        eind = sorted_indicators.pop(0)
        cell_markers[eind[0]] = True
        marked_ind += eind[1]**2

    # Refine mesh
    mesh = refine(mesh, cell_markers)

# Plot final solution
plot(u_h)
plot(mesh)

# Hold plots
interactive()
