from __future__ import division
from dolfin import *
import scipy.sparse as sp
import scipy.sparse.linalg as spla
from scipy.special import kv, gamma
import numpy as np
from functools import partial
from tictoc import TicToc
import argparse
import os
import cPickle as pickle

# use uBLAS backend for conversion to scipy sparse matrices
parameters.linear_algebra_backend = "uBLAS"

old_dolfin = False
try:
    old_dolfin = float(dolfin_version()[:3]) < 1.4
except:
    old_dolfin = True
if old_dolfin:
    def dof_to_vertex_map(V):
        return V.dofmap().vertex_to_dof_map(V.mesh())


parser = argparse.ArgumentParser()
parser.add_argument("-s", "--EV-solver", type=int, choices=[0, 1, 2], default=0,
                    help="EV solver type (0: symmetric (default), 1: unsymmetric, 2: Cholesky)")
parser.add_argument("-cov", "--covariance", type=int, choices=[0, 1, 2], default=2,
                    help="covariance type (0: Gaussian, 1: exponential, 2: Matern (default))")
parser.add_argument("-cov-sigma2", type=float, default=1,
                    help="covariance sigma^2 (default 1)")
parser.add_argument("-cov-a", type=float, default=0.1,
                    help="covariance scaling (default 0.1)")
parser.add_argument("-cov-nu", type=float, default=2.5,
                    help="Matern regularity parameter (0.5=rough, 2.5=smooth (default))")
parser.add_argument("-KL", "--KL-file", type=str, required=False, default="",
                    help="load KL solution file")
parser.add_argument("-m", "--mesh-type", type=int, choices=[0, 1], default=0,
                    help="mesh type (0: square, 1: lshape)")
parser.add_argument("-mN", "--mesh-N", type=int, default=50,
                    help="mesh elements")
parser.add_argument("-p", "--fem-degree", type=int, default=1,
                    help="FEM degree (default 1)")
parser.add_argument("-EV", "--nr-EV", type=int, default=50,
                    help="number eigenvalues")
parser.add_argument("-sp", "--nr-samples", type=int, default=0,
                    help="plot number samples (realisations)")
parser.add_argument("-ex", "--export-KL", action='store_true', default=False,
                    help="export KL modes")
parser.add_argument("--no-plot", action='store_true', default=False,
                    help="don't plot EVs")
parser.add_argument("-logN", action='store_true', default=False,
                    help="log normal")
args = parser.parse_args()
print "========== PARAMETERS:", vars(args)


# define std covariances
def cov_exp(r, a, sigma2):
    return sigma2 * np.exp(-1/a * np.abs(np.linalg.norm(r, axis=1)))

def cov_gauss(r, a, sigma2):
    return sigma2 * np.exp(-1/a**2 * np.linalg.norm(r, axis=1)**2)

def cov_matern(r, a, sigma2, nu):
    assert nu >= 0.5
    sq2nu = np.sqrt(2) * nu
    C = sigma2 * np.prod([np.power(sq2nu*np.abs(di)/a, nu) * kv(nu, sq2nu*np.abs(di)/a) / (gamma(nu) * np.power(2, nu-1)) for di in r.T], axis=0)
    C[np.isnan(C)] = 0
    return C

COVS = [partial(cov_gauss, a=args.cov_a, sigma2=args.cov_sigma2), partial(cov_exp, a=args.cov_a, sigma2=args.cov_sigma2),
        partial(cov_matern, a=args.cov_a, sigma2=args.cov_sigma2, nu=args.cov_nu)]


# FEM based KL evaluation
# see Keese Dissertation p44
EVPtypes = ['symmetric', 'unsymmetric', 'cholesky']
def evaluate_KL(cov, N, k, degree=1, EVPtype='symmetric'):
    def setup_FEM(N):
        mesh = UnitSquareMesh(N,N)
        V = FunctionSpace(mesh, 'CG', degree)
        dof2vert = dof_to_vertex_map(V)
        u = TrialFunction(V)
        v = TestFunction(V)
        return mesh, V, u, v
    # construct FEM space
    mesh, V, u, v = setup_FEM(N)
    dof2vert = dof_to_vertex_map(V)
    coords = mesh.coordinates()
    # assemble mass matrix and convert to scipy
    M = assemble(u*v*dx)
    rows, cols, values = M.data()
    spM = sp.csr_matrix((values, cols, rows)).tocoo()
    M = spM.todense()
    # evaluate covariance matrix
    L = coords.shape[0]
    with TicToc(key="construct C", do_print=True, accumulate=False):
        if True:    # vectorised
            c0 = np.repeat(coords, L, axis=0)
            c1 = np.tile(coords, [L,1])
            # r = np.linalg.norm(c0-c1, axis=1)
            # C = cov(r)
            C = cov(c0-c1)
            C.shape = [L,L]
        else:       # slow validation
            C = np.zeros([L,L])
            for i in range(L):
                for j in range(L):
                    if j <= i:
                        v = cov(np.linalg.norm(coords[i]-coords[j]))
                        C[i,j] = v
                        C[j,i] = v
    # solve eigenvalue problem
    if EVPtype == 'symmetric':
        with TicToc(key="symmetric EVP construct", do_print=True, accumulate=False):
            A = np.dot(M, np.dot(C, M))
        with TicToc(key="symmetric EVP solve", do_print=True, accumulate=False):
            w, v = spla.eigsh(A, k, M)
    elif EVPtype == 'unsymmetric':
        with TicToc(key="unsymmetric EVP construct", do_print=True, accumulate=False):
            A = np.dot(C, M)
        with TicToc(key="unsymmetric EVP solve", do_print=True, accumulate=False):
            w, v = spla.eigs(A, k)
    elif EVPtype == 'cholesky':
        with TicToc(key="cholesky EVP construct", do_print=True, accumulate=False):
            B = np.linalg.cholesky(M)
            A = np.dot(B.T, np.dot(C, B))
        with TicToc(key="cholesky EVP solve", do_print=True, accumulate=False):
            w, y = spla.eigsh(A, k)
        # evaluate B.T kappa = w to obtain correct eigenvectors
        with TicToc(key="cholesky EVP transform", do_print=True, accumulate=False):
            Y = np.array(y)
            v = np.linalg.solve(B.T, Y)
    else:
        raise TypeError('unknown EVP type %s' % EVPtype)
    # return eigenpairs
    print "===== EVP size =====", A.shape, w.shape, v.shape
    v = np.array([z[dof2vert] for z in v.T])
    return w, v.T, V


def export_KL(lambdas, kappas, V):
    export_file = "KL-cov%i-cova%f-covsig%f-covnu%f-mN%i-mT%i-nEV%i-p%i" % (args.covariance, args.cov_a, args.cov_sigma2, args.cov_nu,
                                                                    args.mesh_N, args.mesh_type, args.nr_EV, args.fem_degree)
    export_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), "KL", export_file)
    print "==> exporting data to", export_file

    # export eigenpairs
    DATA = {"lambdas":lambdas, "kappas":kappas, "V":(V.ufl_element().family(), V.ufl_element().degree())}
    with open(export_file + '.dat', 'wb') as outfile:
        pickle.dump(DATA, outfile, pickle.HIGHEST_PROTOCOL)

    # export mesh of FunctionSpace
    mesh = V.mesh()
    File(export_file + "-mesh.xml") << mesh


# setup and solve EV problem
lambdas, kappas, V = evaluate_KL(COVS[args.covariance], args.mesh_N, args.nr_EV, degree=args.fem_degree, EVPtype=EVPtypes[args.EV_solver])
print "dimension of V:", V.dim()
print "eigenvalues:", sorted(lambdas, reverse=True)

if args.export_KL:
    export_KL(lambdas, kappas, V)

if not args.no_plot:
    from matplotlib.pyplot import figure, show, legend
    fig1 = figure()
    fig1.suptitle("eigenvalues")
    ax = fig1.add_subplot(111)
    ax.loglog(range(len(lambdas)), sorted(lambdas, reverse=True), '-ro', label='$\lambda$')
    # ax.loglog(plotdata["DOFS"], plotdata["maxrank"], '-g<', label='max rank')
    # ax.loglog(plotdata["DOFS"], plotdata["xyz"], '-r*', label='xyz')
    legend(loc='upper right')
    show()

if args.nr_samples > 0:
    from plothelper import PlotHelper
#    from alea.stochastics.random_variable import UniformRV, NormalRV
    evs = []
    for x in kappas.T:
        # print "AAA", type(x), x
        # print "BBB", len(x), x.astype(x.dtype, order='C')
        f = Function(V)
        f.vector()[:] = x.astype(x.dtype, order='C')
        evs.append(f)

    ph = PlotHelper()
    lognormal = True
#    rv = NormalRV() if lognormal else UniformRV()
    for r in range(args.nr_samples):
        af = Function(V)
#        v = sum([c * f.vector().array() for c, f in zip(rv.sample(args.nr_EV), evs)])
		v = sum([np.sqrt(l) * c * f.vector().array() for l, c, f in zip(lambdas, np.random.rand(args.nr_EV), evs)])
        # if lognormal:
        #     v = np.exp(v)
        af.vector()[:] = v
        ph["af"].plot(af, interactive=True)
    if not True:
        for i, ev in enumerate(evs):
            ph['EV'].plot(ev, interactive=True)
            if i > 10:
                break
