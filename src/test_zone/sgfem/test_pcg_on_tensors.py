import logging

import numpy as np
import scipy.sparse as sps

from alea.linalg.cptensor import CPTensor
from alea.linalg.operator import MultiplicationOperator
from alea.linalg.tensor_basis import TensorBasis
from alea.linalg.tensor_operator import TensorOperator
from alea.linalg.scipy_operator import ScipyOperator, ScipySolveOperator
from alea.linalg.basis import CanonicalBasis
from test_zone.sgfem import pcg

logger = logging.getLogger(__name__)

with_preconditioner = True

def generate_matrix(n):
    A = np.random.rand(n, n) - 0.5
    X = np.dot(A, A.T) + 0.3 * np.eye(n)
    X = sps.csr_matrix(X)
    #w,v = sps.linalg.eigs(X, 10, which='SR')
    #print "eigs=", w
    return X


def construct_operator(n1, n2, R):
    # prepare "deterministic" matrices
    basis1 = CanonicalBasis(n1)
    basis2 = CanonicalBasis(n2)

    alpha = lambda k: np.exp(-3 *k)
    #alpha = lambda k: 1 if k==0 else 0 
    #alpha = lambda k: 1
    A1 = [ScipyOperator(alpha(k) * generate_matrix(n1),
                        domain=basis1, codomain=basis1)
          for k in range(R)]
    A2 = [ScipyOperator(generate_matrix(n2),
                        domain=basis2, codomain=basis2)
          for k in range(R)]

    basis = TensorBasis([basis1, basis2])
    return TensorOperator(A1, A2, domain=basis, codomain=basis)


def construct_cptensor(n1, n2, R):
    # prepare vector
    basis1 = CanonicalBasis(n1)
    basis2 = CanonicalBasis(n2)
    basis = TensorBasis([basis1, basis2])
    X1 = np.random.rand(n1, R)
    X2 = np.random.rand(n2, R)
    return CPTensor([X1, X2], basis)


def construct_preconditioner(A, identity=False):
    basis = A.domain
    basis1, basis2 = basis[0], basis[1]

    if not identity:
        return TensorOperator(
            [ScipySolveOperator(A.A[0].matrix, domain=basis1, codomain=basis1)],
            [ScipySolveOperator(A.B[0].matrix, domain=basis2, codomain=basis2)],
           # [MultiplicationOperator(1, domain=basis2)],
            basis)
    else:
        return TensorOperator(
            [MultiplicationOperator(1, domain=basis1)],
            [MultiplicationOperator(1, domain=basis2)],
            basis)


def rank_truncate(R_max):
    """Create a truncation function that truncates by rank"""
    def do_truncate(X):
        return X.truncate(R_max)
    return do_truncate


def test_solve_pcg(A, P, u, f, **kwargs):
    """Solve the linear problem with given solution and show solve statistics"""
    [u2, _, numiter] = pcg.pcg(A, f, P, 0 * u, **kwargs)

    logger.info("error:  %s", np.linalg.norm(u.flatten().as_array() - u2.flatten().as_array()))
    logger.info("residual: %s", np.linalg.norm((f - A * u2).flatten().as_array()))
    logger.info("iter:   %s", numiter)
    logger.info("norm_u: %s", np.linalg.norm(u.flatten().as_array()))
    logger.info("norm_f: %s", np.linalg.norm((A * u).flatten().as_array()))

# test tensor operator application
# ================================
np.random.seed(10)

n1 = 100
n2 = 15
R_A = 5
R_u = 4
# n1 = 10
# n2 = 5
# R_A = 1
# R_u = 1

A = construct_operator(n1, n2, R_A)
P = construct_preconditioner(A, identity=not with_preconditioner)

u = construct_cptensor(n1, n2, R_u)
u_flat = u.flatten()

pcg.logger.setLevel(logging.DEBUG)
logger.setLevel(logging.INFO)

eps = 0.3

print "\n================ TEST A =================="
test_solve_pcg(A, P, u_flat, A * u_flat, eps=eps)

print "\n================ TEST B =================="
test_solve_pcg(A, P, u, A * u, truncate_func=rank_truncate(10), eps=eps)
