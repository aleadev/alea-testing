from __future__ import division
from dolfin import *
import numpy as np
from matplotlib.path import Path

# parameters.refinement_algorithm = 'regular_cut'

class SimplePolySubDomain(SubDomain):
    def __init__(self, poly):
        SubDomain.__init__(self)
        self.poly = poly
        X = [x[0] for x in poly]
        Y = [x[1] for x in poly]
        self.x0 = (min(X),max(X))
        self.x1 = (min(Y),max(Y))
    def inside(self, x, on_boundary):
        return between(x[0], (self.x0[0],self.x0[1])) and between(x[1], (self.x1[0],self.x1[1]))

# this doesn't work properly...
class PolySubDomain(SubDomain):
    def __init__(self, poly):
        SubDomain.__init__(self)
        self.poly = poly
        p = list(poly)
        p.append(poly[0])
        nverts = len(p)
        codes = np.ones(nverts, int) * Path.LINETO
        codes[0] = Path.MOVETO
        codes[-1] = Path.CLOSEPOLY
        self.path = Path(p, codes)
    def inside(self, x, on_boundary):
        return self.path.contains_point(x)

refine_subdom = [1,3,13]
m,n = 5,4
dx,dy = 1/m,1/n
x0, x1 = [0,0], [0,0]
dom0 = Rectangle(0, 0, 1, 1)
subdoms, points = [], []
d = 1
for i in range(m):
    for j in range(n):
        # set corner coordinates
        x0[0] = i*dx if i > 0 else 0
        x1[0] = (i+1)*dx if i < m else 1
        x0[1] = j*dy if j > 0 else 0
        x1[1] = (j+1)*dy if j < n else 1
        # construct subdomain
        P1 = ((x0[0],x0[1]),(x1[0],x0[1]),(x1[0],x1[1]),(x0[0],x1[1]))
        dom1 = Polygon([Point(*p) for p in P1])
        subdoms.append(dom1)
        points.append(P1)
        dom0.set_subdomain(d, dom1)
        d += 1
# generate mesh
mesh = Mesh(dom0, 10)
# define domain cell function
domain_cf = MeshFunction("size_t", mesh, 2, mesh.domains())
# refine subdomains
markers = CellFunction('bool', mesh)
markers.set_all(False)
if refine_subdom is not None and len(refine_subdom) > 0:
    for c in cells(mesh):
        cid = int(c.index())
        if domain_cf[cid] in refine_subdom:
            markers[cid] = True
    mesh = adapt(mesh, markers)
domain_cf2 = CellFunction("size_t", mesh)
domain_cf2.set_all(0)
for d, poly in enumerate(points):
    dom1 = SimplePolySubDomain(poly)
    dom1.mark(domain_cf2, d)
plot(domain_cf, title='domain_cf')
plot(domain_cf2, title='domain_cf2')
plot(mesh, title='mesh')
interactive()
