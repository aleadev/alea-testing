from __future__ import division
import numpy as np
import scipy.sparse as sps
import scipy.linalg as la

from alea.linalg.tensor_operator import TensorOperator
from alea.linalg.tensor_vector import FullTensor
from alea.linalg.operator import MultiplicationOperator
from alea.polyquad.structure_coefficients import evaluate_triples
from alea.math_utils.multiindex_set import MultiindexSet
from alea.application.egsz.sample_problems2 import SampleProblem
from alea.application.egsz.sample_domains import SampleDomain
from alea.fem.fenics.fenics_basis import FEniCSBasis
from alea.polyquad.polynomials import LegendrePolynomials
from alea.linalg.tensor_basis import TensorBasis
from alea.linalg.scipy_operator import ScipyOperator, ScipySolveOperator
from alea.linalg.basis import CanonicalBasis
from test_zone.sgfem.pcg import pcg

from dolfin import set_log_level, Function, plot
import logging

# use uBLAS backend for conversion to scipy sparse matrices
from dolfin import parameters
try:
    parameters.linear_algebra_backend = "Eigen"
except:
    parameters.linear_algebra_backend = "uBLAS"

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
set_log_level(logging.INFO)
fenics_logger = logging.getLogger("FFC")
fenics_logger.setLevel(logging.WARNING)
fenics_logger = logging.getLogger("UFL")
fenics_logger.setLevel(logging.WARNING)


def prepare_deterministic_operators(pde, coeff, M, mesh, degree):
    fs = pde.function_space(mesh, degree=degree)
    basis = FEniCSBasis(fs)
    am_f = [coeff.mean_func]
    am_f.extend([coeff_field[m][0] for m in range(M)])
    K = [pde.assemble_operator(basis=basis, coeff=f).as_scipy_operator() for f in am_f]
    return K, basis


def prepare_stochastic_operators(coeff_p, solution_p, M, rv_type='H', mi_type=['complete', 'full'][0]):
    """
    :param coeff_p: gpc degree of coefficient
    :param solution_p: gpc degree of solution
    :param M: number of stochastic (coefficient) dimensions
    :param rv_type: Hermite (H) or Legendre (L) polynomials
    :param mi_type: complete or full multiindex set for trial and test space
    :return:
    """
    # Legendre triple product
    # polysys = LegendrePolynomials()
    # L = evaluate_triples(polysys, I, I)
    fac = np.math.factorial
    def legendre_triprod(i, j, k):
        if bool((i+j+k) % 2) or k < abs(i-j) or k > abs(i+j):
            return 0
        s = int((i+j+k)/2)
        def A(n):
            if n < 0:
                return 0
            elif n == 0:
                return 1
            return np.prod([i for i in range(1, 2*n, 2)])/fac(n)
        return np.sqrt((2*i+1)*(2*j+1)*(2*k+1))/(i+j+k+1) * A(s-i)*A(s-j)*A(s-k)/A(s)

    # Hermite triple product (see e.g. Ullmann dissertation appendix)
    def hermite_triprod(i, j, k):
        if bool((i+j-k) % 2) or k < abs(i-j) or k > abs(i+j):
            return 0
        s = (i+j-k)/2
        return np.sqrt(fac(i)*fac(j)*fac(k))/(fac(s)*fac(i-s)*fac(j-s))

    I = MultiindexSet.createCompleteOrderSet(M, solution_p).arr if mi_type == 'complete'\
        else MultiindexSet.createFullTensorSet(M, solution_p).arr
    print "multiindex set:\n", I

    rv_triple = {'L': legendre_triprod, 'H': hermite_triprod}[rv_type]
    N = I.shape[0]
    L = []
    for k in range(M+1):                        # iterate over coefficient dimensions
        Lk = sps.dok_matrix((N, N))
        for ind_i, nu_i in enumerate(I):        # iterate multiindices nu_i of trial function u
            for ind_j, nu_j in enumerate(I):    # iterate multiindices nu_j of test function v
                v_ijk = []
                for l, ij in enumerate(zip(nu_i, nu_j)):    # iterate over components of multiindices
                    i, j = ij
                    if l == k:  # same component as coefficient k -> triple product
                        v = np.sum([rv_triple(i, j, p) for p in range(coeff_p+1)])
                    else:       # double product since coefficient degree is 0
                        v = rv_triple(i, j, 0)
                    v_ijk += [v]
                # multiply coefficient dimensions
                Lk[ind_i, ind_j] = np.prod(v_ijk)
                assert not np.isnan(Lk[ind_i, ind_j])
        L += [Lk]
    L = [ScipyOperator(Lk.asformat('csr'), domain=CanonicalBasis(N), codomain=CanonicalBasis(N)) for Lk in L]
    return L


# A setup problem
# ===============

# setup domain and meshes
mesh_N = 2
mesh, boundaries, dim = SampleDomain.setupDomain("square", initial_mesh_N=mesh_N)
mesh = SampleProblem.setupMesh(mesh, num_refine=0)

# define coefficient field
coeff_field = SampleProblem.setupCF("EF-square-cos", decayexp=2, gamma=0.9, freqscale=1, freqskip=0, rvtype="normal", scale=1)

# setup boundary conditions and pde
pde, Dirichlet_boundary, uD, Neumann_boundary, g, f = SampleProblem.setupPDE(2, "square", 0, boundaries, coeff_field)


# B setup tensor operator
# =======================

# setup deterministic operators
fem_degree, coeff_p, solution_p, M = 1, 1, 5, 20
K, FS = prepare_deterministic_operators(pde, coeff_field, M, mesh, fem_degree)
print "K", len(K)

# setup stochastic operators
D = prepare_stochastic_operators(coeff_p, solution_p, M, 'L', mi_type='complete')
print "D", len(D), D[0].domain.dim
print D[0].as_matrix()

import matplotlib.pylab as pl
pl.spy(np.sum([Di.as_matrix() for Di in D]), precision=0.01, markersize=1)
pl.show()

# construct combined tensor operator
basis1, basis2 = K[0].domain, D[0].domain
basis = TensorBasis([basis1, basis2])
A = TensorOperator(K, D, domain=basis, codomain=basis)
I, J, M = A.dim
print "TensorOperator A dim", A.dim

# setup tensor vector
u = [FS.new_vector().array for _ in range(J)]
u = FullTensor.from_list(u)
print "FullTensor u", u.dim()
print "u as matrix shape", u.as_matrix().shape, u.flatten().as_array().shape


# test tensor operator
# ====================

# test application of operator
w = A * u

# print matricisation of tensor operator
M = A.as_matrix()
print "======= operator matrix M:", M.shape, la.norm(M)

if not False:
    import matplotlib.pyplot as plt
    # plot sparsity pattern
    fig = plt.figure()
    # plt.spy(M)
    plt.spy(sum([d.as_matrix() for d in D]))
    #spy(K[0].as_matrix())
    fig = plt.figure()
    plt.spy(M)
    print M
    plt.show()


def rank_truncate(R_max):
    """Create a truncation function that truncates by rank"""
    def do_truncate(X):
        return X.truncate(R_max)
    return do_truncate


def construct_preconditioner(A, identity=False):
    basis = A.domain
    basis1, basis2 = basis[0], basis[1]

    if not identity:
        K0inv = pde.assemble_solve_operator(basis=FS, coeff=f).as_scipy_operator()
        return TensorOperator(
            [K0inv],
            # [ScipySolveOperator(A.B[0].matrix, domain=basis2, codomain=basis2)],
            [MultiplicationOperator(1, domain=basis2)],
            basis)
    else:
        return TensorOperator(
            [MultiplicationOperator(1, domain=basis1)],
            [MultiplicationOperator(1, domain=basis2)],
            basis)

print "0 * w", type(w), 0 * w
b = 0 * w
b._X[0,:] = 1
#b._X[:,:] = 1
print "b", b.as_array()
P = construct_preconditioner(A, identity=True)
eps = 1e-6

# Aev= np.linalg.eigvals(A.as_matrix())
# print "eigvals(A)", np.real(Aev)

[u, _, numiter] = pcg(A, b, P, 0 * w, eps=eps)
logger.info("residual: %s", np.linalg.norm((b - A * u).flatten().as_array()))
logger.info("iter:   %s", numiter)
logger.info("norm_u: %s", np.linalg.norm(u.flatten().as_array()))
logger.info("norm_f: %s", np.linalg.norm((A * u).flatten().as_array()))

print "u", numiter, u.as_array()


if False:
    A = sps.csr_matrix(np.random.rand(7,3))
    B = np.random.rand(3,5)
    print A
    print B
    print A*B
    print A*B - (B.T * A.T).T
    #print np.array(sp.dot(A,B))
    print sp.dot(A,spa.csr_matrix(B)).toarray()
