from dolfin import plot, interactive, UnitSquareMesh, Expression, FunctionSpace, interpolate

class PlotProxy(object):
    def __init__(self, name):
        self.name = name
        self.handle = None
    def plot(self, f, *args, **kwargs):
        if self.handle is None:
            if not "title" in kwargs.keys():
                kwargs["title"] = self.name
            self.handle = plot(f, *args, **kwargs)
        else:
            self.handle.plot(f)

class PlotHelper(object):
    def __init__(self):
        self.handles = {}
    def __getitem__(self, name):
        try:
            ph = self.handles[name]
        except:
            ph = PlotProxy(name)
            self.handles[name] = ph
        return ph

def test_plotting():
    mesh = UnitSquareMesh(10,10)
    V = FunctionSpace(mesh, 'CG', 1)
    f1 = Expression("sin(A*(x[0]+x[1]))", A=1)
    f2 = Expression("cos(A*x[0]*x[1])", A=1)
    ph = PlotHelper()
    for A in range(5):
        f1.A = A+1
        f2.A = A+1
        g1 = interpolate(f1, V)
        g2 = interpolate(f2, V)
        ph["f1"].plot(g1)
        ph["f2"].plot(g2, interactive=True)

if __name__ == '__main__': 
    test_plotting()
