from scipy.linalg import norm
import numpy as np
import scipy.sparse as sps

from alea.linalg.tensor_operator import TensorOperator
from alea.linalg.tensor_vector import FullTensor
from alea.linalg.scipy_operator import ScipyOperator
from alea.linalg.basis import CanonicalBasis


def construct_data(k, d, m, stochastic_identity=False):
    # prepare "deterministic" matrices
    K = [sps.csr_matrix(np.random.rand(k,k)) for _ in range(m)]
    K = [ScipyOperator(K_.asformat('csr'), domain=CanonicalBasis(K_.shape[0]), codomain=CanonicalBasis(K_.shape[1])) for K_ in K]

    # prepare "stochastic" matrices
    if stochastic_identity:
        D = [sps.csr_matrix(np.identity(d)) for _ in range(m)]
    else:
        D = [sps.csr_matrix(np.random.rand(d,d)) for _ in range(m)]
    # convert to efficient sparse format
    D = [ScipyOperator(D_.asformat('csr'), domain=CanonicalBasis(D_.shape[0]), codomain=CanonicalBasis(D_.shape[1])) for D_ in D]

    # prepare vector
    u = [np.random.rand(k) for _ in range(d)]
    return K, D, u


# test tensor operator application
# ================================
I, J = 100, 15
for M in [1,2,5]:
    print "========= M =", M
    for stochastic_identity in [True, False]:
        print "\t stochastic identity", stochastic_identity
        # prepare data
        K, D, u = construct_data(I, J, M, stochastic_identity=stochastic_identity)
        A = TensorOperator(K, D)
        u = FullTensor.from_list(u)
        # print matricisation of tensor operator
        Amat = A.as_matrix()
        print Amat.shape

        # compare with numpy kronecker product
        M2 = [sps.kron(K_.matrix, D_.matrix) for K_, D_ in zip(K, D)]
        M2 = np.sum(M2)

        print "error norm: ", norm(Amat-M2.todense()), " == ", norm(Amat-M2)

        # test application of operator
        w = A * u
