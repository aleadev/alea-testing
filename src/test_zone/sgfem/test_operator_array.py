from __future__ import division
import numpy as np
import scipy.sparse as sps
import scipy.linalg as la

from alea.linalg.cptensor import CPTensor
from alea.linalg.tensor_operator import TensorOperator
from alea.linalg.tensor_vector import FullTensor
from alea.linalg.operator import MultiplicationOperator, MatrixOperator
from alea.linalg.vector import FlatVector
from alea.linalg.tensor_basis import TensorBasis
from alea.linalg.scipy_operator import ScipyOperator
from alea.linalg.basis import CanonicalBasis
from test_zone.sgfem.pcg import pcg, simple_cg

# construct operator
#a1 = sps.csr_matrix(np.array([1]))
a1 = sps.csr_matrix(np.array([[4,1],[1,2]]))
N1 = a1.shape[0]
A1 = ScipyOperator(a1, domain=CanonicalBasis(N1), codomain=CanonicalBasis(N1))
a2 = sps.csr_matrix(np.array([[1,0],[0,1]]))
N2 = a2.shape[0]
A2 = ScipyOperator(a2, domain=CanonicalBasis(N2), codomain=CanonicalBasis(N2))
N = N1 * N2

basis1, basis2 = A1.domain, A2.domain
basis = TensorBasis([basis1, basis2])
A = TensorOperator([A1], [A2], domain=basis, codomain=basis)
Am = A.as_matrix()
print Am

# construct vector
f = [[1.] * basis1.dim] * basis2.dim
f = FullTensor.from_list(f)
# X1 = np.ones(basis1.dim)
# X2 = np.ones(basis2.dim)
# f = CPTensor([X1, X2], basis)

fm = f.as_array().flatten()
print fm
w = np.linalg.solve(Am, fm)
print "numpy linear solver solution", w, "A*w =", np.dot(Am, w)

# print "AAA", (A * FullTensor.from_list([[1.,1.]])).as_array()

AO = MatrixOperator(Am)
PO = MatrixOperator(np.eye(N))
[w, _, numiter] = simple_cg(AO, FlatVector(fm), FlatVector(np.zeros(N)), maxiter=10)
print "BBB1 cg with numpy matrix", w.as_array()

[w, _, numiter] = pcg(AO, FlatVector(fm), PO, FlatVector(np.zeros(N)), maxiter=10)
print "BBB2 pcg with numpy matrix", w.as_array()


# construct identity preconditioner
P = TensorOperator(
        [MultiplicationOperator(1, domain=basis1)],
        [MultiplicationOperator(1, domain=basis2)],
        A.domain)

print "CCC (just to verify P is identity) [-1,1] =", (P * FullTensor.from_list([[-1.,1.]])).as_array().flatten()

[w, _, numiter] = pcg(A, f, P, 0 * f, maxiter=10)
print "pcg tensor solution", w.as_array().flatten()
