from __future__ import division
import argparse
import logging
import logging.config
import numpy as np
import scipy as sp

# alea imports
from alea.application.egsz.solver_utils import prepare_tildef
from alea.math_utils.multiindex import Multiindex
from alea.math_utils.multiindex_set import MultiindexSet
from alea.application.egsz.sample_problems2 import SampleProblem
from alea.application.egsz.sample_domains import SampleDomain
from alea.utils.plothelper import PlotHelper
from alea.polyquad.polynomials import StochasticHermitePolynomials

from dolfin import plot, refine, Mesh, FunctionSpace, set_log_level, WARNING, INFO, ERROR, project, Function

# set FEniCS logging
set_log_level(logging.WARNING)
fenics_logger = logging.getLogger("FFC")
fenics_logger.setLevel(logging.WARNING)
fenics_logger = logging.getLogger("UFL")
fenics_logger.setLevel(logging.WARNING)

import sys
sys.setrecursionlimit(10000)

# ###############
# A problem setup
# ###############

parser = argparse.ArgumentParser()
# parser.add_argument("-ct", "--coeff-type", type=str, default="EF-square-cos",
parser.add_argument("-ct", "--coeff-type", type=str, default="cos",
                    help="coefficient type")
parser.add_argument("-cm", "--coeff-mean", type=float, default=0.0,
                    help="coefficient mean")
parser.add_argument("-at", "--amp-type", type=str, default="decay-inf",
                    help="amplification type")
parser.add_argument("-aff", "--affine", action="store_true", default=False,
                    help="affine field instead of exponential")
parser.add_argument("-N", type=int, default=30,
                    help="number of terms for amp-type constant")
parser.add_argument("-M", "--M-terms", type=int, default=10,
                    help="terms in realisation")
parser.add_argument("-xm", "--max-m", type=int, default=-1,
                    help="max m for a_m in expansion")
parser.add_argument("-p", "--pcep", type=int, default=1,
                    help="order of pce")
parser.add_argument("-fL", "--full-set", action="store_true", default=False,
                    help="use full tensor set instead of complete set for Lambda")
parser.add_argument("-de", "--decay-exp", type=float, default=2.0,
                    help="decay exponent")
parser.add_argument("-g", "--gamma", type=float, default=0.9,
                    help="gamma")
parser.add_argument("-fk", "--freq-skip", type=int, default=0,
                    help="frequency skip")
parser.add_argument("-fs", "--freq-scale", type=float, default=1.0,
                    help="frequency scaling")
parser.add_argument("-s", "--scale", type=float, default=1.0,
                    help="scaling")
parser.add_argument("-nr", "--number-realisations", type=int, default=5,
                    help="number realisations")
parser.add_argument("-ref", "--mesh-refinements", type=int, default=2,
                    help="initial mesh refinements")
args = parser.parse_args()
print "========== PARAMETERS:", vars(args)

# setup boundary conditions and pde
mesh, boundaries, dim = SampleDomain.setupDomain("square", initial_mesh_N=10)
mesh = SampleProblem.setupMesh(mesh, num_refine=args.mesh_refinements)

# define coefficient field
coeff_field = SampleProblem.setupCF2(args.coeff_type, amptype=args.amp_type, decayexp=args.decay_exp, gamma=args.gamma, N=args.N,
                                     freqscale=args.freq_scale, freqskip=args.freq_skip, rvtype="normal", scale=args.scale,
                                     coeff_mean=args.coeff_mean)

# setup boundary conditions and pde
pde, Dirichlet_boundary, uD, Neumann_boundary, g, f = SampleProblem.setupPDE(2, "square", 0, boundaries, coeff_field)


# ###################################
# B evaluate coefficient realisations
# ###################################

# prepare expansion terms
CG = FunctionSpace(mesh, 'CG', 1)
F0 = project(coeff_field.mean_func, CG).vector().array()
Fm = [project(coeff_field[m][0], CG).vector().array() for m in range(args.M_terms)]

# decay coefficient function
ampfunc = SampleProblem.get_ampfunc(args.amp_type, args.decay_exp, args.gamma, args.N)
print "ampfunc", [ampfunc(m) for m in range(args.M_terms)]

# tensorized polynomial
def Hmu(y, mu, H):
    return np.prod([H(mum, ym) for mum, ym in zip(mu, y)])

# --- probabilist ---
h = StochasticHermitePolynomials(normalised=False)
# ...normalised probabilist
nh = StochasticHermitePolynomials(normalised=True)
Ha = lambda n, x: h.eval(n, x)
Hb = lambda n, x: np.sqrt(sp.math.factorial(n)) * nh.eval(n, x)
# not normalised (has to be taken care of in prepare_tildef)
Hc = lambda n, x: nh.eval(n, x)
H, normalised_poly = [(Ha, False), (Hb, False), (Hc, True)][2]

if args.full_set:
    Lambda_mis = MultiindexSet.createFullTensorSet(args.M_terms, args.pcep)
else:
    Lambda_mis = MultiindexSet.createCompleteOrderSet(args.M_terms, args.pcep, True)
Lambda = [Multiindex(mu) for mu in Lambda_mis]
print "Lambda length =", len(Lambda)
# print Lambda
max_m = args.max_m if args.max_m >= 0 else args.M_terms
projected_am = False
tildef = prepare_tildef(lambda m: coeff_field[m][0], pde.f, Lambda, a_maxm=max_m,
                        V=CG if projected_am else None, normalised=normalised_poly)

# sample realisations
ph = PlotHelper()
af, xaf, err = Function(CG), Function(CG), Function(CG)
for nr in range(args.number_realisations):
    sample_rvs = coeff_field.sample_rvs()
    sample = [sample_rvs[m] for m in range(args.M_terms)]
    print "sample %i = %s" % (nr, sample)
    c0 = F0 + sum([sample[m] * Fm[m] for m in range(args.M_terms)])
    c0 = np.exp(-c0) if not args.affine else c0
    af.vector()[:] = c0
    if not args.affine:
        if projected_am:
            c1 = sum([tildef[mu].vector().array() * Hmu(sample, mu, H) for mu in Lambda])
        else:
            c1 = project(sum([tildef[mu] * Hmu(sample, mu, H) for mu in Lambda]), CG).vector().array()
        c1 *= np.exp(-F0)
        xaf.vector()[:] = c1
        err.vector()[:] = c0 - c1
        # ph["expanded coeff"].plot(xaf)
        ph["error"].plot(err, interactive=True)
        from dolfin import assemble, dx
        err_norm, ref_norm = np.sqrt(assemble(err**2*dx)), np.sqrt(assemble(af**2*dx))
        print "expansion error =", err_norm, "\trelative =", err_norm/ref_norm
    # ph["coeff"].plot(af, interactive=True)
