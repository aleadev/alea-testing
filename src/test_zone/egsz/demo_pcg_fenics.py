from __future__ import division
from itertools import count
import numpy as np
from scipy.sparse.linalg.isolve import bicg, bicgstab, gmres, lgmres, lsqr, cgs
from scipy.sparse.linalg import LinearOperator

from alea.application.egsz.multi_vector import MultiVectorSharedBasis
from alea.application.egsz.multi_operator2 import MultiOperator, PreconditioningOperator
from alea.application.egsz.coefficient_field import ParametricCoefficientField
from alea.application.egsz.pcg import pcg
from alea.stochastics.random_variable import NormalRV, UniformRV
from alea.math_utils.multiindex import Multiindex
from alea.linalg.vector import inner, FlatVector
from alea.linalg.operator import MultiplicationOperator

from dolfin import Expression, FunctionSpace, UnitSquareMesh, interpolate, Constant
from alea.application.egsz.fem_discretisation import FEMPoisson
from alea.fem.fenics.fenics_vector import FEniCSVector

import logging
logging.getLogger('FFC').setLevel(logging.WARNING)

a0 = Constant("1.0")
a = (Expression('A*cos(pi*I*x[0])*cos(pi*I*x[1])', A=1 / i ** 2, I=i, degree=2) for i in count(1))
rvs = (UniformRV() for _ in count())
coeff_field = ParametricCoefficientField(func_func=a, rv_func=rvs, mean_func=a0)

pde = FEMPoisson()
A = MultiOperator(coeff_field, pde.assemble_operator)
mis = [Multiindex([0]),
       Multiindex([1]),
       Multiindex([0, 1]),
       Multiindex([0, 2])]
mesh = UnitSquareMesh(4, 4)
fs = FunctionSpace(mesh, "CG", 1)
F = [interpolate(Expression("*".join(["x[0]"] * i)), fs) for i in range(1, 5)]
vecs = [FEniCSVector(f) for f in F]

w = MultiVectorSharedBasis()
for mi, vec in zip(mis, vecs):
    w[mi] = vec
v = A * w
#P = MultiplicationOperator(1, vecs[0].basis)
P = PreconditioningOperator(a0, pde.assemble_solve_operator)
w2, zeta, numit = pcg(A, v, P, 0 * v)

print zeta, numit
print inner(w - w2, w - w2) / inner(w, w)
v2 = A * w2
print inner(v - v2, v - v2) / inner(v, v)


# test with scipy iterative solvers
# =================================

# test to and from euclidian vector conversion
EO = w.to_euclidian_operator
MO = w.from_euclidian_operator
w0 = EO(w)
w1 = MO(w0)
# print "types", type(w), "-----", type(w1)
# print "mis", w.basis._mis, "-----", w1.basis._mis
# print "basis", w1.basis._basis, "-----", w1.basis._basis
print [np.linalg.norm(w[mu].array-w1[mu].array) for mu in w.active_indices()]

def mv(v):
    r = MO(FlatVector(v))       # to multivector
    r2 = A * r                  # operator action
    return EO(r2).coeffs        # to euclidian
N = EO._dimsum
A1 = LinearOperator((N,N), matvec=mv, dtype=np.float64)
b1 = EO(v)
print N, b1.dim, type(b1)

# # preconditioner computing P^-1 * x
# import scipy.sparse.linalg as spla
# M_x = lambda x: spla.spsolve(P, x)
# M = spla.LinearOperator((n, n), M_x)

x1, info = gmres(A1, b1.coeffs, tol=1e-4, maxiter=10)
v1 = A * MO(FlatVector(x1))
print inner(v - v1, v - v1) / inner(v, v)

# x2, info = bicg(A1, b1.coeffs)
# x3, info = bicgstab(A1, b1.coeffs)
x4, info = cgs(A1, b1.coeffs, tol=1e-8)
print np.linalg.norm(x4 - EO(w2).coeffs), np.linalg.norm(x4)
v4 = A * MO(FlatVector(x4))
print inner(v - v4, v - v4) / inner(v, v)
