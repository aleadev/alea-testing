"""
Interface to improve python exception handling
"""

import sys
import traceback


def exceptionTB():
    exc_type, exc_value, exc_traceback = sys.exc_info()
    tb = traceback.format_exception(exc_type, exc_value, exc_traceback)
    tb_string = "#"*50 + "\n" + "Exception Traceback \n"
    for line in tb:
        tb_string += repr(line)
    tb_string += "\n" + "#"*50 + "\n"
    return tb_string
