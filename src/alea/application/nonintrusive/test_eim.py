from __future__ import division
import numpy as np
import argparse
import logging

from dolfin import plot, refine, Mesh, FunctionSpace, set_log_level, WARNING, INFO, ERROR, project, Function

# set FEniCS logging
set_log_level(logging.WARNING)
fenics_logger = logging.getLogger("FFC")
fenics_logger.setLevel(logging.WARNING)
fenics_logger = logging.getLogger("UFL")
fenics_logger.setLevel(logging.WARNING)

# alea imports
from alea.application.egsz.sample_problems2 import SampleProblem
from alea.application.egsz.sample_domains import SampleDomain
from alea.utils.plothelper import PlotHelper

from eim import EIM, get_lagrange_polys, evaluate_polys


def test_lagrange():
    # test interpolation polynomials

    # degree and dimensions
    p, d = 2, 35
    nrpoints = 660
    x = np.random.rand(nrpoints, d)
    mis, L = get_lagrange_polys(x, p)
    C, R = L[0], L[1]
    # print x
    print mis.arr.shape
    # print mis
    # print "C", C
    # print "R", R

    Y = evaluate_polys(x, mis, C=C)
    print "interpolation error", np.linalg.norm(Y - np.identity(Y.shape[0]))


def test_eim():
    # test empirical interpolation EIM for lognormal coefficient

    # generate field samples
    samples, eval_coeff, coeff_field, CG = sample_coeff()

    # setup EIM
    eim = EIM(eval_coeff, samples)

    # check interpolation property
    err = 0
    for s0 in samples:
        y0, b0 = s0
        i0 = eim(y0)
        err0 = np.linalg.norm(b0.vector().array() - i0)
        err += err0**2
    err = np.sqrt(err) / len(samples)
    print "interpolation error =", err

    # sample error
    N, M = 100, len(samples[0][0])
    err = 0
    for nr in range(M):
        sample_rvs = coeff_field.sample_rvs()
        sample = [sample_rvs[m] for m in range(M)]
        f0 = eval_coeff(sample)
        i0 = eim(sample)
        err0 = np.linalg.norm(f0 - i0)
        err += err0 ** 2
    err = np.sqrt(err) / M
    print "sampling error =", err


def sample_coeff():
    parser = argparse.ArgumentParser()
    # parser.add_argument("-ct", "--coeff-type", type=str, default="EF-square-cos",
    parser.add_argument("-ct", "--coeff-type", type=str, default="cos",
                        help="coefficient type")
    parser.add_argument("-cm", "--coeff-mean", type=float, default=1.0,
                        help="coefficient mean")
    parser.add_argument("-at", "--amp-type", type=str, default="decay-inf",
                        help="amplification type")
    parser.add_argument("-aff", "--affine", action="store_true", default=False,
                        help="affine field instead of exponential")
    parser.add_argument("-N", type=int, default=30,
                        help="number of terms for amp-type constant")
    parser.add_argument("-M", "--M-terms", type=int, default=2,
                        help="terms in realisation")
    parser.add_argument("-de", "--decay-exp", type=float, default=2.0,
                        help="decay exponent")
    parser.add_argument("-g", "--gamma", type=float, default=0.9,
                        help="gamma")
    parser.add_argument("-fk", "--freq-skip", type=int, default=0,
                        help="frequency skip")
    parser.add_argument("-fs", "--freq-scale", type=float, default=1.0,
                        help="frequency scaling")
    parser.add_argument("-s", "--scale", type=float, default=1.0,
                        help="scaling")
    parser.add_argument("-nr", "--number-realisations", type=int, default=15,
                        help="number realisations")
    parser.add_argument("-ref", "--mesh-refinements", type=int, default=2,
                        help="initial mesh refinements")
    args = parser.parse_args()
    print "========== PARAMETERS:", vars(args)

    # setup boundary conditions and pde
    mesh, boundaries, dim = SampleDomain.setupDomain("square", initial_mesh_N=10)
    mesh = SampleProblem.setupMesh(mesh, num_refine=args.mesh_refinements)

    # define coefficient field
    coeff_field = SampleProblem.setupCF2(args.coeff_type, amptype=args.amp_type, decayexp=args.decay_exp,
                                         gamma=args.gamma, N=args.N,
                                         freqscale=args.freq_scale, freqskip=args.freq_skip, rvtype="normal",
                                         scale=args.scale,
                                         coeff_mean=args.coeff_mean)

    # prepare expansion terms
    CG = FunctionSpace(mesh, 'CG', 1)
    F0 = project(coeff_field.mean_func, CG).vector().array()
    Fm = [project(coeff_field[m][0], CG).vector().array() for m in range(args.M_terms)]

    # sample realisations
    samples = []
    for nr in range(args.number_realisations):
        sample_rvs = coeff_field.sample_rvs()
        sample = [sample_rvs[m] for m in range(args.M_terms)]
        print "sample %i = %s" % (nr, sample)
        c0 = F0 + sum([sample[m] * Fm[m] for m in range(args.M_terms)])
        c0 = np.exp(-c0) if not args.affine else c0
        af = Function(CG)
        af.vector()[:] = c0
        samples.append([sample, af])

    eval_coeff = lambda y: F0 + sum([ym * Fm[m] for m, ym in enumerate(y)])
    return samples, eval_coeff, coeff_field, CG

# test_lagrange()
test_eim()
