from __future__ import division
import scipy.sparse as sps
from dolfin import *
import itertools as iter
import copy
import sympy as syp
import numpy as np


try:
    new_dolfin = float(dolfin_version()[:3]) >= 1.4
    print "dolfin version", dolfin_version(), new_dolfin
except:
    new_dolfin = False


def coo_submatrix_pull(matr, rows, cols, to_dense = False):
    """
    Pulls out an arbitrary i.e. non-contiguous submatrix out of a sparse.coo_matrix.
    https://gist.github.com/dwf/828099
    """
    if type(rows) != np.ndarray:
        rows = np.array(rows)
        cols = np.array(cols)

    if type(matr) != sps.coo_matrix:
        raise TypeError('matrix must be sparse COOrdinate format')

    gr = -1 * np.ones(matr.shape[0])
    gc = -1 * np.ones(matr.shape[1])

    lr = len(rows)
    lc = len(cols)

    ar = np.arange(0, lr)
    ac = np.arange(0, lc)

    gr[rows[ar]] = ar
    gc[cols[ac]] = ac
    mrow = matr.row
    mcol = matr.col
    newelem = (gr[mrow] > -1) & (gc[mcol] > -1)
    newrows = mrow[newelem]
    newcols = mcol[newelem]
    if to_dense:
        raise NotImplementedError
    else:
        return sps.coo_matrix((matr.data[newelem], np.array([gr[newrows], gc[newcols]])), (lr, lc)).tocsr()


def get_vertex_patch(mesh, v):
    # determine patch cells
    cids = set(v.entities(2).tolist())

    # determine inner facets
    inner_fids = [Cell(mesh, int(cid)).entities(1).tolist() for cid in cids]
    inner_fids = set(iter.chain(*inner_fids))

    # determine boundary facets
    boundary_fids, dirichlet_fids = [], []
    for cid in cids:
        c = Cell(mesh, int(cid))
        for f in facets(c):
            flist = f.entities(2).tolist()
            for fcid in flist:
                if fcid not in cids or len(flist)==1:
                    fid = f.index()
                    if len(flist)==2:
                        # inner boundary
                        boundary_fids.append(fid)
                    else:
                        # outer Dirichlet boundary
                        dirichlet_fids.append(fid)
                    break
    return cids, set(inner_fids), set(boundary_fids), set(dirichlet_fids)


def get_groups_weights(mesh, single_patches=False):
    # CG1 data
    V = FunctionSpace(mesh, 'CG', 1)
    if new_dolfin:
        vertex_dof_map = dof_to_vertex_map(V)
    else:
        vertex_dof_map = V.dofmap().vertex_to_dof_map(mesh)
    dof_list = vertex_dof_map.tolist()
    phi_coeffs = np.ndarray(V.dim())

    # DG0 data
    DG0 = FunctionSpace(mesh, 'DG', 0)
    DG0_dofs = dict([(c.index(),DG0.dofmap().cell_dofs(c.index())[0]) for c in cells(mesh)])
    xi_coeffs = np.ndarray(DG0.dim())

    # retrieve patches
    patches = [(v.index(), get_vertex_patch(mesh, v)) for v in vertices(mesh)]
    tmp_patches = copy.copy(patches)
    groups, phis, xis = [], [], []

    # collect patches
    while True:
        if len(tmp_patches) == 0:
            break
        else:
            current_set = tmp_patches.pop()
            # setup phi
            phi_coeffs[:] = 0
            phi_coeffs[dof_list.index(current_set[0])] = 1
            # setup xi
            xi_coeffs[:] = 0
            for cid in current_set[1][0]:
                xi_coeffs[DG0_dofs[int(cid)]] = 1
        unused_patches = []
        for patch in tmp_patches:
            # check that cell and boundary facet ids are disjoint
            if current_set[1][0].isdisjoint(patch[1][0]) and current_set[1][2].isdisjoint(patch[1][2]) and not single_patches:
                # update cell and facet ids of group
                for csi, pi in zip(current_set[1], patch[1]):
                    csi.update(pi)
                # update weight phi
                phi_coeffs[dof_list.index(patch[0])] = 1
                # update characteristic function xi
                for cid in patch[1][0]:
                    xi_coeffs[DG0_dofs[int(cid)]] = 1
            else:
                unused_patches.append(patch)
        tmp_patches = unused_patches
        # add new group
        groups.append(current_set)
        # construct phi for group
        phi = Function(V)
        phi.vector()[:] = phi_coeffs
        phis.append(phi)
        # construct xi for group
        xi = Function(DG0)
        xi.vector()[:] = xi_coeffs
        xis.append(xi)
    return groups, phis, xis


def prepare_data(inhomogeneous_kappa, A, B, degrees, data_degree, solution_type, fixed_f=1, f_degree_inc=0):
    DATA = lambda: None
    x0, x1 = syp.Symbol('x[0]'), syp.Symbol('x[1]')
    if inhomogeneous_kappa:
        kappa_sym = 3 + syp.sin(A*syp.pi*x0) + syp.sin(B*syp.pi*x1)
        kappa_str = str(kappa_sym)
        kappa_str = kappa_str.replace("x[0]**4","x[0]*x[0]*x[0]*x[0]").replace("x[1]**4","x[1]*x[1]*x[1]*x[1]")
        kappa_str = kappa_str.replace("x[0]**2","x[0]*x[0]").replace("x[1]**2","x[1]*x[1]")
        print "====== kappa_sym", kappa_str, "======"
        kappa = Expression(kappa_str, degree = data_degree)
        # set quadrature degree for rhs assembly
        f_degree = 4 + f_degree_inc
        assemble_quad_degree = max(degrees) * 2 + A * B if inhomogeneous_kappa else -1 #max(degrees) * 2
        assemble_quad_degree = max(assemble_quad_degree, max(degrees) + f_degree)
        assemble_quad_degree = min(assemble_quad_degree, 25)   # restrict quadrature
    else:
        kappa_sym = fixed_f
        kappa = Constant(fixed_f)
        assemble_quad_degree = max(degrees) * 2 # + 5
        f_degree = 0 + f_degree_inc

    print "=============== assemble_quad_degree ", assemble_quad_degree
    # determine rhs for analytic solution
    if solution_type > 0:
        assert solution_type in (1,2)
        if solution_type == 1:
            u_sym = x0*(1-x0)*x1*(1-x1)
        elif solution_type == 2:
            u_sym = syp.sin(syp.pi*x0)*syp.sin(syp.pi*x1)
        Du_sym = syp.diff(u_sym, x0), syp.diff(u_sym, x1)
        print "====== u_sym", str(u_sym), "======"
        print "====== Du_sym", str(Du_sym), "======"
        sol_u = Expression(str(u_sym), degree = data_degree)
        sol_Du = Expression((str(Du_sym[0]),str(Du_sym[1])), degree = data_degree)
        f0 = syp.diff(kappa_sym * syp.diff(u_sym, x0), x0)
        f1 = syp.diff(kappa_sym * syp.diff(u_sym, x1), x1)
        f_sym = -(f0 + f1)
        f_str = str(f_sym).replace("pi**2", "pi*pi").replace("cos(pi*x[0])**2", "cos(pi*x[0])*cos(pi*x[0])").replace("cos(pi*x[1])**2", "cos(pi*x[1])*cos(pi*x[1])")
        f_str = f_str.replace("x[0]**4","x[0]*x[0]*x[0]*x[0]").replace("x[1]**4","x[1]*x[1]*x[1]*x[1]")
        f_str = f_str.replace("x[0]**3","x[0]*x[0]*x[0]").replace("x[1]**3","x[1]*x[1]*x[1]")
        f_str = f_str.replace("x[0]**2","x[0]*x[0]").replace("x[1]**2","x[1]*x[1]")
        print "====== f_sym", f_str, "======"
        f = Expression(f_str, degree = data_degree)
        DATA.sol_u = sol_u
        DATA.sol_Du = sol_Du
    else:
        f = Constant(1)

    DATA.assemble_quad_degree = assemble_quad_degree
    DATA.kappa = kappa
    DATA.f = f
    DATA.f_degree = f_degree
    return DATA
