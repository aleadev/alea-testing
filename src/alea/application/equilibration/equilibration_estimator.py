
"""EM1 a posteriori global mixed and local equilibration estimator (FEniCS centric implementation)"""

from __future__ import division
import numpy as np
import scipy.sparse as sps
from scipy.sparse.linalg import spsolve
import itertools as iter
import copy
from dolfin import (assemble, dot, nabla_grad, dx, avg, dS, sqrt, norm, VectorFunctionSpace, cells,
                    Constant, FunctionSpace, TestFunction, CellSize, FacetNormal, parameters, inner,
                    TestFunctions, TrialFunctions, TrialFunction, div, Function, CellFunction, Measure,
                    vertices, Vertex, FacetFunction, Cell, facets, jump, avg, project, solve, plot,
                    vertex_to_dof_map, interpolate)

from alea.application.egsz.coefficient_field import CoefficientField
from alea.application.egsz.multi_vector import MultiVector
from alea.application.egsz.fem_discretisation import element_degree
from alea.linalg.vector import FlatVector
from alea.math_utils.multiindex import Multiindex
from alea.utils.type_check import takes, anything, list_of, optional

from equilibration_estimator_deterministic import eq_patch_assemblyBDM


import logging
logger = logging.getLogger(__name__)


def get_vertex_patch(mesh, v):
    # determine patch cells
    cids = set(v.entities(2).tolist())

    # determine inner facets
    inner_fids = [Cell(mesh,cid).entities(1).tolist() for cid in cids]
    inner_fids = set(iter.chain(*inner_fids))

    # determine boundary facets
    boundary_fids, dirichlet_fids = [], []
    for cid in cids:
        c = Cell(mesh, cid)
        for f in facets(c):
            flist = f.entities(2).tolist()
            for fcid in flist:
                if fcid not in cids or len(flist)==1:
                    fid = f.index()
                    if len(flist)==2:
                        # inner boundary
                        boundary_fids.append(fid)
                    else:
                        # outer Dirichlet boundary
                        dirichlet_fids.append(fid)
                    break
    return cids, set(inner_fids).difference(set(dirichlet_fids)), set(boundary_fids).difference(set(dirichlet_fids)), set(dirichlet_fids)


def get_groups_weights(mesh):
    # CG1 data
    V = FunctionSpace(mesh, 'CG', 1)
    vertex_dof_map = vertex_to_dof_map(V)
    dof_list = vertex_dof_map.tolist()
    phi_coeffs = np.ndarray(V.dim())

    # DG0 data
    DG0 = FunctionSpace(mesh, 'DG', 0)
    DG0_dofs = dict([(c.index(),DG0.dofmap().cell_dofs(c.index())[0]) for c in cells(mesh)])
    xi_coeffs = np.ndarray(DG0.dim())

    # retrieve patches
    patches = [(v.index(), get_vertex_patch(mesh, v)) for v in vertices(mesh)]
    tmp_patches = copy.copy(patches)
    groups, phis, xis = [], [], []

    # collect patches
    while True:
        if len(tmp_patches) == 0:
            break
        else:
            current_set = tmp_patches.pop()
            # setup phi
            phi_coeffs[:] = 0
            phi_coeffs[dof_list.index(current_set[0])] = 1
            # setup xi
            xi_coeffs[:] = 0
            for cid in current_set[1][0]:
                xi_coeffs[DG0_dofs[int(cid)]] = 1
        unused_patches = []
        for patch in tmp_patches:
            # check that cell and boundary facet ids are disjoint
            if current_set[1][0].isdisjoint(patch[1][0]) and current_set[1][2].isdisjoint(patch[1][2]):
                # update cell and facet ids of group
                for csi, pi in zip(current_set[1], patch[1]):
                    csi.update(pi)
                # update weight phi
                phi_coeffs[dof_list.index(patch[0])] = 1
                # update characteristic function xi
                for cid in patch[1][0]:
                    xi_coeffs[DG0_dofs[int(cid)]] = 1
            else:
                unused_patches.append(patch)
        tmp_patches = unused_patches
        # add new group
        groups.append(current_set)
        # construct phi for group
        phi = Function(V)
        phi.vector()[:] = phi_coeffs
        phis.append(phi)
        # construct xi for group
        xi = Function(DG0)
        xi.vector()[:] = xi_coeffs
        xis.append(xi)
    return groups, phis, xis


def evaluate_oscillations_mv(f, w, osc_quad_degree = 20):
    mu0 = Multiindex()
    mesh = w[mu0]._fefunc.function_space().mesh()
    degree = element_degree(w[mu0]._fefunc)
    DG0 = FunctionSpace(mesh, 'DG', 0)
    dg0 = TestFunction(DG0)
    return evaluate_oscillations(f, mesh, degree, dg0, osc_quad_degree)


def evaluate_oscillations(f, mesh, degree, dg0, osc_quad_degree = 20):
    # project f and evaluate oscillations
    dx = Measure('dx')
    DG0 = FunctionSpace(mesh, 'DG', 0)
    DGP = FunctionSpace(mesh, 'DG', degree-1)
    # try:
    #     Pf = project(f, DG)
    # except:
    #     logger.warning("EXCEPTION while projecting f, falling back to interpolation...")
    #     Pf = interpolate(f, DG)
    Pf0 = project(f, DG0)
    Pfp = project(f, DGP)
    h = CellSize(mesh)
    osc_form = h**2 * ((f - Pf0)**2) * dg0 * dx
    osc_local = assemble(osc_form, form_compiler_parameters={'quadrature_degree': osc_quad_degree})
    osc_global = np.sqrt(np.sum(osc_local.array()))
    osc_local = np.sqrt(osc_local.array())
    return osc_global, osc_local, Pfp


def evaluate_numerical_flux(w, mu, coeff_field, f):
    '''determine numerical flux sigma_nu with solution w'''
    Lambda = w.active_indices()
    maxm = w.max_order
    if len(coeff_field) < maxm:
        logger.warning("insufficient length of coefficient field for MultiVector (%i < %i)", len(coeff_field), maxm)
        maxm = len(coeff_field)

    # get mean field of coefficient and initialise sigma
    a0_f = coeff_field.mean_func
    sigma_mu = a0_f * nabla_grad(w[mu]._fefunc)

    # iterate m
    for m in range(maxm):
        am_f, am_rv = coeff_field[m]

        # prepare polynomial coefficients
        beta = am_rv.orth_polys.get_beta(mu[m])

        # mu
        r_mu = -beta[0] * w[mu]

        # mu+1
        mu1 = mu.inc(m)
        if mu1 in Lambda:
            w_mu1 = w[mu1]
            r_mu += beta[1] * w_mu1

        # mu-1
        mu2 = mu.dec(m)
        if mu2 in Lambda:
            w_mu2 = w[mu2]
            r_mu += beta[-1] * w_mu2

        # add flux contribution
        sigma_mu = sigma_mu + am_f * nabla_grad(r_mu._fefunc)

    # initialise f
    if not mu:
        f_mu = f
    else:
        f_mu = interpolate(Constant(0.0), w[mu]._fefunc.function_space())

    return sigma_mu, f_mu


class GlobalEquilibrationEstimator(object):
    """Evaluation of the equilibration error estimator based on the solution of global mixed problems."""

    @classmethod
    @takes(anything, MultiVector, CoefficientField, anything, anything, optional(int))
    def evaluateEstimator(cls, w, coeff_field, pde, f, quadrature_degree= -1, osc_quadrature_degree = 20):
        """Evaluate equilibration estimator for all active mu of w."""

        # determine rhs oscillations
        mu0 = Multiindex()
        mesh = w[mu0]._fefunc.function_space().mesh()
        degree = element_degree(w[mu0]._fefunc)
        DG0 = FunctionSpace(mesh, 'DG', 0)
#        DG0_dofs = dict([(c.index(),DG0.dofmap().cell_dofs(c.index())[0]) for c in cells(mesh)])
        dg0 = TestFunction(DG0)
        osc_global, osc_local, Pf = evaluate_oscillations(f, mesh, degree, dg0, osc_quadrature_degree)

        # evaluate global equilibration estimators
        eta_local = MultiVector()
        eta = {}
        for mu in w.active_indices():
            eta[mu], eta_local[mu] = cls._evaluateGlobalMixedEstimator(mu, w, coeff_field, pde, Pf, quadrature_degree)
        global_eta = sqrt(sum([v ** 2 for v in eta.values()]))
        return global_eta, eta, eta_local, osc_global, osc_local


    @classmethod
    @takes(anything, Multiindex, MultiVector, CoefficientField, anything, anything, int)
    def _evaluateGlobalMixedEstimator(cls, mu, w, coeff_field, pde, f, quadrature_degree, vectorspace_type=['RT','BDM'][1]):
        """Evaluation of global mixed equilibrated estimator."""
        # set quadrature degree
        logger.debug("residual quadrature order = %i", quadrature_degree)

        # prepare numerical flux, mean a0, and f
        sigma_mu, f_mu = evaluate_numerical_flux(w, mu, coeff_field, f)
        a0_f = coeff_field.mean_func
        logger.info("mixed equilibration: a0_f is %s (%s)" % (a0_f((0,0)), 1/a0_f(0,0)))

        # ###################
        # ## MIXED PROBLEM ##
        # ###################

        # get setup data for mixed problem
        V = w[mu]._fefunc.function_space()
        mesh = V.mesh()
        degree = element_degree(w[mu]._fefunc)

#        # DEBUG===
#        print "=============================="
#        print "==============", mu
#        print "=============================="
#        phi = Function(V)
#        for i in range(V.dim()):
#            phi.vector()[:] = 0
#            phi.vector()[i] = 1
#            print "RES", i, assemble((f_mu*phi - inner(sigma_mu,nabla_grad(phi))) * dx)
#
#        # ===DEBUG

        # create function spaces
        DG0 = FunctionSpace(mesh, 'DG', 0)
        DG0_dofs = [DG0.dofmap().cell_dofs(c.index())[0] for c in cells(mesh)]
        RT = FunctionSpace(mesh, vectorspace_type, degree)
        DGp = FunctionSpace(mesh, 'DG', degree - 1)
        W = RT * DGp

        # create trial and test functions
        (sigma, u) = TrialFunctions(W)
        (tau, v) = TestFunctions(W)

        # define variational form
        a_eq = (dot(sigma, tau) / a0_f + div(tau) * u + div(sigma) * v) * dx
        L_eq = (dot(sigma_mu, tau) / a0_f - f_mu * v) * dx

        # compute solution
        w_eq = Function(W)
        solve(a_eq == L_eq, w_eq, form_compiler_parameters={'quadrature_degree': quadrature_degree})
        (sigma_mixed, u_mixed) = w_eq.split()

        # #############################
        # ## EQUILIBRATION ESTIMATOR ##
        # #############################

        # evaluate error estimator
        dg0 = TestFunction(DG0)
        eta_mu = (1 / a0_f) * inner(sigma_mu - sigma_mixed, sigma_mu - sigma_mixed) * dg0 * dx
        eta_T = assemble(eta_mu, form_compiler_parameters={'quadrature_degree': quadrature_degree})

        # reorder array for local estimator and evaluate global error
        eta_T = eta_T[DG0_dofs] #.array()
        eta = sqrt(sum(eta_T))
        eta_T = np.sqrt(eta_T)

        return eta, FlatVector(eta_T)


class LocalEquilibrationEstimator(object):
    @classmethod
    @takes(anything, MultiVector, CoefficientField, anything, anything, optional(int))
    def evaluateEstimator(cls, w, coeff_field, pde, f, quadrature_degree= -1, osc_quadrature_degree = 20):
        """Evaluate patch local equilibration estimator for all active mu of w."""

        # use Eigen/uBLAS backend for conversion to scipy sparse matrices
        backup_backend = parameters.linear_algebra_backend
        try:
            parameters.linear_algebra_backend = "Eigen"
        except:
            parameters.linear_algebra_backend = "uBLAS"

        # determine rhs oscillations
        mu0 = Multiindex()
        mesh = w[mu0]._fefunc.function_space().mesh()
        degree = element_degree(w[mu0]._fefunc)
        DG0 = FunctionSpace(mesh, 'DG', 0)
#        DG0_dofs = dict([(c.index(),DG0.dofmap().cell_dofs(c.index())[0]) for c in cells(mesh)])
        dg0 = TestFunction(DG0)
        osc_global, osc_local, Pf = evaluate_oscillations(f, mesh, degree, dg0, osc_quadrature_degree)

        # prepare data for grouped assembly
        groups, phis, xis = get_groups_weights(mesh)

        # evaluate global equilibration estimators
        eta_local = MultiVector()
        eta = {}
        for mu in w.active_indices():
            # NOTE: in the following, the deterministic code of [EM] is used although the implementation in this class
            # should also work (or at least be easy to fix...)
            if True:
                eq_type, epsilon = 0, 1e-6
                kappa = coeff_field.mean_func
                V = w[mu0]._fefunc.function_space()
                mesh = V.mesh()
                sigma_mu, f_mu = evaluate_numerical_flux(w, mu, coeff_field, f)
                PP = V.ufl_element().degree() - 1 + eq_type
                eta_mu, global_est2_mu, tau_global_mu = eq_patch_assemblyBDM(V, eq_type, sigma_mu, f_mu, kappa, epsilon, assemble_quad_degree=quadrature_degree, check_res=False)
                eta[mu], eta_local[mu] = np.sqrt(global_est2_mu), FlatVector(eta_mu)
            else:
                eta[mu], eta_local[mu] = cls._evaluateLocalEstimatorBDM(mu, w, coeff_field, pde, quadrature_degree, groups, phis, xis)
        global_eta = np.sqrt(sum([v ** 2 for v in eta.values()]))

        # restore backend and return estimator
        parameters.linear_algebra_backend = backup_backend
        return global_eta, eta, eta_local, osc_global, osc_local


    # =================================================
    # NOTE: the following implementation is not used!!!
    # =================================================
    @classmethod
    @takes(anything, Multiindex, MultiVector, CoefficientField, anything, anything, int)
    def _evaluateLocalEstimator(cls, mu, w, coeff_field, pde, f, quadrature_degree, groups, phis, xis, epsilon=1e-4):
        """Evaluation of patch local equilibrated estimator."""

        # prepare numerical flux, mean a0 and f
        sigma_mu, f_mu = evaluate_numerical_flux(w, mu, coeff_field, f)
        a0_f = coeff_field.mean_func

        # ####################
        # ## LOCAL PROBLEMS ##
        # ####################

        # get setup data for mixed problem
        V = w[mu]._fefunc.function_space()
        mesh = V.mesh()
        mesh.init()
        # degree = element_degree(w[mu]._fefunc)

        logger.debug("solving %i patch problems for multiindex %s" %(mesh.num_vertices(), mu))

        degree = V.ufl_element().degree()
        mesh = V.mesh()
        mesh.init()
        logger.info("========>>> solving %i patch problems..." % mesh.num_vertices())
        # space for rhs projection
#        print("*"*80," ---", degree)
        DGm1 = FunctionSpace(mesh, "DG", degree-1)
        # DG0 localisation
        DG0 = FunctionSpace(mesh, 'DG', 0)
        DG0_dofs = dict([(c.index(),DG0.dofmap().cell_dofs(c.index())[0]) for c in cells(mesh)])
        dg0 = TestFunction(DG0)
        # mesh data
        h = CellSize(mesh)
        n = FacetNormal(mesh)

#        # DEBUG===
#        print "=============================="
#        print "==============", mu
#        print "=============================="
#        dx = Measure('dx')
#        phi = Function(V)
#        nonz = 0
#        for i in range(V.dim()):
#            phi.vector()[:] = 0
#            phi.vector()[i] = 1
#            res = assemble((f_mu*phi - inner(sigma_mu,nabla_grad(phi))) * dx)
#            print "RES", i, res
#            if res > 1e-8:
#                nonz += 1
#        print "NONZZZZZZZZZZZZ", nonz
#        # ===DEBUG

        # evaluate \tilde{sigma} and projection of projection error
        # VV1 = VectorFunctionSpace(mesh, 'DG', degree-1)
        VV2 = VectorFunctionSpace(mesh, 'DG', degree)

#        sigma_mu = project(Constant((1,0)),VV2)
#        print "QQQQQQQQQQQQQQ", sigma_mu.vector().array()

        tildesigma_mu = project(sigma_mu, VV2, form_compiler_parameters={'quadrature_degree': quadrature_degree})

        # setup global equilibrated flux vector
        # DG = VectorFunctionSpace(mesh, "DG", degree)
        # TODO: remove either DG or VV2
        DG = VV2
        DG_dofmap = DG.dofmap()

        # define form functions
        tau = TrialFunction(DG)
        v = TestFunction(DG)

        # define global tau
        tau_local = Function(DG)
        tau_global = Function(DG)
        tau_global.vector()[:] = 0.0

        # iterate groups
        group_cf = CellFunction('size_t', mesh)
        group_ff = FacetFunction('size_t', mesh)
        for i, G in enumerate(zip(groups, phis, xis)):
            g, phi, xi = G
            logger.info("************* PATCH GROUP %i of %i *************" % (i,len(groups)))

            # extract cell and boundary facet ids for group
            group_cids, group_inner_fids, group_boundary_fids = g[1][0], g[1][1], g[1][2]

            # determine local dofs
            lDG_cell_dofs = dict([(cid, DG_dofmap.cell_dofs(cid)) for cid in group_cids])
            lDG_dofs = [cd.tolist() for cd in lDG_cell_dofs.values()]
            lDG_dofs = list(iter.chain(*lDG_dofs))

            # create patch group measures
            group_cf.set_all(0)
            group_ff.set_all(0)
            # group patch cells
            for i in group_cids:
                group_cf[int(i)] = 1
            # group patch inner facets
            for i in group_inner_fids:
                group_ff[int(i)] = 1
            # group patch boundary facets
            for i in group_boundary_fids:
                group_ff[int(i)] = 2
            dx = Measure('dx')[group_cf]
            dS = Measure('dS')[group_ff]

            # DEBUG---
#            assemble(project(f_mu * phi,V) * dx(1))
#            assemble(inner(sigma_mu,nabla_grad(phi)) * dx(1))
#            restest = assemble((f_mu * phi - inner(sigma_mu,nabla_grad(phi))) * dx(1), keep_diagonal=True, form_compiler_parameters={'quadrature_degree': quadrature_degree})
#            from dolfin import ds
#            print "mu", mu, "phi*ds:", assemble(phi * ds)
#            print "####restest:", restest
            # ---DEBUG

            # project rhs f
            pf_mu = project(phi * (f_mu + div(tildesigma_mu)), DGm1, form_compiler_parameters={'quadrature_degree': quadrature_degree})

#            # DEBUG===
#            print "=============================="
#            print "==============", mu
#            print "=============================="
#            dx = Measure('dx')
#            phi2 = Function(V)
#            for i in range(V.dim()):
#                phi2.vector()[:] = 0
#                phi2.vector()[i] = 1
#                res = assemble((f_mu*phi2 - inner(tildesigma_mu,nabla_grad(phi2))) * dx)
#                print "RES", i, res
#            # ===DEBUG

            # define forms
            alpha = Constant(1 / epsilon) * (h * h)

            a = (1 / a0_f) * inner(tau,v) * dx(1) + alpha * div(tau) * div(v) * dx(1) + avg(alpha) * jump(tau,n) * jump(v,n) * dS(1)\
                + avg(alpha) * jump(xi * tau,n) * jump(v,n) * dS(2)
            L = - alpha * pf_mu * div(v) * dx(1)\
                - avg(alpha) * jump(tildesigma_mu,n) * jump(v,n) * avg(phi) * dS(1)

#            print "L2 f + div(sigma)", assemble((f + div(sigma))*(f + div(sigma))*dx(0))

            # assemble forms
            lhs = assemble(a, keep_diagonal=True, form_compiler_parameters={'quadrature_degree': quadrature_degree})
            lhs.ident_zeros()
            rhs = assemble(L)

            # convert DOLFIN representation to scipy sparse arrays
            rows, cols, values = as_backend_type(lhs).data()
            lhsA = sps.csr_matrix((values, cols, rows)).tocoo()

            # slice sparse matrix and solve linear problem
            lhsA = coo_submatrix_pull(lhsA, lDG_dofs, lDG_dofs)
            lx = spsolve(lhsA, rhs.array()[lDG_dofs])

            # add up local fluxes
            tau_global.vector()[lDG_dofs] += lx
#            tau_global.vector()[:] += project(-tildesigma_mu*phi,VV2).vector()
#            tau_global.vector()[lDG_dofs] += project(-tildesigma_mu*phi,VV2).vector()[lDG_dofs]

            if False:
                pf2 = project(phi * f_mu, DGm1, form_compiler_parameters={'quadrature_degree': quadrature_degree})
                rphi = pf2 - inner(tildesigma_mu, nabla_grad(phi))
                rphi2 = f_mu * phi - inner(sigma_mu, nabla_grad(phi))
                rphi22 = inner(sigma_mu, nabla_grad(phi))
                rphi3 = pf2 - f_mu * phi
                rphi4 = tildesigma_mu - sigma_mu
                rphi5 = (tildesigma_mu - sigma_mu) * phi
                rphi6 = inner(tildesigma_mu - sigma_mu, nabla_grad(phi))
                rp = assemble(rphi * dx(1), form_compiler_parameters={'quadrature_degree': quadrature_degree})
                rp2 = assemble(rphi2 * dx(1), form_compiler_parameters={'quadrature_degree': quadrature_degree})
                rp22 = assemble(rphi22 * dx(1), form_compiler_parameters={'quadrature_degree': quadrature_degree})
                rp3 = assemble(rphi3 * dx(1))
                rp40 = assemble(rphi4[0] * dx(1))
                rp41 = assemble(rphi4[1] * dx(1))
                rp50 = assemble(rphi5[0] * dx(1))
                rp51 = assemble(rphi5[1] * dx(1))
                rp6 = assemble(rphi6 * dx(1))
                rp7 = assemble((rphi - rphi2) * dx(1), form_compiler_parameters={'quadrature_degree': quadrature_degree})
                rp8 = assemble((rphi3 - rphi6) * dx(1), form_compiler_parameters={'quadrature_degree': quadrature_degree})
                print "RP-A %.4e %.4e %.4e" %(rp, rp2, rp22)
                print "RP-B", rp3, rp6
                # print "RP-C", rp40, rp41, rp50, rp51
                print "RP-D", rp7, rp8
                print "tildesigma", mu, np.linalg.norm(tildesigma_mu.vector().array())
                print "f", np.linalg.norm(f_mu.vector().array())

        if False:
            R = lhsA*lx - rhs.array()[lDG_dofs]
            print(">>>>solver res", np.linalg.norm(R))
            
            dxA = Measure('dx')
            dSA = Measure('dS')
            # sanity check for tau_global
            res1 = div(tau_global) + div(tildesigma_mu) + f_mu
            res1 = assemble(inner(res1, res1) * dxA, form_compiler_parameters={'quadrature_degree': quadrature_degree})
            logger.debug("================= res1 %f" % res1)
            print(">>>================= res1 %f" % res1)
            print(">>>>>>>>>>", f_mu(0,0), "---", mu)
            print("div(tauglobal)", assemble((div(tau_global) + div(tildesigma_mu)) * dxA, form_compiler_parameters={'quadrature_degree': quadrature_degree}))

            n = FacetNormal(mesh)
            res2 = jump(tau_global + tildesigma_mu, n)
            res2 = assemble(res2**2 * dSA, form_compiler_parameters={'quadrature_degree': quadrature_degree})
            logger.debug("================= res2 %f" % res2)
            print(">>>================= res2 %f" % res2)

#        print "TTTTTTTTTTTT", tildesigma_mu.vector().array()
#        print "ZZZZZZZZZZZZ", tau_global.vector().array()

        # evaluate estimator
        # maybe TODO: re-define measure dx
        sigmadist = sigma_mu - tildesigma_mu
        eq_est = assemble( (1 / a0_f) * inner(tau_global + sigmadist, tau_global + sigmadist) * dg0 * (dx(0)+dx(1)),\
                           form_compiler_parameters={'quadrature_degree': quadrature_degree} )

        # reorder according to cell ids
        eq_est = eq_est[DG0_dofs.values()]
        global_est = np.sqrt(np.sum(eq_est.array()))
        return global_est, FlatVector(np.sqrt(eq_est))#, tau_global


    # =================================================
    # NOTE: the following implementation is not used!!!
    # =================================================
    @classmethod
    @takes(anything, Multiindex, MultiVector, CoefficientField, anything, anything, int)
    def _evaluateLocalEstimatorBDM(cls, mu, w, coeff_field, pde, f, quadrature_degree, groups, phis, xis, epsilon=1e-4):
        """Evaluation of patch local equilibrated estimator."""

        # prepare numerical flux, mean a0 and f
        sigma_mu, f_mu = evaluate_numerical_flux(w, mu, coeff_field, f)
        a0_f = coeff_field.mean_func

        # ####################
        # ## LOCAL PROBLEMS ##
        # ####################

        # get setup data for mixed problem
        V = w[mu]._fefunc.function_space()
        mesh = V.mesh()
        mesh.init()
        # degree = element_degree(w[mu]._fefunc)

        logger.debug("solving %i patch problems for multiindex %s" %(mesh.num_vertices(), mu))

        degree = V.ufl_element().degree()
        mesh = V.mesh()
        mesh.init()
        logger.info("========>>> solving %i patch problems..." % mesh.num_vertices())

        h = CellSize(mesh)
        n = FacetNormal(mesh)
        cf = CellFunction('size_t', mesh)
        # DG0 localisation
        DG0 = FunctionSpace(mesh, 'DG', 0)
        DG0_dofs = dict([(c.index(),DG0.dofmap().cell_dofs(c.index())[0]) for c in cells(mesh)])
        dg0 = TestFunction(DG0)

        # setup global equilibrated flux vector
        print "############### using BDM of degree %i ##################" %degree
        BDM = FunctionSpace(mesh, "BDM", degree)
        BDM_dofmap = BDM.dofmap()
        DGm1 = FunctionSpace(mesh, "DG", degree-1)
        W = BDM * DGm1
        W_dofmap = W.dofmap()
        w = Function(W)

        # define form functions
        sigma, u = TrialFunctions(W)
        tau, v = TestFunctions(W)

        # define global tau
        tau_global = Function(BDM)
        tau_global.vector()[:] = 0.0

        # iterate groups
        group_cf = CellFunction('size_t', mesh)
        group_ff = FacetFunction('size_t', mesh)
        for i, G in enumerate(zip(groups, phis, xis)):
            g, phi, xi = G
            logger.info("************* PATCH GROUP %i of %i *************" % (i, len(groups)))

            # extract cell and boundary facet ids for group
            group_cids, group_inner_fids, group_boundary_fids = g[1][0], g[1][1], g[1][2]

            # determine local dofs
#            lDG_cell_dofs = dict([(cid, DG_dofmap.cell_dofs(cid)) for cid in group_cids])
#            lDG_dofs = [cd.tolist() for cd in lDG_cell_dofs.values()]
#            lDG_dofs = list(iter.chain(*lDG_dofs))

            # create patch group measures
            group_cf.set_all(0)
            group_ff.set_all(0)
            # group patch cells
            for i in group_cids:
                group_cf[int(i)] = 1
            # group patch inner facets
            for i in group_inner_fids:
                group_ff[int(i)] = 1
            # group patch boundary facets
            for i in group_boundary_fids:
                group_ff[int(i)] = 2
            dx = Measure('dx')[group_cf]
            dS = Measure('dS')[group_ff]

            # define forms
            alpha = Constant(1/epsilon) * (h * h)
            pf = project(phi * f_mu - inner(nabla_grad(phi), sigma_mu), DGm1)
            a_eq = xi * (1 / a0_f) * inner(sigma, tau) * dx(1) + xi * div(tau) * u * dx(1) + xi * div(sigma) * v * dx(1) +\
                    alpha('+') * inner(avg(sigma), n('+')) * inner(avg(tau), n('+')) * dS(2)
            L_eq = xi * (1 / a0_f) * phi * inner(sigma_mu, tau) * dx(1) - xi * pf * v * dx(1)

            # assemble forms
            lhs = assemble(a_eq, keep_diagonal=True, form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
            lhs.ident_zeros()
            rhs = assemble(L_eq)

            # convert DOLFIN representation to scipy sparse arrays
            try:
                rows, cols, values = as_backend_type(lhs).data()
            except:
                rows, cols, values = lhs.data()
            lhsA = sps.csr_matrix((values, cols, rows)).tocoo()

            # determine local dofs
            lW_cell_dofs = dict([(cid, W_dofmap.cell_dofs(int(cid))) for cid in group_cids])
            lW_dofs = [cd.tolist() for cd in lW_cell_dofs.values()]
            lW_dofs = list(set(iter.chain(*lW_dofs)))
            lBDM_cell_dofs = dict([(cid, BDM_dofmap.cell_dofs(int(cid))) for cid in group_cids])
            lBDM_dofs = [cd.tolist() for cd in lBDM_cell_dofs.values()]
            lBDM_dofs = list(set(iter.chain(*lBDM_dofs)))

            # slice sparse matrix and solve linear problem
            lhsA = coo_submatrix_pull(lhsA, lW_dofs, lW_dofs)
            lx = spsolve(lhsA, rhs.array()[lW_dofs])

            # separate BDM dofs from mixed space function
            vw = np.zeros(W.dim())
            vw[lW_dofs] = lx
            w.vector().set_local(vw)
            w_sigma, _ = w.split(True)

            # add up local fluxes
            tau_global.vector()[lBDM_dofs] += w_sigma.vector().array()[lBDM_dofs]

        # evaluate estimator
        # maybe TODO: re-define measure dx
        eq_est = assemble( (1 / a0_f) * inner(tau_global - sigma_mu, tau_global - sigma_mu) * dg0 * (dx(0) + dx(1)),\
                           form_compiler_parameters={'quadrature_degree': quadrature_degree} )

        # reorder according to cell ids
        eq_est = eq_est[DG0_dofs.values()]
        global_est = np.sqrt(np.sum(eq_est.array()))
        return global_est, FlatVector(np.sqrt(eq_est))


def coo_submatrix_pull(matr, rows, cols):
    """
    Pulls out an arbitrary i.e. non-contiguous submatrix out of a sparse.coo_matrix.
    https://gist.github.com/dwf/828099
    """
    if type(rows) != np.ndarray:
        rows = np.array(rows)
        cols = np.array(cols)

    if type(matr) != sps.coo_matrix:
        raise TypeError('matrix must be sparse COOrdinate format')

    gr = -1 * np.ones(matr.shape[0])
    gc = -1 * np.ones(matr.shape[1])

    lr = len(rows)
    lc = len(cols)

    ar = np.arange(0, lr)
    ac = np.arange(0, lc)

    gr[rows[ar]] = ar
    gc[cols[ac]] = ac
    mrow = matr.row
    mcol = matr.col
    newelem = (gr[mrow] > -1) & (gc[mcol] > -1)
    newrows = mrow[newelem]
    newcols = mcol[newelem]
    return sps.coo_matrix((matr.data[newelem], np.array([gr[newrows], gc[newcols]])), (lr, lc)).tocsr()
