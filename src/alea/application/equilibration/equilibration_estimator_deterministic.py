from __future__ import division
from dolfin import *
import numpy as np
import itertools as iter
import scipy.sparse as sps
from scipy.sparse.linalg import spsolve
from estimator_helper_deterministic import coo_submatrix_pull, get_groups_weights


try:
    new_dolfin = float(dolfin_version()[:3]) >= 1.4
    print "dolfin version", dolfin_version(), new_dolfin
except:
    new_dolfin = False


def evaluate_osc(mesh, degree, f, Pf, osc_quad_degree = 15):
    # DG0 localisation
    DG0 = FunctionSpace(mesh, 'DG', 0)
    dg0 = TestFunction(DG0)
    h = CellSize(mesh)
    # project f and evaluate oscillations
    ff = project(f, FunctionSpace(refine(mesh), 'CG', degree + 1))
    dx = Measure('dx')
    if new_dolfin:
        osc_form = h ** 2 * ((ff - Pf) ** 2) * dg0 * dx(domain=mesh)
    else:
        osc_form = h ** 2 * ((ff - Pf) ** 2) * dg0 * dx
    osc_local = assemble(osc_form, form_compiler_parameters={'quadrature_degree': osc_quad_degree})
    osc_global = np.sqrt(np.sum(osc_local.array()))
    osc_local = np.sqrt(osc_local.array())
    return osc_global, osc_local


def eq_patch_assembly(V, eq_type, sigma, f, Pf, kappa, epsilon, assemble_quad_degree = 15, project_sigma=True, check_res=False):
    # setup mesh
    V_dm = V.dofmap()
    degree = V.ufl_element().degree() + eq_type
    mesh = V.mesh()
    mesh.init()

    # mesh data
    h = CellSize(mesh)
    n = FacetNormal(mesh)
    cf = CellFunction('size_t', mesh)

    # DG0 localisation
    DG0 = FunctionSpace(mesh, 'DG', 0)
    DG0_dofs = dict([(c.index(), DG0.dofmap().cell_dofs(c.index())[0]) for c in cells(mesh)])
    dg0 = TestFunction(DG0)

    # evaluate \tilde{sigma} and projection of projection error
    if project_sigma:
        VV2 = VectorFunctionSpace(mesh, 'DG', degree)
        # VV1 = VectorFunctionSpace(mesh, 'DG', degree-1)
        # VV1 = VectorFunctionSpace(mesh, 'CG', degree-1)
        # VV2 = VectorFunctionSpace(mesh, 'CG', degree)
        VV3 = VectorFunctionSpace(refine(mesh), 'CG', degree+1)
        tildesigma = project(sigma, VV2, form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
        # sigmaerror = project(tildesigma - sigma, VV2, form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
        if check_res:
            sigmaerror = project(tildesigma - project(sigma, VV3, form_compiler_parameters={'quadrature_degree': assemble_quad_degree}),
                                                                    VV2, form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
            print "||sigmaerror|| =", norm(sigmaerror, 'L2')
    else:
        tildesigma = sigma

    # use Eigen/uBLAS backend for conversion to scipy sparse matrices
    backend_backup = parameters.linear_algebra_backend
    try:
        parameters.linear_algebra_backend = "Eigen"
    except:
        parameters.linear_algebra_backend = "uBLAS"

    print "========>>> solving %i patch problems..." % mesh.num_vertices()
    # setup error estimator vector
    eq_est = np.zeros(DG0.dim())

    # setup global equilibrated flux vector
    DG = VectorFunctionSpace(mesh, "DG", degree)
    DG_dofmap = DG.dofmap()
    DGm1 = FunctionSpace(mesh, "DG", degree-1)

    # define form functions
    tau = TrialFunction(DG)
    v = TestFunction(DG)

    # define global tau
    tau_local = Function(DG)
    tau_global = Function(DG)
    tau_global.vector()[:] = 0.0

    # define global Qf
    Qf = Function(DGm1)
    Qf.vector()[:] = 0.0

    # prepare grouped assembly
    groups, phis, xis = get_groups_weights(mesh, single_patches=False)

    print "==== generated %i groups of %i patches ====" % (len(groups), mesh.num_vertices())

    # iterate groups
    group_cf = CellFunction('size_t', mesh)
    group_ff = FacetFunction('size_t', mesh)
    for i, G in enumerate(zip(groups, phis, xis)):
        g, phi, xi = G
        print "************* PATCH GROUP %i of %i *************" %(i,len(groups))

        # extract cell and boundary facet ids for group
        group_cids, group_inner_fids, group_boundary_fids = g[1][0], g[1][1], g[1][2]

        # determine local dofs
        lDG_cell_dofs = dict([(cid, DG_dofmap.cell_dofs(int(cid))) for cid in group_cids])
        lDG_dofs = [cd.tolist() for cd in lDG_cell_dofs.values()]
        lDG_dofs = list(iter.chain(*lDG_dofs))

        # print "\nlocal DG subspace has dimension", len(lDG_dofs), "degree", degree, "cells", len(patch_cid), patch_cid
        # print "local DG_cell_dofs", lDG_cell_dofs
        # print "local DG_dofs", lDG_dofs

        # create patch group measures
        group_cf.set_all(0)
        group_ff.set_all(0)
        # group patch cells
        for i in group_cids:
            group_cf[int(i)] = 1
        # group patch inner facets
        for i in group_inner_fids:
            group_ff[int(i)] = 1
        # group patch boundary facets
        for i in group_boundary_fids:
            group_ff[int(i)] = 2
        dx = Measure('dx')[group_cf]
        dS = Measure('dS')[group_ff]

        # DEBUG ---
        if False:
            restest = assemble((f * phi - inner(sigma,nabla_grad(phi))) * dx(1), form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
            from dolfin import ds
            print "phi*ds:", assemble(phi * ds)
            print "####restest:", restest
        # --- DEBUG

        # plot patch
        if False:
            ph["xi_z"].plot(xi_z)
            ph["phi_z"].plot(phi_z)
            ph["cf"].plot(cf)
            ph["FF_inner"].plot(FF_inner)
            ph["FF_boundary"].plot(FF_boundary)
            interactive()

        # determine best approximation of localised f
        parameters.linear_algebra_backend = backend_backup
        pf = project(phi * (f + div(tildesigma)), DGm1, form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
        Qf.vector()[:] = Qf.vector().array() + pf.vector().array()
        try:
            parameters.linear_algebra_backend = "Eigen"
        except:
            parameters.linear_algebra_backend = "uBLAS"

        if check_res:
            pf2 = project(phi * f, DGm1, form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
            rphi = pf2 - inner(tildesigma, nabla_grad(phi))
            rphi2 = f * phi - inner(sigma, nabla_grad(phi))
            rphi3 = pf2 - f * phi
            rphi4 = tildesigma - sigma
            rphi5 = (tildesigma - sigma) * phi
            rphi6 = inner(tildesigma - sigma, nabla_grad(phi))
            rp = assemble(rphi * dx(1), form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
            rp2 = assemble(rphi2 * dx(1), form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
            rp3 = assemble(rphi3 * dx(1))
            rp40 = assemble(rphi4[0] * dx(1))
            rp41 = assemble(rphi4[1] * dx(1))
            rp50 = assemble(rphi5[0] * dx(1))
            rp51 = assemble(rphi5[1] * dx(1))
            rp6 = assemble(rphi6 * dx(1))
            rp7 = assemble((rphi - rphi2) * dx(1), form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
            rp8 = assemble((rphi3 - rphi6) * dx(1), form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
            print "RP-A", rp, rp2
            print "RP-B", rp3, rp6
            # print "RP-C", rp40, rp41, rp50, rp51
            print "RP-D", rp7, rp8

        # define forms
        alpha = Constant(1/epsilon) * (h * h)
        a = (1 / kappa) * inner(tau,v) * dx(1) + alpha * div(tau) * div(v) * dx(1) + avg(alpha) * jump(tau,n) * jump(v,n) * dS(1)\
            + avg(alpha) * jump(xi * tau,n) * jump(v,n) * dS(2)
        L = - alpha * pf * div(v) * dx(1)\
            - avg(alpha) * jump(tildesigma,n) * jump(v,n) * avg(phi) * dS(1)

#        print "L2 f + div(sigma)", assemble((f + div(sigma))*(f + div(sigma))*dx(0))
#         sigmadist = sigma - tildesigma
#         print "L2 sigma dist", assemble(inner(sigmadist, sigmadist)*dx(0))

        # assemble forms
        lhs = assemble(a, keep_diagonal=True, form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
        lhs.ident_zeros()
        rhs = assemble(L)

        # convert DOLFIN representation to scipy sparse arrays
        try:
            rows, cols, values = as_backend_type(lhs).data()
        except:
            rows, cols, values = lhs.data()
        lhsA = sps.csr_matrix((values, cols, rows)).tocoo()

        # slice sparse matrix and solve linear problem
        lhsA = coo_submatrix_pull(lhsA, lDG_dofs, lDG_dofs)
        lx = spsolve(lhsA, rhs.array()[lDG_dofs])
        # tau_local.vector()[lDG_dofs] = lx

        # # DEBUG ---
        # tau_lobal = Function(DG)
        # tau_lobal.vector()[lDG_dofs] = lx
        # res = jump(tau_lobal + tildesigma * phi, n)
        # res = assemble(res**2 * dS(1), form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
        # print "res local ", i, res
        # # --- DEBUG

        # add up local fluxes
        tau_global.vector()[lDG_dofs] += lx

#        tau_global.vector()[lDG_dofs] += project(-tildesigma*phi,VV2).vector()[lDG_dofs]

    # # DEBUG ---
    # sigma_test = tau_global + tildesigma
    # BDM = FunctionSpace(mesh, "BDM", degree)
    # s1 = project(sigma_test, BDM)
    # s1 = project(s1, DG)
    # s1err = s1 - sigma_test
    # print "s1err:", sqrt(assemble(inner(s1err,s1err) * (dx(0)+dx(1)), form_compiler_parameters={'quadrature_degree': assemble_quad_degree}))
    # # DEBUG ---

    # restore linear algebra backend
    parameters.linear_algebra_backend = backend_backup

    # evaluate estimator
    sigmadist = sigma - tildesigma
    eq_est = assemble( inner(tau_global + sigmadist, tau_global + sigmadist) / kappa * dg0 * (dx(0)+dx(1)),
                                                    form_compiler_parameters={'quadrature_degree': assemble_quad_degree} )

    # reorder according to cell ids
    eq_est = eq_est[DG0_dofs.values()]
    global_est2 = np.sum(eq_est)
    return np.sqrt(eq_est), global_est2, tau_global, tildesigma, Qf


def eq_patch_assemblyBDM(V, eq_type, sigma_eq, f, kappa, epsilon, assemble_quad_degree=15, check_res=False):
    # setup mesh
    V_dm = V.dofmap()
    degree = V.ufl_element().degree() + eq_type
    mesh = V.mesh()
    mesh.init()
    # mesh data
    h = CellSize(mesh)
    n = FacetNormal(mesh)
    # DG0 localisation
    DG0 = FunctionSpace(mesh, 'DG', 0)
    DG0_dofs = dict([(c.index(), DG0.dofmap().cell_dofs(c.index())[0]) for c in cells(mesh)])
    dg0 = TestFunction(DG0)

    # use Eigen/uBLAS backend for conversion to scipy sparse matrices
    backend_backup = parameters.linear_algebra_backend
    try:
        parameters.linear_algebra_backend = "Eigen"
    except:
        parameters.linear_algebra_backend = "uBLAS"

    print "========>>> solving %i patch problems..." % mesh.num_vertices()

    # setup global equilibrated flux vector
    print "############### using BDM of degree %i ##################" %degree
    BDM = FunctionSpace(mesh, "BDM", degree)
    BDM_dofmap = BDM.dofmap()
    DGm1 = FunctionSpace(mesh, "DG", degree-1)
    W = BDM * DGm1
    W_dofmap = W.dofmap()
    w = Function(W)

    # define form functions
    sigma, u = TrialFunctions(W)
    tau, v = TestFunctions(W)

    # define global tau
    tau_global = Function(BDM)
    tau_global.vector()[:] = 0.0

    # prepare grouped assembly
    groups, phis, xis = get_groups_weights(mesh, single_patches=False)

    print "==== generated %i groups of %i patches ====" % (len(groups), mesh.num_vertices())

    parameters.linear_algebra_backend = backend_backup
    try:
        parameters.linear_algebra_backend = "Eigen"
    except:
        parameters.linear_algebra_backend = "uBLAS"

    # iterate groups
    group_cf = CellFunction('size_t', mesh)
    group_ff = FacetFunction('size_t', mesh)
    for i, G in enumerate(zip(groups, phis, xis)):
        g, phi, xi = G
        print "************* PATCH GROUP %i of %i *************" %(i,len(groups))

        # extract cell and boundary facet ids for group
        group_cids, group_inner_fids, group_boundary_fids = g[1][0], g[1][1], g[1][2]

        # create patch group measures
        group_cf.set_all(0)
        group_ff.set_all(0)
        # group patch cells
        for i in group_cids:
            group_cf[int(i)] = 1
        # group patch inner facets
        for i in group_inner_fids:
            group_ff[int(i)] = 1
        # group patch boundary facets
        for i in group_boundary_fids:
            group_ff[int(i)] = 2
        dx = Measure('dx')[group_cf]
        dS = Measure('dS')[group_ff]

        # define forms
        alpha = Constant(1/epsilon) * (h * h)
        pf = project(phi * f - inner(nabla_grad(phi), sigma_eq), DGm1)
        a_eq = xi * (1 / kappa) * inner(sigma, tau) * dx(1) + xi * div(tau) * u * dx(1) + xi * div(sigma) * v * dx(1) +\
                alpha('+') * inner(avg(sigma), n('+')) * inner(avg(tau), n('+')) * dS(2)
        L_eq = xi * (1 / kappa) * phi * inner(sigma_eq, tau) * dx(1) - xi * pf * v * dx(1)

        # assemble forms
        lhs = assemble(a_eq, keep_diagonal=True, form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
        lhs.ident_zeros()
        rhs = assemble(L_eq)

        # convert DOLFIN representation to scipy sparse arrays
        try:
            rows, cols, values = as_backend_type(lhs).data()
        except:
            rows, cols, values = lhs.data()
        lhsA = sps.csr_matrix((values, cols, rows)).tocoo()

        # determine local dofs
        lW_cell_dofs = dict([(cid, W_dofmap.cell_dofs(int(cid))) for cid in group_cids])
        lW_dofs = [cd.tolist() for cd in lW_cell_dofs.values()]
        lW_dofs = list(set(iter.chain(*lW_dofs)))
        lBDM_cell_dofs = dict([(cid, BDM_dofmap.cell_dofs(int(cid))) for cid in group_cids])
        lBDM_dofs = [cd.tolist() for cd in lBDM_cell_dofs.values()]
        lBDM_dofs = list(set(iter.chain(*lBDM_dofs)))

        # slice sparse matrix and solve linear problem
        lhsA = coo_submatrix_pull(lhsA, lW_dofs, lW_dofs)
        lx = spsolve(lhsA, rhs.array()[lW_dofs])

        # separate BDM dofs from mixed space function
        vw = np.zeros(W.dim())
        vw[lW_dofs] = lx
        w.vector().set_local(vw)
        w_sigma, _ = w.split(True)

        # add up local fluxes
        tau_global.vector()[lBDM_dofs] += w_sigma.vector().array()[lBDM_dofs]

    # restore linear algebra backend
    parameters.linear_algebra_backend = backend_backup

    # evaluate estimator
    eq_est = assemble( inner(tau_global - sigma_eq, tau_global - sigma_eq) / kappa * dg0 * (dx(0) + dx(1)),
                                                    form_compiler_parameters={'quadrature_degree': assemble_quad_degree} )

    # reorder according to cell ids
    eq_est = eq_est[DG0_dofs.values()]
    try:
        global_est2 = np.sum(eq_est)
    except:
        global_est2 = np.sum(eq_est.array())
    return np.sqrt(eq_est), global_est2, tau_global


def global_mixed_flux(sigma_eq, f_eq, kappa, V, mixed_type, assemble_quad_degree=15):
        # get function space data
        mesh = V.mesh()
        degree = V.ufl_element().degree()

        # create function spaces
        DG0 = FunctionSpace(mesh, 'DG', 0)
        DG0_dofs = dict([(c.index(),DG0.dofmap().cell_dofs(c.index())[0]) for c in cells(mesh)])
        dg0 = TestFunction(DG0)
        RT = FunctionSpace(mesh, mixed_type, degree)
        DGp = FunctionSpace(mesh, 'DG', degree - 1)
        W = RT * DGp

        # create trial and test functions
        (sigma, u) = TrialFunctions(W)
        (tau, v) = TestFunctions(W)

        # define variational form
        a_eq = (dot(sigma, tau) / kappa + div(tau) * u + div(sigma) * v) * dx
        L_eq = (dot(sigma_eq, tau) / kappa - f_eq * v) * dx
        # L_eq = - f_eq * v * dx

        # compute solution
        w_eq = Function(W)
        solve(a_eq == L_eq, w_eq, form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
        (sigma_mixed, u_mixed) = w_eq.split()

        # # DEBUG ===
        # mA = assemble(a_eq, form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
        # mb = assemble(L_eq)
        # print "MMMMMMM mixed residual", np.linalg.norm(mb.array() - np.dot(mA.array(), w_eq.vector().array()))
        # # DEBUG ===

        # evaluate estimator
        eq_est = assemble( inner( (sigma_eq - sigma_mixed) / kappa, sigma_eq - sigma_mixed) * dg0 * dx,
                                                    form_compiler_parameters={'quadrature_degree': assemble_quad_degree} )

        # reorder according to cell ids
        eq_est = eq_est[DG0_dofs.values()]
        global_est2 = np.sum(eq_est)
        # global_est = np.sqrt(global_est2)

        # res = div(sigma_mixed) + f_eq
        # res = assemble(inner(res, res) * dx, form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
        # print "RRRRRRRRRRRR res", res

        return np.sqrt(eq_est), global_est2, sigma_mixed, u_mixed
