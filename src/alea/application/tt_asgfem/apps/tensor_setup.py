from __future__ import division
import numpy as np
import scipy.sparse as sps

from alea.linalg.cptensor import CPTensor
from alea.linalg.operator import MultiplicationOperator
from alea.linalg.tensor_basis import TensorBasis
from alea.linalg.tensor_operator import TensorOperator
from alea.linalg.scipy_operator import ScipyOperator, ScipySolveOperator
from alea.linalg.basis import CanonicalBasis
from alea.polyquad.structure_coefficients import evaluate_triples
from alea.math_utils.multiindex_set import MultiindexSet
from alea.fem.fenics.fenics_basis import FEniCSBasis
import alea.stochastics.random_variable as rvs
import logging

# retrieve logger
logger = logging.getLogger(__name__)


def construct_deterministic_sgfem_operators(pde, coeff, M, mesh, degree):
    # use uBLAS backend for conversion to scipy sparse matrices
    from dolfin import parameters
    laback = parameters.linear_algebra_backend
    try:
        parameters.linear_algebra_backend = "Eigen"
    except:
        parameters.linear_algebra_backend = "uBLAS"

    fs = pde.function_space(mesh, degree=degree)
    basis = FEniCSBasis(fs)
    am_f = [coeff.mean_func]
    am_f.extend([coeff[m][0] for m in range(M)])
    # assemble stiffness matrices
    if True:
        K = [pde.assemble_operator(basis=basis, coeff=f).as_scipy_operator() for f in am_f]
    else:
        K = [pde.assemble_operator(basis=basis, coeff=f, withDirichletBC=False).as_scipy_operator() for f in am_f]
        K[0] = pde.assemble_operator(basis=basis, coeff=f, withDirichletBC=True).as_scipy_operator()
    # assemble rhs
    B0 = pde.assemble_rhs(basis=basis, coeff=am_f[0])
    B = np.zeros((len(B0),M))
    B[:,0] = B0

    # restore backend
    parameters.linear_algebra_backend = laback
    return K, B, basis


def construct_stochastic_sgfem_operators(M, p, type='L', validate=False):
    ''' construct stochastic gpc matrix of given degree.
        type: H Hermite, L Legendre '''
    def eval_quad(m, mui, muj, P):
        v = 1
        for k, mij in enumerate(zip(mui,muj)):
            mi, mj = mij
            pk = 1 if m != k else P[1]
            pi = P[mi]
            pj = P[mj]
            v *= rv.integrate(pk*pi*pj)     # NOTE: this is unstable for high degrees
        return v
    # setup polynomials
    if type == 'L':
        rv = rvs.UniformRV()
    elif type == 'H':
        rv = rvs.NormalRV()
    else:
        raise TypeError("unsupported type")
    # setup multi-index set
    I = MultiindexSet.createCompleteOrderSet(M, p).arr
    N = I.shape[0]
    # print "STOCHASTIC MI shape", I.shape, M, p
    # print I

    # evaluate triples according to random variable distribution
    polysys = rv.orth_polys
    if not validate:
        L = evaluate_triples(polysys, I, I)
    else:
        # NOTE: the following is extremely slow and just for validation purposes
        # setup polynomials
        P = [np.poly1d(polysys.get_coefficients(i)[::-1]) for i in range(p+1)]
        # setup sparse matrices
        L = [sps.dok_matrix((N, N)) for _ in range(M+1)]
        # evaluate triple product by quadrature
        for m in range(M+1):
            for i, mui in enumerate(I):
                for j, muj in enumerate(I):
                    v = eval_quad(m, mui, muj, P)
                    if abs(v) >= 1e-15:
                        L[m][i,j] = v
        L = [ScipyOperator(l, domain=CanonicalBasis(N), codomain=CanonicalBasis(N)) for l in L]
    # setup rhs B
    B = np.zeros((N,M))
    B[0,:] = 1
    return L, B


def generate_matrix(n):
    A = np.random.rand(n, n) - 0.5
    return sps.csr_matrix(np.dot(A, A.T))


def construct_operator(K, D):
    # prepare "deterministic" matrices
    assert len(K) == len(D)
    n1, n2, R = K[0].matrix.shape[0], D[0].matrix.shape[0], len(K)
    basis1 = CanonicalBasis(n1)
    basis2 = CanonicalBasis(n2)
    basis = TensorBasis([basis1, basis2])
    return TensorOperator(K, D, domain=basis, codomain=basis)


def construct_cptensor(B1, B2):
    # prepare vector
    n1, n2, R = B1.shape[0], B2[0].shape[0], B1.shape[1]
    basis1 = CanonicalBasis(n1)
    basis2 = CanonicalBasis(n2)
    basis = TensorBasis([basis1, basis2])
    return CPTensor([B1, B2], basis)


def construct_preconditioner(A, identity=False):
    basis = A.domain
    basis1 = basis[0]
    basis2 = basis[1]
    if identity:
        P = TensorOperator(
                [MultiplicationOperator(1, domain=basis1)],
                [MultiplicationOperator(1, domain=basis2)],
                basis)
    else:
        P = TensorOperator(
                [ScipySolveOperator(A.A[0].matrix, domain=basis1, codomain=basis1)],
                [MultiplicationOperator(1, domain=basis2)],
                basis)
    return P


def rank_truncate(R_max):
    """Create a truncation function that truncates by rank"""
    def do_truncate(X):
        return X.truncate(R_max)
    return do_truncate