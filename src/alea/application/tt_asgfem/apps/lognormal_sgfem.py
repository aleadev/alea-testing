from __future__ import division
import argparse
import logging
import logging.config
import sys
from dolfin import cells, FunctionSpace, plot, interactive, Function, refine
import numpy as np
import scipy.sparse as sps
import alea.application.tt_asgfem.tensorsolver.tt_sparse_matrix as ttsmat
import tt
from alea.application.egsz.affine_field import AffineField
from alea.application.egsz.sample_domains import SampleDomain
from alea.application.egsz.sample_problems2 import SampleProblem
from alea.utils.tictoc import TicToc
from alea.application.tt_asgfem.apps.sgfem_als_util import (compute_FE_matrices, export_solution,
                                                            compute_FE_matrix_coeff, compute_test_FE_matrix)
from alea.application.tt_asgfem.tensorsolver.tt_als import ttALS
from alea.application.tt_asgfem.tensorsolver.tt_lognormal import (generate_lognormal_tt, sample_lognormal_tt,
                                                                  generate_operator_tt, extract_mean,
                                                                  get_coeff_upper_bound, tt_cont_coeff,
                                                                  sample_cont_coeff, fully_disc_first_core)
from alea.application.tt_asgfem.tensorsolver.tt_param_pde import ttRightHalfdot
from alea.application.tt_asgfem.tensorsolver.tt_sgfem_util import (tt_dofs, increase_tt_rank,
                                                                   ttRandomDeterministicRankOne)
from alea.application.tt_asgfem.tensorsolver.tt_util import reshape, ttNormalize


def setup_logger():
    # configure logger
    logger = logging.getLogger('lognormal_sgfem.log')
    sh = logging.StreamHandler(sys.stdout)
    sh.setLevel(logging.INFO)
    als_logger = logging.getLogger('tensorsolver.tt_als')
    als_logger.setLevel(logging.INFO)
    als_logger.addHandler(sh)
    return logger, als_logger

# ###############
# A problem setup
# ###############

parser = argparse.ArgumentParser()
parser.add_argument("-ct", "--coeff-type", type=str, default="EF-square-cos",
                    help="coefficient type")
parser.add_argument("-M", "--M-terms", type=int, default=10,
                    help="terms in realisation")
parser.add_argument("-at", "--amptype", type=str, default="decay-inf",
                    help="coefficient decay type")
parser.add_argument("-de", "--decay-exponent", type=float, default=2.0,
                    help="decay exponent")
parser.add_argument("-g", "--gamma", type=float, default=0.9,
                    help="gamma")
parser.add_argument("-fk", "--freq-skip", type=int, default=0,
                    help="frequency skip")
parser.add_argument("-fs", "--freq-scale", type=float, default=1.0,
                    help="frequency scaling")
parser.add_argument("-s", "--scale", type=float, default=1.0,
                    help="scaling")
parser.add_argument("-fd", "--fem-degree", type=int, default=1,
                    help="fem degree (default 1)")
parser.add_argument("-dom", "--domain", type=int, choices=[0,1], default=0,
                    help="domain (default 0=square, 1=L-shaped)")
parser.add_argument("-mref", "--mesh-refine", type=int, default=0,
                    help="initial refinement (default 0)")
parser.add_argument("-md", "--mesh-dofs", type=int, default=-1,
                    help="initial mesh dofs, overrides mesh-refine (default -1 = ignore)")
parser.add_argument("-th", "--theta", type=int, default=.5,
                    help="weight of gauss measure (between 0 and 1)")
parser.add_argument("-rh", "--rho", type=int, default=1,
                    help="weight of gauss measure (greater than 0)")
parser.add_argument("-hdeg", "--hermite-degree", type=int, default=8,
                    help="degree of hermite polynomials for approximation of coefficient")
parser.add_argument("-gpc", "--gpc-degree", type=int, default=3,
                    help="degree of hermite polynomials for approximation of operator")
parser.add_argument("-mr", "--max-rank", type=int, default=60,
                    help="maximum rank for operator")
parser.add_argument("-sr", "--sol-rank", type=int, default=5,
                    help="rank of the solution")
parser.add_argument("-alt", "--als-tolerance", type=float, default=1e-12,
                    help="ALS solution tolerance")
parser.add_argument("-ai", "--als-iterations", type=int, default=1000,
                    help="ALS max iterations")
parser.add_argument("-bn", "--basename", type=str, default="",
                    help="export basename")
parser.add_argument("-mmc", "--max-mesh-cells", type=int, default=50000, dest="mmc",
                    help="maximal mesh cells for export (default 50000)")
args = parser.parse_args()
print "========== PARAMETERS:", vars(args)

# setup logger
logger, als_logger = setup_logger()

# set some flags
save_fn = None
WITH_EXPORT_SOLUTION = args.basename != ""

# set export name of experiment
EXPERIMENT = args.basename

# set discretisation parameters
M, femdegree, gpcdegree, polysys = args.M_terms, args.fem_degree, args.gpc_degree, 'H'
decayexp, gamma, rvtype = args.decay_exponent, args.gamma, 'normal'
amptype = args.amptype
assert femdegree == 1       # currently only affine linear elements
assert args.domain == 0     # currently only square mesh

# setup domain and meshes
domain, mesh_refine, mesh_dofs = ["square", "lshape"][args.domain], args.mesh_refine, args.mesh_dofs
if mesh_dofs > 0:
    mesh_refine = 0
mesh, boundaries, dim = SampleDomain.setupDomain(domain, initial_mesh_N=10)
mesh = SampleProblem.setupMesh(mesh, num_refine=mesh_refine)
while mesh_dofs > 0 and FunctionSpace(mesh, 'CG', femdegree).dim() < mesh_dofs:
    # uniformly refine mesh
    mesh = refine(mesh)
logger.info("initial mesh has %i dofs with p%i FEM" % (FunctionSpace(mesh, 'CG', femdegree).dim(), femdegree))

# get cell midpoints
mp = np.zeros((mesh.num_cells(), 2))
for ci, c in enumerate(cells(mesh)):
    mpi = c.midpoint()
    mp[ci] = mpi.x(), mpi.y()

    
# #######################
# B construct coefficient
# #######################

# create field
af = AffineField(args.coeff_type, amptype=amptype, decayexp=decayexp, gamma=gamma, freqscale=args.freq_scale,
                 freqskip=args.freq_skip, scale=args.scale)
coeff_field = af.coeff_field
theta, rho = args.theta, args.rho
hdeg = args.hermite_degree
hdegs = M * [hdeg]
maxrank = args.max_rank
ranks = [1] + M * [maxrank] + [1]

###############################################
######## Full Discretization Method ###########
###############################################

TicToc.clear()
with TicToc(key="**** CreateFullyDiscreteCoefficient ****", active=True, do_print=True):
    # evaluate M basis functions at cell midpoints
    B = af.evaluate_basis(mp, M)
    # TODO: better bxmax
    bxmax = get_coeff_upper_bound(B)
    # print "B", B

    U = generate_lognormal_tt(B, hdeg, ranks, bxmax, theta, rho)
    #U = U.round(1e-14)
    #ranks = U.r
    print "U", U

###############################################
######## Semi Discretized Method ##############
###############################################

with TicToc(key="**** CreateSemiDiscreteCoefficient ****", active=True, do_print=True):
    coeff_cores = tt_cont_coeff(af, mesh, M, ranks, hdegs, bxmax, theta, rho, fe_h=1e-2, fe_p=1)


###############################################
######## Compute Sample Errors ################
###############################################

def evaluate_realisation_error(mp, y, U):
    V1 = af(mp, y)                  # exact values at mp
    V1 = np.exp(V1)
    V2 = sample_lognormal_tt(U, y, bxmax, theta, rho)  # TT values at mp
    V3 = sample_cont_coeff(coeff_cores, af, mp, y, ranks, hdegs, bxmax, theta, rho)
    return np.linalg.norm(V1-V2),np.linalg.norm(V1-V3),np.linalg.norm(V2-V3)

N = 10
coeff_err1 = 0
coeff_err2 = 0
coeff_err3 = 0
for _ in range(N):
    err1,err2,err3= evaluate_realisation_error(mp, af.sample_rvs(M), U)
    coeff_err1 += err1**2
    coeff_err2 += err2**2
    coeff_err3 += err3**2
coeff_err1 = np.sqrt(coeff_err1)
coeff_err2 = np.sqrt(coeff_err2)
coeff_err3 = np.sqrt(coeff_err3)
print "MC sample error", coeff_err1, coeff_err2, coeff_err3

# new test
mesh = refine(mesh)
# get cell midpoints
mp = np.zeros((mesh.num_cells(), 2))
for ci, c in enumerate(cells(mesh)):
    mpi = c.midpoint()
    mp[ci] = mpi.x(), mpi.y()
y = af.sample_rvs(M)
V1 = af(mp, y)
V1 = np.exp(V1)
V3 = sample_cont_coeff(coeff_cores, af, mp, y, ranks, hdegs, bxmax, theta, rho)
print "semi-discrete error", np.linalg.norm(V1-V3)

sigma = np.exp(theta*rho*bxmax)
print "sigma", sigma


###############################################
######## Construct Lognormal Operator #########
###############################################

with TicToc(key="**** ConstructOperator ****", active=True, do_print=True):
    ############### OLD ###########################################
    # gpcdegrees = M * [gpcdegree+1]
    # opcores = generate_operator_tt(U, gpcdegrees, M)
    # # for i in range(M):
    # #     print "opcores", i, opcores[i+1].shape
    #
    # # get first component of coefficient tensor representation
    # cores = tt.tensor.to_list(U)
    # first_comp = reshape(cores[0], (B.shape[0], ranks[1]))
    ###############################################################

    # create first core at current points
    opcores = generate_operator_tt(coeff_cores,ranks,hdegs,M)
    core0 = fully_disc_first_core(coeff_cores,af,mp,ranks,hdegs,bxmax,theta,rho)
    coeff_cores.insert(0,core0)
    first_comp = np.reshape(core0,[len(mp), ranks[1]])

    # compute FE matrices for piecewise constant coefficients
    A0, bc_dofs, BB, CG = compute_FE_matrices(first_comp, mesh)
    # print len(A0), A0[0].shape

    D = A0[0].shape[0]
    opcores[0] = []
    for r0, A in enumerate(A0):
        opcores[0].append(A)
        # print "A0", A.toarray()
        # print "midpoints", first_comp[:,r0]
        # print "eigsA0", np.linalg.eig(A.toarray())

    # generate lognormal operator tensor
    A = ttsmat.smatrix(opcores)

    bcopcores = (M+1)*[[]]
    # bcdense = np.diag(adiag)
    bcdense = np.zeros([D,D])
    bcdense[bc_dofs,bc_dofs] = 1
    bcopcores[0] = []
    bcopcores[0].append(sps.csr_matrix(bcdense))
    for i in range(M):
        bcopcores[i+1] = reshape(np.eye(opcores[i+1].shape[1],opcores[i+1].shape[2]),[1,opcores[i+1].shape[1],opcores[i+1].shape[2],1])
    BCOP = ttsmat.smatrix(bcopcores)

    # Aop = A.full()
    # Aop = reshape(Aop,[A.n[0],(gpcdegree+1),(gpcdegree+1),A.n[0],(gpcdegree+1),(gpcdegree+1)])
    A = A + BCOP
    A = A.round(1e-14) # Important: Don't cut ranks here!
    print "**** Operator ****\n", A
    print "******************"


#############################################
####### Smaller Problem Tests ###############
#############################################

# # -Eigenvalue-Tester-For-Small-Problems-----------------------------
# Amat = A.full()
#
# eigs1, vecs = np.linalg.eig(Amat)
# for s in range(eigs1.shape[0]):
#     print eigs1[s]
# # ------------------------------------------------------------------

# negvec = vecs[:,0]
# E = reshape(negvec,A.n)
# print E.shape
# E = tt.tensor(E, 1E-14)
# print 'Testeig:', tt.dot(E,(tt.matvec(A,E)))
#
# # U0 = np.zeros([cores[0].shape[1],gpcdegree+1,gpcdegree+1])
# # for i in range(cores[0].shape[1]):
# #     for nu1 in range(gpcdegree+1):
# #         for nu2 in range(gpcdegree+1):
# #             U0[i,nu1,nu2] = sum([cores[0][0,i,k]*opcores[1][k,nu1,nu2,0] for k in range(cores[0].shape[2])])
# Afull = compute_test_FE_matrix(af[0][1],af[0][0][0],af[1][1],af[1][0][0],af[2][1],af[2][0][0], Aop, gpcdegree, mesh)
#
# # Afull = reshape(Afull,[A.n[0]*(gpcdegree+1),A.n[0]*(gpcdegree+1)])
# # eigs3 = np.linalg.eig(Afull)[0]
# # print "eigs3:", eigs3
#
# # Aerr = Afull - Amat
# # np.set_printoptions(threshold=np.nan)
# # print "Aerr:", Aerr
#
# # Atest = reshape(Atest,[A.n[0]*(gpcdegree+1),A.n[0]*(gpcdegree+1)])
# # eigs4 = np.linalg.eig(Atest)[0]
# # print "eigs4:", eigs4
#
# # Atesterr = Atest - Amat
# # print "Atesterr:", Atesterr
# # np.set_printoptions(threshold=4)
#
# # np.set_printoptions(threshold=np.nan, precision=2, linewidth=np.nan, suppress=True)
# # print Afull
# # np.set_printoptions(threshold=1000, precision=8, linewidth=75)
# Afull = reshape(Afull,[A.n[0],(gpcdegree+1),(gpcdegree+1),A.n[0],(gpcdegree+1),(gpcdegree+1)])
# Afull = np.transpose(Afull,axes=(0,3,1,4,2,5))
# Afull = reshape(Afull,[A.n[0]*A.n[0],(gpcdegree+1)*(gpcdegree+1),(gpcdegree+1)*(gpcdegree+1)])
# Afull = tt.tensor(Afull)
# Afull = tt.matrix(Afull)
# afullcores = tt.matrix.to_list(Afull)
# core = []
# for r0 in range(afullcores[0].shape[3]):
#     core.append(sps.csr_matrix(afullcores[0][0,:,:,r0]))
# afullcores[0] = core
# Afull = ttsmat.smatrix(afullcores)
#
# Afull = Afull + BCOP
# Afull = Afull.round(1e-14)
# print "AfullOp:", Afull
# Opdiff = A - Afull
# print "Opdiff:", Opdiff.norm()
# Afullmat = Afull.full()
# # A = Afull
#
# # np.set_printoptions(threshold=np.nan, precision=2, linewidth=np.nan, suppress=True)
# # # print Atest - Amat
# # np.set_printoptions(threshold=1000, precision=8, linewidth=75)
#
# print "FullError:", np.linalg.norm(Afullmat - Amat)
# # print "TestError:", np.linalg.norm(Atest - Amat)
# # print "TestError2:", np.linalg.norm(Atest - Afullmat)
# # ------------------------------------------------------------------
#
# assert False



# #################
# D solve TT system
# #################
with TicToc(key="**** ALS ****", active=True, do_print=True):
    n = A.n
    d = A.d
    solrank = args.sol_rank
    # -Right-Side--------------------------------------------
    BB = reshape(BB,[1,n[0],1])
    F = [BB] + M * [[]]
    for i in range(M):
        F[i+1] = reshape(np.eye(n[i+1],1), [1,n[i+1],1])
    F = tt.tensor.from_list(F)
    print "F", F
    # -Start-Vector------------------------------------------
    # r = [1] + M * [solrank] + [1]
    # V = tt.rand(n, d, r)
    # V = (1 / V.norm()) * V
    W = ttRandomDeterministicRankOne(n)

    # -Preconditioner----------------------------------------
    # P = compute_FE_matrix_coeff(B,mesh,exp_a=True)
    # Pvec = extract_mean(U) Doesn't work with new version
    # P = compute_FE_matrix_coeff(Pvec,mesh,exp_a=False)
    P = np.eye(n[0],n[0])
    for i in range(solrank):
        # -ALS---------------------------------------------------
        W, _ = ttALS(A, F, W, conv=args.als_tolerance, maxit=args.als_iterations, P=P)

        from dolfin import FunctionSpace
        from alea.utils.plothelper import PlotHelper

        PH = PlotHelper()
        samples = [0, 0]
        retval = sample_lognormal_tt(F, samples, bxmax, theta=theta, rho=rho)
        fun = Function(FunctionSpace(mesh, 'CG', femdegree))
        fun.vector()[:] = retval
        PH["rhs"].plot(fun)

        retval = sample_lognormal_tt(W, samples, bxmax, theta=theta, rho=rho)
        fun = Function(FunctionSpace(mesh, 'CG', femdegree))
        fun.vector()[:] = retval
        PH["solution"].plot(fun, interactive=True)
    #    R = ttNormalize(A.matvec(W) - F)
    #    print "residual:", R.norm()
        if i < solrank-1:
            W, rank = increase_tt_rank(W, None, None, None)
        print "W", W

    print "coeff_err =", coeff_err2

#############################
### plot solution samples ###
#############################

if False:
    # generate SxM samples
    S = 3
    samples = [np.array(af.sample_rvs(M)) for _ in range(S)]

    # evaluate stochastic polynomials for sample
    poly = af.rvs[0].orth_polys
    Pxs = [np.array(poly.eval(hdeg, sample, all_degrees=True)) for sample in samples]

    # sum up realisation vector
    # produce linear combination of fem_basis corresponding to Px
    # construct FEM functions
    F = []
    for Px in Pxs:
        sample_sol = ttRightHalfdot(W, Px)
        f_sample_sol = Function(CG)
        f_sample_sol.vector()[:] = sample_sol
        F.append(f_sample_sol)
        plot(F[-1])
    D1 = Function(CG)
    D1.vector()[:] = F[0].vector() - F[-1].vector()
    plot(D1)
    interactive()


# ######################
# E save solution tensor
# ######################

if WITH_EXPORT_SOLUTION:
    # determine number dofs
    dofs = tt_dofs(W)
    S = {"V": W, "CG": CG, "DOFS": dofs, "nr_active_ydim": M, "sigma": sigma, "coeff_err_MC": coeff_err2}
    refinements, gpcps = 0, [hdegs]
    save_fn = export_solution([S], args.als_iterations, refinements, polysys, femdegree, gpcps,
                                    domain, decayexp, gamma, rvtype, basename=EXPERIMENT, max_mesh_cells=args.mmc,
                                    delete_file=save_fn, lognormal=True)
