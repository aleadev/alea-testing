from __future__ import division
import cPickle as pickle
import argparse

# sampling parameters
parser = argparse.ArgumentParser()
parser.add_argument("-sf", "--solution-file", type=str, required=True,
                    help="solution file")
args = parser.parse_args()
print "========== PARAMETERS:", vars(args)

SOLUTION_FILE = args.solution_file


def load_solution(solution_file):
    print "loading solution from" + solution_file
    with open(solution_file, 'rb') as infile:
        SOL, INF = pickle.load(infile)
        polysys, femdegree, gpcps = INF["polysys"], INF["femdegree"], INF["gpcps"]

    for i, S in enumerate(SOL):
        V = S["V"]
        # # restore FunctionSpace and MultiindexSet
        # mesh = Mesh(solution_file.replace('.dat','-mesh-%i.xml' % i))
        # S["CG"] = FunctionSpace(mesh, V.CG[0], V.CG[1])
        # V.V = S["CG"]
        V.polysys = polysys
        V.gpcps = gpcps[i]

    return SOL, INF

# load discrete solutions
SOL, INF = load_solution(SOLUTION_FILE)

# iterate MC calculations
strlen = len(str(SOL[-1]["V"].gpcps))
print "===" * strlen
print "TTdofs\tdofs\txdofs\txdofs2\tydofs\tydofs2\tgpcp", " " * strlen, "\tranks"
with file(SOLUTION_FILE.replace('.dat', '.info'), 'w') as f:
    f.write("TTdofs\tdofs\txdofs\txdofs2\tydofs\tydofs2")
    for i, S in enumerate(SOL):
        V = S["V"]
        gpcps = V.gpcps
        import operator
        ydofs = reduce(operator.mul, [i+1 for i in gpcps], 1)
        rr, nn = V.r, V.n
        xdofs = rr[0]*nn[0]*rr[1] - rr[1]*rr[1]
        xdofs2 = nn[0]
        ydofs2 = S["DOFS"] - xdofs
        alldofs = xdofs * ydofs
        print S["DOFS"], "\t", alldofs, "\t", xdofs, "\t", xdofs2, "\t", ydofs, "\t", ydofs2, "\t", gpcps, " " * (strlen - len(str(gpcps))), "\t", S["V"].r
        f.write("\n" + "\t".join([str(i) for i in [S["DOFS"], alldofs, xdofs, xdofs2, ydofs, ydofs2]]))
    f.write("\n")
    print "coordinates{", " ".join(["(%i,%i)" % (i+1,p) for i, p in enumerate(gpcps)]), "}"
