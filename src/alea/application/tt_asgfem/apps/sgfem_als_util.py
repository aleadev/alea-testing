from __future__ import division
import os, glob
import warnings
import cPickle as pickle
import logging
from dolfin import File, FunctionSpace, Function, TrialFunction, TestFunction, Mesh, assemble, assemble_system, dx, Expression, Constant, DirichletBC, nabla_grad, inner, cells, as_backend_type, project
import numpy as np
import scipy.sparse as sps
from alea.utils.plothelper import plot_mpl
from alea.utils.tictoc import TicToc
from alea.linalg.tensor.extended_tt import leg_base_change_constant

try:
    WITH_MPL = True
    import matplotlib.pyplot as plt
except:
    WITH_MPL = False

# module logger
logger = logging.getLogger(__name__)

def compute_FE_matrix_coeff(u0, mesh, degree=1, exp_a=True):
    '''compute FEM matrix with piecewise constant exp(a) coefficient'''
    def u0_boundary(x, on_boundary):
        return on_boundary

    # setup FE functions
    V = FunctionSpace(mesh, 'CG', degree)
    u, v = TrialFunction(V), TestFunction(V)
    bc = DirichletBC(V, Constant(0), u0_boundary)
    f = Expression('1', degree=1)

    # setup piecewise constant coefficient
    DG0 = FunctionSpace(mesh, 'DG', degree-1)
    dm = DG0.dofmap()
    c2d = [dm.cell_dofs(i)[0] for i in range(mesh.num_cells())]
    # c0 = Function(DG0)
    c0 = Function(V)
    # get cell midpoints
    # mp = np.zeros((mesh.num_cells(), 2))
    # for ci, c in enumerate(cells(mesh)):
    #     mpi = c.midpoint()
    #     mp[ci] = mpi.x(), mpi.y()
    # # evaluate and set coefficient
    # u0 = [a(p) for p in mp]
    # c0.vector()[:] = np.exp(u0)[c2d] if exp_a else u0[c2d]
    c0.vector()[:] = np.exp(u0) if exp_a else u0
    # use Eigen backend for conversion to scipy sparse matrices
    from dolfin import parameters
    laback = parameters.linear_algebra_backend
    parameters.linear_algebra_backend = "Eigen"

    # assemble matrix
    A0, B = assemble_system(c0 * inner(nabla_grad(u), nabla_grad(v)) * dx, f * v * dx, bc)
    # convert and store matrix
    rows, cols, values = as_backend_type(A0).data()
    A0 = sps.csr_matrix((values, cols, rows))

    # restore backend
    parameters.linear_algebra_backend = laback

    return A0

parallel_DG0 = None
parallel_CG = None
parallel_ip = None
parallel_bc_dofs = None
parallel_bc = None
parallel_B = None

def parallel_compute_FE_matrices(value_vec, withDirichletBC, k):

    _DG0 = parallel_DG0
    _ip = parallel_ip
    _bc_dofs = parallel_bc_dofs
    _bc = parallel_bc

    c0 = Function(_DG0)
    c0.vector()[:] = value_vec
    # print("assemble A")
    # assemble matrix
    with TicToc(key="      3.1.2.1 assemble matrix", active=True, do_print=False):
        A = assemble(c0 * _ip * dx)
    # print("convert to rows,...")
    # convert and store matrix
    with TicToc(key="      3.1.2.2 save data as backend type", active=True, do_print=False):
        rows, cols, values = as_backend_type(A).data()
    # print("convert to sps matrix")
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        with TicToc(key="      3.1.2.3 create sparse matrix from data", active=True, do_print=False):
            A00 = sps.csr_matrix((values, cols, rows))

    # modify boundary dof entries
    if withDirichletBC:
        with TicToc(key="      3.1.2.4 Apply BC", active=True, do_print=False):
            # delete rows and columns of bc dofs
            for i in _bc_dofs:
                J = cols[rows[i]:rows[i + 1]]
                I = [[i]] * len(J)
                A00[I, J] = 0
                A00[J, I] = 0
                # A00[i, i] = 1

    retval = dict()
    retval["A0"] = A00
    retval["k"] = k
    return retval


def parallel_compute_FE_matrices_alternative(value_vec, withDirichletBC, k):
    _DG0 = parallel_DG0
    _CG = parallel_CG
    _ip = parallel_ip
    _bc_dofs = parallel_bc_dofs
    _bc = parallel_bc
    _B = parallel_B

    # c0 = Function(_DG0)
    c1 = Function(_CG)
    c1.vector()[:] = value_vec
    c0 = project(c1, _DG0)
    # print("assemble A")
    # assemble matrix
    with TicToc(key="      3.1.2."
                    "1 assemble matrix", active=True, do_print=False):
        A = assemble(c0 * _ip * dx)
    # print("convert to rows,...")
    # convert and store matrix
    if withDirichletBC:
        with TicToc(key="      3.1.2.x apply bc", active=True, do_print=False):
            _bc.zero(A)
            # A.zero(np.array(_bc_dofs, dtype=np.intc))
            # A.apply('insert')
            _bc.zero_columns(A, _B)
    with TicToc(key="      3.1.2.2 save data as backend type", active=True, do_print=False):
        rows, cols, values = as_backend_type(A).data()

    # print("convert to sps matrix")
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        with TicToc(key="      3.1.2.3 create sparse matrix from data", active=True, do_print=False):
            A00 = sps.csr_matrix((values, cols, rows))
    retval = dict()
    retval["A0"] = A00
    retval["k"] = k
    return retval


def compute_FE_matrices(U0, mesh, degree=1, withDirichletBC=True, n_cpu=4):
    def u0_boundary(x, on_boundary):
        return on_boundary

    # setup FE functions
    V = FunctionSpace(mesh, 'CG', degree)
    u, v = TrialFunction(V), TestFunction(V)
    f = Expression('1', degree=1)
    bc = DirichletBC(V, Constant(0), u0_boundary)
    dof2val = bc.get_boundary_values()
    bc_dofs = dof2val.keys()

    # setup piecewise constant coefficient
    DG0 = FunctionSpace(mesh, 'DG', 0)
    # fun = Function(DG0)
    # print("fun {}".format(len(fun.vector()[:])))
    # dm = DG0.dofmap()
    # c2d = [dm.cell_dofs(i)[0] for i in range(mesh.num_cells())]
    # print("c2d {}".format(c2d))
    # c0 = Function(DG0)
    # cg = Function(V)

    # assemble linear form and apply bc
    B = assemble(f * v * dx)
    B_dummy = assemble(f * v * dx)
    if withDirichletBC:
        bc.apply(B)         # TODO: to be certain this maybe should be done explicitly as well

    # use Eigen backend for conversion to scipy sparse matrices
    from dolfin import parameters
    laback = parameters.linear_algebra_backend
    parameters.linear_algebra_backend = "Eigen"
    if True:
        # region Compute FE matrices parallel
        with TicToc(sec_key="ASGFEM-solve", key="3.1.1 compute FE matrices parallel", active=True, do_print=False):
            global parallel_DG0
            global parallel_CG
            global parallel_bc_dofs
            global parallel_ip
            global parallel_bc
            global parallel_B
            parallel_ip = inner(nabla_grad(u), nabla_grad(v))
            parallel_bc_dofs = bc_dofs
            parallel_DG0 = V
            parallel_CG = V
            parallel_bc = bc
            parallel_B = B_dummy

            # evaluate FEM matrices

            from joblib import delayed, Parallel
            # ##### Factor 40 slower
            # with TicToc(key="    3.1.1.x compute FE matrices parallel MARTIN", active=True, do_print=True):
            #     __A0 = list(Parallel(n_cpu)(delayed(parallel_compute_FE_matrices)(U0[:, k][c2d], withDirichletBC, k)
            #                                for k in range(U0.shape[1])))
            # ######
            with TicToc(sec_key="ASGFEM-solve", key="3.1.1.y compute FE matrices parallel ROBERT", active=True,
                        do_print=False):
                __A0_alternative = list(Parallel(n_jobs=n_cpu,
                                                 # backend="threading"
                                                 )
                                        (delayed(parallel_compute_FE_matrices_alternative)(U0[:, k], withDirichletBC,
                                                                                           k)
                                        for k in range(U0.shape[1])))

            with TicToc(sec_key="ASGFEM-solve", key="3.1.1.z FE matrices SORTING", active=True, do_print=False):
                # _A0 = [None] * U0.shape[1]
                _A0_alternative = [None] * U0.shape[1]
                for k in range(U0.shape[1]):
                    # for ind in range(len(__A0)):
                    #     if __A0[ind]["k"] == k:
                    #         _A0[k] = __A0[ind]["A0"]
                    #         break
                    for ind in range(len(_A0_alternative)):
                        if __A0_alternative[ind]["k"] == k:
                            _A0_alternative[k] = __A0_alternative[ind]["A0"]
                            break
        # for (A00, A00_alternative) in zip(_A0, _A0_alternative):
        #     print("diff= {}".format(np.linalg.norm(A00.todense()-A00_alternative.todense())))
        # endregion

    # region compute sparse matrix operator serial
    # if False:
    #     with TicToc(key="    3.1.2 compute FE matrices serial", active=True, do_print=True):
    #         A0 = []
    #         for k in range(U0.shape[1]):
    #             # set coefficient
    #             cg.vector()[:] = U0[:, k]
    #             c0 = project(cg, FunctionSpace(mesh, 'DG', 0))
    #             # assemble matrix
    #             A = assemble(c0 * inner(nabla_grad(u), nabla_grad(v)) * dx)
    #
    #             # convert and store matrix
    #             rows, cols, values = as_backend_type(A).data()
    #             A00 = sps.csr_matrix((values, cols, rows))
    #
    #             # modify boundary dof entries
    #             if withDirichletBC:
    #                 # delete rows and columns of bc dofs
    #                 for i in bc_dofs:
    #                     J = cols[rows[i]:rows[i+1]]
    #                     I = [[i]] * len(J)
    #                     A00[I, J] = 0
    #                     A00[J, I] = 0
    #
    #             # store operator matrix
    #             A0.append(A00)
    #     # for (A00, _A00) in zip(A0, _A0):
    #     #     print("diff in computation: {}".format(np.max(np.array(_A00.data) - np.array(A00.data))))
    # endregion

    A0 = _A0_alternative
    # region Check dimension of spaces and fem matrices
    if False:
        print("DGO space dim: {}".format(mesh.num_cells()))
        print("CG {} space dim: {}".format(degree, V.dim()))
        for lia in range(len(A0)):
            print("  A0[{}] shape = {}".format(lia, A0[lia].shape))
    # endregion

    # restore backend
    parameters.linear_algebra_backend = laback

    return A0, bc_dofs, B.array(), V


def export_solution(SOL, iterations, refinements, polysys, femdegree, gpcps, domain, decayexp, gamma, rvtype, basename="", max_mesh_cells=-1, delete_file=None, lognormal=False):
    # determine filename
    M = len(SOL[-1]["V"].r) - 1
    max_rank = max(SOL[-1]["V"].r)
    export_file = 'results/{0}-solution_M{1}_rank{2}_iter{3}_refine{4}_sys{5}_degD{6}_degS{7}_exp{8}_{9}'.format(basename, M, max_rank,
                                                                      iterations, refinements, polysys, femdegree, max(gpcps[-1]), decayexp, domain)
    export_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), export_file)

    CGS = []
    for i, S in enumerate(SOL):
        v, CG = S["V"], S["CG"]
        v.CG = (CG.ufl_element().family(), CG.ufl_element().degree())
        CGS += [CG]
        del S["CG"]         # has to be removed for pickling
        # inject additional solution data
        v.max_order = len(v.r) - 1
        v.max_rank = max(v.r)

        # export mesh of FunctionSpace
        mesh = CG.mesh()
        File(export_file + "-mesh-%i.xml" % i) << mesh

        # export mesh pdf
        try:
            if WITH_MPL and (max_mesh_cells == -1 or mesh.num_cells() <= max_mesh_cells):
                if i == 0:
                    plt.figure(1)
                    plt.gca().set_aspect('equal')
                    plt.gca().xaxis.set_ticklabels([])
                    plt.gca().yaxis.set_ticklabels([])
    #                plt.show(block=False)
                plt.clf()
                plot_mpl(mesh)
    #            plt.title('iteration %i' % i)
                plt.draw()
                plt.savefig(export_file + "-mesh-%i.pdf" % i, dpi=None, facecolor='w', edgecolor='w',
                                    orientation='portrait', papertype=None, format='pdf',
                                    transparent=False, bbox_inches='tight', pad_inches=0.1,
                                    frameon=None)
        except:
            pass

    # write out
    INF = {"polysys": polysys, "femdegree": femdegree, "gpcps": gpcps,
           "domain": domain, "decayexp": decayexp, "gamma": gamma, "rvtype": rvtype, "lognormal": lognormal}

    try:
        # write out
        print "EXPORTING " + export_file
        with open(export_file + '.dat', 'wb') as outfile:
            pickle.dump((SOL, INF), outfile, pickle.HIGHEST_PROTOCOL)
        # delete old file
        try:
            if export_file != delete_file[:-4]:
                print "REMOVING " + delete_file
                # remove dat
                os.remove(delete_file)
                # remove meshes
                for filepath in glob.glob(delete_file.replace(".dat", "-mesh*")):
                    if os.path.isfile(filepath):
                        print "\t", filepath
                        os.remove(filepath)
        except OSError:
            pass
    except:
        pass

    # restore CG in SOLs
    for S, CG in zip(SOL, CGS):
        S["CG"] = CG

    return export_file + '.dat'


def load_solution(solution_file, max_dofs=-1, info=True):
    logger.info("loading solution from" + solution_file)
    with open(solution_file, 'rb') as infile:
        SOL, INF = pickle.load(infile)
        polysys, femdegree, gpcps = INF["polysys"], INF["femdegree"], INF["gpcps"]
        try:
            INF["lognormal"]
        except:
            INF["lognormal"] = False

    if info:
        for i, S in enumerate(SOL):
            print "#%i \t dofs %i \t M %i \t gpcp %s" %(i, S["DOFS"], S["nr_active_ydim"], gpcps[i])

    for i, S in enumerate(SOL):
        # restore FunctionSpace and MultiindexSet
        mesh = Mesh(solution_file.replace('.dat','-mesh-%i.xml' % i))
        V = S["V"]
        S["CG"] = FunctionSpace(mesh, V.CG[0], V.CG[1])
        # enrich V for sampling
        # cos = MultiindexSet.createFullTensorSet(len(V.r) - 2, gpcps[i])
        # mis = [Multiindex(x) for x in cos.arr]
        # V.active_indices = lambda: mis
        V.polysys = polysys
        V.gpcps = gpcps[i]
        V.V = S["CG"]

        # restrict iteration # to maximal # dofs if requested
        if max_dofs > 0 and S["DOFS"] > max_dofs:
            SOL = SOL[:i]
            print "considering solution up to iteration %i with %i dofs..." %(i-1, SOL[-1]["DOFS"])
            break

    return SOL, INF


def compute_test_FE_matrix(a0fac, a0, a1fac, a1, a2fac, a2, Aop, hdeg, mesh, degree=1, withDirichletBC=True):
    def u0_boundary(x, on_boundary):
        return on_boundary

    from alea.polyquad.polynomials import StochasticHermitePolynomials
    from scipy.integrate import quad
    class HermiteCoefficient(Expression):
        def set_params(self, nu1, nu2, nu3, nu4, c0, c1, c2, hgdeg=100):
            self.P, self.W = np.polynomial.hermite.hermgauss(hgdeg)
            self.nu1 = nu1
            self.nu2 = nu2
            self.nu3 = nu3
            self.nu4 = nu4
            self.c0 = c0
            self.c1 = c1
            self.c2 = c2
            # print "MAX of c1", np.max(c1.vector().array())
        def eval(self, value, x):
            w = lambda y: np.exp(-y**2/2)
            H = StochasticHermitePolynomials(normalised=True)
            H12 = lambda y1,y2: np.exp(self.c0(x) + self.c1(x)*y1 + self.c2(x)*y2) * H.eval(self.nu1, y1) * H.eval(self.nu2, y1) * (1/np.sqrt(2*np.pi)) * H.eval(self.nu3, y2) * H.eval(self.nu4, y2) * (1/np.sqrt(2*np.pi))
            if True:
                value[0] = 0;
                for y2, ww2 in zip(self.P, self.W):
                    value[0] += sum([ww1 * ww2 * H12(y1,y2) * np.exp(y1**2/2) * np.exp(y2**2/2) for y1, ww1 in zip(self.P, self.W)])
            else:
                value[0] = quad(lambda y: H12(y) * w(y), -np.inf, np.inf)[0]
            # if self.nu2 == 2:
            #     print "\t>>>>x =", x, np.exp(self.c1(x)), value[0]
            #     assert not np.isnan(value[0])
        def value_shape(self):
            return (1,)

    # setup FE functions
    V = FunctionSpace(mesh, 'CG', degree)
    u, v = TrialFunction(V), TestFunction(V)
    bc = DirichletBC(V, Constant(0), u0_boundary)
    dof2val = bc.get_boundary_values()
    bc_dofs = dof2val.keys()

    # setup piecewise constant coefficient
    DG0 = FunctionSpace(mesh, 'DG', 0)
    dm = DG0.dofmap()
    c2d = [dm.cell_dofs(i)[0] for i in range(mesh.num_cells())]
    c0, c1, c2, cc, c12 = Function(DG0), Function(DG0), Function(DG0), Function(DG0), Function(DG0)
    # get cell midpoints
    mp = np.zeros((mesh.num_cells(), 2))
    for ci, c in enumerate(cells(mesh)):
        mpi = c.midpoint()
        mp[ci] = mpi.x(), mpi.y()
    # evaluate and set coefficient
    u0 = np.array([a0fac*a0(p) for p in mp])
    u1 = np.array([a1fac*a1(p) for p in mp])
    u2 = np.array([a2fac*a2(p) for p in mp])
    c0.vector()[:] = u0[c2d]
    c1.vector()[:] = u1[c2d]
    c2.vector()[:] = u2[c2d]

    # use Eigen backend for conversion to scipy sparse matrices
    from dolfin import parameters
    laback = parameters.linear_algebra_backend
    parameters.linear_algebra_backend = "Eigen"

    # evaluate FEM matrices
    AA = None
    AA2 = None
    UU = np.zeros([mp.shape[0],hdeg,hdeg])
    H12 = HermiteCoefficient(element=V.ufl_element())
    for nu1 in range(hdeg):
        for nu2 in range(nu1+1):
            for nu3 in range(hdeg):
                for nu4 in range(nu3+1):
                    # set coefficients
                    # cc.vector()[:] = U0[:,nu1,nu2][c2d]
                    # evaluate H12 at midpoints
                    H12.set_params(nu1, nu2, nu3, nu4, c0, c1, c2)
                    # evaluate and set coefficient
                    u12 = np.array([H12(p) for p in mp])
                    c12.vector()[:] = u12[c2d]
                    if True:
                        C12 = c12   # piecewise constant
                    else:
                        C12 = H12   # continuous

                    # assemble matrix
                    # A, B = assemble_system(cc * inner(nabla_grad(u), nabla_grad(v)) * dx, Expression('1') * v * dx, bc)
                    A2 = assemble(C12 * inner(nabla_grad(u), nabla_grad(v)) * dx)
                    # BB = assemble(Expression('1') * v * dx)

                    # apply bc
                    # bc.apply(A)
                    # bc.apply(A2)

                    # convert and store matrix
                    # rows, cols, values = as_backend_type(A).data()
                    rows2, cols2, values2 = as_backend_type(A2).data()
                    # A0 = sps.csr_matrix((values, cols, rows))
                    A02 = sps.csr_matrix((values2, cols2, rows2))

                    # modify boundary dof entries
                    if withDirichletBC:
                        # if nu1 == 0 and nu2 == 0:
                        for i in bc_dofs:
                            # J = cols[rows[i]:rows[i+1]]
                            J2 = cols2[rows2[i]:rows2[i+1]]
                            # I = [[i]] * len(J)
                            I2 = [[i]] * len(J2)
                            # A0[I,J] = 0
                            # A0[J,I] = 0
                            A02[I2,J2] = 0
                            A02[J2,I2] = 0

                    # print "*"*80
                    # print "direct:", np.linalg.norm(A0.todense() - A02.todense())
                    # print "Hdiff:", [c2(p) - H12(p) for p in mp]
                    # np.set_printoptions(threshold=np.nan, precision=2, linewidth=np.nan, suppress=True)
                    # print bc_dofs
                    # print BB.array()
                    # print B.array() - BB.array()
                    # print A0.todense() - A02.todense()
                    # if nu1 == 0:
                    #     if nu2 == 0:
                    #         adiag = np.around(np.diag(A0.todense() - A02.todense()),decimals=2)
                    #         print adiag
                    # np.set_printoptions(threshold=1000, precision=8, linewidth=75)
                    # print "*"*80

                    print "--------------------------------"
                    print "TestMatrix:", nu1, nu2, nu3, nu4
                    print "NormDiff:", np.linalg.norm(Aop[:,nu1,nu3,:,nu2,nu4] - A02.todense())
                    print "--------------------------------"

                    # UU[:,nu1,nu2] = [H12(p) for p in mp]
                    # UU[:,nu2,nu1] = [H12(p) for p in mp]
                    if AA == None:
                        # AA = np.zeros([A0.shape[0],hdeg+1,A0.shape[1],hdeg+1])
                        AA2 = np.zeros([A02.shape[0],hdeg+1,hdeg+1,A02.shape[1],hdeg+1,hdeg+1])
                    # AA[:,nu1,:,nu2] = A0.todense()
                    # AA[:,nu2,:,nu1] = A0.todense()
                    AA2[:,nu1,nu3,:,nu2,nu4] = A02.todense()
                    AA2[:,nu2,nu3,:,nu1,nu4] = A02.todense()
                    AA2[:,nu1,nu4,:,nu2,nu3] = A02.todense()
                    AA2[:,nu2,nu4,:,nu1,nu3] = A02.todense()

    # print "ComponentTest:", np.linalg.norm(U0 - UU)

    # restore backend
    parameters.linear_algebra_backend = laback

    return AA2,


parallel_coef_fs = None
parallel_sol_fs = None
parallel_ip1 = None
parallel_ip2 = None


def parallel_compute_FE_matrices_alternative_affine(value_vec, withDirichletBC, k):
    _coef_fs = parallel_coef_fs
    _sol_fs = parallel_sol_fs
    _gradu = parallel_ip1
    _gradv = parallel_ip2
    _bc_dofs = parallel_bc_dofs
    _bc = parallel_bc
    c0 = Function(_coef_fs)
    c0.vector()[:] = np.ascontiguousarray(value_vec)
    # print("assemble A")
    # assemble matrix
    with TicToc(key="      3.1.2."
                    "1 assemble matrix", active=True, do_print=False):
        A = assemble(inner(c0 * _gradu, _gradv) * dx(_sol_fs.mesh()))
    # print("convert to rows,...")
    # convert and store matrix
    if withDirichletBC:
        with TicToc(key="      3.1.2.x apply bc", active=True, do_print=False):
            _bc.zero(A)
            # A.zero(np.array(_bc_dofs, dtype=np.intc))
            # A.apply('insert')
            # _bc.zero_columns(A, _B)
    with TicToc(key="      3.1.2.2 save data as backend type", active=True, do_print=False):
        rows, cols, values = as_backend_type(A).data()

    # print("convert to sps matrix")
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        with TicToc(key="      3.1.2.3 create sparse matrix from data", active=True, do_print=False):
            A00 = sps.csr_matrix((values, cols, rows))
    retval = dict()
    retval["A0"] = A00
    retval["k"] = k
    return retval


def compute_FE_matrices_affine(coef, rhs, sol_fs, coef_fs, rhs_fs, withDirichletBC=True, n_cpu=4):
    def u0_boundary(x, on_boundary):
        return on_boundary

    assert isinstance(sol_fs, FunctionSpace)
    assert isinstance(coef_fs, FunctionSpace)
    assert isinstance(rhs_fs, FunctionSpace)

    # setup FE functions
    u, v = TrialFunction(sol_fs), TestFunction(sol_fs)

    bc = DirichletBC(sol_fs, Constant(0), u0_boundary)
    dof2val = bc.get_boundary_values()
    bc_dofs = dof2val.keys()

    # setup rhs function
    B = []
    rhs_fun = Function(rhs_fs)
    for lia in range(rhs.shape[1]):
        rhs_fun.vector()[:] = np.ascontiguousarray(rhs[:, lia])
        B_buf = assemble(rhs_fun * v * dx(sol_fs.mesh()))
        if withDirichletBC:
            bc.apply(B_buf)
        B.append(B_buf.array())
    B = np.array(B).T
    # use Eigen backend for conversion to scipy sparse matrices
    from dolfin import parameters
    laback = parameters.linear_algebra_backend
    parameters.linear_algebra_backend = "Eigen"
    if True:
        # region Compute FE matrices parallel
        with TicToc(sec_key="ASGFEM-solve", key="3.1.1 compute FE matrices parallel", active=True, do_print=False):
            global parallel_coef_fs
            global parallel_sol_fs
            global parallel_bc_dofs
            global parallel_ip1
            global parallel_ip2
            global parallel_bc
            global parallel_B

            parallel_ip1 = nabla_grad(u)
            parallel_ip2 = nabla_grad(v)
            parallel_bc_dofs = bc_dofs
            parallel_coef_fs = coef_fs
            parallel_sol_fs = sol_fs
            parallel_bc = bc

            # evaluate FEM matrices

            from joblib import delayed, Parallel

            with TicToc(sec_key="ASGFEM-solve", key="3.1.1.y compute FE matrices parallel ROBERT", active=True,
                        do_print=False):
                __A0_alternative = list(Parallel(n_jobs=1,
                                                 backend="threading"
                                                 )
                                        (delayed(parallel_compute_FE_matrices_alternative_affine)(coef[:, k],
                                                                                                  withDirichletBC,
                                                                                                  k)
                                        for k in range(coef.shape[1])))

            with TicToc(sec_key="ASGFEM-solve", key="3.1.1.z FE matrices SORTING", active=True, do_print=False):
                # _A0 = [None] * U0.shape[1]
                _A0_alternative = [None] * coef.shape[1]
                for k in range(coef.shape[1]):
                    # for ind in range(len(__A0)):
                    #     if __A0[ind]["k"] == k:
                    #         _A0[k] = __A0[ind]["A0"]
                    #         break
                    for ind in range(len(_A0_alternative)):
                        if __A0_alternative[ind]["k"] == k:
                            _A0_alternative[k] = __A0_alternative[ind]["A0"]
                            break
        # for (A00, A00_alternative) in zip(_A0, _A0_alternative):
        #     print("diff= {}".format(np.linalg.norm(A00.todense()-A00_alternative.todense())))
        # endregion

    # region compute sparse matrix operator serial
    # if False:
    #     with TicToc(key="    3.1.2 compute FE matrices serial", active=True, do_print=True):
    #         A0 = []
    #         for k in range(U0.shape[1]):
    #             # set coefficient
    #             cg.vector()[:] = U0[:, k]
    #             c0 = project(cg, FunctionSpace(mesh, 'DG', 0))
    #             # assemble matrix
    #             A = assemble(c0 * inner(nabla_grad(u), nabla_grad(v)) * dx)
    #
    #             # convert and store matrix
    #             rows, cols, values = as_backend_type(A).data()
    #             A00 = sps.csr_matrix((values, cols, rows))
    #
    #             # modify boundary dof entries
    #             if withDirichletBC:
    #                 # delete rows and columns of bc dofs
    #                 for i in bc_dofs:
    #                     J = cols[rows[i]:rows[i+1]]
    #                     I = [[i]] * len(J)
    #                     A00[I, J] = 0
    #                     A00[J, I] = 0
    #
    #             # store operator matrix
    #             A0.append(A00)
    #     # for (A00, _A00) in zip(A0, _A0):
    #     #     print("diff in computation: {}".format(np.max(np.array(_A00.data) - np.array(A00.data))))
    # endregion

    A0 = _A0_alternative
    # region Check dimension of spaces and fem matrices
    if False:
        print("DGO space dim: {}".format(mesh.num_cells()))
        print("CG {} space dim: {}".format(degree, V.dim()))
        for lia in range(len(A0)):
            print("  A0[{}] shape = {}".format(lia, A0[lia].shape))
    # endregion

    # restore backend
    parameters.linear_algebra_backend = laback

    return A0, bc_dofs, B, sol_fs

def generate_operator_tt_affine(U, ranks, hdegs, M):

    if not isinstance(U, list):
        raise ValueError("U should be a list of component tensors")
    cores = U
    d = len(cores) - 1
    opcores = [None]
    for iexp in range(M):
        # print("  {}".format(cores[iexp].shape))
        opcorei = np.zeros([ranks[iexp+1],hdegs[iexp],hdegs[iexp],ranks[iexp+2]])
        for nu1 in range(hdegs[iexp]):
            for nu2 in range(hdegs[iexp]):
                # print("mu={}, nu1={}, nu2={}".format(cores[iexp].shape, nu1, nu2))
                opcorei[:,nu1,nu2,:] = sum([cores[iexp][:,mu,:]*leg_base_change_constant(mu,nu1,nu2)
                                            for mu in range(cores[iexp].shape[1])])

        opcores.append(opcorei)

    if 0 < M <= d:
        # buffer_core = np.zeros((opcores[M].shape[0],opcores[M].shape[1], opcores[M].shape[2], 1))
        for lia in range(M, d+1):
            opcores[-1] = np.einsum('lnmk, kt->lnmt', opcores[-1], cores[lia][:, 0, :])
        # print opcores[M].shape
        # buffer_core[:, :, :, 0] = opcores[M]
        # opcores[M] = buffer_core

    return opcores
