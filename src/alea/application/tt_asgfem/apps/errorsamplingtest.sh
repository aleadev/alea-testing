#python lognormal_sgfem.py -bn LongFineLowOPRankTest -M 20 -mref 1 -mr 5
#python lognormal_sgfem.py -bn ShortFineLowOPRankTest -M 10 -mref 1 -mr 5
#python lognormal_sgfem.py -bn LongCoarseLowOPRankTest -M 20 -mref 0 -mr 5
#python lognormal_sgfem.py -bn ShortCoarseLowOPRankTest -M 10 -mref 0 -mr 5
#python lognormal_sgfem.py -bn LongFineHighOPRankTest -M 20 -mref 1
#python lognormal_sgfem.py -bn ShortFineHighOPRankTest -M 10 -mref 1
#python lognormal_sgfem.py -bn LongCoarseHighOPRankTest -M 20 -mref 0
#python lognormal_sgfem.py -bn ShortCoarseHighOPRankTest -M 10 -mref 0
python paper_error_sampling.py -sf ./results/LongCoarseHighOPRankTest-solution_M21_rank5_iter50_refine0_sysH_degD1_degS4_exp2.0_square.dat -mcN 50 -mf ./results/ShortFineLowOPRankTest-solution_M11_rank5_iter50_refine0_sysH_degD1_degS4_exp2.0_square-mesh-0.xml
python paper_error_sampling.py -sf ./results/ShortCoarseHighOPRankTest-solution_M11_rank5_iter50_refine0_sysH_degD1_degS4_exp2.0_square.dat -mcN 50 -mf ./results/ShortFineLowOPRankTest-solution_M11_rank5_iter50_refine0_sysH_degD1_degS4_exp2.0_square-mesh-0.xml
python paper_error_sampling.py -sf ./results/LongFineHighOPRankTest-solution_M21_rank5_iter50_refine0_sysH_degD1_degS4_exp2.0_square.dat -mcN 50 -mf ./results/ShortFineLowOPRankTest-solution_M11_rank5_iter50_refine0_sysH_degD1_degS4_exp2.0_square-mesh-0.xml
python paper_error_sampling.py -sf ./results/ShortFineHighOPRankTest-solution_M11_rank5_iter50_refine0_sysH_degD1_degS4_exp2.0_square.dat -mcN 50 -mf ./results/ShortFineLowOPRankTest-solution_M11_rank5_iter50_refine0_sysH_degD1_degS4_exp2.0_square-mesh-0.xml
python paper_error_sampling.py -sf ./results/LongCoarseLowOPRankTest-solution_M21_rank5_iter50_refine0_sysH_degD1_degS4_exp2.0_square.dat -mcN 50 -mf ./results/ShortFineLowOPRankTest-solution_M11_rank5_iter50_refine0_sysH_degD1_degS4_exp2.0_square-mesh-0.xml
python paper_error_sampling.py -sf ./results/ShortCoarseLowOPRankTest-solution_M11_rank5_iter50_refine0_sysH_degD1_degS4_exp2.0_square.dat -mcN 50 -mf ./results/ShortFineLowOPRankTest-solution_M11_rank5_iter50_refine0_sysH_degD1_degS4_exp2.0_square-mesh-0.xml
python paper_error_sampling.py -sf ./results/LongFineLowOPRankTest-solution_M21_rank5_iter50_refine0_sysH_degD1_degS4_exp2.0_square.dat -mcN 50 -mf ./results/ShortFineLowOPRankTest-solution_M11_rank5_iter50_refine0_sysH_degD1_degS4_exp2.0_square-mesh-0.xml
python paper_error_sampling.py -sf ./results/ShortFineLowOPRankTest-solution_M11_rank5_iter50_refine0_sysH_degD1_degS4_exp2.0_square.dat -mcN 50 -mf ./results/ShortFineLowOPRankTest-solution_M11_rank5_iter50_refine0_sysH_degD1_degS4_exp2.0_square-mesh-0.xml
