from __future__ import division
import csv
import sys
import numpy as np
import argparse
import math

parser = argparse.ArgumentParser()
parser.add_argument("-rf", "--results-file", type=str, required=True)
parser.add_argument("-g", "--gamma", type=float, default=0.9)
parser.add_argument("-ceta", type=float, default=1)
parser.add_argument("-cQ", type=float, default=1)
args = parser.parse_args()
print "========== PARAMETERS:", vars(args)

fname = args.results_file
print "loading results file", fname
with open(fname, 'rb') as csvfile:
    csvreader = csv.reader(csvfile, delimiter='\t')
    data = [row for row in csvreader]
    # print data
N = len(data[0])
assert N in (13, 14)
with_xi = N == 14
if with_xi:
    assert data[0][-1] == "xi"
    # strip xi
    data = [row[:13] for row in data]
else:
    data[0] += ["xi"]

# evaluate and add xi
sgamma, ceta, cQ = math.sqrt(1-args.gamma), args.ceta, args.cQ
print "ceta/sgamma", ceta/sgamma
xi = lambda eta, zeta, res: math.sqrt( (ceta/sgamma * eta + cQ/sgamma * zeta + cQ * res)**2 + res**2 )
data_xi = [xi(float(row[1]), float(row[7]), float(row[5])) for row in data[1:]]
data_eff = [xi_i/float(row[2]) for row, xi_i in zip(data[1:], data_xi)]
data[1:] = [[int(row[0])] + map(float, row[1:4]) + [int(row[4]), float(row[5]), eff_i, float(row[7])] + map(int, row[8:]) + [xi_i] for row, eff_i, xi_i in zip(data[1:], data_eff, data_xi)]

# TODO: reevaluate efficiency

# write out data
fname = fname.replace(".dat", "-xi.dat")
print "writing file", fname
with open(fname, 'wb') as csvfile:
    csvwriter = csv.writer(csvfile, delimiter='\t',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
    for row in data:
        csvwriter.writerow(row)

#dofs	eta	H1err	L2err	maxrank	alsres	efficiency	zeta	iter	active_ydim	marked_cell	marked_mi	new_ydim