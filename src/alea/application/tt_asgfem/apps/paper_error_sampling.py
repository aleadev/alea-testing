from __future__ import division
import logging
import logging.config
import argparse

# ALEA imports
from alea.application.egsz.sample_problems2 import SampleProblem
from alea.application.egsz.sample_domains import SampleDomain
from alea.application.egsz.mc_error_sampling import sample_error_mc
from sgfem_als_util import load_solution

from dolfin import plot, refine, Mesh, set_log_level, WARNING, INFO, ERROR

# sampling parameters
parser = argparse.ArgumentParser()
parser.add_argument("-sf", "--solution-file", type=str, required=True,
                    help="solution file")
parser.add_argument("-mcr", "--mc-runs", type=int, default=1,
                    help="MC runs (default 1)")
parser.add_argument("-mcN", "--mc-N", type=int, default=200,
                    help="MC samples (default 10)")
parser.add_argument("-md", "--max-dofs", type=int, default=-1,
                    help="max overall dofs to be considered in error calculation (default -1 = unrestricted)")
parser.add_argument("-mch", "--mc-hmax", type=float, default=1/10,
                    help="max mesh width for MC (default 0.1)")
parser.add_argument("--no-tt", action="store_true", default=False,
                    help="do not use TT optimised evaluation (default False)")
parser.add_argument("--no-cache", action="store_true", default=False,
                    help="deactivate MC FEM assembly cache (default False)")
parser.add_argument("--low-memory", action="store_true", default=False,
                    help="deactivate sampling optimisation to reduce memory consumption (default True)")
parser.add_argument("-fd", "--fem-degree", type=int, default=3,
                    help="fem degree (default 3)")
parser.add_argument("-pc", "--piecewise-coeffs", type=int, default=-1,
                    help="piecewise coefficient in DGp space (default -1 = continuous)")
parser.add_argument("-qd", "--quad-degree", type=int, default=10,
                    help="quadrature degree (default 10)")
parser.add_argument("-mf", "--mesh-file", type=str, default="",
                    help="use reference mesh from file (optional)")
parser.add_argument("-mr", "--mesh-refine", type=int, default=1,
                    help="initial refinement (default 1)")
parser.add_argument("-mm", "--ref-maxm", type=int, default=200,
                    help="KL terms to consider for reference sampling (default 200)")
parser.add_argument("-cpu", "--n-cpu", type=int, default=1,
                    help="number of cores used in parallel computing (default 1 -> serial computing)")
args = parser.parse_args()
print "========== PARAMETERS:", vars(args)

SOLUTION_FILE = args.solution_file
MC_RUNS, MC_N, MC_HMAX = args.mc_runs, args.mc_N, args.mc_hmax
fem_degree = args.fem_degree
quadrature_degree = args.quad_degree
mesh_refinements = args.mesh_refine
ref_maxm = args.ref_maxm
n_cpu = args.n_cpu

# configure logger
logger = logging.getLogger('paper_error_sampling.log')
logging.config.fileConfig('conf/tt_sampling_logging.conf', disable_existing_loggers=False)

# FEniCS config
#parameters['reorder_dofs_serial'] = False
# FEniCS logging
set_log_level(logging.WARNING)
fenics_logger = logging.getLogger("FFC")
fenics_logger.setLevel(logging.WARNING)
fenics_logger = logging.getLogger("UFL")
fenics_logger.setLevel(logging.WARNING)

# load discrete solutions
SOL, INF = load_solution(SOLUTION_FILE, args.max_dofs)

# ###############
# A problem setup
# ###############

# setup domain and meshes
domain = INF["domain"]
print domain
_, boundaries, _ = SampleDomain.setupDomain(domain, initial_mesh_N=2)

# define coefficient field
coeff_field = SampleProblem.setupCF("EF-square-cos", decayexp=INF["decayexp"], gamma=INF["gamma"],
                                        freqscale=1, freqskip=0, rvtype=INF["rvtype"], scale=1)

# setup boundary conditions and pde
pde, Dirichlet_boundary, uD, Neumann_boundary, g, f = SampleProblem.setupPDE(2, domain, 0, boundaries, coeff_field)


# ###########################
# B evaluate stochastic error
#############################

# reference mesh
if args.mesh_file == "":
    ref_mesh = SOL[-1]["CG"].mesh()
else:
    ref_mesh = Mesh(args.mesh_file)
for _ in range(mesh_refinements):
    ref_mesh = refine(ref_mesh)

if not True:
    plot(ref_mesh, title="reference mesh", interactive=False)

try:
    coeff_err = SOL[0]["coeff_err"]
    print "coeff_err =", coeff_err
except:
    coeff_err = -1

try:
    sigma = SOL[0]["sigma"]
    print "sigma =", sigma
except:
    sigma = 1

# iterate MC calculations
for i, S in enumerate(SOL):
    print "\n\n======= STARTING %s MC RUNS for solution %i of %i in file %s (TT %s) =======\n\n" % (MC_RUNS, i, len(SOL), SOLUTION_FILE, str(not args.no_tt))
    print "----->>>", S["DOFS"], S["CG"].dim()
    stored_rv_samples = []
    L2err, H1err, L2err_a0, H1err_a0, N = sample_error_mc(S["V"], pde, coeff_field, ref_mesh, ref_maxm, MC_RUNS, MC_N, MC_HMAX, stored_rv_samples,
                                                            quadrature_degree, fem_degree=fem_degree, sigma=sigma, TT=not args.no_tt, with_cache=not args.no_cache,
                                                            low_memory=args.low_memory, lognormal=INF["lognormal"], piecewise_coeffs=args.piecewise_coeffs, n_cpu=n_cpu)
    S["L2err"], S["H1err"], S["L2err_a0"], S["H1err_a0"], S["N"] = L2err, H1err, L2err_a0, H1err_a0, N
    print "({} MC runs): L2err {}\tH1err {}\t\n\t\tL2err_a0 {}\tH1err_a0 {}".format(N, L2err, H1err, L2err_a0, H1err_a0)

# export data
with file(SOLUTION_FILE.replace('.dat', '-results.dat'), 'w') as f:
    if not INF["lognormal"]:
        # ---- AFFINE ----
        f.write("dofs\teta\tH1err\tL2err\tmaxrank\talsres\tefficiency\tzeta\titer\tactive_ydim\tmarked_cell\tmarked_mi\tnew_ydim")
        for i, S in enumerate(SOL):
            f.write("\n" + "\t".join(map(str, (S["DOFS"], S["eta_global"], S["H1err"], S["L2err"], S["V"].max_rank, S["res_norm"], S["eta_global"]/S["H1err"],
                                                S["zeta_global"], S["iterations"], S["nr_active_ydim"], S["nr_cells_marked"], S["nr_mi_marked"], S["nr_new_ydim"]))))
        f.write("\n")
    else:
        # ---- LOGNORMAL ----
        try:
            S['coeff_err_MC']
        except:
            S['coeff_err_MC'] = coeff_err

        f.write("dofs\tH1err\tL2err\tH1err_a0\tL2err_a0\tmaxrank\tcoefferr")
        for i, S in enumerate(SOL):
            f.write("\n" + "\t".join(map(str, (S["DOFS"], S["H1err"], S["L2err"], S["H1err_a0"], S["L2err_a0"], S["V"].max_rank, S["coeff_err_MC"]))))
        f.write("\n")
