from __future__ import division

# ALEA imports
from alea.application.egsz.sample_problems2 import SampleProblem
from alea.application.egsz.sample_domains import SampleDomain
from alea.fem.fenics.fenics_basis import FEniCSBasis
from alea.application.egsz.tt_residual_estimator import ttEvaluateResidualEstimator, evaluateResidualEstimator
from alea.application.egsz.tt_tail_estimator import ttEvaluateUpperTailBound, ttMark_y

# tensor SGFEM discretisation
from tensor_setup import construct_deterministic_sgfem_operators, construct_stochastic_sgfem_operators
from sgfem_als_util import export_solution

# FEniCS
from dolfin import (FunctionSpace, MeshFunction, Function, interpolate, Mesh, Constant, MeshFunction, refine,
                                                set_log_level, WARNING, DEBUG, INFO, plot, interactive, File)
from alea.utils.plothelper import PlotHelper

# TT ALS
import tt
from alea.application.tt_asgfem.tensorsolver.tt_param_pde import ttParamPDEALS
from alea.application.tt_asgfem.tensorsolver.tt_util import ttRightOrthogonalize, ravel
from alea.application.tt_asgfem.tensorsolver.tt_sgfem_util import (evaluate_residual_mean_energy_norm, tt_dofs,
                                                                   tt_add_stochastic_dimension, increase_tt_rank,
                                                                   ttRandomDeterministicRankOne, evaluate_residual)

import numpy as np
from numpy.testing import assert_almost_equal
import logging
import logging.config
from operator import itemgetter
import argparse
from alea.utils.tictoc import TicToc
from math import isnan

import sys
sys.getrecursionlimit()
sys.setrecursionlimit(10000)        # increase limit for large M>100


def setup_logger():
    # configure logger
    logger = logging.getLogger('paper_sgfem_als.log')
    logging.config.fileConfig('conf/tt_paper_logging.conf', disable_existing_loggers=False)

    # FEniCS logging
    set_log_level(logging.WARNING)
    fenics_logger = logging.getLogger("FFC")
    fenics_logger.setLevel(logging.WARNING)
    fenics_logger = logging.getLogger("UFL")
    fenics_logger.setLevel(logging.WARNING)

    param_logger = logging.getLogger("../tensorsolver/tt_param_pde.log")
    param_logger.setLevel(logging.INFO)
    return logger


def evaluate_residual_estimator(V, D, CG, coeff_field, f, gpcp=None, assemble_quad_degree=15):
    summed_eta_local, eta_global = None, 0

    if False:
    # DEBUG residual estimator
        assert gpcp is not None
        with TicToc(key="**** TT residual estimator check ****", active=do_timing, do_print=False):
            eta_res_local_check, eta_res_global_check = evaluateResidualEstimator(V, D, CG, coeff_field, f, gpcp, assemble_quad_degree=assemble_quad_degree) #, PH=PH)
        print ">>>>>>>>>>>>>>eta_res_global CHECK>>>>>>>>>>>>", eta_res_global_check, np.sqrt(sum(eta_res_local_check**2))

    with TicToc(key="**** TT residual estimator ****", active=do_timing, do_print=False):
        eta_res_local, eta_res_global = ttEvaluateResidualEstimator(V, D, CG, coeff_field, f, assemble_quad_degree=assemble_quad_degree, with_volume=True, with_edge=True) #, PH=PH)

    print ">>>>>>>>>>>>>>eta_res_global>>>>>>>>>>>>", eta_res_global, np.sqrt(sum(eta_res_local**2))
    assert_almost_equal(eta_res_global, np.sqrt(sum(eta_res_local**2)))
    if summed_eta_local is None:
        summed_eta_local = eta_res_local
    else:
        summed_eta_local = np.sqrt(summed_eta_local**2 + eta_res_local**2)
    eta_global += eta_res_global
    print eta_global, np.sqrt(sum(summed_eta_local**2))
    assert_almost_equal(eta_global, np.sqrt(sum(summed_eta_local**2)))

    # DEBUG ---
    # plot of indicators
    if not True:
        F = Function(FunctionSpace(mesh, 'DG', 0))
        F.vector()[:] = np.array(summed_eta_local)
        plot(F, title="summed eta", interactive=True)
    # --- DEBUG

    return eta_global, eta_res_global, eta_res_local, summed_eta_local


# def refine_mesh_deterministic(eta_global, summed_eta_local, CG, thetaX, V):
#     # sort error indicators
#     eta_local_ind = [(x, i) for i, x in enumerate(summed_eta_local)]
#     eta_local_ind = sorted(eta_local_ind, key=itemgetter(0), reverse=True)
#
#     # setup marking set
#     mesh = CG.mesh()
#     cell_markers = MeshFunction("bool", mesh, mesh.topology().dim())
#     cell_markers.set_all(False)
#     marked_eta, cc = 0.0, 0
#
#     for eta_cell in eta_local_ind:
#         # break if sufficiently many cells are selected
#         if thetaX * eta_global**2 <= marked_eta:
#             break
#         cell_markers[eta_cell[1]] = True
#         marked_eta += eta_cell[0] ** 2
#         cc += 1
#     print "+++++++ cells marked: %i/%i" % (cc, mesh.num_cells()), "\teta_global", eta_global, "\tmarked_eta", marked_eta
#
#     with TicToc(key="**** project solution ****", active=do_timing, do_print=True):
#         F = Function(CG)
#         mesh = refine(mesh, cell_markers)
#         CG = pde.function_space(mesh, femdegree)
#         cores = V.to_list(V)
#         r = cores[0].shape[2]
#         newcore = np.zeros([1, CG.dim(), r])
#         for d in range(r):
#             print "++++++++", CG.dim(), cores[0][0, :, d].shape
#             F.vector()[:] = cores[0][0, :, d]
#             print "refinement", d, F.vector().array().shape,CG.dim()
#             newF = interpolate(F, CG)
#             newcore[0,:,d] = newF.vector().array()
#         # update U
#         cores[0] = newcore
#         V = tt.tensor.from_list(cores)
#     return V, mesh
#
#     # print ">>>>>>>>>>>>>>eta_res_global>>>>>>>>>>>>", eta_res_global, np.sqrt(sum(eta_res_local**2))
#     assert_almost_equal(eta_res_global, np.sqrt(sum(eta_res_local**2)))
#     if summed_eta_local is None:
#         summed_eta_local = eta_res_local
#     else:
#         summed_eta_local = np.sqrt(summed_eta_local**2 + eta_res_local**2)
#     eta_global += eta_res_global
#     print eta_global, np.sqrt(sum(summed_eta_local**2))
#     assert_almost_equal(eta_global, np.sqrt(sum(summed_eta_local**2)))
#
#     # DEBUG ---
#     # plot of indicators
#     if not True:
#         F = Function(FunctionSpace(mesh, 'DG', 0))
#         F.vector()[:] = np.array(summed_eta_local)
#         plot(F, title="summed eta", interactive=True)
#     # --- DEBUG
#
#     return eta_global, eta_res_global, eta_res_local, summed_eta_local


def refine_mesh_deterministic(eta_global, summed_eta_local, CG, thetaX, V):
    # sort error indicators
    eta_local_ind = [(x, i) for i, x in enumerate(summed_eta_local)]
    eta_local_ind = sorted(eta_local_ind, key=itemgetter(0), reverse=True)

    # setup marking set
    mesh = CG.mesh()
    cell_markers = MeshFunction("bool", mesh, mesh.topology().dim())
    cell_markers.set_all(False)
    marked_eta, cc = 0.0, 0

    for eta_cell in eta_local_ind:
        # break if sufficiently many cells are selected
        if thetaX * eta_global**2 <= marked_eta:
            break
        cell_markers[eta_cell[1]] = True
        marked_eta += eta_cell[0] ** 2
        cc += 1
    print "+++++++ cells marked: %i/%i" % (cc, mesh.num_cells()), "\teta_global", eta_global, "\tmarked_eta", marked_eta

    with TicToc(key="**** project solution ****", active=do_timing, do_print=True):
        F = Function(CG)
        mesh = refine(mesh, cell_markers)
        CG = pde.function_space(mesh, femdegree)
        cores = V.to_list(V)
        r = cores[0].shape[2]
        newcore = np.zeros([1, CG.dim(), r])
        for d in range(r):
            print "++++++++", CG.dim(), cores[0][0, :, d].shape
            F.vector()[:] = cores[0][0, :, d]
            print "refinement", d, F.vector().array().shape,CG.dim()
            newF = interpolate(F, CG)
            newcore[0,:,d] = newF.vector().array()
        # update U
        cores[0] = newcore
        V = tt.tensor.from_list(cores)
    return V, mesh


# -----------------------------PARSER-----------------------------------------------------------------------------------

parser = argparse.ArgumentParser()
parser.add_argument("-it", "--iterations", type=int, default=1000,
                    help="ALS iterations (default 1000)")
parser.add_argument("-u", "--updater", type=int, choices=[0,1,2], default=2,
                    help="ALS updater (default 2)")
parser.add_argument("-ref", "--refinements", type=int, default=100,
                    help="adaptive mesh refinements (default 100)")
parser.add_argument("-dofs", "--max-dofs", type=float, default=1e4,
                    help="maximal number dofs (default 1e4)")
parser.add_argument("-bn", "--basename", type=str, default="",
                    help="export basename")
parser.add_argument("-exp", "--decay-exponent", type=float, default=2,
                    help="decay exponent (default 2)")
parser.add_argument("-bd", "--boundary", type=int, choices=[0,1,2,3], default=2,
                    help="boundary (default 2)")
parser.add_argument("-dom", "--domain", type=int, choices=[0,1], default=0,
                    help="domain (default 0=square, 1=L-shaped)")
parser.add_argument("-p", "--problem", type=int, default=0,
                    help="problem type (default 0)")
parser.add_argument("-M", type=int, default=10,
                    help="KL terms starting with 0 (default 10, has to be >1)")
parser.add_argument("-fd", "--fem-degree", type=int, default=1,
                    help="fem degree (default 1)")
parser.add_argument("-gd", "--gpc-degree", type=int, default=1,
                    help="gpc degree (default 1)")
parser.add_argument("-ps", "--polysys", type=str, default="L", choices=["L","H"],
                    help="polynomial system (default L)")
parser.add_argument("-mr", "--mesh-refine", type=int, default=1,
                    help="initial refinement (default 1)")
parser.add_argument("-md", "--mesh-dofs", type=int, default=-1,
                    help="initial mesh dofs, overrides mesh-refine (default -1 = ignore)")
parser.add_argument("-mmc", "--max-mesh-cells", type=int, default=50000, dest="mmc",
                    help="maximal mesh cells for export (default 50000)")
parser.add_argument("-ez", "--eta-zeta-weight", type=float, default=0.1,
                    help="weight for global eta when compared to global zeta for refinement choice (default 0.1)")
parser.add_argument("-rz", "--resnorm-zeta-weight", type=float, default=1,
                    help="weight for resnorm when compared to global zeta for refinement choice (default 1)")
parser.add_argument("-g", "--gamma", type=float, default=0.9,
                    help="gamma (default 0.9)")
parser.add_argument("-tX", "--thetaX", type=float, default=0.5,
                    help="deterministic bulk parameter (default 0.5)")
parser.add_argument("-tY", "--thetaY", type=float, default=0.5,
                    help="stochastic bulk parameter (default 0.5)")
parser.add_argument("-nlz", "--no-longtail-zeta-marking", action="store_true", default=False,
                    help="deactivate longtail zeta marking (default active)")
parser.add_argument("-nmi", "--max-new-dim", type=int, default=1000,
                    help="add max number of additional stochastic dimensions with tail estimator (default 1000)")
parser.add_argument("-rv", "--rv-type", type=str, default="uniform", choices=["uniform","normal"],
                    help="rv type (default uniform)")
parser.add_argument("-nt", "--no-timing", action="store_true", default=False,
                    help="inline timing")
parser.add_argument("-acc","--accuracy", type=float, default=1e-14,
                    help="accuracy for SVD (default 1e-14)")
parser.add_argument("-sr","--start-rank", type=int, default=2,
                    help="starting rank for all components (default 2)")
parser.add_argument("-xr","--max-rank", type=int, default=15,
                    help="maximal rank for all components (default 15)")
parser.add_argument("-ur","--update-rank", type=int, default=1,
                    help="rank for rank update (default 1)")
parser.add_argument("-co","--convergence", type=float, default=1e-10,
                    help="distance of iteration steps in ALS (default 1e-10)")
parser.add_argument("-ra","--rank-always", action="store_true", default=False,
                    help="increases rank in every step (default False)")


args = parser.parse_args()
print "========== PARAMETERS:", vars(args)
assert args.M > 1

# set some flags
save_fn = None
WITH_EXPORT_SOLUTION = args.basename != ""

# set export name of experiment
EXPERIMENT = args.basename

# set iteration and refinement number
iterations, refinements, thetaX, thetaY, srank, maxrank, urank, updater = args.iterations, args.refinements, args.thetaX, \
                                                                          args.thetaY, args.start_rank, args.max_rank, \
                                                                          args.update_rank, args.updater

# set discretisation parameters
M, femdegree, gpcdegree, polysys = args.M, args.fem_degree, args.gpc_degree, args.polysys
decayexp, gamma, rvtype = args.decay_exponent, args.gamma, args.rv_type

# set ALS parameters
acc = args.accuracy

# set timing on/off
do_timing = not args.no_timing

# plot meshes
do_plot = not True
PH = PlotHelper()

# setup logger
#logger = setup_logger()


# A setup problem
# ===============
# setup domain and meshes
domain, mesh_refine, mesh_dofs = ["square", "lshape"][args.domain], args.mesh_refine, args.mesh_dofs
if mesh_dofs > 0:
    mesh_refine = 0
mesh, boundaries, dim = SampleDomain.setupDomain(domain, initial_mesh_N=10)
mesh = SampleProblem.setupMesh(mesh, num_refine=mesh_refine)
while mesh_dofs > 0 and FunctionSpace(mesh, 'CG', femdegree).dim() < mesh_dofs:
    # uniformly refine mesh
    mesh = refine(mesh)
#logger.info("initial mesh has %i dofs with p%i FEM" % (FunctionSpace(mesh, 'CG', femdegree).dim(), femdegree))

# define coefficient field
coeff_field = SampleProblem.setupCF("EF-square-cos", decayexp=decayexp, gamma=gamma,
                                                freqscale=1, freqskip=0, rvtype=rvtype, scale=1)

# setup boundary conditions and pde
pde, Dirichlet_boundary, uD, Neumann_boundary, g, f = SampleProblem.setupPDE(2, domain, args.problem, boundaries, coeff_field)


# B adaptive refinement loop
# ==========================
SOL = []                    # stored solution data for export
rank = srank
ranks = [1] + [rank]*(M-1) + [1]
gpcps, new_dim = [], []
resnorm = float("inf")
refine_deterministic = False
rank_done = False
oldrank = rank
for refinement in range(refinements):
    TicToc.clear()
    SAVEDATA = {}
    print "======= REFINEMENT", refinement

    # C discretisation
    # ================
    with TicToc(key="**** construct operators ****", active=do_timing, do_print=True):
        if refinement == 0:
            # setup gpc degrees
            gpcp = [gpcdegree] * (M-1)
        # setup stochastic operators
        D = [construct_stochastic_sgfem_operators(1, p, polysys)[0][1].as_matrix() for p in gpcp]
        D = [None] + D          # insert dummy for deterministic mode
        # setup deterministic operators and rhs
        K, B, _ = construct_deterministic_sgfem_operators(pde, coeff_field, M-1, mesh, degree=femdegree)
        K = [k.as_matrix() for k in K]
        B = B[:,0]
        B = ravel(B)
        P =  K[0]

    # store gpc degrees
    gpcps.append(tuple(gpcp))

    # set tensor parameters
    dsize = [d.shape[0] for d in D[1:]]
    ksize = K[0].shape[0]

    #logger.info("dsize %s and ksize %s" % (dsize, ksize))

    # setup V
    n = [ksize] + dsize

    print "==========================================="
    print "M", M
    print "gpcp", gpcp
    print "n", n
    print "ranks", ranks, rank
    print "dsize", dsize
    print "ksize", ksize
    print "==========================================="

    if refinement == 0:
        # set random initial U
        if rank == 1:
            V = ttRandomDeterministicRankOne(n)
        else:
            V = tt.rand(n, M, ranks)
            V = V*(1/V.norm())
    with TicToc(key="**** ttRightOrthogonalize ****", active=do_timing, do_print=True):
        V = ttRightOrthogonalize(V)
        ranks = V.r
    print "V[%i]:" % refinement, V

    # ---------------------------ALS------------------------------------------------------------------------------------

    with TicToc(key="**** ttParamPDEALS ****", active=do_timing, do_print=False):
        VV = tt.vector.copy(V)
        V, iter = ttParamPDEALS(K, D, B, V, maxit=iterations, conv=args.convergence, P=P, acc=acc)# D evaluate ALS residual
        if V.norm() < 1e-14:
            V = VV

    # =======================
    with TicToc(key="**** evaluate_residial_norm ****", active=do_timing, do_print=False):
        resnorm = evaluate_residual_mean_energy_norm(V, K, D, B)
        # RES,_,_ = evaluate_residual(V,K,D,B)
        # resnorm = RES.norm()
        # print "resnormerror", resnorm - resnorm3
        print("resnorm: {}".format(resnorm))

        if srank >= maxrank or isnan(resnorm):
            resnorm2 = 0
        else:
            resnorm2 = resnorm


    # E evaluate deterministic error indicator
    # ========================================
    CG = pde.function_space(mesh, femdegree)
    if thetaX > 0:
        print "==(C) evaluating residual estimator..."
        # evaluate residual error estimators
        eta_global, eta_res_global, eta_res_local, summed_eta_local = evaluate_residual_estimator(V, D, CG, coeff_field, pde.f,
                                                                                                  gpcp=gpcp, assemble_quad_degree=15)

        print "============ global_eta %f" % eta_global
    else:
        eta_global = 0


    # F evaluate stochastic error indicator
    # =====================================
    if thetaY > 0:
        print "==(D) evaluating tail estimator..."
        with TicToc(key="**** evaluateUpperTailBound ****", active=do_timing, do_print=True):
            global_zeta, zeta_ym, zeta_ym_tail = ttEvaluateUpperTailBound(V, gpcp, FEniCSBasis(CG), coeff_field, pde, add_maxm=args.max_new_dim)

        if not args.no_longtail_zeta_marking:
            # add all evaluated tail indicators to indicator set
            zeta_ym.update(zeta_ym_tail)
        global_zeta = np.sqrt(sum([z ** 2 for z in zeta_ym.values()]))

        # print "============ global_zeta %f\nzeta_ym %s" % (global_zeta, zeta_ym)
    else:
        global_zeta = 0

    cc, cmi = 0, 0

    # determine if rank has changed
    # =============================
    rank = max(V.r)
    if rank > oldrank:
        rank_done = False
    oldrank = rank

    # decide whether to refine deterministic or stochastic space or ranks
    # ===================================================================
    if global_zeta >= args.resnorm_zeta_weight * resnorm2 or args.rank_always or rank >= maxrank or rank_done:
        refine_deterministic = args.eta_zeta_weight * eta_global > global_zeta
        refine_stochastic = not refine_deterministic
        rank_done = False
    elif args.eta_zeta_weight * eta_global >= args.resnorm_zeta_weight * resnorm2 or args.rank_always or rank >= maxrank or rank_done:
        refine_deterministic = True
        refine_stochastic = False
        rank_done = False
    else:
        refine_deterministic = False
        refine_stochastic = False
        rank_done = True

    print "=========== ERROR ESTIMATES %i: ETA = %.16f (weight %f)\tZETA = %.16f \tRESNORM = %.16f (weight %f) >> refine %s ===========" %\
        (refinement, eta_global, args.eta_zeta_weight, global_zeta, resnorm, args.resnorm_zeta_weight,
         "eta" if refine_deterministic else "zeta" if refine_stochastic else "ranks")


    # G prepare export data and prolongate solution
    # =============================================
    # determine number dofs
    dofs = tt_dofs(V)
    # store iteration data
    SAVEDATA["iterations"] = iter
    SAVEDATA["V"] = V
    SAVEDATA["CG"] = CG
    SAVEDATA["DOFS"] = dofs
    SAVEDATA["XDOFS"] = CG.dim()
    SAVEDATA["res_norm"] = resnorm
    SAVEDATA["zeta_global"] = global_zeta
    SAVEDATA["eta_global"] = eta_global
    SAVEDATA["nr_active_ydim"] = len(gpcp)
    SAVEDATA["gpcp"] = gpcp

    # H refine mesh adaptively and prolongate solution
    # ================================================
    if refinement + 1 < refinements and dofs < args.max_dofs:       # skip refinement in last iteration step
        # deterministic adaptive refinement
        # =================================
        if thetaX > 0 and refine_deterministic:
            V, mesh = refine_mesh_deterministic(eta_global, summed_eta_local, CG, thetaX, V)

        # stochastic adaptive refinement
        # ==============================
        new_dim, cmi = [], 0
        if thetaY > 0 and refine_stochastic:
            new_dim, marked_zeta, not_marked_zeta_ym = ttMark_y(zeta_ym, zeta_ym_tail, thetaY, global_zeta, not args.no_longtail_zeta_marking)
            cmi = len(new_dim)
            print ">>>>>>>>>> new_dim %s for current gpcp %s" % (new_dim, gpcp)

            # update tensor w.r.t. stochastic dimensions
            V, gpcp, M = tt_add_stochastic_dimension(V, new_dim, gpcp, rank)
            print ">>>>>>>>>> gpcp after refinement is", gpcp

        # refine ranks
        # ============
        ranks = V.r
        if rank < maxrank and not refine_deterministic and not refine_stochastic:
            V, rank = increase_tt_rank(V, K, D, B, urank, updater)
        if rank < maxrank and args.rank_always:
            V, rank = increase_tt_rank(V, K, D, B, urank, updater)
        if min(ranks[1:M]) == 1:
            V, rank = increase_tt_rank(V, K, D, B, urank, updater)


    # complete save data and store
    SAVEDATA["nr_cells_marked"] = cc
    SAVEDATA["nr_mi_marked"] = cmi
    SAVEDATA["nr_new_ydim"] = len([i for i in new_dim if i >= M-2])
    SAVEDATA["marked_mi"] = new_dim
    SOL.append(SAVEDATA)

    if WITH_EXPORT_SOLUTION:
        save_fn = export_solution(SOL, args.iterations, refinements, polysys, femdegree, gpcps,
                                        domain, decayexp, gamma, rvtype, basename=EXPERIMENT, max_mesh_cells=args.mmc,
                                        delete_file=save_fn)

    if do_timing:
        print "\n\n", "-" * 80
        print "----------- OVERALL TIMINGS (refinement %i vertices %i max rank %i) -----------" % (refinement, mesh.num_vertices(), max(SOL[-1]["V"].r))
        for k, v in TicToc.items():
            print "\t", k, ":", v

    if do_plot:
        PH["mesh"].plot(mesh, interactive=False)

    # exit refinement loop if max dofs are reached
    if dofs >= args.max_dofs:
        break

for i, SD in enumerate(SOL):
    print "ALS %i: resnorm = %.16f\t\t%s\t\t%s" % (i, SD["res_norm"], SD["V"].r, gpcps[i])

if WITH_EXPORT_SOLUTION:
    export_solution(SOL, args.iterations, refinements, polysys, femdegree, gpcps,
                                    domain, decayexp, gamma, rvtype, basename=EXPERIMENT, max_mesh_cells=args.mmc)

if do_plot:
    interactive()
