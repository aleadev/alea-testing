from __future__ import division
import cPickle as pickle
import os
from dolfin import FunctionSpace, Function, plot, interactive, File, Constant, refine, MeshFunction, set_log_level, WARNING
from operator import itemgetter
import matplotlib
import numpy as np
import tt
from tensorsolver.tt_param_pde import ttParamPDEALS, rankOneProduct, ttParamExpectation, ttParamVariancePart, ttDeterministicRes, ttDeterministicRightSide
from tensorsolver.tt_util import ttNormalize, ttRightOrthogonalize, reshape, ravel, ttTruncate
from alea.application.egsz.sample_domains import SampleDomain
from alea.application.egsz.sample_problems2 import SampleProblem
from alea.fem.fenics.residual_estimator import evaluate_residual_estimator
from alea.math_utils.multiindex_set import MultiindexSet
from alea.stochastics.random_variable import UniformRV, NormalRV

matplotlib.use('TkAgg')
import logging.config

# set iteration and refinement number
iterations, refinements, theta = 20, 0, 0.5

# set saved matrix parameters
M, femdegree, gpcdegree, polysys, refined_mesh = 5, 1, 4, "L", 3

# set some flags
WITH_PLOTTING = False
WITH_EXPORT = True

# configure logger
logger = logging.getLogger('test_matrix_als.log')
logging.config.fileConfig('conf/tt_test_logging.conf', disable_existing_loggers=False)

# FEniCS config
set_log_level(WARNING)
#parameters['reorder_dofs_serial'] = False

def import_data(M, det_degree, stoch_degree, polysys, refined_mesh):
    det_file = 'data/DET_matrices_M{0}_deg{1}_ref{2}.dat'.format(M, det_degree, refined_mesh)
    det_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), det_file)
    logger.info("loading deterministic matrices from %s", det_file)
    with open(det_file, 'rb') as infile:
        data = pickle.load(infile)
    K, B1 = data
    stoch_file = 'data/STOCH_matrices_M{0}_deg{1}_poly{2}.dat'.format(M, stoch_degree, polysys)
    stoch_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), stoch_file)
    logger.info("loading stochastic matrices from %s", stoch_file)
    with open(stoch_file, 'rb') as infile:
        data = pickle.load(infile)
    D, B2 = data
    return K, D, B1, B2


def export_solution(v, V, iterations, refinements, polysys, femdegree, gpcdegree):
    # determine filename
    M = len(v.r) - 1
    max_rank = max(v.r)
    export_file = 'results/solution_M{0}_rank{1}_iter{2}_refine{3}_sys{4}_degD{5}_degS{6}'.format(M, max_rank,
                                                                      iterations, refinements, polysys, femdegree, gpcdegree)
    export_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), export_file)

    # inject additional solution data
    v.V = (V.ufl_element().family(), V.ufl_element().degree())
    v.polysys = polysys
    v.femdegree = femdegree
    v.gpcdegree = gpcdegree
    v.max_order = M
    v.max_rank = max_rank

    # export mesh of FunctionSpace
    File(export_file + "-mesh.xml") << V.mesh()

    # write out
    with open(export_file + '.dat', 'wb') as outfile:
        pickle.dump([v, (polysys, femdegree, gpcdegree)], outfile, pickle.HIGHEST_PROTOCOL)

# load saved matrices
K, D, B, _ = import_data(M, femdegree, gpcdegree, polysys, refined_mesh)
D = [D] * M

# set parameters
krank = 1
ksize = K[0].shape[0]
drank = 1
dsize = D[0].shape[0]

# make U
n = [ksize] + [dsize] * (M - 1)
ranks = [1, krank] + [drank] * (M - 2) + [1]
# set random initial U
U = tt.rand(n, M, ranks)
U = ttRightOrthogonalize(U)
print "U:", U
r = U.r
firstr = r[1]
B = ravel(B)
P =  K[0]

# call ALS
V = U
oldfun = 0
updated = False
resnorm = 1
for kl in range(iterations):
    V = ttParamPDEALS(K, D, B, V, 1, P=P)

    # calculate norm of residual and function value
    SUMM = 0
    pos = V.ps
    vcore = V.core
    r = V.r
    for i in range(M):
        W = tt.tensor.copy(V)
        v = vcore[pos[0]-1:pos[1]-1]
        v = reshape(v,[r[0],n[0],r[1]])
        W.core[pos[0]-1:pos[1]-1] = ravel(rankOneProduct(v,mid=K[i]))
        if i > 0:
            v = vcore[pos[i]-1:pos[i+1]-1]
            v = reshape(v,[r[i],n[i],r[i+1]])
            W.core[pos[i]-1:pos[i+1]-1] = ravel(rankOneProduct(v,mid=D[i]))

        if SUMM == 0:
            SUMM = W
        else:
            SUMM = SUMM + W
        SUMM = ttNormalize(SUMM)

    BB = tt.rand(n,M,1)
    bpos = BB.ps
    BB.core[bpos[0]-1:bpos[1]-1] = ravel(B)
    for i in range(1,M):
        BB.core[bpos[i]-1:bpos[i+1]-1] = ravel(np.array(np.eye(dsize,1)))

    RES = SUMM - BB
    fun = 0.5 * tt.dot(SUMM,V) - tt.dot(BB,V)
    # print kl, ': ', 'norm of residual: ', RES.norm(), '; function value: ', fun, '; relative norm of residual: ', RES.norm() / BB.norm()
    logger.info('%i: norm of residual: %f; function value: %f; relative norm of residual: %f', kl, RES.norm(), fun, RES.norm() / BB.norm())

    # rank update
    if (np.abs(fun - oldfun) < 1e-8 * RES.norm()) & (r[1] < 50 * firstr) & (RES.norm() > 1e-8) and not updated:
        # Version1: Random
        # EE = tt.rand(n, M ,1)
        # Version2: Rank 1 Residual
        EE = ttTruncate(RES,1)

        V = ttNormalize(V + EE)
        V = ttRightOrthogonalize(V)
        updated = True
        logger.info('Rank + 1: %s', V.r)
    else:
        updated = False

    oldfun = fun
    resnorm = RES.norm()


# ========================================================
# postprocessing: evaluate realisation and plot mean field
# ========================================================

# get expectation and variance tensors
v = ravel(ttParamExpectation(V))
vv = ravel(ttParamVariancePart(V))
print V, v
W = V.full()
print "W.shape", W.shape, "gpcdegree", gpcdegree


# generate realisation of parametric solution
# -------------------------------------------

# get sample
if polysys == 'L':
    rv = UniformRV()
elif polysys == 'H':
    rv = NormalRV()
else:
    raise TypeError("unsupported type")
sample = rv.sample(M-1)
print "sample", sample

# evaluate stochastic polynomials for sample
poly = rv.orth_polys
Px = np.array(poly.eval(gpcdegree, sample, all_degrees=True))
print Px.shape

# generate tensorised polynomials
def list_mul(list):
    return reduce(lambda x, y: x * y, list)
I = MultiindexSet.createCompleteOrderSet(M-1, gpcdegree).arr
psi = []
for mu in I:
    val = list_mul([Px[m,i] for i, m in enumerate(mu)])
    psi.append(val)
print psi

# construct realisation
mesh, boundaries, dim = SampleDomain.setupDomain("square", initial_mesh_N=10)
mesh = SampleProblem.setupMesh(mesh, num_refine=refined_mesh)
CG = FunctionSpace(mesh, 'CG', femdegree)

# write out solution
if WITH_EXPORT:
    export_solution(V, CG, iterations, refinements, polysys, femdegree, gpcdegree)

vf1 = Function(CG)
vfvec = np.zeros(CG.dim())
for mu, cmu in zip(I,psi):
    idx = [slice(None)] + mu.tolist()
    val = W[idx] * cmu
    vfvec += val
    logger.debug("norm of %s = %f", mu, np.linalg.norm(W[idx]))
vf1.vector()[:] = vfvec
if WITH_PLOTTING:
    plot(vf1, title='solution realisation')


# generate and plot mean field
# ----------------------------
vf2 = Function(CG)
vf2.vector()[:] = v
if WITH_PLOTTING:
    plot(vf2, title='mean solution')


# plot deviation of realisation
# -----------------------------
vf3 = Function(CG)
vf3.vector()[:] = v - vfvec
if WITH_PLOTTING:
    plot(vf3, title='deviation')


# generate and plot variance field
#---------------------------------
vf4 = Function(CG)
vf4.vector()[:] = vv - v*v
if WITH_PLOTTING:
    plot(vf4, title='variance')
    interactive()


# get deterministic result to calculate residual
#-----------------------------------------------
DetRes = ttDeterministicRes(V,D)
print type(DetRes), len(DetRes)
print [np.linalg.norm(DR) for DR in DetRes]
print DetRes[0].shape
# Here each element of the list gives a matrix for the according part of the operator
# The row indices of that matrix give the coefficients for the FEM basis while the columns yield the rank r1
# Thus, for each rank we get one FEM function of which we can calculate the residual

# get deterministic result for the right side
#--------------------------------------------
Ff = ttDeterministicRightSide(V)
print Ff.shape
print Ff
# As above, the row indices give the FEM coefficients and the columns give the rank


# evaluate residual error estimators and refine mesh
# --------------------------------------------------
# dummy data
kappa = Constant(1)
f = Constant(1)

# evaluation loop
u_h = Function(CG)
summed_eta_local, eta_global = None, 0
for dr in DetRes:
    for u_vec, f_scale in zip(dr.T, Ff):
        print type(u_vec), u_vec.shape, CG.dim()
        u_h.vector()[:] = np.array(u_vec.tolist())  # have to convert to contiguous array...
        eta_res_local, eta_res_global = evaluate_residual_estimator(CG, u_h, kappa, f_scale * f, -1)
        if summed_eta_local is None:
            summed_eta_local = eta_res_local
        else:
            summed_eta_local += eta_res_local
        eta_global += sum(eta_res_local ** 2)

# sort error indicators
eta_local_ind = [(x, i) for i, x in enumerate(summed_eta_local)]
eta_local_ind = sorted(eta_local_ind, key=itemgetter(1), reverse=True)

# setup marking set
mesh = CG.mesh()
cell_markers = MeshFunction("bool", mesh, mesh.topology().dim())
cell_markers.set_all(False)
marked_eta = 0.0
for eta_cell in eta_local_ind:
    # break if sufficiently many cells are selected
    if theta * eta_global <= marked_eta:
        break
    cell_markers[eta_cell[1]] = True
    marked_eta += eta_cell[0] ** 2

# refine mesh adaptively and prolongate solution
mesh = refine(mesh, cell_markers)
CG = FunctionSpace(mesh, 'CG', femdegree)
# TODO...

if WITH_PLOTTING:
    plot(mesh, title='refined mesh')
    interactive()
