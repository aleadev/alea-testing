from tt_normalise import rnormalise, normalise
import tt
import numpy as np

#A = tt.rand(2,3,4)
#A.core = np.array(range(1,7))

#B = rnormalise(A)
#C = A - B
#print "A:", C.norm()

#B = normalise(A)[0]
#C = A - B
#print "B:", C.norm()

def test_normalise(n, d, r):
    print "START ", (n, d, r), "==============="
    A = tt.rand(n, d, r)
    # setup core values for comparison with Matlab implementation
    #A.core = np.array(range(1, len(A.core) + 1), dtype='float64')

    B = rnormalise(A)
    C = A - B
    print "A:", C.norm()
    
    B = normalise(A)[0]
    C = A - B
    print "B:", C.norm()
    print "END ", (n, d, r), "===============\n\n"

test_normalise(2, 4, 5)
test_normalise(2, 8, 10)
test_normalise(2, 10, 20)
test_normalise(3, 4, 10)
