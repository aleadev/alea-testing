from __future__ import division
import argparse
import logging
import logging.config
import sys
from dolfin import UnitSquareMesh, cells, Function, plot, interactive
import numpy as np
import tt
from apps.sgfem_als_util import compute_FE_matrices, compute_FE_matrix_coeff
from tensorsolver.tt_als import ttALS
from tensorsolver.tt_lognormal import generate_lognormal_tt, sample_lognormal_tt, generate_operator_tt
from tensorsolver.tt_param_pde import ttRightHalfdot
from alea.application.egsz.affine_field import AffineField



def setup_logger():
    # configure logger
    logger = logging.getLogger('test_lognormal.log')
    sh = logging.StreamHandler(sys.stdout)
    sh.setLevel(logging.INFO)
    als_logger = logging.getLogger('tensorsolver.tt_als')
    als_logger.setLevel(logging.INFO)
    als_logger.addHandler(sh)
    return logger, als_logger

# ###############
# A problem setup
# ###############

parser = argparse.ArgumentParser()
parser.add_argument("-ct", "--coeff-type", type=str, default="EF-square-cos",
                    help="coefficient type")
parser.add_argument("-M", "--M-terms", type=int, default=2,
                    help="terms in realisation")
parser.add_argument("-dexp", "--decay-exp", type=float, default=2.0,
                    help="decay exponent")
parser.add_argument("-g", "--gamma", type=float, default=0.9,
                    help="gamma")
parser.add_argument("-fk", "--freq-skip", type=int, default=0,
                    help="frequency skip")
parser.add_argument("-fs", "--freq-scale", type=float, default=1.0,
                    help="frequency scaling")
parser.add_argument("-s", "--scale", type=float, default=1.0,
                    help="scaling")
parser.add_argument("-N", "--mesh-elements", type=int, default=2,
                    help="number mesh elements in one direction")
parser.add_argument("-hdeg", "--hermite-degree", type=int, default=8,
                    help="degree of hermite polynomials for approximation of operator")
parser.add_argument("-mr", "--max-rank", type=int, default=30,
                    help="maximum rank for operator")
parser.add_argument("-sr", "--sol-rank", type=int, default=5,
                    help="rank of the solution")
parser.add_argument("-gpc", "--gpc-degree", type=int, default=3,
                    help="gpc degree of the solution and the operator")
parser.add_argument("-it", "--maxit", type=int, default=20,
                    help="iterations for ALS")
args = parser.parse_args()
print "========== PARAMETERS:", vars(args)

# setup logger
logger, als_logger = setup_logger()

# setup mesh
N = args.mesh_elements
mesh = UnitSquareMesh(N, N)

# get cell midpoints
mp = np.zeros((mesh.num_cells(), 2))
for ci, c in enumerate(cells(mesh)):
    mpi = c.midpoint()
    mp[ci] = mpi.x(), mpi.y()

# ##################
# B test field class
# ##################

# create field
af = AffineField(args.coeff_type, decayexp=args.decay_exp, gamma=args.gamma, freqscale=args.freq_scale, freqskip=args.freq_skip, scale=args.scale)

# evaluate M basis functions at cell midpoints
M = args.M_terms
B = af.evaluate_basis(mp, M)
# print "B", B.shape

hdeg = args.hermite_degree
maxrank = args.max_rank
ranks = [1] + M*[maxrank] + [1]
U = generate_lognormal_tt(B,hdeg,ranks)
# print "U", U

# generate sample y and evaluate KL at y
y = af.sample_rvs(M)
V = af(mp, y)
V = np.exp(V)
# print "V", V, "y", y

VV = sample_lognormal_tt(U, y)
# print "VV", VV, "VVshape", VV.shape
print "coefficient sample approx error", np.linalg.norm(V-VV)

def evaluate_realisation_error(mp, y, U):
    V1 = af(mp, y)                  # exakt values at mp
    V1 = np.exp(V1)
    V2 = sample_lognormal_tt(U, y)  # TT values at mp
    return np.linalg.norm(V1-V2)

N = 100
err = np.sqrt(sum([evaluate_realisation_error(mp, af.sample_rvs(M), U)**2 for _ in range(N)])/N)
print "MC sample error", err

gpc = args.gpc_degree
hdegs = M * [gpc]
opcores = generate_operator_tt(U, hdegs, M)
for i in range(M):
    print "opcores", i, opcores[i+1].shape

# get first component of coefficient tensor representation
cores = tt.tensor.to_list(U)
first_comp = np.reshape(cores[0],(B.shape[0],ranks[1]))

# compute FE matrices for piecewise constant coefficients
A0, bcdofs, B, CG = compute_FE_matrices(first_comp, mesh)
# print len(A0), A0[0].shape

D = A0[0].shape[0]
opcores[0] = np.zeros([1,D,D,ranks[1]])
for r0, A in enumerate(A0):
    opcores[0][0,:,:,r0] = A.toarray()
    print "A0", A.toarray()

# generate lognormal operator tensor
A = tt.matrix.from_list(opcores)
print "A", A



####################### First Solver Test ######################
n = A.n
d = A.tt.d
solrank = args.sol_rank
# -Right-Side--------------------------------------------
B = np.reshape(B,[1,n[0],1])
F = [B] + M * [[]]
for i in range(M):
    F[i+1] = np.reshape(np.eye(n[i+1],1), [1,n[i+1],1])
F = tt.tensor.from_list(F)
print "F", F
# -Start-Vector------------------------------------------
r = [1] + M * [solrank] + [1]
U = tt.rand(n, d, r)
U = (1 / U.norm()) * U
#U = ttRandomDeterministicRankOne(n)

# preconditioner: compute mean field FEM matrix
mean_A0, _ = compute_FE_matrix_coeff(af[0][0][0], mesh)

# -ALS---------------------------------------------------
maxit = args.maxit
W = ttALS(A, F, U, approxtol=1e-14, maxit=maxit)
print "W", W


#############################
### plot solution samples ###
#############################

# generate SxM samples
S = 5
samples = [np.array(af.sample_rvs(M)) for _ in range(S)]

# evaluate stochastic polynomials for sample
poly = af.rvs[0].orth_polys
Pxs = [np.array(poly.eval(hdeg, sample, all_degrees=True)) for sample in samples]

# sum up realisation vector
# produce linear combination of fem_basis corresponding to Px
# construct FEM functions
F = []
for Px in Pxs:
	sample_sol = ttRightHalfdot(W, Px)
	f_sample_sol = Function(CG)
	f_sample_sol.vector()[:] = sample_sol
	F.append(f_sample_sol)
	plot(f_sample_sol)
interactive()
