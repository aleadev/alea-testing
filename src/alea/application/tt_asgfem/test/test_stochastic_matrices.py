import matplotlib.pyplot as plt
from tensor_setup import construct_stochastic_sgfem_operators

L = construct_stochastic_sgfem_operators(3,4,validate=False)

for l in L:
	fig = plt.figure()
	plt.spy(l.as_matrix().todense())
	plt.show()
