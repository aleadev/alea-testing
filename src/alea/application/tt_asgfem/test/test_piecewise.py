from __future__ import division
from dolfin import File, FunctionSpace, Function, TrialFunction, TestFunction, Mesh, assemble, dx, exp, Constant, DirichletBC, nabla_grad, inner, cells, parameters, as_backend_type
import scipy.sparse as sps
import numpy as np
# alea imports
from alea.application.egsz.sample_problems2 import SampleProblem
from alea.application.egsz.sample_domains import SampleDomain
from alea.application.egsz.affine_field import AffineField

degree = 1

domain = "square"
mesh, boundaries, dim = SampleDomain.setupDomain(domain, initial_mesh_N=10)
mesh = SampleProblem.setupMesh(mesh, num_refine=0)

# get cell midpoints
mp = np.zeros((mesh.num_cells(), 2))
for ci, c in enumerate(cells(mesh)):
    mpi = c.midpoint()
    mp[ci] = mpi.x(), mpi.y()

# #######################
# B construct coefficient
# #######################

# create field
af = AffineField("EF-square-cos", decayexp=2.0, gamma=0.9, freqscale=1.0, freqskip=0, scale=1.0)

# evaluate M basis functions at cell midpoints
B = af.evaluate_basis(mp, 1)

def u0_boundary(x, on_boundary):
    return on_boundary

# setup FE functions
V = FunctionSpace(mesh, 'CG', degree)
u, v = TrialFunction(V), TestFunction(V)
bc = DirichletBC(V, Constant(0), u0_boundary)
# dof2val = bc.get_boundary_values()
# bc_dofs = dof2val.keys()

# setup piecewise constant coefficient
DG0 = FunctionSpace(mesh, 'DG', 0)
dm = DG0.dofmap()
c2d = [dm.cell_dofs(i)[0] for i in range(mesh.num_cells())]
c0 = Function(DG0)
# evaluate and set coefficient
a = exp(af[4][1]*af[4][0][0])
u0 = np.array([a(p) for p in mp])
c0.vector()[:] = u0[c2d]

# use Eigen backend for conversion to scipy sparse matrices
laback = parameters.linear_algebra_backend
parameters.linear_algebra_backend = "Eigen"

# evaluate FEM matrices
# assemble matrix
A = assemble(c0 * inner(nabla_grad(u), nabla_grad(v)) * dx)
A2 = assemble(a * inner(nabla_grad(u), nabla_grad(v)) * dx)

# apply bc
# bc.apply(A)
# bc.apply(A2)

# convert and store matrix
rows, cols, values = as_backend_type(A).data()
rows2, cols2, values2 = as_backend_type(A2).data()
A0 = sps.csr_matrix((values, cols, rows))
A02 = sps.csr_matrix((values2, cols2, rows2))

# # modify boundary dof entries
# if withDirichletBC:
#     A0[bc_dofs,bc_dofs] = 1 if (nu1 == 0 and nu2 == 0) else 0
#     A02[bc_dofs,bc_dofs] = 1 if (nu1 == 0 and nu2 == 0) else 0

print "*"*80
print "direct:", np.linalg.norm(A0.todense() - A02.todense())
print "Hdiff:", [a(p) - c0(p) for p in mp]
print "*"*80

# restore backend
parameters.linear_algebra_backend = laback