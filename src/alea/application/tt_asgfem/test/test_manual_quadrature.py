from __future__ import division
from dolfin import *
import ffc
import numpy as np

# test manual quadrature
# ======================

new_dolfin = DOLFIN_VERSION_MAJOR > 1

N, deg = 10, 4
non_uniform_refinements = 3
mesh = UnitSquareMesh(N, N)
for _ in range(non_uniform_refinements):
    cf = CellFunctionBool(mesh)
    cf.set_all(False)
    cids = np.random.randint(0, mesh.num_cells()-1, mesh.num_cells()//3)
    for cid in cids:
        cf[cid] = True
    mesh = refine(mesh, cf)

qp, qw = ffc.quadratureelement.create_quadrature('triangle', deg)
qw *= 2.0    # not quite sure why...
if not new_dolfin:
    Q = FunctionSpace(mesh, "Quadrature", deg)
else:
    fe = FiniteElement(family="Quadrature", cell=mesh.ufl_cell(), degree=deg, quad_scheme="default")
    Q = FunctionSpace(mesh, fe)
gdim = mesh.geometry().dim()
# print dir(Q.dofmap())
if not new_dolfin:
    xq = Q.dofmap().tabulate_all_coordinates(mesh)
else:
    xq = Q.tabulate_dof_coordinates()
xq = xq.reshape((-1, gdim))
qn = len(qw)
ncells = xq.shape[0] // qn
vcells = [c.volume() for c in cells(mesh)]
W = np.hstack([qw * cv for cv in vcells])

print "qp qw", qp.shape, qw.shape
print "xq", xq.shape

assert ncells == mesh.num_cells()

# carry out quadrature and compare with FEniCS
def f0(x):
    return np.ones(x.shape[0])

def f1(x):
    return x[:,0]**2

def f2(x):
    return x[:,0]*x[:,1]*np.sin(2*np.pi*x[:,0])*np.sin(2*np.pi*x[:,1])

fex0 = Expression('1', degree=deg)
fex1 = Expression('x[0]*x[0]', degree=deg)
fex2 = Expression('x[0]*x[1]*sin(2*pi*x[0])*sin(2*pi*x[1])', degree=deg)

ftype = 2
f, fex = [[f0,fex0], [f1,fex1], [f2,fex2]][ftype]
fexQ = interpolate(fex, Q)

qf = f(xq)
print "manual quadrature =", np.sum(qf * W)
print "fenics quadrature =", assemble(Constant(1) * fex * dx(domain=mesh))
print "fenics Q manual =", np.sum(fexQ.vector().array() * W)
print "fenics Q =", assemble(fexQ * dx(domain=mesh))
