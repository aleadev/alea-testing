from __future__ import division

__author__ = 'pfeffer'
import numpy
import scipy.integrate

b = 1
expb = numpy.exp(numpy.power(b,2)/2)

coeffs = [expb]
for i in range(20):
    coeffs = coeffs + [(numpy.power(b,i+1)/numpy.math.factorial(i+1))*expb]

print coeffs

H = numpy.polynomial.hermite_e.HermiteE(coeffs,[-numpy.inf,numpy.inf])

print numpy.polynomial.hermite_e.hermeval(0.4027234,[0,1])

def errorfun(x,b):
    return (1/numpy.sqrt(2*numpy.pi))*(numpy.exp(2*b*x - numpy.power(x,2)/2) - 2*numpy.polynomial.hermite_e.hermeval(x,coeffs)*numpy.exp(b*x - numpy.power(x,2)/2) + numpy.power(numpy.polynomial.hermite_e.hermeval(x,coeffs),2)*numpy.exp(-numpy.power(x,2)/2))

result = scipy.integrate.quad(lambda x: errorfun(x,b),-numpy.inf,numpy.inf)
print result
