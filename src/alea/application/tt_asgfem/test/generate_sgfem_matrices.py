from __future__ import division
import numpy as np
import scipy.sparse as sp
import cPickle as pickle
import os

# SGFEM imports
from alea.application.egsz.sample_problems2 import SampleProblem
from alea.application.egsz.sample_domains import SampleDomain

from alea.linalg.scipy_operator import ScipyOperator
from alea.linalg.basis import CanonicalBasis
from alea.linalg.pcg import pcg
from tensor_setup import (construct_operator, construct_cptensor, construct_preconditioner, rank_truncate,
                            construct_deterministic_sgfem_operators, construct_stochastic_sgfem_operators)
from dolfin import refine, Mesh

WITH_LOADING = True
WITH_SAVING = True

# generator parameters
polys = ['L', 'H']
refinements = 4
FEM_degrees = [1]
GPC_degrees = [3,4]
Ms = range(1,7)                    # number expansion terms


def export_deterministic_data(K, B, degree, refinement):
    # determine filename
    M = len(K) #-1
    export_file = 'data/DET_matrices_M{0}_deg{1}_ref{2}.dat'.format(M, degree, refinement)
    export_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), export_file)
    # extract matrices from operators
    K_ = [k.as_matrix() for k in K]
    print "--- exporting data to file", export_file
    # write out
    with open(export_file, 'wb') as outfile:
        pickle.dump([K_, B[:,0]], outfile, pickle.HIGHEST_PROTOCOL)


def export_stochastic_data(M, D, B, degree, polysys):
    # determine filename
    export_file = 'data/STOCH_matrices_M{0}_deg{1}_poly{2}.dat'.format(M, degree, polysys)
    export_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), export_file)
    # extract matrices from operators
    D_ = D.as_matrix()
    print "--- exporting data to file", export_file
    # write out
    with open(export_file, 'wb') as outfile:
        pickle.dump([D_, B[:,0]], outfile, pickle.HIGHEST_PROTOCOL)


def import_data(M, det_degree, stoch_degree, polysys, refinement):
    M += 1
    det_file = 'data/DET_matrices_M{0}_deg{1}_ref{2}.dat'.format(M, det_degree, refinement)
    det_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), det_file)
    print "loading deterministic matrices from", det_file
    with open(det_file, 'rb') as infile:
        data = pickle.load(infile)
    K, B1 = data
    stoch_file = 'data/STOCH_matrices_M{0}_deg{1}_poly{2}.dat'.format(M, stoch_degree, polysys)
    stoch_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), stoch_file)
    print "loading stochastic matrices from", stoch_file
    with open(stoch_file, 'rb') as infile:
        data = pickle.load(infile)
    D, B2 = data
    return K, D, B1, B2


# setup domain and meshes
mesh, boundaries, dim = SampleDomain.setupDomain("square", initial_mesh_N=10)
mesh = SampleProblem.setupMesh(mesh, num_refine=0)

# define coefficient field
coeff_field = SampleProblem.setupCF("EF-square-cos", decayexp=2, gamma=0.9, freqscale=1, freqskip=0, rvtype="uniform", scale=1)

# setup boundary conditions and pde
pde, Dirichlet_boundary, uD, Neumann_boundary, g, f = SampleProblem.setupPDE(2, "square", 0, boundaries, coeff_field)

# construct and export matrices
start_mesh = Mesh(mesh)
for M in Ms:
    for ps in polys:
        for gpcdegree in GPC_degrees:
            # setup stochastic operators
            D, B2 = construct_stochastic_sgfem_operators(1, gpcdegree, ps)
            if WITH_SAVING:
                export_stochastic_data(M+1, D[1], B2, gpcdegree, ps)
            # print "D", len(D), D[0].domain.dim, D[0].as_matrix().shape
    mesh = Mesh(start_mesh)
    for ref in range(refinements+1):
        for femdegree in FEM_degrees:
            # setup deterministic operators
            K, B1, FS = construct_deterministic_sgfem_operators(pde, coeff_field, M, mesh, degree=femdegree)
            if WITH_SAVING:
                export_deterministic_data(K, B1, femdegree, ref)
            # print "K", len(K), type(K[0]), K[0].as_matrix().shape
            # print "B1", type(B1), B1.shape
        mesh = refine(mesh)

# test loading
if WITH_LOADING:
    for M in Ms:
        for ps in polys:
            for gpcdegree in GPC_degrees:
                for ref in range(refinements+1):
                    for femdegree in FEM_degrees:
                        K, D, B1, B2 = import_data(M, femdegree, gpcdegree, ps, ref)
                        print "matrix sizes: DET", K[0].shape, "STOCH", D[0].shape, "\toverall DOFS", K[0].shape[0]*D[0].shape[0]
