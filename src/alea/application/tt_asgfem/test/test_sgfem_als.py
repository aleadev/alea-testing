from __future__ import division
import cPickle as pickle
import logging.config
import os
from dolfin import refine, FunctionSpace, Function, interpolate, MeshFunction, set_log_level, plot, interactive, File
from operator import itemgetter
import numpy as np
import tt
from tensor_setup import (construct_operator, construct_cptensor, construct_preconditioner, construct_deterministic_sgfem_operators, construct_stochastic_sgfem_operators)
from tensorsolver.tt_param_pde import ttParamPDEALS, rankOneProduct, ttDeterministicRes, ttDeterministicRightSide
from tensorsolver.tt_util import ttNormalize, ttRightOrthogonalize, reshape, ravel, ttTruncate
from alea.application.egsz.sample_domains import SampleDomain
from alea.application.egsz.sample_problems2 import SampleProblem
from alea.fem.fenics.residual_estimator import evaluate_residual_estimator
from alea.linalg.pcg import pcg

# set some flags
WITH_PLOTTING = False
WITH_EXPORT_SOLUTION = True

# optionally evaluate solution by second order tensor pcg
WITH_TENSOR_PCG = False

# set export name of experiment
EXPERIMENT = "EXP-0-"

# set iteration and refinement number
iterations, refinements, theta = 1000, 5, 0.5

# set discretisation parameters
M, femdegree, gpcdegree, polysys, refined_mesh = 100, 1, 4, "L", 3
decayexp, gamma, rvtype = 2, 0.9, "uniform"

# configure logger
logger = logging.getLogger('test_sgfem_als.log')
logging.config.fileConfig('conf/tt_test_logging.conf', disable_existing_loggers=False)

# FEniCS logging
set_log_level(logging.WARNING)
fenics_logger = logging.getLogger("FFC")
fenics_logger.setLevel(logging.WARNING)
fenics_logger = logging.getLogger("UFL")
fenics_logger.setLevel(logging.WARNING)

# determine overall dofs of tt tensor
def tt_dofs(V):
    dofs = 0
    rr = V.r
    nn = V.n
    for i in range(V.d):
        if i < V.d-1:
            dofs = dofs + rr[i]*n[i]*rr[i+1] - rr[i+1]*rr[i+1]
        else:
            dofs = dofs + rr[i]*n[i]
    return dofs


def export_solution(SOL, iterations, refinements, polysys, femdegree, gpcdegree, domain, decayexp, gamma, rvtype, basename=""):
    # determine filename
    M = len(SOL[-1]["V"].r) - 1
    max_rank = max(SOL[-1]["V"].r)
    export_file = 'results/{0}solution_M{1}_rank{2}_iter{3}_refine{4}_sys{5}_degD{6}_degS{7}'.format(basename, M, max_rank,
                                                                      iterations, refinements, polysys, femdegree, gpcdegree)
    export_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), export_file)

    for i, S in enumerate(SOL):
        v, CG = S["V"], S["CG"]
        # inject additional solution data
        v.CG = (CG.ufl_element().family(), CG.ufl_element().degree())
        del S["CG"]
        v.max_order = len(v.r) - 1
        v.max_rank = max(v.r)

        # export mesh of FunctionSpace
        File(export_file + "-mesh-%i.xml" % i) << CG.mesh()

    # write out
    INF = {"polysys": polysys, "femdegree": femdegree, "gpcdegree": gpcdegree,
           "domain": domain, "decayexp": decayexp, "gamma": gamma, "rvtype": rvtype}
    with open(export_file + '.dat', 'wb') as outfile:
        pickle.dump((SOL, INF), outfile, pickle.HIGHEST_PROTOCOL)


# A setup problem
# ===============
# setup domain and meshes
domain, mesh_refine = ["square", "lshape"][1], 1
mesh, boundaries, dim = SampleDomain.setupDomain(domain, initial_mesh_N=10)
mesh = SampleProblem.setupMesh(mesh, num_refine=mesh_refine)

# define coefficient field
coeff_field = SampleProblem.setupCF("EF-square-cos", decayexp=decayexp, gamma=gamma,
                                                freqscale=1, freqskip=0, rvtype=rvtype, scale=1)

# setup boundary conditions and pde
pde, Dirichlet_boundary, uD, Neumann_boundary, g, f = SampleProblem.setupPDE(2, domain, 0, boundaries, coeff_field)


# B adaptive refinement loop
# ==========================
SOL = []        # stored solution data for export
for refinement in range(refinements):
    SAVEDATA = {}
    print "======= REFINEMENT", refinement

    # C discretisation
    # ================
    D, _ = construct_stochastic_sgfem_operators(1, gpcdegree, polysys)
    K, B, _ = construct_deterministic_sgfem_operators(pde, coeff_field, M-1, mesh, degree=femdegree)
    K = [k.as_matrix() for k in K]
    B = B[:,0]
    D = [D[1].as_matrix()] * M

    # set tensor parameters
    krank = 1
    ksize = K[0].shape[0]
    drank = 1
    dsize = D[0].shape[0]

    # setup U
    n = [ksize] + [dsize] * (M - 1)
    ranks = [1, krank] + [drank] * (M - 2) + [1]
    if refinement == 0:
        # set random initial U
        U = tt.rand(n, M, ranks)
    U = ttRightOrthogonalize(U)
    print "U[%i]:" % refinement, U
    r = U.r
    firstr = r[1]
    B = ravel(B)
    P =  K[0]

    # Right side in TT format
    BB = tt.rand(n,M,1)
    bpos = BB.ps
    BB.core[bpos[0]-1:bpos[1]-1] = ravel(B)
    for i in range(1,M):
        BB.core[bpos[i]-1:bpos[i+1]-1] = ravel(np.array(np.eye(dsize,1)))

    # D TT-ALS iteration
    # ==================
    V = U
    oldfun = 0
    updated = False
    resnorm, res_eps = 1, 1e-10
    for kl in range(iterations):

        V = ttParamPDEALS(K, D, B, V, 1, P=P, acc=1e-20)

        # calculate norm of residual and function value
        SUMM = 0
        pos = V.ps
        vcore = V.core
        r = V.r
        for i in range(M):
            W = tt.tensor.copy(V)
            v = vcore[pos[0]-1:pos[1]-1]
            v = reshape(v,[r[0],n[0],r[1]])
            W.core[pos[0]-1:pos[1]-1] = ravel(rankOneProduct(v,mid=K[i]))
            if i > 0:
                v = vcore[pos[i]-1:pos[i+1]-1]
                v = reshape(v,[r[i],n[i],r[i+1]])
                W.core[pos[i]-1:pos[i+1]-1] = ravel(rankOneProduct(v,mid=D[i]))

            if SUMM == 0:
                SUMM = W
            else:
                SUMM = SUMM + W
            if divmod(i,10)[1] == 9:
                SUMM = ttNormalize(SUMM,approxtol=1e-14)

        print SUMM.r

        RES = SUMM - BB
        fun = 0.5 * tt.dot(SUMM,V) - tt.dot(BB,V)
        # print kl, ': ', 'norm of residual: ', RES.norm(), '; function value: ', fun, '; relative norm of residual: ', RES.norm() / BB.norm()
        logger.info('%i: norm of residual: %f; function value: %f; relative norm of residual: %f', kl, RES.norm(), fun, RES.norm() / BB.norm())

        # rank update
        if (np.abs(fun - oldfun) < res_eps * RES.norm()) and (r[1] < 50 * firstr) and (RES.norm() > 1e-8) and not updated:
            # Version1: Random
            # EE = tt.rand(n, M ,1)
            # Version2: Rank 1 Residual
            EE = ttTruncate(RES,1)

            V = ttNormalize(V + EE)
            V = ttRightOrthogonalize(V)
            updated = True
            logger.info('Rank + 1: %s', V.r)
        elif np.abs(fun - oldfun) < 1e-14:
            break

        oldfun = fun
        resnorm = RES.norm()

    # E adaptive refinement
    # =====================
    # get deterministic result to calculate residual
    #-----------------------------------------------
    DetRes = ttDeterministicRes(V,D)
    # print [np.linalg.norm(DR) for DR in DetRes]
    # print DetRes[0].shape
    # Here each element of the list gives a matrix for the according part of the operator
    # The row indices of that matrix give the coefficients for the FEM basis while the columns yield the rank r1
    # Thus, for each rank we get one FEM function of which we can calculate the residual

    # get deterministic result for the right side
    #--------------------------------------------
    Ff = ttDeterministicRightSide(V)
    # As above, the row indices give the FEM coefficients and the columns give the rank

    # evaluate residual error estimators and refine mesh
    # --------------------------------------------------
    # evaluation loop
    CG = pde.function_space(mesh, femdegree)
    summed_eta_local, eta_global = None, 0
    for k1 in range(Ff.shape[0]):
        f_scale = Ff[k1]
        UH, CF = [], []
        for m, dr in enumerate(DetRes):
            u_h = Function(CG)
            u_vec = dr.T[k1]
            u_h.vector()[:] = np.array(u_vec.tolist())  # have to convert to contiguous array...
            # plot(u_h, title="F%i"%m, interactive=True)
            UH.append(u_h)
            CF.append(coeff_field[m][0])
        eta_res_local, eta_res_global = evaluate_residual_estimator(CG, UH, coeff_field._mean_func, CF, f_scale * pde.f, 15)

        if not True:
            F = Function(CG)
            F.vector()[:] = sum([U.vector().array() for U in UH])
            plot(F, title="summed U", interactive=True)

        # print eta_res_global, np.sqrt(sum(eta_res_local**2))
        assert eta_res_global == np.sqrt(sum(eta_res_local**2))
        if summed_eta_local is None:
            summed_eta_local = eta_res_local
        else:
            summed_eta_local = np.sqrt(summed_eta_local**2 + eta_res_local**2)
        # eta_global = np.sqrt(eta_global**2 + eta_res_global**2)
        eta_global += eta_res_global
        # print eta_global, np.sqrt(sum(summed_eta_local**2))
        # assert eta_global == np.sqrt(sum(summed_eta_local**2))

    if not True:
        F = Function(FunctionSpace(mesh, 'DG', 0))
        F.vector()[:] = np.array(summed_eta_local)
        plot(F, title="summed eta", interactive=True)

    # sort error indicators
    eta_local_ind = [(x, i) for i, x in enumerate(summed_eta_local)]
    eta_local_ind = sorted(eta_local_ind, key=itemgetter(0), reverse=True)

    # setup marking set
    mesh = CG.mesh()
    cell_markers = MeshFunction("bool", mesh, mesh.topology().dim())
    cell_markers.set_all(False)
    marked_eta, cc = 0.0, 0

    # TODO: BUG IN GLOBAL ERROR CALCULATION
    # FIX AND DELETE FOLLOWING LINE
    eta_global = np.sqrt(sum(e[0]**2 for e in eta_local_ind))

    for eta_cell in eta_local_ind:
        # break if sufficiently many cells are selected
        if theta * eta_global**2 <= marked_eta:
            break
        cell_markers[eta_cell[1]] = True
        marked_eta += eta_cell[0] ** 2
        cc += 1
    print "+++++++ cells marked: %i/%i" % (cc, mesh.num_cells()), "\teta_global", eta_global, "\tmarked_eta", marked_eta

    # determine number dofs
    dofs = tt_dofs(V)

    # store iteration data
    SAVEDATA["iterations"] = kl
    SAVEDATA["V"] = V
    SAVEDATA["CG"] = CG
    SAVEDATA["DOFS"] = dofs
    SAVEDATA["cells_marked"] = cc
    SAVEDATA["res_norm"] = resnorm
    SAVEDATA["eta_global"] = eta_global
    SOL.append(SAVEDATA)

    # refine mesh adaptively and prolongate solution
    if refinement + 1 < refinements:    # skip in last refinement
        F = Function(CG)
        mesh = refine(mesh, cell_markers)
        CG = pde.function_space(mesh, femdegree)
        cores = V.to_list(V)
        r = cores[0].shape[2]
        newcore = np.zeros([1, CG.dim(), r])
        for d in range(r):
            F.vector()[:] = cores[0][0, :, d]
            print "refinement", d, F.vector().array().shape,CG.dim()
            newF = interpolate(F, CG)
            newcore[0,:,d] = newF.vector().array()
        # update U
        cores[0] = newcore
        U = tt.tensor.from_list(cores)


if WITH_EXPORT_SOLUTION:
    export_solution(SOL, iterations, refinements, polysys, femdegree, gpcdegree,
                                    domain, decayexp, gamma, rvtype, basename=EXPERIMENT)


if WITH_PLOTTING:
    from matplotlib.pyplot import figure, show, legend
    plotdata = {"DOFS": [], "eta": [], "rank": [], "resnorm": []}
    for i, S in enumerate(SOL):
            plotdata["DOFS"].append(S["DOFS"])
            plotdata["eta"].append(S["eta_global"])
            plotdata["rank"].append(S["V"].max_rank)
            plotdata["rank"].append(S["resnorm"])

    fig1 = figure()
    fig1.suptitle("error estimator")
    ax = fig1.add_subplot(111)
    ax.loglog(plotdata["DOFS"], plotdata["eta"], '-ro', label='eta')
    ax.loglog(plotdata["DOFS"], plotdata["rank"], '-g<', label='max rank')
    ax.loglog(plotdata["DOFS"], plotdata["resnorm"], '-r*', label='ALS res')
    legend(loc='upper right')
    show()

    plot(mesh, title='refined mesh')
    interactive()



# F test tensor pcg
# =================
def test_solve_pcg(A, P, u, f, **kwargs):
    """Solve the linear problem with given solution and show solve statistics"""
    [u2, _, numiter] = pcg(A, f, P, 0 * u, **kwargs)
    print "error:  %s", np.linalg.norm(u.flatten().as_array() - u2.flatten().as_array())
    print "residual: %s", np.linalg.norm(f.flatten().as_array() - (A * u2).flatten().as_array())
    print "iter:   %s", numiter
    print "norm_u: %s", np.linalg.norm(u.flatten().as_array())
    print "norm_f: %s", np.linalg.norm((A * u).flatten().as_array())
    return u2

if WITH_TENSOR_PCG:
    A = construct_operator(K, D)
    P = construct_preconditioner(A, identity=True)

    u = construct_cptensor(B1, B2)
    u_flat = u.flatten()

    eps = 1e-3
    u = test_solve_pcg(A, P, u_flat, A * u_flat, eps=eps)
    # test_solve_pcg(A, P, u, A * u, truncate_func=rank_truncate(10), eps=eps)

    from dolfin import Function, plot
    U = u.as_matrix()
    V = FS._fefs
    u0 = Function(V)
    v0 = np.array(U[:,0])
    print type(v0), v0.shape
    u0.vector()[:] = v0
    print U.shape
    plot(u0, title='u0', interactive=True)