import cPickle as pickle
import os
from tensorsolver.tt_param_pde import paramOuterProduct, multiFirstUnit
from tensorsolver.tt_util import ravel



# ---------------------IMPORT-MATRICES-AS-IN-TEST-CASE--------------------------------------
def import_data(M, det_degree, stoch_degree, polysys, refinement):
    det_file = 'DET_matrices_M{0}_deg{1}_ref{2}.dat'.format(M, det_degree, refinement)
    det_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), det_file)
    print "loading deterministic matrices from", det_file
    with open(det_file, 'rb') as infile:
        data = pickle.load(infile)
    K, B1 = data
    stoch_file = 'STOCH_matrices_M{0}_deg{1}_poly{2}.dat'.format(2, stoch_degree, polysys)
    stoch_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), stoch_file)
    print "loading stochastic matrices from", stoch_file
    with open(stoch_file, 'rb') as infile:
        data = pickle.load(infile)
    D, B2 = data
    return K, D, B1, B2

# load saved matrices
M, femdegree, gpcdegree, polysys, refinement = 4, 1, 4, "L", 3
K, D, B1, B2 = import_data(M, femdegree, gpcdegree, polysys, refinement)
B = B1

# Set all D, since not correct above
Delta = D[1]
for i in range(2,M):
    D.append(Delta)

B = ravel(B)
# ------------------------------------------------------------------------------------------

# For list D generate list DD[i] = I x I x ... x Delta_i x ... x I
DD = paramOuterProduct(D,M)

# For right side generate tensor product of first unit vector E = e_1 x ... x e_1
E = multiFirstUnit(M,D[1].shape[0])

# Now you can solve the problem with operator K[0] x DD[0] + ... + K[M-1] x DD[M-1]
# and right side B x E