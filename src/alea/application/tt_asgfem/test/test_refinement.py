from __future__ import division
import cPickle as pickle
import logging
import os
from dolfin import refine, FunctionSpace, Function, interpolate, Mesh, Constant, MeshFunction, set_log_level, WARNING
from operator import itemgetter
import numpy as np
import tt
from tensorsolver.tt_param_pde import ttDeterministicRes, ttDeterministicRightSide
from alea.application.egsz.sample_domains import SampleDomain
from alea.application.egsz.sample_problems2 import SampleProblem
from alea.fem.fenics.residual_estimator import evaluate_residual_estimator
from alea.math_utils.multiindex import Multiindex
from alea.math_utils.multiindex_set import MultiindexSet


# FEniCS config
set_log_level(WARNING)

# sampling parameters
SOLUTION_FILES = ["results/solution_M5_rank7_iter50_refine0_sysL_degD1_degS4.dat"]

# configure logger
logger = logging.getLogger('tt-als')
# create file handler which logs even debug messages
fh = logging.FileHandler(__name__+'.log')
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

def import_stochastic_data(M, stoch_degree, polysys, refined_mesh):
    # det_file = 'data/DET_matrices_M{0}_deg{1}_ref{2}.dat'.format(M, det_degree, refined_mesh)
    # det_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), det_file)
    # logger.info("loading deterministic matrices from %s", det_file)
    # with open(det_file, 'rb') as infile:
    #     data = pickle.load(infile)
    # K, B1 = data
    stoch_file = 'data/STOCH_matrices_M{0}_deg{1}_poly{2}.dat'.format(M, stoch_degree, polysys)
    stoch_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), stoch_file)
    logger.info("loading stochastic matrices from %s", stoch_file)
    with open(stoch_file, 'rb') as infile:
        data = pickle.load(infile)
    D = data[0]
    return D


def load_solution(solution_file):
    logger.info("loading solution from", solution_file)
    with open(solution_file, 'rb') as infile:
        w = pickle.load(infile)[0]

    # restore FunctionSpace and MultiindexSet
    mesh = Mesh(solution_file.replace('.dat','-mesh.xml'))
    w.V = FunctionSpace(mesh, w.V[0], w.V[1])
    cos = MultiindexSet.createCompleteOrderSet(w.max_order-1, w.gpcdegree)
    mis = [Multiindex(x) for x in cos.arr]
    w.active_indices = lambda: mis

    return w


# ###############
# A problem setup
# ###############

# setup domain and meshes
mesh, boundaries, dim = SampleDomain.setupDomain("square", initial_mesh_N=10)
mesh = SampleProblem.setupMesh(mesh, num_refine=0)

# define coefficient field
coeff_field = SampleProblem.setupCF("EF-square-cos", decayexp=2, gamma=0.9, freqscale=1, freqskip=0, rvtype="uniform", scale=1)

# setup boundary conditions and pde
pde, Dirichlet_boundary, uD, Neumann_boundary, g, f = SampleProblem.setupPDE(2, "square", 0, boundaries, coeff_field)


# #####################
# B adaptive refinement
# #####################

for fname in SOLUTION_FILES:
    # load discrete solution
    w = load_solution(fname)
    M, stoch_degree, polysys, refinements = (fname[fname.find("_M")+2:fname.find("_rank")],
                                fname[fname.find("_degS")+5:fname.find(".dat")],
                                fname[fname.find("_sys")+4:fname.find("_deg")], 0)
    D = import_stochastic_data(M, stoch_degree, polysys, refinements)
    D = [D] * int(M)

    # test refinement
    CG = w.V
    cores = w.to_list(w)
    print w, len(cores)
    print [c.shape for c in cores]

    F = Function(CG)
    mesh = refine(CG.mesh())
    femfamily, femdegree = CG.ufl_element().family(), CG.ufl_element().degree()
    CG2 = FunctionSpace(mesh, femfamily, femdegree)
    r = cores[0].shape[2]
    newcore = np.zeros([1, CG2.dim(),r])
    for d in range(r):
        F.vector()[:] = cores[0][0,:,d]
        newF = interpolate(F, CG2)
        newcore[0,:,d] = newF.vector().array()

    cores[0] = newcore
    neww = tt.tensor.from_list(cores)
    print neww


    # get deterministic result to calculate residual
    #-----------------------------------------------
    DetRes = ttDeterministicRes(w, D)
    # Here each element of the list gives a matrix for the according part of the operator
    # The row indices of that matrix give the coefficients for the FEM basis while the columns yield the rank r1
    # Thus, for each rank we get one FEM function of which we can calculate the residual

    # get deterministic result for the right side
    #--------------------------------------------
    Ff = ttDeterministicRightSide(w)
    # As above, the row indices give the FEM coefficients and the columns give the rank

    # evaluate residual error estimators and refine mesh
    # --------------------------------------------------
    # dummy data
    kappa = Constant(1)
    f = Constant(1)

    # evaluation loop
    u_h = Function(CG)
    summed_eta_local, eta_global = None, 0
    for dr in DetRes:
        for u_vec, f_scale in zip(dr.T, Ff):
            print type(u_vec), u_vec.shape, CG.dim()
            u_h.vector()[:] = np.array(u_vec.tolist())  # have to convert to contiguous array...
            eta_res_local, eta_res_global = evaluate_residual_estimator(CG, u_h, kappa, f_scale * f, -1)
            if summed_eta_local is None:
                summed_eta_local = eta_res_local
            else:
                summed_eta_local += eta_res_local
            eta_global += sum(eta_res_local ** 2)

    # sort error indicators
    eta_local_ind = [(x, i) for i, x in enumerate(summed_eta_local)]
    eta_local_ind = sorted(eta_local_ind, key=itemgetter(1), reverse=True)

    # setup marking set
    theta = 0.5
    mesh = CG.mesh()
    cell_markers = MeshFunction("bool", mesh, mesh.topology().dim())
    cell_markers.set_all(False)
    marked_eta = 0.0
    for eta_cell in eta_local_ind:
        # break if sufficiently many cells are selected
        if theta * eta_global <= marked_eta:
            break
        cell_markers[eta_cell[1]] = True
        marked_eta += eta_cell[0] ** 2

    # refine mesh adaptively and prolongate solution
    mesh = refine(mesh, cell_markers)
    CG2 = FunctionSpace(mesh, femfamily, femdegree)
    cores = w.to_list(w)
    newcore = np.zeros([1, CG2.dim(),r])
    for d in range(r):
        F.vector()[:] = cores[0][0,:,d]
        newF = interpolate(F, CG2)
        newcore[0,:,d] = newF.vector().array()

    cores[0] = newcore
    neww = tt.tensor.from_list(cores)
    print neww
