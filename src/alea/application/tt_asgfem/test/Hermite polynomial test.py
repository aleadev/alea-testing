
# coding: utf-8

# In[132]:

from __future__ import division
import itertools as iter
import numpy as np
import tt
from scipy.integrate import quad
from scipy.special import eval_hermite, eval_hermitenorm
from tensorsolver.tt_lognormal import generate_operator_tt, generate_lognormal_tt, hermite_triprod
from tensorsolver.tt_util import reshape
from alea.polyquad.polynomials import StochasticHermitePolynomials


# setup different versions of Hermite polynomials

# In[215]:

# alea not normalised
# --- probabilist ---
h = StochasticHermitePolynomials(normalised=False)
# ...normalised probabilist
nh = StochasticHermitePolynomials(normalised=True)

# Mugler (2.5)
fac = np.math.factorial
def He(n, x):   # --- physicist ---
    return np.sum([(-1)**j*fac(n)*x**(n-2*j)/(fac(n-2*j)*fac(j)*2**j) for j in range(np.floor_divide(n, 2)+1)])

# transformation of wikipedia [different in Ullmann (A.2)?]
def THe(n, x):  # --- probabilist ---
    return 2**(n/2)*He(n, x*np.sqrt(2))    
#    return 1/(np.sqrt(2**n*fac(n)))*He(n, x/np.sqrt(2))

def Th(n, x):   # --- physicist ---
    return 2**(n/2)*h.eval(n, x*np.sqrt(2))
#    return 1/(np.sqrt(2**n*fac(n)))*h.eval(n, x/np.sqrt(2))

# scipy polynomials
def spH(n, x):  # --- physicist ---
    return eval_hermite(n, x)

def spHn(n, x): # --- probabilist ---
    return eval_hermitenorm(n, x)

# setup Hermite-Gauss quadrature with weight exp(-x**2)
hgdeg = 100
P, W = np.polynomial.hermite.hermgauss(hgdeg)
for n1, n2 in iter.product(range(8), range(8)):
    if n1 >= n2:
        a = 1/10
        H = lambda x: np.exp(a*x) * nh.eval(n1, x) * nh.eval(n2, x) * (1/np.sqrt(2*np.pi))
        print n1, n2, quad(lambda x: H(x)*np.exp(-x**2/2), -np.inf, np.inf)[0], sum([w*H(p)*np.exp(p**2/2) for p, w in zip(P,W)])


# setup triple quadrature with Gaussian density $w=\exp(-x^2/2)$ on $[-\infty,\infty]$ and show that $\texttt{h, nh, He}$ are normalised

# In[213]:

# def Hquad3(f, i, j=None, k=None, w=lambda x: np.exp(-x**2/2)):
#     if j is None:
#         return quad(lambda x: f(i, x)*w(x), -np.inf, np.inf)[0]
#     elif k is None:
#         return quad(lambda x: f(i, x)*f(j, x)*w(x), -np.inf, np.inf)[0]
#     else:
#         return quad(lambda x: f(i, x)*f(j, x)*f(k, x)*w(x), -np.inf, np.inf)[0]
#
# print [1/(fac(n)*np.sqrt(2*np.pi))*Hquad3(h.eval, n, n) for n in range(5)]
# print [1/np.sqrt(2*np.pi)*Hquad3(nh.eval, n, n) for n in range(5)]
# print [1/(fac(n)*np.sqrt(2*np.pi))*Hquad3(He, n, n) for n in range(5)]
# print [1/(2**n*fac(n)*np.sqrt(np.pi))*Hquad3(Th, n, n, w=lambda x: np.exp(-x**2)) for n in range(5)]


# In[171]:



# check triple product as defined in Ullmann with triple quadrature of different polynomial definitions

# In[202]:

b1 = np.random.rand(8,2)
eb1 = np.exp(b1[:,0])

U = generate_lognormal_tt(b1,8,[1,8,1])
cores = tt.tensor.to_list(U)
opcores = generate_operator_tt(U,[3],1)

coeffs = np.zeros([8,8])
for idet in range(8):
    for ig in range(8):
        coeffs[idet,ig] = (np.power(b1[idet,1],ig)/np.sqrt(np.math.factorial(ig)))*np.exp(np.power(b1[idet,1],2)/2)

X, S, Y = np.linalg.svd(coeffs,full_matrices=0)
X = X[:, :8]
S = np.diag(S[:8])
Y = Y[:8, :]
left = X.dot(S)
right = Y

int1 = np.zeros([8,3,3])
int2 = np.zeros([8,3,3])
for idet in range(8):
    for nu1 in range(3):
        for nu2 in range(3):
            int1[idet,nu1,nu2] = sum( [ sum([eb1[idet]*left[idet,k]*right[k,mu]*hermite_triprod(mu,nu1,nu2) for mu in range(8)]) for k in range(8) ] )

            H = lambda x: np.exp(b1[idet,0] + b1[idet,1]*x) * nh.eval(nu1, x) * nh.eval(nu2, x) * (1/np.sqrt(2*np.pi))
            int2[idet,nu1,nu2] = sum([w*H(p)*np.exp(p**2/2) for p, w in zip(P,W)])

print "intdif:", int1 - int2

opint1 = np.zeros([8,3,3])
opint2 = np.zeros([8,3,3])
for k in range(8):
    for nu1 in range(3):
        for nu2 in range(3):
            opint1[k,nu1,nu2] = sum([right[k,mu]*hermite_triprod(mu,nu1,nu2) for mu in range(8)])

opint2 = reshape(opcores[1],[8,3,3])

print "opintdif:", opint1 - opint2

first_comp = np.zeros([8,8])
for idet in range(8):
    first_comp[idet,:] = eb1[idet]*left[idet,:]

print "firstdif:", first_comp - reshape(cores[0],[8,8])


# setup multiindex triples

# In[203]:

# Lambda = [mi for mi in iter.product(range(8),range(9),range(7))]


# evaluate triple products for comparison to see that structure matches but scaling does not!

# In[207]:

# print sum([hermite_triprod(*mi) - Hquad3(nh.eval, *mi, w=lambda x: (1/np.sqrt(2*np.pi))*np.exp(-x**2/2)) for mi in Lambda])


# In[ ]:



