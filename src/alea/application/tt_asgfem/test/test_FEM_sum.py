from __future__ import division
from dolfin import FunctionSpace, Function, TrialFunction, TestFunction, assemble, dx, Expression, Constant, DirichletBC, nabla_grad, inner, cells
import numpy as np
import scipy.sparse as sps
from tensorsolver.tt_util import reshape
from alea.application.egsz.affine_field import AffineField
from alea.application.egsz.sample_domains import SampleDomain
from alea.application.egsz.sample_problems2 import SampleProblem


degree = 1

domain = "square"
mesh, boundaries, dim = SampleDomain.setupDomain(domain, initial_mesh_N=2)
mesh = SampleProblem.setupMesh(mesh, num_refine=0)

# get cell midpoints
mp = np.zeros((mesh.num_cells(), 2))
for ci, c in enumerate(cells(mesh)):
    mpi = c.midpoint()
    mp[ci] = mpi.x(), mpi.y()

# #######################
# B construct coefficient
# #######################

# create field
af = AffineField("EF-square-cos", decayexp=2.0, gamma=0.9, freqscale=1.0, freqskip=0, scale=1.0)

# evaluate M basis functions at cell midpoints
B = af.evaluate_basis(mp, 1)

def u0_boundary(x, on_boundary):
    return on_boundary

# setup FE functions
V = FunctionSpace(mesh, 'CG', degree)
u, v = TrialFunction(V), TestFunction(V)
f = Expression('1')
bc = DirichletBC(V, Constant(0), u0_boundary)
dof2val = bc.get_boundary_values()
bc_dofs = dof2val.keys()

# setup piecewise constant coefficient
DG0 = FunctionSpace(mesh, 'DG', 0)
dm = DG0.dofmap()
c2d = [dm.cell_dofs(i)[0] for i in range(mesh.num_cells())]
c0 = Function(DG0)

# use uBLAS backend for conversion to scipy sparse matrices
from dolfin import parameters
laback = parameters.linear_algebra_backend
parameters.linear_algebra_backend = "uBLAS"

R = np.random.rand(2,3,3)

A1 = None
for nu1 in range(3):
    for nu2 in range(3):
        A0 = []
        for m in range(B.shape[1]):
            # set coefficient
            c0.vector()[:] = (B[:,m]*R[m,nu1,nu2])[c2d]

            # assemble matrix
            A = assemble(c0 * inner(nabla_grad(u), nabla_grad(v)) * dx)

            # apply bc
#            bc.apply(A)

            # convert and store matrix
            rows, cols, values = A.data()
            A00 = sps.csr_matrix((values, cols, rows))

            # modify boundary dofs
            if nu1 == 0 and nu2 == 0:
                A00[bc_dofs,bc_dofs] = 1 if m == 0 else 0

            A0.append(A00.todense())
            if A1 == None:
                A1 = np.zeros([A0[0].shape[0],3,A0[0].shape[1],3])
        A1[:,nu1,:,nu2] = sum(A0)

R = reshape(R,[B.shape[1],9])
b = reshape(B.dot(R),[B.shape[0],3,3])
A2 = np.zeros(A1.shape)
for nu1 in range(3):
    for nu2 in range(3):
        # set coefficient
        c0.vector()[:] = b[:,nu1,nu2][c2d]

        # assemble matrix
        A = assemble(c0 * inner(nabla_grad(u), nabla_grad(v)) * dx)

        # apply bc
#        bc.apply(A)

        # convert and store matrix
        rows, cols, values = A.data()
        A20 = sps.csr_matrix((values, cols, rows))

        # modify boundary dofs
        if nu1 == 0 and nu2 == 0:
            A20[bc_dofs,bc_dofs] = 1

        A2[:,nu1,:,nu2] = A20.todense()

print A1 - A2
print np.linalg.norm(A1-A2)

# restore backend
parameters.linear_algebra_backend = laback
