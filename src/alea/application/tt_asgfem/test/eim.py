from __future__ import division
from alea.math_utils.multiindex_set import MultiindexSet
import numpy as np
import copy

# TODO: separate specific FEM from implementation, i.e. pass in coordinates and only work with vectors

class EIM(object):
    def __init__(self, f, sample_dict, N=None, eps=1e-10, lagrange_p=2):
        # format for samples is [y_m, u(x,y_m)] and first element is taken as first basis
        self._f = f
        self._coordinates = sample_dict[0][1].function_space().mesh().coordinates()
        self._basis = []
        self._lagrange_p = lagrange_p
        N = N if N is not None else len(sample_dict)
        self.generate_basis(copy.copy(sample_dict), N, eps)

    def generate_basis(self, sample_dict, N=None, eps=1e-10):
        # format for basis is [x id, y, basis]
        assert N <= len(sample_dict)
        # init with first basis
        basis0 = sample_dict.pop(0)
        self._basis = [[np.argmax(basis0[1].vector().array()), basis0[0], basis0[1]]]
        self.update_lagrange()
        # iteratively determine base functions
        while len(sample_dict) > 0 and len(self._basis) < N:
            next_basis = [0, None, None, None, None, None]
            for i, basis0 in enumerate(sample_dict[1:]):
                y0, b0 = basis0
                r0 = self.evaluate_residual(y0)
                i0 = np.argmax(r0)
                if next_basis[0] < r0[i0]:
                    next_basis = [r0[i0], i, i0, y0, b0, r0]
            if next_basis[0] < eps:
                print "------- eps reached", eps, next_basis[0]
                break
            # create new basis
            new_basis = next_basis[5]
            new_basis /= new_basis[next_basis[2]]
            print "adding EIM basis", len(self._basis), next_basis, "\tscaling", new_basis[next_basis[2]]
            f0 = self._basis[0][2].copy(deepcopy=True)
            f0.vector()[:] = new_basis
            self._basis.append(next_basis[2:4] + [f0])
            # print "WWWWWWWWW", len(sample_dict), next_basis[1], next_basis[0]
            sample_dict.pop(next_basis[1])
        self.update_lagrange()

    def update_lagrange(self):
        # evaluate Lagrange coefficient matrix C
        x = [self._coordinates[bm[0]] for bm in self._basis]
        x = np.vstack(x)
        mis, L = get_lagrange_polys(x, self._lagrange_p)
        C, R = L[0], L[1]
        self._mis, self._C = mis, C

    def __getitem__(self, item):
        return self._basis[item]

    def __call__(self, y):
        # evaluate interpolation in current basis
        Y = evaluate_polys(y, mis=self._mis, C=self._C)
        v = [ym * bm[2].vector().array() for ym, bm in zip(Y, self._basis)]
        v = sum(v)
        return v

    def evaluate_residual(self, y):
        # evaluate interpolation residual at y wrt exact f
        r = self._f(y) - self(y)
        return r


def get_lagrange_polys(x, degree, full_set=False):
    # get index set for multivariate polys
    d = x.shape[1]
    if full_set:
        mis = MultiindexSet.createFullTensorSet(d, degree)
    else:
        mis = MultiindexSet.createCompleteOrderSet(d, degree, True)

    # evaluate polys at interpolation points
    X = evaluate_polys(x, mis)

    # solve linear system
    # NOTE: solver with sparsity would be nice, e.g. SPGL1
    C = np.linalg.lstsq(X, np.identity(X.shape[0]))
    return mis, C


def evaluate_polys(x, mis, C=None):
    degree = mis.arr.max()
    # evaluate monomials for each point
    M = {}
    for d in range(degree+1):
        M[d] = np.power(x, d)

    # multiply according to multiindices in mis
    P = []
    for mi in mis:
        try:
            Pmi = np.vstack([M[d][:,i] for i, d in enumerate(mi)])
        except:
            # handle case of just one point, this really is a flaw of numpy...
            Pmi = np.vstack([M[d][i] for i, d in enumerate(mi)])
        Pmi = np.prod(Pmi, axis=0)
        P.append(Pmi)
    P = np.vstack(P)
    if C is not None:
        P = np.dot(C, P)
    return P
