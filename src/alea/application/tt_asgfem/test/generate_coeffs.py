from __future__ import division
import argparse
import logging
import logging.config

# alea imports
from alea.application.egsz.sample_problems2 import SampleProblem
from alea.application.egsz.sample_domains import SampleDomain
# from alea.application.egsz.multi_operator2 import MultiOperator
from alea.utils.plothelper import PlotHelper

from dolfin import plot, refine, Mesh, FunctionSpace, set_log_level, WARNING, INFO, ERROR, project, Function

# set FEniCS logging
set_log_level(logging.WARNING)
fenics_logger = logging.getLogger("FFC")
fenics_logger.setLevel(logging.WARNING)
fenics_logger = logging.getLogger("UFL")
fenics_logger.setLevel(logging.WARNING)

# ###############
# A problem setup
# ###############

# parameters: coeff-type, decay-exp, gamma, freq-scale, freq-skip, rv-type, scale
#               problem-type, domain

parser = argparse.ArgumentParser()
# parser.add_argument("-ct", "--coeff-type", type=str, default="EF-square-cos",
parser.add_argument("-ct", "--coeff-type", type=str, default="cos",
                    help="coefficient type")
parser.add_argument("-at", "--amp-type", type=str, default="decay-inf",
                    help="amplification type")
parser.add_argument("-N", type=int, default=30,
                    help="number of terms for amp-type constant")
parser.add_argument("-M", "--M-terms", type=int, default=30,
                    help="terms in realisation")
parser.add_argument("-de", "--decay-exp", type=float, default=2.0,
                    help="decay exponent")
parser.add_argument("-g", "--gamma", type=float, default=0.9,
                    help="gamma")
parser.add_argument("-fk", "--freq-skip", type=int, default=0,
                    help="frequency skip")
parser.add_argument("-fs", "--freq-scale", type=float, default=1.0,
                    help="frequency scaling")
parser.add_argument("-s", "--scale", type=float, default=1.0,
                    help="scaling")
parser.add_argument("-rv", "--rv-type", type=str, default="uniform",
                    help="RV type")
parser.add_argument("-pt", "--problem-type", type=int, default=2,
                    help="problem type")
parser.add_argument("-d", "--domain", type=str, default="square",
                    help="domain")
parser.add_argument("-nr", "--number-realisations", type=int, default=20,
                    help="number realisations")
parser.add_argument("-v", "--variance", action="store_true", default=False,
                    help="evaluate solution and variance wrt mean field")
parser.add_argument("-np", "--no-plot", action="store_true", default=False,
                    help="suppress plot")
parser.add_argument("-ref", "--mesh-refinements", type=int, default=2,
                    help="initial mesh refinements")
args = parser.parse_args()
print "========== PARAMETERS:", vars(args)

# setup boundary conditions and pde
mesh, boundaries, dim = SampleDomain.setupDomain(args.domain, initial_mesh_N=10)
mesh = SampleProblem.setupMesh(mesh, num_refine=args.mesh_refinements)

# define coefficient field
# coeff_field = SampleProblem.setupCF(args.coeff_type, decayexp=args.decay_exp, gamma=args.gamma, freqscale=args.freq_scale,
#                                                             freqskip=args.freq_skip, rvtype=args.rv_type, scale=args.scale)
coeff_field = SampleProblem.setupCF2(args.coeff_type, amptype=args.amp_type, decayexp=args.decay_exp, gamma=args.gamma, N=args.N,
                                     freqscale=args.freq_scale, freqskip=args.freq_skip, rvtype=args.rv_type, scale=args.scale)

# setup boundary conditions and pde
pde, Dirichlet_boundary, uD, Neumann_boundary, g, f = SampleProblem.setupPDE(args.problem_type, args.domain, 0, boundaries, coeff_field)

# # define multioperator
# A = MultiOperator(coeff_field, pde.assemble_operator, pde.assemble_operator_inner_dofs)


# ###################################
# B evaluate coefficient realisations
# ###################################

# prepare expansion terms
CG = FunctionSpace(mesh, 'CG', 1)
F0 = project(coeff_field.mean_func, CG).vector().array()
Fm = [project(coeff_field[m][0], CG).vector().array() for m in range(args.M_terms)]

# decay coefficient function
ampfunc = SampleProblem.get_ampfunc(args.amp_type, args.decay_exp, args.gamma, args.N)
print "ampfunc", [ampfunc(m) for m in range(args.M_terms)]

# sample realisations
ph = PlotHelper()
A = []
for nr in range(args.number_realisations):
    # if nr in [0,1]:
    #     sample_rvs = [-1 + 2 * nr] * args.M_terms
    # else:
    sample_rvs = coeff_field.sample_rvs()
    # print "sample %i = %s" % (nr, [sample_rvs[m] for m in range(args.M_terms)])
    af = Function(CG)
    af.vector()[:] = F0 + sum([ampfunc(m) * sample_rvs[m] * Fm[m] for m in range(args.M_terms)])
    A.append(af)
    if not args.no_plot:
        ph["coeff"].plot(af, interactive=True)
