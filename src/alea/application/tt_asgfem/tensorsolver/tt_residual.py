# region Imports
from __future__ import division
import numpy as np
from scipy.special import comb
from scipy.sparse.linalg import spsolve
from scipy.sparse import csr_matrix
import tt
import math
import cPickle as Pickle
import itertools as iter
from copy import deepcopy
from alea.polyquad.polynomials import StochasticHermitePolynomials
from alea.utils.tictoc import TicToc
# from alea.utils.plothelper import PlotHelper
from alea.application.bayes.Bayes_util.ForwardOperator.tt_forward_operator_util import calculate_stiffness_matrix
import itertools as iter
import warnings
import scipy.sparse as sps
import gc
from joblib import Parallel, delayed
import multiprocessing as mp
from alea.application.egsz.affine_field import AffineField
from alea.application.tt_asgfem.tensorsolver.tt_lognormal import sample_cont_coeff, sample_lognormal_tt
from dolfin import cells, Function, FunctionSpace, DOLFIN_VERSION_MAJOR, TrialFunction, \
    TestFunction, inner, dx, parameters, assemble, as_backend_type, nabla_div, nabla_grad, FacetNormal, CellSize,\
    Expression, jump, dS, avg, dot, project
from alea.application.tt_asgfem.tensorsolver.tt_lognormal import fully_disc_first_core
from alea.application.tt_asgfem.tensorsolver.tt_util import frob_norm
from dolfin import DOLFIN_VERSION_MAJOR
from decimal import *
new_dolfin = DOLFIN_VERSION_MAJOR > 1
if new_dolfin:
    from dolfin import FiniteElement
# endregion

# region preliminary functions

# region def factorial of nominator and denominator of a fraction


def frac_fac(a, b):
    if a > b:
        return np.prod([lia for lia in range(b+1, a+1)], dtype=np.float64)
    elif a < b:
        return np.prod([lia**(-1) for lia in range(a+1, b+1)], dtype=np.float64)
    else:
        return 1
# endregion
# region def sigma


def sigma(theta, rho, alpha_m):
    return np.exp(theta * rho * alpha_m)
# endregion
# region def sigma_prime


def sigma_prime(theta, rho, alpha_m):
    return sigma(theta, rho, alpha_m)/(np.sqrt(2-sigma(theta, rho, alpha_m)**2))
# endregion
# region def c sigma


def c_sigma(_sigma):
    return (_sigma * (np.sqrt(2 - _sigma ** 2))) ** (-1)
# endregion
# region def tau


def tau(_move, alpha_m, x):
    return np.exp(-_move*alpha_m) * x
# endregion
# region def transformed hermite polynomials


def transformed_herms(nu, x):
    from alea.polyquad.polynomials import StochasticHermitePolynomials

    herms = StochasticHermitePolynomials(0, 1, normalised=True)
    return herms.eval(nu, x, all_degrees=False)
# endregion
# region def zeta


def zeta(theta, rho, alpha_m, x):
    retval = (sigma(theta, rho, alpha_m)) ** (-1) * np.exp(-0.5 * ((sigma(theta, rho, alpha_m) ** (-2)) - 1) * x ** 2)
    return retval
# endregion
# region def zeta^2


def zeta_sq(theta, rho, alpha_m, x):
    retval = zeta(theta, rho, alpha_m, x)**2
    return retval
# endregion
# region def base change constant


def base_change_constant(mu, k, sigma1, sigma2):
    from math import factorial as fac
    if mu > 12:
        fac_mu = Decimal(math.factorial(mu))
        fac_mu2 = Decimal(math.factorial(mu-2*k))
        fac_k = math.factorial(k)
        retval = float(Decimal.sqrt(fac_mu) * Decimal.sqrt(fac_mu2)**(-1))
        retval *= ((sigma2**2 - sigma1**2)**k * sigma2**(mu-2*k)) * \
                  (2**k *fac_k*sigma1**mu)**(-1)
        return float(retval)
    try:
        retval =(np.sqrt(fac(mu))*(sigma2**2 - sigma1**2)**k * sigma2**(mu-2*k)) * \
                (2**k * np.sqrt(fac(mu-2*k))*fac(k)*sigma1**mu)**(-1)
    except AttributeError:
        print("type is not int but {} for \n mu={} \n k={}\n sigma1={}\n sigma2={}".format("long", mu, k, sigma1,
                                                                                           sigma2))
        exit()
    return retval
# endregion
# region def create matrix(vector) hermite_l zeta^2 d gamma


def create_vec(n, theta, rho, alpha_m):
    mat_alt = np.zeros(n)

    def sigma1():
        return sigma(theta, rho, alpha_m)

    def sigma2():
        return sigma_prime(theta, rho, alpha_m)

    for lia in range(n):
        if lia % 2 == 1:
            mat_alt[lia] = 0
        else:
            for _k in range(int(np.floor(lia * 0.5))+1):
                mat_alt[lia] += base_change_constant(lia, _k, sigma1(), sigma2()) * \
                                 c_sigma(sigma1())
    # print mat
    # print mat_alt
    # print c_sigma(sigma1())
    return mat_alt
# endregion
# region def create matrix hermite_l hermite_k zeta^2 d gamma


def create_mat(n, theta, rho, alpha_m):
    mat = np.zeros((n, n))

    def sigma1():
        return sigma(theta, rho, alpha_m)

    def sigma2():
        return sigma_prime(theta, rho, alpha_m)
    for lia in range(n):
        for lic in range(n):
            for _k in range(int(np.floor(lia * 0.5))+1):
                for _k_prime in range(int(np.floor(lic * 0.5))+1):
                    if lia - 2 * _k == lic - 2 * _k_prime:
                        mat[lia, lic] += base_change_constant(lia, _k, sigma1(), sigma2()) * \
                                         base_change_constant(lic, _k_prime, sigma1(), sigma2()) * \
                                         c_sigma(sigma1())

    return mat
# endregion
# region def core multiply


def core_mul(mat, _core):
    for k in range(_core.shape[0]):
        for k1 in range(_core.shape[2]):
            _core[k, :, k1] = np.dot(_core[k, :, k1], mat[:_core.shape[1], :_core.shape[1]])
    return _core
# endregion
# region def base change core


def base_change_core(core, alpha_m, theta=0, rho=1):
    assert(len(core.shape) == 3)
    # print("create matrix of size {} ^2".format(core.shape[1]))
    mat = create_mat(core.shape[1], theta, rho, alpha_m)
    return core_mul(mat, core), mat
# endregion
# region def triple hermite coefficient


def triple_hermite_coef(nu, mu, xi):
    """
    calculates the coefficient of Hermite polynomial expansion when used as triple product
    :param nu: coefficient index
    :param mu: solution index
    :param xi: contraction index
    :return: triple hermite coefficient
    """
    if (nu + mu - xi) % 2 != 0:
        return 0                                    # hermite indices form an odd sum
    if np.abs(nu - mu) > xi:
        return 0                                    # hermite indices do not couple
    if xi > nu + mu:
        return 0                                    # again no coupling
    # if xi > min(nu, mu):                            # new: check for correct deterministic estimator
    #     return 0
    eta = (nu + mu - xi) * 0.5                      # return coefficient as in
    # nodes, weights = np.polynomial.hermite_e.hermegauss(30)
    # assert (len(nodes) == len(weights))
    # quadrature = np.sum([w * transformed_herms(nu, x) * transformed_herms(mu, x) * transformed_herms(xi, x)
    #                      for (x, w) in zip(nodes, weights)])
    # quadrature *= (np.sqrt(2 * np.pi)) ** (-1)
    # return quadrature
    # print("compute coef for nu={}, mu={}, xi={}".format(nu, mu, xi))
    if nu + mu > 12:
        nu_fac_sqrt = Decimal.sqrt(Decimal(np.math.factorial(nu)))
        mu_fac_sqrt = Decimal.sqrt(Decimal(np.math.factorial(mu)))
        xi_fac_sqrt = Decimal.sqrt(Decimal(np.math.factorial(xi)))
        eta_fac = Decimal(np.math.factorial(eta))
        nu_eta_fac = Decimal(np.math.factorial(nu - eta))
        mu_eta_fac = Decimal(np.math.factorial(mu - eta))
        retval = nu_fac_sqrt / eta_fac
        retval *= mu_fac_sqrt / nu_eta_fac
        retval *= xi_fac_sqrt / mu_eta_fac
        return float(retval)
    return (math.sqrt(np.math.factorial(nu) * np.math.factorial(mu) * np.math.factorial(xi))
            * (np.math.factorial(eta) * np.math.factorial(nu - eta) * np.math.factorial(mu - eta)) ** (-1))

# endregion
# region def double hermite coefficient


def double_hermite_coef(nu, mu, xi):
    """
    calculates the coefficient of Hermite polynomial expansion when used as double product
    :param nu: coefficient index
    :param mu: solution index
    :param xi: contraction index
    :return: triple hermite coefficient
    """
    # if zeros:
    #     if nu + mu - 2*xi == 0:
    #         return (math.sqrt(np.math.factorial(nu+mu-2*xi) * np.math.factorial(nu) * np.math.factorial(mu))
    #                 * (np.math.factorial(xi) * np.math.factorial(nu - xi) * np.math.factorial(mu - xi)) ** (-1))
    #     return 0
    return (math.sqrt(np.math.factorial(nu + mu - 2 * xi) * np.math.factorial(nu) * np.math.factorial(mu))
            * (np.math.factorial(xi) * np.math.factorial(nu - xi) * np.math.factorial(mu - xi)) ** (-1))
# endregion
# region def create triple hermite coefficient tensor


def create_triple_hermite_coef_tensor(max_nu, max_mu):
    """
    Creates an order three tensor containing the the variable kappa as the coefficients of triple hermite polynomials.
    Needed to contract later over second dimension
    :param max_nu: maximal number of coefficients from tensor
    :param max_mu: maximal number of coefficients from solution tensor
    :return: order 3 tensor as np.array
    """
    # TODO: catch non integer numbers
    retval = np.zeros((max_nu, max_nu + max_mu, max_mu), dtype=np.float64)
    # TODO: check if zeros is the correct choice here. May contains numbers only close to zero
    for _lia in range(max_nu):
        for _lic in range(max_mu):
            for _lix in range(min(_lia, _lic)+1):
                retval[_lia, _lia + _lic - 2*_lix, _lic] = triple_hermite_coef(_lia, _lic, _lia+_lic-2*_lix)
    # for _lia in range(max_nu):
        # for _lic in range(max_mu):
            # for _lix in range(retval.shape[1]):
                # print("HermCoef [{}, {}, {}] = {}".format(_lia, _lix, _lic, retval[_lia, _lix, _lic]))
    return retval
# endregion
# region def create double hermite coefficient tensor


def create_double_hermite_coef_tensor(max_nu, max_mu):
    """
    Creates an order three tensor containing the the variable \bar{kappa} as the coefficients of double hermite
    polynomials. Needed to contract later over second dimension
    :param max_nu: maximal number of coefficients from tensor
    :param max_mu: maximal number of coefficients from solution tensor
    :return: order 3 tensor as np.array
    """
    # TODO: catch non integer numbers
    retval = np.zeros((max_nu, max_nu + max_mu, max_mu), dtype=np.float64)
    # TODO: check if zeros is the correct choice here. May contains numbers only close to zero
    for _lia in range(max_nu):
        for _lic in range(max_mu):
            for _lix in range(min(_lia, _lic)+1):
                retval[_lia, _lia + _lic - 2*_lix, _lic] += triple_hermite_coef(_lia, _lic, _lix)
    return retval
# endregion
# region def create triple hermite coefficient tensor tail


def create_triple_hermite_coef_tensor_tail(max_nu, max_mu):
    """
    Creates an order three tensor containing the the variable kappa as the coefficients of triple hermite polynomials.
    Needed to contract later over second dimension
    :param max_nu: maximal number of coefficients from tensor
    :param max_mu: maximal number of coefficients from solution tensor
    :return: order 3 tensor as np.array
    """
    # TODO: catch non integer numbers
    max_eta = np.min((max_nu, max_mu))
    retval = np.zeros((max_nu, max_eta, max_mu), dtype=np.float64)
    # TODO: check if zeros is the correct choice here. May contains numbers only close to zero
    for lia in range(max_nu):
        for lic in range(max_mu):
            for lix in range(max_eta):
                if lia + lic - 2*lix < 0:
                    continue
                retval[lia, lix, lic] = triple_hermite_coef(lia, lic, lia+max_mu-2*lix)
    return retval, inner
# endregion
# region def create triple hermite tensor


def create_triple_hermite_tensor(max_nu, max_mu, y):
    """
    Creates an order three tensor containing the the variable kappa as the coefficients of triple hermite polynomials.
    Needed to contract later over second dimension
    :param max_nu: maximal number of coefficients from tensor
    :param max_mu: maximal number of coefficients from solution tensor
    :param y: stochastic variable
    :return: order 3 tensor as np.array
    """
    # TODO: catch non integer numbers
    max_eta = np.min((max_nu, max_mu))
    retval = np.zeros((max_nu, max_eta, max_nu), dtype=np.float64)
    # TODO: check if zeros is the correct choice here. May contains numbers only close to zero
    poly = StochasticHermitePolynomials(0.0, 1.0, normalised=True)
    for lia in range(max_nu):
        for lic in range(max_mu):
            for lix in range(max_eta):
                if lia+lic-2*lix < 0:
                    retval[lia, lix, lic] = 0
                    continue
                if lix > lia or lix > lic:
                    retval[lia, lix, lic] = 0
                    continue
                retval[lia, lix, lic] = poly.eval(lia+lic-2*lix, y, all_degrees=False)
    return retval
# endregion
# endregion


class TTResidual(object):                           # general class object for the tensor train residual
    # region def __init__
    def __init__(self, coeff_field, solution, rhs, bmax, femdegree, lognormal=True, mesh=None, af_type="EF-square-cos",
                 amp_type="decay-inf", decay_exp_rate=2, sgfem_gamma=0.9, freq_scale=1.0, freq_skip=0, scale=1.0,
                 theta=0, rho=1, cache=None, order4_stiffness_tensor=None, order4_volume_tensor_list=None):
        if type(coeff_field).__module__ == np.__name__:
            self.coeff_field = coeff_field          # coefficient field is given as numpy array
        elif isinstance(coeff_field, tt.vector):
            #                                       # coefficient field is given as tt.vector -> convert to list of com.
            self.coeff_field = tt.vector.to_list(coeff_field)
        elif type(coeff_field) == list:
            self.coeff_field = coeff_field
        else:                                       # raise TypeError
            raise TypeError("unknown Type: coeff_field:{}".format(type(coeff_field)))
        if type(solution).__module__ == np.__name__ or isinstance(solution, list):
            self.solution = solution                # solution is given as numpy array
        elif isinstance(solution, tt.vector):
            #                                       # solution is given as tt.vector -> convert to list of components
            self.solution = tt.vector.to_list(solution)
        else:                                       # raise TypeError
            raise TypeError("unknown Type: solution:{}".format(type(solution)))
        if type(rhs).__module__ == np.__name__ or isinstance(rhs, list):
            self.rhs = rhs                          # rhs is given as numpy array
        elif isinstance(rhs, tt.vector):
            self.rhs = tt.vector.to_list(rhs)       # rhs is given as tt.vector -> convert to list of components
        elif isinstance(rhs, float):
            self.rhs = rhs                          # rhs is given as float value
        else:                                       # raise TypeError
            raise TypeError("unknown Type: rhs:{}".format(type(rhs)))
        if isinstance(lognormal, bool):
            self.lognormal = lognormal              # lognormal value to switch between affine and log-normal case
        self.mesh = mesh
        self.af_type = af_type
        self.amp_type = amp_type
        self.decay_exp_rate = decay_exp_rate
        self.sgfem_gamma = sgfem_gamma
        self.freq_scale = freq_scale
        self.freq_skip = freq_skip
        self.scale = scale
        self.theta = theta
        self.rho = rho
        self.bmax = bmax
        self.cache = cache
        self.functionSpace = FunctionSpace(self.mesh, 'CG', femdegree)
        self.order4_stiffness_tensor = order4_stiffness_tensor
        self.order4_volume_tensor_list = order4_volume_tensor_list
    # endregion

    def get_cache(self):
        return self.cache

    # region def check_triple_hermite_integral
    def check_triple_hermite_integral(self, nu, mu, xi, value, theta, rho, alpha):
        from scipy.integrate import quad

        quadresult = quad(lambda x: transformed_herms(nu, x) *
                                    transformed_herms(mu, x) *
                                    transformed_herms(xi, x) *
                                    np.exp(-0.5*x**2)
                          , -np.inf, np.inf)
        integral, err = quadresult[0], quadresult[1]
        integral *= (np.sqrt(2 * np.pi)) ** (-1)

        nodes, weights = np.polynomial.hermite_e.hermegauss(30)
        assert(len(nodes) == len(weights))
        quadrature = np.sum([w*transformed_herms(nu, x)*transformed_herms(mu, x)*transformed_herms(xi, x)
                             for (x, w) in zip(nodes, weights)])
        quadrature *= (np.sqrt(2 * np.pi)) ** (-1)

        coef = triple_hermite_coef(nu, mu, xi)
        if np.abs(value - integral) > err*5 or np.abs(quadrature - value) > 1e-10:
            print("results of kappa and the true integral or the quadrature result are not the same")
            print("     kappa{0:13} {1}".format(":", value))
            print("     int_R H_{0} H_{1} H_{2}{3:1} {4}".format(mu, nu, xi, "=", integral))
            print("     quad{0:14} {1}".format("=", quadrature))
            print("     coef{0:14} {1}".format("=", coef))
            print("         error of integration = {}".format(err))
    # endregion

    # region def Create components
    def _create_components(self, _set="all"):

        if len(self.coeff_field) < len(self.solution) - 1:
            raise ValueError("Coefficient field must have more or equally many TT components as the solution object")
        if _set != "all" and _set != "inner" and _set != "zeros":
            raise ValueError("Set is unknown")
        res = []                                    # create return value
        # inner_list = []                           # index set to represent those indices, inside the solution domain
        m = len(self.coeff_field)
        # component = 0
        q = np.max([core.shape[1] for core in self.coeff_field])
        d = np.max([core.shape[1] for core in self.solution[1:]])
        full_coef_ten = create_triple_hermite_coef_tensor(q, d)

        # region check triple hermite coefficient
        # for nu in range(full_coef_ten.shape[0]):
        #     for mu in range(full_coef_ten.shape[2]):
        #         for xi in range(full_coef_ten.shape[1]):
        #             self.check_triple_hermite_integral(nu, mu, xi, full_coef_ten[nu, xi, mu], self.theta, self.rho, self.bmax[0])
        # endregion

        for lia in range(m-1, -1, -1):              # loop through all component
            # print("calculate component : {}/{}".format(len(self.solution)-lia, len(self.solution)-1))

            if lia+1 >= len(self.solution):         # there is no corresponding solution component, only coefficient
                if _set == "inner":
                    res.insert(0, self.coeff_field[lia][:, 0, :])
                else:
                    res.insert(0, self.coeff_field[lia])
                continue
            # k_prime, k = self.coeff_field[lia].shape[0], self.solution[lia].shape[0]
            q, d = self.coeff_field[lia].shape[1], self.solution[lia+1].shape[1]
            # k1_prime, k1 = self.coeff_field[lia].shape[2], self.solution[lia].shape[2]
            # component = np.zeros((k, k_prime, max(d, q), k1_prime, k1), dtype=np.float32)
            # print("  create hermite tensor")
            # hermite_ten = create_triple_hermite_tensor(n, k, y[lia-1])
            # print("    hermite_ten shape= {}".format(hermite_ten.shape))
            # print("  create coef tensor (coef, sol) = ({}, {})".format(q, d))
            # coef_ten, inner, zeros = create_triple_hermite_coef_tensor(q, d)
            # print coef_ten
            # print inner

            with TicToc(sec_key="TT-residual create component", key="create triple hermite coef tensor", active=True,
                        do_print=False):
                coef_ten = full_coef_ten[:q, :, :d]
                # coef_ten = coef_ten[:, :2*self.solution[lia].shape[1]+1, :]
            # with TicToc(key="  TT-residual-create_components: base change core", active=True,
            #             do_print=False):
            #     coef_ten, self.cache = base_change_core(coef_ten, self.bmax[lia-1], self.theta, self.rho,
            # cache=self.cache)

            # assert(component.shape == (coef_ten.shape[0], hermite_ten.shape[2]))

            with TicToc(sec_key="TT-residual create component", key="restrictions", active=True,
                        do_print=False):
                if _set == "inner":

                    # print("create INNER !!!!!!!!!!!!")
                    # print("coef_ten shape : {}".format(coef_ten.shape))
                    # coef_ten = np.where(inner, coef_ten, 0)
                    coef_ten = coef_ten[:, :d, :]
                    # print("coef_ten shape after inner cutting: {}".format(coef_ten.shape))
                elif _set == "zeros":
                    # print("create Zeros !!!!!!!!!!!!")
                    # print zeros
                    # coef_ten = np.where(zeros, coef_ten, 0)
                    # print coef_ten
                    coef_ten_buff = np.zeros((coef_ten.shape[0], 1, coef_ten.shape[2]))
                    coef_ten_buff[:, 0, :] = coef_ten[:, 0, :]
                    coef_ten = coef_ten_buff

            with TicToc(sec_key="TT-residual create component", key="summations", active=True,
                        do_print=False):
                # core = np.zeros((self.solution[lia].shape[0], q, self.solution[lia].shape[2]))
                # core[:, :d, :] = self.solution[lia]

                component = np.einsum('ijk, lmj->ilmk', self.solution[lia+1], coef_ten)
                # print("    sol={} -> sol*Coef component shape= {}".format(self.solution[lia].shape, component.shape))
                # resulting tensor shape: [k_m', k_m, \eta, k_{m+1}, k_{m+1}']
                component = np.einsum('ijk, ajmc->iamck', self.coeff_field[lia], component)
                # component = np.reshape(component, (component.shape[0]*component.shape[1], component.shape[2],
                #                                    component.shape[3]*component.shape[4]))
                # print("    coef={} -> coef*sol*Coef*Herm component shape= {}".format(self.coeff_field[lia].shape,
                #                                                                      component.shape))

            res.insert(0, component)
        return res
    # endregion

    # region def evaluate resnorm
    def evaluate_resnorm(self, A, F, theta, rho, bmax):
        from alea.application.tt_asgfem.tensorsolver.tt_util import ttNormalize
        retval_dict = dict()
        W = self.solution
        if not isinstance(F, tt.vector):
            F = tt.vector.from_list(F)
        if not isinstance(self.solution, tt.vector):
            W = tt.vector.from_list(W)
        # print("ranks of W prior to F - and application of A")
        # print W.r
        with TicToc(sec_key="resnorm-error-estimator", key="compute residual", active=True, do_print=False):
            W = F - A.matvec(W)
            retval_dict["resnorm_alg"] = frob_norm(W)
        # print("ranks of W after application of A")
        # print W.r
        with TicToc(sec_key="resnorm-error-estimator", key="normalize", active=True, do_print=False):
            W = ttNormalize(W)
            retval_dict["resnorm_alg_norm"] = frob_norm(W)
        # print("ranks of W after normalize")
        # print W.r
        W = tt.vector.to_list(W)
        if False:
            return frob_norm(W)
        # region compute stiffness matrix
        with TicToc(sec_key="resnorm-error-estimator", key="compute stiffness matrix", active=True, do_print=False):
            stiff = calculate_stiffness_matrix(self.functionSpace, with_bc=True)
            # try:
            #     stiff_inv = np.linalg.inv(stiff.toarray())
            #     print("stiffness is invertible")
            # except Exception as ex:
            #     print("stiffness is not invertible")
            # stiff = calculate_stiffness_matrix(self.functionSpace, with_bc=True)
            # try:
            #     stiff_inv = np.linalg.inv(stiff.toarray())
            #     print("stiffness with bc is invertible")
            # except Exception as ex:
            #     print("stiffness with bc is not invertible")

            # TODO: Apply BC. May better incorporate the whole operator and let him handle that.
            # import scipy
            # stiff = scipy.sparse.eye(W[0].shape[1], W[0].shape[1])
        # endregion
        # print stiff.toarray()[0:20, 0:20]
        # exit()
        core = [1]
        for lia in range(len(W)-1, -1, -1):
            assert (len(W[lia].shape) == 3)         # every component has order 3
            if lia == len(W)-1:                     # we start at the last component
                assert(W[lia].shape[2] == 1)        # the last rank should be 1
                if lia == 0:                        # no stochastic dimensions
                    assert (W[lia].shape[0] == 1)   # hence first rank should be 1
                    return np.sqrt(np.dot(W[lia][0, :, 0], spsolve(stiff, W[lia][0, :, 0])))
                #                                   # create base change matrix
                mat = create_mat(W[lia].shape[1], theta, rho, bmax[lia - 1])
                mat_inv = np.linalg.inv(mat)        # invert it. # TODO: solve linear eq. system
                #  [k_M, mu, 0] x [mu, mu'] = [k_M, mu']
                core = np.dot(W[lia][:, :, 0], mat_inv)
                #  [k_M', mu', 0] x [mu', k_M] = [k_M, k_M']
                core = np.dot(W[lia][:, :, 0], core.T).T
                continue
            assert(len(core.shape) == 2)            # previous loop result is of order 2
            assert(core.shape[0] == core.shape[1])  # and symmetric
            #                                       # and of size of the rank of the current component
            assert(W[lia].shape[2] == core.shape[0])
            if lia > 0:                             # we are not at the first component
                #                                   # create base change matrix
                mat = create_mat(W[lia].shape[1], theta, rho, bmax[lia - 1])
                mat_inv = np.linalg.inv(mat)        # invert it. # TODO: solve linear eq. system
                # [k_m-1, mu, k_m] x [k_m, k_m'] = [k_m-1, mu, k_m']
                newcore = np.einsum('ijk, kl->ijl', W[lia], core)
                # [k_m-1, mu, k_m'] x [mu, mu'] = [k_m-1, mu', k_m']
                newcore = np.einsum('ijk, jl->ilk', newcore, mat_inv)
                # [k_m-1, mu', k_m'] x [k_m-1', mu', k_m'] = [k_m-1, k_m-1']
                core = np.einsum('ijk, mjk->im', newcore, W[lia])
            else:
                assert(W[0].shape[0] == 1)          # we reached the first component. first rank = 1, W[0].shape[1]=N
                retval = 0                          # init return value
                with TicToc(sec_key="resnorm-error-estimator", key="stiffness sum over rank", active=True,
                            do_print=False):
                    #                               # contract remainder onto first component
                    core = np.dot(W[0][0, :, :], core)
                    for k in range(W[0].shape[2]):  # sum over the remaining ranks
                        #                           # <R[k], S^{-1}((F-AW)[k])>
                        retval += np.dot(core[:, k], spsolve(stiff, W[0][0, :, k]))
                retval_dict["resnorm"] = np.sqrt(retval)
                return retval_dict
        if True:
            raise ValueError("Do not come here")
            return core[0, 0]
        new_W = []
        for lia, core in enumerate(W):
            if lia == 0:
                # print("append core {}".format(core.shape))
                new_W.append(core)
                continue
            # print("create mat for core shape {}".format(core.shape))
            mat = create_mat(core.shape[1], theta, rho, bmax[lia-1])
            # print("invert mat {}".format(mat.shape))
            mat_inv = np.linalg.inv(mat)
            # print("einsum with mat^-1 {}".format(mat_inv.shape))
            new_core = np.einsum('ijk, jm->imk', core, mat_inv.T)
            # print("append newcore {}".format(new_core.shape))
            new_W.append(new_core)
        # osel_norm = tt.vector.norm(tt.vector.from_list(new_W))
        own_norm = frob_norm(new_W)
        return own_norm
        # new_W_tt = tt.vector.from_list(new_W)
        # print new_W_tt
        # print("calculate norm")
        # retval = tt.vector.norm(new_W_tt)
        # print("return retval")
        # return retval

    # endregion

    # region def deterministic error estimator
    def eval_deterministic_error_estimator(self, af, bmax, theta, rho, femdegree=1, debug=False, assertions=True, 
                                           timing=True):
        return_sampled_estimators = True
        n_samples = 5*(mp.cpu_count()-1)
        # region pre-computations and definitions

        with TicToc(sec_key="det-error-estimator", key="pre-computations", active=timing, do_print=False):
            ncpu = int(math.floor(mp.cpu_count()))
            #                                           # create the affine field as used in the whole process

            # PH = PlotHelper()

            if not new_dolfin:
                #                                       # obtain all coordinates of degrees of freedom
                points = self.functionSpace.dofmap().tabulate_all_coordinates(self.mesh)
            else:
                points = self.functionSpace.tabulate_dof_coordinates()  # obtain all coordinates of degrees of freedom
            #                                           # reshape the coordinates to a vector
            points = points.reshape((-1, self.mesh.geometry().dim()))
            #                                           # initialise the rank vector
            ranks = [1] + [core.shape[0] for core in self.coeff_field] + [1]
            #                                           # initialise the dimension vector
            hdegs = [core.shape[1] for core in self.coeff_field]

            h = CellSize(self.mesh)
            DG0_space = FunctionSpace(self.mesh, 'DG', 0)
            DG0_dofs = dict([(c.index(), DG0_space.dofmap().cell_dofs(c.index())[0]) for c in cells(self.mesh)])
            #                                           # create discrete first core to use in TT core description
            nu = FacetNormal(self.mesh)
        # endregion

        # region create fully discrete first core
        with TicToc(sec_key="det-error-estimator", key="create discrete first core (det)", active=timing, 
                    do_print=False):
            core0 = fully_disc_first_core(self.coeff_field, af, points, ranks, hdegs, bmax, theta=theta, rho=rho)  # ,
            #                              # cg_mesh=self.mesh, femdegree=femdegree)
            coeff_field = deepcopy(self.coeff_field)
            coeff_field.insert(0, core0)
        # for lia in range(10):
        #     y = af.sample_rvs(len(hdegs))
        #     V1_ = af(points, y)
        #     V1_ = np.exp(V1_)
        #     from alea.application.tt_asgfem.tensorsolver.tt_lognormal import sample_lognormal_tt
        #     with TicToc(key="  TEST: Sample cont core at FEM DOFS", active=True, do_print=True):
        #         V3_ = sample_lognormal_tt(tt.vector.from_list(self.coeff_field), y, bmax, theta=theta, rho=rho)
        #     print("error simple: {}".format(np.linalg.norm(V1_ - V3_, ord=2)))

        # endregion

        # region def estimate residual error zero
        def estimate_residual_error_zero(sol_fun, coef_fun, mesh, with_volume, assemble_quad_degree=-1, with_rhs=True,
                                         DG0=None, _DG0_dofs=None, _fs=None, _h=None, _femdegree=None):
            if DG0 is None:
                DG0 = FunctionSpace(mesh, 'DG', 0)
            if _DG0_dofs is None:
                _DG0_dofs = dict([(_c.index(), DG0.dofmap().cell_dofs(c.index())[0]) for _c in cells(mesh)])
            _dg0 = TestFunction(DG0)

            # setup residual estimator forms
            if with_rhs:
                # print("+rhs")
                if _fs is None:
                    R_T = Expression("1", degree=1)
                else:
                    R_T = Function(_fs)
                    R_T.vector()[:] = np.ones(len(R_T.vector()[:]))
                    R_T = inner(R_T, R_T)

            else:
                raise AssertionError()
            res = (_h ** 2 * (_femdegree ** (-2))) * R_T * _dg0 * dx
            assemble_rhs = assemble(res, form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
            assemble_rhs = assemble_rhs.array()[_DG0_dofs.values()]
            # print("femdegree={}".format(femdegree))
            # print("h={}".format(h))
            # print("rhs: {}".format(assemble_rhs))
            # print("volume zero serial : {}".format(assemble_volume))
            # print("rhs zero: {}".format(assemble_rhs))
            return assemble_rhs

        # endregion

        # region obtain inner residual
        #                                           # contains only the stochastic components of order 5
        # ##### we do not need zeros anymore due to the change of implementation
        # with TicToc(key="  det_error_estimator: create zero residual", active=timing, do_print=False):
        #     _semi_discrete_res_zero = self._create_components(set="zeros")
        # with TicToc(key="  det_error_estimator: collapse zero res", active=timing, do_print=False):
        #     collapsed_res = collapse_tail(_semi_discrete_res_zero, _inner=len(self.solution)-2, zero=True)
        # #####
        with TicToc(sec_key="det-error-estimator", key="create inner residual", active=timing, do_print=False):
            _semi_discrete_res = self._create_components(_set="inner")

        with TicToc(sec_key="det-error-estimator", key="reshape inner residual", active=timing, do_print=False):
            for lia in range(len(_semi_discrete_res)-1, -1, -1):
                if lia >= len(self.solution)-1:
                    assert (not assertions or len(_semi_discrete_res[lia].shape) == 2)
                    if lia == 0:
                        raise ValueError("Residual must not have shape 2 for mode 0, i.e. no solution content")
                    continue
                else:
                    assert (not assertions or len(_semi_discrete_res[lia].shape) == 5)
                    if lia == 0:
                        _semi_discrete_res[lia] = np.transpose(_semi_discrete_res[lia], (0, 1, 2, 4, 3))
                        _semi_discrete_res[lia] = np.reshape(_semi_discrete_res[lia],
                                                             (_semi_discrete_res[lia].shape[0],
                                                              _semi_discrete_res[lia].shape[1],
                                                              _semi_discrete_res[lia].shape[2],
                                                              _semi_discrete_res[lia].shape[3] *
                                                              _semi_discrete_res[lia].shape[4]), order='F'
                                                             )
                    else:
                        _semi_discrete_res[lia] = np.transpose(_semi_discrete_res[lia], (0, 1, 2, 4, 3))
                        _semi_discrete_res[lia] = np.reshape(_semi_discrete_res[lia],
                                                             (_semi_discrete_res[lia].shape[0] *
                                                              _semi_discrete_res[lia].shape[1],
                                                              _semi_discrete_res[lia].shape[2],
                                                              _semi_discrete_res[lia].shape[3] *
                                                              _semi_discrete_res[lia].shape[4]), order='F'
                                                             )
        # endregion

        # if debug is True:
        #     for lia in range(100):
        #         y = np.random.randn(len(self.coeff_field))
        #         self.evaluate_product_coef_solution(y, af, bmax, theta, rho, femdegree, 'all')
        #     print("!!! residual evaluation passed !!!")

        # region collapse residual for zero component
        residual_zero_component_tensor = [1]
        with TicToc(sec_key="det-error-estimator", key="collapse res", active=timing, do_print=False):
            for lia in range(len(_semi_discrete_res)-1, -1, -1):
                if lia >= len(self.solution)-1:
                    assert (not assertions or len(_semi_discrete_res[lia].shape) == 2)
                    if lia == len(_semi_discrete_res)-1:
                        assert (not assertions or _semi_discrete_res[lia].shape[1] == 1)
                        residual_zero_component_tensor = _semi_discrete_res[lia][:, 0]
                    else:
                        residual_zero_component_tensor = np.dot(_semi_discrete_res[lia], residual_zero_component_tensor)
                    residual_zero_component_tensor *= c_sigma(sigma(theta, rho, bmax[lia]))
                else:

                    #                                   # obtain precomputed matrix M[l]=\int_\bbR H_l zeta^2 dga
                    # mat = create_vec(_semi_discrete_res[lia].shape[2], theta, rho, bmax[lia])
                    if lia > 0:
                        assert (not assertions or len(_semi_discrete_res[lia].shape) == 3)
                        mat = create_vec(_semi_discrete_res[lia].shape[1], theta, rho, bmax[lia])
                        intermediate_core = np.einsum('ijk, j', _semi_discrete_res[lia], mat)
                        residual_zero_component_tensor = np.dot(intermediate_core,
                                                                residual_zero_component_tensor)
                    else:
                        assert (not assertions or len(_semi_discrete_res[lia].shape) == 4)
                        mat = create_vec(_semi_discrete_res[lia].shape[2], theta, rho, bmax[lia])
                        intermediate_core = np.einsum('ijkl, k', _semi_discrete_res[lia], mat)
                        residual_zero_component_tensor = np.einsum('ijk, k->ij', intermediate_core,
                                                                   residual_zero_component_tensor)
                    # residual_zero_component_tensor *= c_sigma(sigma(theta, rho, bmax[lia]))

        # endregion
        # region Residual RHS

        u = Function(self.functionSpace)            # create function to store the FEM Function of the solution
        v = Function(self.functionSpace)            # create function to store the FEM Function of the coeff field
        with TicToc(sec_key="det-error-estimator", key="residual error zero-rhs", active=timing, do_print=False):
            residual_zero_component_rhs = estimate_residual_error_zero(u, v, self.mesh, False, with_rhs=True,
                                                                       _fs=self.functionSpace, DG0=DG0_space,
                                                                       _DG0_dofs=DG0_dofs, _h=h, _femdegree=femdegree)
            residual_zero_component_rhs *= np.prod([c_sigma(sigma(theta, rho, bmax[lia]))
                                                    for lia in range(len(self.solution)-1)])
        # print("rhs-residual: {}".format(residual_zero_component_rhs))
        # rhs_residual = Function(DG0_space)
        # rhs_residual.vector()[:] = residual_zero_component_rhs
        # PH["rhs-residual"].plot(rhs_residual, interactive=timing)

        # res = CellSize(self.mesh) * dg0 * dx
        # _h = assemble(res, form_compiler_parameters={'quadrature_degree': -1})
        # _h = _h.array()[DG0_dofs.values()]
        # _h_cell = np.array([c.h() for c in cells(self.mesh)])**2 / 4

        # print np.array(_h_cell)
        # print("max mesh diameter: {}".format(max(_h_cell)))
        # print("max cell diameter: {}".format(max(_h_cell)**2))
        # print("max rhs residual: {}".format(max(residual_zero_component_rhs)))

        # endregion
        # region Residual Zero

        assert (not assertions or len(residual_zero_component_tensor.shape) == 2)
        remainder = np.dot(coeff_field[0][0, :, :], residual_zero_component_tensor[:, :])
        global _global_objects
        with TicToc(sec_key="det-error-estimator", key="residual error zero-vector parallel", active=True,
                    do_print=False):
            C = Computer_estimator(self.mesh, self.functionSpace, DG0=DG0_space, fs=self.functionSpace,
                                   DG0_dofs=DG0_dofs, h=h, timing=True, femdegree=femdegree)
            _global_objects = C
            residual_zero_component = np.sum(Parallel(ncpu)
                                                     (delayed(det_estimator_zero_parallel)
                                                      (self.solution[0][0, :, k], remainder[:, k])
                                                      for k in range(self.solution[0].shape[2])), axis=0)
        # residual_zero_component = 0
        # with TicToc(sec_key="det-error-estimator", key="residual error zero-vector serial", active=timing,
        #             do_print=False):
        #     for k in range(self.solution[0].shape[2]):
        #         # print("FACTOR={}".format(factor))

        #         u.vector()[:] = np.ascontiguousarray(remainder[:, k])
        #         v.vector()[:] = np.ascontiguousarray(self.solution[0][0, :, k])
                # PH["u {}".format(k)].plot(u)
                # PH["v {}".format(k)].plot(v, interactive=timing#)

        #         residual_zero_component += estimate_residual_error_zero(v, u, self.mesh, True, with_rhs=False,
        #                                                                 _fs=self.functionSpace, DG0=DG0_space,
        #                                                                 _DG0_dofs=DG0_dofs)
        # print("error zero {}".format(np.linalg.norm(residual_zero_component - residual_zero_component_parallel)))
        # print("zero-residual: {}".format(residual_zero_component))
        # zero_residual = Function(DG0_space)
        # zero_residual.vector()[:] = residual_zero_component
        # PH["zero-residual"].plot(zero_residual)

        # endregion

        # region Mean as reference
        if False:
            # ################## mean sol and coef as reference

            u1 = Function(self.functionSpace)           # create function to store the FEM Function of the solution
            v1 = Function(self.functionSpace)           # create function to store the FEM Function of the coeff field
            sol = 1
            for lia in range(len(self.solution)-1, 0, -1):
                sol = np.dot(self.solution[lia][:, 0, :], sol)
            sol = np.dot(self.solution[0][0, :, :], sol)
            coef = 1
            for lia in range(len(coeff_field)-1, 0, -1):
                coef = np.dot(coeff_field[lia][:, 0, :], coef)
            coef = np.dot(coeff_field[0][0, :, :], coef)

            # ##################
            # calculate zero component of residual for the mean of solution and coefficient
            residual_zero_mean = estimate_residual_error_zero(v1, u1, self.mesh, False, with_rhs=True,
                                                              DG0=DG0_space, _DG0_dofs=DG0_dofs)

            for k in range(sol.shape[1]):
                for k_prime in range(coef.shape[1]):
                    if (k == 0 and k_prime == 0) or True:
                        u1.vector()[:] = np.ascontiguousarray(coef[:, k_prime])
                        v1.vector()[:] = np.ascontiguousarray(sol[:, k])
                    else:
                        u1.vector()[:] += np.ascontiguousarray(coef[:, k_prime])
                        v1.vector()[:] += np.ascontiguousarray(sol[:, k])
                    residual_zero_mean += estimate_residual_error_zero(v1, u1, self.mesh, True, with_rhs=False,
                                                                       DG0=DG0_space, _DG0_dofs=DG0_dofs)

            # ##################

            # PH["coef"].plot(u)
            # PH["sol"].plot(v)
            # PH["coef_mean"].plot(u1)
            # PH["sol_mean"].plot(v1, interactive=timing)
            # print("residual zero norm: {}".format(np.linalg.norm(residual_zero_component)))

            # print("residual zero mean norm: {}".format(np.linalg.norm(residual_zero_mean)))
        # endregion

        # region collapse tail of residual
        residual_sq_component_tensor = np.array([1])
        with TicToc(sec_key="det-error-estimator", key="collapse res", active=timing, do_print=False):
            for lia in range(len(_semi_discrete_res) - 1, -1, -1):
                # print("residual shape: {}".format(_semi_discrete_res[lia].shape))
                if lia >= len(self.solution)-1:
                    # print("{} > {}: index > len(sol)-1".format(lia, len(self.solution)-1))
                    assert (not assertions or len(_semi_discrete_res[lia].shape) == 2)
                    if lia == len(_semi_discrete_res)-1:
                        assert (not assertions or _semi_discrete_res[lia].shape[1] == 1)
                        #                           # [k_L, 0]
                        residual_sq_component_tensor = _semi_discrete_res[lia][:, 0]
                    else:
                        #                           # [k_m, k_m+1]  [k_m+1] = [k_m]
                        residual_sq_component_tensor = np.dot(_semi_discrete_res[lia], residual_sq_component_tensor)
                    #                               # [k_m] * \int_\R \zeta^2 d\gamma = c_\sigma
                    residual_sq_component_tensor *= c_sigma(sigma(theta, rho, bmax[lia]))
                elif lia == len(self.solution)-2 and lia > 0:
                    # print("lia == len(sol)-1")
                    assert (not assertions or len(_semi_discrete_res[lia].shape) == 3)
                    assert (not assertions or len(residual_sq_component_tensor.shape) == 1)
                    assert (not assertions or residual_sq_component_tensor.shape[0] == _semi_discrete_res[lia].shape[2])
                    #                               # [k_m, mu, k_m+1] x [k_m+1] = [k_m, mu]
                    residual_sq_component_tensor = np.einsum('ijk, k->ij', _semi_discrete_res[lia],
                                                             residual_sq_component_tensor)
                    mat = create_mat(_semi_discrete_res[lia].shape[1], theta, rho, bmax[lia])
                    #                               # [k_m, mu] x [mu, mu'] = [k_m, mu']
                    core2 = np.dot(residual_sq_component_tensor, mat)
                    core3 = np.dot(residual_sq_component_tensor, mat.T)
                    #                               # [k_m, mu'] x [k_m', mu'] = [k_m, k_m']
                    #residual_sq_component_tensor = np.dot(core2, residual_sq_component_tensor.T)
                    residual_sq_component_tensor = np.dot(core2, core3.T)
                else:
                    if lia > 0:
                        # print("lia>0")
                        assert (not assertions or len(_semi_discrete_res[lia].shape) == 3)
                        assert (not assertions or len(residual_sq_component_tensor.shape) == 2)

                        #                               # obtain matrix M[l,l']=\int_\bbR H_lH_l' zeta^2 dga
                        mat = create_mat(_semi_discrete_res[lia].shape[1], theta, rho, bmax[lia])
                        assert (not assertions or mat.shape[0] == mat.shape[1])
                        assert (not assertions or mat.shape[0] >= _semi_discrete_res[lia].shape[1])
                        #                           # [k_m', mu', k_m+1'] x [k_m+1, k_m+1'] = [k_m', mu', k_m+1]
                        residual_sq_component_tensor = np.einsum('ijk, lk->ijl', _semi_discrete_res[lia],
                                                                 residual_sq_component_tensor)
                        #                           # [k_m, mu, k_m+1] x [k_m', mu', k_m+1] = [k_m, k_m', mu, mu']
                        residual_sq_component_tensor = np.einsum('ijk, lmk->iljm', _semi_discrete_res[lia],
                                                                 residual_sq_component_tensor)
                        #                           # [k_m, k_m', mu, mu'] x [mu, mu'] = [k_m, k_m']
                        residual_sq_component_tensor = np.einsum('ijkl, kl-> ij', residual_sq_component_tensor, mat)
                    else:
                        # print("lia==0")
                        assert (not assertions or len(_semi_discrete_res[lia].shape) == 4)

                        mat = create_mat(_semi_discrete_res[lia].shape[2], theta, rho, bmax[lia])
                        assert (not assertions or mat.shape[0] == mat.shape[1])
                        assert (not assertions or mat.shape[0] >= _semi_discrete_res[lia].shape[2])
                        if len(residual_sq_component_tensor.shape) == 2:
                            #                       # [k1_c', k1_s', mu', k_m+1'] x [k_m+1, k_m+1'] =
                            #                                                               [k1_c', k1_s', mu', k_m+1]
                            residual_sq_component_tensor = np.einsum('aijk, lk->aijl', _semi_discrete_res[lia],
                                                                     residual_sq_component_tensor)
                            #                       # [k1_c, k1_s, mu, k_m+1] x [k1_c', k1_s', mu', k_m+1] =
                            #                                                     [k1_c, k1_s, k1_c', k1_s', mu, mu']
                            residual_sq_component_tensor = np.einsum('aijk, blmk->aibljm', _semi_discrete_res[lia],
                                                                     residual_sq_component_tensor)
                            #                       # [k1_c, k1_s, k1_c', k1_s', mu, mu'] x [mu, mu'] =
                            #                                                               [k1_c, k1_s, k1_c', k1_s']
                            residual_sq_component_tensor = np.einsum('aibljm, jm-> aibl', residual_sq_component_tensor,
                                                                     mat)
                        elif len(residual_sq_component_tensor.shape) == 1:
                            #                       # [k1_c', k1_s', mu', k_m+1'] x [k_m+1'] =
                            #                                                               [k1_c', k1_s', mu']
                            core = np.einsum('aijk, k->aij', _semi_discrete_res[lia],
                                                                     residual_sq_component_tensor)
                            #                       # [k1_c', k1_s', mu'] x M[mu, mu'] = [k1_c', k1_s', mu]
                            core2 = np.einsum('ijk, km->ijm', core, mat.T)
                            #                       # [k1_c, k1_s, mu] x [k1_c', k1_s', mu] =
                            #                                                     [k1_c, k1_s, k1_c', k1_s', mu, mu']
                            residual_sq_component_tensor = np.einsum('aij, blj->aibl', core,
                                                                     core2)
                        else:
                            raise ValueError(" this can not be reached")

        # endregion
        # region Residual square and jump component
        # residual_component = 0
        # residual_component_jump = 0
        # u = Function(self.functionSpace)            # create function to store the FEM Function of the solution
        # v = Function(self.functionSpace)            # create function to store the FEM Function of the coeff field
        # u1 = Function(self.functionSpace)           # create function to store the FEM Function of the solution
        # v1 = Function(self.functionSpace)           # create function to store the FEM Function of the coeff field
        # plot_sol1 = Function(fs)
        # plot_sol2 = Function(fs)
        # plot_coef1 = Function(fs)
        # plot_coef2 = Function(fs)
        if debug is True:
            assert (len(residual_sq_component_tensor.shape) == 4)
            assert (residual_sq_component_tensor.shape[0] == coeff_field[0].shape[2])
            assert (residual_sq_component_tensor.shape[2] == coeff_field[0].shape[2])
            assert (residual_sq_component_tensor.shape[1] == self.solution[0].shape[2])
            assert (residual_sq_component_tensor.shape[3] == self.solution[0].shape[2])
            assert (coeff_field[0].shape[0] == 1)
        # region compute residual in parallel. old version using 3 rank sums (atm only stable version)
        with TicToc(sec_key="det-error-estimator", key="contract remainder and coef", active=True, do_print=False):
            remainder = np.einsum('jk, klmn->jlmn', coeff_field[0][0, :, :], residual_sq_component_tensor[:, :, :, :])

        with TicToc(sec_key="det-error-estimator", key="residual error sq-vector parallel", active=True,
                    do_print=False):
            C = Computer_estimator(self.mesh, self.functionSpace, femdegree=femdegree, DG0=DG0_space,
                                   fs=self.functionSpace, DG0_dofs=DG0_dofs, h=h, timing=True)
            _global_objects = C
            residual_component = np.sum(Parallel(ncpu)
                                                (delayed(det_estimator_sq_parallel)
                                                 (self.solution[0][0, :, k],
                                                  remainder[:, k, k1_prime, k1],
                                                  self.solution[0][0, :, k1],
                                                  coeff_field[0][0, :, k1_prime])
                                                 for k, k1, k1_prime in iter.product(range(self.solution[0].shape[2]),
                                                                                     range(self.solution[0].shape[2]),
                                                                                     range(coeff_field[0].shape[2]))),
                                        axis=0)
        # endregion
        # region compute residual with order 4 stiffness tensor parallel
        if False:
            residual_component_order4 = np.zeros(len(residual_component))
            with TicToc(sec_key="det-error-estimator",
                        key="residual error sq (4-tensor, parallel)",
                        active=True, do_print=False):
                for cell in range(len(self.order4_volume_tensor_list)):
                    print("compute for cell {}".format(cell))

                    with TicToc(sec_key="det-error-estimator",
                                key="residual error sq (4-tensor, parallel) - reduce coef",
                                active=True, do_print=False):
                        red_rem = np.zeros((len(self.order4_volume_tensor_list[cell]), remainder.shape[1],
                                            remainder.shape[3]))
                        for lia, (key, _) in enumerate(self.order4_volume_tensor_list[cell]):
                            print("  dofs: {}".format(key))
                            red_rem[lia, :, :] = np.sum([remainder[key[0], :, k, :] * coeff_field[0][0, key[1], k]
                                                         for k in range(coeff_field[0].shape[2])], axis=0)
                    with TicToc(sec_key="sto-error-estimator",
                                key="residual error (4-tensor, parallel) - init worker",
                                active=True, do_print=False):
                        C = Computer_tensor(self.order4_volume_tensor_list[cell])
                        _global_objects = C
                    with TicToc(sec_key="sto-error-estimator",
                                key="residual error (4-tensor, parallel) - parallel assembly",
                                active=True, do_print=False):
                        residual_component_order4[cell] = np.sum(Parallel(mp.cpu_count())
                                                                 (delayed(sto_estimator_parallel_order4)
                                                                  (self.solution[0][0, :, k],
                                                                   self.solution[0][0, :, k1],
                                                                   red_rem[:, k, k1])
                                                                  for k, k1 in
                                                                  iter.product(range(self.solution[0].shape[2]),
                                                                               range(self.solution[0].shape[2]))))
                print("!!!!!!!!!!!!!! det residual difference with order 4 tensor approach: "
                      "\n   {}".format(np.linalg.norm(residual_component -
                                                      residual_component_order4) /
                                       np.linalg.norm(residual_component)))
                print(residual_component)
                print(residual_component_order4)
                exit()

        # endregion
        # region compute residual with projection of product -> inaccurate
        if False:
            with TicToc(sec_key="det-error-estimator", key="residual error sq-vector parallel (new)", active=True,
                        do_print=False):
                with TicToc(sec_key="det-error-estimator", key="residual error sq-vector parallel (new) - contraction",
                            active=True, do_print=False):
                    remainder_new = np.zeros((remainder.shape[0], remainder.shape[1], remainder.shape[3]))
                    for p in range(remainder.shape[0]):
                        remainder_new[p, :, :] = np.einsum('i, aib->ab', coeff_field[0][0, p, :], remainder[p, :, :, :])
                with TicToc(sec_key="det-error-estimator", key="residual error sq-vector parallel (new) - summation",
                            active=True, do_print=False):
                    C = Computer_estimator(self.mesh, self.functionSpace, femdegree=femdegree, DG0=DG0_space,
                                           fs=self.functionSpace, DG0_dofs=DG0_dofs, h=h, timing=True)
                    _global_objects = C
                    residual_component_new = np.sum(Parallel(ncpu)
                                                (delayed(det_estimator_sq_parallel_new)
                                                 (self.solution[0][0, :, k],
                                                  remainder_new[:, k, k1],
                                                  self.solution[0][0, :, k1])
                                                 for k, k1 in iter.product(range(self.solution[0].shape[2]),
                                                                           range(self.solution[0].shape[2]))),
                                                axis=0)

            print("!!!!!!!!!!!!!! residual volume difference with new contraction: "
                  "\n        {}".format(np.linalg.norm(residual_component - residual_component_new)/
                                        np.linalg.norm(residual_component)))
        # endregion

        with TicToc(sec_key="det-error-estimator", key="residual error jump-vector parallel", active=True,
                    do_print=False):
            C = Computer_estimator(self.mesh, self.functionSpace, femdegree=femdegree, DG0=DG0_space,
                                   fs=self.functionSpace, DG0_dofs=DG0_dofs, h=h, timing=True)
            _global_objects = C
            residual_component_jump = np.sum(Parallel(ncpu)
                                                     (delayed(det_estimator_jump_parallel)
                                                      (self.solution[0][0, :, k],
                                                       remainder[:, k, k1_prime, k1],
                                                       self.solution[0][0, :, k1],
                                                       coeff_field[0][0, :, k1_prime])
                                                      for k, k1, k1_prime in iter.product(range(self.solution[0].shape[2]),
                                                                                          range(self.solution[0].shape[2]),
                                                                                          range(coeff_field[0].shape[2]))),
                                             axis=0)

        # for k, k1, k1_prime in iter.product(range(self.solution[0].shape[2]),
        #                                     range(self.solution[0].shape[2]),
        #                                     range(self.coeff_field[0].shape[2])):
        #     with TicToc(sec_key="det-error-estimator", key="residual error-estimate", active=timing,
        #                 do_print=False):
        #         residual_component += residual_sq(k, k1, k1_prime, remainder)

        #     with TicToc(sec_key="det-error-estimator", key="residual jump-estimate", active=timing,
        #                 do_print=False):
        #         residual_component_jump += residual_jump(k, k1, k1_prime, remainder)

        # print("residual sq error {}".format(np.linalg.norm(residual_component - residual_component_parallel)))
        # print("residual jump error {}".format(np.linalg.norm(residual_component_jump -
        # residual_component_jump_parallel)))

        # endregion

        # region Reference with mean
        if False:
            # ########################## sol and coef mean as reference

            u = Function(self.functionSpace)        # create function to store the FEM Function of the solution
            v = Function(self.functionSpace)        # create function to store the FEM Function of the coeff field
            u1 = Function(self.functionSpace)       # create function to store the FEM Function of the solution
            v1 = Function(self.functionSpace)       # create function to store the FEM Function of the coeff field
            for k in range(sol.shape[1]):
                for k_prime in range(coef.shape[1]):
                    for k1 in range(sol.shape[1]):
                        for k1_prime in range(coef.shape[1]):
                            if k == 0 and k_prime == 0 and k1 == 0 and k1_prime == 0:
                                # u.vector()[:] = np.ascontiguousarray(self.solution[0][0, :, k])
                                u.vector()[:] = np.ascontiguousarray(coef[:, k_prime])
                                v.vector()[:] = np.ascontiguousarray(sol[:, k])
                                u1.vector()[:] = np.ascontiguousarray(coef[:, k1_prime])
                                v1.vector()[:] = np.ascontiguousarray(sol[:, k1])
                                # PH['coef 1'].plot(u)
                                # PH['sol 1'].plot(v)
                                # PH['coef 2'].plot(u1)
                                # PH['sol 2'].plot(v1, interactive=timing)
                            else:
                                u.vector()[:] += np.ascontiguousarray(coef[:, k_prime])
                                v.vector()[:] += np.ascontiguousarray(sol[:, k])
                                u1.vector()[:] += np.ascontiguousarray(coef[:, k1_prime])
                                v1.vector()[:] += np.ascontiguousarray(sol[:, k1])
            # PH['coef 1 mean'].plot(u)
            # PH['sol 1 mean'].plot(v)
            # PH['coef 2 mean'].plot(u1)
            # PH['sol 2 mean'].plot(v1, interactive=timing)
            # residual_component_mean = estimate_residual_error(v, u, v1, u1, self.mesh, True, True)
            # print("res 2 mean norm : {}".format(np.linalg.norm(residual_component_mean)))
            # if True:
            #     exit()

            # ##############################
            # print the error estimators for every element
            dg0zero = Function(DG0_space)
            dg0zero.vector()[:] = residual_zero_component

            dg0zero_mean = Function(DG0_space)
            dg0zero_mean.vector()[:] = residual_zero_mean

            dg0 = Function(DG0_space)
            dg0.vector()[:] = residual_component

            dg0_jump = Function(DG0_space)
            dg0_jump.vector()[:] = residual_component_jump

            dg0_mean = Function(DG0_space)
            # dg0_mean.vector()[:] = residual_component_mean

            # PH["dg0 zero"].plot(dg0zero)
            # PH["dg0 zero mean"].plot(dg0zero_mean)
            # PH["dg0"].plot(dg0)
            # PH["dg0 mean"].plot(dg0_mean)
            # PH["dg0_jump"].plot(dg0_jump, interactive=timing)
        # endregion

        # region sample estimators

        if return_sampled_estimators is True:
            y_list = [af.sample_rvs(len(self.coeff_field)) for _ in range(n_samples)]
            with TicToc(sec_key="det-error-estimator", key="sample coefficient as pre allocation", active=True,
                        do_print=True):
                coef_at_y_list = [sample_cont_coeff(self.coeff_field, af, points, y, ranks, hdegs, bmax, theta, rho,
                                                    fs=self.functionSpace)
                                  for y in y_list]
            with TicToc(sec_key="det-error-estimator", key="sample solution", active=True,
                        do_print=True):
                sol_at_y_list = [sample_lognormal_tt(tt.vector.from_list(self.solution), y, bmax, theta=theta, rho=rho)
                                 for y in y_list]

            print("start parallel sampling of {} samples with {} cores".format(n_samples, ncpu))
            with TicToc(sec_key="det-error-estimator", key="sample mixed error parallel", active=True,
                        do_print=True):
                C = Computer_estimator(self.mesh, self.functionSpace, femdegree=femdegree, DG0=DG0_space,
                                       fs=self.functionSpace, DG0_dofs=DG0_dofs, h=h, timing=True, theta=theta, rho=rho,
                                       bxmax=bmax, affine_field=af, coeff_field=None, sol_ten=None,
                                       points=points)
                _global_objects = C
                mixed_y = np.sum(Parallel(ncpu)(delayed(sampled_det_estimator_mixed_parallel)(y,
                                                                                              coef_at_y_list[lia],
                                                                                              sol_at_y_list[lia])
                                                for lia, y in enumerate(y_list)),
                                 axis=0)

            with TicToc(sec_key="det-error-estimator", key="sample volume error parallel", active=True,
                        do_print=True):
                C = Computer_estimator(self.mesh, self.functionSpace, femdegree=femdegree, DG0=DG0_space,
                                       fs=self.functionSpace, DG0_dofs=DG0_dofs, h=h, timing=True, theta=theta, rho=rho,
                                       bxmax=bmax, affine_field=af, coeff_field=None, sol_ten=None,
                                       points=points)
                _global_objects = C
                volume_y = np.sum(Parallel(ncpu)(delayed(sampled_det_estimator_sq_parallel)(y,
                                                                                            coef_at_y_list[lia],
                                                                                            sol_at_y_list[lia])
                                                 for lia, y in enumerate(y_list)),
                                  axis=0)

            with TicToc(sec_key="det-error-estimator", key="sample jump error parallel", active=True,
                        do_print=True):
                C = Computer_estimator(self.mesh, self.functionSpace, femdegree=femdegree, DG0=DG0_space,
                                       fs=self.functionSpace, DG0_dofs=DG0_dofs, h=h, timing=True, theta=theta, rho=rho,
                                       bxmax=bmax, affine_field=af, coeff_field=None, sol_ten=None,
                                       nu=nu, points=points)
                _global_objects = C
                jump_y = np.sum(Parallel(ncpu)(delayed(sampled_det_estimator_jump_parallel)(y,
                                                                                            coef_at_y_list[lia],
                                                                                            sol_at_y_list[lia])
                                               for lia, y in enumerate(y_list)),
                                axis=0)

            with TicToc(sec_key="det-error-estimator", key="sample rhs error parallel", active=True,
                        do_print=True):
                C = Computer_estimator(self.mesh, self.functionSpace, femdegree=femdegree, DG0=DG0_space,
                                       fs=self.functionSpace, DG0_dofs=DG0_dofs, h=h, timing=True, theta=theta, rho=rho,
                                       bxmax=bmax, affine_field=af, coeff_field=None, sol_ten=None)
                _global_objects = C
                rhs_y = np.sum(Parallel(ncpu)(delayed(sampled_det_estimator_rhs_parallel)(y,
                                                                                          coef_at_y_list[lia],
                                                                                          sol_at_y_list[lia])
                                              for lia, y in enumerate(y_list)),
                               axis=0)

            rhs_y *= n_samples**(-1)
            mixed_y *= n_samples ** (-1)
            volume_y *= n_samples ** (-1)
            jump_y *= n_samples ** (-1)
        else:
            rhs_y = 0
            mixed_y = 0
            volume_y = 0
            jump_y = 0
        # endregion

        # region Compute estimators
        with TicToc(sec_key="det-error-estimator", key="final residual computation", active=timing, do_print=False):
            # _h = np.array([c.h() for c in cells(self.mesh)])
            sampled_eta_global = 0
            sampled_eta_local = 0
            sampled_zero_global = 0
            sampled_zero_local = 0
            sampled_rhs_global = 0
            sampled_rhs_local = 0
            sampled_sq_global = 0
            sampled_sq_local = 0
            sampled_jump_global = 0
            sampled_jump_local = 0

            eta_local = residual_zero_component_rhs + residual_zero_component + residual_component
            eta_local += residual_component_jump
            if return_sampled_estimators is True:

                sampled_eta_local = rhs_y + mixed_y + volume_y + jump_y

                # if not (np.linalg.norm(residual_zero_component - mixed_y) / np.linalg.norm(mixed_y) <= 1e-2):
                #     print("residual_mixed_component: \n{}\n".format(residual_zero_component))
                #     print("samples_mixed_component: \n{}\n".format(mixed_y))
                #
                # if not (np.linalg.norm(residual_zero_component_rhs - rhs_y) / np.linalg.norm(rhs_y) <= 1e-2):
                #     print("residual_rhs: \n{}\n".format(residual_zero_component_rhs))
                #     print("samples_rhs: \n{}\n".format(rhs_y))
                #
                # if not (np.linalg.norm(residual_component - volume_y) / np.linalg.norm(volume_y) <= 1e-2):
                #     print("residual_volume: \n{}\n".format(residual_component))
                #     print("samples_volume: \n{}\n".format(volume_y))
                #
                # if not (np.linalg.norm(residual_component_jump - jump_y) / np.linalg.norm(jump_y) <= 1e-2):
                #     print("residual_jump: \n{}\n".format(residual_component_jump))
                #     print("sampled_jump: \n{}\n".format(jump_y))
                #
                # if not (np.linalg.norm(eta_local - sampled_eta_local) / np.linalg.norm(sampled_eta_local) <= 1e-2):
                #     print("eta: \n{}\n".format(eta_local))
                #     print("sampled_eta: \n{}\n".format(sampled_eta_local))

                sampled_eta_global = np.sqrt(sum([eta2 for eta2 in sampled_eta_local]))
                sampled_eta_local = np.sqrt(sampled_eta_local)
                sampled_rhs_global = np.sqrt(sum([eta2 for eta2 in rhs_y]))
                sampled_rhs_local = np.sqrt(rhs_y)
                sampled_zero_global = np.sqrt(sum([-eta2 for eta2 in mixed_y]))
                sampled_zero_local = np.sqrt(-1*mixed_y)
                sampled_sq_global = np.sqrt(sum([eta2 for eta2 in volume_y]))
                sampled_sq_local = np.sqrt(volume_y)
                sampled_jump_global = np.sqrt(sum([eta2 for eta2 in jump_y]))
                sampled_jump_local = np.sqrt(jump_y)
            # print("eta local: \n{}".format(eta_local))
            eta_global = np.sqrt(sum([eta2 for eta2 in eta_local]))
            for lia in range(len(eta_local)):
                eta_local[lia] = np.max((0, eta_local[lia]))

            eta_local = np.sqrt(eta_local)

            # print("\n***************")
            # cg_fun = Function(fs)
            # print("dofs                  = {}".format(len(cg_fun.vector()[:])))
            # _h = np.array([c.h() for c in cells(self.mesh)])
            # print("h                     = {}".format(max(_h)))
            # print("femdegree (p)         = {}".format(femdegree))
            # _vol = np.array([c.volume() for c in cells(self.mesh)])
            # print("volume                = 1/4 h**2 = {}".format(max(_vol)))
            # dg0 = TestFunction(DG0_space)
            # _h_cell = assemble(h*dg0*dx)
            # _h_cell = _h_cell.array()[DG0_dofs.values()]
            # print("CellSize              = h * volume = {}".format(max(_h_cell)))
            # print("p**2/(2*sqrt(dofs))   = h = {}".format(femdegree**2/np.sqrt(len(cg_fun.vector()[:]))*0.5))
            # print("residual rhs          = h/p = {}".format(rhs_global))
            # print("**************\n")
            rhs_global = np.sqrt(sum([eta2 for lia, eta2 in enumerate(residual_zero_component_rhs)]))
            rhs_local = np.sqrt(residual_zero_component_rhs)
            zero_global = np.sqrt(sum([-eta2 for eta2 in residual_zero_component]))
            zero_local = np.sqrt(-1*residual_zero_component)
            sq_global = np.sqrt(sum([eta2 for eta2 in residual_component]))
            sq_local = np.sqrt(residual_component)
            jump_global = np.sqrt(sum([eta2 for eta2 in residual_component_jump]))
            jump_local = np.sqrt(residual_component_jump)

            retval = {"rhs_global": rhs_global, "rhs_local": rhs_local,
                      "zero_global": zero_global, "zero_local": zero_local,
                      "sq_global": sq_global, "sq_local": sq_local,
                      "jump_global": jump_global, "jump_local": jump_local,
                      "eta_global": sampled_eta_global, "eta_local": sampled_eta_local,
                      "sampled_rhs_global": sampled_rhs_global, "sampled_rhs_local": sampled_rhs_local,
                      "sampled_zero_global": sampled_zero_global, "sampled_zero_local": sampled_zero_local,
                      "sampled_sq_global": sampled_sq_global, "sampled_sq_local": sampled_sq_local,
                      "sampled_jump_global": sampled_jump_global, "sampled_jump_local": sampled_jump_local,
                      "sampled_eta_global": eta_global, "sampled_eta_local": eta_local
                      }
            if return_sampled_estimators is True:
                print("det error estimator done with difference to sampled global estimator: {}".format(np.linalg.norm(
                    eta_global - sampled_eta_global)))
        # endregion

        return retval
    # endregion

    # region def evaluate_product_coef_solution
    def evaluate_product_coef_solution(self, y, af, bxmax, theta, rho, femdegree, _set='all'):
        # region imports
        from alea.application.tt_asgfem.tensorsolver.tt_lognormal import sample_cont_coeff, sample_lognormal_tt
        from dolfin import project, norm, errornorm, File
        # endregion
        # region definitions
        if not new_dolfin:
            #                                   # obtain all coordinates of degrees of freedom
            points = self.functionSpace.dofmap().tabulate_all_coordinates(self.mesh)
        else:
            points = self.functionSpace.tabulate_dof_coordinates()  # obtain all coordinates of degrees of freedom
        # # reshape the coordinates to a vector
        points = points.reshape((-1, self.mesh.geometry().dim()))

        #                                       # initialise the rank vector
        ranks = [1] + [core.shape[0] for core in self.coeff_field] + [1]
        #                                       # initialise the dimension vector
        hdegs = [core.shape[1] for core in self.coeff_field]
        #                                       # sample coefficient field
        # endregion
        # region sample true coefficient and solution
        coef_at_y = sample_cont_coeff(self.coeff_field, af, points, y, ranks, hdegs, bxmax, theta, rho)
        sol_at_y = sample_lognormal_tt(tt.vector.from_list(self.solution), y, bxmax, theta=theta, rho=rho)
        # endregion
        # region create true coef and solution functions
        coef_fun = Function(self.functionSpace)
        coef_fun.vector()[:] = coef_at_y
        sol_fun = Function(self.functionSpace)
        sol_fun.vector()[:] = sol_at_y
        sample_function = coef_fun * sol_fun
        sample_function = project(sample_function, self.functionSpace)
        # file = File("sample_prodSolCoef_coef_{}.pvd".format(hash(y[0])))
        # file << coef_fun
        # file = File("sample_prodSolCoef_sol_{}.pvd".format(hash(y[0])))
        # file << sol_fun
        # file = File("sample_prodSolCoef_sample_{}.pvd".format(hash(y[0])))
        # file << sample_function
        # endregion

        all_list = self._create_components(_set=_set)

        for _lia in range(len(all_list) - 1, -1, -1):
            if len(all_list[_lia].shape) == 5:
                all_list[_lia] = np.transpose(all_list[_lia], (0, 1, 2, 4, 3))
                all_list[_lia] = np.reshape(all_list[_lia], (all_list[_lia].shape[0] *
                                                             all_list[_lia].shape[1],
                                                             all_list[_lia].shape[2],
                                                             all_list[_lia].shape[3] *
                                                             all_list[_lia].shape[4]), order="F")

        res_at_y = sample_lognormal_tt(all_list, y, bxmax, theta=theta, rho=rho, all_cores_stochastic=True)
        #                                       # create discrete first core to use in TT core description
        core0 = fully_disc_first_core(self.coeff_field, af, points, ranks, hdegs, bxmax, theta=theta, rho=rho,
                                      cg_mesh=self.mesh, femdegree=femdegree)
        res_at_y = np.reshape(res_at_y, (core0.shape[2], self.solution[0].shape[2]), order="F")
        res_coef_at_y = np.dot(core0[0, :, :], res_at_y)

        res_coef_function = Function(self.functionSpace)
        res_sol_function = Function(self.functionSpace)
        res_function = Function(self.functionSpace)
        for lia in range(res_coef_at_y.shape[1]):
            res_coef_function.vector()[:] = np.ascontiguousarray(res_coef_at_y[:, lia])
            res_sol_function.vector()[:] = np.ascontiguousarray(self.solution[0][0, :, lia])
            res_function += res_coef_function * res_sol_function
            res_function = project(res_function, self.functionSpace)
        # print "="*20
        # file = File("sample_prodSolCoef_res_coef_{}.pvd".format(hash(y[0])))
        # file << res_coef_function
        # file = File("sample_prodSolCoef_res_sol_{}.pvd".format(hash(y[0])))
        # file << res_sol_function
        # file = File("sample_prodSolCoef_res_sample_{}.pvd".format(hash(y[0])))
        # file << res_function
        curr_error = errornorm(res_function, sample_function)/norm(sample_function)
        # print("error of direct and tt residual function: {}".format(curr_error))
        assert (curr_error < 1e-10)
    # endregion

    # region def stochastic error estimator
    def eval_stochastic_error_estimator(self, af, bmax, theta, rho, femdegree=1, reference_m=None):
        # region pre-computations and definitions
        #                                           # create the affine field as used in the whole process
        with TicToc(sec_key="sto-error-estimator", key="create discrete first core (sto)", active=True, do_print=False):
            # dg_space = FunctionSpace(self.mesh, 'DG', femdegree-1)
            old_version = False
            if not new_dolfin:
                #                                   # obtain all coordinates of degrees of freedom
                points = self.functionSpace.dofmap().tabulate_all_coordinates(self.mesh)
            else:
                points = self.functionSpace.tabulate_dof_coordinates()  # obtain all coordinates of degrees of freedom
            #                                       # reshape the coordinates to a vector
            points = points.reshape((-1, self.mesh.geometry().dim()))

            #                                       # initialise the rank vector
            ranks = [1] + [core.shape[0] for core in self.coeff_field] + [1]
            #                                       # initialise the dimension vector
            hdegs = [core.shape[1] for core in self.coeff_field]
            #                                       # create discrete first core to use in TT core description
            core0 = fully_disc_first_core(self.coeff_field, af, points, ranks, hdegs, bmax, theta=theta, rho=rho,
                                          cg_mesh=self.mesh, femdegree=femdegree)
            coeff_field = deepcopy(self.coeff_field)
            coeff_field.insert(0, core0)
            zeta_list = {}
            zeta_tail_list = []
            residual_component = np.zeros(len(coeff_field) - 1)
            residual_component_order4 = np.zeros(len(coeff_field) - 1)
            # PH = PlotHelper()
        # endregion

        # region def collapse tail
        def collapse_tail_new(ten_left, ten_right, max_index, _index, global_det=False):
            retval = 0
            assert len(ten_left) == len(ten_right)
            for _lib in range(len(ten_left)-1, -1, -1):
                # print("lia={}, index={}".format(lia, index))
                # print("shape[lia-1] = {}, shape[lia]={}".format(ten[lia-1].shape, ten[lia].shape))
                # if lia < len(ten)-1:
                #     print("comp={}".format(comp.shape))
                assert(len(ten_left[_lib].shape) == 3)
                assert(len(ten_right[_lib].shape) == 3)
                assert(ten_left[_lib].shape[1] == ten_right[_lib].shape[1])
                out = _lib >= len(self.solution) - 1
                # print("index: {} : {} / {} len(sol) --> out={} and max_index {}".format(_index, _lib,
                #                                                                         len(self.solution), out,
                #                                                                         max_index))
                if _lib == len(ten_left)-1:
                    assert (ten_right[_lib].shape[2] == 1)
                    assert (ten_left[_lib].shape[2] == 1)
                    if _lib == _index:
                        if global_det is True:
                            raise AssertionError("you should not be here")
                        if out is True:
                            with TicToc(sec_key="sto-error-estimator", key="collapse-tail: mat", active=True,
                                        do_print=False):
                                mat = create_mat(ten_left[_lib].shape[1], theta, rho, bmax[_lib])

                            with TicToc(sec_key="sto-error-estimator", key="collapse-tail: index kron", active=True,
                                        do_print=False):
                                retval = np.kron(ten_left[_lib][:, 1, 0], ten_right[_lib][:, 1, 0])
                                retval = np.reshape(retval, (ten_left[_lib].shape[0], ten_right[_lib].shape[0]),
                                                    order='F')
                                retval *= mat[1, 1]
                        else:
                            with TicToc(sec_key="sto-error-estimator", key="collapse-tail: mat", active=True,
                                        do_print=False):
                                mat = create_mat(ten_left[_lib].shape[1], theta, rho, bmax[_lib])

                            with TicToc(sec_key="sto-error-estimator", key="collapse-tail: index kron", active=True,
                                        do_print=False):
                                retval = np.kron(ten_left[_lib][:, max_index, 0], ten_right[_lib][:, max_index, 0])
                                retval = np.reshape(retval, (ten_left[_lib].shape[0], ten_right[_lib].shape[0]),
                                                    order='F')
                                retval *= mat[max_index, max_index]
                    else:
                        if global_det is True:
                            with TicToc(sec_key="sto-error-estimator", key="collapse-tail: mat", active=True,
                                        do_print=False):
                                mat = create_mat(ten_left[_lib].shape[1], theta, rho, bmax[_lib])
                            with TicToc(sec_key="sto-error-estimator", key="collapse-tail: end dot 1", active=True,
                                        do_print=False):
                                assert (ten_right[_lib].shape[2] == 1)
                                retval = np.dot(ten_right[_lib][:, :, 0], mat)
                            with TicToc(sec_key="sto-error-estimator", key="collapse-tail: end dot 2", active=True,
                                        do_print=False):
                                assert (ten_left[_lib].shape[2] == 1)
                                retval = np.dot(ten_left[_lib][:, :, 0], retval.T)

                        else:
                            if out is True:
                                with TicToc(sec_key="sto-error-estimator", key="collapse-tail: mat", active=True,
                                            do_print=False):
                                    mat = create_mat(ten_left[_lib].shape[1], theta, rho, bmax[_lib])

                                with TicToc(sec_key="sto-error-estimator", key="collapse-tail: index kron", active=True,
                                            do_print=False):
                                    retval = np.kron(ten_left[_lib][:, 0, 0], ten_right[_lib][:, 0, 0])
                                    retval = np.reshape(retval, (ten_left[_lib].shape[0], ten_right[_lib].shape[0]),
                                                        order='F')
                                    retval *= mat[0, 0]
                            else:
                                with TicToc(sec_key="sto-error-estimator", key="collapse-tail: mat", active=True,
                                            do_print=False):
                                    mat = create_mat(self.solution[_lib+1].shape[1], theta, rho, bmax[_lib])
                                with TicToc(sec_key="sto-error-estimator", key="collapse-tail: end dot 1", active=True,
                                            do_print=False):
                                    assert (ten_right[_lib].shape[2] == 1)
                                    retval = np.dot(ten_right[_lib][:, :self.solution[_lib+1].shape[1], 0], mat)
                                with TicToc(sec_key="sto-error-estimator", key="collapse-tail: end dot 2", active=True,
                                            do_print=False):
                                    assert (ten_left[_lib].shape[2] == 1)
                                    retval = np.dot(ten_left[_lib][:, :self.solution[_lib+1].shape[1], 0], retval.T)

                else:
                    # regardless of the previous here we have the shape of retval R = [k_m+1, k_m+1']
                    if _lib == _index:
                        if global_det is True:
                            raise AssertionError("you should not be here")
                        with TicToc(sec_key="sto-error-estimator", key="collapse-tail: mat", active=True,
                                    do_print=False):
                            #                       # M[mu, mu']
                            mat = create_mat(ten_left[_lib].shape[1], theta, rho, bmax[_lib])

                        with TicToc(sec_key="sto-error-estimator", key="collapse-tail: index dot 1", active=True,
                                    do_print=False):
                            #                       # S[k', mu*, k_m+1'] x R[k_m+1, k_m+1'].T = R [k', k_m+1]
                            retval = np.dot(ten_right[_lib][:, max_index, :], retval.T)
                            #                       # S[k, mu*, k_m+1] x R[k', k_m+1].T = R[k, k']
                            retval = np.dot(ten_left[_lib][:, max_index, :], retval.T)
                            retval *= mat[max_index, max_index]
                    else:
                        if global_det is True:
                            with TicToc(sec_key="sto-error-estimator", key="collapse-tail: mat", active=True,
                                        do_print=False):
                                #                       # M[mu, mu']
                                mat = create_mat(ten_left[_lib].shape[1], theta, rho, bmax[_lib])
                            with TicToc(sec_key="sto-error-estimator", key="collapse-tail: einsum 1", active=True,
                                        do_print=False):
                                # region Alternative 1 using contraction over mat
                                # #                       # S[k', mu', k_m+1'] x M[mu, mu'].T = S[k', mu, k_m+1']
                                # comp = np.einsum('ijk, jl->ilk', ten_right[_lib], mat)
                                # endregion
                                # region Alternative 2 using contraction over R
                                # #                       # S[k', mu', k_m+1'] x R[k_m+1, k_m+1'].T = R[k', mu', k_m+1]
                                # retval = np.einsum('ijk, lk->ijl', ten_right[_lib], retval)
                                # endregion
                                # region Alternative 3 using reshaping and contraction over R (fast)
                                S = np.reshape(ten_right[_lib],
                                               (ten_right[_lib].shape[0] * ten_right[_lib].shape[1],
                                                ten_right[_lib].shape[2]), order='F')
                                retval = np.dot(S, retval.T)
                                retval = np.reshape(retval, (ten_right[_lib].shape[0], ten_right[_lib].shape[1],
                                                             ten_left[_lib].shape[2]), order='F')
                                # endregion
                            with TicToc(sec_key="sto-error-estimator", key="collapse-tail: einsum 1 matmul",
                                        active=True, do_print=False):
                                retval = np.transpose(retval, (0, 2, 1))
                                retval = np.reshape(retval, (ten_right[_lib].shape[0] * ten_left[_lib].shape[2],
                                                             ten_right[_lib].shape[1]), order='F')
                                retval = np.dot(retval, mat)
                                retval = np.reshape(retval, (ten_right[_lib].shape[0], ten_left[_lib].shape[2],
                                                             ten_right[_lib].shape[1]), order='F')
                                retval = np.transpose(retval, (0, 2, 1))

                            with TicToc(sec_key="sto-error-estimator", key="collapse-tail: einsum 2", active=True,
                                        do_print=False):
                                # region Alternative 1 using contraction over mat
                                # #                       # S[k', mu, k_m+1'] x R[k_m+1, k_m+1'] = R[k', mu, k_m+1]
                                # retval = np.einsum('ijk, lk->ijl', comp, retval)
                                # endregion
                                # region Alternative 2 using contraction over R
                                # #                       # S[k, mu, k_m+1] x R[k', mu', k_m+1] = R[k, k', mu, mu']
                                # retval = np.einsum('ijk, lmk->iljm', ten_left[_lib], retval)
                                # endregion
                                # region Alternative 3 using reshaping and contraction over R (fast)
                                with TicToc(sec_key="sto-error-estimator",
                                            key="collapse-tail: einsum 2 - reshape 1",
                                            active=True, do_print=False):
                                    S = np.reshape(ten_left[_lib], (ten_left[_lib].shape[0], ten_left[_lib].shape[1]
                                                                    * ten_left[_lib].shape[2]), order='F')
                                    R = np.reshape(retval, (retval.shape[0], retval.shape[1] * retval.shape[2]),
                                                   order='F')
                                with TicToc(sec_key="sto-error-estimator", key="collapse-tail: einsum 2 - dot",
                                            active=True, do_print=False):
                                    retval = np.dot(S, R.T)
                        else:
                            if out is True:
                                with TicToc(sec_key="sto-error-estimator", key="collapse-tail: out and not index",
                                            active=True, do_print=False):
                                    mat = create_mat(ten_left[_lib].shape[1], theta, rho, bmax[_lib])
                                    retval = np.dot(ten_right[_lib][:, 0, :], retval.T)
                                    retval = np.dot(ten_left[_lib][:, 0, :], retval.T)

                                    retval *= mat[0, 0]
                            else:
                                with TicToc(sec_key="sto-error-estimator", key="collapse-tail: mat", active=True,
                                            do_print=False):
                                    #                       # M[mu, mu']
                                    _d = self.solution[_lib + 1].shape[1]
                                    mat = create_mat(_d, theta, rho, bmax[_lib])
                                with TicToc(sec_key="sto-error-estimator", key="collapse-tail: einsum 1", active=True,
                                            do_print=False):
                                    # region Alternative 1 using contraction over mat
                                    # #                       # S[k', mu', k_m+1'] x M[mu, mu'].T = S[k', mu, k_m+1']
                                    # comp = np.einsum('ijk, jl->ilk', ten_right[_lib], mat)
                                    # endregion
                                    # region Alternative 2 using contraction over R
                                    # #                       # S[k', mu', k_m+1'] x R[k_m+1, k_m+1'].T = R[k', mu', k_m+1]
                                    # retval = np.einsum('ijk, lk->ijl', ten_right[_lib], retval)
                                    # endregion
                                    # region Alternative 3 using reshaping and contraction over R (fast)
                                    S = np.reshape(ten_right[_lib], (ten_right[_lib].shape[0] * ten_right[_lib].shape[1],
                                                                     ten_right[_lib].shape[2]), order='F')
                                    retval = np.dot(S, retval.T)
                                    retval = np.reshape(retval, (ten_right[_lib].shape[0], ten_right[_lib].shape[1],
                                                                 ten_left[_lib].shape[2]), order='F')
                                    # endregion
                                with TicToc(sec_key="sto-error-estimator", key="collapse-tail: einsum 1 matmul",
                                            active=True, do_print=False):
                                    retval = np.transpose(retval, (0, 2, 1))
                                    retval = np.reshape(retval, (ten_right[_lib].shape[0] * ten_left[_lib].shape[2],
                                                                 ten_right[_lib].shape[1]), order='F')
                                    retval = np.dot(retval[:, :_d], mat)
                                    retval = np.reshape(retval, (ten_right[_lib].shape[0], ten_left[_lib].shape[2],
                                                                 _d), order='F')
                                    retval = np.transpose(retval, (0, 2, 1))

                                with TicToc(sec_key="sto-error-estimator", key="collapse-tail: einsum 2", active=True,
                                            do_print=False):
                                    # region Alternative 1 using contraction over mat
                                    # #                       # S[k', mu, k_m+1'] x R[k_m+1, k_m+1'] = R[k', mu, k_m+1]
                                    # retval = np.einsum('ijk, lk->ijl', comp, retval)
                                    # endregion
                                    # region Alternative 2 using contraction over R
                                    # #                       # S[k, mu, k_m+1] x R[k', mu', k_m+1] = R[k, k', mu, mu']
                                    # retval = np.einsum('ijk, lmk->iljm', ten_left[_lib], retval)
                                    # endregion
                                    # region Alternative 3 using reshaping and contraction over R (fast)
                                    with TicToc(sec_key="sto-error-estimator", key="collapse-tail: einsum 2 - reshape 1",
                                                active=True, do_print=False):
                                        S = np.reshape(ten_left[_lib][:, :_d, :], (ten_left[_lib].shape[0], _d *
                                                       ten_left[_lib].shape[2]), order='F')
                                        R = np.reshape(retval, (retval.shape[0], retval.shape[1] * retval.shape[2]),
                                                       order='F')
                                    with TicToc(sec_key="sto-error-estimator", key="collapse-tail: einsum 2 - dot",
                                                active=True, do_print=False):
                                        retval = np.dot(S, R.T)

                            # with TicToc(sec_key="sto-error-estimator", key="collapse-tail: einsum 3", active=True,
                            #             do_print=False):
                            #     #                       # S[k, mu, k_m+1] x R[k', mu, k_m+1] = R[k, k_m+1]
                            #     retval = np.tensordot(ten_left[_lib], retval, axes=([2, 1], [2, 1]))
            return retval

        def collapse_tail(ten_left, ten_right, max_index, _index):
            comp = 0
            residual_tensor_list = []
            assert len(ten_left) == len(ten_right)
            for _lib in range(len(ten_left)-1, -1, -1):
                # print("lia={}, index={}".format(lia, index))
                # print("shape[lia-1] = {}, shape[lia]={}".format(ten[lia-1].shape, ten[lia].shape))
                # if lia < len(ten)-1:
                #     print("comp={}".format(comp.shape))
                assert(len(ten_left[_lib].shape) == 3)
                assert(len(ten_right[_lib].shape) == 3)
                assert(ten_left[_lib].shape[1] == ten_right[_lib].shape[1])
                if _lib == _index:
                    with TicToc(sec_key="sto-error-estimator", key="collapse-tail: mat", active=True,
                                do_print=False):
                        mat = create_mat(ten_left[_lib].shape[1], theta, rho, bmax[_lib])

                    with TicToc(sec_key="sto-error-estimator", key="collapse-tail: einsum 1", active=True,
                                do_print=False):
                        comp = np.einsum('ij, ab->iabj', ten_left[_lib][:, max_index, :], ten_right[_lib][:, max_index, :])
                        comp *= mat[max_index, max_index]
                    residual_tensor_list.insert(0, comp)
                else:
                    with TicToc(sec_key="sto-error-estimator", key="collapse-tail: mat", active=True,
                                do_print=False):
                        mat = create_mat(ten_left[_lib].shape[1], theta, rho, bmax[_lib])
                    with TicToc(sec_key="sto-error-estimator", key="collapse-tail: einsum 2", active=True,
                                do_print=False):
                        comp = np.einsum('ijk, jl->ilk', ten_right[_lib], mat)
                    with TicToc(sec_key="sto-error-estimator", key="collapse-tail: einsum 3", active=True,
                                do_print=False):
                        comp = np.einsum('ilk, alb->iabk', ten_left[_lib], comp)
                    residual_tensor_list.insert(0, comp)
            for _lib in range(len(residual_tensor_list)-1, -1, -1):
                if _lib == len(residual_tensor_list) - 1:
                    comp = residual_tensor_list[_lib]
                else:
                    # print("comp = {} vs {} ten".format(comp.shape, residual_tensor_list[_lib]))
                    # if _lib == 0:
                    #     print(residual_tensor_list[0][0,0,:,:])
                    #     print(comp[:,:,0,0])
                    #     print("dot={}".format((np.dot(residual_tensor_list[0][0,0,:,:], comp[:,:,0,0]))))

                    with TicToc(sec_key="sto-error-estimator", key="collapse-tail: einsum 4", active=True,
                                do_print=False):
                        comp = np.einsum('ijkl, lkcd->ijcd', residual_tensor_list[_lib], comp)
                    # if _lib == 0:
                    #     print("result={}".format(comp[0,0,0,0]))
                    #     exit()
            return comp
        # endregion
        # region get residual
        #                                           # contains only the stochastic components of order 5 and 3 resp
        with TicToc(sec_key="sto-error-estimator", key="create residual operators-all, inner", active=True,
                    do_print=False):
            _semi_discrete_res = self._create_components(_set="all")
            _semi_discrete_res_inner = self._create_components(_set="inner")
        # endregion

        # region debug functions
        # check if the outer residual is zero for all inner indices
        import itertools as iter
        def check_inner_old(outer_ten, inner_ten, all_ten):
            gpcd_lambda = [core.shape[1] for core in self.solution[1:]]
            p_lambda = [range(p) for p in gpcd_lambda + [1] * (len(coeff_field) - len(gpcd_lambda))]
            Lambda = iter.product(*p_lambda)
            for mu in Lambda:
                print mu
                fun_value = 0
                for dim_out in range(len(outer_ten) - 1, -1, -1):
                    if dim_out == len(outer_ten) - 1:
                        fun_value = outer_ten[dim_out][:, mu[dim_out - 1], :]
                    elif dim_out == 0:
                        fun_value = np.dot(outer_ten[dim_out][0, :, :], fun_value)
                    else:
                        fun_value = np.dot(outer_ten[dim_out][:, mu[dim_out - 1], :], fun_value)
                all_value = 0
                for dim_out in range(len(all_ten) - 1, -1, -1):
                    if dim_out == len(all_ten) - 1:
                        all_value = all_ten[dim_out][:, mu[dim_out - 1], :]
                    elif dim_out == 0:
                        all_value = np.dot(all_ten[dim_out][0, :, :], all_value)
                    else:
                        all_value = np.dot(all_ten[dim_out][:, mu[dim_out - 1], :], all_value)
                inner_value = 0
                for dim_out in range(len(inner_ten) - 1, -1, -1):
                    if dim_out == len(inner_ten) - 1:
                        inner_value = inner_ten[dim_out][:, mu[dim_out - 1], :]
                    elif dim_out == 0:
                        inner_value = np.dot(inner_ten[dim_out][0, :, :], inner_value)
                    else:
                        inner_value = np.dot(inner_ten[dim_out][:, mu[dim_out - 1], :], inner_value)
                print("inner = {} = {} = all".format(np.linalg.norm(inner_value), np.linalg.norm(all_value)))
                print("{} <= res[{}] <= {}".format(np.min(fun_value), mu, np.max(fun_value)))

        def check_inner(outer_ten_new, inner_ten_new, all_ten_new):
            gpcd_lambda = [core.shape[1] for core in self.solution[1:]]
            p_lambda = [range(p) for p in gpcd_lambda + [1] * (len(coeff_field) - len(gpcd_lambda) - 1)]
            Lambda = iter.product(*p_lambda)
            for mu in Lambda:
                fun_value = 0
                for dim_out in range(len(outer_ten_new) - 1, -1, -1):
                    if dim_out == len(outer_ten_new) - 1:
                        fun_value = outer_ten_new[dim_out][:, mu[dim_out], 0]
                    else:
                        fun_value = np.dot(outer_ten_new[dim_out][:, mu[dim_out], :], fun_value)

                fun_value = np.reshape(fun_value, (self.solution[0].shape[2],
                                                   coeff_field[0].shape[2]), order='F')
                fun_value = np.dot(self.solution[0][0, :, :], fun_value)
                fun_value = np.dot(coeff_field[0][0, :, :], fun_value.T)

                all_value = 0
                for dim_out in range(len(all_ten_new) - 1, -1, -1):
                    if dim_out == len(all_ten_new) - 1:
                        all_value = all_ten_new[dim_out][:, mu[dim_out], :]
                    else:
                        all_value = np.dot(all_ten_new[dim_out][:, mu[dim_out], :], all_value)
                all_value = np.reshape(all_value, (self.solution[0].shape[2],
                                                   coeff_field[0].shape[2]), order='F')
                all_value = np.dot(self.solution[0][0, :, :], all_value)
                all_value = np.dot(coeff_field[0][0, :, :], all_value.T)

                inner_value = 0
                for dim_out in range(len(inner_ten_new) - 1, -1, -1):
                    if dim_out == len(inner_ten_new) - 1:
                        inner_value = inner_ten_new[dim_out][:, mu[dim_out], :]
                    else:
                        inner_value = np.dot(inner_ten_new[dim_out][:, mu[dim_out], :], inner_value)
                inner_value = np.reshape(inner_value, (self.solution[0].shape[2],
                                                       coeff_field[0].shape[2]), order='F')
                inner_value = np.dot(self.solution[0][0, :, :], inner_value)
                inner_value = np.dot(coeff_field[0][0, :, :], inner_value.T)
                assert (np.linalg.norm(inner_value) == np.linalg.norm(all_value))
                # print("inner = {} = {} = all".format(np.linalg.norm(inner_value),
                #                                      np.linalg.norm(all_value)))
                # print("{} <= res[{}] <= {}".format(np.min(fun_value), mu, np.max(fun_value)))
                assert np.abs(np.min(fun_value)) < 1e-10
                assert np.abs(np.max(fun_value)) < 1e-10

                # print("frob norm of res = {}".format(np.linalg.norm(fun_value)))

        # check if the outer residual is equal to the usual residual for all indices outside of the solution
        def check_outer_old(outer_ten, inner_ten, all_ten):
            from numpy.testing import assert_almost_equal
            gpcd_lambda = [core.shape[1] for core in self.solution[1:]]
            p_lambda = [range(p) for p in gpcd_lambda + [1] * (len(coeff_field) - len(gpcd_lambda) - 1)]
            Lambda = iter.product(*p_lambda)
            gpcd_xi = [core.shape[1] for core in coeff_field[1:]]
            p_xi = [range(p) for p in gpcd_xi]
            Xi = iter.product(*p_xi)
            for mu in Xi:
                forward = True
                for l in Lambda:
                    for __lia in range(len(l)):
                        if not mu[__lia] == l[__lia]:
                            forward = False
                            break
                    if forward == False:
                        break
                if forward == True:
                    continue
                outer_value = 0
                for dim_out in range(len(outer_ten) - 1, -1, -1):
                    if dim_out == len(outer_ten) - 1:
                        outer_value = outer_ten[dim_out][:, mu[dim_out - 1], :]
                    elif dim_out == 0:
                        outer_value = np.dot(outer_ten[dim_out][0, :, :], outer_value)
                    else:
                        outer_value = np.dot(outer_ten[dim_out][:, mu[dim_out - 1], :], outer_value)
                all_value = 0
                for dim_all in range(len(all_ten) - 1, -1, -1):
                    if dim_all == len(all_ten) - 1:
                        all_value = all_ten[dim_all][:, mu[dim_all - 1], :]
                    elif dim_all == 0:
                        all_value = np.dot(all_ten[dim_all][0, :, :], all_value)
                    else:
                        all_value = np.dot(all_ten[dim_all][:, mu[dim_all - 1], :], all_value)
                inner_value = 0
                for dim_all in range(len(inner_ten) - 1, -1, -1):
                    if dim_all == len(inner_ten) - 1:
                        inner_value = inner_ten[dim_all][:, mu[dim_all - 1], :]
                    elif dim_all == 0:
                        inner_value = np.dot(inner_ten[dim_all][0, :, :], inner_value)
                    else:
                        inner_value = np.dot(inner_ten[dim_all][:, mu[dim_all - 1], :], inner_value)
                # print("inner {} == 0!".format(np.linalg.norm(inner_value)))
                # print("{} = res_all[{}] == res_out[{}] = {}".format(np.linalg.norm(all_value), mu, mu,
                #                                                     np.linalg.norm(outer_value)))

                assert (np.linalg.norm(outer_value - all_value) < 1e-6)
                assert (np.linalg.norm(inner_value) < 1e-6)
                # assert_almost_equal(np.linalg.norm(outer_value), np.linalg.norm(all_value))

        def check_outer(outer_ten_new, inner_ten_new, all_ten_new):
                from numpy.testing import assert_almost_equal
                gpcd_lambda = [core.shape[1] for core in self.solution[1:]]
                p_lambda = [range(p) for p in gpcd_lambda + [1] * (len(coeff_field) - len(gpcd_lambda))]
                Lambda = iter.product(*p_lambda)
                gpcd_xi = [core.shape[1] for core in coeff_field[1:]]
                p_xi = [range(p) for p in gpcd_xi]
                Xi = iter.product(*p_xi)
                Xi = [mu for mu in Xi if mu not in Lambda]
                for mu in Xi:
                    forward = True
                    for l in Lambda:
                        for __lia in range(len(l)):
                            if not mu[__lia] == l[__lia]:
                                forward = False
                                break
                        if forward == False:
                            break
                    if forward == True:
                        continue
                    outer_value = 0
                    for dim_out in range(len(outer_ten_new) - 1, -1, -1):
                        if dim_out == len(outer_ten_new) - 1:
                            outer_value = outer_ten_new[dim_out][:, mu[dim_out], :]
                        else:
                            outer_value = np.dot(outer_ten_new[dim_out][:, mu[dim_out], :], outer_value)
                    all_value = 0
                    for dim_all in range(len(all_ten_new) - 1, -1, -1):
                        if dim_all == len(all_ten_new) - 1:
                            all_value = all_ten_new[dim_all][:, mu[dim_all], :]
                        else:
                            all_value = np.dot(all_ten_new[dim_all][:, mu[dim_all], :], all_value)
                    inner_value = 0
                    for dim_all in range(len(inner_ten_new) - 1, -1, -1):
                        if dim_all == len(inner_ten_new) - 1:
                            inner_value = inner_ten_new[dim_all][:, mu[dim_all], :]
                        else:
                            inner_value = np.dot(inner_ten_new[dim_all][:, mu[dim_all], :], inner_value)
                    # print("inner {} == 0!".format(np.linalg.norm(inner_value)))
                    # print("{} = res_all[{}] == res_out[{}] = {}".format(np.linalg.norm(all_value), mu, mu,
                    #                                                     np.linalg.norm(outer_value)))
                    assert (np.linalg.norm(outer_value - all_value) < 1e-6)
                    assert (np.linalg.norm(inner_value) < 1e-6)
                    # assert_almost_equal(np.linalg.norm(outer_value), np.linalg.norm(all_value))
        # endregion

        # region def create outer tensor set with own tt summation
        def create_outer_tt_own_sum(_inner, _all):
            inner_list = _inner
            all_list = _all

            #                                       # store overall tensor components
            # region store all
            # for _lia in range(len(_all) - 1, -1, -1):
            #     all_list.insert(0, _all[_lia])
            # for _lia in range(len(_inner) - 1, -1, -1):
            #     inner_list.insert(0, _inner[_lia])
            # endregion
            #                                       # reshape both tensor components to order 3 tensors
            # region reshape and enhance
            for _lia in range(len(all_list) - 1, -1, -1):
                if len(all_list[_lia].shape) == 5:
                    all_list[_lia] = np.transpose(all_list[_lia], (0, 1, 2, 4, 3))
                    all_list[_lia] = np.reshape(all_list[_lia], (all_list[_lia].shape[0] *
                                                                 all_list[_lia].shape[1],
                                                                 all_list[_lia].shape[2],
                                                                 all_list[_lia].shape[3] *
                                                                 all_list[_lia].shape[4]), order="F")
                if len(inner_list[_lia].shape) == 2:
                    inner_list[_lia] = np.reshape(inner_list[_lia], (inner_list[_lia].shape[0],
                                                                     1,
                                                                     inner_list[_lia].shape[1]), order="F")
                if len(inner_list[_lia].shape) == 5:
                    inner_list[_lia] = np.transpose(inner_list[_lia], (0, 1, 2, 4, 3))
                    inner_list[_lia] = np.reshape(inner_list[_lia], (inner_list[_lia].shape[0] *
                                                                     inner_list[_lia].shape[1],
                                                                     inner_list[_lia].shape[2],
                                                                     inner_list[_lia].shape[3] *
                                                                     inner_list[_lia].shape[4]), order="F")
                # region enhance zeros
                component = np.zeros((inner_list[_lia].shape[0], all_list[_lia].shape[1], inner_list[_lia].shape[2]))
                component[:, :inner_list[_lia].shape[1], :] = inner_list[_lia]
                inner_list[_lia] = component
                # endregion
            # endregion

            assert (len(inner_list) == len(all_list))

            # region def own_tt_sum

            def own_tt_sum(ten1, ten2):
                _retval = []
                assert (len(ten1) == len(ten2))  # one more in first list than in second list
                if len(ten1) % 2 == 0:
                    p = -1
                else:
                    p = 1
                __core = None
                for __lia in range(len(ten1)):
                    # print("own tt sum: {}/{}".format(__lia, len(ten1)))
                    if __lia == len(ten1) - 1:
                        __core = np.zeros((ten1[__lia].shape[0] + ten2[__lia].shape[0], ten1[__lia].shape[1], 1))
                        __core[:ten1[__lia].shape[0], :, :] = ten1[__lia]
                        __core[ten1[__lia].shape[0]:, :, :] = (-1) * ten2[__lia]
                        if len(ten1) > 1:
                            _retval.append(__core)

                            continue
                        # print("core{} {} ten1 {} ten2 {}".format(__lia, __core.shape, ten1[__lia].shape,
                        #                                          ten2[__lia].shape))
                    if len(ten1) > 1:
                        __core = np.zeros((ten1[__lia].shape[0] + ten2[__lia].shape[0], ten1[__lia].shape[1],
                                           ten1[__lia].shape[2] + ten2[__lia].shape[2]))
                        __core[:ten1[__lia].shape[0], :, :ten1[__lia].shape[2]] = ten1[__lia]
                        # print("core{} {} ten1 {} ten2 {}".format(__lia, __core.shape, ten1[__lia].shape,
                        #                                          ten2[__lia].shape))
                        __core[ten1[__lia].shape[0]:, :, ten1[__lia].shape[2]:] = (-1) * ten2[__lia]
                    if __lia == 0:
                        core_of_ones = np.zeros((int(math.ceil(__core.shape[0]*0.5)), __core.shape[0]))
                        # print("core_of_ones shape {}".format(core_of_ones.shape))
                        core_of_ones[:, :ten1[0].shape[0]] = np.eye(int(math.ceil(__core.shape[0]*0.5)),
                                                                    int(math.ceil(__core.shape[0]*0.5)))
                        # print("ones shape {}".format(int(__core.shape[0]*0.5)))

                        core_of_ones[:, ten1[0].shape[0]:] = p*np.eye(int(math.ceil(__core.shape[0]*0.5)),
                                                                      int(math.ceil(__core.shape[0]*0.5)))
                        # print("core of ones {} x {}".format(core_of_ones.shape, __core.shape))
                        # __core = np.einsum('ij, jkl->ikl', core_of_ones, __core)
                        _k, _l = __core.shape[1], __core.shape[2]
                        __core = np.reshape(__core, (__core.shape[0], _k * _l), order='F')
                        __core = np.dot(core_of_ones, __core)
                        __core = np.reshape(__core, (core_of_ones.shape[0], _k, _l), order='F')
                    _retval.append(__core)

                return _retval

            # endregion
            return own_tt_sum(all_list, inner_list), all_list, inner_list
        # endregion

        global_zeta_order4 = 0
        if True:                                   # complete outer residual with own tt summation
            with TicToc(sec_key="sto-error-estimator", key="all local error estimators", active=True,
                        do_print=False):
                # region Create outer residual tensor
                if old_version is True:
                    with TicToc(sec_key="sto-error-estimator", key="create outer residual tensor sparse", active=True,
                                do_print=False):
                        # outer_ten = create_outer_tt_zero_first_all_core(_semi_discrete_res_inner, _semi_discrete_res)
                        outer_ten, all_ten, inner_ten = create_outer_tt_sparse(_semi_discrete_res_inner,
                                                                               _semi_discrete_res)
                with TicToc(sec_key="sto-error-estimator", key="create outer residual tensor sum", active=True,
                            do_print=False):
                    # outer_ten = create_outer_tt_zero_first_all_core(_semi_discrete_res_inner, _semi_discrete_res)
                    outer_ten_new, all_ten_new, inner_ten_new = create_outer_tt_own_sum(_semi_discrete_res_inner,
                                                                                        _semi_discrete_res)

                # endregion

                # region Debug: check outer_ten for residual properties
                if old_version is True:
                    check_inner_old(outer_ten, inner_ten, all_ten)
                    check_outer_old(outer_ten, inner_ten, all_ten)
                else:
                    outer_ten = outer_ten_new
                    inner_ten = inner_ten_new
                    all_ten = all_ten_new
                if False:
                    # print("begin debugging")
                    with TicToc(sec_key="sto-error-estimator", key="debugging", active=True,
                                do_print=False):
                        check_inner(outer_ten, inner_ten, all_ten)
                        # check_outer(outer_ten, inner_ten, all_ten)

                        outer_ten_debug = tt.vector.from_list(outer_ten)
                        inner_ten_debug = tt.vector.from_list(inner_ten)
                        all_ten_debug = tt.vector.from_list(all_ten)
                        # print("Norm of all tensor: {}".format(tt.vector.norm(all_ten_debug)))
                        # print("Norm of inner tensor: {}".format(tt.vector.norm(inner_ten_debug)))
                        # print("Norm of outer tensor: {}".format(tt.vector.norm(outer_ten_debug)))
                    # print("end debugging. GOOD!")
                # endregion

                for index in range(len(coeff_field)-1):
                    # region Compute the index corresponding to the collapsed residual matrix
                    if index >= len(self.solution) - 1:
                        if not len(self.solution) > len(coeff_field) - 1:
                            if (isinstance(reference_m, int) or isinstance(reference_m, float)) \
                                    and len(self.solution) >= reference_m+1:
                                zeta_tail_list.append((index, 0))
                                continue
                            with TicToc(sec_key="sto-error-estimator", key="collapse residual cores tail", active=True,
                                        do_print=False):
                                # test here with 1 or 0 and check if this is reached at all
                                if old_version:
                                    collapsed_res = collapse_tail(outer_ten[1:], outer_ten[1:], 1, index)
                                else:
                                    # comp_1 = collapse_tail(outer_ten, outer_ten, 1, index)
                                    collapsed_res = collapse_tail_new(outer_ten, outer_ten, 1, index)

                        else:
                            zeta_tail_list.append((index, 0))
                            continue
                    else:
                        with TicToc(sec_key="sto-error-estimator", key="collapse residual cores", active=True,
                                    do_print=False):
                            # test if the -1 is desired or not
                            if old_version:
                                collapsed_res = collapse_tail(outer_ten[1:], outer_ten[1:],
                                                              self.solution[index + 1].shape[1], index)
                            else:
                                # comp_1 = collapse_tail(outer_ten, outer_ten, self.solution[index + 1].shape[1] - 1,
                                #                        index)
                                collapsed_res = collapse_tail_new(outer_ten, outer_ten,
                                                                  self.solution[index + 1].shape[1], index)

                    # endregion

                    # region residual contract collapsed residual with coeff field to reduce one rank

                    with TicToc(sec_key="sto-error-estimator", key="create remainder by contraction", active=True,
                                do_print=False):
                        remainder = np.reshape(collapsed_res, (coeff_field[0].shape[2],
                                                               self.solution[0].shape[2],
                                                               coeff_field[0].shape[2],
                                                               self.solution[0].shape[2]), order='F')

                        # #                           contract [0, N, k_c] x [k_c, k_s, k_c, k_s] = [N, k_s, k_c, k_s]
                        # remainder = np.einsum('ijk, klmn->ijlmn', coeff_field[0], remainder)
                        N, k_c, k_s = coeff_field[0].shape[1], remainder.shape[0], remainder.shape[1]
                        remainder = np.reshape(remainder, (k_c, k_s * k_c * k_s), order='F')
                        remainder = np.dot(coeff_field[0][0, :, :], remainder)
                        remainder = np.reshape(remainder, (N, k_s, k_c, k_s), order='F')
                        assert (remainder.shape[0] == coeff_field[0].shape[1])
                        assert (remainder.shape[1] == self.solution[0].shape[2])
                        assert (remainder.shape[2] == coeff_field[0].shape[2])
                        assert (remainder.shape[3] == self.solution[0].shape[2])
                    # endregion
                    # region compute local error estimator
                    global _global_objects
                    if False:
                        with TicToc(sec_key="sto-error-estimator", key="parallel local estimator", active=True,
                                    do_print=False):
                            with TicToc(sec_key="sto-error-estimator", key="init parallel computer", active=True,
                                        do_print=False):
                                C = Computer_estimator(self.mesh, self.functionSpace)
                                _global_objects = C
                            residual_component[index] = np.sum(Parallel(
                                                                        mp.cpu_count()
                                                                        # 1
                                                                        )
                                                               (delayed(sto_estimator_parallel)
                                                               (self.solution[0][0, :, k_s_1],
                                                                coeff_field[0][0, :, k_c],
                                                                self.solution[0][0, :, k_s_2],
                                                                remainder[:, k_s_1, k_c, k_s_2])
                                                               for k_s_1, k_c, k_s_2 in
                                                                iter.product(range(remainder.shape[1]),
                                                                             range(remainder.shape[2]),
                                                                             range(remainder.shape[3]))))

                    # endregion
                    # region compute residual with order 4 stiffness tensor parallel
                    with TicToc(sec_key="sto-error-estimator",
                                key="residual error (4-tensor, parallel)",
                                active=True, do_print=False):
                        if False:
                            with TicToc(sec_key="sto-error-estimator",
                                        key="residual error (4-tensor, parallel) - init worker",
                                        active=True, do_print=False):
                                C = Computer_tensor_multiply(remainder, coeff_field[0][0, :, :])
                                _global_objects = C
                        with TicToc(sec_key="sto-error-estimator",
                                    key="residual error (4-tensor, parallel) - reduce coef",
                                    active=True, do_print=False):
                            red_rem = np.zeros((len(self.order4_stiffness_tensor), remainder.shape[1],
                                                remainder.shape[3]))
                            for lia, (key, _) in enumerate(self.order4_stiffness_tensor):
                                if False:
                                    # incredible slow. why? something seems to be blocked
                                    red_rem[lia, :, :] = np.sum(Parallel(
                                                                         mp.cpu_count(),
                                                                         # backend='threading'
                                                                        )
                                                                (delayed(sto_estimator_parallel_reduction)
                                                                (key[0], key[1], k)
                                                                for k in range(coeff_field[0].shape[2])), axis=0)
                                else:
                                    red_rem[lia, :, :] = np.sum([remainder[key[0], :, k, :]*coeff_field[0][0, key[1], k]
                                                                 for k in range(coeff_field[0].shape[2])], axis=0)
                        with TicToc(sec_key="sto-error-estimator",
                                    key="residual error (4-tensor, parallel) - init worker",
                                    active=True, do_print=False):
                            C = Computer_tensor(self.order4_stiffness_tensor)
                            _global_objects = C
                        residual_component_order4[index] = np.sum(Parallel(
                                                                           mp.cpu_count()
                                                                          )
                                                                  (delayed(sto_estimator_parallel_order4)
                                                                  (self.solution[0][0, :, k],
                                                                   self.solution[0][0, :, k1],
                                                                   red_rem[:, k, k1])
                                                                   for k, k1 in
                                                                   iter.product(range(self.solution[0].shape[2]),
                                                                                range(self.solution[0].shape[2]))))
                    # print("!!!!!!!!!!!!!! sto residual difference with order 4 tensor approach: "
                    #       "\n   {}".format(np.linalg.norm(residual_component[index] -
                    #                                       residual_component_order4[index]) /
                    #                        np.linalg.norm(residual_component[index])))

                    # endregion
            # print("residual component list for own summation: {}".format(residual_component))
            # region global zeta error estimator
            with TicToc(sec_key="sto-error-estimator", key="only global error", active=True, do_print=False):
                # global_zeta = np.sqrt(sum(map(lambda x: x ** 2, zeta_list.values())))
                collapsed_res = collapse_tail_new(outer_ten, outer_ten, len(outer_ten)+2, -1, global_det=True)
                # ###### contract collapsed residual with first core remainder
                assert (len(collapsed_res.shape) == 2)
                # region residual contract collapsed residual with coeff field to reduce one rank
                with TicToc(sec_key="sto-error-estimator", key="create remainder by contraction global", active=True,
                            do_print=False):
                    remainder = np.reshape(collapsed_res, (coeff_field[0].shape[2],
                                                           self.solution[0].shape[2],
                                                           coeff_field[0].shape[2],
                                                           self.solution[0].shape[2]), order='F')

                    # #                           contract [0, N, k_c] x [k_c, k_s, k_c, k_s] = [N, k_s, k_c, k_s]
                    # remainder = np.einsum('ijk, klmn->ijlmn', coeff_field[0], remainder)
                    N, k_c, k_s = coeff_field[0].shape[1], remainder.shape[0], remainder.shape[1]
                    remainder = np.reshape(remainder, (k_c, k_s * k_c * k_s), order='F')
                    remainder = np.dot(coeff_field[0][0, :, :], remainder)
                    remainder = np.reshape(remainder, (N, k_s, k_c, k_s), order='F')
                    assert (remainder.shape[0] == coeff_field[0].shape[1])
                    assert (remainder.shape[1] == self.solution[0].shape[2])
                    assert (remainder.shape[2] == coeff_field[0].shape[2])
                    assert (remainder.shape[3] == self.solution[0].shape[2])
                # endregion
                if False:
                    with TicToc(sec_key="sto-error-estimator", key="parallel global estimator", active=True,
                                do_print=False):
                        C = Computer_estimator(self.mesh, self.functionSpace)
                        _global_objects = C
                        global_zeta = np.sum(Parallel(
                                                      mp.cpu_count()
                                                      # 1
                                                      )
                                             (delayed(sto_estimator_parallel)
                                             (self.solution[0][0, :, k_s_1],
                                              coeff_field[0][0, :, k_c],
                                              self.solution[0][0, :, k_s_2],
                                              remainder[:, k_s_1, k_c, k_s_2])
                                             for k_s_1, k_c, k_s_2 in iter.product(range(remainder.shape[1]),
                                                                                   range(remainder.shape[2]),
                                                                                   range(remainder.shape[3]))))

            # endregion
            # region global zeta error estimator order-4 tensor
            with TicToc(sec_key="sto-error-estimator",
                        key=" only global residual (4-tensor, parallel)",
                        active=True, do_print=False):
                with TicToc(sec_key="sto-error-estimator",
                            key="only global residual (4-tensor, parallel) - reduce coef",
                            active=True, do_print=False):
                    # red_coef = np.zeros((len(self.order4_stiffness_tensor), coeff_field[0].shape[2]))
                    red_rem = np.zeros((len(self.order4_stiffness_tensor), remainder.shape[1], remainder.shape[3]))
                    for lia, (key, _) in enumerate(self.order4_stiffness_tensor):
                        red_rem[lia, :, :] = np.sum([remainder[key[0], :, k, :] * coeff_field[0][0, key[1], k]
                                                     for k in range(coeff_field[0].shape[2])], axis=0)
                with TicToc(sec_key="sto-error-estimator",
                            key="only global residual (4-tensor, parallel) - init worker",
                            active=True, do_print=False):
                    C = Computer_tensor(self.order4_stiffness_tensor)
                    _global_objects = C
                global_zeta_order4 = np.sum(Parallel(
                                                     mp.cpu_count()
                                                    )
                                            (delayed(sto_estimator_parallel_order4)
                                            (self.solution[0][0, :, k],
                                             self.solution[0][0, :, k1],
                                             red_rem[:, k, k1])
                                            for k, k1 in iter.product(range(self.solution[0].shape[2]),
                                                                      range(self.solution[0].shape[2]))))
            # print("!!!!!!!!!!!!!! sto residual difference with order 4 tensor approach: "
            #       "\n   {}".format(np.linalg.norm(global_zeta - global_zeta_order4) / global_zeta))
            # endregion
        residual_component = residual_component_order4
        global_zeta = global_zeta_order4
        # region store results in lists to pass to old sgfem functions
        with TicToc(sec_key="sto-error-estimator", key="append to zeta list", active=True, do_print=False):
            for index in range(len(residual_component)):
                if index < len(self.solution)-1:
                    zeta_list[index] = np.sqrt(residual_component[index])
                else:
                    zeta_tail_list.append((index, np.sqrt(residual_component[index])))
            global_zeta = np.sqrt(global_zeta)
        # endregion

        print("tail list: {}".format(zeta_tail_list))
        print("zeta list: {}".format(zeta_list))
        print("global zeta: {}".format(global_zeta))
        retval = {"zeta_list": zeta_list, "global_zeta": global_zeta, "zeta_tail_list": zeta_tail_list}
        return retval
    # endregion

_global_objects = None


# region det tt estimators for parallel treatment
# region def sto_estimator_parallel
def sto_estimator_parallel(sol1, coef1, sol2, coef2):
    O = _global_objects
    data = {"coef1": coef1, "sol1": sol1, "coef2": coef2, "sol2": sol2}
    result = O.compute_sto(data)
    gc.collect()
    return result
# endregion

# region def sto_estimator_parallel tensor
def sto_estimator_parallel_order4(sol1, sol2, coef1):
    O = _global_objects
    data = {"a1": coef1, "u1": sol1, "u2": sol2}
    result = O.compute_sto_estimator_parallel_order4(data)
    gc.collect()
    return result
# endregion


# region def sto_estimator_parallel tensor reduction
def sto_estimator_parallel_reduction(i, j, k):
    O = _global_objects
    data = {"i": i, "j": j, "k": k}
    result = O.multiply_vec(data)
    gc.collect()
    return result
# endregion

# region def det_estimator_zero_parallel


def det_estimator_zero_parallel(sol1, coef1):
    O = _global_objects
    data = {"coef1": coef1, "sol1": sol1, "with_volume": True, "with_rhs": False}
    result = O.compute_residual_error_zero(data)
    gc.collect()
    return result
# endregion

# region def det_estimator_sq_parallel


def det_estimator_sq_parallel(sol1, coef1, sol2, coef2):
    O = _global_objects
    data = {"coef1": coef1, "sol1": sol1, "coef2": coef2, "sol2": sol2, "with_volume": True, "with_jump": False}
    result = O.compute_residual_error_volume(data)
    gc.collect()
    return result

def det_estimator_sq_parallel_new(sol1, coef1, sol2):
    O = _global_objects
    data = {"coef1": coef1, "sol1": sol1, "sol2": sol2, "with_volume": True, "with_jump": False}
    result = O.compute_residual_error_volume_new(data)
    gc.collect()
    return result
# endregion

# region def det_estimator_jump_parallel


def det_estimator_jump_parallel(sol1, coef1, sol2, coef2):
    O = _global_objects
    data = {"coef1": coef1, "sol1": sol1, "coef2": coef2, "sol2": sol2, "with_volume": False, "with_jump": True}
    result = O.compute_residual_error_edge(data)
    gc.collect()
    return result
# endregion
# endregion

# region sampled det estimators for parallel treatment
# region def sampled_det_estimator_rhs_parallel


def sampled_det_estimator_rhs_parallel(y, coef, sol):
    O = _global_objects
    data = {"y": y, "coef": coef, "sol": sol}
    result = O.sample_det_estimator_rhs_term(data)
    gc.collect()
    return result
# endregion

# region def sampled_det_estimator_mixed_parallel


def sampled_det_estimator_mixed_parallel(y, coef, sol):
    O = _global_objects
    data = {"y": y, "coef": coef, "sol": sol}
    result = O.sample_det_estimator_mixed_term(data)
    gc.collect()
    return result
# endregion

# region def sampled_det_estimator_sq_parallel


def sampled_det_estimator_sq_parallel(y, coef, sol):
    O = _global_objects
    data = {"y": y, "coef": coef, "sol": sol}
    result = O.sample_det_estimator_volume_term(data)
    gc.collect()
    return result
# endregion

# region def sampled_det_estimator_jump_parallel


def sampled_det_estimator_jump_parallel(y, coef, sol):
    O = _global_objects
    data = {"y": y, "coef": coef, "sol": sol}
    result = O.sample_det_estimator_jump_term(data)
    gc.collect()
    return result
# endregion
# endregion

# region def class Computer estimator to compute residual error in parallel


class Computer_estimator(object):
    # region init
    def __init__(self, mesh, V, femdegree=1, DG0=None, DG0_dofs=None, fs=None, h=None, timing=False, coeff_field=None,
                 affine_field=None, bxmax=None, theta=None, rho=None, sol_ten=None, nu=None, points=None):
        self.FEM = self.setup_FEM(mesh, V, femdegree=femdegree, DG0=DG0, DG0_dofs=DG0_dofs, fs=fs, h=h, timing=timing,
                                  coeff_field=coeff_field, affine_field=affine_field, bxmax=bxmax, theta=theta, rho=rho,
                                  sol_ten=sol_ten, nu=nu, points=points)

    # endregion

    # region setup FEM
    def setup_FEM(self, mesh, V, femdegree=1, DG0=None, DG0_dofs=None, fs=None, h=None, timing=False, coeff_field=None,
                  affine_field=None, bxmax=None, theta=None, rho=None, sol_ten=None, nu=None, points=None):
        coef_fun1 = Function(V)
        coef_fun2 = Function(V)
        sol_fun1 = Function(V)
        sol_fun2 = Function(V)
        rhs_fun = Function(V)
        FEM = {'mesh': mesh, 'V': V, 'assemble_quad_degree': -1, "coef_fun1": coef_fun1, "coef_fun2": coef_fun2,
               "sol_fun1": sol_fun1, "sol_fun2": sol_fun2,  "DG0": DG0, "DG0_dofs": DG0_dofs, "fs": fs, "h": h,
               "timing": timing, "femdegree": femdegree, "rhs_fun": rhs_fun, "coeff_field": coeff_field, "affine_field":
               affine_field, "bxmax": bxmax, "theta": theta, "rho": rho, "sol_ten": sol_ten, "nu": nu, "points": points}
        return FEM
    # endregion

    def compute_sto(self, _data):
        _coef_fun = Function(self.FEM["V"])
        # _coef_fun.vector()[:] = _data["coef1"]
        _coef_fun1 = Function(self.FEM["V"])
        # _coef_fun1.vector()[:] = _data["coef2"]
        _sol_fun = Function(self.FEM["V"])
        # _sol_fun.vector()[:] = _data["sol1"]
        _sol_fun1 = Function(self.FEM["V"])
        # _sol_fun1.vector()[:] = _data["sol2"]
        # _coef_fun = self.FEM["coef_fun1"]
        _coef_fun.vector().set_local(_data["coef1"])
        # _coef_fun1 = self.FEM["coef_fun2"]
        _coef_fun1.vector().set_local(_data["coef2"])
        # _sol_fun = self.FEM["sol_fun1"]
        _sol_fun.vector().set_local(_data["sol1"])
        # _sol_fun1 = self.FEM["sol_fun2"]
        _sol_fun1.vector().set_local(_data["sol2"])
        # setup residual estimator forms
        R_T = inner(_coef_fun * nabla_grad(_sol_fun), _coef_fun1 * nabla_grad(_sol_fun1))
        res = R_T * dx(self.FEM["mesh"])
        zeta_res = assemble(res, form_compiler_parameters={'quadrature_degree': self.FEM["assemble_quad_degree"]})

        return zeta_res

    def compute_residual_error_zero(self, _data):
        sol_fun = self.FEM["sol_fun1"]
        coef_fun = self.FEM["coef_fun1"]
        sol_fun.vector()[:] = np.ascontiguousarray(_data["sol1"])
        coef_fun.vector()[:] = np.ascontiguousarray(_data["coef1"])
        mesh = self.FEM["mesh"]
        # with_volume = _data["with_volume"]
        # with_rhs = _data["with_rhs"]
        assemble_quad_degree = self.FEM["assemble_quad_degree"]
        DG0 = self.FEM["DG0"]
        _DG0_dofs = self.FEM["DG0_dofs"]
        _fs = self.FEM["fs"]
        _h = self.FEM["h"]
        femdegree = self.FEM["femdegree"]
        if DG0 is None:
            DG0 = FunctionSpace(mesh, 'DG', 0)
        if _DG0_dofs is None:
            _DG0_dofs = dict([(_c.index(), DG0.dofmap().cell_dofs(_c.index())[0]) for _c in cells(mesh)])
        _dg0 = TestFunction(DG0)

        # setup residual estimator forms
        # if with_rhs:
        #     # print("+rhs")
        #     if _fs is None:
        #         R_T = Expression("1", degree=1)
        #     else:
        #         R_T = Function(_fs)
        #         R_T.vector()[:] = np.ones(len(R_T.vector()[:])) ** 2

        # else:
        #     if _fs is None:
        #         R_T = Expression("0", degree=0) * _dg0 * dx
        #     else:
        #         R_T = Function(_fs)
        #         R_T.vector()[:] = np.zeros(len(R_T.vector()[:]))
        # res = (_h ** 2 * (femdegree ** (-2))) * R_T * _dg0 * dx
        # assemble_rhs = assemble(res, form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
        # assemble_rhs = assemble_rhs.array()[_DG0_dofs.values()]

        # print("femdegree={}".format(femdegree))
        # print("h={}".format(h))
        # print("rhs: {}".format(assemble_rhs))
            # print("+volume")
        fun_T = Function(_fs)
        fun_T.vector()[:] = np.ones(len(fun_T.vector()[:]))
        R_T = inner(nabla_div(coef_fun * nabla_grad(sol_fun)), fun_T) * 2
        res = (_h ** 2 * (femdegree ** (-2))) * R_T * _dg0 * dx
        assemble_volume = assemble(res, form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
        assemble_volume = assemble_volume.array()[_DG0_dofs.values()]

        # print("zero volume: {}".format(assemble_volume))
        # return assemble_rhs + assemble_volume

        return assemble_volume

    def compute_residual_error_volume(self, _data):
        sol_fun = self.FEM["sol_fun1"]
        coef_fun = self.FEM["coef_fun1"]
        sol_fun1 = self.FEM["sol_fun2"]
        coef_fun1 = self.FEM["coef_fun2"]
        sol_fun.vector()[:] = np.ascontiguousarray(_data["sol1"])
        coef_fun.vector()[:] = np.ascontiguousarray(_data["coef1"])
        sol_fun1.vector()[:] = np.ascontiguousarray(_data["sol2"])
        coef_fun1.vector()[:] = np.ascontiguousarray(_data["coef2"])
        mesh = self.FEM["mesh"]
        assemble_quad_degree = self.FEM["assemble_quad_degree"]
        DG0 = self.FEM["DG0"]
        _DG0_dofs = self.FEM["DG0_dofs"]
        _h = self.FEM["h"]
        timing = self.FEM["timing"]
        femdegree = self.FEM["femdegree"]

        with TicToc(sec_key="det-error-estimator", key="res error-h", active=timing, do_print=False):
            if _h is None:
                _h = CellSize(mesh)
        with TicToc(sec_key="det-error-estimator", key="res error-DG0 fs", active=timing, do_print=False):
            if DG0 is None:
                DG0 = FunctionSpace(mesh, 'DG', 0)

        with TicToc(sec_key="det-error-estimator", key="res error-DG0 dofs", active=timing, do_print=False):
            if _DG0_dofs is None:
                _DG0_dofs = dict([(c.index(), DG0.dofmap().cell_dofs(c.index())[0]) for c in cells(mesh)])
            _dg0 = TestFunction(DG0)

        # setup residual estimator forms

        with TicToc(sec_key="det-error-estimator", key="res error-volume 1", active=timing, do_print=False):
            R_T = inner(nabla_div(coef_fun * nabla_grad(sol_fun)),
                        nabla_div(coef_fun1 * nabla_grad(sol_fun1)))

        with TicToc(sec_key="det-error-estimator", key="res error-volume 2", active=timing, do_print=False):
            res = (_h ** 2 * (femdegree ** (-2))) * R_T * _dg0 * dx
        with TicToc(sec_key="det-error-estimator", key="res error-assemble", active=timing, do_print=False):
            res_volume = assemble(res, form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
        with TicToc(sec_key="det-error-estimator", key="res error-eval", active=timing, do_print=False):
            res_volume = res_volume.array()[_DG0_dofs.values()]
        # print("resVolume:{}".format(res_volume))
        return res_volume

    def compute_residual_error_volume_new(self, _data):
        sol_fun = self.FEM["sol_fun1"]
        coef_fun = self.FEM["coef_fun1"]
        sol_fun1 = self.FEM["sol_fun2"]
        sol_fun.vector()[:] = np.ascontiguousarray(_data["sol1"])
        coef_fun.vector()[:] = np.ascontiguousarray(_data["coef1"])
        sol_fun1.vector()[:] = np.ascontiguousarray(_data["sol2"])
        mesh = self.FEM["mesh"]
        assemble_quad_degree = self.FEM["assemble_quad_degree"]
        DG0 = self.FEM["DG0"]
        _DG0_dofs = self.FEM["DG0_dofs"]
        _h = self.FEM["h"]
        timing = self.FEM["timing"]
        femdegree = self.FEM["femdegree"]

        with TicToc(sec_key="det-error-estimator", key="res error-h", active=timing, do_print=False):
            if _h is None:
                _h = CellSize(mesh)
        with TicToc(sec_key="det-error-estimator", key="res error-DG0 fs", active=timing, do_print=False):
            if DG0 is None:
                DG0 = FunctionSpace(mesh, 'DG', 0)

        with TicToc(sec_key="det-error-estimator", key="res error-DG0 dofs", active=timing, do_print=False):
            if _DG0_dofs is None:
                _DG0_dofs = dict([(c.index(), DG0.dofmap().cell_dofs(c.index())[0]) for c in cells(mesh)])
            _dg0 = TestFunction(DG0)

        # setup residual estimator forms

        with TicToc(sec_key="det-error-estimator", key="res error-volume 1", active=timing, do_print=False):
            R_T = inner(nabla_div(coef_fun * nabla_grad(sol_fun)),
                        nabla_div(nabla_grad(sol_fun1)))

        with TicToc(sec_key="det-error-estimator", key="res error-volume 2", active=timing, do_print=False):
            res = (_h ** 2 * (femdegree ** (-2))) * R_T * _dg0 * dx
        with TicToc(sec_key="det-error-estimator", key="res error-assemble", active=timing, do_print=False):
            res_volume = assemble(res, form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
        with TicToc(sec_key="det-error-estimator", key="res error-eval", active=timing, do_print=False):
            res_volume = res_volume.array()[_DG0_dofs.values()]
        # print("resVolume:{}".format(res_volume))
        return res_volume

    def compute_residual_error_edge(self, _data):
        sol_fun = self.FEM["sol_fun1"]
        coef_fun = self.FEM["coef_fun1"]
        sol_fun1 = self.FEM["sol_fun2"]
        coef_fun1 = self.FEM["coef_fun2"]
        sol_fun.vector()[:] = np.ascontiguousarray(_data["sol1"])
        coef_fun.vector()[:] = np.ascontiguousarray(_data["coef1"])
        sol_fun1.vector()[:] = np.ascontiguousarray(_data["sol2"])
        coef_fun1.vector()[:] = np.ascontiguousarray(_data["coef2"])
        mesh = self.FEM["mesh"]
        assemble_quad_degree = self.FEM["assemble_quad_degree"]
        DG0 = self.FEM["DG0"]
        _DG0_dofs = self.FEM["DG0_dofs"]
        _h = self.FEM["h"]
        timing = self.FEM["timing"]
        femdegree = self.FEM["femdegree"]

        with TicToc(sec_key="det-error-estimator", key="res error-nu", active=timing, do_print=False):
            nu = FacetNormal(mesh)
        with TicToc(sec_key="det-error-estimator", key="res error-h", active=timing, do_print=False):
            if _h is None:
                _h = CellSize(mesh)
        with TicToc(sec_key="det-error-estimator", key="res error-DG0 fs", active=timing, do_print=False):
            if DG0 is None:
                DG0 = FunctionSpace(mesh, 'DG', 0)

        with TicToc(sec_key="det-error-estimator", key="res error-DG0 dofs", active=timing, do_print=False):
            if _DG0_dofs is None:
                _DG0_dofs = dict([(c.index(), DG0.dofmap().cell_dofs(c.index())[0]) for c in cells(mesh)])
            _dg0 = TestFunction(DG0)

        # setup residual estimator forms
        with TicToc(sec_key="det-error-estimator", key="res error-edge 1", active=timing, do_print=False):
            R_dT = inner(jump(coef_fun * nabla_grad(sol_fun), nu), jump(coef_fun1 * nabla_grad(sol_fun1), nu))
        with TicToc(sec_key="det-error-estimator", key="res error-edge 2", active=timing, do_print=False):
            res = (avg(_h) * (femdegree ** (-1))) * R_dT * avg(_dg0) * dS
        with TicToc(sec_key="det-error-estimator", key="res error-assemble", active=timing, do_print=False):
            res_edge = assemble(res, form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
        with TicToc(sec_key="det-error-estimator", key="res error-eval", active=timing, do_print=False):
            res_edge = res_edge.array()[_DG0_dofs.values()]
        # print("resEdge:{}".format(res_edge))
        return res_edge

    # region def sample det estimator mixed term
    def sample_det_estimator_mixed_term(self, _data):
        # region get variables from data
        V = self.FEM["V"]
        # mesh = self.FEM["mesh"]
        # coeff_field = self.FEM["coeff_field"]
        # af = self.FEM["affine_field"]
        bxmax = self.FEM["bxmax"]
        theta = self.FEM["theta"]
        rho = self.FEM["rho"]
        # sol_ten = self.FEM["sol_ten"]
        sol_fun1 = self.FEM["sol_fun1"]
        coef_fun1 = self.FEM["coef_fun1"]
        femdegree = self.FEM["femdegree"]
        DG0_space = self.FEM["DG0"]
        _DG0_dofs = self.FEM["DG0_dofs"]
        _h = self.FEM["h"]
        # points = self.FEM["points"]
        y = _data["y"]
        coef_at_y = _data["coef"]
        sol_at_y = _data["sol"]
        # endregion
        # region definitions
        #                                           # initialise the rank vector
        # ranks = [1] + [core.shape[0] for core in coeff_field] + [1]
        #                                           # initialise the dimension vector
        # hdegs = [core.shape[1] for core in coeff_field]
        # endregion
        # region sample true coefficient and solution
        # coef_at_y = coeff_field[y]
        # sol_at_y = sol_ten[y]
        # endregion
        # region create true coef and solution functions
        coef_fun1.vector()[:] = coef_at_y
        sol_fun1.vector()[:] = sol_at_y
        fun_T = Function(V)

        _dg0 = TestFunction(DG0_space)
        fun_T.vector()[:] = np.ones(len(fun_T.vector()[:]))
        R_T = inner(nabla_div(coef_fun1 * nabla_grad(sol_fun1)), fun_T) * 2
        res = (_h ** 2 * (femdegree ** (-2))) * R_T * _dg0 * dx
        assemble_volume = assemble(res, form_compiler_parameters={'quadrature_degree': -1})
        assemble_volume = assemble_volume.array()[_DG0_dofs.values()]
        # endregion
        zeta_y = np.prod([zeta(theta, rho, bxmax[lic], y[lic]) ** 2 for lic in range(len(y))])
        return assemble_volume * zeta_y

    # endregion
    # region def sample det estimator rhs term
    def sample_det_estimator_rhs_term(self, _data):
        # region get variables from data
        V = self.FEM["V"]
        # mesh = self.FEM["mesh"]
        femdegree = self.FEM["femdegree"]
        theta = self.FEM["theta"]
        rho = self.FEM["rho"]
        bxmax = self.FEM["bxmax"]
        DG0_space = self.FEM["DG0"]
        _DG0_dofs = self.FEM["DG0_dofs"]
        _h = self.FEM["h"]
        # nu = self.FEM["nu"]
        # points = self.FEM["points"]
        y = _data["y"]
        # endregion
        # region assemble residual
        fun_T = Function(V)
        fun_T.vector()[:] = np.ones(len(fun_T.vector()[:]))
        _dg0 = TestFunction(DG0_space)
        R_T = inner(fun_T, fun_T)
        res = (_h ** 2 * (femdegree ** (-2))) * R_T * _dg0 * dx
        assemble_volume = assemble(res, form_compiler_parameters={'quadrature_degree': -1})
        assemble_volume = assemble_volume.array()[_DG0_dofs.values()]
        # endregion
        zeta_y = np.prod([zeta(theta, rho, bxmax[lic], y[lic]) ** 2 for lic in range(len(y))])
        return assemble_volume * zeta_y

    # endregion
    # region def sample det estimator volume term
    def sample_det_estimator_volume_term(self, _data):
        # region get variables from data
        # V = self.FEM["V"]
        # mesh = self.FEM["mesh"]
        # coeff_field = self.FEM["coeff_field"]
        # af = self.FEM["affine_field"]
        bxmax = self.FEM["bxmax"]
        theta = self.FEM["theta"]
        rho = self.FEM["rho"]
        sol_ten = self.FEM["sol_ten"]
        sol_fun1 = self.FEM["sol_fun1"]
        coef_fun1 = self.FEM["coef_fun1"]
        femdegree = self.FEM["femdegree"]
        DG0_space = self.FEM["DG0"]
        _DG0_dofs = self.FEM["DG0_dofs"]
        _h = self.FEM["h"]
        coef_at_y = _data["coef"]
        sol_at_y = _data["sol"]
        # points = self.FEM["points"]
        y = _data["y"]
        # endregion
        # region definitions
        #                                       # initialise the rank vector
        # ranks = [1] + [core.shape[0] for core in coeff_field] + [1]
        #                                       # initialise the dimension vector
        # hdegs = [core.shape[1] for core in coeff_field]
        # endregion
        # region sample true coefficient and solution
        # coef_at_y = coeff_field[y]
        # sol_at_y = sol_ten[y]
        # endregion
        # region create true coef and solution functions
        coef_fun1.vector()[:] = coef_at_y
        sol_fun1.vector()[:] = sol_at_y
        # endregion
        # region assemble residual
        _dg0 = TestFunction(DG0_space)
        R_T = inner(nabla_div(coef_fun1 * nabla_grad(sol_fun1)), nabla_div(coef_fun1 * nabla_grad(sol_fun1)))
        res = (_h ** 2 * (femdegree ** (-2))) * R_T * _dg0 * dx
        assemble_volume = assemble(res, form_compiler_parameters={'quadrature_degree': -1})
        assemble_volume = assemble_volume.array()[_DG0_dofs.values()]
        # endregion
        zeta_y = np.prod([zeta(theta, rho, bxmax[lic], y[lic]) ** 2 for lic in range(len(y))])
        return assemble_volume * zeta_y

    # endregion
    # region def sample det estimator jump term
    def sample_det_estimator_jump_term(self, _data):
        # region get variables from data
        # V = self.FEM["V"]
        # mesh = self.FEM["mesh"]
        # coeff_field = self.FEM["coeff_field"]
        # af = self.FEM["affine_field"]
        bxmax = self.FEM["bxmax"]
        theta = self.FEM["theta"]
        rho = self.FEM["rho"]
        sol_ten = self.FEM["sol_ten"]
        sol_fun1 = self.FEM["sol_fun1"]
        coef_fun1 = self.FEM["coef_fun1"]
        femdegree = self.FEM["femdegree"]
        DG0_space = self.FEM["DG0"]
        _DG0_dofs = self.FEM["DG0_dofs"]
        _h = self.FEM["h"]
        nu = self.FEM["nu"]
        # points = self.FEM["points"]
        y = _data["y"]
        coef_at_y = _data["coef"]
        sol_at_y = _data["sol"]
        # endregion
        # region definitions
        #                                       # initialise the rank vector
        # ranks = [1] + [core.shape[0] for core in coeff_field] + [1]
        #                                       # initialise the dimension vector
        # hdegs = [core.shape[1] for core in coeff_field]
        # endregion
        # region sample true coefficient and solution
        # coef_at_y = coeff_field[y]
        # sol_at_y = sol_ten[y]
        # endregion
        # region create true coef and solution functions
        coef_fun1.vector()[:] = coef_at_y
        sol_fun1.vector()[:] = sol_at_y
        # endregion
        # region assemble residual
        _dg0 = TestFunction(DG0_space)
        R_dT = inner(jump(coef_fun1 * nabla_grad(sol_fun1), nu), jump(coef_fun1 * nabla_grad(sol_fun1), nu))
        res = (avg(_h) * (femdegree ** (-1))) * R_dT * avg(_dg0) * dS
        assemble_volume = assemble(res, form_compiler_parameters={'quadrature_degree': -1})
        assemble_volume = assemble_volume.array()[_DG0_dofs.values()]
        # endregion
        zeta_y = np.prod([zeta(theta, rho, bxmax[lic], y[lic]) ** 2 for lic in range(len(y))])
        return assemble_volume * zeta_y

    # endregion

def sto_estimate_residual_error(_sol_fun, _coef_fun, _sol_fun1, _coef_fun1, mesh, assemble_quad_degree=-1):

    # setup residual estimator forms
    R_T = inner(nabla_div(_coef_fun * nabla_grad(_sol_fun)), nabla_div(_coef_fun1 * nabla_grad(_sol_fun1)))
    res = R_T * dx(mesh)
    zeta_res = assemble(res, form_compiler_parameters={'quadrature_degree': assemble_quad_degree})

    return zeta_res
# endregion


# region def class Computer tensor for the stochastic error estimator using order 4 tensor operator
class Computer_tensor(object):
    def __init__(self, ten):
        self.FEM = self.setup_FEM(ten)

    def setup_FEM(self, ten):
        FEM = {'ten': ten}
        return FEM

    def compute_sto_estimator_parallel_order4(self, _data):
        a1, u1, u2 = _data["a1"], _data["u1"], _data["u2"]

        return np.sum(a1[lia] * np.dot(u1, sps.csr_matrix.dot(value, u2)) for lia, (_, value) in enumerate(self.FEM["ten"]))
# endregion


# def class Computer tensor multiply. Not used atm. somehow blocks an object so it is very slow
class Computer_tensor_multiply(object):
    def __init__(self, a, b):
        self.FEM = self.setup_FEM(a, b)

    def setup_FEM(self, a, b):
        FEM = {'a': a, 'b': b}
        return FEM

    def multiply_vec(self, _data):
        i, j = _data["i"], _data["j"]
        k = _data["k"]
        a = self.FEM["a"]
        b = self.FEM["b"]
        return a[j, :, k, :] * b[i, k]
# endregion
