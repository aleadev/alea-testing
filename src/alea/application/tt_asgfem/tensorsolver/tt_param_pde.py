from __future__ import division
import tt
import numpy
import scipy.sparse
import scipy.sparse.linalg
from tt_util import ravel,reshape,ttRightOrthogonalize, rankOneProduct, leftThreeHalfdot, rightThreeHalfdot, ttGetVec
from tt_sgfem_util import evaluate_residual_norm, ttRandomDeterministicRankOne, miniLeftMatrixCG
import logging
import operator
def prod(factors):
    return reduce(operator.mul, factors, 1)

# retrieve logger
logger = logging.getLogger(__name__)


def ttParamPDEALS(A,Delta,B,U,maxit=1000,conv=1e-10,cgconv=1e-14,acc=1e-14,P=None):
    K = numpy.size(A,0)

    ucore = U.core
    pos = U.ps
    r = U.r
    n = U.n

    V = 0*U.copy()

    AA = list(A)
    kk = 0

    # oldres = float("inf")
    # resnorm = evaluate_residual_norm(U,A,Delta,B)
    logger.info('distance of iteration steps: %.16f;', (U-V).norm())
    # logger.info('norm of residual: %.16f;', resnorm)
    while kk < maxit and ((U-V).norm() > conv):
        V = U.copy()
        # oldres = resnorm
        kk = kk+1

        #------------HALFSWEEP--------------
        for i in range(K):
            u = ucore[pos[i]-1:pos[i+1]-1]
            left = []
            right = []

            logger.debug('Halfdot for step %i!' % i)
            # Left Side
            for j in range(K):

                if i > 0:
                    if i == 1:
                        uu = reshape(ucore[pos[0]-1:pos[1]-1],[n[0],r[1]])
                        AA[j] = numpy.dot(uu.T,A[j].dot(uu))
                    leftj = AA[j]
                else:
                    leftj = A[j]


                for m in range(1,i):
                    uu = ucore[pos[m]-1:pos[m+1]-1]
                    uu = reshape(uu,[r[m],n[m],r[m+1]])
                    if m == j:
                        leftj = rankOneProduct(uu,left=leftj,mid=Delta[j])
                        leftj = leftThreeHalfdot(uu,leftj)
                    else:
                        leftj = rankOneProduct(uu,left=leftj)
                        leftj = leftThreeHalfdot(uu,leftj)

                left.append(leftj)

                if i < K-1:
                    if j > i:
                        uu = ucore[pos[j]-1:pos[j+1]-1]
                        uu = reshape(uu,[r[j],n[j],r[j+1]])
                        rightj = rankOneProduct(uu,mid=Delta[j])
                        rightj = rightThreeHalfdot(uu,rightj)
                        for m in range(j-1,i,-1):
                            uu = ucore[pos[m]-1:pos[m+1]-1]
                            uu = reshape(uu,[r[m],n[m],r[m+1]])
                            rightj = rankOneProduct(uu,right=rightj)
                            rightj = rightThreeHalfdot(uu,rightj)
                        right.append(rightj)
                    else: right.append(None)
                else: right.append(None)

            # Right Side
            leftj = B
            for m in range(i):
                uu = ucore[pos[m]-1:pos[m+1]-1]
                uu = reshape(uu,[r[m],n[m],r[m+1]])
                if m > 0:
                    leftj = rankOneProduct(uu,left=leftj,mid=ravel(numpy.array(numpy.eye(n[m],1))))
                else:
                    logger.debug("shapes uu, leftj: %s  %s" % (str(uu.shape), str(leftj.shape)))
                    leftj = rankOneProduct(uu,mid=leftj)
                leftj = ravel(leftj)

            if i > 0:
                leftj = numpy.kron(ravel(numpy.array(numpy.eye(n[i],1))),leftj)
                leftj = ravel(leftj)

            if i < K-1:
                uu = ucore[pos[K-1]-1:pos[K]-1]
                uu = reshape(uu,[r[K-1],n[K-1]])
                rightj = uu.dot(ravel(numpy.array(numpy.eye(n[K-1],1))))
                for m in range(K-2,i,-1):
                    uu = ucore[pos[m]-1:pos[m+1]-1]
                    uu = reshape(uu,[r[m],n[m],r[m+1]])
                    rightj = rankOneProduct(uu,mid=ravel(numpy.array(numpy.eye(n[m],1))),right=rightj)
                    rightj = ravel(rightj)
                b = ravel(numpy.kron(rightj,leftj))
            else:
                b = ravel(leftj)

            logger.debug('Start solving step %i!' % i)
            # print('Start solving step %i!' % i)
            # CG-Solver
            # Problem in Components
            summ = 0
            for kl in range(K):
                u = reshape(u,[r[i],n[i],r[i+1]])
                if i == 0:
                    a = rankOneProduct(u,mid=left[kl],right=right[kl])
                elif kl == i:
                    a = rankOneProduct(u,left=left[kl],mid=Delta[kl])
                else:
                    a = rankOneProduct(u,left=left[kl],right=right[kl])
                summ = summ + ravel(a)

            # CG Steps
            R = b - summ

            # Preconditioning
            if (i == 0) & (P is not None):
                h = miniLeftMatrixCG(P,R,n[0],r[1])
                # PP = scipy.sparse.kron(scipy.sparse.eye(r[1],r[1],format='csr'),P)
                # h = scipy.sparse.linalg.spsolve(PP,R)
            else:
                h = R

            d = h
            count = 0
            while numpy.linalg.norm(R) > cgconv and count < 10000:#for kk in range(100):#numpy.size(b,0)/2):
                z = 0
                for kl in range(K):
                    d = reshape(d,[r[i],n[i],r[i+1]])
                    if i == 0:
                        a = rankOneProduct(d,mid=left[kl],right=right[kl])
                    elif kl == i:
                        a = rankOneProduct(d,left=left[kl],mid=Delta[kl])
                    else:
                        a = rankOneProduct(d,left=left[kl],right=right[kl])
                    z = z + ravel(a)

                d = ravel(d)
                alpha = numpy.dot(R,h)/numpy.dot(d,z)
                u = ravel(u) + alpha*d
                rr = R - alpha*z
                logger.debug("norm(R) %.16f" % numpy.linalg.norm(R))
                # print("norm(R) %.16f" % numpy.linalg.norm(R))

                # Preconditioning
                if (i == 0) & (P is not None):
                    hh = miniLeftMatrixCG(P,rr,n[0],r[1],h)
                    # hh = scipy.sparse.linalg.spsolve(PP,rr)
                else:
                    hh = rr

                beta = numpy.dot(rr,hh)/numpy.dot(R,h)
                h = hh
                R = rr
                d = h + beta*d

                count += 1

            logger.debug('Orthogonalization of step %i!' % i)
            # Orthogonalization
            if i == 0:
                u = reshape(u,[n[0],r[1]])
                uau = numpy.dot(u.T,(A[0].dot(u)))

                X,S,Y = numpy.linalg.svd(uau,full_matrices=1)
                # if numpy.linalg.norm(X-Y.T) > 1e-10:
                #     print numpy.linalg.norm(X-Y.T),'kann nicht symmetrisch sein.'

                uu = ucore[pos[1]-1:pos[2]-1]
                uu = reshape(uu,[r[1],n[1]*r[2]])
                right = ucore[pos[i + 2] - 1:pos[K] - 1]

                s = numpy.linalg.norm(S)
                Y = Y/numpy.sqrt(s)
                S = numpy.diag(S)
                D = S/s
                D = numpy.sqrt(D)
                Dinv = numpy.linalg.inv(D)

                ucore[pos[0]-1:pos[1]-1] = ravel(numpy.dot(u.dot(Y.T),Dinv))
                ucore[pos[1]-1:pos[2]-1] = ravel(numpy.dot(D,(s*Y.dot(uu))))
                ucore[pos[i+2]-1:pos[K]-1] = right
                U.ps = pos
                U.r = r
                U.core = ucore
            elif i < K-1:
                u = reshape(u,[r[i]*n[i],r[i+1]])
                if numpy.linalg.norm(u) < 1e-10:
                    logger.debug(i,r[i],r[i+1],str(u))
                a,b,c = numpy.linalg.svd(u,full_matrices=0);
                x = reshape(ucore[pos[i + 1] - 1:pos[i + 2] - 1], [r[i + 1], n[i + 1] * r[i + 2]])
                right = ucore[pos[i + 2] - 1:pos[K] - 1]
                r[i + 1] = max(len(numpy.nonzero(numpy.array(b) > acc)[0].tolist()),1)
                pos = numpy.cumsum([1] + (n * r[:K] * r[1:K + 1]).tolist())

                a = a[:, :r[i + 1]]
                b = numpy.diag(b[:r[i + 1]])
                c = c[:r[i + 1], :]

                ucore[pos[i]-1:pos[i+1]-1] = ravel(a)
                ucore[pos[i+1]-1:pos[i+2]-1] = ravel(numpy.dot(numpy.dot(b,c),x))
                ucore[pos[i+2]-1:pos[K]-1] = right
                ucore = ucore[0:pos[K]-1]
                U.ps = pos
                U.r = r
                U.core = ucore
            else:
                u = reshape(u,[r[K-1],n[K-1]])
                a,b,c = numpy.linalg.svd(u,full_matrices=0)
                x = reshape(ucore[pos[K-2] - 1:pos[K-1] - 1], [r[K-2]*n[K-2],r[K-1]])
                r[K-1] = max(len(numpy.nonzero(numpy.array(b) > acc)[0].tolist()),1)
                pos = numpy.cumsum([1] + (n * r[:K] * r[1:K + 1]).tolist())

                a = a[:, :r[K-1]]
                b = numpy.diag(b[:r[K-1]])
                c = c[:r[K-1], :]

                ucore[pos[K-1]-1:pos[K]-1] = ravel(c)
                ucore[pos[K-2]-1:pos[K-1]-1] = ravel(numpy.dot(numpy.dot(x,a),b))
                ucore = ucore[0:pos[K]-1]
                U.ps = pos
                U.r = r
                U.core = ucore


        #------------BACKSWEEP--------------
        for i in range(K-2,0,-1):
            u = ucore[pos[i]-1:pos[i+1]-1]
            left = []
            right = []

            logger.debug('Halfdot for step %i!' % i)
            # Left Side
            for j in range(K):

                leftj = AA[j]

                for m in range(1,i):
                    uu = ucore[pos[m]-1:pos[m+1]-1]
                    uu = reshape(uu,[r[m],n[m],r[m+1]])
                    if m == j:
                        leftj = rankOneProduct(uu,left=leftj,mid=Delta[j])
                        leftj = leftThreeHalfdot(uu,leftj)
                    else:
                        leftj = rankOneProduct(uu,left=leftj)
                        leftj = leftThreeHalfdot(uu,leftj)

                left.append(leftj)

                if i < K-1:
                    if j > i:
                        uu = ucore[pos[j]-1:pos[j+1]-1]
                        uu = reshape(uu,[r[j],n[j],r[j+1]])
                        rightj = rankOneProduct(uu,mid=Delta[j])
                        rightj = rightThreeHalfdot(uu,rightj)
                        for m in range(j-1,i,-1):
                            uu = ucore[pos[m]-1:pos[m+1]-1]
                            uu = reshape(uu,[r[m],n[m],r[m+1]])
                            rightj = rankOneProduct(uu,right=rightj)
                            rightj = rightThreeHalfdot(uu,rightj)
                        right.append(rightj)
                    else: right.append(None)
                else: right.append(None)

            # Right Side
            leftj = B
            for m in range(i):
                uu = ucore[pos[m]-1:pos[m+1]-1]
                uu = reshape(uu,[r[m],n[m],r[m+1]])
                if m > 0:
                    leftj = rankOneProduct(uu,left=leftj,mid=ravel(numpy.array(numpy.eye(n[m],1))))
                else:
                    leftj = rankOneProduct(uu,mid=leftj)
                leftj = ravel(leftj)

            leftj = numpy.kron(ravel(numpy.array(numpy.eye(n[i],1))),leftj)
            leftj = ravel(leftj)

            if i < K-1:
                uu = ucore[pos[K-1]-1:pos[K]-1]
                uu = reshape(uu,[r[K-1],n[K-1]])
                rightj = uu.dot(ravel(numpy.array(numpy.eye(n[K-1],1))))
                for m in range(K-2,i,-1):
                    uu = ucore[pos[m]-1:pos[m+1]-1]
                    uu = reshape(uu,[r[m],n[m],r[m+1]])
                    rightj = rankOneProduct(uu,mid=ravel(numpy.array(numpy.eye(n[m],1))),right=rightj)
                    rightj = ravel(rightj)
                b = ravel(numpy.kron(rightj,leftj))
            else:
                b = ravel(leftj)

            logger.debug('Start solving step %i!' % i)
            # CG-Solver
            # Problem in Components
            summ = 0
            for kl in range(K):
                u = reshape(u,[r[i],n[i],r[i+1]])
                if kl == i:
                    a = rankOneProduct(u,left=left[kl],mid=Delta[kl])
                else:
                    a = rankOneProduct(u,left=left[kl],right=right[kl])
                summ = summ + ravel(a)

            # CG Steps
            R = b - summ
            d = R
            count = 0
            while numpy.linalg.norm(R) > cgconv and count < 10000:#for kk in range(100):#numpy.size(b,0)/2):
                z = 0
                for kl in range(K):
                    d = reshape(d,[r[i],n[i],r[i+1]])
                    if kl == i:
                        a = rankOneProduct(d,left=left[kl],mid=Delta[kl])
                    else:
                        a = rankOneProduct(d,left=left[kl],right=right[kl])
                    z = z + ravel(a)

                d = ravel(d)
                alpha = numpy.dot(R,R)/numpy.dot(d,z)
                u = ravel(u) + alpha*d
                rr = R - alpha*z
                beta = numpy.dot(rr,rr)/numpy.dot(R,R)
                R = rr
                d = R + beta*d

                count += 1

            logger.debug('Orthogonalization of step %i!' % i)
            # Orthogonalization
            u = reshape(u,[r[i],n[i]*r[i+1]])
            a,b,c = numpy.linalg.svd(u,full_matrices=0);
            x = reshape(ucore[pos[i-1] - 1:pos[i] - 1], [r[i-1]*n[i-1],r[i]])
            r[i] = max(len(numpy.nonzero(numpy.array(b) > acc)[0].tolist()),1)
            pos = numpy.cumsum([1] + (n * r[:K] * r[1:K + 1]).tolist())

            a = a[:, :r[i]]
            b = numpy.diag(b[:r[i]])
            c = c[:r[i], :]

            ucore[pos[i]-1:pos[i+1]-1] = ravel(c)
            ucore[pos[i-1]-1:pos[i]-1] = ravel(numpy.dot(numpy.dot(x,a),b))
            ucore = ucore[0:pos[K]-1]
            U.ps = pos
            U.r = r
            U.core = ucore

        # resnorm = evaluate_residual_norm(U,A,Delta,B)
        logger.info('distance of iteration steps: %.16f;', (U-V).norm())
        print('distance of iteration steps: %.16f;', (U - V).norm())
        # logger.info('norm of residual: %.16f;', resnorm)


    return U, kk


def paramOuterProduct(Delta,M):
    D = list();

    for i in range(M):
        left = 1
        for j in range(1,M):
            if i == j:
                left = scipy.sparse.kron(left,Delta[j])
            else:
                n = Delta[j].shape[0]
                I = scipy.sparse.eye(n)
                left = scipy.sparse.kron(left,I)

        D.append(left)

    return D


def multiFirstUnit(M,n):
    e = scipy.sparse.eye(prod(n[i] for i in range(1,M)),1)

    return e


def ttParamExpectation(U):
    d = U.d
    n = U.n
    cores = tt.tensor.to_list(U)

    right = None
    for i in range(d-1,0,-1):
        u = cores[i]
        right = rankOneProduct(u,mid=ravel(numpy.eye(n[i],1)),right=right)
        right = ravel(right)

    u = cores[0]
    right = rankOneProduct(u,right=right)
    right = ravel(right)

    return right


def ttParamVariancePart(U):
    n = U.n
    r = U.r

    U = ttRightOrthogonalize(U)
    cores = tt.tensor.to_list(U)

    u = cores[0]
    u = reshape(u,[n[0],r[1]])

    u = u.dot(u.T)

    return numpy.diag(u)


def ttDeterministicRes(U,Delta):
    n = U.n
    r = U.r
    d = U.d

    U = ttRightOrthogonalize(U)
    cores = tt.tensor.to_list(U)

    DetRes = list()
    DetRes.append(reshape(cores[0],[n[0],r[1]]))
    for i in range(1,d):
        u = cores[i]
        uu = rankOneProduct(u,mid=Delta[i])
        uu = rightThreeHalfdot(u,uu)
        for j in range(i-1,0,-1):
            u = cores[j]
            uu = rankOneProduct(u,right=uu)
            uu = rightThreeHalfdot(u,uu)
        u = cores[0]
        u = rankOneProduct(u,right=uu)
        DetRes.append(reshape(u,[n[0],r[1]]))

    return DetRes


def ttDeterministicRightSide(U):
    n = U.n
    d = U.d
    r = U.r

    ttRightOrthogonalize(U)
    cores = tt.tensor.to_list(U)
    u = cores[d-1]
    uu = rankOneProduct(u,mid=ravel(numpy.array(numpy.eye(n[d-1],1))))
    uu = ravel(uu)
    for i in range(d-2,0,-1):
        u = cores[i]
        uu = rankOneProduct(u,mid=ravel(numpy.array(numpy.eye(n[i],1))),right=uu)
        uu = ravel(uu)

    return uu


# Usage:
# U: Solution in tt
# vecs: Matrix with evaluated polynomials as columns for each component
# Output: Vector over ranks corresponding to a FEM function in the first component
# Sum up
def ttRightHalfdot(U,vecs):
    cores = tt.tensor.to_list(U)
    n = U.n
    d = U.d

    right = rankOneProduct(cores[d-1],mid=vecs[:n[d-1],d-2])
    right = ravel(right)
    for i in range(d-2,0,-1):
        right = rankOneProduct(cores[i],mid=vecs[:n[i],i-1],right=right)
        right = ravel(right)
    right = rankOneProduct(cores[0],right=right)
    right = ravel(right)

    return right

# Usage:
# U: Solution in tt (needs to be right-orthogonalized!)
# k: Stochastic component that shall be estimated (between 1 and M+1)
# Output: core0*right*core0' = Matrix
def ttComponentGPCEstimator(U,k):
    cores = tt.tensor.to_list(U)
    n = U.n
    d = U.d
    r = U.r

    core0 = reshape(cores[0],[n[0],r[1]])
    if k < d:
        right = cores[k]
        right = right[:,n[k]-1,:]
        right = right.dot(right.T)
        for i in range(k-1,0,-1):
            right = rankOneProduct(cores[i],right=right)
            right = rightThreeHalfdot(cores[i],right)
    else:
        right = numpy.eye(r[1],r[1])

    return core0, right



def ttCollapseTail(U,V):
    ucores = tt.tensor.to_list(U)
    vcores = tt.tensor.to_list(V)
    n = U.n
    d = U.d
    r = U.r

    uright = reshape(ucores[d-1],[r[d-1],n[d-1]])
    vright = reshape(vcores[d-1],[r[d-1],n[d-1]])
    right = vright.dot(uright.T)
    for i in range(d-2,0,-1):
        uright = rankOneProduct(ucores[i],right=right)
        right = rightThreeHalfdot(vcores[i],uright)
    uright = reshape(ucores[0],[n[0],r[1]])
    vright = reshape(vcores[0],[n[0],r[1]])
    right = vright.dot(right.dot(uright.T))

    return right



def ttCollapseTailOnly(U,V):
    ucores = tt.tensor.to_list(U)
    vcores = tt.tensor.to_list(V)
    n = U.n
    d = U.d
    r = U.r

    uright = reshape(ucores[d-1],[r[d-1],n[d-1]])
    vright = reshape(vcores[d-1],[r[d-1],n[d-1]])
    right = vright.dot(uright.T)
    for i in range(d-2,0,-1):
        uright = rankOneProduct(ucores[i],right=right)
        right = rightThreeHalfdot(vcores[i],uright)

    return right


# Returns a [M,M]-list of (d_0 x r_1)-matrices and one single (d_0 x r_1)-matrix
# For each [m_1,m_2] take the corresponding matrix and on the right side of the scalar product always the same,
# sum over k_1 = 1,...,r_1
def ttCanonicalFEMEstimator(U,Delta):
    n = U.n
    d = U.d
    r = U.r
    cores = tt.tensor.to_list(U)
    w0 = reshape(cores[0],[n[0],r[1]])

    # Make canonical list of TT tensors
    wlist = [U]
    for i in range(1,d):
        vcores = list(cores)
        uu = rankOneProduct(cores[i],mid=Delta[i])
        vcores[i] = uu
        V = tt.tensor.from_list(vcores)
        wlist.append(V)

    return wlist,w0


def ttParamProdFirstVec(U,Delta):
    d = U.d
    cores = tt.tensor.to_list(U)

    # Make canonical list of TT tensors
    wlist = [ttGetVec(U, numpy.zeros(d-1))]
    for i in range(1,d):
        vcores = list(cores)
        uu = rankOneProduct(cores[i],mid=Delta[i])
        vcores[i] = uu
        V = tt.tensor.from_list(vcores)
        wlist.append(ttGetVec(V, numpy.zeros(d-1)))

    return wlist