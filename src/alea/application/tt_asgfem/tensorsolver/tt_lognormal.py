from __future__ import division

__author__ = 'pfeffer'

import numpy as np
import tt
import alea.polyquad.polynomials as polys
import scipy
# FEniCS imports
import ffc
from dolfin import cells, FunctionSpace, interactive, DOLFIN_VERSION_MAJOR, refine
new_dolfin = DOLFIN_VERSION_MAJOR > 1
if new_dolfin:
    from dolfin import FiniteElement

# TODO: check reshapes!!!!!!!!!!!!

def own_tensordot(a, b, axes=2):
    """
    Compute tensor dot product along specified axes for arrays >= 1-D.

    Given two tensors (arrays of dimension greater than or equal to one),
    `a` and `b`, and an array_like object containing two array_like
    objects, ``(a_axes, b_axes)``, sum the products of `a`'s and `b`'s
    elements (components) over the axes specified by ``a_axes`` and
    ``b_axes``. The third argument can be a single non-negative
    integer_like scalar, ``N``; if it is such, then the last ``N``
    dimensions of `a` and the first ``N`` dimensions of `b` are summed
    over.

    Parameters
    ----------
    a, b : array_like, len(shape) >= 1
        Tensors to "dot".

    axes : int or (2,) array_like
        * integer_like
          If an int N, sum over the last N axes of `a` and the first N axes
          of `b` in order. The sizes of the corresponding axes must match.
        * (2,) array_like
          Or, a list of axes to be summed over, first sequence applying to `a`,
          second to `b`. Both elements array_like must be of the same length.

    See Also
    --------
    dot, einsum

    Notes
    -----
    Three common use cases are:
        * ``axes = 0`` : tensor product :math:`a\\otimes b`
        * ``axes = 1`` : tensor dot product :math:`a\\cdot b`
        * ``axes = 2`` : (default) tensor double contraction :math:`a:b`

    When `axes` is integer_like, the sequence for evaluation will be: first
    the -Nth axis in `a` and 0th axis in `b`, and the -1th axis in `a` and
    Nth axis in `b` last.

    When there is more than one axis to sum over - and they are not the last
    (first) axes of `a` (`b`) - the argument `axes` should consist of
    two sequences of the same length, with the first axis to sum over given
    first in both sequences, the second axis second, and so forth.
    """
    try:
        iter(axes)
    except:
        axes_a = list(range(-axes, 0))
        axes_b = list(range(0, axes))
    else:
        axes_a, axes_b = axes
    try:
        na = len(axes_a)
        axes_a = list(axes_a)
    except TypeError:
        axes_a = [axes_a]
        na = 1
    try:
        nb = len(axes_b)
        axes_b = list(axes_b)
    except TypeError:
        axes_b = [axes_b]
        nb = 1

    a, b = np.asarray(a), np.asarray(b)
    as_ = a.shape
    nda = len(a.shape)
    bs = b.shape
    ndb = len(b.shape)
    equal = True
    if na != nb:
        equal = False
    else:
        for k in range(na):
            if as_[axes_a[k]] != bs[axes_b[k]]:
                equal = False
                break
            if axes_a[k] < 0:
                axes_a[k] += nda
            if axes_b[k] < 0:
                axes_b[k] += ndb
    if not equal:
        raise ValueError("shape-mismatch for sum")

    # Move the axes to sum over to the end of "a"
    # and to the front of "b"
    notin = [k for k in range(nda) if k not in axes_a]
    newaxes_a = notin + axes_a
    N2 = 1
    for axis in axes_a:
        N2 *= as_[axis]
    newshape_a = (-1, N2)
    olda = [as_[axis] for axis in notin]

    notin = [k for k in range(ndb) if k not in axes_b]
    newaxes_b = axes_b + notin
    N2 = 1
    for axis in axes_b:
        N2 *= bs[axis]
    newshape_b = (N2, -1)
    oldb = [bs[axis] for axis in notin]

    at = a.transpose(newaxes_a).reshape(newshape_a)
    bt = b.transpose(newaxes_b).reshape(newshape_b)
    out = np.empty((at.shape[0], bt.shape[-1]))
    np.dot(at, bt, out=out)
    return out.reshape(olda + oldb)

def generate_lognormal_tt(B, hdeg, ranks, bxmax, theta=0, rho=1, acc=1e-7):

    d = B.shape[1]
    n = [B.shape[0]] + (d-1)*[hdeg]
    cores = d*[[]]

    right = np.ones([B.shape[0],1])
    for iexp in range(d-1,0,-1):
        coeffs = np.zeros([B.shape[0],hdeg,ranks[iexp+1]])
        tau = np.exp(theta*rho*bxmax[iexp-1])
        fac = np.exp(np.power(B[:,iexp]*tau,2)/2)
        for ig in range(hdeg):
            precoeff = (np.power(B[:,iexp]*tau,ig)/np.sqrt(float(np.math.factorial(ig))))*fac
            for k in range(ranks[iexp+1]):
                coeffs[:,ig,k] = precoeff*right[:,k]

        coeffs = np.reshape(coeffs, (B.shape[0], hdeg*ranks[iexp+1]))
        a, b, c = np.linalg.svd(coeffs, full_matrices=0)
        rr = min([ranks[iexp],a.shape[1]])
        rr = min(rr,max(len(np.nonzero(np.array(b) > acc)[0].tolist()),1))
        a = a[:, :rr]
        b = np.diag(b[:rr])
        c = c[:rr, :]
        right = a.dot(b)
        cores[iexp] = np.reshape(c,(rr,hdeg,ranks[iexp+1]))
        ranks[iexp] = rr

    cores[0] = np.zeros([1,B.shape[0],ranks[1]])
    for idet in range(B.shape[0]):
        for k in range(ranks[1]):
            cores[0][0,idet,k] = np.exp(B[idet,0])*right[idet,k]

    U = tt.tensor.from_list(cores)
    return U


def get_coeff_upper_bound(B):

    bxmax = np.zeros(B.shape[1]-1)

    for i in range(B.shape[1]-1):
        bxmax[i] = max(abs(B[:,i+1]))

    return bxmax

def sample_lognormal_tt(U, sample, bmax, theta=0, rho=1, all_cores_stochastic=False):

    if isinstance(U, tt.vector):
        d = U.d
        n = U.n
        ranks = U.r
        cores = tt.tensor.to_list(U)
    else:
        d = len(U)
        n = [core.shape[1] for core in U]
        ranks = [core.shape[2] for core in U]
        ranks.insert(0, U[0].shape[0])
        cores = U
    # print("samples: {}".format(sample))
    herms = polys.StochasticHermitePolynomials(0,1,normalised=True)
    right = [1]
    if all_cores_stochastic is True:
        right = cores[-1]
        assert (len(right.shape) == 3)
        assert (right.shape[2] == 1)
        right = right[:, :, 0]
        tau = np.exp(-theta * rho * bmax[-1])

        right = np.dot(right, np.array(herms.eval(right.shape[1]-1, tau*sample[-1], all_degrees=True)))
        for i in range(d - 2, -1, -1):
            tau = np.exp(-theta * rho * bmax[i])
            # print("{} rank[{}]={}, n[{}]={}".format(i, i, ranks[i], i, n[i]))
            core = cores[i]
            if len(core.shape) == 5:
                if len(right.shape) == 1:
                    assert (core.shape[3] == 1)
                    core = core[:, :, :, 0, :]
                    right = np.einsum('ijal, l->ija', core, right)
                    right = np.einsum('ija, a->ij', right, np.array(herms.eval(right.shape[2]-1, tau*sample[i],
                                                                               all_degrees=True)))
                elif len(right.shape) == 2:
                    right = np.einsum('ijakl, lk->ija', core, right)
                    right = np.einsum('ija, a->ij', right, np.array(herms.eval(right.shape[2]-1, tau * sample[i],
                                                                               all_degrees=True)))
                else:
                    print("this should not have happen. len(right.shape) = {}".format(len(right.shape)))
                    exit()
            elif len(core.shape) == 3:
                assert (len(right.shape) == 1)
                right = np.einsum('ial, l->ia', core, right)
                right = np.dot(right, np.array(herms.eval(right.shape[1]-1, tau*sample[i], all_degrees=True)))
            else:
                print("this should not have happen. len(core.shape) = {}".format(len(core.shape)))
                exit()

        return right
    else:
        for i in range(d-1,0,-1):
            left = np.reshape(cores[i],(ranks[i]*n[i],ranks[i+1]))
            left = np.reshape(left.dot(right),(ranks[i],n[i]))
            right = []
            tau = np.exp(-theta*rho*bmax[i-1])
            for k in range(ranks[i]):
                right.append(sum([left[k,c]*herms.eval(c,tau*sample[i-1],all_degrees=False) for c in range(left.shape[1])]))

        right = np.array(right)
        left = np.reshape(cores[0],(n[0],ranks[1]))
        left = np.array(left.dot(right))
        return left


def generate_operator_tt(U, ranks, hdegs, M):
    if isinstance(U,tt.vector):
        d = U.d
        cores = tt.tensor.to_list(U)

        opcores = [None]
        for iexp in range(M):
            opcorei = np.zeros([ranks[iexp+1],hdegs[iexp],hdegs[iexp],ranks[iexp+2]])
            for nu1 in range(hdegs[iexp]):
                for nu2 in range(hdegs[iexp]):
                    opcorei[:,nu1,nu2,:] = sum([cores[iexp+1][:,mu,:]*hermite_triprod(mu,nu1,nu2) for mu in range(cores[iexp+1].shape[1])])

            opcores.append(opcorei)

        if M <= d:
            for lia in range(M, d+1):
                opcores[M] = np.einsum('lnmk, kt->lnmt', opcores[M], cores[lia][:, 0, :])

    elif isinstance(U,list):
        cores = U
        d = len(cores) - 1
        print(ranks)
        opcores = [None]
        for iexp in range(M):
            print("  {}".format(cores[iexp].shape))
            opcorei = np.zeros([ranks[iexp+1],hdegs[iexp],hdegs[iexp],ranks[iexp+2]])
            for nu1 in range(hdegs[iexp]):
                for nu2 in range(hdegs[iexp]):
                    # print("mu={}, nu1={}, nu2={}".format(cores[iexp].shape[1], nu1, nu2))
                    opcorei[:,nu1,nu2,:] = sum([cores[iexp][:,mu,:]*hermite_triprod(mu,nu1,nu2) for mu in range(cores[iexp].shape[1])])

            opcores.append(opcorei)

        if 0 < M <= d:
            # buffer_core = np.zeros((opcores[M].shape[0],opcores[M].shape[1], opcores[M].shape[2], 1))
            for lia in range(M, d+1):
                opcores[-1] = np.einsum('lnmk, kt->lnmt', opcores[-1], cores[lia][:, 0, :])
            # print opcores[M].shape
            # buffer_core[:, :, :, 0] = opcores[M]
            # opcores[M] = buffer_core

    return opcores


def hermite_triprod(i, j, k):
    from alea.application.tt_asgfem.tensorsolver.tt_residual import triple_hermite_coef
    if True:
        return triple_hermite_coef(i, j, k)
    if bool((i+j-k) % 2) or k < abs(i-j) or k > i+j:
        return 0
    s = (i+j-k)/2
    fac = np.math.factorial
    return np.sqrt(float(fac(i)*fac(j)*fac(k)))/(fac(s)*fac(i-s)*fac(j-s))
    # return np.sqrt(float(fac(i)*fac(j)*fac(i+j-2*k)/fac(k)*fac(k-i)*fac(k-j)))


def extract_mean(U):

    d = U.d
    n = U.n
    ranks = U.r
    cores = tt.tensor.to_list(U)

    right = [1]
    for m in range(d-1,0,-1):
        right = np.dot(cores[m][:,0,:],right)

    return np.dot(np.reshape(cores[0],[n[0],ranks[1]]),right)


def mult_diag(d, mtx, left=True):
    """Multiply a full matrix by a diagonal matrix.
    This function should always be faster than dot.

    Input:
      d -- 1D (N,) array (contains the diagonal elements)
      mtx -- 2D (N,N) array

    Output:
      mult_diag(d, mts, left=True) == dot(diag(d), mtx)
      mult_diag(d, mts, left=False) == dot(mtx, diag(d))
    """
    if left:
        return (d*mtx.T).T
    else:
        return d*mtx


def tt_cont_coeff(af, mesh, M, ranks, hdegs, bmax, theta=0, rho=1, acc=1e-10, mesh_dofs=500, quad_degree=2,
                  coef_mesh_cache=None, domain="undefined", scale=1.0):
    # TODO: make a new mesh (with fe_h)
    # prepare quadrature points and weights as well as the mesh
    from alea.utils.tictoc import TicToc
    with TicToc(sec_key="Create coefficient", key="1. Create mesh and FunctionSpace", active=True, do_print=False):
        if coef_mesh_cache is not None:
            mesh = coef_mesh_cache
        _, qw = ffc.quadratureelement.create_quadrature('triangle', quad_degree)
        qw *= 2.0                                       # not quite sure why...
        if not new_dolfin:
            while mesh_dofs > 0 and FunctionSpace(mesh, 'Quadrature', quad_degree).dim() < mesh_dofs:
                mesh = refine(mesh)  # uniformly refine mesh
            Q = FunctionSpace(mesh, "Quadrature", quad_degree)
        else:

            if mesh_dofs > 0:
                fe = FiniteElement(family="Quadrature", cell=mesh.ufl_cell(), degree=quad_degree, quad_scheme="default")
                while FunctionSpace(mesh, fe).dim() < mesh_dofs:
                    mesh = refine(mesh)  # uniformly refine mesh
                    fe = FiniteElement(family="Quadrature", cell=mesh.ufl_cell(), degree=quad_degree,
                                       quad_scheme="default")
            Q = FunctionSpace(mesh, fe)
        # if coef_mesh_cache is None:
        #     from dolfin import File
        #     File("{}-{}-mesh.xml".format(domain, scale)) << mesh
    with TicToc(sec_key="Create coefficient", key="2. Get quadrature nodes and weights", active=True, do_print=False):
        gdim = mesh.geometry().dim()
        if not new_dolfin:
            qpoints = Q.dofmap().tabulate_all_coordinates(mesh)
        else:
            qpoints = Q.tabulate_dof_coordinates()
        qpoints = qpoints.reshape((-1, gdim))
        N = len(qpoints)
        vcells = [c.volume() for c in cells(mesh)]
        qweights = np.hstack([qw * cv for cv in vcells])

    # bmax = np.zeros(M)
    # for m in range(M):
    #     bmax[m] = max([abs(af[m+1][0][0](p)) for p in qpoints])

    def sigma(m):
        return np.exp(theta*rho*bmax[m-1])

    def eval_bfun(m,nu):
        sigmam = sigma(m)
        fun = af[m][0][0]
        from dolfin import interpolate
        fun_Q = interpolate(fun, Q)
        # retval_old = [(np.power(fun(p)*sigmam, nu) / np.sqrt(float(np.math.factorial(nu)))) *
        #                np.exp(np.power(fun(p)*sigmam, 2) / 2)
        #               for p in qpoints]
        retval = (np.power(fun_Q.vector()[:]*sigmam, nu) / np.sqrt(float(np.math.factorial(nu)))) * np.exp(np.power(
            fun_Q.vector()[:]*sigmam, 2) / 2)
        # print("err={}".format(np.linalg.norm(retval - retval_old)))
        return retval

    cores = M*[[]]
    red_coeffs = np.ones([1, N])

    with TicToc(sec_key="Create coefficient", key="3. Create Cores", active=True, do_print=False):
        for m in range(M, 0, -1):
            # print("compute coef core {}/{}".format(M-m, M))
            with TicToc(sec_key="Create coefficient", key="3. compute correlation matrix", active=True, do_print=False):
                # Compute correlation matrix
                # EB = np.zeros([hdegs[m-1],ranks[m+1],N])
                # print("red_coef shape {}".format(red_coeffs.shape))
                with TicToc(sec_key="Create coefficient", key="3.0 create EB", active=True, do_print=False):
                    EB = np.array([eval_bfun(m, nu) * red_coeffs for nu in range(hdegs[m-1])])

                # print("EB 1 {}".format(EB.shape))
                with TicToc(sec_key="Create coefficient", key="3.1 reshape EB", active=True, do_print=False):
                    EB = np.reshape(EB, [hdegs[m-1]*ranks[m+1], N])
                # print("EB 2 {}".format(EB.shape))

                with TicToc(sec_key="Create coefficient", key="3.2 multiply column with weights".format(m), active=True,
                            do_print=False):
                    C = EB*qweights
                # print("C 1 {}".format(C.shape))

                with TicToc(sec_key="Create coefficient", key="3.4 numpy dot".format(m), active=True, do_print=False):
                    C = np.dot(C, EB.T)

                # print("C 2 {}".format(C.shape))
            with TicToc(sec_key="Create coefficient", key="3.5 compute SVD", active=True, do_print=False):
                # Eigenvalue decomposition
                vecs, vals, _ = scipy.linalg.svd(C,full_matrices=False, check_finite=False)
                ranks[m] = min(ranks[m],max(len(np.nonzero(np.array(np.sqrt(vals)) > acc)[0].tolist()),1))
                vecs = vecs[:,:ranks[m]]
                vecs = vecs.T
            with TicToc(sec_key="Create coefficient", key="3.6 compute reduced basis", active=True, do_print=False):
                # Compute reduced basis functions
                EB = np.reshape(EB,[hdegs[m-1],ranks[m+1],N])
                vecs = np.reshape(vecs,[ranks[m],hdegs[m-1],ranks[m+1]])
                red_coeffs = np.tensordot(vecs,EB,axes=([1,2],[0,1]))
                cores[m-1] = vecs
        TicToc.sortedTimes()
        TicToc.clear()
    return cores


def sample_cont_coeff(tt_coeff, af, points, sample, ranks, hdegs, bmax, theta=0, rho=1, dg_mesh=None, fs=None,
                      af_cache=None):
    from alea.utils.tictoc import TicToc
    M = len(tt_coeff)
    N = len(points)
    from dolfin import interpolate
    dg_fs = None
    if dg_mesh is not None:
        dg_fs = FunctionSpace(dg_mesh, 'DG', 0)

    # bmax = np.zeros(M)
    # for m in range(M):
    #     bmax[m] = max([abs(af[m+1][0][0](p)) for p in points])

    def tau(m):
        return np.exp(-theta*rho*bmax[m-1])

    def sigma(m):
        return np.exp(theta*rho*bmax[m-1])

    def eval_bfun(m,nu):
        sigmam = sigma(m)
        fun = af[m][0][0]
        if dg_mesh is not None:
            dg_fun = interpolate(fun, dg_fs)
            return (np.power(dg_fun.vector()[:]*sigmam, nu) / np.sqrt(float(np.math.factorial(nu)))) * \
                   np.exp(np.power(dg_fun.vector()[:]*sigmam, 2) / 2)
        elif fs is not None:
            if af_cache is not None:
                func = af_cache.af[m]
            else:
                func = interpolate(fun, fs)
                func = func.vector()[:]
            with TicToc(sec_key="sample cont coef", key="  bfun retval", active=True, do_print=False):
                _retval = (np.power(func * sigmam, nu) / np.sqrt(float(np.math.factorial(nu)))) * \
                    np.exp(np.power(func * sigmam, 2) / 2)
            return _retval
        else:
            return [(np.power(fun(p) * sigmam, nu) / np.sqrt(float(np.math.factorial(nu)))) * \
                   np.exp(np.power(fun(p) * sigmam, 2) / 2) for p in points]

    herms = polys.StochasticHermitePolynomials(0,1,normalised=True)
    right = [1]
    left = np.ones([1, N])
    for m in range(M, 0, -1):
        # EB = np.zeros([hdegs[m-1],ranks[m+1],N])
        if af_cache is not None:
            with TicToc(sec_key="sample cont coef", key="  EB", active=True, do_print=False):
                EB = np.array([af_cache.b_fun[m][nu] * left for nu in range(hdegs[m-1])])
            # with TicToc(sec_key="sample cont coef", key="  left2", active=True, do_print=False):
            #     EB2 = af_cache.b_fun[m][:hdegs[m-1], :]
            #      core = np.reshape(tt_coeff[m-1], (tt_coeff[m-1].shape[0]*tt_coeff[m-1].shape[1], tt_coeff[m-1].shape[2]), order="F")
            #     core = np.dot(core, left)
            #     core = np.reshape(core, (tt_coeff[m-1].shape[0], tt_coeff[m-1].shape[1], core.shape[1]), order="F")
            #     left2 = np.sum(core * EB2, axis=1)
            #     # left_stretch = np.tile(left, (hdegs[m-1], 1, 1)).transpose((1, 0, 2))
            #     # EB2 = (EB2*left_stretch).transpose((1, 0, 2))
        else:
            with TicToc(sec_key="sample cont coef", key="  EB", active=True, do_print=False):
                EB = np.array([eval_bfun(m, nu) * left for nu in range(hdegs[m-1])])
        # print("sample {}/{}".format(m-1, len(sample)))
        # print("hdegs: {}".format(hdegs))
        with TicToc(sec_key="sample cont coef", key="  herms", active=True, do_print=False):
            hb = [herms.eval(nu, tau(m)*sample[m-1], all_degrees=False) for nu in range(hdegs[m-1])]
        with TicToc(sec_key="sample cont coef", key="  left", active=True, do_print=False):
            left = np.tensordot(tt_coeff[m-1], EB, axes=([1, 2], [0, 1]))
        # assert np.allclose(left, left2)
        with TicToc(sec_key="sample cont coef", key="  right 1", active=True, do_print=False):
            right = np.tensordot(tt_coeff[m-1], right, axes=(2, 0))
        with TicToc(sec_key="sample cont coef", key="  right 2", active=True, do_print=False):
            right = right.dot(hb)
    with TicToc(sec_key="sample cont coef", key="  final b0", active=True, do_print=False):
        if dg_mesh is not None:
            dg_fun = interpolate(af[0][0][0], dg_fs)
            b0 = np.exp(dg_fun.vector()[:])
            # b0_old = [np.exp(af[0][0][0](p)) for p in points]
            # print("err={}".format(np.linalg.norm(b0-b0_old)))
        elif fs is not None:
            if af_cache is not None:
                dg_fun = af_cache.af[0]
                # print("use cache at end")
            else:
                dg_fun = interpolate(af[0][0][0], fs)
                dg_fun = dg_fun.vector()[:]
            b0 = np.exp(dg_fun)
        else:
            b0 = [np.exp(af[0][0][0](p)) for p in points]
    # newleft = np.array(left[k, :]*b0 for k in range(ranks[1]))
    with TicToc(sec_key="sample cont coef", key="  newleft", active=True, do_print=False):
        # newleft = np.zeros([ranks[1], N])
        # for k in range(ranks[1]):
        #     newleft[k, :] = left[k, :]*b0
        newleft = left * b0
        # assert np.allclose(newleft, newleft2)
    with TicToc(sec_key="sample cont coef", key="  retval", active=True, do_print=False):
        retval = newleft.T.dot(right)
    # TicToc.sortedTimes(sec_sorted=True)
    return retval


def sample_cont_coeff_2(tt_coeff, af, points, sample, ranks, hdegs, bmax, theta=0, rho=1):

    M = len(tt_coeff)
    N = len(points)

    # bmax = np.zeros(M)
    # for m in range(M):
    #     bmax[m] = max([abs(af[m+1][0][0](p)) for p in points])

    def tau(m):
        return np.exp(-theta*rho*bmax[m-1])

    def sigma(m):
        return np.exp(theta*rho*bmax[m-1])

    def eval_bfun(m,nu):
        sigmam = sigma(m)
        fun = af[m][0][0]
        return [(np.power(fun(p)*sigmam, nu) / np.sqrt(float(np.math.factorial(nu)))) * np.exp(np.power(fun(p)*sigmam, 2) / 2) for p in points]

    herms = polys.StochasticHermitePolynomials(0,1,normalised=True)
    right = [1]
    left = np.ones([1,N])
    for m in range(M,0,-1):
        EB = np.zeros([hdegs[m-1],ranks[m+1],N])
        hb = np.zeros(hdegs[m-1])
        for nu in range(hdegs[m-1]):
            eb = eval_bfun(m,nu)
            hb[nu] = herms.eval(nu,tau(m)*sample[m-1],all_degrees=False)
            for k in range(ranks[m+1]):
                EB[nu,k,:] = eb*left[k,:]
        left = np.tensordot(tt_coeff[m-1],EB,axes=([1,2],[0,1]))
        right = np.tensordot(tt_coeff[m-1],right,axes=(2,0))
        right = right.dot(hb)

    b0 = [np.exp(af[0][0][0](p)) for p in points]
    newleft = np.zeros([ranks[1],N])
    for k in range(ranks[1]):
        newleft[k,:] = left[k,:]*b0

    return newleft.T.dot(right)


def fully_disc_first_core(tt_coeff, af, points, ranks, hdegs, bmax, theta=0, rho=1, dg_mesh=None, cg_mesh=None,
                          femdegree=1):
    M = len(tt_coeff)
    N = len(points)
    from dolfin import interpolate
    fs = None
    if dg_mesh is not None:
        fs = FunctionSpace(dg_mesh, 'DG', femdegree-1)
    if cg_mesh is not None:
        fs = FunctionSpace(cg_mesh, 'CG', femdegree)
    # bmax = np.zeros(M)
    # for m in range(M):
    #     bmax[m] = max([abs(af[m+1][0][0](p)) for p in points])

    def sigma(m):
        return np.exp(theta*rho*bmax[m-1])

    def eval_bfun(m,nu):
        with TicToc(sec_key="create discrete first core", key="  sigma", active=False, do_print=False):
            sigmam = sigma(m)
        with TicToc(sec_key="create discrete first core", key="fun", active=False, do_print=False):
            fun = af[m][0][0]
        with TicToc(sec_key="create discrete first core", key="retval", active=True, do_print=False):
            if dg_mesh is not None or cg_mesh is not None:
                # retval_old = [(np.power(fun(p)*sigmam, nu) / np.sqrt(np.math.factorial(nu))) *
                #           np.exp(np.power(fun(p)*sigmam, 2) / 2) for p in points]
                discrete_fun = interpolate(fun, fs)
                retval = (np.power(discrete_fun.vector()[:] * sigmam, nu) / np.sqrt(float(np.math.factorial(nu)))) * \
                          np.exp(np.power(discrete_fun.vector()[:] * sigmam, 2) / 2)
                # print("err={}".format(np.linalg.norm(retval_old - retval)))
            else:
                retval = [(np.power(fun(p) * sigmam, nu) / np.sqrt(float(np.math.factorial(nu)))) *
                          np.exp(np.power(fun(p) * sigmam, 2) / 2) for p in points]
        return retval

    from alea.utils.tictoc import TicToc
    left = np.ones([1,N])
    # print("go for M={}".format(M))
    for m in range(M,0,-1):
        # EB = np.zeros([hdegs[m-1],ranks[m+1],N])
        with TicToc(sec_key="create discrete first core", key="create EB", active=True, do_print=False):
            EB = np.array([eval_bfun(m, nu) * left for nu in range(hdegs[m-1])])
        # for nu in range(hdegs[m-1]):
        #     eb = eval_bfun(m, nu)
        #     EB[nu, :, :] = eb*left
        #     # for k in range(ranks[m+1]):
        #     #    EB[nu,k,:] = eb*left[k,:]
        # with TicToc(key="  tensordot", active=True, do_print=False):
        #     left = np.tensordot(tt_coeff[m-1],EB,axes=([1,2],[0,1]))
        # print("tt_coef:{} x {} :EB".format(tt_coeff[m-1].shape, EB.shape))
        with TicToc(sec_key="create discrete first core", key="own tensordot", active=True, do_print=False):
            left = own_tensordot(tt_coeff[m - 1], EB, axes=([1, 2], [0, 1]))
        # print(" left = {}".format(left.shape))
        # einsum is much much slower. think about that for the estimators
        # with TicToc(key="  einsum", active=True, do_print=False):
        #     _left = np.einsum('ijk, jkn->in', tt_coeff[m-1], EB)
        # print("err={}".format(np.linalg.norm(left - _left)))
        # print("tt {} * EB {} = left {}".format(tt_coeff[m-1].shape, EB.shape, left.shape))
    with TicToc(sec_key="create discrete first core", key="first core", active=True, do_print=False):
        if dg_mesh is not None or cg_mesh is not None:
            discrete_fun = interpolate(af[0][0][0], fs)
            b0 = np.exp(discrete_fun.vector()[:])
            # b0_old = [np.exp(af[0][0][0](p)) for p in points]
            # print("err={}".format(np.linalg.norm(b0-b0_old)))
        else:
            b0 = [np.exp(af[0][0][0](p)) for p in points]
        # newleft = np.array(left[k, :]*b0 for k in range(ranks[1]))
        newleft = np.zeros([left.shape[0],N])
        # print newleft.shape
        # print left.shape
        # print ranks
        for k in range(left.shape[0]):
            newleft[k,:] = left[k,:]*b0

    return np.reshape(newleft.T,[1,N,left.shape[0]])
'''


def fully_disc_first_core(tt_coeff, af, points, ranks, hdegs, theta=0, rho=1):

    M = len(tt_coeff)
    N = len(points)

    bmax = np.zeros(M)
    for m in range(M):
        bmax[m] = max([abs(af[m+1][0][0](p)) for p in points])

    def tau(m):
        return np.exp(-theta*rho*bmax[m-1])

    def sigma(m):
        return np.exp(theta*rho*bmax[m-1])

    def eval_bfun(m,nu):
        sigmam = sigma(m)
        fun = af[m][0][0]
        return [(np.power(fun(p)*sigmam, nu) / np.sqrt(np.math.factorial(nu))) * np.exp(np.power(fun(p)*sigmam, 2) / 2) for p in points]

    left = np.ones([1,N])
    for m in range(M,0,-1):
        EB = np.zeros([hdegs[m-1],ranks[m+1],N])
        for nu in range(hdegs[m-1]):
            eb = eval_bfun(m,nu)
            for k in range(ranks[m+1]):
                EB[nu,k,:] = eb*left[k,:]
        left = np.tensordot(tt_coeff[m-1],EB,axes=([1,2],[0,1]))

    b0 = [np.exp(af[0][0][0](p)) for p in points]
    newleft = np.zeros([ranks[1],N])
    for k in range(ranks[1]):
        newleft[k,:] = left[k,:]*b0

    return newleft.T
'''
