import numpy as np

import tt
from tt_util import reshape
import copy
import xerus as xe


class smatrix(object):
    def __init__(self,cores=None,eps=1e-14):
        if cores is None:
            self.n = 0
            self.m = 0
            self.d = 0
            self.r = 0
            self.cores = []
            return
        else:
            self.d = len(cores)
            n = np.zeros(self.d,dtype=int)
            m = np.zeros(self.d,dtype=int)
            r = np.zeros(self.d+1,dtype=int)
            r[0] = 1
            for i in range(self.d):
                if i == 0:
                    n[0] = cores[0][0].shape[0]
                    m[0] = cores[0][0].shape[1]
                    r[1] = len(cores[0])
                else:
                    n[i] = cores[i].shape[1]
                    m[i] = cores[i].shape[2]
                    r[i+1] = cores[i].shape[3]
            self.n = n
            self.m = m
            self.r = r
            self.cores = cores
            return

    @staticmethod
    def to_list(smat):
        return smat.cores

    def __repr__(self):
        res = "This is a %d-dimensional sparse tt operator \n" % self.d
        r = self.r
        d = self.d
        n = self.n
        m = self.m
        for i in range(d):
            res = res + ("r(%d)=%d, n(%d)=%d, m(%d)=%d \n" % (i, r[i],i,n[i],i,m[i]))
        res = res + ("r(%d)=%d \n" % (d,r[d]))
        return res

    def __getitem__(self, index):
        if len(index) == 1:
            return self.cores[index]

    def __add__(self,other):
        if other is None:
            return self
        c = smatrix()
        c.n = self.n
        c.m = self.m
        c.d = self.d
        r = np.ones(self.d+1,dtype=int)
        newcores = self.d*[[]]
        newcores[0] = list(self.cores[0])
        for i in range(other.r[1]):
            newcores[0].append(other.cores[0][i])
        for j in range(1,self.d):
            r[j] = self.r[j] + other.r[j]
            if j < self.d-1:
                newcore = np.zeros([r[j], self.n[j], self.m[j], self.r[j+1] + other.r[j+1]])
                newcore[:self.r[j],:,:,:self.r[j+1]] = self.cores[j]
                newcore[self.r[j]:r[j],:,:,self.r[j+1]:(self.r[j+1] + other.r[j+1])] = other.cores[j]
            else:
                newcore = np.zeros([r[self.d-1], self.n[self.d-1], self.m[self.d-1], self.r[self.d]])
                newcore[:self.r[self.d-1],:,:,:self.r[self.d]] = self.cores[self.d-1]
                newcore[self.r[self.d-1]:r[self.d-1],:,:,:self.r[self.d]] = other.cores[self.d-1]
            newcores[j] = newcore
        c.cores = newcores
        c.r = r
        c.round(1e-18)
        return c

    def __sub__(self,other):
        c = self + (-other)
        return c

    def __neg__(self):
        c = self.copy()
        for i in range(len(c.cores[0])):
            c.cores[0][i] = (-1)*c.cores[0][i]
        return c

    def __matmul__(self,other):
        return "not yet implemented"

    def __rmul__(self,other):
        if hasattr(other,'__matmul__'):
            return other.__matmul__(self)
        else:
            return "not yet implemented"

    def __mul__(self,other):
        if hasattr(other,'__matmul__'):
            return self.__matmul__(other)
        else:
            return "not yet implemented"

    def __kron__(self,other):
        """ Kronecker product of two TT-matrices """
        return "not yet implemented"

    def norm(self):
        c = self.round(0)
        norm = 0
        for i in range(len(c.cores[0])):
            norm += np.power(np.linalg.norm(c.cores[0][i].todense()),2)
        return np.sqrt(norm)

    def round(self,eps):
        """ Computes an approximation to a sparse tt operator with accuracy EPS and right orthogonalizes the tensor"""
        # Disclaimer: This method should not be used for rounding as of now, only for cutting off redundancies.

        c = self.copy()
        for i in range(c.d-1,0,-1):
            right = reshape(c.cores[i], [c.r[i], c.n[i] * c.m[i] * c.r[i+1]])

            left, mid, right = np.linalg.svd(right,full_matrices=0)

            if i > 1:
                u = reshape(c.cores[i-1], [c.r[i-1] * c.n[i-1] * c.m[i-1], c.r[i]])
            else:
                u0 = c.cores[0]
            c.r[i] = max(len(np.nonzero(np.array(mid) > eps)[0].tolist()),1)

            right = right[:c.r[i], :]
            c.cores[i] = reshape(right, [c.r[i], c.n[i], c.m[i], c.r[i+1]])

            if i > 1:
                left = np.dot(u, np.dot(left[:, :c.r[i]], np.diag(mid[:c.r[i]])))
                c.cores[i-1] = reshape(left, [c.r[i-1], c.n[i-1], c.m[i-1], c.r[i]])
            else:
                left = np.dot(left[:, :c.r[1]], np.diag(mid[:c.r[1]]))
                leftlist = []
                for j in range(c.r[1]):
                    leftlist.append(sum([u0[k]*left[k,j] for k in range(len(u0))]))
                c.cores[0] = leftlist

        return c

    def copy(self):
        """ Creates a copy of the TT-matrix """
        c = smatrix()
        c.cores = copy.deepcopy(self.cores)
        c.n = self.n.copy()
        c.m = self.m.copy()
        c.d = self.d
        c.r = self.r.copy()
        return c

    def full(self):
        """ Computes full matrix """
        c = self.copy()
        right = [1]
        for i in range(c.d-1,0,-1):
            left = reshape(c.cores[i],[c.r[i]*c.n[i]*c.m[i],c.r[i+1]])
            right = reshape(left.dot(right),[c.r[i],c.n[i],c.m[i],np.prod(c.n[i+1:c.d]),np.prod(c.m[i+1:c.d])])
            right = np.transpose(right, axes=(0,1,3,2,4))
            right = reshape(right,[c.r[i],np.prod(c.n[i:c.d])*np.prod(c.m[i:c.d])])

        right = reshape(right,[c.r[1],np.prod(c.n[1:c.d]),np.prod(c.m[1:c.d])])
        Afull = np.zeros([np.prod(c.n),np.prod(c.m)])
        for j in range(c.r[1]):
            Afull += np.kron(right[j,:,:],c.cores[0][j].todense())

        return Afull

    def matvec(self,other):
        """ Computes matrix vector product with TT tensor """
        c = self.copy()
        n = c.n
        m = c.m
        mr = c.r
        d = c.d
        ur = other.r
        mcores = c.cores
        ucores = tt.tensor.to_list(other)

        vr = mr*ur
        vcores = d*[[]]
        for i in range(d-1,0,-1):
            core = np.tensordot(mcores[i],ucores[i],axes=(2,1))
            core = np.transpose(core,axes=(0,3,1,2,4))
            vcores[i] = reshape(core,[vr[i],n[i],vr[i+1]])

        core = np.zeros([n[0],mr[1],ur[1]])
        for uk in range(ur[1]):
            for mk in range(mr[1]):
                core[:,mk,uk] = mcores[0][mk].dot(ucores[0][0,:,uk])
        vcores[0] = reshape(core,[1,n[0],vr[1]])

        v = tt.tensor.from_list(vcores)
        return v

    def trace(self):
        retval = 1
        for lia in range(len(self.cores)-1, -1, -1):
            core = self.cores[lia]
            if lia > 0:
                assert (len(core.shape) == 4)
                if lia == len(self.cores)-1:
                    retval = np.squeeze(np.trace(core, axis1=1, axis2=2))
                    continue
                retval = np.squeeze(np.dot(np.trace(core, axis1=1, axis2=2), retval))
                continue
            retval = np.sum([core[lic] * retval[lic] for lic in range(len(core))])
        return retval

    def to_xerus_op(self):
        dim_list = []
        for lia in range(self.d):
            dim_list.append(self.n[lia])
            dim_list.append(self.m[lia])
        retval = xe.TTOperator(dim_list)
        curr_comp = np.zeros((1, self.n[0], self.m[0], self.r[1]))
        for k in range(self.r[1]):
            curr_comp[0, :, :, k] = self.cores[0][k].toarray()
        curr_comp = xe.Tensor.from_ndarray(curr_comp)
        curr_comp.use_sparse_representation()
        retval.set_component(0, curr_comp)
        for lia in range(self.d-1):
            curr_comp = xe.Tensor.from_ndarray(self.cores[lia+1])
            retval.set_component(lia+1, curr_comp)
        retval.canonicalize_left()
        return retval
