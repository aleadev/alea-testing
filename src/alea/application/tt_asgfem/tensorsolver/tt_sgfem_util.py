from __future__ import division

import tt
from tt_util import ravel, reshape, ttTruncate, ttNormalize, ttRightOrthogonalize, rankOneProduct
import numpy as np
from alea.utils.tictoc import TicToc
from scipy.linalg import solve as scsolve
import scipy.linalg as sc
import scipy.sparse as sps
from scipy.sparse.linalg import splu
from scipy.sparse.linalg import spsolve as sparsesolve
from joblib import delayed
from scipy.sparse.linalg import LinearOperator
from scipy.sparse.linalg import cg
from scipy.sparse.linalg import factorized
from alea.application.bayes.alea_util.solver.SylvesterIterativeSolver import SylvesterIterativeSolver


def csr_vappend(a,b):
    """ Takes in 2 csr_matrices and appends the second one to the bottom of the first one.
    Much faster than scipy.sparse.vstack but assumes the type to be csr and overwrites
    the first matrix instead of copying it. The data, indices, and indptr still get copied."""

    print a.shape
    print b.shape
    a.data = np.hstack((a.data,b.data))
    a.indices = np.hstack((a.indices,b.indices))
    a.indptr = np.hstack((a.indptr,(b.indptr + a.nnz)[1:]))
    a._shape = (a.shape[0]+b.shape[0], a.shape[1] + b.shape[1])
    return a


def tt_op_dofs(A):
    dofs = 0
    r = A.r
    n = A.n
    m = A.m
    for i in range(A.d):
        if i < A.d-1:
            dofs += r[i]*n[i]*m[i]*r[i+1] - r[i+1]*r[i+1]
        else:
            dofs += r[i]*n[i]
    return dofs


def tt_op_tensor_dofs(A):
    dofs = 0
    r = A.r
    n = A.n
    for i in range(A.d):
        if i < A.d-1:
            dofs += r[i]*n[i]*r[i+1] - r[i+1]*r[i+1]
        else:
            dofs += r[i]*n[i]
    return dofs

# determine overall dofs of tt tensor
def tt_dofs(V):
    dofs = 0
    rr = V.r
    nn = V.n
    for i in range(V.d):
        if i < V.d-1:
            dofs = dofs + rr[i]*nn[i]*rr[i+1] - rr[i+1]*rr[i+1]
        else:
            dofs = dofs + rr[i]*nn[i]
    return dofs


def miniLeftMatrixCG(A,b,ls,rs,x0=None,acc=1e-16,maxit=10000):
    if x0 is None:
        x0 = np.array(np.random.rand(b.shape[0],1))

    x = reshape(x0,[ls,rs])
    ax = ravel(A.dot(x))
    x = ravel(x)
    r = b - ax
    d = r
    i = 0
    # conv = np.linalg.norm(r)
    while np.linalg.norm(r) > acc and i < maxit:
        # if i % 20:
            # print("        local mini CG step {} - conv {}".format(i, conv))
        d = reshape(d,[ls,rs])
        z = ravel(A.dot(d))
        d = ravel(d)
        alpha = np.dot(r,r)/np.dot(d,z)
        x = x + alpha*d
        rr = r - alpha*z
        beta = np.dot(rr,rr)/np.dot(r,r)
        r = rr
        # conv = np.linalg.norm(r)
        d = r + beta*d
        i += 1
        if i > 9998:
            print "Preconditioning:", np.linalg.norm(r)

    return x


def kronprod(A, B):
    retval = sps.kron(A, B)
    return retval

def kronprod_data(A, B, active=True):
    # with TicToc(sec_key="sps kron own", key="to coo 1", active=active, do_print=False):
        # assert B is already a sparse matrix
        # B = B.tocoo()
    # If B is already in sparse format. data will not change (scipy.sparse.compressed.tocoo())
    nnz = B.nnz
    with TicToc(sec_key="sps kron own", key="repeat A", active=active, do_print=False):
        data = A.reshape(-1).repeat(nnz)

    # compute block entries
    with TicToc(sec_key="sps kron own", key="data mul", active=active, do_print=False):
        data = data.reshape(-1, nnz) * B.data
    with TicToc(sec_key="sps kron own", key="data reshape", active=active, do_print=False):
        data = data.reshape(-1)

    return data

def kronprod_data_slow(A, data_list, active=True):
    nnz = data_list.shape[1]

    with TicToc(sec_key="sps kron own new", key="transpose A", active=active, do_print=False):
        data = A.transpose(1, 0, 2)
        # print B[0].shape
        # print data.shape
    with TicToc(sec_key="sps kron own new", key="create data ", active=active, do_print=False):
        data = data.reshape(A.shape[1], -1).repeat(nnz).reshape(A.shape[1], -1, nnz)
        # print data.shape
    # print data.shape
    with TicToc(sec_key="sps kron own new", key="transpose data", active=active, do_print=False):
        data = data.transpose(1, 0, 2)
    # print data_list.shape
    with TicToc(sec_key="sps kron own new", key="data mul ", active=active, do_print=False):
        data = data * data_list
    # print data.shape
    with TicToc(sec_key="sps kron own new", key="data sum ", active=active, do_print=False):
        data = np.sum(data, axis=1)
    with TicToc(sec_key="sps kron own new", key="data reshape ", active=active, do_print=False):
        data = data.reshape(-1)

    return data

def create_kronprod_coo_matrix(A, B, active=False):
    nnz = B.nnz
    with TicToc(sec_key="sps kron own", key="arange row", active=active, do_print=False):
        _Arow = np.arange(0, A.shape[0]).repeat(A.shape[0])
    with TicToc(sec_key="sps kron own", key="tile column", active=active, do_print=False):
        _Acol = np.tile(np.arange(0, A.shape[0]), A.shape[0])
    with TicToc(sec_key="sps kron own", key="to coo", active=active, do_print=False):
        _B = B.tocoo()
    with TicToc(sec_key="sps kron own", key="repeat row", active=active, do_print=False):
        row = _Arow.repeat(nnz)
    with TicToc(sec_key="sps kron own", key="mult row", active=active, do_print=False):
        row *= _B.shape[0]
    with TicToc(sec_key="sps kron own", key="repeat col", active=active, do_print=False):
        col = _Acol.repeat(nnz)
    with TicToc(sec_key="sps kron own", key="mult col", active=active, do_print=False):
        col *= _B.shape[0]
    with TicToc(sec_key="sps kron own", key="reshape both", active=active, do_print=False):
        row, col = row.reshape(-1, nnz), col.reshape(-1, nnz)
    with TicToc(sec_key="sps kron own", key="mult both", active=active, do_print=False):
        row += _B.row
        col += _B.col
    with TicToc(sec_key="sps kron own", key="reshape both 2", active=active, do_print=False):
        row, col = row.reshape(-1), col.reshape(-1)
    return row, col


def ttCompPCG(A,b,x=None,right=None,P=None,maxit=1000,acc=1e-12, ret_conv=False, para=None):

    if right is None:
        # print("    solve linalg system: {}".format(b.shape[0]))
        # with TicToc(sec_key="ALS-solver", key="    **** np linalg solve size: {}****".format(b.shape[0]), active=True,
        #             do_print=False):
        #    x = np.linalg.solve(A,b)
        # assert (np.allclose(A, A.T))
        with TicToc(sec_key="ALS-solver", key="    **** sc linalg spsolve size: {}****".format(b.shape[0]), active=True,
                    do_print=False):
            x = sc.solve(A, b, sym_pos=False, check_finite=False, overwrite_a=False, overwrite_b=False, assume_a="sym")
            # if not np.linalg.norm(A.dot(x) - b) < 1e-11:
            #     print("!!! what?")
            #     x = np.linalg.solve(A, b)
        return x
    else:
        if x is None:
            x = np.array(np.random.rand(b.shape[0], 1))
        def Asum(a, r, xx, para=None):

            # region ### slow old version
            # with TicToc(sec_key="ALS-solver", key="    **** res ****", active=True, do_print=False):
            #     xx = reshape(xx,[1,a[0].shape[1],r.shape[2]])
            #     res = 0
            #     for lia in range(len(a)):
            #         res += rankOneProduct(xx, mid=a[lia], right=r[:, lia, :])
            #     res = ravel(res)
            # region ### end slow old version
            # ### sparse first version
            # with TicToc(sec_key="ALS-solver", key="    **** res (sparse first) ****", active=True, do_print=False):
            #     xx = reshape(xx, [a[0].shape[1], r.shape[2]])
            #     res = np.sum([np.dot(a[lia].dot(xx), r[:, lia, :]) for lia in range(len(a))], axis=0)
            #     res = ravel(res)
            # ### end sparse first version
            # ### sparse last version (faster)
            # with TicToc(sec_key="ALS-solver", key="    **** res (sparse last) ****", active=True, do_print=False):
            #     with TicToc(sec_key="ALS-solver", key="    **** res reshape ****", active=True, do_print=False):
            #         _x = reshape(xx, [a[0].shape[1], r.shape[2]])
            #     with TicToc(sec_key="ALS-solver", key="    **** res for loop ****", active=True, do_print=False):
            #         res = np.sum([a[lia].dot(np.dot(_x, r[:, lia, :])) for lia in range(len(a))], axis=0)
            #     with TicToc(sec_key="ALS-solver", key="    **** res ravel ****", active=True, do_print=False):
            #         res = ravel(res)
            # if True:
            #     return res
            # ###
            # ### pre compute x*r
            with TicToc(sec_key="ALS-solver", key="    **** res (pre compute x*r) ****", active=True,
                        do_print=False):
                with TicToc(sec_key="ALS-solver", key="    **** res reshape x ****", active=True, do_print=False):
                    _x = reshape(xx, [a[0].shape[1], r.shape[2]])
                with TicToc(sec_key="ALS-solver", key="    **** res reshape r ****", active=True, do_print=False):
                    _r = reshape(r, [r.shape[0], r.shape[1]*r.shape[2]])
                with TicToc(sec_key="ALS-solver", key="    **** res dot x, r ****", active=True, do_print=False):
                    buf = np.dot(_x, _r)
                with TicToc(sec_key="ALS-solver", key="    **** res reshape back ****", active=True, do_print=False):
                    buf = reshape(buf, [a[0].shape[1], r.shape[1], r.shape[2]])
                with TicToc(sec_key="ALS-solver", key="    **** res for loop ****", active=True,
                            do_print=False):
                    res = np.sum([a[_lia].dot(buf[:, _lia, :]) for _lia in range(len(a))], axis=0)
                with TicToc(sec_key="ALS-solver", key="    **** res ravel ****", active=True, do_print=False):
                    res = ravel(res)

            # region tensor dot method (faster for small physical systems)
            # with TicToc(sec_key="ALS-solver ultimate", key="    **** res sparse ultimate ****", active=True,
            #             do_print=False):
            #     with TicToc(sec_key="ALS-solver ultimate", key="    **** res reshape ****", active=True,
            #                 do_print=False):
            #         xx = reshape(xx, [a[0].shape[1], r.shape[2]])
            #     with TicToc(sec_key="ALS-solver ultimate", key="    **** sparse dot loop ****", active=True,
            #                 do_print=False):
            #         res_alt = np.zeros((a[0].shape[0], len(a), a[0].shape[1]))
            #         for lia in range(len(a)):
            #             res_alt[:, lia, :] = sparseAdot(a[lia], xx)
            #     with TicToc(sec_key="ALS-solver ultimate", key="    **** sparse transpose ****", active=True,
            #                 do_print=False):
            #         res_alt = np.transpose(res_alt, (1, 0, 2))
            #     with TicToc(sec_key="ALS-solver ultimate", key="    **** ten reshape ****", active=True,
            #                 do_print=False):
            #         res_alt = reshape(res_alt, (res_alt.shape[0], res_alt.shape[1] * res_alt.shape[2]))
            #         r_reshaped = reshape(r, (r.shape[0], r.shape[1] * r.shape[2]))
            #     with TicToc(sec_key="ALS-solver ultimate", key="    **** tensor dot ****", active=True,
            #                 do_print=False):
            #         res_alt = ravel(np.dot(res_alt, r_reshaped.T))
            # region end tensor dot method

            # assert(np.linalg.norm(res - res_alt) < 1e-10)

            return res
        if P["method"] == "stored rank":
            with TicToc(sec_key="ALS-solver", key="    **** preconditioner creation ****", active=True,
                        do_print=False):
                precond = P["precond"]
                Pmat = 0
                for lia in range(len(precond)):
                    Pmat += sps.diags(sps.kron(right[:, lia, :], precond[lia]).diagonal(), 0, format="dia")

        # region Methods with internal or no iterations
        if str(P["method"]).startswith("direct"):
            with TicToc(sec_key="ALS-solver", key="    **** create operator ****", active=True, do_print=False):
                # print("  build operator")
                if para is not None:
                    Pmat = sum(para(delayed(kronprod)(right[:, lia, :], A[lia]) for lia in range(len(A))))
                else:
                    # for lia in range(len(A)-1):
                    #     assert (np.allclose(A[lia].indices, A[lia+1].indices))
                    #     assert (np.allclose(A[lia].indptr, A[lia+1].indptr))
                    output_format = (right.shape[0]*A[0].shape[0], right.shape[0]*A[0].shape[0])
                    with TicToc(sec_key="ALS-solver", key="create operator: create pattern", active=True,
                                do_print=False):
                        row, col = create_kronprod_coo_matrix(right[:, 0, :], A[0])

                    with TicToc(sec_key="ALS-solver", key="create operator: sum kron", active=True, do_print=False):
                        data = sum(kronprod_data(right[:, lia, :], A[lia], active=False) for lia in range(len(A)))
                    # with TicToc(sec_key="ALS-solver", key="create operator: sum kron new", active=True,
                    #             do_print=False):
                    #     data2 = kronprod_data_slow(right, P["data"])
                    #     # print data2.shape
                    # assert (np.linalg.norm(data - data2) < 1e-10)
                    with TicToc(sec_key="ALS-solver", key="create operator: create coo", active=True, do_print=False):
                        Pmat = sps.coo_matrix((data, (row, col)), shape=output_format)
                    # print("  solve system")
                    # for lia in range(len(A)):
                    #     assert np.allclose(A[lia].todense(), A[lia].todense().T)
                    #     assert np.allclose(right[:, lia, :], right[:, lia, :].T)
                    # print("    all symmetric")
            if P["method"] == "direct":
                with TicToc(sec_key="ALS-solver", key="    **** solve direct system: {}****".format(b.shape[0]),
                            active=True, do_print=False):
                    if ret_conv is True:
                        return sparsesolve(Pmat, b), [0]
                    else:
                        return sparsesolve(Pmat, b)
            elif P["method"] == "direct lu":
                with TicToc(sec_key="ALS-solver", key="    **** factorize LU system: {}****".format(b.shape[0]),
                            active=True, do_print=False):

                    Pmat = factorized(Pmat.tocsc())
                    with TicToc(sec_key="ALS-solver", key="    **** solve LU system: {}****".format(b.shape[0]),
                                active=True, do_print=False):
                        if ret_conv is True:
                            return Pmat(b), [0]
                        else:
                            return Pmat(b)
        if P["method"] == "scipy cg":
            with TicToc(sec_key="ALS-solver", key="    **** create operator ****", active=True, do_print=False):

                def mv(_x):
                    return Asum(A, right, _x)
                A_scipy = LinearOperator((b.shape[0], b.shape[0]), matvec=mv)
            with TicToc(sec_key="ALS-solver", key="    **** create precond ****", active=True, do_print=False):
                A_precond = sps.kron(sps.eye(right.shape[2], right.shape[2]), P["precond"])
            with TicToc(sec_key="ALS-solver", key="    **** solve system ****", active=True, do_print=False):
                return cg(A_scipy, b, tol=acc, maxiter=maxit, M=A_precond)
        if P["method"] == "sylvester glcr":
            with TicToc(sec_key="ALS-solver", key="    **** create object ****", active=True, do_print=False):
                r_list = [right[:, lia, :] for lia in range(right.shape[1])]
                solver = SylvesterIterativeSolver(A, r_list, b.reshape((A[0].shape[0], right.shape[2]), order="F"))
            with TicToc(sec_key="ALS-solver", key="    **** solve sylvester ****", active=True, do_print=False):
                if ret_conv:
                    return solver.GLCR(acc, maxit, X=x.reshape((A[0].shape[0], right.shape[2]), order="F"))
                return solver.GLCR(acc, maxit)[0], [0]
        # endregion

        # print("right shape: {}".format(right.shape))
        # print("A[0].shape: {}".format(A[0].shape))
        # print("x shape: {}".format(x.shape))
        # print("x type: {}".format(type(x)))
        r = b - Asum(A,right,x)
        # print("r.shape={}".format(r.shape))
        if P is None:
            z = r
        else:
            if not isinstance(P, dict):
                with TicToc(sec_key="ALS-solver", key="    **** preconditioner mini CG****", active=True,
                            do_print=False):
                    z = miniLeftMatrixCG(P, r, P.shape[1], right.shape[2])

            else:
                precond = P["precond"]
                if P["method"] == "none":
                    z = r
                elif P["method"] == "cg":
                    with TicToc(sec_key="ALS-solver", key="    **** preconditioner mini CG ****", active=True,
                                do_print=False):
                        z = miniLeftMatrixCG(precond, r, precond.shape[1], right.shape[2])
                elif P["method"] == "spsolve":
                    with TicToc(sec_key="ALS-solver", key="    **** preconditioner spsolve ****", active=True,
                                do_print=False):
                        z = np.reshape(scsolve(precond, r.reshape(precond.shape[1], right.shape[2], order='F')),
                                       [precond.shape[1]*right.shape[2]], order='F')
                elif P["method"] == "direct diag":
                    precond = P["precond_inv"]
                    with TicToc(sec_key="ALS-solver", key="    **** preconditioner direct diag ****", active=True,
                                do_print=False):
                        z = np.reshape(precond.dot(r.reshape(precond.shape[1], right.shape[2], order='F')),
                                       [precond.shape[1]*right.shape[2]], order='F')
                elif P["method"] == "stored":
                    precond = P["precond_inv"]
                    with TicToc(sec_key="ALS-solver", key="    **** preconditioner stored ****", active=True,
                                do_print=False):
                        z = np.reshape(precond.dot(r.reshape(precond.shape[1], right.shape[2], order='F')),
                                       [precond.shape[1]*right.shape[2]], order='F')
                        z = np.squeeze(np.asarray(z))
                elif P["method"] == "stored rank":
                    # precond = P["precond"]
                    with TicToc(sec_key="ALS-solver", key="    **** preconditioner stored ****", active=True,
                                do_print=False):
                        # print("right shape: {}".format(right.shape))
                        # _z = np.zeros((precond[0].shape[1], right.shape[2]))
                        # rr = r.reshape(precond[0].shape[1], right.shape[2], order='F')
                        # print("r shape: {}".format(r.shape))
                        z = ravel(Pmat.dot(r))
                        # for lia in range(right.shape[2]):
                        #     # print("precond {} has norm: {}".format(lia, np.linalg.norm(precond[lia])))
                        #     _z[:, lia] = precond[lia].dot(rr[:, lia]) # * right[0, lia] **(-1)
                        #     _z[:, lia] *= np.sum(right[lia, :, lia]) ** (-1)
                        # _z = np.squeeze(_z.reshape(precond[0].shape[1] * right.shape[2], order='F'))
                        # assert (np.allclose(r, _z))
                        # z = _z
                        # print("z shape: {}".format(z.shape))
                else:
                    raise ValueError("no preconditioner type given")
        p = z
        k = 0
        conv = [np.linalg.norm(r)]
        # print("  {}".format(conv[-1]))
        with TicToc(sec_key="ALS-solver", key="    **** local CG ****", active=True, do_print=False):
            while k < maxit and conv[-1] > acc:
                # if k % 10 == 0:
                #     print("      local CG step {} - conv {}".format(k, conv[-1]))
                with TicToc(sec_key="ALS-solver", key="    **** local CG: Asum****", active=True, do_print=False):
                    # print("right shape: {}".format(right.shape))
                    # print("p shape: {}".format(p.shape))
                    # print("p type: {}".format(type(p)))
                    # print("A[0].shape: {}".format(A[0].shape))
                    ap = Asum(A, right, p)
                with TicToc(sec_key="ALS-solver", key="    **** local CG: rdot/pdot ****", active=True, do_print=False):
                    a = r.dot(z)/p.dot(ap)
                with TicToc(sec_key="ALS-solver", key="    **** local CG: update x/r ****", active=True,
                            do_print=False):
                    x += a*p
                    rr = r - a*ap
                if P is None:
                    zz = rr
                else:

                    if not isinstance(P, dict):
                        with TicToc(sec_key="ALS-solver", key="    **** preconditioner mini CG****", active=True,
                                    do_print=False):
                            zz = miniLeftMatrixCG(P, rr, P.shape[1], right.shape[2], z)
                    else:
                        precond = P["precond"]
                        if P["method"] == "none":
                            zz = rr
                        elif P["method"] == "cg":
                            with TicToc(sec_key="ALS-solver", key="    **** preconditioner mini CG ****", active=True,
                                        do_print=False):
                                zz = miniLeftMatrixCG(precond, rr, precond.shape[1], right.shape[2], z)
                        elif P["method"] == "spsolve":
                            with TicToc(sec_key="ALS-solver", key="    **** preconditioner spsolve ****", active=True,
                                        do_print=False):
                                zz = np.reshape(scsolve(precond, rr.reshape(precond.shape[1], right.shape[2],
                                                                            order='F')),
                                                [precond.shape[1] * right.shape[2]], order='F')
                        elif P["method"] == "direct diag":
                            precond = P["precond_inv"]
                            with TicToc(sec_key="ALS-solver", key="    **** preconditioner direct diag ****",
                                        active=True, do_print=False):
                                zz = np.reshape(precond.dot(rr.reshape(precond.shape[1], right.shape[2], order='F')),
                                                [precond.shape[1] * right.shape[2]], order='F')
                        elif P["method"] == "stored":
                            precond = P["precond_inv"]
                            with TicToc(sec_key="ALS-solver", key="    **** preconditioner stored ****", active=True,
                                        do_print=False):
                                zz = np.reshape(precond.dot(rr.reshape(precond.shape[1], right.shape[2], order='F')),
                                                [precond.shape[1] * right.shape[2]], order='F')
                                zz = np.squeeze(np.asarray(zz))
                        elif P["method"] == "stored rank":
                            # precond = P["precond_inv"]
                            with TicToc(sec_key="ALS-solver", key="    **** preconditioner stored ****", active=True,
                                        do_print=False):
                                # print("rr shape: {}".format(rr.shape))
                                zz = ravel(Pmat.dot(rr))
                                # print("zz shape: {}".format(zz.shape))
                                # zz = np.zeros((precond[0].shape[1], right.shape[2]))
                                # _rr = rr.reshape(precond[0].shape[1], right.shape[2], order='F')
                                # for lia in range(right.shape[2]):
                                #     print("precond {} has norm: {}".format(lia, np.linalg.norm(precond[lia])))
                                #     zz[:, lia] = precond[lia].dot(_rr[:, lia]) # * right[0, lia] ** (-1)
                                #     zz[:, lia] *= np.sum(right[lia, :, lia]) ** (-1)
                                # zz = np.squeeze(zz.reshape(precond[0].shape[1] * right.shape[2], order='F'))
                                # #assert(np.allclose(rr, zz))
                                # zz = rr
                                # #print("zz shape: {}".format(zz.shape))
                        else:
                            raise ValueError("no preconditioner type given")
                with TicToc(sec_key="ALS-solver", key="    **** local CG: beta ****", active=True, do_print=False):
                    beta = zz.dot(rr)/z.dot(r)
                with TicToc(sec_key="ALS-solver", key="    **** local CG: z/r/p ****", active=True, do_print=False):
                    z = zz
                    r = rr
                    p = zz + beta*p
                k += 1
                with TicToc(sec_key="ALS-solver", key="    **** local CG: norm ****", active=True, do_print=False):
                    if ret_conv is True:
                        conv.append(np.linalg.norm(r))
                    else:
                        conv = [np.linalg.norm(r)]
                #if 100 < k < 102:

                #    print("calculate EW of Matrix")
                #    if P is None:
                #        print("!!! No preconditioner")
                #    Amat = np.zeros([A[0].shape[0]*right.shape[0],A[0].shape[1]*right.shape[2]])
                #    for i in range(len(A)):
                #        print("  compute matrix of rank: {}/{} size: {}x{}".format(i+1, len(A), Amat.shape[0],
                #                                                                    Amat.shape[1]))
                #        Amat = Amat + np.kron(right[:,i,:],A[i].todense())
                    # eigs = np.linalg.eig(Amat)[0]
                #    print("  begin eigs computation")
                #    eigs = sc.linalg.eigvals(Amat)
                #    print("  sort eigs")
                #    eigs = np.sort(eigs)
                #    print("  smallest EW: {} \n   largest EW: {}".format(eigs[0], eigs[-1]))
                #    for j in range(eigs.shape[0]):
                #        if eigs[j] < -1e-14:
                #            print "  negative A0eigsj:", eigs[j]
                #            exit()
                #    print "CG:", np.linalg.norm(r)
    if ret_conv is True:
        return x, conv
    return x

class myMicroOperator(object):
    def __init__(self, Alist, Rlist):
        """

        :param Alist:  a list of r matrices A[k], k = 1, ..., r
        :param Rlist:  a list of r vectors  R[k,:]
        """
        dim = Alist[0].shape[0]

        assert len(Alist) == len(Rlist)
        self.r = len(Alist)

        for k in range(self.r):
            assert len(Alist[k].shape[0]) == dim and len(Alist[k].shape[1]) == dim

        self.Alist = Alist
        self.Rlist = [sps.diags(Rlist[k]) for k in range(self.r)]

        self.Pleft = splu(Alist[0])
        self.Pright= sps.diags(Rlist[0]**(-1))

        self.left = [sps.identity(dim)] + [self.Pleft.solve(self.Alist[k]) for k in range(2,self.r)]
        self.right = [sps.identity(self.r)] + [sps.diags(Rlist[k] * Rlist[0]**(-1)) for k in range(2,self.r)]


    def dot(self, X):

        return sum( (self.left[k].dot(X)).dot(self.right[k]) for k in range(self.r))

    def getP(self):
        """
        Used to change right hand side, since inverse matrix is computed here!
        :return:
        """
        return self.Pleft, self.Pright

def ttCompPCG2(A,b,x=None,right=None,maxit=1000,acc=1e-12, ret_conv=False):

    if x is None:
        x = np.array(np.random.rand(b.shape[0], 1))

    A_1 = np.linalg.inv(A[0].todense())
    D_1 = np.diag(right[0, :])
    b = np.dot(A_1, np.dot(b, D_1))

    r_list = [right[k, :] for k in range(right.shape[1])]
    A = myMicroOperator(A, r_list)
    
    # print("right shape: {}".format(right.shape))
    # print("A[0].shape: {}".format(A[0].shape))
    # print("x shape: {}".format(x.shape))
    # print("x type: {}".format(type(x)))
    r = b - A.dot(x)
    # print("r.shape={}".format(r.shape))
    z = r

    p = z
    k = 0
    conv = [np.linalg.norm(r)]
    # print("  {}".format(conv[-1]))
    with TicToc(sec_key="ALS-solver", key="    **** local CG ****", active=True, do_print=False):
        while k < maxit and conv[-1] > acc:
            # if k % 10 == 0:
            # print("      local CG step {} - conv {}".format(k, conv[-1]))
            with TicToc(sec_key="ALS-solver", key="    **** local CG: Asum****", active=True, do_print=False):
                # print("right shape: {}".format(right.shape))
                # print("p shape: {}".format(p.shape))
                # print("p type: {}".format(type(p)))
                # print("A[0].shape: {}".format(A[0].shape))
                ap = A.dot(p)
            with TicToc(sec_key="ALS-solver", key="    **** local CG: rdot/pdot ****", active=True, do_print=False):
                a = r.dot(z)/p.dot(ap)
            with TicToc(sec_key="ALS-solver", key="    **** local CG: update x/r ****", active=True,
                        do_print=False):
                x += a*p
                rr = r - a*ap
            zz = rr
            with TicToc(sec_key="ALS-solver", key="    **** local CG: beta ****", active=True, do_print=False):
                beta = zz.dot(rr)/z.dot(r)
            with TicToc(sec_key="ALS-solver", key="    **** local CG: z/r/p ****", active=True, do_print=False):
                z = zz
                r = rr
                p = zz + beta*p
            k += 1
            with TicToc(sec_key="ALS-solver", key="    **** local CG: norm ****", active=True, do_print=False):
                if ret_conv is True:
                    conv.append(np.linalg.norm(r))
                else:
                    conv = [np.linalg.norm(r)]

    if ret_conv is True:
        return x, conv
    return x



def evaluate_residual(V, K, D, B, Normalize=True, acc=1e-14):
    SUMM = 0
    pos = V.ps
    vcore = V.core
    n = V.n
    r = V.r
    M = V.d

    for i in range(M):
        W = V.copy()
        v = vcore[pos[0]-1:pos[1]-1]
        v = reshape(v, [r[0], n[0], r[1]])
        W.core[pos[0]-1:pos[1]-1] = ravel(rankOneProduct(v, mid=K[i]))
        if i > 0:
            v = vcore[pos[i]-1:pos[i+1]-1]
            v = reshape(v,[r[i],n[i],r[i+1]])
            W.core[pos[i]-1:pos[i+1]-1] = ravel(rankOneProduct(v, mid=D[i]))

        if SUMM == 0:
            SUMM = W
        else:
            SUMM = SUMM + W
            if Normalize:
                SUMM = ttNormalize(SUMM, approxtol=acc)
            else:
                SUMM = ttTruncate(SUMM, ranks=M, approxtol=acc)

    BB = tt.rand(n, M, 1)
    bpos = BB.ps
    BB.core[bpos[0]-1:bpos[1]-1] = ravel(B)
    for i in range(1, M):
        BB.core[bpos[i]-1:bpos[i+1]-1] = ravel(np.array(np.eye(n[i],1)))

    RES = SUMM - BB
    return RES, SUMM, BB


def evaluate_residual_norm(V, K, D, B):
    RESList = []
    pos = V.ps
    vcore = V.core
    n = V.n
    r = V.r
    M = V.d

    for i in range(M):
        W = V.copy()
        v = vcore[pos[0]-1:pos[1]-1]
        v = reshape(v, [r[0], n[0], r[1]])
        W.core[pos[0]-1:pos[1]-1] = ravel(rankOneProduct(v, mid=K[i]))
        if i > 0:
            v = vcore[pos[i]-1:pos[i+1]-1]
            v = reshape(v,[r[i],n[i],r[i+1]])
            W.core[pos[i]-1:pos[i+1]-1] = ravel(rankOneProduct(v, mid=D[i]))

        RESList.append(W)

    BB = tt.rand(n, M, 1)
    bpos = BB.ps
    BB.core[bpos[0]-1:bpos[1]-1] = ravel(B)
    for i in range(1, M):
        BB.core[bpos[i]-1:bpos[i+1]-1] = ravel(np.array(np.eye(n[i],1)))

    RESList.append(-BB)

    resnormmat = np.zeros((M+1,M+1))
    for i in range(M+1):
        for j in range(i,M+1):
            resnormmat[i,j] = RESList[i].__dot__(RESList[j])
            resnormmat[j,i] = resnormmat[i,j]

    # RES,_,_ = evaluate_residual(V,K,D,B)
    # resnorm = RES.norm()
    # print "resnorm", resnorm
    return np.sqrt(np.abs(resnormmat.sum()))


def evaluate_residual_mean_energy_norm(V, K, D, B):
    RESList = []
    RESList2 = []
    pos = V.ps
    vcore = V.core
    n = V.n
    r = V.r
    M = V.d

    for i in range(M):
        W = V.copy()
        W2 = V.copy()
        v = vcore[pos[0]-1:pos[1]-1]
        v = reshape(v, [r[0], n[0], r[1]])
        W.core[pos[0]-1:pos[1]-1] = ravel(rankOneProduct(v, mid=K[i]))
        w = ravel(rankOneProduct(v, mid=K[i]))
        if i==0:
            print(K[0].toarray()[0:20, 0:20])
        w = miniLeftMatrixCG(K[0],w,n[0],r[1])
        W2.core[pos[0]-1:pos[1]-1] = w
        if i > 0:
            v = vcore[pos[i]-1:pos[i+1]-1]
            v = reshape(v,[r[i],n[i],r[i+1]])
            W.core[pos[i]-1:pos[i+1]-1] = ravel(rankOneProduct(v, mid=D[i]))
            W2.core[pos[i]-1:pos[i+1]-1] = ravel(rankOneProduct(v, mid=D[i]))

        RESList.append(W)
        RESList2.append(W2)

    BB = tt.rand(n, M, 1)
    BB2 = tt.rand(n, M, 1)
    bpos = BB.ps
    BB.core[bpos[0]-1:bpos[1]-1] = ravel(B)
    bb = ravel(B)
    bb = miniLeftMatrixCG(K[0],bb,n[0],1)
    BB2.core[bpos[0]-1:bpos[1]-1] = bb
    for i in range(1, M):
        BB.core[bpos[i]-1:bpos[i+1]-1] = ravel(np.array(np.eye(n[i],1)))
        BB2.core[bpos[i]-1:bpos[i+1]-1] = ravel(np.array(np.eye(n[i],1)))

    RESList.append(-BB)
    RESList2.append(-BB2)

    resnormmat = np.zeros((M+1,M+1))
    for i in range(M+1):
        for j in range(i,M+1):
            resnormmat[i,j] = RESList[i].__dot__(RESList2[j])
            resnormmat[j,i] = resnormmat[i,j]

    # RES,_,_ = evaluate_residual(V,K,D,B)
    # resnorm = RES.norm()
    # print "resnorm", resnorm
    return np.sqrt(np.abs(resnormmat.sum()))


def increase_tt_rank(V, K, D, B, rank=1, updater=2, acc=1e-12):
    M = V.d
    r = max(V.r)

    if updater == 0:
        RES,_,_ = evaluate_residual(V, K, D, B, Normalize=True)
        UPD = ttTruncate(RES,rank)
    else:
        # RES with ttTruncate ranks=M
        if updater == 1:
            RES,_,_ = evaluate_residual(V, K, D, B, Normalize=False)
            UPD = ttTruncate(RES,rank)
        # Random update
        elif updater == 2:
            if rank == 1:
                UPD = ttRandomDeterministicRankOne(V.n)
            else:
                UPD = tt.rand(V.n,M,rank)

    V = ttNormalize(V+UPD,approxtol=acc)
    # V = ttRightOrthogonalize(V,approxtol=acc)

    return V, r+rank


def tt_add_stochastic_dimension(V,new_dim,gpcp,srank, new_gpcd=2):
    M = V.d

    for ym in new_dim:
        if ym < M-1:
            # increase polynomial degree of existing dimension ym
            gpcp[ym] += 1
            r = V.r
            n = V.n
            vcores = tt.tensor.to_list(V)
            newcore = np.zeros([r[ym+1], n[ym+1]+1, r[ym+2]])
            newcore[:, :n[ym+1], :] = vcores[ym+1]
            vcores[ym+1] = newcore
            V = tt.tensor.from_list(vcores)
        else:
            # enlarge stochastic dimensions
            gpcp.extend([new_gpcd])
            M += 1
            r = V.r
            n = V.n
            vcores = tt.tensor.to_list(V)
            newcore = np.zeros([r[M-2], n[M-2], srank])
            newcore[:, :, 0] = reshape(vcores[M-2], [r[M-2], n[M-2]])
            for jj in range(1, srank):
                newcore[:, :, jj] = np.eye(r[M-2], n[M-2])
            vcores[M-2] = newcore
            vcores.append(np.reshape(np.eye(srank, new_gpcd), [srank, new_gpcd, 1]))
            V = tt.tensor.from_list(vcores)

    return V, gpcp, M


def ttRandomDeterministicRankOne(n):
    U = tt.rand(n,len(n),1)
    pos = U.ps
    ucore = U.core
    for i in range(1,len(n)):
        ucore[pos[i]-1:pos[i+1]-1] = ravel(np.array(np.eye(n[i],1)))

    U.core = ucore
    return U


def sparseAdot(a, b):
    return a.dot(b)

