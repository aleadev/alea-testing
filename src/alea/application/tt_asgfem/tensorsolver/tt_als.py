# region Imports
from __future__ import division
import tt
import numpy as np
from tt_util import (ttRightOrthogonalize, ttMoveOrthogonality, reshape, leftThreeHalfdot, rightThreeHalfdot,
                     rankOneProduct, leftThreeMatrixHalfdot, rightThreeMatrixHalfdot, ravel)
from tt_sgfem_util import ttCompPCG
import tt_sparse_matrix
import logging
from alea.utils.tictoc import TicToc
from tt_sparse_matrix import smatrix
from joblib import Parallel
import multiprocessing as mp
# endregion

logger = logging.getLogger(__name__)                # retrieve logger


def ttALS(A,                                        # type: smatrix
          B,                                        # type: tt.vector
          U,                                        # type: tt.vector
          maxit=1000,                               # type: int
          conv=1e-12,                               # type: float
          approxtol=1e-20,                          # type: float
          P=None,                                   # type: np.array or dict
          converged_by_ref=None,                    # type: bool
          return_local_error=False,                 # type: bool
          reference_solution=None,                  # type: tt.vector
          no_print=False                            # type: bool
          ):
    """
    Computes the solution of the tensor train system \| AX - B\|_F using alternating least squares
    :param A: operator used in the system as tensor train operator with sparse first component
    :param B: rhs as tensor train
    :param U: initial guess as tensor train
    :param maxit: number of maximal iterations (sweeps)
    :param conv: desired accuracy to reach. can Only be estiamted as difference between two iterations
    :param approxtol: approximation tolerance used for rounding tensors in the svd procedure
    :param P: dictionary containing additional information for the local solver
    :param converged_by_ref: boolean to store the convergence property. Do not ask why I introduced that.
    :param return_local_error: Flag to return more verbose information about the solution process
    :param reference_solution: optional reference solution to look at convergence
    :param no_print: flag to stop printing
    :return: Solution as tensor train
    """
    # region Assertions on convergence by ref object
    if converged_by_ref is not None:
        assert(isinstance(converged_by_ref, list))
        assert(len(converged_by_ref) == 1)
        assert(isinstance(converged_by_ref[0], bool))
    # endregion
    # region initial problem print
    if not no_print:
        print("#"*40)
        print("Solve AU=B using ALS")
        print("A: {}".format(A))
        print("B: {}".format(B))
        print("init value: {}".format(U))
        print("using maximal {} sweeps and a micro-iteration approximation tolerance of {}".format(maxit, conv))
        print("#"*40)
    # endregion

    # region def set core
    def set_core(V, k, v):
        cores = tt.tensor.to_list(V)
        cores[k] = v
        return tt.tensor.from_list(cores)
    # endregion

    # region orthogonalize the initial guess
    V = ttRightOrthogonalize(U,approxtol)
    # endregion

    # region some parameters used later
    d = U.d
    n = U.n
    ranks = V.r
    bool1 = True
    local_error_list = []
    first_comp_convergence_list = []
    i = 0

    if False:
        parallel = Parallel(mp.cpu_count(), backend="threading")
    else:
        parallel = None
    # endregion
    for i in range(maxit):
        for it in range(2*d-2):
            if bool1:
                k = it % (d-1)
                with TicToc(sec_key="ALS-solver", key="  **** ALS sweeps ****", active=True, do_print=False):

                    if k == 0:
                        #                           # get list of component tensors of current iterate
                        cores = tt.tensor.to_list(V)
                        vv = ravel(cores[0])        # vectorize the first component
                        #                           # multiply rhs and current iterate
                        b = reshape(ttHalfdot(V, B, 0), [ranks[0]*n[0]*ranks[1]])
                        #                           # get operator list and remainder term
                        a, right = ttHalfdot(V, A, 0)
                        if P is not None:
                            with TicToc(sec_key="ALS-solver", key="    **** ALS first comp PCG ****", active=True,
                                        do_print=False):
                                v, first_comp_convergence = ttCompPCG(a, b, x=vv, right=right, P=P, ret_conv=True,
                                                                      maxit=maxit, para=parallel)
                                first_comp_convergence_list.append(first_comp_convergence)
                                # print("  Micro PCG steps: {}".format(len(first_comp_convergence)))
                                v = reshape(v, [ranks[0], n[0], ranks[1]])
                            V = set_core(V, 0, v)
                            if not isinstance(P, dict):
                                V = ttMoveOrthogonality(V, 0, 1, P=P, approxtol=approxtol)
                            else:
                                if P["method"] == "direct" or P["method"] == "none" or P["method"] == "sylvester glcr" \
                                        or P["method"] == "direct lu":
                                    V = ttMoveOrthogonality(V, 0, 1, approxtol=approxtol)
                                else:
                                    print("you should not be here")
                                    V = ttMoveOrthogonality(V, 0, 1, P=P["precond"], approxtol=approxtol)
                        else:
                            with TicToc(sec_key="ALS-solver", key="    **** ALS first PCG w/o P****", active=True,
                                        do_print=False):
                                v, first_comp_convergence = ttCompPCG(a, b, x=vv, right=right, maxit=maxit,
                                                                      ret_conv=True)
                                first_comp_convergence_list.append(first_comp_convergence)
                                v = reshape(v, [ranks[0], n[0], ranks[1]])
                            V = set_core(V, 0, v)
                            V = ttMoveOrthogonality(V,0,1, approxtol=approxtol)
                    else:
                        b = reshape(ttHalfdot(V, B, k),[ranks[k]*n[k]*ranks[k+1]])
                        a = ttHalfdot(V, A, k)
                        with TicToc(sec_key="ALS-solver", key="    **** ALS direct solve ****", active=True,
                                    do_print=False):
                            v = reshape(ttCompPCG(a, b, maxit=maxit),[ranks[k],n[k],ranks[k+1]])
                        V = set_core(V, k, v)
                        V = ttMoveOrthogonality(V,k,1, approxtol=approxtol)

                ranks = V.r
                # logger.debug('ranks after step %i,%i: %s', i, k, ranks)
                if k == d-2:
                    # print('distance of iteration step {}: {}'.format(k, (U - V).norm()))
                    # logger.debug('distance of iteration steps: %.16f;', (U-V).norm())
                    bool1 = False
            else:
                k = d-1-(it % (d-1))
                with TicToc(sec_key="ALS-solver", key="  **** ALS back sweep ****", active=True, do_print=False):
                    b = reshape(ttHalfdot(V, B, k),[ranks[k]*n[k]*ranks[k+1]])
                    a = reshape(ttHalfdot(V, A, k),[ranks[k]*n[k]*ranks[k+1],ranks[k]*n[k]*ranks[k+1]])
                    with TicToc(sec_key="ALS-solver", key="    **** ALS direct solve back****", active=True,
                                do_print=False):
                        v = reshape(ttCompPCG(a, b, maxit=maxit),[ranks[k],n[k],ranks[k+1]])
                    V = set_core(V, k, v)
                    V = ttMoveOrthogonality(V,k,-1, approxtol=approxtol)
                    ranks = V.r
                    logger.debug('ranks after step %i,%i: %s', i, k, ranks)

                    if k == 1:
                        # print('distance of iteration step {}: {}'.format(k, (U - V).norm()))
                        # logger.info('distance of iteration steps: %.16f;', (U-V).norm())
                        bool1 = True

        no_print_local = False
        if return_local_error is True or True:
            # this was used for Max preconditioner test. Here we need a reference solution
            if reference_solution is not None:
                local_error_list.append(tt.vector.norm(reference_solution - V))
            else:
                local_error_list.append((U-V).norm())
            if i > 100 and local_error_list[-1] > 0.01:
                print("local error of step {} is to huge after this long time -> restart with rank 1 update")
                if return_local_error is False:
                    return U, i, True
                return U, local_error_list, first_comp_convergence_list, i, True
            if i > 20:
                history = local_error_list[i-20:]
                max_h = np.max(history)
                min_h = np.min(history)
                update = min_h * max_h ** (-1)
                print("local error of step {}: {} -> update: {}".format(i, local_error_list[-1], update))
                no_print_local = True
                if update > 0.5 and 1e-7 < min_h <= max_h:
                    print("  no significant increase in acc -> restart with rank 1 update")
                    if return_local_error is False:
                        return U, i, True
                    return U, local_error_list, first_comp_convergence_list, i, True
                if update > 0.5 and min_h < 1e-7:
                    print("  no significant increase in acc  but difference is small -> take current solution")
                    if return_local_error is False:
                        return U, i, False
                    return U, local_error_list, first_comp_convergence_list, i, False
        if not no_print_local:
            print("local error of step {}: {}".format(i, local_error_list[-1]))

        if (U-V).norm() < conv:
            if not no_print:
                print("ALS converged")
            U = V
            if converged_by_ref is not None:
                converged_by_ref[0] = True
            break
        U = V
    if return_local_error is True:
        return U, local_error_list, first_comp_convergence_list, i, False
    else:
        return U, i, False


def ttHalfdot(U, B, k):
    """input has to be orthogonal in the respective component!"""

    # tt_tensor

    if isinstance(B, tt.vector):
        d = U.d
        ucores = tt.tensor.to_list(U)
        bcores = tt.tensor.to_list(B)
    
        left = np.array([1])
        for i in range(k):
            b = rankOneProduct(bcores[i],left=left)
            left = leftThreeHalfdot(ucores[i],b)

        right = np.array([1])
        for i in range(d-1,k,-1):
            b = rankOneProduct(bcores[i],right=right)
            right = rightThreeHalfdot(ucores[i],b)
    
        return rankOneProduct(bcores[k],left=left,right=right)

    # tt_matrix
    elif isinstance(B, tt.matrix):
        d = U.d
        n = U.n
        ur = U.r
        ucores = tt.tensor.to_list(U)
        ar = B.tt.r
        acores = tt.matrix.to_list(B)
    
        left = np.array([[[1]]])
        for i in range(k):
            left = leftThreeMatrixHalfdot(ucores[i],acores[i],left)

        right = np.array([[[1]]])
        for i in range(d-1,k,-1):
            right = rightThreeMatrixHalfdot(ucores[i],acores[i],right)

        left = np.transpose(left,axes=(0,2,1))
        left = reshape(left,[ur[k]*ur[k],ar[k]])
        right = np.transpose(right,axes=(0,2,1))
        right = reshape(right,[ur[k+1]*ur[k+1],ar[k+1]])
        a = reshape(acores[k],[ar[k],n[k]*n[k],ar[k+1]])
        mid = rankOneProduct(a,left=left,right=right)
        mid = reshape(mid,[ur[k],ur[k],n[k],n[k],ur[k+1],ur[k+1]])
        mid = np.transpose(mid,axes=(0,2,4,1,3,5))

        return reshape(mid,[ur[k]*n[k]*ur[k+1],ur[k]*n[k]*ur[k+1]])

    # tt_sparse_matrix
    else:
        assert isinstance(B, tt_sparse_matrix.smatrix)
        d = U.d
        n = U.n
        ur = U.r
        ucores = tt.tensor.to_list(U)

        ar = B.r
        acores = B.cores

        if k == 0:
            right = np.array([[[1]]])
            for i in range(d-1,k,-1):
                right = rightThreeMatrixHalfdot(ucores[i],acores[i],right)

            return acores[0], right

        else:
            u0 = reshape(ucores[0],[n[0],ur[1]])
            left = np.zeros([ur[1],ar[1],ur[1]])
            for k1 in range(ar[1]):
                left[:,k1,:] = u0.T.dot(acores[0][k1].dot(u0))
            for i in range(1,k):
                left = leftThreeMatrixHalfdot(ucores[i],acores[i],left)

            right = np.array([[[1]]])
            for i in range(d-1,k,-1):
                right = rightThreeMatrixHalfdot(ucores[i],acores[i],right)

            left = np.transpose(left,axes=(0,2,1))
            left = reshape(left,[ur[k]*ur[k],ar[k]])
            right = np.transpose(right,axes=(0,2,1))
            right = reshape(right,[ur[k+1]*ur[k+1],ar[k+1]])
            a = reshape(acores[k],[ar[k],n[k]*n[k],ar[k+1]])
            mid = rankOneProduct(a,left=left,right=right)
            mid = reshape(mid,[ur[k],ur[k],n[k],n[k],ur[k+1],ur[k+1]])
            mid = np.transpose(mid,axes=(0,2,4,1,3,5))

            return reshape(mid,[ur[k]*n[k]*ur[k+1],ur[k]*n[k]*ur[k+1]])
