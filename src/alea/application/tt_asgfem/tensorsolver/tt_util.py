import tt
import numpy as np
import scipy
from scipy.linalg import svd, qr


def ravel(A):
    return np.ravel(np.array(A), order='F')


def reshape(vec, shape):
    return np.reshape(vec, shape, order='F')


# Not tested???
def ttNormalize(U, k=None, approxtol=1e-14):
    if k is None:
        k = U.d

    V = ttRightOrthogonalize(U, approxtol)

    vcore = V.core
    n = V.n
    d = V.d
    r = V.r
    pos = V.ps

    for i in range(k - 1):
        v = reshape(vcore[pos[i] - 1:pos[i + 1] - 1], (r[i] * n[i], r[i + 1]))

        a, b, c = np.linalg.svd(v,full_matrices=0)
        # a, b, c = scipy.linalg.svd(v, full_matrices=0)

        x = reshape(vcore[pos[i + 1] - 1:pos[i + 2] - 1], (r[i + 1], n[i + 1] * r[i + 2]))
        if i < d - 2:
            right = vcore[pos[i + 2] - 1:pos[d] - 1]
        r[i + 1] = max(len(np.nonzero(np.array(b) > approxtol)[0].tolist()), 1)
        pos = np.cumsum([1] + (n * r[:d] * r[1:d + 1]).tolist())

        a = a[:, :r[i + 1]]
        b = np.diag(b[:r[i + 1]])
        c = c[:r[i + 1], :]

        x = b.dot(c.dot(x))

        vcore[pos[i] - 1:pos[i + 1] - 1] = ravel(a)
        vcore[pos[i + 1] - 1:pos[i + 2] - 1] = ravel(x)
        if i < d - 2:
            vcore[pos[i + 2] - 1:pos[d] - 1] = ravel(right)

    V.ps = pos
    V.r = r
    V.core = vcore

    return V


def ttRightOrthogonalize(U, approxtol=1e-14):
    if isinstance(U, list):
        U = tt.vector.from_list(U)
    V = U.copy()
    vcore = V.core
    r = V.r
    n = V.n
    d = V.d
    pos = V.ps

    for i in range(d - 1, 0, -1):
        right = vcore[pos[i] - 1:pos[i + 1] - 1]
        right = reshape(right, (r[i], n[i] * r[i + 1]))

        left, mid, right = np.linalg.svd(right,full_matrices=0)
        # left, mid, right = scipy.linalg.svd(right, full_matrices=0)

        u = reshape(vcore[pos[i - 1] - 1:pos[i] - 1], (r[i - 1] * n[i - 1], r[i]))
        if i + 1 < d:
            v = vcore[pos[i + 1] - 1:pos[d] - 1]
        r[i] = max(len(np.nonzero(np.array(mid) > approxtol)[0].tolist()), 1)

        right = right[:r[i], :]
        left = np.dot(u, np.dot(left[:, :r[i]], np.diag(mid[:r[i]])))

        pos = np.cumsum([1] + (n * r[:d] * r[1:d + 1]).tolist())
        vcore[pos[i] - 1:pos[i + 1] - 1] = ravel(right)
        vcore[pos[i - 1] - 1:pos[i] - 1] = ravel(left)
        if i + 1 < d:
            vcore[pos[i + 1] - 1:pos[d] - 1] = v
        vcore = vcore[:pos[d] - 1]

    V.core = vcore
    V.ps = pos
    V.r = r
    return V


def ttMoveOrthogonality(U, k, direction, approxtol=1e-14, P=None):
    d = U.d
    n = U.n
    ranks = U.r
    cores = tt.vector.to_list(U)

    if direction == 1:
        if k == 0 and P is not None:
            u = reshape(cores[0], [n[0], ranks[1]])
            if isinstance(P, list):
                # print("u shape: {}".format(u.shape))
                uau = np.zeros((n[0], ranks[1]))
                for lia in range(ranks[1]):
                    # print("P[{}] shape: {}".format(lia, P[lia].shape))
                    uau[:, lia] = P[lia].dot(u[:, lia])

                    # print("uau shape: {}".format(uau[lia].shape))
                # print("uau shape: {}".format(uau.shape))
                # assert (np.allclose(uau, u))
                uau = np.dot(u.T, uau)
            else:
                uau = np.dot(u.T, (P.dot(u)))
            a,b,c = np.linalg.svd(uau,full_matrices=0)
            # a, b, c = scipy.linalg.svd(uau, full_matrices=0)
            right = reshape(cores[1], [ranks[1], n[1] * ranks[2]])
            ranks[1] = max(len(np.nonzero(np.array(b) > approxtol)[0].tolist()), 1)

            a = a[:, :ranks[1]]
            b = np.diag(b[:ranks[1]])
            s = np.linalg.norm(b)
            c = c[:ranks[1], :]
            c = c / np.sqrt(s)
            d = np.sqrt(b / s)
            dinv = np.linalg.inv(d)

            cores[0] = reshape(np.dot(u.dot(c.T), dinv), [1, n[0], ranks[1]])
            cores[1] = reshape(np.dot(d, (s * c.dot(right))), [ranks[1], n[1], ranks[2]])
            return tt.vector.from_list(cores)
        else:
            u = reshape(cores[k], [ranks[k] * n[k], ranks[k + 1]])
            a,b,c = np.linalg.svd(u,full_matrices=0)
            # a, b, c = scipy.linalg.svd(u, full_matrices=0)
            right = reshape(cores[k + 1], [ranks[k + 1], n[k + 1] * ranks[k + 2]])
            ranks[k + 1] = max(len(np.nonzero(np.array(b) > approxtol)[0].tolist()), 1)

            a = a[:, :ranks[k + 1]]
            b = np.diag(b[:ranks[k + 1]])
            c = c[:ranks[k + 1], :]

            cores[k] = reshape(a, [ranks[k], n[k], ranks[k + 1]])
            cores[k + 1] = reshape(np.dot(np.dot(b, c), right), [ranks[k + 1], n[k + 1], ranks[k + 2]])
            return tt.vector.from_list(cores)
    else:
        assert direction == -1
        u = reshape(cores[k], [ranks[k], n[k] * ranks[k + 1]])
        a,b,c = np.linalg.svd(u,full_matrices=0);
        # a, b, c = scipy.linalg.svd(u, full_matrices=0);
        left = reshape(cores[k - 1], [ranks[k - 1] * n[k - 1], ranks[k]])
        ranks[k] = max(len(np.nonzero(np.array(b) > approxtol)[0].tolist()), 1)

        a = a[:, :ranks[k]]
        b = np.diag(b[:ranks[k]])
        c = c[:ranks[k], :]

        cores[k] = reshape(c, [ranks[k], n[k], ranks[k + 1]])
        cores[k - 1] = reshape(np.dot(np.dot(left, a), b), [ranks[k - 1], n[k - 1], ranks[k]])
        return tt.vector.from_list(cores)

    return 0


def ttTruncate(U, ranks, approxtol=1e-14):
    # from right to left in this version
    if isinstance(ranks, int):
        ranks = ranks * np.ones([U.d, 1])

    V = U.copy()
    vcore = V.core
    r = V.r
    n = V.n
    d = V.d
    pos = V.ps

    for i in range(d - 1, 0, -1):  # d:-1:2
        right = vcore[pos[i] - 1:pos[i + 1] - 1]
        right = reshape(right, (r[i], n[i] * r[i + 1]))

        left, mid, right = np.linalg.svd(right,full_matrices=0)
        # left, mid, right = scipy.linalg.svd(right, full_matrices=0)

        u = reshape(vcore[pos[i - 1] - 1:pos[i] - 1], (r[i - 1] * n[i - 1], r[i]))
        if i + 1 < d:
            v = vcore[pos[i + 1] - 1:pos[d] - 1]
        r[i] = max(len(np.nonzero(np.array(mid) > approxtol)[0].tolist()), 1)
        if ranks[i] < r[i]:
            r[i] = ranks[i]

        right = right[:r[i], :]
        left = np.dot(u, np.dot(left[:, :r[i]], np.diag(mid[:r[i]])))

        pos = np.cumsum([1] + (n * r[:d] * r[1:d + 1]).tolist())
        vcore[pos[i] - 1:pos[i + 1] - 1] = ravel(right)
        vcore[pos[i - 1] - 1:pos[i] - 1] = ravel(left)
        if i + 1 < d:
            vcore[pos[i + 1] - 1:pos[d] - 1] = v
        vcore = vcore[:pos[d] - 1]

    V.core = vcore
    V.ps = pos
    V.r = r
    return V


def ttIsOrthogonalized(U, i):
    # For validation purposes only, very slow
    Cores = tt.vector.to_list(U)
    d = U.d
    n = U.n
    r = U.r

    for k in range(i - 1):
        u = reshape(Cores[k], [r[k] * n[k], r[k + 1]])
        c = u.T.dot(u)
        if np.linalg.norm(c - np.eye(r[k + 1])) > 1e-10:
            return False

    for k in range(i, d):
        u = reshape(Cores[k], [r[k], n[k] * r[k + 1]])
        c = u.dot(u.T)
        if np.linalg.norm(c - np.eye(r[k])) > 1e-10:
            return False

    return True


def ttGetVec(U, vec):
    cores = tt.vector.to_list(U)
    r = U.r
    n = U.n
    d = U.d

    right = cores[d - 1][:, int(vec[d - 2]), 0]
    for i in range(d - 2, 0, -1):
        left = cores[i][:, int(vec[i - 1]), :]
        right = left.dot(right)
    left = reshape(cores[0], [n[0], r[1]])

    right = ravel(left.dot(right))
    return right


def rankOneProduct(comp, left=None, mid=None, right=None):
    l2 = comp.shape[0]
    m2 = comp.shape[1]
    r2 = comp.shape[2]

    if left is not None:
        if len(left.shape) > 1:
            l1 = left.shape[0]
        else:
            l1 = 1
        comp = reshape(comp, [l2, m2 * r2])
        comp = left.dot(comp)
        comp = reshape(comp, [l1, m2, r2])
        l2 = l1

    if mid is not None:
        if len(mid.shape) > 1:
            m1 = mid.shape[0]
        else:
            m1 = 1
        comp = np.transpose(comp, axes=(1, 0, 2))
        comp = reshape(comp, [m2, l2 * r2])
        comp = mid.dot(comp)
        comp = reshape(comp, [m1, l2, r2])
        comp = np.transpose(comp, axes=(1, 0, 2))
        m2 = m1

    if right is not None:
        if len(right.shape) > 1:
            r1 = right.shape[0]
        else:
            r1 = 1
        comp = reshape(comp, [l2 * m2, r2])
        comp = comp.dot(right.T)
        comp = reshape(comp, [l2, m2, r1])

    return comp


def leftThreeHalfdot(U, V):
    ur1 = U.shape[0]
    ur2 = U.shape[1]
    ur3 = U.shape[2]

    vr3 = V.shape[2]

    u = reshape(U, [ur1 * ur2, ur3])
    v = reshape(V, [ur1 * ur2, vr3])

    return np.dot(u.T, v)


def rightThreeHalfdot(U, V):
    ur1 = U.shape[0]
    ur2 = U.shape[1]
    ur3 = U.shape[2]

    vr1 = V.shape[0]

    u = reshape(U, [ur1, ur2 * ur3])
    v = reshape(V, [vr1, ur2 * ur3])

    return np.dot(u, v.T)


def leftThreeMatrixHalfdot(U, A, left):
    ur1 = U.shape[0]
    ur2 = U.shape[1]
    ur3 = U.shape[2]
    ar1 = A.shape[0]
    ar4 = A.shape[3]

    u = reshape(U, [ur1, ur2 * ur3])
    left = rankOneProduct(left, left=u.T, right=u.T)
    left = reshape(left, [ur2, ur3, ar1, ur2, ur3])
    left = np.transpose(left, axes=(1, 4, 0, 2, 3))
    left = reshape(left, [ur3 * ur3, ur2 * ar1 * ur2])
    a = np.transpose(A, axes=(1, 0, 2, 3))
    a = reshape(a, [ur2 * ar1 * ur2, ar4])
    left = np.dot(left, a)
    left = reshape(left, [ur3, ur3, ar4])
    left = np.transpose(left, axes=(0, 2, 1))

    return left


def rightThreeMatrixHalfdot(U, A, right):
    ur1 = U.shape[0]
    ur2 = U.shape[1]
    ur3 = U.shape[2]
    ar1 = A.shape[0]
    ar4 = A.shape[3]

    u = reshape(U, [ur1 * ur2, ur3])
    right = rankOneProduct(right, left=u, right=u)
    right = reshape(right, [ur1, ur2, ar4, ur1, ur2])
    right = np.transpose(right, axes=(0, 3, 1, 2, 4))
    right = reshape(right, [ur1 * ur1, ur2 * ar4 * ur2])
    a = np.transpose(A, axes=(1, 3, 2, 0))
    a = reshape(a, [ur2 * ar4 * ur2, ar1])
    right = np.dot(right, a)
    right = reshape(right, [ur1, ur1, ar1])
    right = np.transpose(right, axes=(0, 2, 1))

    return right


def tt_round(_ten, ranks):
    """ Computes an approximation to a tt.vector with maximal ranks and right orthogonalizes the tensor"""
    if not isinstance(_ten, list):
        raise ValueError("tt_round: ten is not a list")

    d = len(_ten)  # get dimension of the tensor as length of the list of cores
    if isinstance(ranks, int):
        ranks = [1] + [ranks] * (d - 1) + [1]
    ten = tt.vector.from_list(_ten).copy()
    # print(ten)
    ten = tt.vector.to_list(ten)
    # print("ranks= {}".format(ranks))
    err = 0
    for i in range(d - 1, 0, -1):
        # print("core {} shape= {}".format(i, ten[i].shape))
        #                                           # create last core by reshaping it to a matrix
        right = reshape(ten[i], [ten[i].shape[0], ten[i].shape[1] * ten[i].shape[2]])
        # print(" matricisation shape= {}".format(right.shape))
        # left, mid, right = np.linalg.svd(right,full_matrices=0)
        #                                           # calculate singular value decomposition

        left, mid, right = svd(right, full_matrices=False)
        if i > 1:
            u = reshape(ten[i - 1], [ten[i - 1].shape[0] * ten[i - 1].shape[1], ten[i - 1].shape[2]])
        else:
            u0 = ten[0]
            # for lia in mid:
            # print lia
        # c.r[i] = max(len(np.nonzero(np.array(mid) > eps)[0].tolist()),1)
        ranks[i] = np.min([ranks[i], right.shape[0]])
        right = right[:ranks[i], :]
        err += np.sum(np.array(mid[ranks[i] + 1:]) ** 2)
        # print("  reduced matrix shape= {}".format(right.shape))
        # print("  reshape into = {}".format([ranks[i], ten[i].shape[1], ranks[i+1]]))
        ten[i] = reshape(right, [ranks[i], ten[i].shape[1], ranks[i + 1]])
        if i > 1:
            left = np.dot(u, np.dot(left[:, :ranks[i]], np.diag(mid[:ranks[i]])))
            # print(" left matricisation shape= {}".format(left.shape))
            # print("  reshape into = {}".format([ten[i-1].shape[0], ten[i-1].shape[1], ranks[i]]))
            ten[i - 1] = reshape(left, [ten[i - 1].shape[0], ten[i - 1].shape[1], ranks[i]])
        else:
            left = np.dot(left[:, :ranks[1]], np.diag(mid[:ranks[1]]))
            leftlist = []
            # for j in range(ranks[1]):
            #    leftlist.append(sum([u0[k] * left[k, j] for k in range(len(u0))]))
            # print leftlist
            # ten[0] = np.array(leftlist)
            ten[0] = np.array([np.dot(u0[0], left)])
            #     print("round error = {}".format(np.sqrt(err)))
    return ten


def tt_round_qr(_ten, ranks):
    """ Computes an approximation to a tt.vector with maximal ranks and right orthogonalizes the tensor"""

    print("test")
    if not isinstance(_ten, list):
        if isinstance(_ten, tt.vector):
            _ten = tt.vector.to_list(_ten)
        else:
            raise ValueError("tt_round: ten is not a list nor a tensor")
    d = len(_ten)  # get dimension of the tensor as length of the list of cores
    if isinstance(ranks, int):
        ranks = [1] + [ranks] * (d - 1) + [1]
    ten = tt.vector.from_list(_ten).copy()
    # print(ten)
    ten = tt.vector.to_list(ten)
    # print("ranks= {}".format(ranks))
    err = 0
    for i in range(d):
        # print("core {} shape= {}".format(i, ten[i].shape))
        #                                           # create last core by reshaping it to a matrix
        right = reshape(ten[i], [ten[i].shape[0] * ten[i].shape[1], ten[i].shape[2]])
        # print(" matricisation shape= {}".format(right.shape))
        # left, mid, right = np.linalg.svd(right,full_matrices=0)
        #                                           # calculate singular value decomposition
        core, right = qr(right, mode='economic', pivoting=False, overwrite_a=True)
        if i < d - 1:
            right_core = reshape(ten[i + 1], [ten[i + 1].shape[0], ten[i + 1].shape[1] * ten[i + 1].shape[2]])

            # print("  reduced matrix shape= {}".format(core.shape))
            # print("  reshape into = {}".format([core.shape[0], ten[i].shape[1], ten[i].shape[2]]))
            ten[i] = reshape(core, [ten[i].shape[0], ten[i].shape[1], core.shape[1]])
            # print("  multiply R {} with right core {} ".format(right.shape, right_core.shape))
            right = np.dot(right, right_core)
            ten[i + 1] = reshape(right, [right.shape[0], ten[i + 1].shape[1], ten[i + 1].shape[2]])
    err = 0
    for i in range(d - 1, 0, -1):
        # print("core {} shape= {}".format(i, ten[i].shape))
        #                                           # create last core by reshaping it to a matrix
        right = reshape(ten[i], [ten[i].shape[0], ten[i].shape[1] * ten[i].shape[2]])
        # print(" matricisation shape= {}".format(right.shape))
        # left, mid, right = np.linalg.svd(right,full_matrices=0)
        #                                           # calculate singular value decomposition
        left, mid, right = svd(right, full_matrices=0)
        if i > 1:
            u = reshape(ten[i - 1], [ten[i - 1].shape[0] * ten[i - 1].shape[1], ten[i - 1].shape[2]])
        else:
            u0 = ten[0]
        # for lia in mid:
        #     print lia
        # c.r[i] = max(len(np.nonzero(np.array(mid) > eps)[0].tolist()),1)
        ranks[i] = np.min([ranks[i], right.shape[0]])
        right = right[:ranks[i], :]
        err += np.sum(mid[ranks[i]:] ** 2)
        # print("  reduced matrix shape= {}".format(right.shape))
        # print("  reshape into = {}".format([ranks[i], ten[i].shape[1], ranks[i+1]]))
        ten[i] = reshape(right, [ranks[i], ten[i].shape[1], ranks[i + 1]])

        if i > 1:
            left = np.dot(u, np.dot(left[:, :ranks[i]], np.diag(mid[:ranks[i]])))
            # print(" left matricisation shape= {}".format(left.shape))
            # print("  reshape into = {}".format([ten[i-1].shape[0], ten[i-1].shape[1], ranks[i]]))
            ten[i - 1] = reshape(left, [ten[i - 1].shape[0], ten[i - 1].shape[1], ranks[i]])
        else:
            left = np.dot(left[:, :ranks[1]], np.diag(mid[:ranks[1]]))
            leftlist = []
            # for j in range(ranks[1]):
            #    leftlist.append(sum([u0[k] * left[k, j] for k in range(len(u0))]))
            # print leftlist
            # ten[0] = np.array(leftlist)
            ten[0] = np.array([np.dot(u0[0], left)])
    # print("rounding error QR : {}".format(np.sqrt(err)))
    return ten, np.sqrt(err)


def tt_round_nmf(_ten, ranks):
    """ Computes an approximation to a tt.vector with maximal ranks and right orthogonalizes the tensor"""

    if not isinstance(_ten, list):
        if isinstance(_ten, tt.vector):
            _ten = tt.vector.to_list(_ten)
        else:
            raise ValueError("tt_round: ten is not a list nor a tensor")
    d = len(_ten)  # get dimension of the tensor as length of the list of cores
    if isinstance(ranks, int):
        ranks = [1] + [ranks] * (d - 1) + [1]
    ten = tt.vector.from_list(_ten).copy()
    # print(ten)
    ten = tt.vector.to_list(ten)
    # print("ranks= {}".format(ranks))
    err = 0
    for i in range(d):
        # print("core {} shape= {}".format(i, ten[i].shape))
        #                                           # create last core by reshaping it to a matrix
        right = reshape(ten[i], [ten[i].shape[0] * ten[i].shape[1], ten[i].shape[2]])
        # print(" matricisation shape= {}".format(right.shape))
        # left, mid, right = np.linalg.svd(right,full_matrices=0)
        #                                           # calculate singular value decomposition
        core, right = qr(right, mode='economic', pivoting=False, overwrite_a=True)
        if i < d - 1:
            right_core = reshape(ten[i + 1], [ten[i + 1].shape[0], ten[i + 1].shape[1] * ten[i + 1].shape[2]])

            # print("  reduced matrix shape= {}".format(core.shape))
            # print("  reshape into = {}".format([core.shape[0], ten[i].shape[1], ten[i].shape[2]]))
            ten[i] = reshape(core, [ten[i].shape[0], ten[i].shape[1], core.shape[1]])
            # print("  multiply R {} with right core {} ".format(right.shape, right_core.shape))
            right = np.dot(right, right_core)
            ten[i + 1] = reshape(right, [right.shape[0], ten[i + 1].shape[1], ten[i + 1].shape[2]])
    err = 0
    for i in range(d - 1, 0, -1):
        # print("core {} shape= {}".format(i, ten[i].shape))
        #                                           # create last core by reshaping it to a matrix
        right = reshape(ten[i], [ten[i].shape[0], ten[i].shape[1] * ten[i].shape[2]])
        # print(" matricisation shape= {}".format(right.shape))
        # left, mid, right = np.linalg.svd(right,full_matrices=0)
        #                                           # calculate singular value decomposition
        left, right = nmf_gradient_method(right, ranks[i])
        if i > 1:
            u = reshape(ten[i - 1], [ten[i - 1].shape[0] * ten[i - 1].shape[1], ten[i - 1].shape[2]])
        else:
            u0 = ten[0]
        # for lia in mid:
        #     print lia
        # c.r[i] = max(len(np.nonzero(np.array(mid) > eps)[0].tolist()),1)
        ranks[i] = np.min([ranks[i], right.shape[0]])
        right = right[:ranks[i], :]
        # print("  reduced matrix shape= {}".format(right.shape))
        # print("  reshape into = {}".format([ranks[i], ten[i].shape[1], ranks[i+1]]))
        ten[i] = reshape(right, [ranks[i], ten[i].shape[1], ranks[i + 1]])

        if i > 1:
            left = np.dot(u, left[:, :ranks[i]])
            # print(" left matricisation shape= {}".format(left.shape))
            # print("  reshape into = {}".format([ten[i-1].shape[0], ten[i-1].shape[1], ranks[i]]))
            ten[i - 1] = reshape(left, [ten[i - 1].shape[0], ten[i - 1].shape[1], ranks[i]])
        else:
            left = left[:, :ranks[1]]
            leftlist = []
            # for j in range(ranks[1]):
            #    leftlist.append(sum([u0[k] * left[k, j] for k in range(len(u0))]))
            # print leftlist
            # ten[0] = np.array(leftlist)
            ten[0] = np.array([np.dot(u0[0], left)])
    # print("rounding error QR : {}".format(np.sqrt(err)))
    return ten

def tt_nmf_from_full(_ten, ranks):
    """ Computes an approximation to a tt.vector with maximal ranks and right orthogonalizes the tensor"""

    shapes = _ten.shape
    d = len(_ten.shape)  # get dimension of the tensor as length of the list of cores
    if isinstance(ranks, int):
        ranks = [1] + [ranks] * (d - 1) + [1]
    ten = []
    curr_core = _ten
    # print("ranks= {}".format(ranks))
    for i in range(d - 1, 0, -1):
        # print("core {} shape= {}".format(i, ten[i].shape))
        #                                           # create last core by reshaping it to a matrix
        if i < d-1:
            reshaped_core = reshape(curr_core, [np.prod(shapes[:i]), shapes[i] * curr_core.shape[-1]])
        else:
            print("shapes:i-1 = {}".format(shapes[:i]))
            reshaped_core = reshape(curr_core, [np.prod(shapes[:i]), shapes[i]])
        # print(" matricisation shape= {}".format(right.shape))
        # left, mid, right = np.linalg.svd(right,full_matrices=0)
        #                                           # calculate singular value decomposition
        norm = np.linalg.norm(reshaped_core, ord='fro')
        curr_core, right = nmf_gradient_method(reshaped_core*(norm)**(-1), ranks[i])
        right *= norm
        #  left, mid, right = svd(reshaped_core, full_matrices=0)
        #  ranks[i] = np.min([ranks[i], right.shape[0]])
        #  right = right[:ranks[i], :]
        #  curr_core = np.dot(left[:, :ranks[1]], np.diag(mid[:ranks[1]]))
        # print("  reduced matrix shape= {}".format(right.shape))
        # print("  reshape into = {}".format([ranks[i], ten[i].shape[1], ranks[i+1]]))
        if i < d-1:
            ten.insert(0, reshape(right, [right.shape[0], shapes[i], ranks[i+1]]))
            if i == 1:
                ten.insert(0, reshape(curr_core, [1, shapes[i-1], ranks[i]]))
        else:
            ten.insert(0, reshape(right, [right.shape[0], right.shape[1], 1]))

        # for lia in mid:
        #     print lia
        # c.r[i] = max(len(np.nonzero(np.array(mid) > eps)[0].tolist()),1)
        ## ranks[i] = np.min([ranks[i], right.shape[0]])
        ## right = right[:ranks[i], :]
        # print("  reduced matrix shape= {}".format(right.shape))
        # print("  reshape into = {}".format([ranks[i], ten[i].shape[1], ranks[i+1]]))

    # print("rounding error QR : {}".format(np.sqrt(err)))
    return ten


def nmf_gradient_method(A, rank, tol=1e-10, lambd=1.0, delta=1.0, max_iters=100, d_improve_iters=10, sweeps=100):
    from alea.utils.tictoc import TicToc
    n = A.shape[0]
    m = A.shape[1]
    def gradient_decent_step(alpha, x, gradient):
        with TicToc(key="gradient step", active=False, do_print=False):
            retval = project_to_non_negative(x - alpha * gradient(x))
        return retval

    method_step = gradient_decent_step
    method_string = "gradient_descent"

    f = lambda W, H: (np.linalg.norm(A - np.dot(W, H), ord='fro') ** 2 + lambd * np.trace(np.dot(W.T, W) +
                                                                                          (delta - 1) * np.eye(
                                                                                              W.T.shape[0],
                                                                                              W.shape[1])))
    f_orig = lambda W, H: (np.linalg.norm(A - np.dot(W, H), ord='fro') ** 2 +
                           lambd * np.log(np.linalg.det(np.dot(W.T, W) + delta * np.eye(W.T.shape[0], W.shape[1]))))

    value = lambda W, H: np.linalg.norm(A - np.dot(W, H), ord='fro') / np.linalg.norm(A, ord='fro')

    def project_to_non_negative(mat):
        return mat.clip(min=0)

    def grad_f(W, H, half_sweep, _D_1=None):
        # if half_sweep is true, then H is fixed, otherwise W is fixed
        if _D_1 is None:
            _D_1 = np.eye(rank, rank)
            _key = "gradient evaluation"
        else:
            _key = "gradient evaluation (speed)"
        with TicToc(key=_key, active=True, do_print=False):
            if half_sweep is True:
                buffer_matrix = np.dot(W, H)
                # print("W*H: {}".format(buffer_matrix))
                buffer_matrix = buffer_matrix - A
                # print("W*H-A: {}".format(buffer_matrix))
                buffer_matrix = np.dot(buffer_matrix, H.T)
                # print("(W*H-A)*H.T: {}".format(buffer_matrix))
                buffer_matrix2 = np.dot(W, _D_1)
                # print("W*D: {}".format(buffer_matrix2))
                retval = 2*buffer_matrix + lambd * buffer_matrix2
                # print("retval: {}".format(retval))
                return retval
            else:
                return 2 * np.dot(W.T, (np.dot(W, H) - A))

    def grad_f_log(W, H, half_sweep):
        # if half_sweep is true, then H is fixed, otherwise W is fixed
        with TicToc(key="gradient evaluation (logdet)", active=True, do_print=False):
            if half_sweep is True:
                return 2 * np.dot((np.dot(W, H) - A), H.T) + lambd * np.dot(W, (
                np.dot(W.T, W) + delta * np.eye(rank, rank)))
            else:
                return 2 * np.dot(W.T, (np.dot(W, H) - A))

    print("calculate gradient method for lambda = {}".format(lambd))
    D_1 = np.zeros((rank, rank))
    D_2 = np.zeros((rank, rank))
    fun_speed = []

    functional_speed = []
    log_det_value_speed = []
    w = np.random.rand(n, rank) * 100
    # w = w * np.linalg.norm(np.dot(w.T, w))**(-1)
    h = np.random.rand(rank, m) *200
    # h = h * np.linalg.norm(np.dot(h.T, h))**(-1)
    w_speed = w
    h_speed = h
    #fun_speed.append(value(w_speed, h_speed))
    #functional_speed.append(f(w_speed, h_speed))

    end_all = False

    alpha = 0.01
    for d in range(d_improve_iters):
        if d < 4:
            alpha *= 2
        if end_all is True:
            break
        if d == 0:
            D_1 = np.eye(rank, rank)
            D_2 = np.zeros((rank, rank))
        else:
            if d % 5 == 0:
                TicToc.sortedTimes()
            with TicToc(key="SVD", active=True, do_print=False):
                # _sigma = np.linalg.svd(np.dot(D_1, (np.dot(w_speed.T, w_speed) + delta*np.eye(rank, rank)) + D_2),
                #                                full_matrices=False, compute_uv=False)
                # matrix_to_svd = (np.dot(w_speed.T, w_speed) + delta * np.eye(rank, rank)) * np.linalg.norm(
                #     np.dot(w_speed.T, w_speed) + delta * np.eye(rank, rank)) ** (-1)
                # print("normalizing: {}".format(np.linalg.norm(
                #     np.dot(w_speed.T, w_speed) + delta * np.eye(rank, rank))))
                _sigma = np.linalg.svd(np.dot(w_speed.T, w_speed) + np.eye(rank, rank), full_matrices=False,
                                       compute_uv=False)  # * np.linalg.norm(
                # np.dot(w_speed.T, w_speed) + delta * np.eye(rank, rank))**(0.5)


                print("new eigenvalues to adapt to: {}".format(_sigma))
                if np.min(_sigma) < 1e-5:
                    end_all = True
                    break
                D_1 = np.diag(np.array([mu ** (-2) for mu in _sigma]))
                D_2 = np.diag(np.array([np.log(mu ** 2) - 1 for mu in _sigma]))
        for k in range(max_iters):
            alpha = alpha
            for i in range(sweeps):

                if method_string == "gradient_descent":
                        # with TicToc(key="normalisation (truncated)", active=True, do_print=False):
                        #     w *= np.linalg.norm(w, ord=1) ** (-1)
                        #     h *= np.linalg.norm(h, ord=1) ** (-1)
                    with TicToc(key="gradient step (speed)", active=True, do_print=False):
                        w_speed = method_step(alpha, w_speed, lambda W: grad_f(W, h_speed, True, _D_1=D_1))
                        h_speed = method_step(alpha, h_speed, lambda H: grad_f(w_speed, H, False, _D_1=D_1))
                        # with TicToc(key="normalisation (speed)", active=True, do_print=False):
                        #     w_speed *= np.linalg.norm(w_speed, ord=1) ** (-1)
                        #     h_speed *= np.linalg.norm(h_speed, ord=1) ** (-1)
                        # with TicToc(key="normalisation (logdet)", active=True, do_print=False):
                        #     w_log *= np.linalg.norm(w_log, ord=1) ** (-1)
                        #     h_log *= np.linalg.norm(h_log, ord=1) ** (-1)
                    # with TicToc(key="gradient step (det)", active=True, do_print=False):
                    #     w_log = method_step(alpha, w_log, lambda W: grad_f_det(W, h_det, True))
                    #     h_log = method_step(alpha, h_log, lambda H: grad_f_det(w_det, H, False))

                    # print("w = {}".format(w_speed))
                    # print("h = {}".format(h_speed))
                    if w_speed.max <= 0:
                        print("w_speed is zero in step {}".format((d, k)))

            # fun_speed.append(value(w_speed, h_speed))
            # functional_speed.append(f(w_speed, h_speed))
            # log_det_value_speed.append(f_orig(w_speed, h_speed))
    return w_speed, h_speed


# region def has full rank
def has_full_rank(ten):
    if isinstance(ten, tt.vector):
        ten = tt.vector.to_list(ten)

    n = 1
    for lia in range(len(ten)-1, -1, -1):
        if lia == len(ten)-1:
            if ten[lia].shape[2] != 1:
                raise ValueError("last rank should be == 1")
        if lia == 0:
            if ten[lia].shape[0] != 1:
                raise ValueError("first rank should be == 1")
            break
        n *= ten[lia].shape[1]
        r = ten[lia].shape[0]
        if r < n:
            return False
    return True
# endregion

# region def compute Frobenius norm of a given tensor
def frob_norm(U):
    """
    computes the Frobenius norm of a tensor using a faster algorithm than the Oseledets implementation by contracting
    over the ranks and dimensions in one step.
    :param U: tt.vector or numpy list or list of components
    :return: Frobenius norm of U
    """
    if type(U).__module__ == np.__name__ or isinstance(U, list):
        U = U                                       # U is already a numpy array
    elif isinstance(U, tt.vector):
        U = tt.vector.to_list(U)                    # U is given as tt.vector -> convert to list of components
    elif isinstance(U, float) or isinstance(U, int):
        return U                                    # numbers do not need to be converted
    else:                                           # raise TypeError
        raise TypeError("unknown Type: U:{}".format(type(U)))

    retval = 0
    for lia in range(len(U)-1, -1, -1):
        # print("U[{}].shape = {}".format(lia, U[lia].shape))
        if lia == len(U)-1:
            assert U[lia].shape[2] == 1
            #                                       # retval shape [r_{M-1}, r_{M-1}']
            retval = np.dot(U[lia][:, :, 0], U[lia][:, :, 0].T)
            continue
        # print("retval.shape= {}".format(retval.shape))
        #                                           # reshape U_m -> [r_{m-1}*n_m, r_m]
        core = np.reshape(U[lia], (U[lia].shape[0]*U[lia].shape[1], U[lia].shape[2]))
        retval = np.dot(core, retval)               # dot [r_{m-1}*n_m, r_m] x [r_m, r_m'] -> [r_{m-1}*n_m, r_m']
        # print("  2 retval.shape= {}".format(retval.shape))

        retval = np.reshape(retval, (U[lia].shape[0], U[lia].shape[1], retval.shape[1]))
        #                                           # reshape [r_{m-1}, n_m * r_m']
        retval = np.reshape(retval, (U[lia].shape[0], U[lia].shape[1]*retval.shape[2]))
        # print("  3 retval.shape= {}".format(retval.shape))

        #                                           # reshape core [r_{m-1}', n_m * r_m']
        core = np.reshape(U[lia], (U[lia].shape[0], U[lia].shape[1] * U[lia].shape[2]))
        # print("  core.shape= {}".format(core.shape))

        retval = np.dot(retval, core.T)
    assert len(retval.shape) == 2
    assert retval.shape[0] == 1
    assert retval.shape[1] == 1
    if retval[0, 0] <= 0:
        return 0.0
    retval = retval[0, 0]
    return np.sqrt(retval)
# endregion
