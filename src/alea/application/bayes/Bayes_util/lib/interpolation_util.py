from __future__ import division
import numpy as np
from scipy.integrate import fixed_quad
from scipy.special import legendre
from numpy.polynomial.hermite_e import hermegauss
import operator


def get_hermite_points(dim):
    return [list(hermegauss(dim[lia]))[0] for lia in range(len(dim))]


def get_cheb_points(cheb_dim):
    return [[np.cos((2*(i+1)-1)/(2*cheb_dim[d])*np.pi) for i in range(cheb_dim[d])] for d in range(len(cheb_dim))]
    # reference implementation
    # from numpy.polynomial.chebyshev import chebgauss
    # return chebgauss(...)


def evaluate_lagrange(x, nodes, j):
    k = len(nodes)
    p = [(x - nodes[m])/(nodes[j] - nodes[m]) for m in xrange(k) if m != j]
    return reduce(operator.mul, p)


def interpolate_lagrange(x, f, nodes):
    N, L = len(x), len(nodes)
    samples = map(f, nodes)
    y = np.array([s*evaluate_lagrange(x, nodes, j) for s, j in zip(samples, range(L))])
    y = np.sum(y, axis=0)
    return y, samples


def get_legendre_basis(degree):
    B = [legendre(d) for d in range(degree)]
    N = np.sqrt(np.array([fixed_quad(lambda x: b(x)**2, -1, 1, n=5)[0] for b in B]))
    N_1 = np.array([fixed_quad(lambda x: b(x), -1, 1, n=5)[0] for b in B])
    return B, N, N_1


def lagrange_to_legendre(nodes):
    d = len(nodes)
    # project Lagrange onto Legendre polynomials
    q, w = np.polynomial.legendre.leggauss(2*d)
    # evaluate Lagrange and Legendre at quad points
    ql1 = [evaluate_lagrange(q, nodes, j) for j in range(d)]
    B, N, N_1 = get_legendre_basis(d)
    ql2 = [b(q) for b in B]
    T = np.array([[np.sum(w * q1 * q2 / n2) for q1 in ql1] for n2, q2 in zip(N, ql2)])
    return T, B, N, N_1


def evaluate_function_representation(x, basis, weights, coeffs):
    val = np.vstack([c * np.array(map(b, x) / w) for c, b, w in zip(coeffs, basis, weights)])
    return np.sum(val, axis=0)
