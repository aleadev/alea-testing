# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  Library for Bayesian inversion using a direct forward operator
"""

# region Imports
from __future__ import division
import numpy as np
import tt
from scipy.integrate import simps
import logging
import logging.config
# endregion

# region Create Tensor from solution


def create_tensor_from_solution(_solution_handle, _x, _y):
    """
    Creates a Tensor in TT Format from given function handle, physical grid and list of stochastic grids
    @param _solution_handle: function handle representing the solution operator here: a_0[x] + \sum_{m=1}^M a_m[x]y_m
    @type _solution_handle: lambda
    @param _x: grid in physical domain
    @type _x: list
    @param _y: list of grids in stochastic domain
    @type _y: list of lists
    @return: TT-Tensor as TT representation of the full tensor determined by the _solution_handle
    @rtype: tt.vector
    """

    list_of_components = []
    _m = len(_y)
    _mx = len(_x)
    component = np.zeros((1, _mx, _m+1))
    for lia in range(_m+1):
        for i, x in enumerate(_x):
            component[0, i, lia] = _solution_handle(x, lia)
    list_of_components.append(component)

    for lia in range(_m-1):
        component = np.zeros((_m+1, len(_y[lia]), _m+1))
        for lic in range(len(_y[lia])):
            eye = np.identity(_m+1)
            eye[lia+1, lia+1] = _y[lia][lic]
            component[:, lic, :] = eye

        list_of_components.append(component)

    component = np.ones((_m+1, len(_y[-1]), 1))
    for lia in range(len(_y[-1])):
        component[-1, lia, 0] = _y[-1][lia]
    list_of_components.append(component)
    return tt.vector.from_list(list_of_components)
# endregion

# region Apply Observation Operator


def apply_observation(_tensor, _grid_points, _round=True, _precision=1e-10, _maxrank=1000, _print=False):
    """
    Applies the observation operator just by erasing the non observed rows in the physical domain tensor component
    @param _tensor: tensor representing the solution in TT format
    @type _tensor: tt.vector
    @param _grid_points: list of degrees of freedom do use as observation (cancel the rest)
    @type _grid_points: list
    @param _round: switch if there should be rounding
    @param _precision: rounding precision
    $@type _precision: float
    @param _maxrank: maximal rank to start rounding at
    @type _maxrank: int
    @param _print: switch if plotting should be enabled
    @type  _print: bool
    @return: list of TT tensors, each representing the observation at the corresponding grid point in physical domain
    @rtype: list
    """

    logging.config.fileConfig('logging.conf')       # load logger configuration
    log = logging.getLogger('test_bayes')           # get new logger object

    retval = []                                     # define return value
    #                                               # list of components from tensor
    list_of_components = tt.vector.to_list(_tensor)
    for lia in range(len(_grid_points)):            # loop through dofs
        buffer_list = []                            # list to store current tensor inside
        #                                           # initialize tensor component with correct size
        buffer_component = np.zeros((1, 1, list_of_components[0].shape[2]))
        #                                           # evaluate first component and store the remaining row
        buffer_component[0, 0, :] = list_of_components[0][0, _grid_points[lia], :]
        buffer_list.append(buffer_component)        # add to buffer list
        #                                           # store the remaining components in the buffer list
        for lic in range(1, len(list_of_components)):
            buffer_list.append(list_of_components[lic])

        buffer_ten = tt.vector.from_list(buffer_list)
        if _round:
            if max(buffer_ten.r) > _maxrank:
                if _print:
                    print "round tensor of rank {0}".format(buffer_ten.r)
                log.debug("round tensor of rank {0}".format(buffer_ten.r))
                buffer_ten = tt.vector.round(buffer_ten, _precision, _maxrank)
                if _print:
                    print "tensor rounded to rank {0}".format(buffer_ten.r)
                log.debug("tensor rounded to rank {0}".format(buffer_ten.r))
        #                                           # add current tensor to return value list
        retval.append(buffer_ten)
    return retval

# endregion

# region Apply Observation Operator in place


def apply_observation_inplace(_tensor, _grid_points):
    """
    Applies the observation operator just by erasing the non observed rows in the physical domain tensor component
    @param _tensor: tensor representing the solution in TT format
    @type _tensor: tt.vector
    @param _grid_points: list of degrees of freedom do use as observation (cancel the rest)
    @type _grid_points: list
    @return: tt tensor corresponding to the vector of observations
    @rtype: tt.vector
    """

    logging.config.fileConfig('logging.conf')       # load logger configuration
    #                                               # list of components from tensor
    list_of_components = tt.vector.to_list(_tensor)
    list_of_components[0] = list_of_components[0][:, _grid_points, :]
    return tt.vector.from_list(list_of_components)

# endregion

# region Apply Inner product


def apply_inner_product(_tensor_list, _gamma=1, _round=True, _precision=1e-10, _maxrank=1000, _print=False):
    """
    Applies the inner product calculation to the given list of tt tensors and return the resulting tensor
    Note: No orthogonalty used here
    @param _tensor_list: list of tensors
    @type _tensor_list: list
    @param _gamma: covariance constant to create a covariance matrix gamma*I
    @type _gamma: float
    @param _round: switch if there should be rounding
    @param _precision: rounding precision
    @type _precision: float
    @param _maxrank: maximal rank to start rounding at
    @type _maxrank: int
    @param _print: switch if printing is enabled
    @type: _print: bool
    @return: TT tensor corresponding to the inner product of the list with itself
    @rtype: tt.vector
    """
    from alea.utils.tictoc import TicToc
    fp = ''
    try:
        f = open('logging.conf')
        f.close()
        fp = 'logging.conf'
    except IOError:
        print("logging not in working directory")
        try:
            f = open('/home/marschall/WIAS/alea-testing/src/alea/application/bayes/logging.conf')
            f.close()
            fp = '/home/marschall/WIAS/alea-testing/src/alea/application/bayes/logging.conf'
        except IOError:
            print("No logging file found. -> exit()")
            exit()
    logging.config.fileConfig(fp)
    log = logging.getLogger('test_bayes')           # get new logger object

    with TicToc(key="  -- calculate tensor product list", active=True, do_print=True):
        buffer_list = [t1*t2*_gamma**(-1) for t1, t2 in zip(_tensor_list, _tensor_list)]
    with TicToc(key="  -- calculate tensor sum", active=True, do_print=True):
        tensor = buffer_list[0]
        for lia in range(1, len(buffer_list)):
            if _print:
                print tensor
            if _round:
                if max(tensor.r) > _maxrank and True:
                    if _print:
                        print "round tensor of rank {0}".format(tensor.r)
                    log.debug("round tensor of rank {0}".format(tensor.r))
                    ten_old = tensor.copy()
                    # tensor = tt.vector.to_list(tensor)
                    # tensor = tt.vector.from_list(tensor)
                    tensor = tt.vector.round(tensor, _precision, _maxrank)

                    if _print:
                        print "tensor rounded to rank {0}, error= {1}".format(tensor.r, tt.vector.norm(tensor-ten_old))
                    log.debug("tensor rounded to rank {0}, error= {1}".format(tensor.r, tt.vector.norm(tensor - ten_old)))
            tensor += buffer_list[lia]
    ten_old = tensor.copy()
    log.debug("pre_tensor.r: {}".format(tensor.r))
    tensor = tt.vector.round(tensor, _precision, _maxrank)
    round_error = tt.vector.norm(tensor - ten_old)
    log.debug("post_tensor.r: {0}, error= {1}".format(tensor.r, round_error))
    if round_error > 1e-5:
        log.warning("   rounding error seems to be dominant after inner product !")

    return tensor

# endregion

# region Apply Inner product in place


def apply_inner_product_inplace(_tensor, _gamma=1, _round=True, _precision=1e-10, _maxrank=1000, _print=False):
    """
    Applies the inner product calculation to the given list of tt tensors and return the resulting tensor
    Note: No orthogonality used here
    @param _tensor: tensor corresponding to the observation
    @type _tensor: tt.vector
    @param _gamma: covariance constant to create a covariance matrix gamma*I
    @type _gamma: float
    @param _round: switch if there should be rounding
    @param _precision: rounding precision
    @type _precision: float
    @param _maxrank: maximal rank to start rounding at
    @type _maxrank: int
    @param _print: switch if printing is enabled
    @type: _print: bool
    @return: TT tensor corresponding to the inner product of the list with itself
    """

    logging.config.fileConfig('logging.conf')       # load logger configuration
    log = logging.getLogger('test_bayes')           # get new logger object
    _tensor_list = []
    for lia in range(_tensor.n[0]):
        _tensor_list.append(tt.vector.from_list([tt.vector.to_list(_tensor)[0][:, lia, :]] +
                                                tt.vector.to_list(_tensor)[1:]))
    buffer_list = [t1*t2*_gamma**(-1) for t1, t2 in zip(_tensor_list, _tensor_list)]
    tensor = buffer_list[0]
    for lia in range(1, len(buffer_list)):
        if _print:
            print tensor
        if _round:
            if max(tensor.r) > _maxrank:
                if _print:
                    print "round tensor of rank {0}".format(tensor.r)
                log.debug("round tensor of rank {0}".format(tensor.r))
                # tensor = tt.vector.to_list(tensor)
                # tensor = tt.vector.from_list(tensor)
                tensor = tt.vector.round(tensor, _precision, _maxrank)

                if _print:
                    print "tensor rounded to rank {0}".format(tensor.r)
                log.debug("tensor rounded to rank {0}".format(tensor.r))
        tensor += buffer_list[lia]

    return tensor

# endregion

# region Create rank one tensor with given values


def create_rank_one(_vector, _dimensions, _print=False):
    """
    Create a random rank one tensor and replace its values with the one given in the vector
    @param _vector: list of values
    @param _dimensions: list of dimensions
    @param _print: switch if printing should be activated
    @return: rank one tensor
    """

    if _print:
        print "Create random rank one Tensor and replace with vector data"
#                                                   create random rank 1 tt
    _tensor = tt.rand(_dimensions, len(_dimensions), [1 + 0*_lia for _lia in range(len(_dimensions)+1)])
    _tensor = tt.vector.to_list(_tensor)		    # store tensor as list of 3d array
    _vector = np.array(_vector)	                    # save possible list as array
    _tensor[0][0, :, 0] = _vector                   # store vector values in first comp
    for _lia in range(1, len(_dimensions)):	        # replace other comp with first unit vector
        _tensor[_lia][0, :, 0] = [1]*_dimensions[_lia]
        # _tensor[_lia][0, :, 0] = [1] + [0*_lic for _lic in range(_dimensions[_lia]-1)]
    return tt.vector.from_list(_tensor)
# endregion

# region estimate marginal derivatives by integrating in stochastic space (suitable for the sgfem tensor)


def integrate_ydims_tt_sgfem(y_measurements, pi_y, dy, my, _print=False):
    """
    creates from the solution of the bayesian procedure the marginal densities by integration over all remaining
    dimensions. Due to tensor train format this can be done with linear effort.
    @param y_measurements: tensor containing the exponential of the misfit
    @type y_measurements: tt.vector
    @param pi_y: list of density functions
    @type pi_y: list(lambda)
    @param dy: used integration distance
    @type dy: float
    @param my: interpolation grid
    @type my: list(float)
    @param _print: switch if printing is enabled
    @return list of densities
    """
    #assert isinstance(y_measurements, tt.vector)
    assert y_measurements.n[0] == 1
    m, ny = len(y_measurements.n)-1, len(my)
    assert len(pi_y) == m
    list_of_components = tt.vector.to_list(y_measurements)
    first_row = list_of_components[0][0, 0, :]
    y_int = np.ones((ny, m))
    buff_list = []
    for i, dpi in enumerate(pi_y):
        if _print:
            print "density {0}".format(i)
        for j in range(m+1):
            if _print:
                print "  j == {0}".format(j)
            if j == 0:
                buff = first_row
                if _print:
                    print "    first row {0}".format(buff.shape)
                continue
            if j == i+1 and j < m:
                if _print:
                    print "   start listing all y entry"
                if _print:
                    print "    {0} x {1} ".format(buff.shape, list_of_components[j][:, 0, :].shape)
                buff_list = [np.dot(buff, list_of_components[j][:, yi, :]) for yi in range(y_measurements.n[j])]
                if _print:
                    print "    = {0}".format(buff_list[0].shape)
                continue
            if j == i+1 and j == m:
                if _print:
                    print "   last and listing"
                if _print:
                    print "    {0} x {1} ".format(buff.shape, list_of_components[j][:, 0, 0].shape)
                buff_list = [np.dot(buff, list_of_components[j][:, yi, 0]) for yi in range(y_measurements.n[j])]
                if _print:
                    print "    = {0}".format(buff_list[0].shape)
                y_int[:, i] = np.array(buff_list)
                continue
            if j == m:
                if _print:
                    print "   last"
                if _print:
                    print "    {0} x {1} ".format(buff_list[0].shape, simps(list_of_components[j][:, :, 0],
                                                                            dx=dy, axis=1).shape)
                buff_list = [np.dot(buff_list[yi], simps(list_of_components[j][:, :, 0], dx=dy, axis=1))
                             for yi in range(y_measurements.n[j])]
                if _print:
                    print "    = {0}".format(buff_list[0].shape)
                y_int[:, i] = np.array(buff_list)
                continue
            if j < i+1:
                if _print:
                    print "   left"
                if _print:
                    print "    {0} x {1} ".format(buff.shape, simps(list_of_components[j], dx=dy, axis=1).shape)
                buff = np.dot(buff, simps(list_of_components[j], dx=dy, axis=1))
                if _print:
                    print "    = {0}".format(buff.shape)
                continue
            if j > i+1:
                if _print:
                    print "   right"
                if _print:
                    print "    {0} x {1}".format(buff_list[0].shape, simps(list_of_components[j], dx=dy, axis=1).shape)
                buff_list = [np.dot(buff_list[yi], simps(list_of_components[j], dx=dy, axis=1))
                             for yi in range(y_measurements.n[j])]
                if _print:
                    print "    = {0}".format(buff_list[0].shape)
                continue

    mean = simps(y_int[:, 0]*pi_y[0](my), dx=dy)
    return y_int, mean
# endregion

# region estimate marginal derivatives by integrating in stochastic space (suitable for the test tensor)


def integrate_ydims_tt(y_measurements, pi_y, dy, my, _print=False):
    """
    creates from the solution of the bayesian procedure the marginal densities by integration over all remaining
    dimensions. Due to tensor train format this can be done with linear effort.
    @param y_measurements: tensor containing the exponential of the misfit
    @type y_measurements: tt.vector
    @param pi_y: list of density functions
    @type pi_y: list(lambda)
    @param dy: used integration distance
    @type dy: float
    @param my: interpolation grid
    @type my: list(float)
    @param _print: switch if printing is enabled
    @return list of densities
    """
    assert isinstance(y_measurements, tt.vector)
    assert y_measurements.n[0] == 1
    m, ny = len(y_measurements.n)-1, len(my)
    assert len(pi_y) == m
    list_of_components = tt.vector.to_list(y_measurements)
    first_row = list_of_components[0][0, 0, :]
    y_int = np.ones((ny, m))
    buff_list = []
    for i, dpi in enumerate(pi_y):
        if _print:
            print "density {0}".format(i)
        for j in range(m+1):
            if _print:
                print "  j == {0}".format(j)
            if j == 0:
                buff = first_row
                if _print:
                    print "    first row {0}".format(buff.shape)
                continue
            if j == i+1 and j < m:
                if _print:
                    print "   start listing all y entry"
                if _print:
                    print "    {0} x {1} ".format(buff.shape, list_of_components[j][:, 0, :].shape)
                buff_list = [np.dot(buff, list_of_components[j][:, yi, :]) for yi in range(y_measurements.n[j])]
                if _print:
                    print "    = {0}".format(buff_list[0].shape)
                continue
            if j == i+1 and j == m:
                if _print:
                    print "   last and listing"
                if _print:
                    print "    {0} x {1} ".format(buff.shape, list_of_components[j][:, 0, 0].shape)
                buff_list = [np.dot(buff, list_of_components[j][:, yi, 0]) for yi in range(y_measurements.n[j])]
                if _print:
                    print "    = {0}".format(buff_list[0].shape)
                y_int[:, i] = np.array(buff_list)
                continue
            if j == m:
                if _print:
                    print "   last"
                if _print:
                    print "    {0} x {1} ".format(buff_list[0].shape, simps(list_of_components[j][:, :, 0],
                                                                            dx=dy, axis=1).shape)
                buff_list = [np.dot(buff_list[yi], simps(list_of_components[j][:, :, 0], dx=dy, axis=1))
                             for yi in range(y_measurements.n[j])]
                if _print:
                    print "    = {0}".format(buff_list[0].shape)
                y_int[:, i] = np.array(buff_list)
                continue
            if j < i+1:
                if _print:
                    print "   left"
                if _print:
                    print "    {0} x {1} ".format(buff.shape, simps(list_of_components[j], dx=dy, axis=1).shape)
                buff = np.dot(buff, simps(list_of_components[j], dx=dy, axis=1))
                if _print:
                    print "    = {0}".format(buff.shape)
                continue
            if j > i+1:
                if _print:
                    print "   right"
                if _print:
                    print "    {0} x {1}".format(buff_list[0].shape, simps(list_of_components[j], dx=dy, axis=1).shape)
                buff_list = [np.dot(buff_list[yi], simps(list_of_components[j], dx=dy, axis=1))
                             for yi in range(y_measurements.n[j])]
                if _print:
                    print "    = {0}".format(buff_list[0].shape)
                continue

    mean = simps(y_int[:, 0]*pi_y[0](my), dx=dy)
    return y_int, mean
# endregion

# region Create unique dofs


def create_unique_dofs(n_dofs, n_x):
    """
    returns a list of dofs in the physical domain
    """
    #                                               # Create random integers to use as degrees of freedom
    measure_points = np.zeros(n_dofs)
    for lia in range(n_dofs):                       # take care of the dofs being unique
        curr_dof = np.random.random_integers(1, n_x-1, 1)
        if np.setdiff1d(measure_points, curr_dof, assume_unique=True).size:
            measure_points[lia] = curr_dof          # dof accepted
        else:
            lia -= 1                                # already in list : create a new one
    return list(measure_points)
# endregion
