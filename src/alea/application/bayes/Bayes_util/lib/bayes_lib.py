# !usr/bin/env python
# -*- coding: latin-1 -*-

"""
  function library for bayesian estimation using tensor train format
"""

# region Imports
# region standard libraries
from __future__ import division                     # import float division as standard division

import logging                                      # get standard logging library
import logging.config                               # enable logging configuration files
from collections import Iterable

import matplotlib.pyplot as plt                     # import plot library
import numpy as np				                    # standard array manipulation lib
import time
import gc
# endregion
# region third party imports from Fenics
#   						                        import Fenics functions
from dolfin import FunctionSpace, vertex_to_dof_map, File, Mesh
# endregion
# region local libraries
# 						                            import RV object only uniform
from alea.stochastics.random_variable import UniformRV, NormalRV
#                                                   implement existing sampling methods
# from alea.application.egsz.sampling import get_projection_basis, \
#                                            compute_parametric_sample_solution_TT
#                                                   import Multiindex set
# from alea.math_utils.multiindex_set import MultiindexSet
# from alea.math_utils.multiindex import Multiindex # import multiindex class
#                                                   import SampleProblem
# from alea.application.egsz.sample_problems2 import SampleProblem

from alea.utils.parametric_array import ParametricArray
from alea.application.bayes.Bayes_util.lib.interpolation_util import (evaluate_lagrange, lagrange_to_legendre,
                                                                      get_legendre_basis)
from alea.utils.plothelper import plot_mpl
import cPickle as Pickle
from joblib import Parallel, delayed
# from alea.fem.fenics.fenics_utils import error_norm
# endregion
# region TT Tensor library
import tt		                               		# import Tensor library
import tt.amen                                      # import amen algorithm
# region Injected getitem method for tt library


def tt_getitem(self, _index):
    """
    Get element of the TT-tensor.
    @param self:
    @type self: tt.tensor
    @param _index: array_like
    @return: number -- an element of the tensor.
    """

    if len(_index) != self.d:
        print("Incorrect index length")
        return
    _prefix = 1
    for _lia in xrange(self.d):
        _cur_core = self.core[self.ps[_lia]-1:self.ps[_lia+1]-1]
        _cur_core = _cur_core.reshape((self.r[_lia], self.n[_lia], self.r[_lia + 1]), order='F')
        _cur_core = _cur_core[:, _index[_lia], :]
        _prefix = np.dot(_prefix, _cur_core)
    return _prefix[0][0]
# endregion

tt.__getitem__ = tt_getitem
# endregion
# endregion

# region Global variables for parallel use
# region Global Inner Product Variables
_global_ip_tensor_list = None
_global_ip_measurements = None
_global_ip_maxrank = None
_global_ip_precision = None
_global_ip_tensor_list_new = None
# endregion
# endregion

# region Class Iterator


class MyIterator:
    """
    simple iterator class to create and loop through multi indices
    """
    nodes = []                                      # nodes describing the used dimensions for the multi index
    CurrentIt = []                                  # current iteration through multi index

    def __init__(self, n):
        self.nodes = n
        self.CurrentIt = [0*_lia for _lia in range(len(n))]

    def increment(self):
        """
        creates the next multi index and stores it in CurrentIt
        @return: nothing
        """
        if len(self.nodes) == 0:
            return
        for _lia in range(len(self.nodes)-1, -1, -1):
            if self.CurrentIt[_lia] < self.nodes[_lia]-1:
                self.CurrentIt[_lia] += 1
                break
            else:
                self.CurrentIt[_lia] = 0

    def reset(self):
        """
        sets the CurrentIt to the zero index
        @return: nothing
        """
        self.CurrentIt = [0*_lia for _lia in range(len(self.nodes))]

# endregion

# region Function: Explicit Euler Method


def exp_euler(tensor, steps, precision, maxrank, _print=False):
    """
    Calculates the exponential of a given tensor
    @param tensor: TT-Tensor to calculate exponential from
    @type tensor: tt.tensor
    @param steps: Number of steps to use in iteration
    @type steps: int
    @param precision: used precision in SVD
    @type precision: float
    @param maxrank: used maximal rank in SVD
    @type maxrank: int
    @param _print: switch if steps should be printed
    @type _print: bool
    @return: TT-Tensor as exponential of tensor
    @rtype: tt.tensor
    """

    logging.config.fileConfig('logging.conf')       # load logger configuration
    log = logging.getLogger('test_bayes')           # get new logger object

    #if not isinstance(tensor, tt.tensor):
    #    print("ExpEuler Error: tensor is not a tt")
    #    log.error('ExpEuler Error: tensor is not a tt')
    #    raise TypeError
    if not isinstance(steps, int):
        print("ExpEuler Error: steps is not an integer")
        log.error("ExpEuler Error: steps is not an integer")
        raise TypeError
    if not isinstance(precision, float):
        print("ExpEuler Error: precision is not a float")
        log.error("ExpEuler Error: precision is not a float")
        raise TypeError
    if not isinstance(maxrank, int):
        print("ExpEuler Error: maxrank is not an integer")
        log.error("ExpEuler Error: maxrank is not an integer")
        raise TypeError

    convergence = []
    monte_carlo_steps = 1000
#                                                   create random rank 1 tt
    eulertestbuffer = tt.rand(tensor.n, len(tensor.n), [1 + 0*_lia for _lia in range(len(tensor.n)+1)])
#                                                   store tensor as list of 3d array
    eulertestbuffer = tt.tensor.to_list(eulertestbuffer)
#                                                   store true values in first comp
    eulertestbuffer[0][0, :, 0] = np.ones(tensor.n[0])
    for _lia in range(1, len(tensor.n)):	        # replace other comp with vector of ones
        eulertestbuffer[_lia][0, :, 0] = np.ones(tensor.n[_lia])
    if _print:
        print("start tensor has rank {0}".format(tensor.r))
    log.info("start tensor has rank {0}".format(tensor.r))
    if max(tensor.r) > maxrank:
        if _print:
            print("round start tensor")
        log.info(' round start tensor')
        tensor = tt.round(tensor, precision, maxrank)
        if _print:
            print("rounded to {0}".format(tensor.r))
        log.info("rounded to {0}".format(tensor.r))
    eulertestbuffer = tt.tensor.from_list(eulertestbuffer)

    if _print:
        print("Start Euler with precision {0}".format(precision))
    log.info("Start Euler with precision {0}".format(precision))
    for _lia in range(1, steps+1):
        if _print:
            print("Step {0} with ranks {1}".format(_lia, eulertestbuffer.r))
        log.info("Step {0} with ranks {1}".format(_lia, eulertestbuffer.r))
        if max(eulertestbuffer.r) > maxrank:
            if _print:
                print("  round buffer tensor from {0}".format(eulertestbuffer.r))
            log.info("  round buffer tensor from {0}".format(eulertestbuffer.r))
            eulertestbuffer = tt.tensor.round(eulertestbuffer, precision, maxrank)
            tensor = tt.tensor.round(tensor, precision, maxrank)
            if _print:
                print("  buffer tensor rounded to {0}".format(eulertestbuffer.r))
            log.info("  buffer tensor rounded to {0}".format(eulertestbuffer.r))
        eulertestbuffer += 1./steps * tensor * eulertestbuffer

        convergence.append(0)
        #                                           # create new index samples for MC
        y = [
             [np.random.random_integers(0, high=eulertestbuffer.n[lic]-1) for lic in range(len(eulertestbuffer.n))]
             for _ in range(monte_carlo_steps)
            ]
        for lia, y_k in enumerate(y):
            convergence[-1] += np.abs(eulertestbuffer[y_k] - np.exp(tensor[y_k]))
        convergence[-1] /= monte_carlo_steps
        log.info('  sampled current distance to solution: %.16f', convergence[-1])

    return eulertestbuffer, convergence
# endregion

# region Function: Adaptive Explicit Euler Method


def adaptive_exp_euler(tensor, precision, maxrank, _print=False):
    """
    Calculates the exponential of a given tensor using an adaptive stepsize control
    @param tensor: TT-Tensor to calculate exponential from
    @type tensor: tt.tensor
    @param precision: used precision in SVD
    @type precision: float
    @param maxrank: used maximal rank in SVD
    @type maxrank: int
    @param _print: switch if steps should be printed
    @type _print: bool
    @return: TT-Tensor as exponential of tensor
    @rtype: tt.tensor
    """
    #if not isinstance(tensor, tt.tensor):
    #    print "ExpEuler Error: tensor is not a tt"
    #    raise TypeError
    if not isinstance(precision, float):
        print "ExpEuler Error: precision is not a float"
        raise TypeError
    if not isinstance(maxrank, int):
        print "ExpEuler Error: maxrank is not an integer"
        raise TypeError

#                                                   # create random rank 1 tt
    eulertestbuffer = tt.rand(tensor.n, len(tensor.n), [1 + 0*_lia for _lia in range(len(tensor.n)+1)])
#                                                   # store tensor as list of 3d array
    eulertestbuffer = tt.tensor.to_list(eulertestbuffer)
    eulertestbuffer_h = 0                           # init half step tensor
    eulertestbuffer_big_h = 0                       # init step tensor
#                                                   # store true values in first comp
    max_steps = 1000                                # number of steps to do maximal if precision is not reached
    min_step_size = 1e-30                           # minimal step size to stop iteration if it is reached
    step_size = [1./100]                            # starting step size
    tau = []                                        # truncation errors
    m = 1                                          # Taylor truncation parameter
    k = 1                                           # unknown const from error estimation (heuristically set to 1)
    eulertestbuffer[0][0, :, 0] = np.ones(tensor.n[0])
    for _lia in range(1, len(tensor.n)):	        # replace other comp with vector of ones
        eulertestbuffer[_lia][0, :, 0] = np.ones(tensor.n[_lia])
    if _print:
        print "start tensor has rank {0}".format(tensor.r)
    if max(tensor.r) > maxrank:
        if _print:
            "round start tensor"
        tensor = tt.tensor.round(tensor, precision, maxrank)
        if _print:
            print "rounded to {0}".format(tensor.r)
    eulertestbuffer = tt.tensor.from_list(eulertestbuffer)

    if _print:
        print "Start Euler with precision {0} and step size {1}".format(precision, step_size[-1])
    for _lia in range(1, max_steps+1):
        if _print:
            print "Step {0} with ranks {1}".format(_lia, eulertestbuffer.r)
        if max(eulertestbuffer.r) > maxrank:
            if _print:
                print "  round"
            eulertestbuffer = tt.tensor.round(eulertestbuffer, precision, maxrank)
            tensor = tt.tensor.round(tensor, precision, maxrank)
            if _print:
                print "  rounded to {0}".format(eulertestbuffer.r)
        big_h = 4                                   # set start step size
        while True:
            if step_size[-1] <= 1/4*big_h:          # check if approximation is to rough
                big_h = 2*step_size[-1]             # repeat with new step size
                if big_h < min_step_size:           # we reached a critical level of step sizes
                    raise Exception("Exp_Euler: step size to small")
            else:                                   # iteration step is accepted
                #                                   # do extrapolation of solution
                eulertestbuffer = ((2**m)*eulertestbuffer_h - eulertestbuffer_big_h)/(2**m - 1)
                break
            eulertestbuffer_big_h = eulertestbuffer + big_h * tensor * eulertestbuffer
            eulertestbuffer_h = eulertestbuffer + 1./2 * big_h * tensor * eulertestbuffer

            tau.append((eulertestbuffer_h - eulertestbuffer_big_h))
            denom = (1/(big_h**(m+1)*(1-2**(-1*m))))
            tau[-1] *= denom
            # Problem: Norm explodes and the resulting step size is to small
            norm = float(tt.tensor.norm(tau[-1]))
            step_size.append((precision/(k*1*norm))**(1/m))

    return eulertestbuffer, step_size, tau
# endregion

# region Function: Implicit Euler Method


def imp_euler(tensor, steps, precision, maxrank, _print=False):
    """
    Calculates the exponential of a given tensor
    @param tensor: TT-Tensor to calculate exponential from
    @type tensor: tt.tensor
    @param steps: Number of steps to use in iteration
    @type steps: int
    @param precision: used precision in SVD
    @type precision: float
    @param maxrank: used maximal rank in SVD
    @type maxrank: int
    @param _print: switch if steps should be printed
    @type _print: bool
    @return: TT-Tensor as exponential of tensor
    @rtype: tt.tensor
    """

    logging.config.fileConfig('logging.conf')       # load logger configuration
    log = logging.getLogger('test_bayes')           # get new logger object

    #if not isinstance(tensor, tt.tensor):
    #    print "ImpEuler Error: tensor is not a tt"
    #    log.error("ImpEuler Error: tensor is not a tt")
    #    raise TypeError
    if not isinstance(steps, int):
        print "ImpEuler Error: steps is not an integer"
        log.error("ImpEuler Error: steps is not an integer")
        raise TypeError
    if not isinstance(precision, float):
        print "ImpEuler Error: precision is not a float"
        log.error("ImpEuler Error: maxrank is not an integer")
        raise TypeError

    convergence = []
    monte_carlo_steps = 1000
    # initial_guess = tt.rand(tensor.n, len(tensor.n),  [1 + 0*_lia for _lia in range(len(tensor.n)+1)])
    initial_guess = tt.rand(tensor.n, len(tensor.n), tensor.r)
#                                                   create random rank 1 tt
    eulertestbuffer = tt.rand(tensor.n, len(tensor.n), [1 + 0*_lia for _lia in range(len(tensor.n)+1)])
#                                                   store tensor as list of 3d array
    eulertestbuffer = tt.tensor.to_list(eulertestbuffer)
#                                                   store true values in first comp
    eulertestbuffer[0][0, :, 0] = np.ones(tensor.n[0])

    for _lia in range(1, len(tensor.n)):	        # replace other comp with vector of ones
        eulertestbuffer[_lia][0, :, 0] = np.ones(tensor.n[_lia])
    if _print:
        print "start tensor has rank {0}".format(tensor.r)
    log.info("start tensor has rank {0}".format(tensor.r))
    if max(tensor.r) > maxrank:
        if _print:
            print "round start tensor from {0}".format(tensor.r)
        log.info("round start tensor from {0}".format(tensor.r))
        tensor = tt.tensor.round(tensor, precision, maxrank)
        if _print:
            print "rounded to {0}".format(tensor.r)
        log.info("rounded to {0}".format(tensor.r))
    eulertestbuffer = tt.tensor.from_list(eulertestbuffer)

    tensor_buf = eulertestbuffer - 1./steps*tensor

    if _print:
        print "Start implicit Euler with precision {0}".format(precision)
    log.info("Start implicit Euler with precision {0}".format(precision))

    for _lia in range(1, steps+1):
        if _print:
            print "Step {0} with ranks {1}".format(_lia, eulertestbuffer.r)
        log.info("Step {0} with ranks {1}".format(_lia, eulertestbuffer.r))
        if max(eulertestbuffer.r) > maxrank:
            if _print:
                print "  round buffer tensor from {0}".format(eulertestbuffer.r)
            log.info("  round buffer tensor from {0}".format(eulertestbuffer.r))
            eulertestbuffer = tt.tensor.round(eulertestbuffer, precision, maxrank)
            if _print:
                print "  rounded to {0}".format(eulertestbuffer.r)
            log.info("  rounded to {0}".format(eulertestbuffer.r))
        if _print:
            print "  Start amen"
        log.info('  Start amen')
        eulertestbuffer = tt.amen.amen_solve(tt.diag(tensor_buf), eulertestbuffer, initial_guess, precision)

        convergence.append(0)
        #                                           # create new index samples for MC
        y = [
             [np.random.random_integers(0, high=eulertestbuffer.n[lic]-1) for lic in range(len(eulertestbuffer.n))]
             for _ in range(monte_carlo_steps)
            ]
        for lia, y_k in enumerate(y):
            convergence[-1] += np.abs(eulertestbuffer[y_k] - np.exp(tensor[y_k]))
        convergence[-1] /= monte_carlo_steps
        log.info('  sampled current distance to solution: %.16f', convergence[-1])
    return eulertestbuffer, convergence


# endregion

# region Function: Sample forward solution in stochastic space


def sample_in_stoch_space(tensor, gpcps, max_index, _poly='Legendre', _samples=None, _print=False):
    """
    Sample a solution represented in TT-tensor decomposition in the stochastic space
    @param tensor: TT-Tensor
    @type tensor: tt.tensor
    @param gpcps: array-like list of numbers indicating maximal dimension in stochastic space to use as nodal basis in
                  polynomial definition
    @type gpcps: list
    @param _poly: switch to determine the polynomials to use
    @type _poly: str
    @param _samples: array-like list of points to sample polynomials at
    @type _samples: list
    @param _print: switch if printing is active
    @type _print: bool
    @return: TT-Tensor
    @rtype: tt.tensor
    """

    from alea.application.bayes.Bayes_util.lib.interpolation_util import get_cheb_points
    fp = ''
    try:
        f = open('logging.conf')
        f.close()
        fp = 'logging.conf'
    except IOError:
        print("logging not in working directory")
        try:
            f = open('/home/marschall/WIAS/alea-testing/src/alea/application/bayes/logging.conf')
            f.close()
            fp = '/home/marschall/WIAS/alea-testing/src/alea/application/bayes/logging.conf'
        except IOError:
            print("No logging file found. -> exit()")
            exit()
    logging.config.fileConfig(fp)
    log = logging.getLogger('test_bayes')           # get new logger object

    # if not isinstance(tensor, tt.tensor):
    #    print "SampleInStochSpace Error: tensor is not a TT-Tensor"
    #    log.error("SampleInStochSpace Error: tensor is not a TT-Tensor")
    #    raise TypeError
    # if not isinstance(_nodes, list):
    #     print "SampleInStochSpace Error: nodes is not a list"
    #     log.error("SampleInStochSpace Error: nodes is not a list")
    #     raise TypeError
    if not isinstance(_poly, str):
        # print "SampleInStochSpace Error: poly is not a string"
        log.error("SampleInStochSpace Error: poly is not a string")
        raise TypeError
    # else:
        # if not isinstance(_samples, list):
        #    print "SampleInStochSpace Error: samples is not a list"
        #    raise TypeError

    components = tt.tensor.to_list(tensor)          # create array from tensor

    buffer_tt = []					                # Buffer list to store comp in
    if _print:
        # print "start sampling"
        log.info("start sampling")
#                                                   loop through components
    for _comp_index in range(len(components)):

        _comp_value = components[_comp_index]
        if _print:
            # print "shape of new component {0} is [{1},{2},{3}]".format(_comp_index, _comp_value.shape[0],
            #                                                           len(_nodes), _comp_value.shape[2])
            log.debug("shape of new component {0} is [{1},{2},{3}]".format(_comp_index, _comp_value.shape[0],
                                                                           len(_nodes), _comp_value.shape[2]))
        if _comp_index == 0:				        # skip first Core, since it is the space representation
            buffer_tt.append(_comp_value)
            continue
#                                                   estimate every core sum as matrix representation

        min_index = np.min(np.array([2*gpcps[_comp_index-1]+1, max_index]))
        if min_index < 2*gpcps[_comp_index-1]+1:
            log.warning("Solution gpc degree:{}, considered collocation points {} "
                        "for dimension {}. APPROXIMATION WARNING ".format(gpcps[_comp_index-1], min_index,
                                                                          _comp_index))
        #  !!!!! Delete ME
        # min_index = 80
        #  !!!!
        _nodes = np.array(get_cheb_points([min_index])[0])
        if _samples is None:
            _samples = _nodes

        if _poly == 'Legendre':
            #                                           # Get Legendre polynomials from RV
            # poly, norms = get_legendre_basis(max(tensor.n[1:]))
            from alea.stochastics.random_variable import UniformRV
            rv = UniformRV()
            poly = rv.orth_polys
        elif _poly == 'Hermite':
            from alea.stochastics.random_variable import NormalRV
            # !!!!!!!! Change that accoriding to the prior measure in every component
            rv = NormalRV(0, 1)
            # !!!!!!!!
            poly = rv.orth_polys
        else:
            if _poly == 'Lagrange':
                poly = [evaluate_lagrange(_samples, _nodes, j) for j in range(max(tensor.n))]
                # norms = [1] * len(poly)
            else:
                log.error("SampleInStochSpace Error: poly='{0}' is unknown".format(_poly))
                # print "SampleInStochSpace Error: poly='{0}' is unknown".format(_poly)
                raise AttributeError

        buffer_tt.append(np.zeros([_comp_value.shape[0], min_index, _comp_value.shape[2]]))
        for _lia in range(0, min_index):        # run through samples
            _poly_value_buffer = np.array(poly.eval(max(tensor.n), _nodes[_lia], all_degrees=True))
            for _lic in range(0, tensor.n[_comp_index]):
                # _poly_value = _poly.eval(_lic, _samples[_lia], all_degrees=False)
                if _poly == 'Legendre':
                    _poly_value = _poly_value_buffer[_lic]
                    # _poly_value = poly[_lic](_samples[_lia])
                    # Do we have to norm it here?
                    # _poly_value = _poly_value       # / norms[_lic] #Do not norm the polynomials
                else:
                    if _poly == 'Lagrange':
                        _poly_value = poly[_lic][_lia]
                try:
                    buffer_tt[-1][:, _lia, :] += _comp_value[:, _lic, :]*_poly_value
                except ValueError:
                    print("invalid value at compValue: {0} or poly_value {1}".format(_comp_value[:, _lic, :],
                                                                                     _poly_value))
                    quit()
    return tt.tensor.from_list(buffer_tt)

# endregion

# region Sample in stochastic space using hermite interpolation nodes and weights

def sample_in_stoch_space_hermite(tensor, gpcps, max_index, _print=False):
    """
    Sample a solution represented in TT-tensor decomposition in the stochastic space
    @param tensor: TT-Tensor
    @type tensor: tt.tensor
    @param gpcps: array-like list of numbers indicating maximal dimension in stochastic space to use as nodal basis in
                  polynomial definition
    @type gpcps: list
    @param _samples: array-like list of points to sample polynomials at
    @type _samples: list
    @param _print: switch if printing is active
    @type _print: bool
    @return: TT-Tensor
    @rtype: tt.tensor
    """

    # from alea.application.bayes.Bayes_util.lib.interpolation_util import get_cheb_points
    logging.config.fileConfig('logging.conf')       # load logger configuration
    log = logging.getLogger('test_bayes')           # get new logger object

    components = tt.tensor.to_list(tensor)          # create array from tensor

    buffer_tt = []					                # Buffer list to store comp in
    if _print:
        # print "start sampling"
        log.info("start sampling")
#                                                   loop through components
    for _comp_index in range(len(components)):

        _comp_value = components[_comp_index]
        if _comp_index == 0:				        # skip first Core, since it is the space representation
            buffer_tt.append(_comp_value)
            continue
#                                                   estimate every core sum as matrix representation

        min_index = np.min(np.array([4*gpcps[_comp_index-1]+1, max_index]))
        if min_index < 4*gpcps[_comp_index-1]+1:
            log.warning("Solution gpc degree:{}, considered collocation points {} "
                        "for dimension {}. APPROXIMATION WARNING ".format(gpcps[_comp_index-1], min_index,
                                                                          _comp_index))

        if _print:
            # print "shape of new component {0} is [{1},{2},{3}]".format(_comp_index, _comp_value.shape[0],
            #                                                           len(_nodes), _comp_value.shape[2])
            log.debug("shape of new component {0} is [{1},{2},{3}]".format(_comp_index, _comp_value.shape[0],
                                                                           min_index, _comp_value.shape[2]))
        from numpy.polynomial.hermite_e import hermegauss as gp

        _nodes, _weights = gp(min_index)

        from alea.stochastics.random_variable import NormalRV
        # TODO
        # !!!!!!!! Change that according to the prior measure in every component
        rv = NormalRV(0, 1)
        # !!!!!!!!
        poly = rv.orth_polys

        buffer_tt.append(np.zeros([_comp_value.shape[0], min_index, _comp_value.shape[2]]))
        for _lia in range(0, min_index):        # run through samples
            # print("tensor dimensions: {}".format(tensor.n))
            _poly_value_buffer = np.array(poly.eval(max(tensor.n[1:]), _nodes[_lia], all_degrees=True))
            for _lic in range(0, tensor.n[_comp_index]):
                _poly_value = np.sqrt(np.math.factorial(_lic))**(-1)*_poly_value_buffer[_lic]
                try:
                    buffer_tt[-1][:, _lia, :] += _comp_value[:, _lic, :]*_poly_value
                except ValueError:
                    print("invalid value at compValue: {0} or poly_value {1}".format(_comp_value[:, _lic, :],
                                                                                     _poly_value))
                    quit()
    return tt.tensor.from_list(buffer_tt)

# endregion

# region Function: Create a list of samples in the stochastic space


def create_parameter_samples(_m, lognormal=False, mu=0, sigma=1):
    """
    Creates a list of parameters to sample the parametric solution from
    @param _m: amount of coefficients
    @type _m: int
    @return: list of parameters
    @rtype: list
    """
    if not lognormal:
        _sample_rvs = ParametricArray(lambda i: float(ParametricArray(lambda j: UniformRV(a=-1, b=1))[i].sample(1)))
    else:
        _sample_rvs = ParametricArray(lambda i: float(ParametricArray(lambda j: NormalRV(mu=mu, sigma=sigma))[i].sample(1)))
        # _sample_rvs = ParametricArray(lambda i: float(ParametricArray(lambda j: UniformRV(a=-1, b=1))[i].sample(1)))
    return [_sample_rvs[k] for k in range(_m)]

# endregion

# region Function: Create noised measured data from a refined solution with given sample coefficients at given dofs


def create_noisy_measurement_data_dofs(_solution, _rv_samples, _dofs, _gamma):
    """

    @param _solution: Stoch Galerkin solution in TT-format
    @type _solution: tt.tensor
    @param _rv_samples: list of parameters in stochastic space
    @type _rv_samples: list
    @param _dofs: list of degrees of freedom
    @type _dofs: list
    @param _gamma: factor of the covariance operator
    @type _gamma: float
    @return: list of measurements at given space points
    @rtype: list
    """

    #if not isinstance(_solution, tt.tensor):
    #    # log.error('createNoisyMeasurementDataDofs: solution is not a tt')
    #    raise TypeError('solution is no tt')
    if not isinstance(_rv_samples, list):
        # log.error('createNoisyMeasurementDataDofs: rv_samples is not a list')
        raise TypeError('createNoisyMeasurementDataDofs: rv_samples is not a list')
    # assert isinstance(_dofs, list)
    if not isinstance(_gamma, float):
        # log.error('createNoisyMeasurementDataDofs: gamma is not a float')
        raise TypeError('createNoisyMeasurementDataDofs: gamma is not a float')

    testmeasurements = []                           # create empty list
    #                                               # get legendre polynomials and norms
    poly, norms = get_legendre_basis(max(_solution.n[1:]))
    #                                               # get list of tensor representation
    list_of_components = tt.tensor.to_list(_solution)
    for i, x in enumerate(_dofs):                   # enumerate through all dofs
        #                                           # first tt component has to be a row or one-dim for every x and 0
        assert(len(list_of_components[0][:, x, 0]) == 1)
        buff = list_of_components[0][0, x, :]       # get first row vector
        #                                           # enumerate through every component
        for comp_index in range(len(list_of_components)):

            if comp_index == 0:                     # ok, skip the first one
                continue
            comp_value = list_of_components[comp_index]
            #                                       # reached last component
            if comp_index == len(list_of_components)-1:
                #                                   # evaluate polynomials first
                buff_3 = np.array([comp_value[:, mu, 0]*poly[mu](_rv_samples[comp_index-1])  # /norms[mu]
                                   for mu in range(_solution.n[comp_index])])
                #                                   # evaluate sum over all polynomials
                buff_2 = np.sum(buff_3, axis=0)
                #                                   # multiply with previous row
                buff = np.dot(buff, buff_2)
                continue                            # continue for loop, or we could quit it here
            buff = np.dot(buff, np.sum(np.array([comp_value[:, mu, :]*poly[mu](_rv_samples[comp_index-1])  # /norms[mu]
                                                for mu in range(_solution.n[comp_index])]),
                                       axis=0)
                          )                         # as above but as a one liner
        assert isinstance(buff, float)              # assert that buff is just a float
        #                                           # add the noise factor
        # noinspection PyArgumentList
        buff += np.random.randn() * _gamma
        testmeasurements.append(buff)

    return testmeasurements
# endregion

# region Function: Get dofs from observation points


def get_dofs_from_observation(_mesh, _observation_points, _femdegree):
    """
    Creates a list of dofs that correspond to the given observation points
    @param _mesh: mesh created by the sample problem
    @param _observation_points: list of coordinates used to measure space points at
    @param _femdegree: degree of fe basis functions
    @return: list of dofs corresponding to observation points
    """
    vertex_list = []					            # list containing the vertices of the coordinates
    _dof_list = []					                # list containing the dof positions
    space = FunctionSpace(_mesh, 'CG', _femdegree)  # create fenics Function space
    vertex_to_dof = vertex_to_dof_map(space)	    # Get vertex to dof mapping

    for item in _observation_points:			    # loop through needed data points
        _lia = 0					                # loop variable
        for cord in _mesh.coordinates():		    # loop through possible coordinates
            #                                       correct coordinate found
            if item[0] == cord[0] and item[1] == cord[1]:
                vertex_list.append(_lia)			# store vertex number in list
                break					            # exit for loop
            _lia += 1                               # update loop variable

    for vertex in vertex_list:			            # loop through vertex list
        _dof_list.append(vertex_to_dof[vertex])		# get dof from vertex
    return _dof_list
# endregion

# region Function: Create rank one tensor from vector


def create_rank_one_tensor_from_vector(_vector, _dimensions):
    """
    Create a random rank one tensor and replace its values with the one given in the vector
    @param _vector: list of values
    @param _dimensions: list of dimensions
    @return: rank one tensor
    """

    from bayes_lib_simple import create_rank_one

    return create_rank_one(_vector, _dimensions)
# endregion

# region Function: Create interpolation matrix


def create_interpolation_matrix(tensor, _samples):
    """
    While sampling the solution tensor, save the polynomial values
    @param tensor: TT-Tensor
    @type tensor: tt.tensor
    @param _samples: array-like list of sample points in stochastic space
    @type _samples: list
    @return: list of interpolation matrices
    @rtype: list
    """

    #if not isinstance(tensor, tt.tensor):
    #    print "CreateInterpolationMatrix Error: tensor is not a TT-Tensor"
    #    raise TypeError
    if not isinstance(_samples, list):
        print "CreateInterpolationMatrix Error: _samples is not a list"
        raise TypeError

    components = tt.tensor.to_list(tensor)          # create array from tensor

    interpol_matrix = []					        # Buffer list to store interpolation matrices in
    _rv = UniformRV()				                # Get uniform RV object
    _poly = _rv.orth_polys				            # Get Legendre polynomials from RV
#                                                   loop through components
    for _comp_index_t, _comp_value in np.ndenumerate(components):
        _comp_index = _comp_index_t[0]              # index is just first entry of hole iterator
        if _comp_index == 0:
            continue
        interpol_matrix.append(np.empty([tensor.n[_comp_index], len(_samples)]))
        for _lia in range(0, len(_samples)):        # run through samples
            for _lic in range(0, tensor.n[_comp_index]):
                _poly_value = _poly.eval(_lic, _samples[_lia], all_degrees=False)
                interpol_matrix[-1][_lic, _lia] = _poly_value
    return interpol_matrix
# endregion

# region Function: Evaluate Tensor


def evaluate_tensor(tensor, _index, _check_negativity=False):
    """
    evaluates the tensor at the first index many indices started from (0,0,...,0,0), (0,0,...,0,1)
    @param tensor: tt tensor to evaluate
    @type tensor: TT-Tensor
    @param _index: last index to evaluate
    @type: _index: integer
    @param _check_negativity: check if there is a negative value in the tensor check list default = False
    @type: _check_negativity: boolean
    @return: list of float evaluations
    @rtype: list
    """

    #if not isinstance(tensor, tt.tensor):
    #    print "evaluateTensor Error: tensor is not a TT-Tensor"
    #    raise TypeError
    if not isinstance(_index, int):
        print "evaluateTensor Error: index is not an integer"
        raise TypeError
    if _index < 0:
        print "evaluateTensor Error: index must be positive"
        raise IndexError
    if np.prod(np.array(tensor.n)) < _index:
        print "evaluateTensor Error: index must be inside the range of the tensor"
        raise IndexError
    if not isinstance(_check_negativity, bool):
        print "evaluateTensor Error: _check_negativity must be a boolean"
        raise TypeError

    _eval = np.zeros(_index)
    _iter = MyIterator(tensor.n)
    for _lia in range(0, _index):
        _eval[_lia] = tensor[_iter.CurrentIt]
        if _check_negativity and _eval[_lia] < 0:
            print "negative value {0} at {1}".format(_eval[_lia], _lia)
        _iter.increment()
    _iter.reset()
    return _eval

# endregion

# region Function: Calculate inner product needed in the Bayesian procedure


def calculate_inner_product(_tensor, _gamma, _dof_list, _true_values,
                            _round=True, _round_precision=1e-10, _round_rank=10, _print=False):
    """
    Calculates the inner product of the given tensor as described in the Bayesian procedure and returns the
    misfit-/ potential function
    @param _tensor: tensor to work with
    @type _tensor: tt.tensor
    @param _gamma: covariance factor
    @type _gamma: float
    @param _dof_list: list of degrees of freedom to use in observation operator
    @type _dof_list: list
    @param _true_values: list of values coming from true measurements (or dummy data)
    @type _true_values: list
    @param _round: switch to round the solution
    @type _round: bool
    @param _round_precision: rounding precision to use in HSVD
    @type _round_precision: float
    @param _round_rank: maximal rank to use in HSVD
    @type _round_rank: int
    @param _print: switch if printing is enabled
    @return: tensor corresponding to the misfit function
    @rtype: tt.tensor
    """

    logging.config.fileConfig('logging.conf')       # load logger configuration
    log = logging.getLogger('test_bayes')           # get new logger object

    # if not isinstance(_tensor, tt.tensor):
    #    print "calculate_inner_product Error: _tensor must be a TT-Tensor"
    #    log.error("calculate_inner_product Error: _tensor must be a TT-Tensor")
    #    raise TypeError
    if not isinstance(_gamma, float):
        print "calculate_inner_product Error: _gamma must be a float"
        log.error("calculate_inner_product Error: _gamma must be a float")
        raise TypeError
    # if not isinstance(_dof_list, list):
    #    print "calculate_inner_product Error: _dof_list must be a list like array"
    #    raise TypeError
    if not isinstance(_true_values, list):
        print "calculate_inner_product Error: _true_values must be a list like array"
        log.error("calculate_inner_product Error: _true_values must be a list like array")
        raise TypeError
    if not isinstance(_round, bool):
        print "calculate_inner_product Error: _round must be a boolean"
        log.error("calculate_inner_product Error: _round must be a boolean")
        raise TypeError
    if not isinstance(_round_precision, float):
        print "calculate_inner_product Error: _round_precision must be a float"
        log.error("calculate_inner_product Error: _round_precision must be a float")
        raise TypeError
    if not isinstance(_round_rank, int):
        print "calculate_inner_product Error: _round_rank must be an integer"
        log.error("calculate_inner_product Error: _round_rank must be an integer")
        raise TypeError

    from bayes_lib_simple import apply_observation  # ,apply_inner_product

    if _print:
        print "Apply Observation"
    log.info('Apply Observation')
    solution = apply_observation(_tensor, _dof_list, _round=_round, _precision=_round_precision,
                                 _maxrank=_round_rank, _print=_print)

    if _print:
        print "Apply Inner Product"
    log.info('Apply Inner Product NEW')
    from alea.application.bayes.Bayes_util.lib.bayes_lib_simple import apply_inner_product
    solution = apply_inner_product(solution, _gamma, False, _round_precision, _round_rank)
    #solution = apply_bayes_inner_product_new(solution, _true_values, _gamma, _round_precision, _round_rank,
    #                                         _print=_print)
    # if _print:
    #    print "Create Values"
    # log.info('Create Values')
    # value_list = create_noisy_measurement_data_dofs(_tensor, _mesh, _true_values, _dof_list, 3, 1)
    # for lia in range(len(solution)):
    #    value = create_rank_one_tensor_from_vector(_true_values[lia], solution[lia].n)
    #    solution[lia] = value - solution[lia]

    # if _print:
    #    print "Apply Inner Product"
    # log.info('Apply Inner Product')
    # solution = -0.5*apply_inner_product(solution, _gamma=_gamma, _precision=_round_precision,
    #                                    _maxrank=_round_rank, _print=_print)
    return solution

# endregion

# region Function: Calculate inner Product using rank reduction and parallel computation
def apply_bayes_inner_product_new(ten_list, measurements, gamma, precision, maxrank, _print=True, n_cpu=10,
                                  _round=False):
    """

    :param ten_list:
    :param measurements:
    :param gamma:
    :param precision:
    :param maxrank:
    :param _print:
    :param n_cpu:
    :param _round
    :return:
    """

    assert len(measurements) == len(ten_list)
    assert isinstance(gamma, float)
    assert isinstance(precision, float)
    assert isinstance(maxrank, int)

    global _global_ip_tensor_list
    global _global_ip_measurements
    global _global_ip_maxrank
    global _global_ip_precision
    # print "tensor0.rank = {}".format(value.r)
    # print "tensor1.rank = {}".format(tensor1.r)
    # print "tensor2.rank = {}".format(tensor2.r)
    _global_ip_tensor_list = ten_list
    _global_ip_measurements = measurements
    _global_ip_maxrank = maxrank
    _global_ip_precision = precision
    if n_cpu < 2:
        value = calculate_tensors(0, 1, _print=_print)
        ten1 = calculate_tensors(1, 1, _print=_print)
        ten2 = calculate_tensors(2, 1, _print=_print, _round=_round)
    else:
        value = calculate_tensors(0, 1, _print=_print)
        ten1 = calculate_tensors(1, 1, _print=_print)
        ten2 = calculate_tensors(2, n_cpu, _print=_print, _round=_round)

        # print "output: {}".format(output)
    retval = value + ten1 + ten2
    return (gamma**(-1))*(retval)


# endregion

# region Function: Test Euler exponential result
def test_exponential(_tensor, _result, _indices, _plot=False):
    """
    Calculates the error of the exact pointwise exponential and the given _result tensor
    @param _tensor: original tensor to calculate pointwise exponential from
    @type _tensor: tt.tensor
    @param _result: resulting tensor from approximated exponential calculation
    @type _result: tt.tensor
    @param _indices: maximal number of indices to check the exponential
    @type _indices: int
    @param _plot: switch if the residual should be plotted
    @type _plot: bool
    @return: maximal residual value
    @rtype: float
    """

    _euler_check_start = np.zeros(_indices)
    _euler_check_end = np.zeros(_indices)
    _euler_check_true = np.zeros(_indices)
    _myiter = MyIterator(_tensor.n)                  # initialize iterator to go through solution
    for _lia in range(0, _indices - 1):
        _euler_check_start[_lia] = _tensor[_myiter.CurrentIt]
        _euler_check_end[_lia] = _result[_myiter.CurrentIt]
        _myiter.increment()
        try:
            _euler_check_true[_lia] = np.exp(_euler_check_start[_lia])
        except ValueError:
            print "Error while calculating exponential of {0}".format(_euler_check_start[_lia])
            quit()
    _myiter.reset()
    if _plot:
        plt.figure(0)
        plt.title("Error of the euler Method result and the exact, pointwise exponential")
        plt.plot(abs(_euler_check_end - _euler_check_true))
        plt.show()
    return max(abs(_euler_check_end - _euler_check_true))
# endregion

# region Function: Interpolate in stochastic space
def interpolate_in_stoch_space(_tensor, _samples):
    """
    interpolates the sampled misfit function using Lagrange interpolation at cheb nodes and a basis transformation to
    orthogonal Legendre polynomials
    @param _tensor: tensor as sampled misfit function
    @type _tensor: tt.tensor
    @param _samples: list of sample points
    @type _samples: list
    @return: solution tensor corresponding to the interpolated function
    @rtype: tt.tensor
    """

    #if not isinstance(_tensor, tt.tensor):
    #    print "SampleInStochSpace Error: _tensor is not a TT-Tensor"
    #    raise TypeError
    if not isinstance(_samples, list):
        print "SampleInStochSpace Error: _samples is not a list"
        raise TypeError

    #                                               create interpolation matrix from cheb nodes and orth polynomials
    # list_interpol_matrix = create_interpolation_matrix(_tensor, _samples)
    list_of_cores = tt.tensor.to_list(_tensor)      # go back to list representation of components
    _transfer_matrix, _transfer_basis, _transfer_norms = lagrange_to_legendre(_samples)
#                                                   loop through components
    for comp_index_t, comp_value in np.ndenumerate(list_of_cores):
        comp_index = comp_index_t[0]
        if comp_index == 0:                         # leave out the first spatial component
            continue
        for lia in range(0, comp_value.shape[0]):   # loop through matrices in component representation
                                                    # do the basis transformation
            list_of_cores[comp_index][lia, :, :] = np.dot(_transfer_matrix, comp_value[lia, :, :])

    return tt.tensor.from_list(list_of_cores)

# endregion

# region Function: calculate mean
def calculate_mean(_tensor):
    """
    calculates the mean of the solution given as in the SG approach
    @param _tensor: tensor to use in mean calculation
    @type _tensor: tt.tensor
    @return: mean value
    @rtype: float
    """

    # if not isinstance(_tensor, tt.tensor):
    #    print "calculateMean Error: _tensor is not a TT-Tensor"
    #    raise TypeError

    index = np.array([0*lia for lia in range(len(_tensor.n))])
    return _tensor[index]
# endregion

# region Function: Sample marginal densities
def sample_marginal_densities(_tensor, _sample_points, _mean):
    """
    calculates the marginal densities by sampling at _sample_points many equidistant grid points
    @param _tensor: tensor to use in calculation
    @type _tensor: tt.tensor
    @param _sample_points: number of sample points to use
    @type _sample_points: int
    @param _mean: mean value to normalize
    @type _mean: float
    @return: list of sampled densities
    @rtype: list
    """
    # if not isinstance(_tensor, tt.tensor):
    #    print "SampleMarginalDensities Error: _tensor is not a TT-Tensor"
    #    raise TypeError
    if not isinstance(_sample_points, int):
        print "SampleMarginalDensities Error: _sample_points is not an integer"
        raise TypeError
    if not isinstance(_mean, float):
        print "SampleMarginalDensities Error: _mean is not a float"
        raise TypeError

    list_of_cores = tt.tensor.to_list(_tensor)
    density_samples = np.linspace(-1, 1, _sample_points, True)

    component_density = []
    # rv = UniformRV()				                # Get uniform RV object
    # poly = rv.orth_polys				            # Get Legendre polynomials from RV
    for comp_index_t, comp_value in np.ndenumerate(list_of_cores):
        comp_index = comp_index_t[0]
        if comp_index == 0:
            continue
        component_density.append(np.zeros([len(density_samples)]))
        for lia in range(0, len(density_samples)):
            component_density[-1][lia] = 0
            for lic in range(_tensor.n[comp_index]):
                                                    # Get Legendre Basis functions and norms
                _legendre_basis, _legendre_norm = get_legendre_basis(lic)
                # poly_value = poly.eval(lic, density_samples[lia], all_degrees=False)
                #                                   # evaluate Legendre polynomial at current data point and normalize
                if lic == 0:
                    poly_value = 1
                else:
                    poly_value = _legendre_basis[-1](density_samples[lia])/_legendre_norm[-1]

                index = [0] + [0 * lix for lix in range(1, comp_index)] + [lic] + \
                        [0 * lix for lix in range(comp_index + 1, len(_tensor.n))]
                sol_value = float(_tensor[index])
                component_density[-1][lia] += sol_value * poly_value
        component_density[-1] *= _mean**(-1) * 0.5

    return component_density
# endregion

# region Function: Export SGFEM solution
def export_solution(export_file, sol, polysys, femdegree, gpcps, domain, decayexp, gamma,
                    rvtype, max_mesh_cells=-1):
    """
    exports the asgfem solution to a file using binary pickling
    """
    # determine filename

    for i, S in enumerate(sol):
        v, cg = S["V"], S["CG"]
        # inject additional solution data
        v.CG = (cg.ufl_element().family(), cg.ufl_element().degree())
        del S["CG"]
        v.max_order = len(v.r) - 1
        v.max_rank = max(v.r)

        # export mesh of FunctionSpace
        mesh = cg.mesh()
        File(export_file + "-mesh-%i.xml" % i) << mesh

        # export mesh pdf
        if max_mesh_cells == -1 or mesh.num_cells() <= max_mesh_cells:
            if i == 0:
                plt.figure(1)
                plt.gca().set_aspect('equal')
                plt.gca().xaxis.set_ticklabels([])
                plt.gca().yaxis.set_ticklabels([])
#                plt.show(block=False)
            plt.clf()
            plot_mpl(mesh)
#            plt.title('iteration %i' % i)
            plt.draw()
            plt.savefig(export_file + "-mesh-%i.pdf" % i, dpi=None, facecolor='w', edgecolor='w',
                        orientation='portrait', papertype=None, format='pdf',
                        transparent=False, bbox_inches='tight', pad_inches=0.1,
                        frameon=None)

    # write out
    inf = {"polysys": polysys, "femdegree": femdegree, "gpcps": gpcps,
           "domain": domain, "decayexp": decayexp, "gamma": gamma, "rvtype": rvtype}
    with open(export_file + '.dat', 'wb') as outfile:
        Pickle.dump((sol, inf), outfile, Pickle.HIGHEST_PROTOCOL)
# endregion

# region Function: Export Bayes Objects
def export_bayes(export_file, sol, samples, dof_list, true_values, euler_step_size, mc_mean, precision, mean, poly,
                 step_size, tau, convergence, toy_coeff):
    """
    exports all the important information of the bayesian procedure by pickling it to a file
    @param export_file: path to export file
    @param sol: solution tensor
    @param samples: points to sample in stochastic dimension (only 1D)
    @param dof_list: list of degrees of freedom to sample toy data from
    @param true_values: values of the toy data
    @param euler_step_size: step sizes for the implicit scheme
    @param mc_mean: list of monte carlo means after iteration steps
    @param precision: accuracy used in all steps
    @param mean: Z
    @param poly: interpolated polynomials describing the densities
    @param step_size: step_sizes from the adaptive explicit euler method
    @param tau: list of truncation error approximations
    @param convergence: list of errors of convergence
    @param toy_coeff: true values to recover, i.e. true values are taken from those coefficients
    @return: nothing
    """
    inf = {"samples": samples, "dof_list": dof_list, "true_values": true_values, "euler_step_size": euler_step_size,
           "mc_mean": mc_mean, "precision": precision, "mean": mean, "poly": poly, "step_size": step_size,
           "tau": tau, "conv": convergence, "toy_coeff": toy_coeff}
    with open(export_file + '.dat', 'wb') as outfile:
        Pickle.dump((sol, inf), outfile, Pickle.HIGHEST_PROTOCOL)
# endregion

# region Function: Import SGFEM solution
def import_solution(solution_file, max_dofs=-1, _print=False):
    """
      imports the asgfem solution object from binary files
    """
    with open(solution_file + '.dat', 'rb') as infile:
        sol, inf = Pickle.load(infile)
        polysys, femdegree = inf["polysys"], inf["femdegree"]

    if _print:
        for i, S in enumerate(sol):
            print "#%i \t dofs %i \t M %i \t gpcp %s" % (i, S["DOFS"], S["nr_active_ydim"], S["gpcps"])

    for i, S in enumerate(sol):
        # restore FunctionSpace and MultiindexSet
        mesh = Mesh(solution_file + '-mesh-%i.xml' % i)
        v = S["V"]
        S["CG"] = FunctionSpace(mesh, v.CG[0], v.CG[1])
        # enrich V for sampling
        # cos = MultiindexSet.createFullTensorSet(len(V.r) - 2, gpcps[i])
        # mis = [Multiindex(x) for x in cos.arr]
        # V.active_indices = lambda: mis
        v.polysys = polysys
        v.gpcps = S["gpcps"][i]
        v.V = S["CG"]

        # restrict iteration # to maximal # dofs if requested
        if (max_dofs > 0) and (S["DOFS"] > max_dofs):
            sol = sol[:i]
            print "considering solution up to iteration %i with %i dofs..." % (i-1, sol[-1]["DOFS"])
            break

    return sol, inf

# endregion

# region Function: Import Bayes Object
def import_bayes(solution_file):
    """
    imports the solution_file and saves the solution tensor and further information about the bayesian procedure
    @param solution_file: path to the Procedure.dat file
    @return: solution tensor and further information
    @rtype: tt.tensor, dict
    """
    with open(solution_file + '.dat', 'rb') as infile:
        sol, inf = Pickle.load(infile)

    return sol, inf
# endregion

# region Function: Ensure iterable
def ensure_iterable(_x):
    """
    simple needed function to ensure that x is an iterable object
    """
    return _x if isinstance(_x, Iterable) else (_x,)
# endregion

# region Function: Evaluate
def evaluate(tensor, _poly='Legendre', _samples=None):
    """
    working interpolation and sampling method. Just insert the tensor corresponding to the evaluation at
    quadrature points and the polynomial system you want to use.
    :param tensor: tt tensor sampled at quadrature points
    :param _nodes: quadrature nodes in one dimension
    :param _poly: either Lagrange or Legendre
    :param _samples: points to sample in each dimension at. corresponds to tensor indices
    :return: value
    """

    from alea.application.bayes.Bayes_util.lib.interpolation_util import get_cheb_points
    from numpy.polynomial.hermite_e import hermegauss as gp
    from alea.stochastics.random_variable import NormalRV

    if _poly is not 'Lagrange' and _poly is not 'Hermite':
        print("SampleInStochSpace Error: poly='{0}' is unknown".format(_poly))
        raise AttributeError
    if _samples is None:
        raise AttributeError("SampleInStochSpace Error: even if we state None here, this is no suitable way of "
                             "sampling someting =)")
    #                                               # create array from tensor
    # print tensor
    components = tt.tensor.to_list(tt.vector.copy(tensor))
    buffer_tt = None
    #                                               # loop through components
    for lia in range(len(components)):
        _comp_index = lia
        _comp_value = components[lia]
        # print "shape of new component {0} is [{1},{2},{3}]".format(_comp_index, _comp_value.shape[0],
        #                                                           len(_nodes), _comp_value.shape[2])
        if _comp_index == 0:				        # skip first Core, since it is the space representation
            buffer_tt = _comp_value
            continue
        # if _poly == 'Legendre':
        #     for lib in range(0, _comp_value.shape[0]):
        #         for lic in range(0, _comp_value.shape[2]):
        #             _comp_value[lib, :, lic] = np.dot(_transfer_matrix, _comp_value[lib, :, lic])
        if _poly == 'Lagrange':
            _nodes = np.array(get_cheb_points([_comp_value.shape[1]])[0])
            poly = [evaluate_lagrange(_samples, _nodes, j) for j in range(len(_nodes))]
        elif _poly == 'Hermite':
            # !!!!!! Change according to prior
            rv = NormalRV(mu=0, sigma=1)
            nodes, weights = gp(_comp_value.shape[1])
            poly = rv.orth_polys.eval(_comp_value.shape[1], _samples, True)
            # !!!!!!
        buffer_core = None
        for _lic in range(0, tensor.n[_comp_index]):
            # if _poly == 'Legendre':
            #     if _lic == 0:
            #         buffer_core = _comp_value[:, _lic, :] * _transfer_basis[_lic](_samples[_comp_index])/
            # _transfer_norms[_lic]
            #     else:
            #         buffer_core += _comp_value[:, _lic, :] * _transfer_basis[_lic](_samples[_comp_index])/
            # _transfer_norms[_lic]
            #     continue
            # elif _poly == 'Lagrange'
            if _poly == 'Lagrange':
                # print "poly_{}({})".format(_comp_index, _lic)
                _poly_value = poly[_lic][_comp_index-1]
                try:
                    if _lic == 0:
                        buffer_core = _comp_value[:, _lic, :] * _poly_value
                    else:
                        buffer_core += _comp_value[:, _lic, :] * _poly_value
                except ValueError:
                    print("invalid value at compValue: {0} or poly_value {1}".format(_comp_value[:, _lic, :],
                                                                                     _poly_value))
                    quit()
            elif _poly == 'Hermite':
                if _lic > tensor.n[_comp_index]*0.5 - 1:
                    continue
                _poly_value = poly[_lic]

                def hermite_coeff(weights, func_evals, nodes, index):
                    return np.sum(
                        [weight * f * rv.orth_polys.eval(index, node, False) / np.sqrt((2 * np.pi)) for (weight, node,
                                                                                                         f) in
                         zip(weights, nodes, func_evals)])
                try:
                    if _lic == 0:
                        buffer_core = hermite_coeff(weights, [np.array(_comp_value[:, lib, :]) for lib in
                                                              range(_comp_value.shape[1])], nodes, _lic) * _poly_value
                    else:
                        buffer_core += hermite_coeff(weights, [np.array(_comp_value[:, lib, :]) for lib in
                                                               range(_comp_value.shape[1])], nodes, _lic) * _poly_value
                except ValueError:
                    print("invalid value at compValue: {0} or poly_value {1}".format(_comp_value[:, _lic, :],
                                                                                     _poly_value))
                    quit()
        buffer_tt = np.dot(buffer_tt, buffer_core)

    return buffer_tt[0,0,0]
# endregion

# region Function: Evaluate
def evaluate_marginal(tensor, dimension, _samples=None):
    """
    working interpolation and sampling method for marginal density estimation.
    Just insert the tensor corresponding to the evaluation at
    quadrature points and the polynomial system you want to use.
    :param tensor: tt tensor sampled at quadrature points
    :param dimension: the dimension to sample in. The remaining will be integrated out.
    :param _samples: points to sample in each dimension at. corresponds to tensor indices
    :return: value
    """

    from alea.application.bayes.Bayes_util.lib.interpolation_util import get_cheb_points

    #                                               # Get Legendre polynomials from RV
    #                                               # create array from tensor
    components = tt.tensor.to_list(tt.vector.copy(tensor))
#                                                   loop through components
    for _comp_index in range(len(components)):
        _comp_value = components[_comp_index]
        # print "shape of new component {0} is [{1},{2},{3}]".format(_comp_index, _comp_value.shape[0],
        #                                                           len(_nodes), _comp_value.shape[2])
        if _comp_index == 0:				        # skip first Core, since it is the space representation
            buffer_tt = _comp_value
            continue
        _nodes = np.array(get_cheb_points([_comp_value.shape[1]])[0])
        _transfer_matrix, _transfer_basis, _transfer_norms, N_1 = lagrange_to_legendre(_nodes)
        # print(_transfer_norms)
        # print(N_1)
        for lib in range(0, _comp_value.shape[0]):
            for lic in range(0, _comp_value.shape[2]):
                _comp_value[lib, :, lic] = (np.dot(_transfer_matrix, _comp_value[lib, :, lic]) / _transfer_norms) * N_1

        buffer_core = None
        if _comp_index != dimension:
            #print("transfer basis ({}) / transfer norm {} * norm {} = {}".format(_samples[_comp_index], _transfer_norms[0], N_1[0], _transfer_basis[0](_samples[_comp_index])))
            buffer_core = _comp_value[:, 0, :]
        else:
            for _lic in range(0, tensor.n[_comp_index]):
                if _lic == 0:
                    buffer_core = _comp_value[:, _lic, :] * _transfer_basis[_lic](_samples[_comp_index]) /_transfer_norms[_lic]
                else:
                    buffer_core += _comp_value[:, _lic, :] * _transfer_basis[_lic](_samples[_comp_index]) /_transfer_norms[_lic]

        buffer_tt = np.dot(buffer_tt, buffer_core)

    return buffer_tt[0,0,0]
# endregion

# region Function: calculate_tensors (parallel version for inner product)
def calculate_tensors(step, n_cpu_inner, _print=False, _round=False):
    """

    :param step:
    :param n_cpu_inner:
    :param _print:
    :return:
    """
    global _global_ip_tensor_list_new

    _ten_list = _global_ip_tensor_list
    _measurements = _global_ip_measurements

    # print "step: {} , CPU USAGE: {}".format(step, n_cpu_inner)

    if step == 0:
        # print "tensor 0 globals received"
        value1 = create_rank_one_tensor_from_vector(0, _ten_list[0].n)*(len(_measurements)-1)
        # print "tensor 0 value 1 created"
        value2 = create_rank_one_tensor_from_vector(np.sum([_measurements[lia]**2
                                                            for lia in range(len(_measurements))]),
                                                    _ten_list[0].n)
        # print "tensor 0 done"
        value = value1 + value2
        return value
    elif step == 1:
        # print "tensor 1 globals received"
        tensor1_sum = tt.vector.to_list(tt.vector.copy(_ten_list[0]))
        assert tensor1_sum[0].shape[0] == 1
        assert tensor1_sum[0].shape[1] == 1
        # print "tensor 1 assertion passed"
        tensor1_sum[0][0, 0, :] = [0 for _ in range(tensor1_sum[0].shape[2])]
        tensor1_sum = tt.vector.from_list(tensor1_sum)
        tensor1_sum *= len(_measurements)-1
        # print "tensor 1 sum created"
        tensor1_first = tt.vector.to_list(tt.vector.copy(_ten_list[0]))
        tensor1_first[0][0, 0, :] = np.sum([tt.vector.to_list(_ten_list[lic])[0][0, 0, :]*_measurements[lic]
                                            for lic in range(len(_ten_list))], axis=0)
        # print "tensor 1 first created"
        tensor1_first_ten = tt.vector.from_list(tensor1_first)
        tensor1 = -2*(tensor1_sum + tensor1_first_ten)
        # print "tensor 1 done"
        return tensor1
    elif step == 2:
        # print "tensor 2 globals received"
        output = np.array(Parallel(n_cpu_inner)(delayed(multiply_inner_product_tensors)(lix)
                                                for lix in range(0, len(_measurements))))
        gc.collect()
        # print "output: {}".format(len(output))

        _global_ip_tensor_list_new = output
        while len(_global_ip_tensor_list_new) > 1:
            # print "length of list: {}".format(len(_global_ip_tensor_list_new))
            if len(_global_ip_tensor_list_new) % 2 == 0:
                #    print "length is even"
                output = np.array(Parallel(n_cpu_inner)(delayed(sum_inner_product_tensors)(lix)
                                  for lix in range(0, len(_global_ip_tensor_list_new), 2)))
            else:
                #    print "length is odd"
                output = np.array(Parallel(n_cpu_inner)(delayed(sum_inner_product_tensors)(lix, _round=_round)
                                  for lix in range(0, len(_global_ip_tensor_list_new)-1, 2)))
                output = np.insert(output, 0, _global_ip_tensor_list_new[-1])
            _global_ip_tensor_list_new = output
            # print "loop done. new length: {}".format(len(_global_ip_tensor_list_new))
#        time.sleep(1)
        tensor2 = _global_ip_tensor_list_new[0]
        # print "tensor 2 done"
#        gc.collect()
        return tensor2
# endregion

# region Function multiply inner product tensors (parallel use)
def multiply_inner_product_tensors(index):
    # print "square tensor with index: {}".format(index)
    _ten_list = _global_ip_tensor_list
    return tt.vector.copy(_ten_list[index])*tt.vector.copy(_ten_list[index])

# endregion
def sum_inner_product_tensors(index, _round=False):
    # print "sum tensors {} and {}".format(index, index+1)
    _ten_list = [tt.vector.copy(_global_ip_tensor_list_new[lib]) for lib in range(len(_global_ip_tensor_list_new))]
    _maxrank = _global_ip_maxrank
    _precision = _global_ip_precision
    _ten = _ten_list[index]+_ten_list[index+1]
    _ten_old = _ten_list[index]+_ten_list[index+1]
    if max(_ten.r) > _maxrank and _round:
        print "round tensor of rank {0}".format(_ten.r)
        _ten = tt.vector.round(_ten, _precision, _maxrank)
        print "tensor rounded to rank {0}, error: {1}".format(_ten.r, tt.vector.norm(_ten - _ten_old))

    return _ten
