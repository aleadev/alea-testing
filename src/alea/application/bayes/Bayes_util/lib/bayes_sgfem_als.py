# -*- coding: latin-1 -*-
"""
  adaptive stochastic Galerkin finite element method for parametric partial differential equations
  @author: M. Eigel, M Marschall, M. Pfeffer,

"""

# region Imports
from __future__ import division                     # float division as standard
from copy import deepcopy

# region ALEA imports
from alea.application.egsz.sample_problems2 import SampleProblem
from alea.application.egsz.sample_domains import SampleDomain
from alea.application.egsz.affine_field import AffineField
from alea.fem.fenics.fenics_basis import FEniCSBasis
from alea.fem.fenics.fenics_vector import FEniCSVector
from alea.application.egsz.tt_residual_estimator import ttEvaluateResidualEstimator
from alea.application.egsz.tt_tail_estimator import ttEvaluateUpperTailBound, ttMark_y
from alea.utils.timing import get_current_time_string
# endregion
# region tensor SGFEM discretisation
#                                                   # deterministic and stochastic operator creation
from alea.application.tt_asgfem.apps.tensor_setup import (construct_deterministic_sgfem_operators,
                                                          construct_stochastic_sgfem_operators)
from alea.application.tt_asgfem.apps.sgfem_als_util import (compute_FE_matrices, export_solution,
                                                            compute_FE_matrix_coeff, compute_test_FE_matrix)
# endregion

# region FEniCS imports
#                                                   # used FEniCS functions
from dolfin import FunctionSpace, Function, interpolate, refine, project, cells, MeshFunction
#                                                   # currently unused FEniCS functions
# from dolfin import Mesh, Constant, set_log_level, WARNING, DEBUG, INFO, interactive, File
# endregion

# region TT ALS
import tt                                           # Oseledets TT tensor library
import alea.application.tt_asgfem.tensorsolver.tt_sparse_matrix as ttsmat
from alea.application.tt_asgfem.tensorsolver.tt_lognormal import (generate_lognormal_tt, sample_lognormal_tt,
                                                                  generate_operator_tt, extract_mean,
                                                                  get_coeff_upper_bound, tt_cont_coeff,
                                                                  sample_cont_coeff, fully_disc_first_core)
#                                                   # ALS solver for parametric PDEs
from alea.application.tt_asgfem.tensorsolver.tt_param_pde import ttParamPDEALS
#                                                   # orthogonalization and raveled (vectorized)
from alea.application.tt_asgfem.tensorsolver.tt_util import ttRightOrthogonalize, ravel
#                                                   # adaptive reaction methods
from alea.application.tt_asgfem.tensorsolver.tt_sgfem_util import (evaluate_residual_mean_energy_norm, tt_dofs,
                                                                   tt_add_stochastic_dimension, increase_tt_rank,
                                                                   ttRandomDeterministicRankOne)
from alea.application.tt_asgfem.tensorsolver.tt_als import ttALS
from alea.application.tt_asgfem.tensorsolver.tt_util import reshape, ttNormalize

# endregion
# region useful python packages
import numpy as np                                  # standard numpy library
from numpy.testing import assert_almost_equal       # gets the almost equal operator for numpy arrays
from operator import itemgetter                     # enables items to pass the __getitem__() method
# import argparse                                   # enable argument parsing from command line
from alea.utils.tictoc import TicToc                # time measurement library https://github.com/tsroten/ticktock
from math import isnan                              # get the isnan operator to check values for existing
import logging
import logging.config
import cPickle as Pickle
# endregion
# region System settings
import scipy.sparse as sps
import sys                                          # import system library
sys.getrecursionlimit()                             # prepare recursion limit
sys.setrecursionlimit(10000)                        # increase limit for large M > 100
# endregion

# endregion

# region Function: refine mesh deterministic log normal


def refine_mesh_deterministic_log(eta_global, summed_eta_local, CG, thetaX, V, _pde, _femdegree):
    """
    refine the mesh adaptive
    @param eta_global:
    @param summed_eta_local:
    @param CG:
    @param thetaX:
    @param V:
    @param do_timing:
    @param _pde:
    @param _femdegree:
    @return:
    """
    from operator import itemgetter  # enables items to pass the __getitem__() method
    from dolfin import MeshFunction
    # sort error indicators
    eta_local_ind = [(x, i) for i, x in enumerate(summed_eta_local)]
    eta_local_ind = sorted(eta_local_ind, key=itemgetter(0), reverse=True)

    # setup marking set
    mesh = CG.mesh()
    cell_markers = MeshFunction("bool", mesh, mesh.topology().dim())
    cell_markers.set_all(False)
    marked_eta, cc = 0.0, 0

    for eta_cell in eta_local_ind:
        # break if sufficiently many cells are selected
        if thetaX * eta_global ** 2 <= marked_eta:
            break
        cell_markers[eta_cell[1]] = True
        marked_eta += eta_cell[0] ** 2
        cc += 1
    # print "+++++++ cells marked: %i/%i" % (cc, mesh.num_cells()), "\theta_global", eta_global, "
    # \tmarked_eta", marked_eta

    with TicToc(key="**** project solution ****", active=False, do_print=False):
        F = Function(CG)
        mesh = refine(mesh, cell_markers)
        CG = _pde.function_space(mesh, _femdegree)
        # cores = V.to_list(V)
        cores = V
        r = cores[0].shape[2]
        newcore = np.zeros([1, CG.dim(), r])
        for d in range(r):
            # print "++++++++", CG.dim(), cores[0][0, :, d].shape
            F.vector().set_local(cores[0][0, :, d])
            # print "refinement", d, F.vector().array().shape,CG.dim()
            newF = interpolate(F, CG)
            newcore[0, :, d] = newF.vector().array()
        # update U
        cores[0] = newcore
        V = tt.vector.from_list(cores)
    return V, mesh


# endregion

# region Function: Residual estimator evaluation


def evaluate_residual_estimator(V, D, CG, coeff_field, f, _mesh, gpcp=None, assemble_quad_degree=15, do_timing=True,
                                ncpu=1):
    """
    calculates the error estimator corresponding to the residual
    @param V: TT-Tensor solution object
    @param D:
    @param CG:
    @param coeff_field: coefficient field for stochastic representation
    @param f: rhs
    @param _mesh: finite dimensional mesh
    @param gpcp: general polynomial chaos polynomials
    @param assemble_quad_degree: degree of used quadrature
    @param do_timing:
    @return:
    """
    fp = ''
    try:
        _f = open('logging.conf')
        _f.close()
        fp = 'logging.conf'
    except IOError:
        print("logging not in working directory")
        try:
            _f = open('/home/marschall/WIAS/alea-testing/src/alea/application/bayes/logging.conf')
            _f.close()
            fp = '/home/marschall/WIAS/alea-testing/src/alea/application/bayes/logging.conf'
        except IOError:
            print("No logging file found. -> exit()")
            exit()
    logging.config.fileConfig(fp)
    log = logging.getLogger('test_bayes')           # get new logger object
    summed_eta_local, eta_global = None, 0

    with TicToc(key="**** TT residual estimator ****", active=do_timing, do_print=False):
        eta_res_local, eta_res_global = ttEvaluateResidualEstimator(V, D, CG, coeff_field, f,
                                                                    assemble_quad_degree=assemble_quad_degree,
                                                                    with_volume=True, with_edge=True, ncpu=ncpu)

    # print ">>>>>>>>>>>>>>eta_res_global>>>>>>>>>>>>", eta_res_global, np.sqrt(sum(eta_res_local**2))
    assert_almost_equal(eta_res_global, np.sqrt(sum(eta_res_local**2)))
    if summed_eta_local is None:
        summed_eta_local = eta_res_local
    else:
        summed_eta_local = np.sqrt(summed_eta_local**2 + eta_res_local**2)
    eta_global += eta_res_global
    # print eta_global, np.sqrt(sum(summed_eta_local**2))
    assert_almost_equal(eta_global, np.sqrt(sum(summed_eta_local**2)))

    return eta_global, eta_res_global, eta_res_local, summed_eta_local
# endregion

# region Function: refine mesh deterministic


def refine_mesh_deterministic(eta_global, summed_eta_local, CG, thetaX, V, do_timing, _pde, _femdegree):
    """
    refine the mesh adaptive
    @param eta_global:
    @param summed_eta_local:
    @param CG:
    @param thetaX:
    @param V:
    @param do_timing:
    @param _pde:
    @param _femdegree:
    @return:
    """
    # sort error indicators
    eta_local_ind = [(x, i) for i, x in enumerate(summed_eta_local)]
    eta_local_ind = sorted(eta_local_ind, key=itemgetter(0), reverse=True)

    # setup marking set
    mesh = CG.mesh()
    cell_markers = MeshFunction("bool", mesh, mesh.topology().dim())
    cell_markers.set_all(False)
    marked_eta, cc = 0.0, 0

    for eta_cell in eta_local_ind:
        # break if sufficiently many cells are selected
        if thetaX * eta_global**2 <= marked_eta:
            break
        cell_markers[eta_cell[1]] = True
        marked_eta += eta_cell[0] ** 2
        cc += 1
    # print "+++++++ cells marked: %i/%i" % (cc, mesh.num_cells()), "\theta_global", eta_global, "
    # \tmarked_eta", marked_eta

    with TicToc(key="**** project solution ****", active=do_timing, do_print=False):
        F = Function(CG)
        mesh = refine(mesh, cell_markers)
        CG = _pde.function_space(mesh, _femdegree)
        cores = V.to_list(V)
        r = cores[0].shape[2]
        newcore = np.zeros([1, CG.dim(), r])
        for d in range(r):
            # print "++++++++", CG.dim(), cores[0][0, :, d].shape
            F.vector().set_local(cores[0][0, :, d])
            # print "refinement", d, F.vector().array().shape,CG.dim()
            newF = interpolate(F, CG)
            newcore[0,:,d] = newF.vector().array()
        # update U
        cores[0] = newcore
        V = tt.vector.from_list(cores)
    return V, mesh

# endregion

# region Function: stochastic Galerkin with ALS for lognormal coefficient


def log_normal_sgfem_als(maxrank=1500,              # type: int
                         n_coeff=10,                # type: int
                         femdegree=1,               # type: int
                         gpcdegree=3,               # type: int
                         decayexp=2,                # type: float
                         gamma=0.9,                 # type: float
                         domain="square",           # type: str
                         mesh_refine=1,             # type: int
                         mesh_dofs=-1,              # type: int
                         sol_rank=5,                # type: int
                         freq_skip=0,               # type: int
                         freq_scale=1.0,            # type: float
                         scale=1.0,                 # type: float
                         theta=0.5,                 # type: float
                         rho=1.0,                   # type: float
                         hermite_degree=8,          # type: int
                         als_iterations=1000,       # type: int
                         convergence=1e-12,         # type: float
                         coeffield_type="monomial",  # type: str
                         _print=False,              # type: bool
                         field_mean=1.0,            # type: float
                         coef_acc=1e-10,            # type: float
                         iterations=5,              # type: int
                         theta_x=0.5,               # type: float
                         theta_y=0.5,               # type: float
                         start_rank=2,              # type: int
                         n_coef_coef=15,            # type: int
                         max_hdegs_coef=15,         # type: int
                         eta_zeta_weight=0.1,       # type: float
                         resnorm_zeta_weight=1.0,   # type: float
                         coef_mesh_dofs=3000000,    # type: int
                         coef_quad_degree=2,        # type: int
                         rank_always=True,          # type: bool
                         max_dofs=1e6,              # type: float
                         new_hdeg=4,                # type: int
                         coef_mesh_cache=None,      # type: object
                         cont_coef_cache=None       # type: object
                         ):
    """
    :param maxrank: maximal rank used in coefficient field approximation
    :param n_coeff: number of initial dimension of the solution tensor
    :param femdegree: finite element order
    :param gpcdegree: general polynomial chaos degree (not used anymore)
    :param decayexp: decay of the continuous affine coefficient field
    :param gamma: scaling factor to ensure convergence of the affine coefficient field
    :param amptype: type of coefficient field
    :param domain: domain of physical space. Either square or L-shape
    :param mesh_refine: number of mesh refinements prior to adaptive process (none: -1)
    :param mesh_dofs: number of desired dofs in physical mesh prior to adaptive process
    :param sol_rank: solution rank of the initial tt-solution
    :param freq_skip: frequencies skipped in the affine coefficient field
    :param freq_scale: scaling of each frequency in the affine coefficient field
    :param scale: scaling of the whole affine coefficient field including the mean value
    :param theta: Bochner space scaling of the gaussian measure
    :param rho: Bochner space scaling to concentrate the gaussian measure again
    :param hermite_degree: polynomial degree of the initial tt-solution
    :param als_iterations: number of iterations in the alternating-least-square algorithm (n.o. forward-backward sweeps)
    :param convergence: desired tolerance to reach in the ALS scheme (difference of two iteration steps)
    :param coeffield_type: type of the coefficient field
    :param _print: optional printing flag for the TicToc Timer
    :param field_mean: mean value of the affine coefficient field
    :param coef_acc: desired accuracy to reach in the approximation of the exponential field
    :param iterations: maximal number of adaptive iteration steps
    :param theta_x: D�rfler marking value for the physical space refinement
    :param theta_y: D�rfler marking value for the stochastic space refinement
    :param start_rank: initial rank of the tt-solution
    :param n_coef_coef: number of coefficients (dimensions +1 ) in tt representation of the coefficient field
    :param max_hdegs_coef: maximal number of hermite degrees in the coefficient field approximation
    :param eta_zeta_weight: weight to balance the physical and stochastic estimators
    :param resnorm_zeta_weight: weight to balance the residual and stochastic estimators
    :param coef_mesh_dofs: number of dofs to use in the physical grid approximation of the coefficient field
    :param coef_quad_degree: quadrature degree to use when solving the quadruple integrals in the coefficient field
    :param rank_always: flag to refine the tt-rank in every iteration step
    :param max_dofs: maximal number of degrees of freedom to reach in the adaptive process (ttdofs)
    :param new_hdeg: number of update dimensions if the stochastic space is enriched with a new dimension
    :param coef_mesh_cache: cache object used to store the mesh of the coefficient field
    :param cont_coef_cache: cache object used to store the components of the continuous coefficient field
    :return: dictionary of all needed results
    """

    amptype = "decay-algebraic"

    # region Switches to show additional info
    #                                               # hash to read file from if found
    # read_from_hash = "{}P{}EZW{}RZW{}".format(get_current_time_string(), femdegree, eta_zeta_weight,
    #                                          resnorm_zeta_weight)
    # read_from_hash = "20180119132807P{}EZW{}RZW{}".format(femdegree, eta_zeta_weight, resnorm_zeta_weight)
    read_from_hash = None

    plot_coef_samples = 0                           # plot coef samples to file
    plot_coef_terminate = False                     # termination switch for coef sample plotting
    print_min_of_coef_samples = 0                   # if < 1 nothing is shown. If x>0, the minimum value of x coef
    #                                               # samples is printed to console
    print_min_of_coef_samples_terminate = False
    plot_mesh = True                                # switch to plot the mesh after every iteration step
    plot_solution_only_once = False                 # switch to plot the solution only once and exit() afterwards
    quant_factor = 1                                # scaling factor of the stochastic samples (quantile factor)

    residual_samples = 10                           # number of samples to use in sampling the residuum (debug)
    #                                               # max value of all sample amounts to create a sufficient sample list
    n_samples = max(print_min_of_coef_samples, plot_coef_samples, residual_samples)

    desired_overall_error = 0.0001                  # estimator error tolerance to reach -> in docstring later
    refine_adaptive = True                          # hard switch to set refinement to uniform if desired (not intended)
    use_preconditioner = True                       # hard switch to turn of the preconditioner (not intended)
    # endregion

    # region Setup domain and mesh
    # ###############################################
    # A: setup domain and meshes
    # ###############################################

    if mesh_dofs > 0:                               # mesh dofs are defined
        mesh_refine = 0                             # there are no mesh refinements
    #                                               # call sampleDomain and get boundaries, mesh and dimension

    mesh, boundaries, dim = SampleDomain.setupDomain(domain, initial_mesh_N=2)
    #                                               # refine mesh according to input
    mesh = SampleProblem.setupMesh(mesh, num_refine=mesh_refine)
    #                                               # further refinement if you want to have a specific amount of dofs
    from dolfin import FunctionSpace                # WTF here again? Why doesn't python detect the import in the head
    print("soll mesh_dofs={} haben dofs={}".format(mesh_dofs, FunctionSpace(mesh, 'CG', femdegree).dim()))
    while mesh_dofs > 0 and FunctionSpace(mesh, 'CG', femdegree).dim() < mesh_dofs:
        mesh = refine(mesh)                         # uniformly refine mesh

    from dolfin import File
    File("{}-{}-mesh.xml".format(read_from_hash, -1)) << mesh
    # logger.info("initial mesh has %i dofs with p%i FEM" % (FunctionSpace(mesh, 'CG', femdegree).dim(), femdegree))
    #                                               # obtain all coordinates of degrees of freedom
    mp = np.array(FunctionSpace(mesh, 'DG', femdegree-1).tabulate_dof_coordinates())
    mp = mp.reshape((-1, mesh.geometry().dim()))
    # endregion

    # region Construct coefficient
    # ##############################################
    # B construct coefficient
    # ##############################################
    # region Construct affine Field for exponent and PDE
    #                                               # create affine coefficient field as logarithm of the used field
    af = AffineField(coeffield_type, amptype=amptype, decayexp=decayexp, gamma=gamma, freqscale=freq_scale,
                     freqskip=freq_skip, scale=scale, coef_mean=field_mean)
    #                                               # coefficient mean is 0 anyway. therefore do not set it later
    #                                               # prepare the PDE problem
    pde, _, _, _, _, _ = SampleProblem.setupPDE(2, domain, 0, boundaries, af.coeff_field)
    # endregion
    # region define hermite degrees, tensor ranks and evaluate field for old version of coefficient tensor
    hdegs = n_coeff * [hermite_degree]              # set hermite degree list of solution
    hdegs_coef = n_coef_coef * [max_hdegs_coef]     # set hermite degree list of coefficient

    n_coef_coef = len(hdegs_coef)                   # define number of dimensions in coefficient field

    ranks = [1] + n_coeff * [start_rank] + [1]      # create rank vector (1, STARTRANK, STARTRANK, ..., 1)
    #                                               # create rank vector for coefficient (1, MAXRANK, MAXRANK, ..., 1)
    ranks_coef = [1] + len(hdegs_coef) * [maxrank] + [1]
    B = af.evaluate_basis(mp, n_coef_coef)          # evaluate physical basis functions at cell midpoints (old version)
    # endregion

    # region Calculate scaling parameter bmax
    # TODO: better bxmax for other amp types
    if coeffield_type == 'EF-square-cos':           # using a decay that defines a convergent series in the affine part
        from scipy.special import zeta              # get zeta function
        #                                           # get decay start value from affine field
        start = SampleProblem.get_decay_start(decayexp, gamma)
        amp = gamma / zeta(decayexp, start)         # get corresponding amplification
        #                                           # \|a_m\|_\infty = scale*gamma/zeta * (m+start)^{-sigma}
        bxmax_m = [scale * amp * (i + start) ** (-decayexp) for i in range(B.shape[1]-1)]
        bxmax = np.array(bxmax_m)                   # use np.arrays
    elif coeffield_type == 'EF-square-cos-algebraic':
        #                                           # special case using algebraic decay
        # start = SampleProblem.get_decay_start(decayexp, gamma)
        amp = gamma
        start = 1
        bxmax_m = [scale * amp * (i + start) ** (-decayexp) for i in range(B.shape[1]-1)]
        # print bxmax_M
        bxmax = np.array(bxmax_m)
    else:
        bxmax = get_coeff_upper_bound(B)

    print("bxmax: {}".format(bxmax))
    print("old bxmax: {}".format(get_coeff_upper_bound(B)))
    # endregion

    # region Create reduced basis coefficient tensor
    ###############################################
    # Semi Discretized Method
    ###############################################
    #                                               # create semi discrete coefficient with reduced basis approach
    with TicToc(sec_key="Create coefficient", key="**** CreateSemiDiscreteCoefficient ****", active=True,
                do_print=True):
        if cont_coef_cache is None:
            cont_coeff_cores = tt_cont_coeff(af, mesh, n_coef_coef, ranks_coef, hdegs_coef, bxmax, theta=theta, rho=rho,
                                             acc=coef_acc, mesh_dofs=coef_mesh_dofs, quad_degree=coef_quad_degree,
                                             coef_mesh_cache=coef_mesh_cache, domain=domain, scale=scale)
            import cPickle as Pickle
            outfile = open('{}-{}-{}-cont_coef_cores.dat'.format(domain, scale, field_mean), 'wb')
            Pickle.dump(cont_coeff_cores, outfile, Pickle.HIGHEST_PROTOCOL)
            outfile.close()
        else:
            # cont_coeff_cores = tt.vector.to_list(tt.vector.round(tt.vector.from_list(cont_coef_cache), 1e-16))
            cont_coeff_cores = cont_coef_cache
            ranks_coef = [1] + list(tt.vector.from_list(cont_coeff_cores).r)
            hdegs_coef = tt.vector.from_list(cont_coeff_cores).n
    # endregion
    # endregion

    # region Create Samples
    sample_y_list = []                              # create empty list to  store samples
    for lia in range(n_samples):                    # store samples of the affine field in list
        sample_y_list.append(af.sample_rvs(len(hdegs_coef)))

    # endregion

    # region define sigma as scaling parameter
    sigma = np.exp(-theta*rho*bxmax)
    # endregion

    sol_list = []                                   # init return value as list of the solution dicts

    rank = start_rank                               # set current rank to the start rank value
    base_change_cache = None                        # init base change matrix/vector cache

    # region optional: create some information about the coefficient field
    # ######## Sampling of the coefficient field
    if print_min_of_coef_samples or plot_coef_samples:
        # region Refine solution mesh to coef mesh resolution
        with TicToc(key="  1. Sample coefficient field: create Mesh", active=True, do_print=True):
            if coef_mesh_cache is None:
                mesh2 = mesh  # refine(mesh)
                while FunctionSpace(mesh2, 'DG', 0).dim() < coef_mesh_dofs:
                    mesh2 = refine(mesh2)  # uniformly refine mesh
                sample_dofs = FunctionSpace(mesh2, 'DG', 0).dim()
            else:
                mesh2 = coef_mesh_cache
                sample_dofs = FunctionSpace(mesh2, 'DG', 0).dim()
        # endregion
        # region obtain cell midpoints
        with TicToc(key="  2. Get cell midpoints", active=True, do_print=True):

            mp = np.zeros((mesh2.num_cells(), 2))
            for ci, c in enumerate(cells(mesh2)):
                mpi = c.midpoint()
                mp[ci] = mpi.x(), mpi.y()
        # endregion
        n_samples = max(print_min_of_coef_samples, plot_coef_samples)
        minimal_coef_value = np.inf
        for lia in range(n_samples):
            # region Sample true and approximated coefficient field
            y = list(sample_y_list[lia])
            V1_ = af(mp, y)
            V1_ = np.exp(V1_)
            print("sample_error with ranks={}, hdegs={} on mesh with {} DG0 dofs".format(ranks_coef, hdegs_coef,
                                                                                         sample_dofs))
            # from alea.application.tt_asgfem.tensorsolver.tt_lognormal import sample_cont_coeff_2
            with TicToc(key="  3. Sample cont core", active=True, do_print=True):
                V3_ = sample_cont_coeff(cont_coeff_cores, af, mp, y, ranks_coef, hdegs_coef, bxmax, theta, rho,
                                        dg_mesh=mesh2
                                        )
            print("error simple: {}".format(np.linalg.norm(V1_ - V3_, ord=2)))
            # endregion
            # region Prepare Fenics utilities
            with TicToc(key="  4. create Function Spaces", active=True, do_print=True):
                from dolfin import TrialFunction, TestFunction, parameters, inner, dx, assemble, as_backend_type
                fs = FunctionSpace(mesh2, "DG", 0)
                fs_cg = FunctionSpace(mesh2, "CG", 1)

                _u = TrialFunction(fs_cg)
                _v = TestFunction(fs_cg)
                parameters['linear_algebra_backend'] = 'Eigen'
            with TicToc(key="  5. Assemble Mass matrix", active=True, do_print=True):
                #                                       # create bilinear form of mass matrix
                BF = inner(_u, _v) * dx(mesh2)
                M = assemble(BF)
                M = as_backend_type(M)
                M = M.array()
            with TicToc(key="  6. interpolate to CG mesh", active=True, do_print=True):
                fun_v3 = Function(fs)
                fun_v1 = Function(fs)
                fun_v3.vector()[:] = V3_
                fun_v1.vector()[:] = V1_
                fun_v3 = interpolate(fun_v3, fs_cg)
                fun_v1 = interpolate(fun_v1, fs_cg)
                plot_fun_v3 = fun_v3.compute_vertex_values(mesh2)
                plot_fun_v1 = fun_v1.compute_vertex_values(mesh2)
                plot_fun_diff = Function(fs_cg)
                plot_fun_diff.vector()[:] = np.array(fun_v3.vector()[:]) - np.array(fun_v1.vector()[:])
                plot_fun_diff = plot_fun_diff.compute_vertex_values(mesh2)
            # endregion
            # region Plot coefficient field samples to file
            if plot_coef_samples:
                import matplotlib.pyplot as plt
                from dolfin.common.plotting import mesh2triang
                ax = plt.gca(projection='3d')
                ax.set_aspect('equal')
                ax.plot_trisurf(mesh2triang(mesh2), plot_fun_v3, cmap=plt.cm.CMRmap)
                if domain == "square":
                    plt.savefig("mean{}_quantile{}_S_{}_scal{}_de{}_coef.png".format(field_mean, quant_factor,
                                                                                        lia, scale,
                                                                                        decayexp),
                                format='png')
                elif domain == "lshape":
                    plt.savefig("mean{}_qauntile{}_L_{}_scal{}_de{}_coef.png".format(field_mean, quant_factor,
                                                                                        lia, scale,
                                                                                        decayexp),
                                format='png')
                plt.clf()
                ax = plt.gca(projection='3d')
                ax.set_aspect('equal')
                ax.plot_trisurf(mesh2triang(mesh2), plot_fun_diff, cmap=plt.cm.CMRmap)
                if domain == "square":
                    plt.savefig("mean{}_quantile{}_S_{}_scal{}_de{}_coef_diff.png".format(field_mean,
                                                                                             quant_factor, lia,
                                                                                             scale, decayexp),
                                format='png')
                elif domain == "lshape":
                    plt.savefig("mean{}_quantile{}_L_{}_scal{}_de{}_coef_diff.png".format(field_mean,
                                                                                             quant_factor, lia,
                                                                                             scale, decayexp),
                                format='png')
                plt.clf()
                ax = plt.gca(projection='3d')
                ax.set_aspect('equal')
                ax.plot_trisurf(mesh2triang(mesh2), plot_fun_v1, cmap=plt.cm.CMRmap)
                if domain == "square":
                    plt.savefig("True_mean{}_quantile{}_S_{}_scal{}_de{}_coef.png".format(field_mean,
                                                                                             quant_factor,
                                                                                             lia, scale, decayexp),
                                format='png')
                elif domain == "lshape":
                    plt.savefig("True_mean{}_qauntile{}_L_{}_scal{}_de{}_coef.png".format(field_mean,
                                                                                          quant_factor,
                                                                                          lia, scale, decayexp),
                                format='png')
                plt.clf()

                # ph = PlotHelper()
                # ph["coef it {}".format(step+1)].plot(fun_v3)
                diff = np.array(fun_v3.vector()[:]) - np.array(fun_v1.vector()[:])
                errl2 = np.sqrt(np.dot(diff.T, np.dot(M, diff)))
                normalization = np.sqrt(np.dot(fun_v1.vector()[:], np.dot(M, fun_v1.vector()[:])))
                print("semi-discrete error: {}".format(errl2))
                print("semi-discrete relative error: {}".format(errl2 * normalization**(-1)))
            # endregion
            # region Print minimal value of coefficient field to console
            if print_min_of_coef_samples:
                curr_min_value = np.min(plot_fun_v3)
                print("minimal value of coefficient sample {}: {}".format(lia, curr_min_value))
                if curr_min_value < minimal_coef_value:
                    minimal_coef_value = curr_min_value
                    # endregion
        # region early termination switches
        if plot_coef_terminate is True:
            exit()
        if print_min_of_coef_samples_terminate is True:
            exit()
        # endregion

    # ####### End sampling of the coefficient field
    # endregion
    step_buffer = 0
    for step in range(iterations):

        # region NOT FOR FINAL: read data from binary file for every iteration step
        if step == 0 and read_from_hash is not None:
            try:
                from dolfin import Mesh
                import cPickle as Pickle
                f = open(read_from_hash, 'rb')
                sol_list = Pickle.load(f)
                f.close()
                step_buffer = sol_list[-1]['step']
                mesh = Mesh("{}-{}-mesh.xml".format(read_from_hash, step_buffer + step))
                iterations = sol_list[-1]['iterations']
                femdegree = sol_list[-1]['femdegree']
                cont_coeff_cores = sol_list[-1]['cont_coef_cores']
                coeffield_type = sol_list[-1]['coeffield_type']
                amptype = sol_list[-1]['amptype']
                decayexp = sol_list[-1]['decayexp']
                gamma = sol_list[-1]['gamma']
                freq_scale = sol_list[-1]['freq_scale']
                freq_skip = sol_list[-1]['freq_skip']
                scale = sol_list[-1]['scale']
                field_mean = sol_list[-1]['field_mean']
                af = AffineField(coeffield_type, amptype=amptype, decayexp=decayexp, gamma=gamma, freqscale=freq_scale,
                                 freqskip=freq_skip, scale=scale, coef_mean=field_mean)
                ranks_coef = sol_list[-1]['ranks_coef']
                hdegs_coef = sol_list[-1]['hdegs_coef']
                bxmax = sol_list[-1]['bxmax']
                theta = sol_list[-1]['theta']
                rho = sol_list[-1]['rho']
                hdegs = sol_list[-1]['hdegs']
                n_coef_coef = sol_list[-1]['n_coef_coef']
                rank = sol_list[-1]['rank']
                start_rank = sol_list[-1]['start_rank']
                W = sol_list[-1]['W']
                use_preconditioner = sol_list[-1]['preconditioner']
                convergence = sol_list[-1]['convergence']
                als_iterations = sol_list[-1]['als_iterations']
                refine_adaptive = sol_list[-1]['refine_adaptive']
                max_dofs = sol_list[-1]['max_dofs']
                base_change_cache = sol_list[-1]['base_change_cache']
                maxrank = sol_list[-1]['maxrank']
                theta_x = sol_list[-1]['theta_x']
                theta_y = sol_list[-1]['theta_y']
                resnorm_zeta_weight = sol_list[-1]['resnorm_zeta_weight']
                eta_zeta_weight = sol_list[-1]['eta_zeta_weight']
                rank_always = sol_list[-1]['rank_always']
                _, boundaries, _ = SampleDomain.setupDomain(domain, initial_mesh_N=2)
                pde, _, _, _, _, _ = SampleProblem.setupPDE(2, domain, 0, boundaries, af.coeff_field)
                new_hdeg = sol_list[-1]['new_hdeg']
                sol_rank = sol_list[-1]['sol_rank']
                dofs = tt_dofs(tt.vector.from_list(W))
                if dofs >= max_dofs:
                    print("dofs reached: {} >= {}".format(dofs, max_dofs))
                    break
                if step_buffer >= iterations-1:
                    print("iterations reached {} >= {}".format(step_buffer, iterations))
                    break
            except Exception as e:
                import sys
                print("did not found file {}: {} ".format(read_from_hash, sys.exc_info()))
        # endregion

        print("="*10)
        print("iteration step {} / {}".format(step_buffer + step+1, iterations))

        # region Get cell midpoints to discretize semi-discrete coef tensor
        with TicToc(sec_key="ASGFEM-solve", key="1. Get cell midpoints and dofs", active=True, do_print=_print):
            cg_space = FunctionSpace(mesh, 'CG', femdegree)

            # region obtain dofs of cg space
            #                                       # obtain all coordinates of degrees of freedom
            mp = np.array(cg_space.tabulate_dof_coordinates())
            #                                       # reshape dof vector according to physical dimension
            mp = mp.reshape((-1, mesh.geometry().dim()))
            mesh_midpoint = mp
            # endregion

        # endregion

        # region Create fully discrete coefficient tensor

        with TicToc(sec_key="ASGFEM-solve", key="2. create fully discrete first core", active=True, do_print=False):
            #                                       # create discrete first core of coefficient field
            core0 = fully_disc_first_core(cont_coeff_cores, af, mesh_midpoint, ranks_coef, hdegs_coef, bxmax,
                                          theta=theta, rho=rho, cg_mesh=mesh, femdegree=femdegree)
            #                                       # copy continuous cores to use later and not change the reference
            coeff_cores = deepcopy(cont_coeff_cores)
            coeff_cores.insert(0, core0)            # add discrete first core to obtain fully discrete coefficient field
            #                                       # create order 2 core out of first component to use later
            first_comp = np.reshape(core0, [len(mesh_midpoint), ranks_coef[1]])
        # endregion
        # region Create discrete operators

        with TicToc(sec_key="ASGFEM-solve", key="3. Create operators", active=True, do_print=_print):
            #                                           # compute FE matrices for piecewise constant coefficients
            with TicToc(sec_key="ASGFEM-solve", key="  3.1. compute FE matrices", active=True, do_print=_print):
                A0, bc_dofs, BB, CG = compute_FE_matrices(first_comp, mesh, degree=femdegree, n_cpu=40)
            # print len(A0), A0[0].shape
            n_coeff = len(hdegs)
            if n_coeff > n_coef_coef:
                print("Dimension of solution exhibits dimension of coefficient -> exit()")
                return sol_list

            with TicToc(sec_key="ASGFEM-solve", key="  3.2 generate TT operator", active=True, do_print=False):
                opcores = generate_operator_tt(cont_coeff_cores, ranks_coef, hdegs, len(hdegs))
            D = A0[0].shape[0]                          # get size of first matrix
            opcores[0] = []                             # re_init first operator core (it was None anyway)
            for r0, A in enumerate(A0):
                opcores[0].append(A)
                # print "A0", A.toarray()
                # print "midpoints", first_comp[:,r0]
                # print "eigsA0", np.linalg.eig(A.toarray())

            # generate lognormal operator tensor

            with TicToc(sec_key="ASGFEM-solve", key="  3.3. Create sparse TT operator", active=True, do_print=False):
                A = ttsmat.smatrix(opcores)

                bcopcores = (len(hdegs)+1)*[[]]
                # bcdense = np.zeros([D, D])
                # bcdense[bc_dofs, bc_dofs] = 1
                bcsparse_direct = sps.csr_matrix((D, D))
                bcsparse_direct[bc_dofs, bc_dofs] = 1
                bcopcores[0] = []
                bcopcores[0].append(bcsparse_direct)
                for i in range(n_coeff):
                    bcopcores[i+1] = reshape(np.eye(opcores[i+1].shape[1], opcores[i+1].shape[2]),
                                             [1, opcores[i+1].shape[1], opcores[i+1].shape[2], 1])
                BCOP = ttsmat.smatrix(bcopcores)

                # Aop = A.full()
                # Aop = reshape(Aop,[A.n[0],(gpcdegree+1),(gpcdegree+1),A.n[0],(gpcdegree+1),(gpcdegree+1)])
                A += BCOP
                A = A.round(1e-16)                  # ! important: do not cut ranks here. Just orthogonalize cores[1:]
            # print "A", A
            # pass those to robert !!!!!!!!!!!!!!! A[0][:, :, k] for k in range(A[0].shape[2]
        # endregion

        # region Create rhs
        BB = reshape(BB, [1, A.n[0], 1])
        F = [BB] + len(hdegs) * [[]]
        for i in range(len(hdegs)):
            F[i+1] = reshape(np.eye(A.n[i+1], 1), [1, A.n[i+1], 1])
        F = tt.vector.from_list(F)
        # print "F", F
        # endregion
        # region Create starting vector for solver
        # -Start-Vector------------------------------------------
        if rank == 1:
            W = ttRandomDeterministicRankOne(A.n)     # create random deterministic rank one tensor
        else:
            if step > 0:
                # print("rank of W : {}".format(max(tt.vector.from_list(W).r)))
                W = tt.vector.from_list(W)
                # rank100ten = tt.rand(A.n, A.d, 100)
                # W += rank100ten
                # W = tt.rand(A.n, A.d, start_rank)
                W *= (1 / W.norm())  # normalize
                W = tt.vector.to_list(W)

                print("rank of W : {}".format(max(tt.vector.from_list(W).r)))
            else:
                if refine_adaptive is False:
                    W = tt.rand(A.n, A.d, start_rank)       # create tensor with given rank and size
                    W *= (1 / W.norm())                 # normalize
                else:
                    W = tt.rand(A.n, A.d, rank)  # create tensor with given rank and size
                    # W = tt.rand(A.n, A.d, start_rank)  # create tensor with given rank and size
                    W *= (1 / W.norm())  # normalize

        # with TicToc(key="**** ttRightOrthogonalize ****", active=True, do_print=True):
        #     W = ttRightOrthogonalize(W)             # orthogonalize the solution Tensor from the right
        # endregion

        # region Create preconditioner for solver
        # P = compute_FE_matrix_coeff(B,mesh,exp_a=True)
        with TicToc(sec_key="ASGFEM-solve", key="4. create preconditioner", active=True, do_print=False):
            # core0 = fully_disc_first_core(cont_coeff_cores, af, fem_dofs, ranks_coef, hdegs_coef, bxmax, theta=theta,
            #                               rho=rho)

            # precond_coeff_cores = deepcopy(cont_coeff_cores)
            # precond_coeff_cores.insert(0, core0)
            if use_preconditioner:
                Pvec = extract_mean(tt.vector.from_list(coeff_cores))
                P = compute_FE_matrix_coeff(Pvec, mesh, exp_a=True, degree=femdegree)
            else:
                P = np.eye(A.n[0], A.n[0])
            # print("Preconditioner shape: {}".format(P.shape))
        # endregion
        # region Solver (ALS)
        converged = [False]
        if isinstance(W, list):
            W = tt.vector.from_list(W)
        with TicToc(sec_key="ALS-solver", key="**** ALS conv:{}, iterations:{}, step:{}, "
                                              "init rank:{}****".format(convergence, als_iterations, step+1, max(W.r)),
                    active=True, do_print=False):
            W, als_iter = ttALS(A, F, W, conv=convergence, maxit=als_iterations, P=P, converged_by_ref=converged)

        if isinstance(W, list):
            # print tt.vector.from_list(W)
            dofs = tt_dofs(tt.vector.from_list(W))
            retval = tt.vector.from_list(W)
            print("solution has size: {}".format(tt.vector.from_list(W)))
        else:
            dofs = tt_dofs(W)
            retval = W
            print("solution has size: {}".format(W))
        # endregion

        # region optional: solution sampling
            if print_min_of_coef_samples or plot_coef_samples:
                for lia, y in enumerate(sample_y_list):
                    # region Sample solution
                    with TicToc(key="  1. sample solution", active=True, do_print=True):
                        V3_ = sample_lognormal_tt(retval, y, bxmax, theta=theta, rho=rho)
                    # endregion
                    # region Prepare Fenics utilities
                    with TicToc(key="  2. create function space and function", active=True, do_print=True):
                        fs_cg = FunctionSpace(mesh, "CG", femdegree)
                        sol_fun = Function(fs_cg)
                        sol_fun.vector()[:] = V3_
                        plot_sol_fun = sol_fun.compute_vertex_values(mesh)
                    # endregion
                    # region Plot solution samples to file
                    if plot_coef_samples:
                        with TicToc(key="  3. write solution to file", active=True, do_print=True):
                            import matplotlib.pyplot as plt
                            from dolfin.common.plotting import mesh2triang
                            ax = plt.gca(projection='3d')
                            ax.set_aspect('equal')
                            ax.plot_trisurf(mesh2triang(mesh), plot_sol_fun, cmap=plt.cm.CMRmap)
                            if domain == "square":
                                plt.savefig("mean{}_quantile{}_S_{}_scal{}_de{}_sol_{}.png".format(field_mean,
                                                                                                      quant_factor, lia,
                                                                                                      scale, decayexp,
                                                                                                      step+1),
                                            format='png')
                            elif domain == "lshape":
                                plt.savefig("mean{}_quantile{}_L_{}_scal{}_de{}_sol_{}.png".format(field_mean,
                                                                                                      quant_factor, lia,
                                                                                                      scale, decayexp,
                                                                                                           step+1),
                                            format='png')
                            plt.clf()
                    # endregion
                if plot_solution_only_once is True:
                    exit()
        # endregion

        # region refinement procedure. adaptive and uniform

        # region Update solution return value

        SOL = {"V": W, "coeff_field": None, "cont_coeff_cores": cont_coeff_cores, "coeff_cores": coeff_cores,
               "rhs": F, "DOFS": dofs, "nr_active_ydim": n_coeff, "sigma": sigma,
               "gpcps": [hdegs], "refinements": 0, "femdegree": femdegree, "domain": domain, "decayexp": decayexp,
               "gamma": gamma, "mesh_midpoint": mesh_midpoint, "bmax": bxmax}
        sol_list.append(SOL)
        if read_from_hash is None:
            sol_list[-1]['mesh'] = mesh
            sol_list[-1]['CG'] = FunctionSpace(mesh, 'CG', femdegree)
        # endregion
        if refine_adaptive:

            if step + 1 < iterations and dofs < max_dofs:
                # region create residual object
                from alea.application.tt_asgfem.tensorsolver.tt_residual import TTResidual
                residual = TTResidual(deepcopy(cont_coeff_cores), W, F, bxmax, femdegree, lognormal=True, mesh=mesh,
                                      af_type=coeffield_type, amp_type=amptype, decay_exp_rate=decayexp,
                                      sgfem_gamma=gamma, freq_scale=freq_scale, freq_skip=freq_skip, scale=scale,
                                      theta=theta, rho=rho, cache=base_change_cache)

                # endregion
                # region residual estimator
                with TicToc(sec_key="RESIDUAL: resnorm", key="**** evaluate_residual_norm ****", active=True,
                            do_print=_print):
                    #                      				# calculate residual mean in energy norm
                    resnorm = residual.evaluate_resnorm(A, F, theta, rho, bxmax)
                    # approx_residual = ttNormalize(A.matvec(W) - F)
                    # resnorm = approx_residual.norm()
                    print("residual of step {} with rank {} = {}:".format(step+1, max(ranks), resnorm))
                    # if there is no residual or the ranks are smaller than initially given, set residual norm to 0
                    if start_rank >= maxrank or isnan(resnorm):
                        resnorm = 0
                # endregion
                # region deterministic error estimator
                if theta_x > 0:
                    with TicToc(sec_key="det-error-estimator", key="**** TT deterministic residual estimator ****",
                                active=True, do_print=False):
                        eta_res_local, eta_res_global, rhs_global,\
                            zero_global, sq_global, \
                            jump_global = residual.eval_deterministic_error_estimator(af, bmax=bxmax, theta=theta,
                                                                                      rho=rho, femdegree=femdegree)
                    #print("eta_global={}".format(eta_res_global))
                    #print("rhs_global={}".format(rhs_global))
                    #print("zero_global={}".format(zero_global))
                    #print("sq_global={}".format(sq_global))
                    #print("jump_global={}".format(jump_global))

                    summed_eta_local, eta_global = None, 0
                    assert_almost_equal(eta_res_global, np.sqrt(sum(eta_res_local ** 2)))
                    if summed_eta_local is None:
                        summed_eta_local = eta_res_local
                    else:
                        summed_eta_local = np.sqrt(summed_eta_local ** 2 + eta_res_local ** 2)
                    eta_global += eta_res_global
                    # print eta_global, np.sqrt(sum(summed_eta_local**2))
                    assert_almost_equal(eta_global, np.sqrt(sum(summed_eta_local ** 2)))
                else:
                    eta_global = 0
                    rhs_global = 0
                    zero_global = 0
                    sq_global = 0
                    jump_global = 0
                # endregion
                # region stochastic error estimator
                if theta_y > 0:
                    with TicToc(sec_key="sto-error-estimator", key="**** TT stochastic residual estimator ****",
                                active=True, do_print=False):
                        zeta_res_local, zeta_res_global, \
                            zeta_tail_list = residual.eval_stochastic_error_estimator(af, bmax=bxmax, theta=theta,
                                                                                      rho=rho,
                                                                                      femdegree=femdegree)

                    zeta_res_local.update(zeta_tail_list)   # add all evaluated tail indicators to indicator set
                    #   		                        # calculate global zeta value
                    zeta_global_sum = np.sqrt(sum([z ** 2 for z in zeta_res_local.values()]))
                    zeta_global = zeta_res_global
                    # print zeta_res_local
                    # print("zeta_res_local={}".format(zeta_res_local))
                    print("zeta_sum = {} <= {} zeta_global".format(zeta_global_sum, zeta_global))
                    # print("zeta_tail_list= {}".format(zeta_tail_list))
                else:
                    zeta_global = 0
                # endregion

                # region decide for refinement step
                rank_done = False
                if zeta_global >= resnorm_zeta_weight * resnorm or rank_always or max(ranks) >= sol_rank or rank_done:
                    refine_deterministic = eta_zeta_weight * eta_global > zeta_global
                    refine_stochastic = not refine_deterministic
                    if refine_deterministic or refine_stochastic:
                        rank_done = True
                if zeta_global >= resnorm_zeta_weight * resnorm or rank_always or max(ranks) >= sol_rank or rank_done:
                    refine_deterministic = eta_zeta_weight * eta_global > zeta_global
                    refine_stochastic = not refine_deterministic
                    if refine_deterministic or refine_stochastic:
                        rank_done = True
                elif eta_zeta_weight * eta_global >= resnorm_zeta_weight * resnorm or rank_always \
                        or max(ranks) >= sol_rank or rank_done:
                    refine_deterministic = True
                    refine_stochastic = False
                    if refine_deterministic or refine_stochastic:
                        rank_done = True
                else:  # no refinement
                    refine_deterministic = False
                    refine_stochastic = False
                    rank_done = False
                # if last_refinement_sto is True and refine_stochastic is True \
                #         and last_refinement_sto_estimator <= zeta_global:
                #         refine_stochastic = False
                #         refine_deterministic = True
                #         print("refine physic instead of stochastic, since zeta got worse.")
                # endregion
                # region refine mesh, stochastic index set or solution rank
                #                                   # deterministic adaptive refinement
                if theta_x > 0 and refine_deterministic:
                    fs = FunctionSpace(mesh, 'CG', femdegree)
                    residual.solution, mesh = refine_mesh_deterministic_log(eta_global, summed_eta_local, fs, theta_x,
                                                                            residual.solution, pde, femdegree)
                    print("new mesh dofs: {}".format(FunctionSpace(mesh, 'CG', femdegree).dim()))
                    last_refinement_sto = False
                #                                   # stochastic adaptive refinement
                if theta_y > 0 and refine_stochastic:
                    #                               # use only zeta sum, since the global zeta can not be reached
                    new_dim, marked_zeta, not_marked_zeta_ym = ttMark_y(zeta_res_local, zeta_tail_list, theta_y,
                                                                        zeta_global_sum, False)
                    # print(new_dim)
                    # print(marked_zeta)
                    # print("prev solution: {}".format(tt.vector.from_list(residual.solution)))
                    residual.solution, hdegs, M = tt_add_stochastic_dimension(tt.vector.from_list(residual.solution),
                                                                              new_dim, hdegs, start_rank,
                                                                              new_gpcd=new_hdeg)
                    last_refinement_sto = True
                    last_refinement_sto_estimator = zeta_global
                    print("new hdegs:{}".format(hdegs))
                    # print("post solution: {}".format(residual.solution))
                if isinstance(residual.solution, list):
                    residual.solution = tt.vector.from_list(residual.solution)
                ranks = residual.solution.r
                if not rank_done:
                    if max(ranks) < sol_rank and not refine_deterministic and not refine_stochastic:
                        residual.solution, rank = increase_tt_rank(residual.solution, None, None, None, acc=1e-18)
                        print("increase rank to {} since nothing else was refined".format(rank))
                    elif max(ranks) < sol_rank and rank_always:
                        residual.solution, rank = increase_tt_rank(residual.solution, None, None, None, acc=1e-18)
                        print("increase rank to {}, since we always increase the rank".format(rank))
                if min(ranks[1:-1]) == 1:
                    residual.solution, rank = increase_tt_rank(residual.solution, None, None, None, acc=1e-18)
                    print("increase rank to {}, since there is one rank = 1".format(rank))

                W = tt.vector.to_list(residual.solution)
                print("New rank: {}".format(max(tt.vector.from_list(W).r)))
                # endregion
            else:
                print("!!!!!!! NO refinement. Why iterating anyway?")

            mesh_string = "adaptive_mesh-index{}_P{}_{}.png".format(step + 1, femdegree, get_current_time_string())
            # TicToc.sortedTimes(sec_sorted=True)
            base_change_cache = residual.get_cache()
        else:
                eta_global = 0
                zeta_global = 0
                resnorm = 0
                rhs_global = 0
                zero_global = 0
                jump_global = 0
                sq_global = 0
                if (step == 0 or step == 1) and True:
                    mesh = refine(mesh)
                else:
                    if True:
                        if len(hdegs) < 8:
                            hdegs = hdegs + [1]
                        else:
                            for lia in range(len(hdegs)-1):
                                if hdegs[lia] == hdegs[lia+1]:
                                    if hdegs[lia] < 8:
                                        hdegs[lia] += 1
                                    break
                    else:
                        for lia in range(len(hdegs)):
                            if hdegs[lia] < 8:
                                hdegs[lia] += 1
                        if len(hdegs) < 8:
                            hdegs = hdegs + [1]
                    print("new hdegs={}".format(hdegs))
                mesh_string = "uniform_mesh-index{}_P{}_{}.png".format(step + 1, femdegree, get_current_time_string())
        # endregion

        if plot_mesh is True:
            # region Export mesh to file
            from alea.utils.plothelper import plot_mpl
            import matplotlib.pyplot as plt
            # export mesh as file
            plt.figure(1)
            plt.gca().set_aspect('equal')
            plt.gca().xaxis.set_ticklabels([])
            plt.gca().yaxis.set_ticklabels([])
            #                plt.show(block=False)
            plt.clf()
            plot_mpl(mesh)
            plt.title(mesh_string)
            plt.draw()
            plt.savefig(mesh_string, dpi=None, facecolor='w', edgecolor='w', orientation='portrait', papertype=None,
                        format='png', transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None)
            # endregion

        # region Update solution return value
        sol_list[-1]["eta_global"] = eta_global
        print("eta: {}".format(eta_global))
        print("zeta: {}".format(zeta_global))
        print("resnorm : {}".format(resnorm))
        print("ttdofs: {}".format(dofs))
        # print("ALS iter {} where preconditioner = {}".format(als_iter, use_preconditioner))

        sol_list[-1]["zeta_global"] = zeta_global
        sol_list[-1]["resnorm"] = resnorm
        sol_list[-1]["overall-estimator"] = ((#eta_zeta_weight**(-1) *
                                              eta_global +
                                              #+ resnorm_zeta_weight**(-1)*
                                              zeta_global
                                              + resnorm)**2
                                             + resnorm**2)**0.5

        sol_list[-1]["rhs_global"] = rhs_global
        sol_list[-1]["zero_global"] = zero_global
        sol_list[-1]["sq_global"] = sq_global
        sol_list[-1]["jump_global"] = jump_global
        sol_list[-1]['step'] = step_buffer + step
        sol_list[-1]['iterations'] = iterations
        sol_list[-1]['femdegree'] = femdegree
        sol_list[-1]['cont_coef_cores'] = cont_coeff_cores
        if read_from_hash is not None:
            from dolfin import File
            File("{}-{}-mesh.xml".format(read_from_hash, step_buffer + step)) << mesh
            sol_list[-1]['cont_coef_cores'] = cont_coeff_cores
            sol_list[-1]['coeffield_type'] = coeffield_type
            sol_list[-1]['amptype'] = amptype
            sol_list[-1]['decayexp'] = decayexp
            sol_list[-1]['gamma'] = gamma
            sol_list[-1]['freq_scale'] = freq_scale
            sol_list[-1]['freq_skip'] = freq_skip
            sol_list[-1]['scale'] = scale
            sol_list[-1]['field_mean'] = field_mean
            sol_list[-1]['ranks_coef'] = ranks_coef
            sol_list[-1]['hdegs_coef'] = hdegs_coef
            sol_list[-1]['bxmax'] = bxmax
            sol_list[-1]['theta'] = theta
            sol_list[-1]['rho'] = rho
            sol_list[-1]['hdegs'] = hdegs
            sol_list[-1]['n_coef_coef'] = n_coef_coef
            sol_list[-1]['rank'] = rank
            sol_list[-1]['start_rank'] = start_rank
            sol_list[-1]['W'] = W
            sol_list[-1]['preconditioner'] = use_preconditioner
            sol_list[-1]['convergence'] = convergence
            sol_list[-1]['als_iterations'] = als_iterations
            sol_list[-1]['refine_adaptive'] = refine_adaptive
            sol_list[-1]['max_dofs'] = max_dofs
            sol_list[-1]['base_change_cache'] = base_change_cache
            sol_list[-1]['maxrank'] = maxrank
            sol_list[-1]['theta_x'] = theta_x
            sol_list[-1]['theta_y'] = theta_y
            sol_list[-1]['resnorm_zeta_weight'] = resnorm_zeta_weight
            sol_list[-1]['eta_zeta_weight'] = eta_zeta_weight
            sol_list[-1]['rank_always'] = rank_always
            sol_list[-1]['new_hdeg'] = new_hdeg
            sol_list[-1]['sol_rank'] = sol_rank
            import cPickle as Pickle
            f = open(read_from_hash, 'wb')
            Pickle.dump(sol_list, f, Pickle.HIGHEST_PROTOCOL)
            f.close()
        # endregion
        # region Check for abortion criteria
        # if step + step_buffer >= iterations:
        #     break
        if sol_list[-1]["overall-estimator"] <= desired_overall_error:
            print("accuracy reached: {} <= {}".format(sol_list[-1]["overall-estimator"], desired_overall_error))
            break
        else:
            print("current overall error: {} > {} desired accuracy".format(sol_list[-1]["overall-estimator"], desired_overall_error))
        if dofs >= max_dofs:
            print("dofs reached: {} >= {}".format(dofs, max_dofs))
            break
        # endregion
    if read_from_hash is not None:
        # region Export all meshes as xml and store function spaces again
        for lia in range(len(sol_list)):
            # print(" read sol list {}".format(lia))
            sol_list[lia]["mesh"] = Mesh('{}-{}-mesh.xml'.format(str(read_from_hash), lia-1))
            sol_list[lia]["CG"] = FunctionSpace(sol_list[lia]["mesh"], 'CG', sol_list[lia]["femdegree"])
            # print(" cg dofs: {}".format(sol_list[lia]["CG"].dim()))
            # print(" tt: {}".format(sol_list[lia]["V"]))
        # endregion
    return sol_list
# endregion

# region Function: adaptive stochastic Galerkin with ALS


def sgfem_als(iterations=1000, refinements=10, max_dofs=1e6, thetax=0.5, thetay=0.5, srank=2, maxrank=15, urank=1,
              updater=2, M=10, femdegree=1, gpcdegree=1, polysys="L", decayexp=2, gamma=0.9, rvtype="uniform",
              acc=1e-14, do_timing=True, domain="square", mesh_refine=1, mesh_dofs=-1, problem=0,
              convergence=1e-10, max_new_dim=1000, no_longtail_zeta_marking=False, resnorm_zeta_weight=1,
              rank_always=False, eta_zeta_weight=0.1, coeffield_type="EF-square-cos", amp_type="constant",
              field_mean=1.0, _print=False):
    """
    adaptive stochastic Galerkin finite element method using alternating least squares for tensor train objects
    @param iterations:
    @param refinements:
    @param max_dofs:
    @param thetax:
    @param thetay:
    @param srank:
    @param maxrank:
    @param urank:
    @param updater:
    @param M:
    @param femdegree:
    @param gpcdegree:
    @param polysys:
    @param decayexp:
    @param gamma:
    @param rvtype:
    @param acc:
    @param do_timing:
    @param domain:
    @param mesh_refine:
    @param mesh_dofs:
    @param problem:
    @param convergence:
    @param max_new_dim:
    @param no_longtail_zeta_marking:
    @param resnorm_zeta_weight:
    @param rank_always:
    @param eta_zeta_weight:
    @param coeffield_type:
    @param amp_type:
    @param _print:
    @return:
    """

    fp = ''
    try:
        f = open('logging.conf')
        f.close()
        fp = 'logging.conf'
    except IOError:
        print("logging not in working directory")
        try:
            f = open('/home/marschall/WIAS/alea-testing/src/alea/application/bayes/logging.conf')
            f.close()
            fp = '/home/marschall/WIAS/alea-testing/src/alea/application/bayes/logging.conf'
        except IOError:
            print("No logging file found. -> exit()")
            exit()
    logging.config.fileConfig(fp)
    log = logging.getLogger('test_bayes')           # get new logger object

    if _print:
        print 'start A: setup Problem WITH {} COEFFICIENTS'.format(M)
    log.info('  start A: setup Problem WITH {} COEFFICIENTS'.format(M))
# A setup problem
# =================================================================================================
    #                                               # setup domain and meshes
    if mesh_dofs > 0:                               # there is more than one mesh dof
        mesh_refine = 0				                # set mesh refinement to 0
#                           					    # set domain, boundaries and dimensions
    log.info('    create Domain')
    mesh, boundaries, dim = SampleDomain.setupDomain(domain, initial_mesh_N=10)
#                                                   # set mesh again
    log.info('    create Mesh')
    mesh = SampleProblem.setupMesh(mesh, num_refine=mesh_refine)
#                                                   # start mesh refinement
    while mesh_dofs > 0 and FunctionSpace(mesh, 'CG', femdegree).dim() < mesh_dofs:
        if _print:
            print '  initial mesh refinement from %i dofs' % (FunctionSpace(mesh, 'CG', femdegree).dim())
        log.info('      refine mesh')
        mesh = refine(mesh)				            # refine mesh uniformly

    log.info("    initial mesh has %i dofs with p%i FEM" % (FunctionSpace(mesh, 'CG', femdegree).dim(), femdegree))

#                       						    # define coefficient field
    log.info('    create coefficient field with mean {} and amptype {}'.format(field_mean, amp_type))

    coeff_field = SampleProblem.setupCF2(coeffield_type, amp_type, decayexp=decayexp, gamma=gamma, N=4,
                                         freqscale=1, freqskip=0, rvtype=rvtype, scale=1, coeff_mean=field_mean)

#                       						    # setup boundary conditions and pde
    log.info('    create PDE problem objects')
    pde, Dirichlet_boundary, uD, Neumann_boundary, g, f = SampleProblem.setupPDE(2, domain, problem, boundaries,
                                                                                 coeff_field)
    if _print:
        print 'start B: adaptive refinement loop'
    log.info('  start B: adaptive refinement loop')
# B adaptive refinement loop
# =================================================================================================
    SOL = []                    			        # stored solution data for export
    rank = srank					                # store start rank
    ranks = [1] + [rank]*(M-1) + [1]		        # calculate ranks
    gpcps, new_dim = [], []	            		    # lists for gpcs and dimensions
    resnorm = float("inf")			                # init residual norm
    refine_deterministic = False			        # set deterministic refinement off
    rank_done = False				                # set finished rank adaption off
    log.info('    start %i refinements', refinements)
    lia = 0
    for refinement in range(refinements):		    # loop through all refinements
        lia += 1
        TicToc.clear()				                # start time measurements
        SAVEDATA = {}       			     	    # init Savedata dictionary
        if _print:
            print 'start C: Discretisation step number {}'.format(lia)
        log.info('      start C: Discretisation')
# C discretisation
# =================================================================================================
        log.info('        construct operators')
        with TicToc(key="**** construct operators ****", active=do_timing, do_print=False):
            if refinement == 0:			            # stop refinement
                gpcp = [gpcdegree] * (M-1)		    # setup gpc degrees
#                                                   # setup stochastic operators
            D = [construct_stochastic_sgfem_operators(1, p, polysys)[0][1].as_matrix() for p in gpcp]
            D = [None] + D			                # insert dummy for deterministic mode
#                                                   # setup deterministic operators and rhs
            K, B, _ = construct_deterministic_sgfem_operators(pde, coeff_field, M-1, mesh, degree=femdegree)
            K = [k.as_matrix() for k in K]		    # Store K as Matrix
            # Pass those to Robert. K[0][:, :, k] for k in range() !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            B = B[:,0]	            			    #
            B = ravel(B)        				    #
            P = K[0]

        log.info('        operators created')
        gpcps.append(tuple(gpcp))			        # store gpc degrees

        dsize = [d.shape[0] for d in D[1:]]		    # set tensor dimension
        ksize = K[0].shape[0]			            # set tensor size

        # logger.info("dsize %s and ksize %s" % (dsize, ksize))
        n = [ksize] + dsize				            # determine dimensions

        if refinement == 0:				            # last case if there is no refinement left
            log.info('        last refinement')
            if rank == 1:				            # only rank one
                log.info('          rank = 1 -> start ttRandomDeterministicRankOne(%i)', n)
                V = ttRandomDeterministicRankOne(n)     # create random TT Tensor
            else:					                # more than rank one
                log.info('          create random tt')
                V = tt.rand(n, M, ranks)		    # create random TT Tensor with given size
                V *= (1/V.norm())			        # normalize
        log.info('        start right orthogonalization')
        with TicToc(key="**** ttRightOrthogonalize ****", active=do_timing, do_print=False):
            V = ttRightOrthogonalize(V)		        # orthogonalize the solution Tensor from the right
        log.info('        end right orthogonalization')
        if _print:
            print 'start ALS'
        log.info('      start ALS')
# ALS Processing
# ==================================================================================================
        with TicToc(key="**** ttParamPDEALS ****", active=do_timing, do_print=False):
            #                      				    # do parametrized ALS for TT Tensors
            print("start ALS with {} iterations, {} convergence and {} accuracy".format(iterations, convergence, acc))
            V, iter = ttParamPDEALS(K, D, B, V, maxit=iterations, conv=convergence, P=P, acc=acc)
        if _print:
            print 'Start D: evaluate ALS residual'
        log.info('      start D: evaluate ALS residual')
# D evaluate ALS residual
# =================================================================================================
        with TicToc(key="**** evaluate_residual_norm ****", active=do_timing, do_print=False):
            #                      				    # calculate residual mean in energy norm
            resnorm = evaluate_residual_mean_energy_norm(V, K, D, B)
            # if there is no residual or the ranks are smaller than initially given, set residual norm to 0
            if srank >= maxrank or isnan(resnorm):
                resnorm2 = 0
            else:
                resnorm2 = resnorm			        # store residual norm
        if _print:
            print 'Start E: evaluate deterministic error estimator'
        log.info('      start E: evaluate deterministic error estimator')
# E evaluate deterministic error indicator
# =================================================================================================
        CG = pde.function_space(mesh, femdegree)    # set CG as fenics function space
        if thetax > 0:				                # start if the adaptive use is wanted
            #        					            # evaluate residual error estimators
            eta_global, eta_res_global, eta_res_local, \
                       summed_eta_local = evaluate_residual_estimator(V, D, CG, coeff_field, pde.f, pde, gpcp=gpcp,
                                                                      assemble_quad_degree=15, do_timing=do_timing,
                                                                      ncpu=4)
        else:					                    # no error indicator to calculate
            eta_global = 0				            # set global eta to 0
        if _print:
            print 'start F: evaluate stochastic error indicator'
        log.info('      start F: evaluate stochastic error estimator')
        # with open("test.txt", "a") as f:
        #     f.write("ref {} : eta_res_local={} \n".format(refinement, eta_res_local))

# F evaluate stochastic error indicator
# =================================================================================================
        if thetay > 0:				                # do tail estimation
            with TicToc(key="**** evaluateUpperTailBound ****", active=do_timing, do_print=True):
                #                       		    # evaluate residual error estimator
                global_zeta, zeta_ym, zeta_ym_tail = ttEvaluateUpperTailBound(V, gpcp, FEniCSBasis(CG), coeff_field,
                                                                              pde, add_maxm=max_new_dim)

            if not no_longtail_zeta_marking:        #
                zeta_ym.update(zeta_ym_tail)		# add all evaluated tail indicators to indicator set
            #   		                            # calculate global zeta value
            global_zeta = np.sqrt(sum([z ** 2 for z in zeta_ym.values()]))
        else:					                    # no tail estimation or stochastic refinement
            global_zeta = 0				            # set global zeta to zero

        cc, cmi = 0, 0				                # ?
        if _print:
            print 'start decide refinement'
        log.info('      start refinement decision')
# decide whether to refine deterministic or stochastic space or ranks
# =================================================================================================
#           	                   				    # stochastic indicator dominates residual norm
        if global_zeta >= resnorm_zeta_weight * resnorm2 or rank_always or rank >= maxrank or rank_done:
            #                                       # refine deterministic if eta > zeta
            refine_deterministic = eta_zeta_weight * eta_global > global_zeta
            refine_stochastic = not refine_deterministic
            rank_done = False
            if _print:
                print '  refine deterministic'
            log.info('        refine deterministic')
            #  						                # refine stochastic
        elif eta_zeta_weight * eta_global >= resnorm_zeta_weight * resnorm2 or \
                rank_always or rank >= maxrank or rank_done:
            refine_deterministic = True
            refine_stochastic = False
            rank_done = False
            if _print:
                print '  refine stochastic'
            log.info('        refine stochastic')
        else:					                    # no refinement
            refine_deterministic = False
            refine_stochastic = False
            rank_done = True
            if _print:
                print '  no refinement'
            log.info('        no refinement')
        if _print:
            print 'start G export'
        log.info('      start G export')
# G prepare export data and prolongate solution
# =================================================================================================
        dofs = tt_dofs(V)				            # determine number of dofs
        log.info('          tt_dofs=%i', dofs)
#                       						    # store iteration data
        SAVEDATA["iterations"] = iter		        # store number of iterations
        SAVEDATA["V"] = V				            # store solution
        SAVEDATA["CG"] = CG				            # store CG
        SAVEDATA["DOFS"] = dofs			            # store degrees of freedom
        SAVEDATA["res_norm"] = resnorm		        # store residual norm
        SAVEDATA["zeta_global"] = global_zeta	    # store global zeta error indicator
        SAVEDATA["eta_global"] = eta_global	        # store global eta error indicator
        SAVEDATA["nr_active_ydim"] = len(gpcp)	    # store # of active y dimensions
        SAVEDATA["gpcp"] = gpcp			            # store gpcp
        if _print:
            print 'start H refine mesh and prolongate solution'
# H refine mesh adaptively and prolongate solution
# =================================================================================================
    #                          					    # skip refinement in last iteration step
        if refinement + 1 < refinements and dofs < max_dofs:
            if thetax > 0 and refine_deterministic: # deterministic adaptive refinement
                log.info('      refine mesh deterministic')
                V, mesh = refine_mesh_deterministic(eta_global, summed_eta_local, CG, thetax, V, do_timing,
                                                    pde, femdegree)

            new_dim, cmi = [], 0
            if thetay > 0 and refine_stochastic:    # stochastic adaptive refinement
                new_dim, marked_zeta, not_marked_zeta_ym = ttMark_y(zeta_ym, zeta_ym_tail, thetay, global_zeta,
                                                                    not no_longtail_zeta_marking)
                cmi = len(new_dim)
                # 	                                # update tensor w.r.t. stochastic dimensions
                log.info('      add stochastic dimension')
                V, gpcp, M = tt_add_stochastic_dimension(V, new_dim, gpcp, rank)

            ranks = V.r
            #                   				    # refine ranks
            if rank < maxrank and not refine_deterministic and not refine_stochastic:
                log.info('      increase ranks')
                V, rank = increase_tt_rank(V, K, D, B, urank, updater)
            if rank < maxrank and rank_always:
                log.info('      increase ranks')
                V, rank = increase_tt_rank(V, K, D, B, urank, updater)
            if min(ranks[1:M]) == 1:
                log.info('      increase ranks')
                V, rank = increase_tt_rank(V, K, D, B, urank, updater)

#                       						    # complete save data and store
        SAVEDATA["nr_cells_marked"] = cc		    # store # of marked cells
        SAVEDATA["nr_mi_marked"] = cmi  		    # store # of marked mi
        #                       				    # store number of new y dimensions
        SAVEDATA["nr_new_ydim"] = len([i for i in new_dim if i >= M-2])
        SAVEDATA["marked_mi"] = new_dim		        # store marked dimension
        # SAVEDATA["mesh"] = mesh			        # store mesh no swig element
        # SAVEDATA["coeff_field"] = coeff_field     # store coefficient field no swig element
        # SAVEDATA["pde"] = pde                     # store pde no swig element
        SAVEDATA["gpcps"] = gpcps
        SOL.append(SAVEDATA)			            # store dictionary in list
        if _print:
            print 'refinement={0} with tt_dofs={1} of maximal {2}'.format(refinement, dofs, max_dofs)
        if dofs >= max_dofs:			    # exit refinement loop if max dofs are reached
            if _print:
                print 'refinement done'
            log.info('  refinement done')
            break
    return SOL
# endregion

# region Function: get_coefficient_field


def get_coefficient_field(coeffield_type="EF-square-cos", amp_type="constant", decayexp=2, gamma=0.9, rvtype="uniform",
                          coeff_mean=1.0):
    """
      returns the coefficient field created by the given parameters
    @return:
    """
    #                       						    # define coefficient field
    return SampleProblem.setupCF2(coeffield_type, amp_type, decayexp=decayexp, gamma=gamma, freqscale=1, freqskip=0,
                                  rvtype=rvtype, scale=1, coeff_mean=coeff_mean)
# endregion

# region Function: get_coefficient_field_realisation


# noinspection PyProtectedMember
def get_coefficient_field_realisation(rv_samples, coeff_field, max_m, project_basis, log_normal=False):
    """
    creates a sample of the given coefficient field
    @param rv_samples: coefficients to sample at
    @param coeff_field: given coefficient field to sample
    @param max_m: truncation value
    @param project_basis: FEM Basis
    @param log_normal: switch if return value is the exponential
    @return: FEniCSVector
    """
    a = coeff_field.mean_func
    for m in range(max_m):
        a_m = rv_samples[m] * coeff_field[m][0]
        a = a + a_m
    pa = np.exp(a) if log_normal else a
    return FEniCSVector(project(pa, project_basis._fefs))

# endregion
