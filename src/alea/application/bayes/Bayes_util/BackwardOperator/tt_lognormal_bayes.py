from alea.linalg.tensor.extended_tt import ExtendedTT
from alea.linalg.tensor.extended_fem_tt import ExtendedFEMTT


class TTLognormalAsgfemBayes:
    def __init__(self,
                 forward_solution,                  # type: ExtendedFEMTT
                 ):
        """
        Initialises the TTLognormalAsgfemBayes object with the forward solution provided
        :param forward_solution: ExtendedTT
        """
        assert isinstance(forward_solution, ExtendedFEMTT)
        self.forward_solution = forward_solution
        self.measurement_nodes = None
        self.measurement_data = None

    def applyObservation(self,
                         measurement_nodes,         # type: list
                         measurement_data           # type: list
                         ):
        assert isinstance(measurement_nodes, list)
        assert isinstance(measurement_data, list)
        assert len(measurement_nodes) == len(measurement_data)



