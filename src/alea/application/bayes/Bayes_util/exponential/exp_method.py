# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  Simple super class for iterative methods to calculate the exponential of an object
"""
# region Imports
import tt
import numpy as np
import logging
import logging.config
from datetime import datetime
from abc import ABCMeta, abstractmethod
# endregion

# region Class: ExpMethod


class ExpMethod:
    """
      Simple super class for iterative methods to calculate the exponential on an object
    """
    __metaclass__ = ABCMeta

    fp = ''
    try:
        f = open('logging.conf')
        f.close()
        fp = 'logging.conf'
    except IOError:
        print("logging not in working directory")
        try:
            f = open('../../logging.conf')
            f.close()
            fp = '../../logging.conf'
        except IOError:
            print("No logging file found 1")
            try:
                f = open('/home/marschall/WIAS/alea-testing/src/alea/application/bayes/logging.conf')
                f.close()
                fp = '/home/marschall/WIAS/alea-testing/src/alea/application/bayes/logging.conf'
            except IOError:
                print("No logging file found. -> exit()")
                exit()
    logging.config.fileConfig(fp)
    log = logging.getLogger('test_bayes')  # get new logger object

    # region Constructor
    def __init__(self, exp_iter, exp_precision, exp_rank, exp_mc, exp_repeat, exp_repeat_mc):
        """
        constructor method
        @param exp_iter: number of iterations in one process
        @param exp_precision: precision to round objects to using an SVD
        @param exp_rank: maximal rank to round to or start rounding at
        @param exp_mc: number of MonteCarlo samples to estimate convergence with
        @param exp_repeat: number of repetition with doubled step size to reach better precision
        @param exp_repeat_mc: number of MonteCarlo samples to estimate global precision and decide for repetitions
        @return: Null
        """

        if not isinstance(exp_iter, int):
            raise TypeError("ExpMethod Error: steps is not an integer")
        if not isinstance(exp_precision, float):
            raise TypeError("ExpMethod Error: precision is not a float")
        if not isinstance(exp_rank, int):
            raise TypeError("ExpMethod Error: maxrank is not an integer")
        if not isinstance(exp_mc, int):
            raise TypeError("ExpMethod Error: MonteCarlo steps is not an integer")
        if not isinstance(exp_repeat, int):
            raise TypeError("ExpMethod Error: exp_repeat is not an integer")
        if not isinstance(exp_repeat_mc, int):
            raise TypeError("ExpMethod Error: exp_repeat_mc is not an integer")
        #                                           # load logger configuration
        fp = ''
        try:
            f = open('logging.conf')
            f.close()
            fp = 'logging.conf'
        except IOError:
            print("logging not in working directory")
            try:
                f = open('../../logging.conf')
                f.close()
                fp = '../../logging.conf'
            except IOError:
                print("No logging file found 1")
                try:
                    f = open('/home/marschall/WIAS/alea-testing/src/alea/application/bayes/logging.conf')
                    f.close()
                    fp = '/home/marschall/WIAS/alea-testing/src/alea/application/bayes/logging.conf'
                except IOError:
                    print("No logging file found. -> exit()")
                    exit()
        logging.config.fileConfig(fp, defaults={'logFilePrefix': str(datetime.now().strftime('%Y%m%d'))})
        log = logging.getLogger('test_bayes')               # get new logger object
        self.__exp_iter = exp_iter
        self.__exp_precision = exp_precision
        self.__exp_rank = exp_rank
        self.__exp_mc = exp_mc
        self.__exp_repeat = exp_repeat
        self.__exp_repeat_mc = exp_repeat_mc
        self.local_convergence = []
        self.global_convergence = []
        self.step_size_list = []
        self.solution = 0
        self.global_precision = 0

    # endregion

    def __str__(self):
        name = ""
        name += "i%i_" % self.exp_iter
        name += "p%.2e_" % self.exp_precision
        name += "r%i_" % self.exp_rank
        name += "lMc%i_" % self.exp_mc
        name += "rep%i_" % self.exp_repeat
        name += "gMc%i_" % self.exp_repeat_mc
        name += "gP%.2e_" % self.global_precision
        return name

    # region Property exp_iter
    @property
    def exp_iter(self):
        """
          gets the number of iterations in the iterative process
        @return: exp_iter
        @rtype: int
        """
        return self.__exp_iter
    
    @exp_iter.setter
    def exp_iter(self, value):
        """
        sets the number of iterations in the iterative process
        @param value: value
        @type value: int
        @return: 
        """
        if not isinstance(value, int):
            raise TypeError('iteration number is not an integer')
        if value < 1:
            raise AssertionError('iteration number should be positive')
        self.__exp_iter = value
    # endregion
        
    # region Property exp_precision
    @property
    def exp_precision(self):
        """
          gets the used precision for rounding in the iterative process
        @return: exp_precision
        @rtype: float
        """
        return self.__exp_precision
    
    @exp_precision.setter
    def exp_precision(self, value):
        """
        sets the used precision for rounding in the iterative process
        @param value: value
        @type value: float
        @return: 
        """
        if not isinstance(value, float):
            raise TypeError('precision is not float')
        if value < 0:
            raise AssertionError('precision should be positive')
        if value > 1:
            raise AssertionError('precision should not be > 1')
        self.__exp_precision = value
    # endregion
    
    # region Property exp_rank
    
    @property
    def exp_rank(self):
        """
          gets the maximal rank used in the iteration process
        @return: exp_rank
        @rtype: int
        """
        return self.__exp_rank
    
    @exp_rank.setter
    def exp_rank(self, value):
        """
        sets the maximal rank used in the iterative process
        @param value: value
        @type value: int
        @return: 
        """
        if not isinstance(value, int):
            raise TypeError('rank is not an integer')
        if value < 1:
            raise AssertionError('rank should be positive')
        if value > 400:
            raise AssertionError('rank is to high. Space will probably explode')
        self.__exp_rank = value
    # endregion
    
    # region Property exp_mc
    @property
    def exp_mc(self):
        """
          gets the number of monte carlo samples to check the convergence
        @return: exp_mc
        @rtype: int
        """
        return self.__exp_mc
    
    @exp_mc.setter
    def exp_mc(self, value):
        """
        sets the number of monte carlo samples to check the convergence
        @param value: value
        @type value: int
        @return: 
        """
        if not isinstance(value, int):
            raise TypeError('monte carlo samples number is not an integer')
        if value < 1:
            raise AssertionError('monte carlo samples number should be positive')
        if value > 1e6:
            raise RuntimeWarning('more than a million monte carlo steps result in long calculation time')
        self.__exp_mc = value
    # endregion
    
    # region Property exp_repeat
    # region Getter
    @property
    def exp_repeat(self):
        """
          gets the number of repetitions of the hole scheme
        @return: exp_repeat
        @rtype: int
        """
        return self.__exp_repeat
    # endregion
    # region Setter

    @exp_repeat.setter
    def exp_repeat(self, value):
        """
        sets the number of repetitions of the hole scheme
        @param value: value
        @type value: int
        @return: 
        """
        if not isinstance(value, int):
            raise TypeError('repetition number is not an integer')
        if value < 1:
            raise AssertionError('repetition number should be positive')
        if value > 100:
            raise RuntimeWarning('more than 100 repetitions should be to much')
        self.__exp_repeat = value
    # endregion
    # endregion

    # region Property exp_mc_repeat
    # region Getter
    @property
    def exp_repeat_mc(self):
        """
          gets the number of monte carlo samples to check the result of the iteration process
        @return: exp_repeat_mc
        @rtype: int
        """
        return self.__exp_repeat_mc
    # endregion
    # region Setter

    @exp_repeat_mc.setter
    def exp_repeat_mc(self, value):
        """
        sets the number of monte carlo samples to check the result of the iteration process
        @param value: value
        @type value: int
        @return: 
        """
        if not isinstance(value, int):
            raise TypeError('monte carlo sample number is not an integer')
        if value < 1:
            raise AssertionError('monte carlo sample number should be positive')
        if value > 1e6:
            raise RuntimeWarning('To many monte carlo sampling result in long computational time')
        self.__exp_repeat_mc = value
    # endregion

    # endregion

    # region Function: Calculate exponential of a TT-tensor using the direct exponential function of numpy
    @abstractmethod
    def calculate_exponential_tt(self, tensor):
        """
        calculates the exponential.
        @param tensor: object to calculate exponential from. Here TT-Tensor
        @return: exponential
        """
        pass
    # endregion

    # region Get exponential
    def get_exponential(self, tensor, precision):
        """
        returns the exponential of the given tensor with used precision
        """
        if self.solution == 0:
            self.calculate_exponential_tt_precision(tensor, precision)
        return self.solution, self.local_convergence, self.global_convergence, self.step_size_list

    # endregion

    # region Function: calculate exponential of a TT-tensor up to a given precision
    def calculate_exponential_tt_precision(self, tensor, precision):
        """
        calculates the exponential function of a given TT-tensor up to a give precision (note: precision is calculated
        via expensive samples. The amount of maximal repetitions and MonteCarlo samplings is defined in the upper class
        @param tensor: object to calculate exponential from
        @param precision: desired precision for the exponential function
        @return: solution, local_convergence, global_convergence, step_size
        """
        # logging.config.fileConfig('logging.conf')   # load logger configuration
        # log = logging.getLogger('test_bayes')       # get new logger object
        if not isinstance(tensor, tt.vector):
            raise TypeError("ExplicitEuler Error: tensor is not a tt")
        if not isinstance(precision, float):
            raise TypeError("ExplicitEuler Error: precision is not a float")

        self.local_convergence = []
        self.global_convergence = []
        self.step_size_list = []

        solution_prev = tensor
        self.global_precision = precision
        euler_counter = 0                           # prepare counter for MC repetition
        self.step_size_list.append(1./self.exp_iter)

        while (len(self.global_convergence) < 1) or \
                (self.global_precision < self.global_convergence[-1] and euler_counter < self.exp_repeat):

            if euler_counter > 0:
                #                                   # store new step size
                self.step_size_list.append(1./(2**euler_counter*self.exp_iter))
                self.exp_iter *= 2
                # log.debug("    euler repetition # {0} for MC Mean {1:16f} > {2:16f} with "
                #          "{3} steps".format(euler_counter, self.global_convergence[-1], self.global_precision,
                #                             (2 ** euler_counter * self.exp_iter)))
            # else:
                # log.info("    initially calculate exponential")
            #                                       # calculate point-wise exponential using explicit euler

            self.solution, convergence = self.calculate_exponential_tt(solution_prev)
            #                                       # store current convergence list
            self.local_convergence.append(convergence)
            #                                       # create new index samples for MC
            y = [
                [np.random.random_integers(0, high=self.solution.n[lic]-1) for lic in range(len(self.solution.n))]
                for _ in range(self.exp_repeat_mc)
                ]
            self.global_convergence.append(0)       # append new convergence object extremely large
            for lia, y_k in enumerate(y):           # should be comparable to the last entry of local_convergence
                self.global_convergence[-1] += np.abs(self.solution[y_k] - np.exp(solution_prev[y_k]))
            self.global_convergence[-1] /= self.exp_repeat_mc
            print ("    resulting MC error = {0:.4e} "
                   "should be at least {1:.4e}".format(self.global_convergence[-1], self.global_precision))
            # log.debug("    resulting MC error = {0:.4e} "
            #          "should be at least {1:.4e}".format(self.global_convergence[-1], self.global_precision))
            euler_counter += 1                      # increase current counter variable

    # endregion

# endregion
