# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  calculates the exponential function of a given object. suitable for tensor train formats
"""

# region Imports
import logging
import logging.config

import numpy as np
import tt

from alea.application.bayes.Bayes_util.exponential.exp_method import ExpMethod

from joblib import Parallel, delayed
import gc

_eulertensor = None
_starttensor = None
# endregion


class ExplicitEuler(ExpMethod):
    """
      class object for the explicit euler scheme inheriting an general iterative method for calculation of the
      exponential function
    """

    # region overwrites Function Constructor
    def __init__(self, *args):
        ExpMethod.__init__(self, *args)
    # endregion

    # region overwrites Function tostring
    def __str__(self):
        return ExpMethod.__str__(self) + "exp"
    # endregion

    # region overwrites Function exponential calculation
    def calculate_exponential_tt(self, tensor, n_cpu = 40):
        """
        calculates the exponential of the solution object using defined parameters
        @param tensor: TT-tensor to calculate exponential of
        @return: convergence, exponential
        """
        global _eulertensor
        global _starttensor

        # logging.config.fileConfig('logging.conf')   # load logger configuration
        # log = logging.getLogger('test_bayes')       # get new logger object

        if not isinstance(tensor, tt.vector):

            raise TypeError("ExplicitEuler Error: tensor is not a tt")
        convergence = []

        #                                           # create random rank 1 tt
        eulertestbuffer = tt.rand(tensor.n, len(tensor.n), [1 + 0*_lia for _lia in range(len(tensor.n)+1)])
        #                                           # store tensor as list of 3d array
        eulertestbuffer = tt.vector.to_list(eulertestbuffer)
        #                                           # store true values in first comp
        eulertestbuffer[0][0, :, 0] = np.ones(tensor.n[0])
        for _lia in range(1, len(tensor.n)):	    # replace other comp with vector of ones
            eulertestbuffer[_lia][0, :, 0] = np.ones(tensor.n[_lia])
            # log.info("start tensor has rank {0}".format(tensor.r))
            if max(tensor.r) > self.exp_rank:
                # log.info(' round start tensor')
                tensor = tt.vector.round(tensor, self.exp_precision, self.exp_rank)
                # log.info("start tensor rounded to {0}".format(tensor.r))
        eulertestbuffer = tt.vector.from_list(eulertestbuffer)

        # log.info("Start Euler with precision {0}".format(self.exp_precision))
        for _lia in range(1, self.exp_iter+1):
            # if _lia % 100 == 0:
            # log.info("Step {0} with ranks {1}".format(_lia, eulertestbuffer.r))
            if max(eulertestbuffer.r) > self.exp_rank:
                # log.info("  round buffer tensor from {0}".format(eulertestbuffer.r))
                eulertestbuffer = tt.vector.round(eulertestbuffer, self.exp_precision, self.exp_rank)
                # log.info("  buffer tensor rounded to {0}".format(eulertestbuffer.r))
            eulertestbuffer += 1./self.exp_iter * tensor * eulertestbuffer

            convergence.append(0)
            # _eulertensor = eulertestbuffer
            # _starttensor = tensor
            #                                       # create new index samples for MC
            # y = np.array([
            #     [np.random.random_integers(0, high=eulertestbuffer.n[lic]-1) for lic in range(len(eulertestbuffer.n))]
            #     for _ in range(self.exp_mc)
            #     ])

            # convergence[-1] = np.sum(np.array(Parallel(n_cpu)(delayed(_workInParallel)(xi) for xi in y)))
            # convergence[-1] /= self.exp_mc
            # log.info('  sampled current distance to solution: %.16f', convergence[-1])

        return eulertestbuffer, convergence
    # endregion
def _workInParallel(xi):
    mc_sample = np.abs(_eulertensor[xi] - np.exp(_starttensor[xi]))
    #gc.collect()
    return mc_sample
