# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  calculates the exponential function of a given object. suitable for tensor train formats
"""

# region Imports
from __future__ import division
import numpy as np
from alea.linalg.tensor.extended_tt import ExtendedTT, BasisType
import tt

# from abc import ABCMeta
from alea.application.bayes.Bayes_util.exponential.exp_method import ExpMethod


# endregion
# region Method Enum
class METHODS(set):
    def __getattr__(self, name):
        if name in self:
            return name
        raise AttributeError


# endregion

class ExtendedTTEmbeddedRungeKutta(ExpMethod):
    """
      class object for arbitrary explicit embedded Runge-Kutta scheme inheriting an general iterative method
      for calculation of the exponential function using the possibility of step size control due to different
      Runge-Kutta methods
    """

    # __metaclass__ = ABCMeta

    # region overwrites Function Constructor
    def __init__(self, *args):
        ExpMethod.__init__(self, *args)
        self.METHODS = METHODS(["linear", "square", "forth"])
        self._method = self.METHODS.linear
        self._beta = 0.9
        self._max_stepsize_change = 0.1
        self._min_stepsize = 1e-6
        self._max_stepsize = 0.5
        self._acc = 1e-5

    # endregion

    # region overwrites Function tostring
    def __str__(self):
        return ExpMethod.__str__(self) + "erk"

    # endregion

    # region Property method
    @property
    def method(self):
        """
          gets the used method
        @return: method
        @rtype: str
        """
        return self._method

    @method.setter
    def method(self, value):
        """
        sets the used method to one of the known values
        @param value: value
        @type value: str
        @return:
        """
        if not isinstance(value, str):
            raise TypeError('method is not a string')
        if value not in self.METHODS:
            raise ValueError("method is unknown")
        self._method = value

    # endregion

    # region Property beta
    @property
    def beta(self):
        """
          gets the used beta as relaxation parameter (default 0.9)
        @return: beta
        @rtype: float
        """
        return self._beta

    @beta.setter
    def beta(self, value):
        """
        sets the used beta to an admissible value
        @param value: value to set to
        @type value: float
        """
        if not isinstance(value, float):
            raise TypeError('beta is not a float')
        if not 0 < value < 1:
            raise ValueError("beta is not between 0 and 1")
        self._beta = value

    # endregion

    # region Property max_stepsize
    @property
    def max_stepsize(self):
        """
          gets the used max_stepsize
        @return: max_stepsize
        @rtype: float
        """
        return self._max_stepsize

    @max_stepsize.setter
    def max_stepsize(self, value):
        """
        sets the used max_stepsize to an admissible value
        @param value: value
        @type value: float
        @return:
        """
        if not isinstance(value, float):
            raise TypeError('max_stepsize is not a float')
        if not 0 < value < 1:
            raise ValueError("max_stepsize is not between 0 and 1")
        self._max_stepsize = value

    # endregion

    # region Property min_stepsize
    @property
    def min_stepsize(self):
        """
          gets the used min_stepsize
        @return: min_stepsize
        @rtype: float
        """
        return self._min_stepsize

    @min_stepsize.setter
    def min_stepsize(self, value):
        """
        sets the used min_stepsize to an admissible value
        @param value: value
        @type value: float
        @return:
        """
        if not isinstance(value, float):
            raise TypeError('min_stepsize is not a float')
        if not 0 < value < 1:
            raise ValueError("min_stepsize is not between 0 and 1")
        self._min_stepsize = value

    # endregion

    # region Property acc
    @property
    def acc(self):
        """
          gets the used acc
        @return: acc
        @rtype: float
        """
        return self._acc

    @acc.setter
    def acc(self, value):
        """
        sets the used acc to an admissible value
        @param value: value
        @type value: float
        @return:
        """
        if not isinstance(value, float):
            raise TypeError('acc is not a float')
        if not 0 < value < 1:
            raise ValueError("acc is not between 0 and 1")
        self._acc = value

    # endregion

    # region Property max_stepsize_change
    @property
    def max_stepsize_change(self):
        """
          gets the used max_stepsize_change
        @return: max_stepsize_change
        @rtype: float
        """
        return self._max_stepsize_change

    @max_stepsize_change.setter
    def max_stepsize_change(self, value):
        """
        sets the used max_stepsize_change to an admissible value
        @param value: value
        @type value: float
        @return:
        """
        if not isinstance(value, float):
            raise TypeError('max_stepsize_change is not a float')
        if not 0 < value < 1:
            raise ValueError("max_stepsize_change is not between 0 and 1")
        self._max_stepsize_change = value

    # endregion

    # region overwrites Function exponential calculation
    def calculate_exponential_tt(self, tensor, n_cpu=40):
        """
        calculates the exponential of the solution object using defined parameters
        @param tensor: TT-tensor to calculate exponential of
        @return: convergence, exponential
        """
        # logging.config.fileConfig('logging.conf')   # load logger configuration
        # log = logging.getLogger('test_bayes')       # get new logger object
        max_n = 12
        if not isinstance(tensor, ExtendedTT):
            raise TypeError("ExtendedTtEmbeddedRungeKutta Error: tensor is not an extended tt")

        if self.method == self.METHODS.linear:  # heun / euler
            mid = [
                [0, 0],
                [1, 0]
            ]
            b_1 = [1, 0]
            b_2 = [0.5, 0.5]
        elif self.method == self.METHODS.square:  # Bogacki / Shampine
            mid = [
                [0, 0, 0, 0],
                [1 / 2, 0, 0, 0],
                [0, 3 / 4, 0, 0],
                [2 / 3, 1 / 3, 4 / 9, 0]
            ]
            b_1 = [2 / 9, 1 / 3, 4 / 9, 0]
            b_2 = [7 / 24, 1 / 4, 1 / 3, 1 / 8]
        elif self.method == self.METHODS.forth:  # Runge-Kutta / Fehlberg
            # c = [0, 1/2, 1]
            mid = [
                [0, 0, 0, 0, 0, 0],
                [1 / 4, 0, 0, 0, 0, 0],
                [3 / 32, 9 / 32, 0, 0, 0, 0],
                [1932 / 2197, -7200 / 2197, 7296 / 2197, 0, 0, 0],
                [439 / 216, -8, 3680 / 513, -845 / 4104, 0, 0],
                [-8 / 27, 2, -3544 / 2565, 1859 / 4104, -11 / 40, 0]
            ]
            b_1 = [25 / 216, 0, 1408 / 2565, 2197 / 4104, -1 / 5, 0]
            b_2 = [16 / 135, 0, 6656 / 12825, 28561 / 56430, -9 / 50, 2 / 55]
        else:
            raise TypeError("RungeKutta: Method is unknown")

        def f(x):
            """
            function evaluation. In general this is the rhs von the ODE. Same here, but just a stationary multiplication
            :param x: current value of the iteration
            :return: rhs of ODE
            """

            assert isinstance(x, ExtendedTT)
            return x.multiply_with_extendedTT(tensor)

        convergence = []
        if max(tensor.r) > self.exp_rank:
            tensor = tensor.round(self.exp_precision, self.exp_rank)

        #                                           # create random rank 1 tt
        eulertestbuffer = tt.rand(tensor.n, len(tensor.n), [1 + 0 * _lia for _lia in range(len(tensor.n) + 1)])
        #                                           # store tensor as list of 3d array
        eulertestbuffer = tt.vector.to_list(eulertestbuffer)
        #                                           # store true values in first comp
        eulertestbuffer[0][0, :, 0] = np.zeros(tensor.n[0])
        eulertestbuffer[0][0, 0, 0] = 1
        for _lia in range(1, len(tensor.n)):  # replace other comp with vector of ones
            eulertestbuffer[_lia][0, :, 0] = np.zeros(tensor.n[_lia])
            eulertestbuffer[_lia][0, 0, 0] = 1
        eulertestbuffer = ExtendedTT(eulertestbuffer, basis=[BasisType.NormalisedHermite]*tensor.dim)

        exp_a_rk_1 = eulertestbuffer.copy()
        exp_a_rk_2 = eulertestbuffer.copy()
        h = [1. / self.exp_iter]
        lia = 0
        while True:
            lia += 1
            if len(h) > 1e6:
                raise AssertionError("no convergence after {} steps".format(1e6))

            summ = 0
            k = []
            for j in range(0, len(b_1)):
                for i in range(0, j):
                    if summ == 0:
                        summ = mid[j][i] * k[i]
                    elif isinstance(summ, ExtendedTT):
                        summ += mid[j][i] * k[i]
                if summ == 0:
                    k.append((f(exp_a_rk_2)) * h[-1])
                elif isinstance(summ, ExtendedTT):
                    if max(summ.r) > self.exp_rank:
                        summ.round(self.exp_precision, self.exp_rank)
                    if max(summ.n) > max_n:
                        summ = summ.cut(max_n)
                    k.append(h[-1] * (f(exp_a_rk_2 + summ)))
                else:
                    raise TypeError
                summ = 0
            summ1 = 0
            summ2 = 0
            for j in range(0, len(b_1)):
                if summ1 == 0:
                    summ1 = b_1[j] * k[j]
                    summ2 = b_2[j] * k[j]
                elif isinstance(summ1, ExtendedTT):
                    if max(summ1.r) > self.exp_rank:
                        summ1.round(self.exp_precision, self.exp_rank)
                    if max(summ1.n) > max_n:
                        summ1 = summ1.cut(max_n)
                    summ1 += b_1[j] * k[j]
                    if max(summ2.r) > self.exp_rank:
                        summ2.round(self.exp_precision, self.exp_rank)
                    if max(summ2.n) > max_n:
                        summ2 = summ2.cut(max_n)
                    summ2 += b_2[j] * k[j]
                else:
                    raise TypeError

            exp_a_rk_2 = exp_a_rk_1 + summ2
            exp_a_rk_1 += summ1

            if np.sum(np.array(h)) == 1:
                return exp_a_rk_1, convergence
            # eps = tt.vector.norm(exp_a_rk_1 - exp_a_rk_2)
            eps = 0
            for j in range(len(b_1)):
                if j == 0:
                    eps = (b_1[j] - b_2[j]) * k[j]
                else:
                    eps += (b_1[j] - b_2[j]) * k[j]
            assert isinstance(eps, ExtendedTT)
            eps = tt.vector.norm(tt.vector.from_list(eps.components))
            print eps
            if eps >= self.acc:
                h.append(self.beta * h[-1] * ((self.acc / eps) ** 0.2))
            else:
                h.append(self.beta * h[-1] * ((self.acc / eps) ** 0.25))
            if h[-1] / h[-2] > self.max_stepsize_change:
                h[-1] *= self.max_stepsize_change
            elif h[-1] / h[-2] < 1. / self.max_stepsize_change:
                h[-1] /= self.max_stepsize_change

            if h[-1] < self.min_stepsize:
                raise AssertionError("stepsize to small: {} < {}".format(h[-1], self.min_stepsize))

            if np.sum(np.array(h)) > 1:
                h[-1] = 1 - (np.sum(np.array(h)) - h[-1])
            if h[-1] > self.max_stepsize:
                raise AssertionError("stepsize to big: {} > {}".format(h[-1], self.max_stepsize))
            convergence.append(eps)
            if h[-1] < 1e-15:
                return exp_a_rk_1, convergence
            #                                       # Just a hack to obtain smaller step sizes
            if h[-1] < 1e-5 and h[-1] * 10 < 1 - 1e-4:
                h[1] *= 10

            if max(exp_a_rk_1.r) > self.exp_rank:
                exp_a_rk_1.round(self.exp_precision, self.exp_rank)
            if max(exp_a_rk_2.r) > self.exp_rank:
                exp_a_rk_2.round(self.exp_precision, self.exp_rank)
            if max(exp_a_rk_1.n) > max_n:
                exp_a_rk_1 = exp_a_rk_1.cut(max_n)
            if max(exp_a_rk_2.n) > max_n:
                exp_a_rk_2 = exp_a_rk_2.cut(max_n)
            if lia % 1 == 0:
                print("step {}, stepsize = {}, remaining: {}".format(lia, h[-1], 1 - np.sum(np.array(h))))
                print("curr solution: \n{}".format(exp_a_rk_1))

        raise AssertionError("ExtendedTTEmbeddedRungeKutta: You should not come here...")
    # endregion


