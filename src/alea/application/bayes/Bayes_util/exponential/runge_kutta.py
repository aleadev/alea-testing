# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  calculates the exponential function of a given object. suitable for tensor train formats
"""

# region Imports
from __future__ import division
import numpy as np
import tt

from alea.application.bayes.Bayes_util.exponential.exp_method import ExpMethod

from joblib import Parallel, delayed
import gc

_eulertensor = None
_starttensor = None
# endregion


class RungeKutta(ExpMethod):
    """
      class object for arbitrary explicit Runge-Kutta scheme inheriting an general iterative method for calculation of
      the exponential function
    """

    # region overwrites Function Constructor
    def __init__(self, *args):
        ExpMethod.__init__(self, *args)
        self.method = ""
    # endregion

    # region overwrites Function tostring
    def __str__(self):
        return ExpMethod.__str__(self) + "rk"
    # endregion

    # region overwrites Function exponential calculation
    def calculate_exponential_tt(self, tensor, n_cpu=40):
        """
        calculates the exponential of the solution object using defined parameters
        @param tensor: TT-tensor to calculate exponential of
        @return: convergence, exponential
        """
        global _eulertensor
        global _starttensor

        # logging.config.fileConfig('logging.conf')   # load logger configuration
        # log = logging.getLogger('test_bayes')       # get new logger object

        if not isinstance(tensor, tt.vector):
            raise TypeError("RungeKutta Error: tensor is not a tt")

        if self.method == "eigen":
            # c = [0, 1, 1/2]
            mid = [[0, 0, 0],
                   [1, 0, 0],
                   [0, 1/2, 0]]
            b=[0,0,1]
        elif self.method == "exp euler":
            # c = [0]
            mid = [[1]]
            b = [1]
        elif self.method == "heun":
            # c = [0, 1/3, 2/3]
            mid = [[0, 0, 0],
                   [1/3, 0, 0],
                   [0, 2/3, 0]]
            b = [1/4, 0, 3/4]
        elif self.method == "3rk":
            # c = [0, 1/2, 1]
            mid = [[0, 0, 0],
                   [1/2, 0, 0],
                   [-1, 2, 0]]
            b = [1/6, 4/6, 1/6]
        elif self.method == "runge kutta":
            # c = [0, 1/2, 1/2, 1]
            mid = [[0, 0, 0, 0],
                   [1/2, 0, 0, 0],
                   [0, 1/2, 0, 0],
                   [0, 0, 1, 0]]
            b = [1/6, 2/6, 2/6, 1/6]
        else:
            raise TypeError("RungeKutta: Method is unknown")

        def f(x):
            """
            function evaluation. In general this is the rhs von the ODE. Same here, but just a stationary multiplication
            :param x: current value of the iteration
            :return: rhs of ODE
            """
            return tensor*x
        convergence = []

        if max(tensor.r) > self.exp_rank:
            tensor = tt.vector.round(tensor, self.exp_precision, self.exp_rank)

        #                                           # create random rank 1 tt
        eulertestbuffer = tt.rand(tensor.n, len(tensor.n), [1 + 0*_lia for _lia in range(len(tensor.n)+1)])
        #                                           # store tensor as list of 3d array
        eulertestbuffer = tt.vector.to_list(eulertestbuffer)
        #                                           # store true values in first comp
        eulertestbuffer[0][0, :, 0] = np.ones(tensor.n[0])
        for _lia in range(1, len(tensor.n)):	    # replace other comp with vector of ones
            eulertestbuffer[_lia][0, :, 0] = np.ones(tensor.n[_lia])
        eulertestbuffer = tt.vector.from_list(eulertestbuffer)

        convergence.append(0)

        h = 1./self.exp_iter
        # print "start runge kutta with step size {}".format(h)
        for l in range(self.exp_iter):
            if l % 10 == 0 or l == self.exp_iter-1:
                # _eulertensor = eulertestbuffer
                # _starttensor = tensor
                #                                       # create new index samples for MC
                y = np.array([
                    [np.random.random_integers(0, high=eulertestbuffer.n[lic]-1)
                     for lic in range(len(eulertestbuffer.n))]
                    for _ in range(self.exp_mc)
                    ])
                mc_sample = 0
                for xi in y:
                    mc_sample += np.abs(eulertestbuffer[xi] - np.exp(tensor[xi]))
                mc_sample /= self.exp_mc
                # convergence[-1] = np.sum(np.array(Parallel(n_cpu)(delayed(_workInParallel)(xi) for xi in y)))
                # convergence[-1] /= self.exp_mc
                # log.info('  sampled current distance to solution: %.16f', convergence[-1])
                print "step {} / {}. sampled distance: {:.16f}".format(l, self.exp_iter, mc_sample)
            buff = 0
            k = []
            for j in range(len(b)):
                for i in range(j):
                    if buff == 0:
                        buff = mid[j][i] * k[i]
                    elif isinstance(buff, tt.vector):
                        buff += mid[j][i] * k[i]
                    else:
                        raise TypeError("RungeKutta: buff is not a vector, nor 0")
                    if max(buff.r) > self.exp_rank:
                        # print "round tensor from {}, since max = {} > {}".format(buff.r,max(buff.r),self.exp_rank)
                        buff = tt.vector.round(buff, self.exp_precision, self.exp_rank)
                if buff == 0:
                    k.append(h*(f(eulertestbuffer)))
                elif isinstance(buff, tt.vector):
                    if max(buff.r) > self.exp_rank:
                        # print "round tensor from {}, since max = {} > {}".format(buff.r,max(buff.r),self.exp_rank)
                        buff = tt.vector.round(buff, self.exp_precision, self.exp_rank)
                    k.append(h*(f(eulertestbuffer+buff)))
                else:
                    raise TypeError("RungeKutta. buff is not a vector, nor 0")
                buff = 0
            buff = 0
            for j in range(len(b)):
                if buff == 0:
                    buff = b[j]*k[j]
                elif isinstance(buff, tt.vector):
                    buff += b[j]*k[j]
                else:
                    raise TypeError("RungeKutta: buff is not a vector, nor 0")

                if max(buff.r) > self.exp_rank:
                    # print "round tensor from {}, since max = {} > {}".format(buff.r,max(buff.r),self.exp_rank)
                    buff = tt.vector.round(buff, self.exp_precision, self.exp_rank)
            eulertestbuffer += buff

            if max(eulertestbuffer.r) > self.exp_rank:
                eulertestbuffer = tt.vector.round(eulertestbuffer, self.exp_precision, self.exp_rank)

        return eulertestbuffer, convergence
    # endregion
def _workInParallel(xi):
    mc_sample = np.abs(_eulertensor[xi] - np.exp(_starttensor[xi]))
    #gc.collect()
    return mc_sample
