# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  class for bayesian inversion using a black box MCMC solver to estimate samples according to the posterior measure
"""
# region Import
import cPickle as Pickle

import numpy as np

from alea.application.bayes.Bayes_util.ForwardOperator.asgfem_operator import AsgFemOperator
from alea.application.bayes.Bayes_util.lib.bayes_sgfem_als import get_coefficient_field_realisation
from alea.application.bayes.Bayes_util.Interface.IBayes import IBayes
from alea.application.bayes.Bayes_util.lib.bayes_lib import ensure_iterable
import emcee
from alea.application.bayes.Bayes_util.lib.bayes_lib import create_noisy_measurement_data_dofs
# endregion


class LogNormalASGFEMBayesMCMC(IBayes):
    # TODO: Create MCMC interface and implement here
    """
      Simple Bayesian inversion object
    """

    MCMC_RUN = 0
    USE_DOFS = False

    # region Constructor
    def __init__(self, solution, dofs, measurements, true_values, covariance, sample_coordinates, chain_walker=None,
                 burn_in=100, mc_steps=1000):
        """
          constructor method using all objects needed for bayesian inversion
        @param solution: solution object of the forward operator
        @param measurements: measured values. z = (O o G)(u) + eta
        @param dofs: degrees of freedom to sample forward solution at
        @param covariance: covariance value
        @param chain_walker: number of parallel chains for the mcmc process. Default 2*#coeff + 1
        @param sample_coordinates: coordinates used for sampling the measurements
        @return:
        """

        if chain_walker is None:
            chain_walker = 2*len(true_values) + 1

        self.solution = solution
        self.dofs = dofs
        self.sample_coordinates = sample_coordinates
        self.measurements = measurements
        self.true_values = true_values
        self.covariance = covariance
        self.chain_walker = chain_walker
        self.burn_in = burn_in
        self.mc_steps = mc_steps
        self.forwardOp = AsgFemOperator()
        self.solution = None
        self.sampler = None
    # endregion

    # region Function: init_solver
    @classmethod
    def init_solver(cls, dofs, sample_coordinates, true_values, chain_walker, burn_in, mc_steps, covariance,
                    iterations=1000, refinements=10, max_dofs=1e6, thetax=0.5, thetay=0.5, srank=2, maxrank=15, urank=1,
                    updater=2, m=10, femdegree=1, gpcdegree=1, polysys="L", decayexp=2, gamma=0.9, rvtype="uniform",
                    acc=1e-14, do_timing=True, domain="square", mesh_refine=1, mesh_dofs=-1, problem=0,
                    convergence=1e-12, max_new_dim=1000, no_longtail_zeta_marking=False, resnorm_zeta_weight=1,
                    rank_always=False, eta_zeta_weight=0.1, coeffield_type="EF-square-cos", amp_type="constant"
                    ):
        """
        initializes the Bayesian object with everything needed for the calculation process
        """
        obj = cls(0, dofs, [], true_values, covariance, sample_coordinates, chain_walker, burn_in=burn_in,
                  mc_steps=mc_steps)
        obj.create_forward_operator(iterations=iterations, refinements=refinements, max_dofs=max_dofs, thetax=thetax,
                                    thetay=thetay, srank=srank, maxrank=maxrank, urank=urank, updater=updater, m=m,
                                    femdegree=femdegree, gpcdegree=gpcdegree, polysys=polysys, decayexp=decayexp,
                                    gamma=gamma, rvtype=rvtype, acc=acc, do_timing=do_timing, domain=domain,
                                    mesh_refine=mesh_refine, mesh_dofs=mesh_dofs, problem=problem,
                                    convergence=convergence, max_new_dim=max_new_dim,
                                    no_longtail_zeta_marking=no_longtail_zeta_marking,
                                    resnorm_zeta_weight=resnorm_zeta_weight, rank_always=rank_always,
                                    eta_zeta_weight=eta_zeta_weight, coeffield_type=coeffield_type,
                                    amp_type=amp_type)
        return obj
    # endregion

    # region overwrites Function Create Coefficient Field
    def create_coeff_field(self, funcs, mean):
        """
        not needed for asgfem operator
        """
        raise NotImplementedError
    # endregion

    # region Function Calculate samples direct from FEM solution
    def calculate_samples_direct(self):
        """
        starts the MCMC procedure and creates a list of samples, distributed according to the posterior measure
        """

        # TODO: check validity of all needed elements
        def lnprob(loc_xi, loc_y, loc_solution, loc_cov, loc_dofs):
            """
            calculates the potential function
            @param loc_xi: current choice for coefficients
            @param loc_y: measurements
            @param loc_solution: tensor solution object
            @param loc_cov: covariance value
            @param loc_dofs: dofs where the measurements took place
            @return: solution value
            """

            for _xi in loc_xi:
                if _xi < -1 or _xi > 1:
                    return -np.inf
            self.MCMC_RUN += 1
            print("MCMC Run number {0} with coefficients {1} at coordinates {2}".format(self.MCMC_RUN, loc_xi,
                                                                                        loc_dofs))
            sol_buffer = loc_solution.compute_direct_sample_solution(loc_xi)
            sol_measurements = np.array([sol_buffer(loc_dof) for loc_dof in loc_dofs])
            print("     result: {0}".format(sol_measurements))
            diff = np.array(loc_y) - np.array(sol_measurements)
            potential = -0.5 * np.sum(diff ** 2 * (loc_cov ** (-1)))
            # mean = calculate_mean(solution)
            # return np.log(1/mean * np.exp(potential))
            return 0.5 + potential

        dimension = len(self.true_values)
        if self.USE_DOFS:
            #                                       # This works only if the dofs REALLY belong to the current forward
            #                                       # solution. Otherwise the mesh will be different
            d = self.forwardOp.SOL[0]["V"].mesh.geometry().dim()
            di_dx = self.forwardOp.SOL[0]["V"].V.dofmap().tabulate_all_coordinates(
                self.forwardOp.SOL[0]["V"].mesh).reshape((-1, d))
            loc_dofs = np.array([di_dx[dof] for dof in self.dofs])
        else:
            loc_dofs = self.sample_coordinates

        p0 = np.random.rand(dimension * self.chain_walker).reshape((self.chain_walker, dimension))
        self.sampler = emcee.EnsembleSampler(self.chain_walker, dimension, lnprob,
                                             args=[self.measurements, self.forwardOp, self.covariance, loc_dofs])

        # test = lnprob(sample_data_list, true_value_data_list, solution, 0.001, dof_list)
        print "run burn in"
        pos, prob, state = self.sampler.run_mcmc(p0, self.burn_in)
        print "run mcmc"
        self.sampler.run_mcmc(pos, self.mc_steps)

    # endregion

    # region Function Calculate samples
    def calculate_samples(self):
        """
        starts the MCMC procedure and creates a list of samples, distributed according to the posterior measure
        uses the asgfem solution as a surrogate model
        """
        # TODO: check validity of all needed elements

        def lnprob(loc_xi, loc_y, loc_solution, loc_cov, loc_dofs):
            """
            calculates the potential function
            @param loc_xi: current choice for coefficients
            @param loc_y: measurements
            @param loc_solution: tensor solution object
            @param loc_cov: covariance value
            @param loc_dofs: dofs where the measurements took place
            @return: solution value
            """

            for _xi in loc_xi:
                if _xi < -1 or _xi > 1:
                    return -np.inf
            self.MCMC_RUN += 1
            print("MCMC Run number {0} with coefficients {1} at coordinates {2}".format(self.MCMC_RUN, loc_xi,
                                                                                        loc_dofs))
            if self.USE_DOFS:
                sol_measurements = create_noisy_measurement_data_dofs(loc_solution, list(loc_xi), loc_dofs, 0.0)
            else:
                sol_measurements = self.forwardOp.sample_at_cord_and_coeff(list(loc_xi), loc_dofs)

                # sol_measurements = np.array([sol_measurement + np.random.randn() * loc_cov
                #                             for sol_measurement in sol_measurements])
            diff = np.array(loc_y) - np.array(sol_measurements)
            potential = -0.5 * np.sum(diff ** 2 * (loc_cov ** (-1)))
            # mean = calculate_mean(solution)
            # return np.log(1/mean * np.exp(potential))
            return 0.5 + potential

        dimension = len(self.true_values)

        if self.USE_DOFS:
            loc_dofs = self.dofs
        else:
            loc_dofs = self.sample_coordinates

        p0 = np.random.rand(dimension * self.chain_walker).reshape((self.chain_walker, dimension))
        self.sampler = emcee.EnsembleSampler(self.chain_walker, dimension, lnprob,
                                             args=[self.measurements, self.solution, self.covariance, loc_dofs])
        # test = lnprob(sample_data_list, true_value_data_list, solution, 0.001, dof_list)
        print "run burn in"
        pos, prob, state = self.sampler.run_mcmc(p0, self.burn_in)
        print "run mcmc"
        self.sampler.run_mcmc(pos, self.mc_steps)
    # endregion

    # region overwrites Function: Import Bayes Object

    def import_bayes(self, import_path):
        """
        creates an instance of the current object using the information from the given file
        """
        try:
            infile = open(import_path + str(hash(self.file_name)) + '.dat', 'rb')
            (sampler, inf) = Pickle.load(infile)
            self.sampler = emcee.EnsembleSampler(self.chain_walker, self.forwardOp.coefficients, None)
            self.sampler._chain = sampler["chain"]
            self.sampler._flatchain = sampler["flatchain"]
            if self.USE_DOFS:
                self.dofs = inf["dofs"]
            else:
                self.sample_coordinates = inf["sample_coordinates"]
            self.measurements = inf["measurements"]
            self.true_values = inf["true_values"]
            self.covariance = inf["covariance"]
            self.chain_walker = inf["chain_walker"]
            self.burn_in = inf["burn_in"]
            self.mc_steps = inf["mc_steps"]
            infile.close()
        except IOError as ex:
            self.log.error("can not import from file " + import_path + str(hash(self.file_name)) +
                           " err: " + ex.message)
            return False
        except Exception as ex:
            self.log.error("can not assign value or anything else is wrong: " + ex.message)
            return False
        return True

    # endregion

    # region overwrites Function: Export Bayes Object

    def export_bayes(self, export_path):
        """
        exports all the important information of the bayesian procedure by pickling it to a file
        @param export_path: path to export file
        @return: success
        """
        # TODO: check validity
        # TODO: pickle coeff_field

        inf = {"dofs": self.dofs, "sample_coordinates": self.sample_coordinates, "measurements": self.measurements,
               "true_values": self.true_values, "covariance": self.covariance, "chain_walker": self.chain_walker,
               "burn_in": self.burn_in, "mc_steps": self.mc_steps}
        inf_str = "This file is only for informational purpose and contains settings for \n " \
                  + str(hash(self.file_name)) + ".dat \n"
        inf_str += "Forward Operator: \n"
        inf_str += self.forwardOp.filename + " = " + str(hash(self.forwardOp.filename)) + "\n"
        if self.USE_DOFS:
            inf_str += "dofs:\n"
            for lia in range(len(self.dofs)):
                inf_str += "    {} : {:2.4f} \n".format(lia, self.dofs[lia])
        else:
            inf_str += "sample_coordinates:\n"
            for lia in range(len(self.sample_coordinates)):
                inf_str += "    {} : {} \n".format(lia, self.sample_coordinates[lia])
        inf_str += "measurements:\n"
        for lia in range(len(self.measurements)):
            inf_str += "    {} : {:2.4f} \n".format(lia, self.measurements[lia])
        inf_str += "true_values:\n"
        for lia in range(len(self.true_values)):
            inf_str += "    {} : {:2.4f} \n".format(lia, self.true_values[lia])
        inf_str += "covariance:{}\n".format(self.covariance)
        inf_str += "chain_walker:{}\n".format(self.chain_walker)
        inf_str += "burn_in:{}\n".format(self.burn_in)
        inf_str += "mc_steps{}\n".format(self.mc_steps)
        sampler = {"chain": self.sampler.chain, "flatchain": self.sampler.flatchain}
        try:
            outfile = open(export_path + str(hash(self.file_name)) + '.dat', 'wb')
            Pickle.dump((sampler, inf), outfile, Pickle.HIGHEST_PROTOCOL)
            outfile.close()
            outfile = open(export_path + str(hash(self.file_name)) + '.conf', 'wb')
            outfile.write(inf_str)
            outfile.close()
        except IOError as ex:
            self.log.error("can not export file to " + export_path + str(hash(self.file_name)) +
                           "  err: " + str(ex.strerror))
            return False
        return True

    # endregion

    # region overwrites Function create forward operator
    def create_forward_operator(self, iterations=1000, refinements=10, max_dofs=1e6, thetax=0.5, thetay=0.5, srank=2,
                                maxrank=15, urank=1,
                                updater=2, m=10, femdegree=1, gpcdegree=1, polysys="L", decayexp=2, gamma=0.9,
                                rvtype="uniform",
                                acc=1e-14, do_timing=True, domain="square", mesh_refine=1, mesh_dofs=-1, problem=0,
                                convergence=1e-12, max_new_dim=1000, no_longtail_zeta_marking=False,
                                resnorm_zeta_weight=1,
                                rank_always=False, eta_zeta_weight=0.1, coeffield_type="EF-square-cos",
                                amp_type="constant"):
        """
        creates the forward solution operator from a given coefficient field
        """

        op = AsgFemOperator()
        op.create_forward_operator(iterations=iterations, refinements=refinements, max_dofs=max_dofs, thetax=thetax,
                                   thetay=thetay, srank=srank, maxrank=maxrank, urank=urank, updater=updater, m=m,
                                   femdegree=femdegree, gpcdegree=gpcdegree, polysys=polysys, decayexp=decayexp,
                                   gamma=gamma, rvtype=rvtype, acc=acc, do_timing=do_timing, domain=domain,
                                   mesh_refine=mesh_refine, mesh_dofs=mesh_dofs, problem=problem,
                                   convergence=convergence, max_new_dim=max_new_dim,
                                   no_longtail_zeta_marking=no_longtail_zeta_marking,
                                   resnorm_zeta_weight=resnorm_zeta_weight, rank_always=rank_always,
                                   eta_zeta_weight=eta_zeta_weight, coeffield_type=coeffield_type,
                                   amp_type=amp_type, _print=False)
        self.forwardOp = op
        self.solution = op.SOL[-1]["V"]
    # endregion

    # region static Function estimate Karhunen-Loeve
    @staticmethod
    def kl(coeff_field, x, y):
        """
        calculates the Karhunen-Loeve expansion of the given coefficient field
        """
        return coeff_field.funcs(0)(x) + np.sum([coeff_field.funcs(m + 1)(x) * yi
                                                 for m, yi in enumerate(ensure_iterable(y))])
    # endregion

    # region overwrites Function Calculate Marginal Densities
    def calculate_marginal_densities(self, solution, eval_densities, stoch_grid, integration_step_size,
                                     eval_grid_points):
        """
        impossible
        @param solution:
        @param eval_densities:
        @param stoch_grid:
        @param integration_step_size:
        @param eval_grid_points:
        @return:
        """
        raise NotImplementedError
    # endregion

    # region overwrites Function Apply inner product
    def apply_inner_product(self, solution, gamma, precision, rank):
        """
        not needed
        @param solution:
        @param gamma:
        @param precision:
        @param rank:
        @return:
        """
        raise NotImplementedError
    # endregion

    # region overwrites Function Calculate Measurement difference
    def calculate_measurement_diff(self, solution, value_list):
        """
        not needed
        @param solution:
        @param value_list:
        @return:
        """
        raise NotImplementedError
    # endregion

    # region overwrites Function Apply Observation
    def apply_observation(self):
        """
        not needed
        @return:
        """
        raise NotImplementedError
    # endregion

    # region Property File Name
    @property
    def file_name(self):
        """
        returns the file name of the current object
        """
        file_name = "ASGFEM_MCMC_"
        if self.USE_DOFS:
            file_name += "dofs%i_" % len(self.dofs)
        else:
            file_name += "sC%i_" % len(self.sample_coordinates)
        # file_name += "M%i_" % len(self.true_values)
        file_name += "iG%.2e_" % self.covariance
        file_name += "MCMC%i_" % self.chain_walker
        file_name += "burnIn%i_" % self.burn_in
        file_name += "mcSteps%i_" % self.mc_steps
        file_name += self.forwardOp.filename
        return str(file_name)
    # endregion

    # region Function Get True Coefficient Field
    def get_true_coefficient_field(self):
        """
          creates a Fenics vector of the coefficient field from the true values given for the measurements
        @return: FEniCSVector
        """
        return get_coefficient_field_realisation(self.true_values,
                                                 self.forwardOp.get_coefficient_field,
                                                 len(self.true_values), self.forwardOp.get_proj_basis, log_normal=False)
    # endregion

    # region Function Get Parametric Coefficient Field
    def get_parametric_coefficient_field(self, coefficients):
        """
        creates the coefficient field from the given parameters
        @param coefficients: list
        @return: FEniCSVector
        """
        return get_coefficient_field_realisation(coefficients,
                                                 self.forwardOp.get_coefficient_field, len(coefficients),
                                                 self.forwardOp.get_proj_basis, log_normal=False)
    # endregion