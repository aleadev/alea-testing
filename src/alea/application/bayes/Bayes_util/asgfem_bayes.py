# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  Simple class for Bayesian objects using the asgfem forward operator
"""
# region Import
from __future__ import division
from alea.application.bayes.Bayes_util.lib.bayes_lib import ensure_iterable, sample_in_stoch_space
from alea.application.bayes.Bayes_util.lib.bayes_sgfem_als import get_coefficient_field_realisation

from alea.application.bayes.Bayes_util.ForwardOperator.asgfem_operator import AsgFemOperator
from alea.application.bayes.Bayes_util.Interface.IBayes import IBayes
from alea.application.bayes.Bayes_util.lib.bayes_lib_simple import apply_observation, integrate_ydims_tt

import cPickle as Pickle
import numpy as np
from scipy.interpolate import interp1d
from dolfin import Function
from alea.fem.fenics.fenics_vector import FEniCSVector
import tt
from alea.utils.tictoc import TicToc
# endregion


class ASGFEMBayes(IBayes):
    """
      Simple Bayesian inversion object
    """
    USE_DOFS = False

    # region Constructor
    def __init__(self, solution, dofs, sample_coordinates, measurements, true_values,  obs_precision, obs_rank,
                 inp_precision, inp_rank, inp_gamma, exp_method, global_exp_precision, eval_grid_points,
                 prior_densities, eval_densities, mean, stoch_grid):
        """
          constructor method using all objects needed for bayesian inversion
        @param solution: solution object of the forward operator
        @param measurements: measured values. z = (O o G)(u) + eta
        @param dofs: degrees of freedom to sample forward solution at
        @param eval_grid_points: 1D stoch domain grid to interpolate densities at
        @param eval_densities: list of densities for every marginal estimation
        @return:
        """
        self.solution = solution
        self.dofs = dofs
        self.sample_coordinates = sample_coordinates
        self.measurements = measurements
        self.true_values = true_values
        self.obs_precision = obs_precision
        self.obs_rank = obs_rank
        self.inp_precision = inp_precision
        self.inp_rank = inp_rank
        self.inp_gamma = inp_gamma
        self.exp_method = exp_method
        self.exp_method.global_precision = global_exp_precision
        self.global_exp_precision = global_exp_precision
        self.eval_grid_points = eval_grid_points
        self.prior_densities = prior_densities
        self.eval_densities = eval_densities
        self.mean = mean
        self.stoch_grid = stoch_grid
        self.forwardOp = AsgFemOperator()
        self.solution = 0
        self.sol_index = -1
        self.error = 0
    # endregion

    # region Function: init_solver
    @classmethod
    def init_solver(cls, dofs, sample_coordinates, true_values, obs_precision, obs_rank, inp_precision,
                    inp_rank, inp_gamma, exp_method, global_exp_precision, eval_grid_points,
                    prior_densities, stoch_grid, iterations=1000, refinements=10, max_dofs=1e6, thetax=0.5, thetay=0.5,
                    srank=2, maxrank=15, urank=1,
                    updater=2, m=10, femdegree=1, gpcdegree=1, polysys="L", decayexp=2, gamma=0.9,
                    rvtype="uniform",
                    acc=1e-14, do_timing=False, domain="square", mesh_refine=1, mesh_dofs=-1, problem=0,
                    convergence=1e-12, max_new_dim=1000, no_longtail_zeta_marking=False,
                    resnorm_zeta_weight=1,
                    rank_always=False, eta_zeta_weight=0.1, coeffield_type="EF-square-cos",
                    amp_type="constant", sol_index=-1, field_mean=1.0
                    ):
        """
        initializes the Bayesian object with everything needed for the calculation process
        """
        obj = cls(0, dofs, sample_coordinates, [], true_values, obs_precision, obs_rank, inp_precision, inp_rank,
                  inp_gamma, exp_method, global_exp_precision, eval_grid_points, prior_densities, [], 0, stoch_grid)
        obj.sol_index = sol_index
        obj.create_forward_operator(iterations=iterations, refinements=refinements, max_dofs=max_dofs, thetax=thetax,
                                    thetay=thetay, srank=srank, maxrank=maxrank, urank=urank, updater=updater, m=m,
                                    femdegree=femdegree, gpcdegree=gpcdegree, polysys=polysys, decayexp=decayexp,
                                    gamma=gamma, rvtype=rvtype, acc=acc, do_timing=do_timing, domain=domain,
                                    mesh_refine=mesh_refine, mesh_dofs=mesh_dofs, problem=problem,
                                    convergence=convergence, max_new_dim=max_new_dim,
                                    no_longtail_zeta_marking=no_longtail_zeta_marking,
                                    resnorm_zeta_weight=resnorm_zeta_weight, rank_always=rank_always,
                                    eta_zeta_weight=eta_zeta_weight, coeffield_type=coeffield_type,
                                    amp_type=amp_type, sol_index=sol_index, field_mean=field_mean)

        return obj
    # endregion

    # region overwrites Function Create Coefficient Field
    def create_coeff_field(self, funcs, mean):
        """
        not needed for asgfem operator
        """
    # endregion

    # region Function calculate densities
    def calculate_densities(self, stop_at_potential=False, stop_at_observation=False, stop_at_collocation=False):
        """
        starts the bayesian procedure and calculates the marginal densities
        :param stop_at_potential: switch to indicate that we only calculate the potential.
                                  Saves the time to calculate the exponential method.
        :type stop_at_potential: bool
        """
        # TODO: check validity of all needed elements
        self.log.info("Create Tensor")

        print("maximal ranks: {}, {}".format(self.obs_rank, self.inp_rank))
        print("given gpcps {}".format(self.forwardOp.SOL[-1]["gpcps"][self.sol_index]))
        self.solution = sample_in_stoch_space(self.solution, self.forwardOp.SOL[-1]["gpcps"][self.sol_index],
                                              len(self.stoch_grid))
        print("Collocated tensor has dimension: {}".format(self.solution.n))
        print("Collocated tensor has ranks: {}".format(self.solution.r))
        if stop_at_collocation:
            return
        # solution = create_tensor_from_solution(a, mx, [my*(lia+1-lia) for lia in range(len(true_y))])
        self.log.info("Apply Observation")
        # print "observation nodes: {}".format(self.sample_coordinates)
        # print "pre-observation: {}".format(self.solution)
        self.apply_observation()                    # modifies self.solution from tt.vector to list(Of tt.vector)
        if stop_at_observation:
            return
        print("Observed tensor has dimension: {}".format(self.solution[-1].n))
        print("Observed tensor has ranks: {}".format(self.solution[-1].r))
        # print "post-observation: {}".format(self.solution)
        # self.calculate_measurement_diff(self.solution, self.measurements)

        self.log.info("Apply Inner Product")
        self.apply_inner_product(self.solution, self.inp_gamma, self.inp_precision, self.inp_rank)
        if stop_at_potential:
            return
        print("IP tensor has dimension: {}".format(self.solution.n))
        print("IP tensor has ranks: {}".format(self.solution.r))
        self.exp_method.start_rank = int(np.max(self.solution.r))
        self.exp_method.calculate_exponential_tt_precision(self.solution, self.global_exp_precision)
        self.solution = self.exp_method.solution
        print("exponential tensor has dimension: {}".format(self.solution.n))
        print("exponential tensor has ranks: {}".format(self.solution.r))
        #                                           # create suitable format and remove redundancies
        self.solution = tt.vector.round(self.solution, 0, rmax=100000)
        print("final Bayes tensor has dimension: {}".format(self.solution.n))
        print("final Bayes tensor has ranks: {}".format(self.solution.r))

        self.log.info("calculate marginal densities")
        self.calculate_marginal_densities(self.exp_method.solution, self.prior_densities,
                                          self.stoch_grid, 1./10,
                                          self.eval_grid_points)
    # endregion

    # region overwrites Function: Import Bayes Object

    def import_bayes(self, import_path):
        """
        creates an instance of the current object using the information from the given file
        """
        try:
            infile = open(import_path + str(hash(self.file_name)) + '.dat', 'rb')
            (self.solution, inf) = Pickle.load(infile)
            self.dofs = inf["dofs"]
            self.sample_coordinates = inf["sample_coordinates"]
            self.measurements = inf["measurements"]
            self.true_values = inf["true_values"]
            self.obs_precision = inf["obs_precision"]
            self.obs_rank = inf["obs_rank"]
            self.inp_precision = inf["inp_precision"]
            self.inp_rank = inf["inp_rank"]
            self.inp_gamma = inf["inp_gamma"]
            self.exp_method = inf["exp_method"]
            self.global_exp_precision = inf["global_exp_precision"]
            self.eval_grid_points = inf["eval_grid_points"]
            self.eval_densities = inf["eval_densities"]
            self.mean = inf["mean"]
            infile.close()
        except IOError as ex:
            self.log.error("can not import from file " + import_path + str(hash(self.file_name)) +
                           " err: " + ex.message)
            return False
        except Exception as ex:
            self.log.error("can not assign value or anything else is wrong: " + ex.message)
            return False
        return True

    # endregion

    # region overwrites Function: Export Bayes Object

    def export_bayes(self, export_path):
        """
        exports all the important information of the bayesian procedure by pickling it to a file
        @param export_path: path to export file
        @return: success
        """
        # TODO: check validity
        # TODO: pickle coeff_field

        inf = {"dofs": self.dofs, "sample_coordinates": self.sample_coordinates, "measurements": self.measurements,
               "true_values": self.true_values, "obs_precision": self.obs_precision, "obs_rank": self.obs_rank,
               "inp_precision": self.inp_precision, "inp_rank": self.inp_rank, "inp_gamma": self.inp_gamma,
               "exp_method": self.exp_method, "global_exp_precision": self.global_exp_precision,
               "eval_grid_points": self.eval_grid_points, "eval_densities": self.eval_densities,
               "mean": self.mean}
        inf_str = "This is just a file for informational purpose for the ASGFEM Bayes Object: \n"
        inf_str += "    " + self.file_name + " = " + str(hash(self.file_name)) + "\n"
        inf_str += "Forward Operator: \n"
        inf_str += self.forwardOp.filename + " = " + str(hash(self.forwardOp.filename)) + "\n"
        inf_str += "dofs\n"
        for lia in range(len(self.dofs)):
            inf_str += "    {} : {:2.4f} \n".format(lia, self.dofs[lia])
        inf_str += "sample_coordinates: \n"
        for lia in range(len(self.sample_coordinates)):
            inf_str += "    {} : {} \n".format(lia, self.sample_coordinates[lia])
        inf_str += "measurements\n"
        for lia in range(len(self.measurements)):
            inf_str += "    {} : {:2.4f}\n".format(lia, self.measurements[lia])

        inf_str += "true values\n"
        for lia in range(len(self.true_values)):
            inf_str += "    {} : {:2.4f}\n".format(lia, self.true_values[lia])

        inf_str += "obs_precision: {}\n".format(self.obs_precision)
        inf_str += "obs_rank: {}\n".format(self.obs_rank)
        inf_str += "inp_precision: {}\n".format(self.inp_precision)
        inf_str += "inp_rank: {}\n".format(self.inp_rank)
        inf_str += "inp_gamma: {}\n".format(self.inp_gamma)
        inf_str += "exp_method: {}\n".format(str(self.exp_method))
        inf_str += "global_exp_precision: {}\n".format(self.global_exp_precision)
        inf_str += "eval_grid_points\n"
        for lia in range(len(self.eval_grid_points)):
            inf_str += "    {} : {:2.4f}\n".format(lia, self.eval_grid_points[lia])
        inf_str += "eval_densities not here\n"
        inf_str += "mean: {}\n".format(self.mean)
        inf_str += "stoch_grid\n"
        for lia in range(len(self.stoch_grid)):
            inf_str += "    {} : {:2.4f}\n".format(lia, self.stoch_grid[lia])
        try:
            outfile = open(export_path + str(hash(self.file_name)) + '.dat', 'wb')
            Pickle.dump((self.solution, inf), outfile, Pickle.HIGHEST_PROTOCOL)
            outfile.close()
            outfile = open(export_path + str(hash(self.file_name)) + '.conf', 'wb')
            outfile.write(inf_str)
            outfile.close()
        except IOError as ex:
            self.log.error("can not export file to " + export_path + str(hash(self.file_name)) +
                           "  err: " + str(ex.strerror))
            return False
        return True

    # endregion

    # region overwrites Function Create Forward Operator
    def create_forward_operator(self, iterations=1000, refinements=10, max_dofs=1e6, thetax=0.5, thetay=0.5, srank=2,
                                maxrank=15, urank=1,
                                updater=2, m=10, femdegree=1, gpcdegree=1, polysys="L", decayexp=2, gamma=0.9,
                                rvtype="uniform",
                                acc=1e-14, do_timing=True, domain="square", mesh_refine=1, mesh_dofs=-1, problem=0,
                                convergence=1e-12, max_new_dim=1000, no_longtail_zeta_marking=False,
                                resnorm_zeta_weight=1,
                                rank_always=False, eta_zeta_weight=0.1, coeffield_type="EF-square-cos",
                                amp_type="constant", sol_index=-1, field_mean=1.0):
        """
        creates the forward solution operator from a given coefficient field
        """
        print("ASGFEM Bayes M: {}".format(m))
        op = AsgFemOperator()
        op.create_forward_operator(iterations=iterations, refinements=refinements, max_dofs=max_dofs, thetax=thetax,
                                   thetay=thetay, srank=srank, maxrank=maxrank, urank=urank, updater=updater, m=m,
                                   femdegree=femdegree, gpcdegree=gpcdegree, polysys=polysys, decayexp=decayexp,
                                   gamma=gamma, rvtype=rvtype, acc=acc, do_timing=do_timing, domain=domain,
                                   mesh_refine=mesh_refine, mesh_dofs=mesh_dofs, problem=problem,
                                   convergence=convergence, max_new_dim=max_new_dim,
                                   no_longtail_zeta_marking=no_longtail_zeta_marking,
                                   resnorm_zeta_weight=resnorm_zeta_weight, rank_always=rank_always,
                                   eta_zeta_weight=eta_zeta_weight, coeffield_type=coeffield_type,
                                   amp_type=amp_type, field_mean=field_mean, _print=False)
        self.forwardOp = op

        self.solution = op.SOL[self.sol_index]["V"]
    # endregion

    def set_sol_index(self, sol_index):
        self.sol_index = sol_index
        self.solution = self.forwardOp.SOL[sol_index]["V"]

    # region overwrites Function Apply Observation
    def apply_observation(self):
        """
        applies the observation operator at given dofs and round eventually to given precision and rank
        """
        assert isinstance(self.solution, tt.vector)
        if self.USE_DOFS:
            self.solution = apply_observation(self.solution, self.dofs, _precision=self.obs_precision,
                                              _maxrank=self.obs_rank, _print=False)
        else:
            retval = []                             # define return value
            #                                       # list of components from tensor
            list_of_components = tt.vector.to_list(self.solution)
            #                                       # loop through dofs
            for lia in range(len(self.sample_coordinates)):
                if lia % 10 == 0 and lia > 0:
                    TicToc.sortedTimes()
                buffer_list = []                    # list to store current tensor inside
                with TicToc(key="Observation: create zero core of size {}".format(list_of_components[0].shape[2]),
                            active=True, do_print=False):
                    #                                   # initialize tensor component with correct size
                    buffer_comp = np.zeros((1, 1, list_of_components[0].shape[2]))

                #                                       # create FEM solution from first component
                with TicToc(key="Observation: Project solution on FS and solve at measurement point", active=True,
                            do_print=False):
                    for lic in range(list_of_components[0].shape[2]):
                        sample_sol = Function(self.forwardOp.SOL[self.sol_index]["CG"])
                        sample_sol.vector()[:] = list_of_components[0][0, :, lic]
                        # vec = self.forwardOp.get_proj_basis(sol_index=self.sol_index).project_onto(FEniCSVector(sample_sol))._fefunc
                        buffer_comp[0, 0, lic] = sample_sol(self.sample_coordinates[lia])

                buffer_list.append(buffer_comp)     # add to buffer list
                with TicToc(key="Observation: round tensor", active=True, do_print=False):
                    #                               # store the remaining components in the buffer list
                    for lic in range(1, len(list_of_components)):
                        buffer_list.append(list_of_components[lic])
                    #                               # create tensor from list
                    buffer_ten = tt.vector.from_list(buffer_list)
                    #                               # round if necessary
                    if max(buffer_ten.r) > self.obs_rank and False:
                        print('round tensor')
                        buffer_ten = tt.vector.round(buffer_ten, self.obs_precision, self.obs_rank)
                #                                   # add current tensor to return value list
                retval.append(buffer_ten)
            self.solution = retval
    # endregion

    # region overwrites Function Create Measurement difference
    def calculate_measurement_diff(self, solution, value_list):
        """
        calculates the left side of the inner product
        """
        from alea.application.bayes.Bayes_util.lib.bayes_lib_simple import create_rank_one
        for lia in range(len(solution)):
           value = create_rank_one(value_list[lia], solution[lia].n)
           solution[lia] = value - solution[lia]
        self.solution = solution
    # endregion

    # region overwrites Function Apply Inner Product
    def apply_inner_product(self, solution, gamma, precision, rank):
        """
        calculates the misfit function with given variance as gamma*I and rounds result
        """
        # self.solution = -0.5 * apply_inner_product(solution, _gamma=gamma, _precision=precision, _maxrank=rank,
        #                                           _print=False)
        self.calculate_measurement_diff(self.solution, self.measurements)
        from alea.application.bayes.Bayes_util.lib.bayes_lib_simple import apply_inner_product
        self.solution = -0.5 * apply_inner_product(solution, gamma, _round=True,_precision=precision, _maxrank=rank)
        # from Bayes_util.lib.bayes_lib import apply_bayes_inner_product_new
        # self.solution = -0.5 * apply_bayes_inner_product_new(solution, self.measurements, gamma, precision, rank,
        #                                                      _print=False, _round=True)

    # endregion

    # region overwrites Function Calculate Marginal Densities
    def calculate_marginal_densities(self, solution, eval_densities, _, integration_step_size,
                                     eval_grid_points):
        """
        calculates the interpolated marginal densities
        """
        from alea.application.bayes.Bayes_util.lib.bayes_lib import evaluate
        self.eval_densities = []
        #self.mean = evaluate(solution, stoch_grid, _poly="Legendre", _samples=[0]*(len(solution.n))) * \
        #            (0.5**(len(solution.n)-1))
        from alea.application.bayes.Bayes_util.lib.bayes_lib import evaluate_marginal
        self.mean = evaluate_marginal(solution, 0, _samples=[0]*(len(solution.n)-1))*(0.5**(2*(len(solution.n)-1)))

        print("mean={}".format(self.mean))
        for k in range(len(solution.n)-1):
            eval_list = []
            for lia in range(len(eval_grid_points)):
                evaluation = evaluate_marginal(solution, k+1, _samples=[0]+[0]*k+[eval_grid_points[lia]]+[0]*(len(solution.n)-1-k))
                eval_list.append(evaluation)
                # print "eval solution at {}".format([0]+[0]*k+[lia]+[0]*(len(solution.n)-2-k))
            eval_list = np.array(eval_list)
            eval_list *= self.mean**(-1)
            print("len solution.n: {}".format(len(solution.n)))
            eval_list *= 0.5**(2*(len(solution.n)-2))           # !!! the 2 in exponent is due to the transf_norms
            #                                       # and their norming on [-1, 1]

            self.eval_densities.append(eval_list)
            # print self.eval_densities[-1]
            #for l in range(len(eval_grid_points)):
            #    eval_list.append(evaluate(solution, stoch_grid, _poly="Legendre",
            #                              _samples=[0]+[0]*k+[eval_grid_points[l]]+[0]*(len(solution.n)-1-k)))
            #self.eval_densities.append((((0.5)**(len(solution.n)-2))*np.array(eval_list)*(self.mean**(-1))))
    # endregion

    # region overwrites Function Calculate Marginal Densities
    def calculate_marginal_densities_old(self, solution, eval_densities, stoch_grid, integration_step_size,
                                         eval_grid_points):
        """
        calculates the interpolated marginal densities
        """
        y_int, self.mean = integrate_ydims_tt(solution, eval_densities, integration_step_size, stoch_grid)
        print y_int
        self.eval_densities = [IBayes.extrap1d(interp1d(stoch_grid, y_int[:, k], kind='cubic'))(eval_grid_points)
                               for k in range(len(solution.n)-1)]

        # self.eval_densities = [np.sum(np.array([evaluate_lagrange(eval_grid_points, stoch_grid, j) * y_int[j, k]
        #                       for j in range(len(stoch_grid))]), axis=0) for k in range(len(solution.n)-1)]
    # endregion

    # region static Function evaluate Karhunen-Loeve expansion
    @staticmethod
    def kl(coeff_field, x, y):
        """
        calculates the Karhunen-Loeve expansion of the given coefficient field
        @return Double
        """
        return coeff_field.funcs(0)(x) + np.sum([coeff_field.funcs(m + 1)(x) * yi
                                                 for m, yi in enumerate(ensure_iterable(y))])
    # endregion

    # region Property File Name
    @property
    def file_name(self):
        """
        returns the file name of the current object
        @return String
        """
        file_name = "direct_"
        if self.USE_DOFS:
            file_name += "dofs%i_" % len(self.dofs)
        else:
            file_name += "sC%i_" % len(self.sample_coordinates)
        # file_name += "M%i_" % len(self.true_values)
        file_name += "oP%.2e_" % self.obs_precision
        file_name += "oR%i_" % self.obs_rank
        file_name += "iP%.2e_" % self.inp_precision
        file_name += "iR%i_" % self.inp_rank
        file_name += "iG%.2e_" % self.inp_gamma
        file_name += str(self.exp_method) + "_"
        file_name += "den%i_" % len(self.eval_grid_points)
        file_name += "sGrid{}".format(len(self.stoch_grid))
        file_name += "solInd{}".format(self.sol_index)
        file_name += self.forwardOp.filename
        return str(file_name)
    # endregion

    # region Function Get True Coefficient Field
    def get_true_coefficient_field(self):
        """
          creates a Fenics vector of the coefficient field from the true values given for the measurements
        @return: FEniCSVector
        """
        return get_coefficient_field_realisation(self.true_values,
                                                 self.forwardOp.get_coefficient_field,
                                                 len(self.forwardOp.SOL[self.sol_index]["V"].n)-1,
                                                 self.forwardOp.get_proj_basis(sol_index=self.sol_index),
                                                 log_normal=False)
    # endregion

    # region Function Get Parametric Coefficient Field
    def get_parametric_coefficient_field(self, coefficients):
        """
        creates the coefficient field from the given parameters
        @param coefficients: list
        @return: FEniCSVector
        """
        return get_coefficient_field_realisation(coefficients,
                                                 self.forwardOp.get_coefficient_field, len(coefficients),
                                                 self.forwardOp.get_proj_basis(sol_index=self.sol_index),
                                                 log_normal=False)
    # endregion

    # region Function Sample the potential at Coefficients
    def sample_potential_old(self, rv_samples):
        """
        samples the potential tensor at given coefficients and returns the value
        @param rv_samples: list of parameters in stochastic space
        @type rv_samples: list
        @return: value of the potential at the given coefficients
        @rtype: float
        """

        if not isinstance(rv_samples, list):
            self.log.error('sample_potential: rv_samples is not a list')
            raise TypeError('sample_potential: rv_samples is not a list')
        if not isinstance(self.solution, tt.vector):
            self.log.error('sample_potential: solution is not a tensor -> calculate euler first')
            raise TypeError('sample_potential: solution is not a tensor -> calculate euler first')
        from alea.application.bayes.Bayes_util.lib .bayes_lib import interpolate_in_stoch_space
        self.solution = interpolate_in_stoch_space(self.solution, list(self.stoch_grid))
        from alea.application.bayes.Bayes_util.lib.interpolation_util import evaluate_lagrange
        #                                           # get list of tensor representation
        list_of_components = tt.vector.to_list(self.solution)
        #                                           # first tt component has to be a row or one-dim for every x and 0
        buff = list_of_components[0][0, :, :]       # get first component tensor = rank 2 tensor = matrix
        #                                           # enumerate through every component
        for comp_index in range(len(list_of_components)):

            if comp_index == 0:
                continue
            comp_value = list_of_components[comp_index]
            #                                       # reached last component
            if comp_index == len(list_of_components) - 1:

                #                                   # evaluate polynomials first
                buff_3 = np.array([comp_value[:, mu, 0] * evaluate_lagrange(rv_samples[comp_index - 1],
                                                                            self.stoch_grid, mu)  # / norms[mu]
                                   for mu in range(self.solution.n[comp_index])])
                #                                   # evaluate sum over all polynomials
                buff_2 = np.sum(buff_3, axis=0)
                #                                   # multiply with previous row
                buff = np.dot(buff, buff_2)
                continue                            # continue for loop, or we could quit it here
            buff = np.dot(buff, np.sum(
                          np.array([comp_value[:, mu, :] * evaluate_lagrange(rv_samples[comp_index - 1],
                                                                             self.stoch_grid, mu)  # / norms[mu]
                                   for mu in range(self.solution.n[comp_index])]), axis=0)
                          )                         # as above but as a one liner

        return buff

    # endregion

    # region Function Sample the potential at Coefficients
    def sample_potential(self, rv_samples):
        """
        samples the potential tensor at given coefficients and returns the value
        @param rv_samples: list of parameters in stochastic space
        @type rv_samples: list
        @return: value of the potential at the given coefficients
        @rtype: float
        """

        if not isinstance(rv_samples, list):
            self.log.error('sample_potential: rv_samples is not a list')
            raise TypeError('sample_potential: rv_samples is not a list')
        if not isinstance(self.solution, tt.vector):
            self.log.error('sample_potential: solution is not a tensor -> calculate euler first')
            raise TypeError('sample_potential: solution is not a tensor -> calculate euler first')
        from alea.application.bayes.Bayes_util.lib.bayes_lib import evaluate
        buff = np.array([evaluate(self.solution, _poly="Lagrange", _samples=rv_samples)])
        return buff[0]

    # endregion

    # region Function Sample the already collocated FEM solution
    def sample_potential_no_interpolation(self, rv_samples):
        """
        samples the collocated solution object in stochastic space at given sample index and returns the fenics function
        :param rv_samples: list of stochastic samples
        :return: fenics function
        """
        if not isinstance(rv_samples, list):
            raise TypeError('sample_collocation: rv_samples is not a list')
        if not isinstance(self.solution, tt.vector):
            raise AssertionError("sample_collocation: solution is not a tt")
        if not len(rv_samples) == len(self.solution.n)-1:
            raise AssertionError("sample_collocation: rv_samples has not the correct length "
                                 "{} != {}".format(len(rv_samples), len(self.solution.n)-1))
        for lia in range(len(rv_samples)):
            if not isinstance(rv_samples[lia], int):
                raise TypeError("sample_collocation: sample index ({}) is not an integer".format(rv_samples[lia]))
        # print self.solution
        # print rv_samples
        list_of_components = tt.vector.to_list(self.solution)
        #                                           # first tt component has to be a row or one-dim for every x and 0
        #                                           # get last component tensor = rank 2 tensor = matrix
        buff = list_of_components[-1][:, rv_samples[-1], 0]
        #                                           # enumerate through every component backwards
        for comp_index in range(len(list_of_components)-2, 0, -1):
            # print "index: {}".format(comp_index)
            if not 0 <= rv_samples[comp_index-1] < list_of_components[comp_index].shape[1]:
                raise AssertionError("sample_collocation: sample index ({}) for "
                                     "dimension ({}) is out of range".format(rv_samples[comp_index-1], comp_index))
            buff = np.dot(list_of_components[comp_index][:, rv_samples[comp_index-1], :], buff)
            # print "buff: {}".format(buff.shape)
        buff = np.dot(list_of_components[0][0, :, :], buff)
        return buff
    # endregion

    # region Function Sample the already collocated FEM solution
    def sample_collocation(self, rv_samples):
        """
        samples the collocated solution object in stochastic space at given sample index and returns the fenics function
        :param rv_samples: list of stochastic samples
        :return: fenics function
        """
        if not isinstance(rv_samples, list):
            raise TypeError('sample_collocation: rv_samples is not a list')
        if not isinstance(self.solution, tt.vector):
            raise AssertionError("sample_collocation: solution is not a tt")
        if not len(rv_samples) == len(self.solution.n)-1:
            raise AssertionError("sample_collocation: rv_samples has not the correct length "
                                 "{} != {}".format(len(rv_samples), len(self.solution.n)-2))
        for lia in range(len(rv_samples)):
            if not isinstance(rv_samples[lia], int):
                raise TypeError("sample_collocation: sample index ({}) is not an integer".format(rv_samples[lia]))
        # print self.solution

        list_of_components = tt.vector.to_list(self.solution)
        #                                           # first tt component has to be a row or one-dim for every x and 0
        #                                           # get last component tensor = rank 2 tensor = matrix
        buff = list_of_components[-1][:, rv_samples[-1], 0]
        #                                           # enumerate through every component backwards
        for comp_index in range(len(list_of_components)-2, 0, -1):
            # print "index: {}".format(comp_index)
            if not 0 <= rv_samples[comp_index-1] < list_of_components[comp_index].shape[1]:
                raise AssertionError("sample_collocation: sample index ({}) for "
                                     "dimension ({}) is out of range".format(rv_samples[comp_index-1], comp_index))
            buff = np.dot(list_of_components[comp_index][:, rv_samples[comp_index-1], :], buff)
            # print "buff: {}".format(buff.shape)
        buff = np.dot(list_of_components[0][0, :, :], buff)

        # print "buff: {}".format(buff.shape)
        sample_sol = Function(self.forwardOp.SOL[self.sol_index]["CG"])
        sample_sol.vector()[:] = buff

        return self.forwardOp.get_proj_basis(sol_index=self.sol_index).project_onto(FEniCSVector(sample_sol))
    # endregion

    # region Function estimate sampling Error
    def estimate_posterior_solution_error(self, xi_samples=None, n_xi_samples=100, sol_index=-1, sol_index_ref=-1,
                                          relative=False, z_xi_samples=None, n_z_xi_samples=1000):
        """
        samples the error between the calculated solution object and the true solution from
        'compute_direct_sample_solution' in a Monte-Carlo fashion. Used norms are the L2 and the H1 norms.
        @param xi_samples: list of samples to create the sampling error at
        @type xi_samples: list
        @param sol_index: index of the solution refinement. finest solution at -1
        @type sol_index: int
        @:type sol_index_ref:
        @return: L2-error, H1-error
        @rtype: float, float
        """
        from dolfin import set_log_level, ERROR
        set_log_level(ERROR)
        from joblib import Parallel, delayed                # use parallel computing and function delaying

        if not self.forwardOp.SOL:
            raise TypeError("estimate_sampling_error: Solution object is not given")
        if not xi_samples:
            xi_samples = np.array([np.random.rand(self.forwardOp.coefficients)*2-1 for _ in range(n_xi_samples)])
        if not z_xi_samples:
            z_xi_samples = np.array([np.random.rand(self.forwardOp.coefficients)*2-1 for _ in range(n_z_xi_samples)])
        if not isinstance(xi_samples, list) and not isinstance(xi_samples, np.ndarray):
            raise TypeError("estimate_sampling_error: sample list is not loaded")
        if not isinstance(z_xi_samples, list) and not isinstance(z_xi_samples, np.ndarray):
            raise TypeError("estimate_sampling_error: Z sample list is not loaded")
        if len(xi_samples) < 1:
            raise AssertionError("estimate_sampling_error: to few samples in list")
        if len(z_xi_samples) < 1:
            raise AssertionError("estimate_sampling_error: Z to few samples in list")
        if self.forwardOp.domain != "square":
            raise NotImplementedError
        if sol_index > len(self.forwardOp.SOL):
            raise AssertionError("estimate_sampling_error: sol_index out of range")
        if sol_index_ref > len(self.forwardOp.SOL):
            raise AssertionError("estimate_sampling_error: sol_index_ref out of range")

        # region sample Z

        print("calculate Z with {} samples".format(len(z_xi_samples)))
        # geht
        # z_mc = np.sum(_sample_z_parallel_tt(xi) for xi in z_xi_samples)
        # geht nicht
        global global_sol
        global_sol = self
        z_mc = np.sum(Parallel(50)(delayed(_sample_z_parallel_tt)(xi, sol_index_ref) for xi in z_xi_samples))
        z_mc *= len(z_xi_samples)**(-1)
        z_mc *= 0.5**(len(self.solution.n)-1)
        # endregion

        # region sample density

        print("calculate integrand with {} samples".format(len(xi_samples)))
        # dens_mc = np.sum((_sample_integrand_parallel_tt(xi) for xi in xi_samples), axis=0)
        dens_mc = np.sum(Parallel(50)(delayed(_sample_integrand_parallel_tt)(xi, sol_index_ref) for xi in xi_samples))
        dens_mc *= len(xi_samples)**(-1)
        dens_mc *= 0.5**(len(self.solution.n)-1)
        dens_mc *= z_mc**(-1)
        # endregion
        # region Create posterior mean function from mc result
        mean_mc = self.forwardOp.compute_direct_sample_solution(list([0]*len(xi_samples[0])), sol_index=sol_index_ref)._fefunc
        mean_mc.vector()[:] = dens_mc
        # endregion

        # region calculate posterior mean in TT format

        V = self.solution
        U = self.forwardOp.SOL[sol_index]["V"]
        print U
        print V
        assert isinstance(U, tt.vector)
        assert isinstance(V, tt.vector)
        assert len(U.n) == len(V.n)
        assert V.n[0] == 1

        from alea.application.bayes.Bayes_util.lib.interpolation_util import lagrange_to_legendre, get_cheb_points

        V = tt.vector.to_list(V)
        U = tt.vector.to_list(U)

        mean_left = 0
        mean_right = 0
        scalar = 0
        t_vec = []
        for lia in range(len(U)):
            if lia > 0:
                _nodes = np.array(get_cheb_points([V[lia].shape[1]])[0])
                _transfer_matrix, _transfer_basis, _transfer_norms, N_1 = lagrange_to_legendre(_nodes)
                for lib in range(0, V[lia].shape[0]):
                    for lic in range(0, V[lia].shape[2]):
                        V[lia][lib, :, lic] = np.dot(_transfer_matrix, V[lia][lib, :, lic])
            t = np.min(np.array([U[lia].shape[1], V[lia].shape[1]]))
            t_vec.append(t)
            if lia == 0:
                mean_left = U[lia][0, :, :]
                mean_right = V[lia][0, :, :]
                scalar = 2./(2*t+1)
                continue
            t = np.min(np.array([U[lia].shape[1], V[lia].shape[1]]))
            assert t > 0
            curr_core_left = 0
            curr_core_right = 0
            _nodes = np.array(get_cheb_points([t])[0])
            _transfer_matrix, _transfer_basis, _transfer_norms, N_1 = lagrange_to_legendre(_nodes)
            for lic in range(t):
                if lic == 0:
                    curr_core_left = np.array(U[lia][:, lic, :]) / _transfer_norms[lic]
                    curr_core_right = np.array(V[lia][:, lic, :])
                    continue
                curr_core_left += np.array(U[lia][:, lic, :]) / _transfer_norms[lic]
                curr_core_right += np.array(V[lia][:, lic, :])
            mean_left = np.dot(mean_left, curr_core_left)
            mean_right = np.dot(mean_right, curr_core_right)
            scalar *= 2./(2*t+1)
        mean_left = mean_left[:, 0]
        mean_right = mean_right[0, 0]
        print "t={} -> scalar={}".format(t_vec, scalar)
        mean_vec = mean_left * mean_right# * scalar

        mean_vec *= 0.5**(len(U)-1)
        mean_vec *= self.mean**(-1)
        from alea.utils.plothelper import PlotHelper
        from dolfin import Function
        mean = Function(self.forwardOp.SOL[sol_index]["CG"])

        mean.vector()[:] = mean_vec
        # endregion
        from dolfin.fem.norms import errornorm, norm
        print "mean_vec={} vs mean_mc={} ... factor={}".format(norm(mean), norm(mean_mc), norm(mean)/norm(mean_mc))
        # print "compare solution {} with refSolution {}".format(sol_index, sol_index_ref)
        if relative:
            errl2 = errornorm(mean_mc, mean, norm_type="l2", degree_rise=0)/norm(mean_mc, norm_type="l2")
            errh1 = errornorm(mean_mc, mean, norm_type="H1", degree_rise=0)/norm(mean_mc, norm_type="h1")
        else:
            errl2 = errornorm(mean_mc, mean, norm_type="l2", degree_rise=0)
            errh1 = errornorm(mean_mc, mean, norm_type="H1", degree_rise=0)
        print "errL2={}".format(errl2)
        return errl2, errh1
    # endregion

global_sol = None


def _sample_z_parallel_tt(xi, ref_index):
    # sol_buffer = self.forwardOp.compute_direct_sample_solution(list(xi), sol_index=sol_index_ref)
    # sol_buffer = self.forwardOp.sample_at_coeff(list(xi), sol_index=sol_index_ref)
    sol_buffer = global_sol.forwardOp.sample_at_coeff(list(xi), sol_index=ref_index)
    sol_measurements = np.array([sol_buffer(loc_dof) for loc_dof in global_sol.sample_coordinates])
    # print("     (O o G)(u) at {0} = {1}".format(bayes_obj.sample_coordinates, sol_measurements))
    diff = np.array(global_sol.measurements) - np.array(sol_measurements)
    # print("     given measurements: {}".format(bayes_obj.measurements))
    _Z = np.exp(-0.5 * np.sum((diff ** 2) * (global_sol.inp_gamma ** (-1))))
    return _Z


def _sample_integrand_parallel_tt(xi, ref_index):
    sol_buffer = global_sol.forwardOp.sample_at_coeff(list(xi), sol_index=ref_index)._fefunc
    # sol_buffer = self.forwardOp.sample_at_coeff(list(xi), sol_index=sol_index_ref)._fefunc
    sol_measurements = np.array([sol_buffer(loc_dof) for loc_dof in global_sol.sample_coordinates])
    # print("     (O o G)(u) at {0} = {1}".format(bayes_obj.sample_coordinates, sol_measurements))
    diff = np.array(global_sol.measurements) - np.array(sol_measurements)
    # print("     given measurements: {}".format(bayes_obj.measurements))
    dens = np.exp(-0.5 * np.sum((diff ** 2) * (global_sol.inp_gamma ** (-1))))
    return dens * sol_buffer.vector()[:]