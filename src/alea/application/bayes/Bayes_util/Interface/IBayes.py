# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  Simple Interface for Bayesian objects containing all important methods
"""
# region Import
from abc import ABCMeta, abstractmethod
import logging
import logging.config
from scipy import array
# endregion


class IBayes(object):
    """
      Simple Bayesian inversion Interface that provides needed functions to estimate the posterior density
      using Bayes Rule.
    """
    __metaclass__ = ABCMeta

    fp = ''
    try:
        f = open('logging.conf')
        f.close()
        fp = 'logging.conf'
    except IOError:
        print("logging not in working directory")
        try:
            f = open('/home/marschall/WIAS/alea-testing/src/alea/application/bayes/logging.conf')
            f.close()
            fp = '/home/marschall/WIAS/alea-testing/src/alea/application/bayes/logging.conf'
        except IOError:
            print("No logging file found. -> exit()")
            exit()
    logging.config.fileConfig(fp)                   # load logger configuration
    log = logging.getLogger('IBayes')               # get new logger object

    # region abstract Function Export Bayes Object
    @abstractmethod
    def export_bayes(self, export_path):
        """
        stub for exporting the object to a file
        """
        pass
    # endregion

    # region abstract Function Import Bayes Object
    @abstractmethod
    def import_bayes(self, import_path):
        """
        creates an instance of the current object using the information from the given file
        """
        pass
    # endregion

    # region abstract Function Create Coefficient Field
    @abstractmethod
    def create_coeff_field(self, funcs, mean):
        """
        creates a coefficient field given by the amplitude functions and a mean function
        """
        pass
    # endregion

    # region abstract Function Create Forward Operator
    @abstractmethod
    def create_forward_operator(self, coeff_field, coefficients, physical_grid, stochastic_grid):
        """
        creates the forward solution operator from a given coefficient field
        """
        pass
    # endregion

    # region abstract Function Apply Observation
    @abstractmethod
    def apply_observation(self):
        """
        applies the observation operator at given dofs and round eventually to given precision and rank
        """
        pass
    # endregion

    # region abstract Function Calculate Measurement Difference
    @abstractmethod
    def calculate_measurement_diff(self, solution, value_list):
        """
        calculates the left side of the inner product
        """
        pass
    # endregion

    # region abstract Function Apply Inner Product
    @abstractmethod
    def apply_inner_product(self, solution, gamma, precision, rank):
        """
        calculates the misfit function with given variance as gamma*I and rounds result
        """
        pass
    # endregion

    # region abstract Calculate Marginal Densities
    @abstractmethod
    def calculate_marginal_densities(self, solution, eval_densities, stoch_grid, integration_step_size,
                                     eval_grid_points):
        """
        calculates the interpolated marginal densities
        """
        pass
    # endregion

    # region static Function extrapolate in 1D (artificial, here: set everything to 0)
    @staticmethod
    def extrap1d(interpolator):
        """
        adds an extrapolation method to an interpolator. Just set everything outside the interval to 0
        """
        # region Function Extrapolate to new boundaries
        def pointwise(x):
            """
            sets the left and right point of the interpolated value
            """
            if x < interpolator.x[0]:
                return 0
                # return ys[0] + (x - xs[0]) * (ys[1] - ys[0]) / (xs[1] - xs[0])
            elif x > interpolator.x[-1]:
                return 0
                # return ys[-1] + (x - xs[-1]) * (ys[-1] - ys[-2]) / (xs[-1] - xs[-2])
            else:
                return interpolator(x)
        # endregion

        # region Function create a function
        def ufunclike(xs):
            """
            creates a function of the new values
            """
            return array(map(pointwise, array(xs)))
        # endregion
        return ufunclike
    # endregion
