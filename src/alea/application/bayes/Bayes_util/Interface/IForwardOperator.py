# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  Interface for modelling different types of forward operators.
"""
# region Import
from abc import ABCMeta, abstractmethod, abstractproperty
import logging
import logging.config
# endregion


class IForwardOperator:
    """
      Base interface for forward operators used in Bayesian inversion
    """
    __metaclass__ = ABCMeta

    fp = ''
    try:
        f = open('logging.conf')
        f.close()
        fp = 'logging.conf'
    except IOError:
        print("logging not in working directory")
        try:
            f = open('/home/marschall/WIAS/alea-testing/src/alea/application/bayes/logging.conf')
            f.close()
            fp = '/home/marschall/WIAS/alea-testing/src/alea/application/bayes/logging.conf'
        except IOError:
            print("No logging file found. -> exit()")
            exit()
    logging.config.fileConfig(fp)
    log = logging.getLogger('IForwardOperator')     # get new logger object

    # region abstract Constructor
    @abstractmethod
    def __init__(self):
        """
          default constructor
        """
        pass
    # endregion

    # region abstract Function write to file
    @abstractmethod
    def write_to_file(self, export_file):
        """
          writes the items of the current instance into a file using pickling
          @param export_file: path to the file
          @type export_file: str
          @return: success
          @rtype: bool
        """
        pass
    # endregion

    # region abstract Function read from file
    @abstractmethod
    def read_from_file(self, import_file):
        """
          tries to read the instance from a file
          @param import_file: path to file to read from
          @type import_file: str
          @return: success
          @rtype: bool
        """
        pass
    # endregion

    # region abstract Function check validity of current object instance
    @abstractmethod
    def check_validity(self):
        """
          checks current instance member on plausibility
          @return: success
          @rtype: bool
        """
        pass
    # endregion

    # region abstract Property File name
    @abstractproperty
    def filename(self):
        """
        defines the file name which is used to store the data via pickling
        @return: str
        """
        pass
    # endregion
