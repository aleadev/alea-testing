# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  Forward operator as solution of an adaptive stochastic Galerkin method with parametrized unknown coefficient
  G(u(x,y)) = TT Tensor of solution w.r.t. physical FEM basis functions and stochastic basis given by multidimensional
  Legendre Polynomials
"""
# region Import
from __future__ import division
import cPickle as Pickle
from dolfin import File, Mesh, FunctionSpace, solve, Function, Expression, interpolate

import matplotlib.pyplot as plt
import numpy as np
import tt

from alea.application.bayes.Bayes_util.Interface.IForwardOperator import IForwardOperator
from alea.application.bayes.Bayes_util.lib.bayes_sgfem_als import log_normal_sgfem_als
from alea.application.bayes.Bayes_util.lib.interpolation_util import get_legendre_basis
from alea.application.egsz.sample_domains import SampleDomain
from alea.application.egsz.sample_problems2 import SampleProblem
from alea.application.egsz.affine_field import AffineField
from alea.fem.fenics.fenics_basis import FEniCSBasis
from alea.fem.fenics.fenics_vector import FEniCSVector
from alea.utils.plothelper import plot_mpl
from alea.utils.tictoc import TicToc

import warnings
import scipy.sparse as sps
# endregion


class LognormalsgFemOperator(IForwardOperator):
    """
      asgfem operator
    """

    FILE_PATH = "results/lognormal_sgfem/"

    # region Constructor
    def __init__(self):
        """
          instance constructor with properties
        """
        IForwardOperator.__init__(self)
        # self.max_dofs = 1e6
        # self.poly_sys = "L"
        # self.gpcps = 1
        # self.iterations = 1000
        # self.refinements = 10
        # self.thetax = 0.5
        # self.thetay = 0.5
        # self.srank = 2
        # self.urank = 1
        # self.updater = 2
        # self.coefficients = 10
        # self.rvtype = "uniform"
        # self.acc = 1e-14
        # self.do_timing = True
        # self.problem = 0
        # self.convergence = 1e-12
        # self.max_new_dim = 1000
        # self.no_longtail_zeta_marking = False
        # self.resnorm_zeta_weight = 1
        # self.rank_always = False
        # self.eta_zeta_weight = 0.1
        self.femdegree = 1                          # finite element degree of basis functions
        self.domain = "square"                      # used domain for physical approximation
        self.decayexp = 2                           # rate of decay in the gpc expansion of the coefficient
        self.maxrank = 15                           # maximal rank for the operator
        self.sol_rank = 5                           # maximal rank of the solution
        self.gamma = 0.9                            # safety factor in gpc expansion
        self.mesh_refine = 1                        # number of mesh refinements
        self.mesh_dofs = -1                         # fixed number of mesh dofs (dominant over mesh_refine)
        self.coeff_field_type = "EF-square_cos-algebraic"     # used coefficientfield type
        self.amptype = "decay-algebraic"            # used decay function (algebraic, exponential)
        self.n_coeff = 10                           # number of coefficients in the gpc expansion of the coefficient
        self.freq_skip = 0                          # skipping every (##) frequence
        self.freq_scale = 1.0                       # scaling the frequencies
        self.scale = 1.0                            # scaling the whole solution
        self.theta = 0.5                            # weight of gaussian measure 0<theta<1
        self.rho = 1.0                              # weight of gaussian measure >1
        self.hermite_degree = 8                     # degree of hermite polynomials in coefficient approximation
        self.gpc_degree = 3                         # degree of hermite polynomials in operator approximation
        self.als_tol = 1e-12                        # tolerance of the ALS convergence
        self.als_iterations = 1000                  # maximal number of iterations in the ALS
        # self.amp_type = "constant"
        self.SOL = []
        self.coefficient_field = None
        self.proj_basis = None
        self.sampling_error_l2 = None
        self.sampling_error_h1 = None
        self.field_mean = 1.0
        self.coefficients = 0
        self.coef_acc = 1e-7
        self.iterations = 5
        self.theta_x = 0.5
        self.theta_y = 0.5
        self.start_rank = 5
        self.num_coeff_in_coeff = 15
        self.max_hdegs_coef = 15
        self.eta_zeta_weight = 0.1
        self.resnorm_zeta_weight = 1.0
        self.coef_mesh_dofs = 300000
        self.coef_quad_degree = 2
        self.rank_always = True
        self.max_dofs = 1e6
        self.new_hdeg = 2
    # endregion

    # region Function create_forward_operator
    def create_forward_operator(self, maxrank=15, n_coeff=10, femdegree=1, gpcdegree=1, decayexp=2, gamma=0.9,
                                domain="square", mesh_refine=1, mesh_dofs=-1, sol_rank = 5, freq_skip=0,
                                freq_scale=1.0, scale=1.0, theta=0.5, rho=1.0, hermite_degree=8, als_iterations=1000,
                                convergence=1e-12, coeffield_type="EF-square-cos-algebraic", _print=False,
                                field_mean=1.0, force_recalculation=False, coef_acc=1e-7, iterations=5,
                                theta_x=0.5, theta_y=0.5, start_rank=5, num_coeff_in_coeff=15, max_hdegs_coef=15,
                                eta_zeta_weight=0.1, resnorm_zeta_weight=1, coef_mesh_dofs=3000000, coef_quad_degree=2,
                                rank_always=True, max_dofs=1e6, new_hdeg=2):
        """
        calls the asgfem method
        """
        self.femdegree = femdegree
        self.gpc_degree = gpcdegree
        self.domain = domain
        self.decayexp = decayexp
        self.als_iterations = als_iterations
        self.theta = theta
        self.rho = rho
        self.maxrank = maxrank
        self.sol_rank = sol_rank
        self.freq_scale = freq_scale
        self.freq_skip = freq_skip
        self.scale = scale
        self.hermite_degree = hermite_degree
        self.n_coeff = 4
        # self.n_coeff = n_coeff
        self.gamma = gamma
        self.mesh_refine = mesh_refine
        self.mesh_dofs = mesh_dofs
        self.als_tol = convergence
        if coeffield_type == "EF-square-cos-algebraic":
            self.amp_type = "decay-algebraic"
        else:
            raise ValueError("unknown coeffield type: {}".format(coeffield_type))
        self.coeff_field_type = coeffield_type
        self.field_mean = field_mean
        self.coefficients = self.n_coeff
        self.coef_acc = coef_acc
        self.iterations = iterations
        self.theta_x = theta_x
        self.theta_y = theta_y
        self.start_rank = start_rank
        self.num_coeff_in_coeff = num_coeff_in_coeff
        self.max_hdegs_coef = max_hdegs_coef
        self.eta_zeta_weight = eta_zeta_weight
        self.resnorm_zeta_weight = resnorm_zeta_weight
        self.coef_mesh_dofs = coef_mesh_dofs
        self.coef_quad_degree = coef_quad_degree
        self.rank_always = rank_always
        self.max_dofs = max_dofs
        self.new_hdeg = new_hdeg
        if not self.read_from_file(self.FILE_PATH):
            if not force_recalculation:
                try:
                    coef_mesh_cache = Mesh('{}-{}-mesh.xml'.format(str(self.domain), str(self.scale)))
                except RuntimeError:
                    coef_mesh_cache = None
                try:
                    infile = open('{}-{}-{}-cont_coef_cores.dat'.format(self.domain, str(self.scale), str(self.field_mean)),
                                  'rb')
                    cont_coef_cache = Pickle.load(infile)
                    infile.close()
                except RuntimeError:
                    cont_coef_cache = None
                except IOError:
                    cont_coef_cache = None
            else:
                cont_coef_cache = None
                coef_mesh_cache = None
            self.SOL = log_normal_sgfem_als(maxrank=maxrank, n_coeff=n_coeff, femdegree=femdegree, gpcdegree=gpcdegree,
                                            decayexp=decayexp, gamma=gamma, domain=domain, mesh_refine=mesh_refine,
                                            mesh_dofs=mesh_dofs, sol_rank=sol_rank, freq_skip=freq_skip,
                                            freq_scale=freq_scale, scale=scale, theta=theta, rho=rho,
                                            hermite_degree=hermite_degree,
                                            als_iterations=als_iterations, convergence=convergence,
                                            coeffield_type=coeffield_type,
                                            _print=False, field_mean=field_mean, coef_acc=coef_acc,
                                            coef_mesh_cache=coef_mesh_cache, cont_coef_cache=cont_coef_cache,
                                            iterations=iterations, theta_x=theta_x,
                                            theta_y=theta_y, start_rank=start_rank, n_coef_coef=num_coeff_in_coeff,
                                            max_hdegs_coef=max_hdegs_coef, eta_zeta_weight=eta_zeta_weight,
                                            resnorm_zeta_weight=resnorm_zeta_weight,
                                            coef_mesh_dofs=coef_mesh_dofs,
                                            coef_quad_degree=coef_quad_degree, rank_always=rank_always,
                                            max_dofs=max_dofs, new_hdeg=new_hdeg)
            self.coefficients = len(self.SOL[-1]["V"].n)-1
            self.n_coeff = self.coefficients
            # self.log.info("calculate solution object error")
            # xi_samples = [2*np.random.rand(self.coefficients)-1 for _ in range(n_solution_samples)]
            # self.sampling_error_l2, self.sampling_error_h1 = self.estimate_sampling_error(list(xi_samples))
            self.sampling_error_h1 = [-1]
            self.sampling_error_l2 = [-1]
            if not self.write_to_file(self.FILE_PATH):
                self.log.error("can not write asgfem file " + self.FILE_PATH + str(hash(self.filename)))
                raise IOError("can not write asgfem file " + self.FILE_PATH + str(hash(self.filename)))
            if not self.read_from_file(self.FILE_PATH):
                self.log.error("can not read from asgfem file " + self.FILE_PATH + str(hash(self.filename)))
                raise IOError("can not read from asgfem file " + self.FILE_PATH + str(hash(self.filename)))
    # endregion

    # region Function overrides write_to_file
    def write_to_file(self, export_path):
        """
        writes all important information into the export file
        """

        for i, S in enumerate(self.SOL):
            v, cg = S["V"], FunctionSpace(S["mesh"], 'CG', S["femdegree"])
            # inject additional solution data
            v.CG = (cg.ufl_element().family(), cg.ufl_element().degree())
            del S["CG"]
            del S["mesh"]
            v.max_order = len(v.r) - 1
            v.max_rank = max(v.r)

            # export mesh of FunctionSpace
            mesh = cg.mesh()
            File(export_path + str(hash(self.filename)) + "-mesh-%i.xml" % i) << mesh

            # export mesh pdf
            #if i == 0:
            #    plt.figure(1)
            #    plt.gca().set_aspect('equal')
            #    plt.gca().xaxis.set_ticklabels([])
            #    plt.gca().yaxis.set_ticklabels([])
            #    #                plt.show(block=False)
            plt.clf()
            plot_mpl(mesh)
            #            plt.title('iteration %i' % i)
            plt.draw()
            plt.savefig(export_path + str(hash(self.filename)) + "-mesh-%i.pdf" % i, dpi=None, facecolor='w',
                        edgecolor='w', orientation='portrait', papertype=None, format='pdf',
                        transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None)

        # write out
        inf = {"femdegree": self.femdegree, "gpc_degree": self.gpc_degree,
               "domain": self.domain, "decayexp": self.decayexp, "iterations": self.als_iterations, "rho": self.rho,
               "theta": self.theta, "maxrank": self.maxrank, "solrank": self.sol_rank, "coefficients": self.n_coeff,
               "gamma": self.gamma, "als_tol": self.als_tol, "mesh_refine": self.mesh_refine,
               "mesh_dofs": self.mesh_dofs, "coeff_field_type": self.coeff_field_type,
               "amptype": self.amptype, "sampling_error_l2": self.sampling_error_l2,
               "sampling_error_h1": self.sampling_error_h1, "field_mean": self.field_mean, "coef_acc": self.coef_acc}
        inf_str = "This is just for informational purpose for the ASGFEM Object: \n"
        inf_str += "    " + self.filename + " : " + str(hash(self.filename)) + "\n"
        inf_str += "femdegree: {}\n".format(self.femdegree)
        inf_str += "gpc_degree: {}\n".format(self.gpc_degree)
        inf_str += "domain: {}\n".format(self.domain)
        inf_str += "decayexp: {}\n".format(self.decayexp)
        inf_str += "amptype: {}\n".format(self.amptype)
        inf_str += "coeff_field_type: {}\n".format(self.coeff_field_type)
        inf_str += "iterations: {}\n".format(self.als_iterations)
        inf_str += "theta: {}\n".format(self.theta)
        inf_str += "gamma: {}\n".format(self.gamma)
        inf_str += "als_tol: {}\n".format(self.als_tol)
        inf_str += "maxrank: {}\n".format(self.maxrank)
        inf_str += "solrank: {}\n".format(self.sol_rank)
        inf_str += "coefficients: {}\n".format(self.n_coeff)
        inf_str += "mesh_refine: {}\n".format(self.mesh_refine)
        inf_str += "mesh_dofs: {}\n".format(self.mesh_dofs)
        inf_str += "coef_acc: {}\n".format(self.coef_acc)
        inf_str += "sampling_error_l2: \n"
        for lia in range(len(self.sampling_error_l2)):
            inf_str += "    {} = {} \n".format(lia, self.sampling_error_l2[lia])
        inf_str += "sampling_error_H1: \n"
        for lia in range(len(self.sampling_error_h1)):
            inf_str += "    {} = {} \n".format(lia, self.sampling_error_h1[lia])
        inf_str += "field_mean: {}\n".format(self.field_mean)
        try:
            outfile = open(export_path + str(hash(self.filename)) + '.dat', 'wb')
            Pickle.dump((self.SOL, inf), outfile, Pickle.HIGHEST_PROTOCOL)
            outfile.close()
            outfile = open(export_path + str(hash(self.filename)) + '.conf', 'wb')
            outfile.write(inf_str)
            outfile.close()
        except IOError as ex:
            self.log.error("io error while export:" + ex.message)
            return False
        except Exception as ex:
            self.log.error("error while export: " + ex.message)
            return False
        return True
    # endregion

    # region Function overrides read_from_file
    def read_from_file(self, import_path):
        """
        tries to read all important objects from file
        """
        try:
            # print import_path
            # print self.filename
            # exit()
            infile = open(import_path + str(hash(self.filename)) + '.dat', 'rb')
            self.SOL, inf = Pickle.load(infile)
            infile.close()
            # print self.SOL
            self.femdegree = inf["femdegree"]
            self.gpc_degree = inf["gpc_degree"]
            self.domain = inf["domain"]
            self.decayexp = inf["decayexp"]
            self.als_iterations = inf["iterations"]
            self.theta = inf["theta"]
            self.rho = inf["rho"]
            self.maxrank = inf["maxrank"]
            self.sol_rank = inf["solrank"]
            self.n_coeff = inf["coefficients"]
            self.coefficients = inf["coefficients"]
            self.gamma = inf["gamma"]
            self.als_tol = inf["als_tol"]
            self.mesh_refine = inf["mesh_refine"]
            self.mesh_dofs = inf["mesh_dofs"]
            self.coeff_field_type = inf["coeff_field_type"]
            self.sampling_error_l2 = inf["sampling_error_l2"]
            self.sampling_error_h1 = inf["sampling_error_h1"]
            self.coef_acc = inf["coef_acc"]

            for i, S in enumerate(self.SOL):
                # restore FunctionSpace and MultiindexSet
                mesh = Mesh(import_path + str(hash(self.filename)) + '-mesh-%i.xml' % i)
                v = S["V"]
                S["CG"] = FunctionSpace(mesh, v.CG[0], v.CG[1])
                # enrich V for sampling
                # cos = MultiindexSet.createFullTensorSet(len(V.r) - 2, gpcps[i])
                # mis = [Multiindex(x) for x in cos.arr]
                # V.active_indices = lambda: mis
                v.polysys = 'H'
                # v.gpcps = [self.gpc_degree]*self.coefficients
                v.gpcps = []
                for lia in range(len(S["V"].n)-1):
                    v.gpcps.append(S["V"].n[lia+1]+1)
                v.V = S["CG"]
                v.mesh = mesh
                # restrict iteration # to maximal # dofs if requested
                #if (self.max_dofs > 0) and (S["DOFS"] > self.max_dofs) and (i > 0):
                #    self.SOL = self.SOL[:i]
                #    print "considering solution up to iteration %i with %i dofs..." % (i - 1, self.SOL[-1]["DOFS"])
        except IOError as ex:
            self.log.error("IO error while import: " + ex.message)
            return False
        except Exception as ex:
            self.log.error("error while import: " + ex.message)
            return False
        return True
    # endregion

    # region overrides Function check_validity
    def check_validity(self):
        """
        check validity of instance data TODO
        """
        return True
    # endregion

    # region Property filename
    @property
    def filename(self):
        """
        returns the file name according to the members of the current instance
        """

        filename = "log_normal_sgfem_"
        filename += "fD%i" % self.femdegree
        filename += "gD%i" % self.gpc_degree
        filename += "dom%s" % self.domain
        filename += "dexp%3.2f" % self.decayexp
        filename += "it%i" % self.als_iterations
        filename += "t%3.2f" % self.theta
        filename += "mR%i" % self.maxrank
        filename += "sR%i" % self.sol_rank
        filename += "g%i" % self.gamma
        filename += "tol%.2e" % self.als_tol
        filename += "mRef%i" % self.mesh_refine
        filename += "cFT%s" % self.coeff_field_type
        filename += "aT%s" % self.amptype
        filename += "cM%s" % self.field_mean
        filename += "nC%i" % self.n_coeff
        filename += "cAcc%.2e" % self.coef_acc
        filename += "rho%3.2f" % self.rho
        filename += "fscale%3.2f" % self.freq_scale
        filename += "fskip%i" % self.freq_skip
        filename += "scale%5.2f" % self.scale
        filename += "hd%i" % self.hermite_degree
        filename += "md%i" % self.mesh_dofs
        filename += "alstol%i" % self.als_tol
        filename += "iter%i" % self.iterations
        filename += "thX%3.2f" % self.theta_x
        filename += "thY%3.2f" % self.theta_y
        filename += "startR%i" % self.start_rank
        filename += "num_coef%i" % self.num_coeff_in_coeff
        filename += "hd_coef%i" % self.max_hdegs_coef
        filename += "etaZeta%3.2f" % self.eta_zeta_weight
        filename += "resZeta%3.2f" % self.resnorm_zeta_weight
        filename += "coefMeshDof%i" % self.coef_mesh_dofs
        filename += "coefQuadDeg%i" % self.coef_quad_degree
        filename += "maxDof%i" % self.max_dofs
        filename += "newhdeg%i" % self.new_hdeg

        return str(filename)
    # endregion

    # region Property get_coefficient_field
    @property
    def get_coefficient_field(self):
        """
        returns the coefficient field belonging to the current instance settings
        Attention: This is only the affine field. Exp has to be done later!!!!!!
        @return: Parametric coefficient field
        """

        #return get_coefficient_field(self.coeff_field_type, self.amp_type, self.decayexp,
        #                                                   self.gamma, self.rvtype, coeff_mean=self.field_mean)
        af = AffineField(self.coeff_field_type, decayexp=self.decayexp, gamma=self.gamma, freqscale=self.freq_scale,
                         freqskip=self.freq_skip, scale=self.scale, amptype=self.amptype)
        return af.coeff_field
    # endregion

    # region function get_proj_basis
    def get_proj_basis(self, sol_index=-1, force_recalculation=False):
        """
        creates the projection basis
        @return: FEniCSBasis
        """
        if sol_index > len(self.SOL):
            raise AssertionError("get_proj_basis: sol_index out of range")
        if self.proj_basis is None or force_recalculation:
            assert isinstance(self.SOL[sol_index]["CG"], FunctionSpace)
            self.proj_basis = FEniCSBasis(self.SOL[sol_index]["CG"])
        return self.proj_basis
    # endregion

    # region refine function space
    def refine_function_space(self, sol_index, dofs):
        if dofs > 0:
            from dolfin import refine
            while self.SOL[sol_index]["CG"].dim() < dofs:
                self.SOL[sol_index]["CG"] = FunctionSpace(refine(self.SOL[sol_index]["CG"].mesh()), "CG",
                                                          self.femdegree)
    # endregion

    # region Property get_pde
    @property
    def get_pde(self):
        """
          creates the ALEA PDE Object from the current instance
        @return:
        """
        #                                           # only needed for the boundaries entry. Contains just min and max
        _, boundaries, _ = SampleDomain.setupDomain(self.domain, initial_mesh_N=10)
        #                                           # create ALEA PDE from SampleProblem
        pde, _, _, _, _, _ = SampleProblem.setupPDE(2, self.domain, 0, boundaries,
                                                    self.get_coefficient_field)
        return pde
    # endregion

    # region Function compute_direct_sample_solution

    def compute_direct_sample_solution(self, rv_samples, pde=None, proj_basis=None, coef_field=None, reference_m=10,
                                       cache=None, expression_degree=10, return_coef=False):
        """
          creates the solution object from the parametric pde setting for a given set of samples
        @param rv_samples: list of samples to evaluate the coefficient field at
        @return: FEniCSVector
        """
        assert len(self.SOL[-1]["V"].n)-1 <= reference_m
        if pde is None:
            pde = self.get_pde
        if proj_basis is None:
            proj_basis = self.get_proj_basis
        if coef_field is None:
            coef_field = self.get_coefficient_field
        a_0 = self.get_coefficient_field.mean_func
        if True:

            # NOTE: this variant creates a single large expression and so requires just one assembly with exp_a
            # should be much faster than the second variant below
            if cache.str is None:
                def getcpp(coeff_exp):
                    cpp = coeff_exp.cppcode
                    return cpp.replace("A", str(coeff_exp.A)).replace("B", str(coeff_exp.B)).replace("freq", str(
                        coeff_exp.freq)).replace("m", str(coeff_exp.m)).replace("n", str(coeff_exp.n))
                                      # or a_0.B as before? In log normal case a_0.A fits mutch better, since it is the mean
                a_ex_m = "+".join([str(a_0.A)] + ["A" + str(m) + "*" + getcpp(coef_field[m][0]) for m in
                                                  range(reference_m)])
                a_A_m = ",".join(["A%i=0" % m for m in range(reference_m)])
                a_ex_m = "exp(" + a_ex_m + ")"

                print "=" * 80
                print reference_m
                print a_ex_m
                print "=" * 80
                cell_str = ',' if range(len(self.SOL[-1]["V"].n)-1) > 0 else ''
                cell_str += " cell='triangle', degree={}".format(expression_degree)
                eval_m = "Expression('%s', %s)" % (a_ex_m, a_A_m + cell_str)
                cache.str = eval_m
                exp_a = eval(eval_m)
                for m in range(reference_m):
                    Am = "A%i" % m
                    setattr(exp_a, Am, rv_samples[m])
            else:
                # print("string cache found")
                exp_a = eval(cache.str)
                for m in range(reference_m):
                    Am = "A%i" % m
                    setattr(exp_a, Am, rv_samples[m])

        else:
            # NOTE: this was replaced by the above code
            a = a_0
            for m in range(reference_m):
                a += rv_samples[m] * coef_field[m][0]
            exp_a = np.exp(a)

        #if piecewise_coeffs_data is not None:
        #    # approximation by piecewise function
        #    if piecewise_coeffs_data['p'] == 0:
        #        c0 = Function(piecewise_coeffs_data['DG'])
        #        u0 = np.array([exp_a(p) for p in piecewise_coeffs_data['mp']])
        #        c0.vector()[:] = u0[piecewise_coeffs_data['c2d']]
        #        exp_a = c0
        #    else:
        #        exp_a = project(exp_a, piecewise_coeffs_data['DG'])

        b = pde.assemble_rhs(basis=proj_basis(sol_index=-1, force_recalculation=True), coeff=exp_a, withDirichletBC=False)
        A = pde.assemble_lhs(basis=proj_basis(sol_index=-1, force_recalculation=True), coeff=exp_a, withDirichletBC=False)
        A, b = pde.apply_dirichlet_bc(proj_basis(sol_index=-1, force_recalculation=True)._fefs, A, b)

        X = 0 * b
        # logger.info("compute_direct_sample_solution with %i dofs" % b.size())
        solve(A, X, b)
        print("exp_a = {}".format(exp_a))
        print("exp_a = {}".format(type(exp_a)))
        print("proj_basis = {}".format(proj_basis(sol_index=-1)._fefs))
        print("proj_basis = {}".format(type(proj_basis(sol_index=-1)._fefs)))

        if return_coef is True:
            from dolfin import project, Expression
            return FEniCSVector(Function(proj_basis(sol_index=-1)._fefs, X)), interpolate(exp_a, proj_basis(sol_index=-1)._fefs)
        return FEniCSVector(Function(proj_basis(sol_index=-1)._fefs, X))

    # endregion

    # region Function estimate sampling Error
    def estimate_sampling_error(self, xi_samples=None, n_xi_samples=10, pde=None, proj_basis=None, coef_field=None,
                                relative=False, sol_index=-1, sol_index_ref=-1, reference_m=200, cache=None):
        """
        samples the error between the calculated solution object and the true solution from
        'compute_direct_sample_solution' in a Monte-Carlo fashion. Used norms are the L2 and the H1 norms.
        @param xi_samples: list of samples to create the sampling error at
        @type xi_samples: list
        @param sol_index: index of the solution refinement. finest solution at -1
        @type sol_index: int
        @:type sol_index_ref:
        @return: L2-error, H1-error
        @rtype: float, float
        """
        from joblib import Parallel, delayed
        # from dolfin.fem.norms import norm, errornorm
        from dolfin import set_log_level, ERROR, TrialFunction, TestFunction, dx, assemble, as_backend_type, \
            inner, parameters, grad
        if reference_m < self.coefficients:
            reference_m = self.coefficients + 1
        set_log_level(ERROR)
        if not self.SOL:
            raise TypeError("estimate_sampling_error: Solution object is not given")
        try:
            if not xi_samples:
                xi_samples = np.array([np.random.randn(reference_m) for _ in range(n_xi_samples)])
        except:
            if False:
                print("xi_samples given")
        if not isinstance(xi_samples, list) and not isinstance(xi_samples, np.ndarray):
            raise TypeError("estimate_sampling_error: sample list is not loaded")
        if len(xi_samples) < 1:
            raise AssertionError("estimate_sampling_error: to few samples in list")

        # print "compare solution {} with refSolution {}".format(sol_index, sol_index_ref)
        #                                           # obtain informations about reference function space
        fs = self.SOL[-1]["CG"]

        with TicToc(key="  create M and S", active=True, do_print=True):
            if cache.M is None and cache.S is None:
                _u = TrialFunction(fs)
                _v = TestFunction(fs)
                # laback = parameters.linear_algebra_backend
                parameters['linear_algebra_backend'] = 'Eigen'
                parameters.linear_algebra_backend = "Eigen"

                #                                           # create bilinear form of mass matrix
                BF = inner(_u, _v) * dx
                M = assemble(BF)
                rows, cols, values = as_backend_type(M).data()
                # print("convert to sps matrix")
                with warnings.catch_warnings():
                    warnings.simplefilter("ignore")
                    cache.M = sps.csr_matrix((values, cols, rows))

                # M = M.array()                               # obtain mass matrix
                #                                           # create bilinear form of stiffness matrix
                BF = inner(grad(_u), grad(_v)) * dx
                S = assemble(BF)

                rows, cols, values = as_backend_type(S).data()
                # print("convert to sps matrix")
                with warnings.catch_warnings():
                    warnings.simplefilter("ignore")
                    cache.S = sps.csr_matrix((values, cols, rows))
            M = cache.M
            S = cache.S
        # S = S.array()                               # obtain stiffness matrix

        global global_sol
        global_sol = self
        # global global_errornorm
        # global_errornorm = errornorm
        # global global_norm
        # global_norm = norm
        global global_ref_pde
        global_ref_pde = pde
        global global_ref_coef_field
        global_ref_coef_field = coef_field
        global global_ref_proj_basis
        global_ref_proj_basis = proj_basis
        global global_ref_function_space
        global_ref_function_space = fs
        global global_M
        global_M = M
        global global_S
        global_S = S

        with TicToc(key="  do first iteration", active=True, do_print=True):
            _ = sample_sol_parallel(xi_samples[0], relative, sol_index=sol_index, reference_m=reference_m,
                                    cache=cache)

        with TicToc(key="  do parallel run", active=True, do_print=True):
            arr = np.array(Parallel(n_jobs=1
                                    # , backend="threading"
                                    )
                           (delayed(sample_sol_parallel)(xi, relative, sol_index=sol_index,
                                                         reference_m=reference_m, cache=cache)
                            for xi in xi_samples))
            # arr = np.array([sample_sol_parallel(xi, relative, sol_index=sol_index, reference_m=reference_m,
            #                                     cache=cache)
            #                 for xi in xi_samples])
        errl2 = arr[:, 0]
        errh1 = arr[:, 1]

        return errl2, errh1
    # endregion

    # region Function Sample at Coefficients
    def sample_at_coeff(self, rv_samples, sol_index=-1, no_proj=False, return_coef=False):
        """
        samples the tensor in the stochastic space at given coefficients and returns a Fenics Function
        We assume the original solution object here.
        @param rv_samples: list of parameters in stochastic space
        @type rv_samples: list
        @return: list of measurements at given space points
        @rtype: FEniCSVector
        """

        if not isinstance(rv_samples, list):
            self.log.error('sample_at_coeff: rv_samples is not a list')
            raise TypeError('sample_at_coeff: rv_samples is not a list')

        if sol_index > len(self.SOL):
            raise AssertionError("sample_at_coeff: sol_index out of range")

        from alea.application.egsz.sampling import compute_parametric_sample_solution_TT
        from alea.application.tt_asgfem.tensorsolver.tt_lognormal import sample_lognormal_tt
        samples = [rv_samples]
        if True:

            # alternative sampling method using Max 'sample_lognormal_tt' with weighted hermite polynomials
            samples = samples[0]
            # print("sol index = {}, ten: {}".format(sol_index, self.SOL[sol_index]["V"]))
            retval = sample_lognormal_tt(self.SOL[sol_index]["V"], samples, self.SOL[sol_index]["bmax"],
                                         theta=self.theta, rho=self.rho)
            # print("size of sample at coef retval {}".format(retval.shape))
            fun = Function(self.SOL[sol_index]["V"].V)
            # print("size of function vector for sample at coef {}".format(len(fun.vector()[:])))
            fun.vector()[:] = retval
            # from alea.utils.plothelper import PlotHelper
            # PH = PlotHelper()
            # PH["test"].plot(fun)

            if return_coef is True:
                from alea.application.tt_asgfem.tensorsolver.tt_lognormal import fully_disc_first_core, \
                    sample_cont_coeff
                af = AffineField(self.coeff_field_type, amptype=self.amptype, decayexp=self.decayexp, gamma=self.gamma,
                                 freqscale=self.freq_scale, freqskip=self.freq_skip, scale=self.scale,
                                 coef_mean=self.field_mean)
                # region obtain dofs of cg space
                #                                       # obtain all coordinates of degrees of freedom
                mp = np.array(FunctionSpace(self.SOL[sol_index]["V"].mesh, 'CG', self.femdegree).tabulate_dof_coordinates())
                #                                       # reshape dof vector accorind to physical dimension
                mp = mp.reshape((-1, self.SOL[sol_index]["V"].mesh.geometry().dim()))

                # endregion
                hdegs_coef = self.num_coeff_in_coeff * [self.max_hdegs_coef]  # set hermite degree list of coefficient

                ranks_coef = [1] + len(hdegs_coef) * [self.maxrank] + [1]

                #                                       # create discrete first core of coefficient field
                core0 = fully_disc_first_core(self.SOL[sol_index]["cont_coef_cores"], af, mp, ranks_coef, hdegs_coef,
                                              self.SOL[sol_index]["bmax"],
                                              theta=self.theta, rho=self.rho, cg_mesh=self.SOL[sol_index]["V"].mesh,
                                              femdegree=self.femdegree)
                from copy import deepcopy
                coeff_cores = deepcopy(self.SOL[sol_index]["cont_coef_cores"])
                #                                     # copy continuous cores to use later and not change the reference
                # coeff_cores.insert(0, core0)  # add discrete first core to obtain fully discrete coefficient field
                coef_values = sample_cont_coeff(coeff_cores, af, mp, rv_samples, ranks_coef, hdegs_coef, self.SOL[sol_index]["bmax"],
                                        self.theta, self.rho)
                return self.get_proj_basis(sol_index=sol_index).project_onto(FEniCSVector(fun)), \
                       self.get_proj_basis(sol_index=sol_index).project_onto(FEniCSVector(Function(self.get_proj_basis(sol_index=-1)._fefs, coef_values)))
            else:
                return self.get_proj_basis(sol_index=sol_index).project_onto(FEniCSVector(fun))
            # print "samples: {}".format(samples)
            # print "SOL[sol_index]: {}, norm={}".format(self.SOL[sol_index]["V"],
            # tt.vector.norm(self.SOL[sol_index]["V"]))
            # print "SOL[-1]: {}, norm={}".format(self.SOL[-1]["V"], tt.vector.norm(self.SOL[-1]["V"]))
            # print "tensor mean: {}".format(self.SOL[sol_index]["V"][[0]*len(self.SOL[sol_index]["V"].n)])
            # print "length of SOL: {}".format(len(self.SOL))
            # print "sol_index={}".format(sol_index)
            # print "norm(U-V)= {}".format(tt.vector.norm(self.SOL[-1]["V"]-self.SOL[sol_index]["V"]))
        else:
            return compute_parametric_sample_solution_TT(samples, self.SOL[sol_index]["V"],
                                                         self.get_proj_basis(sol_index=sol_index), 1, lognormal=True,
                                                         no_proj=no_proj)[0]

    # endregion

global_sol = None
# global_errornorm = None
# global_norm = None
global_ref_pde = None
global_ref_proj_basis = None
global_ref_coef_field = None
global_ref_function_space = None
global_M = None
global_S = None


def sample_sol_parallel(xi, relative, sol_index=-1, reference_m=10, cache=None):
    _sol = global_sol                               # get current operator object from global variable (self)
    # _errornorm = global_errornorm                 # get Fenics errornorm function from global variable
    # _norm = global_norm                           # get Fenics norm function from global variable
    _pde = global_ref_pde                           # get PDE from global variable (self.get_pde)
    _coef_field = global_ref_coef_field             # get coefficient field from global variable (self.get_coeff_field)
    _proj_basis = global_ref_proj_basis             # get reference projected basis from global variable (self.get_proj)
    _ref_fs = global_ref_function_space
    mass = global_M
    stiff = global_S
    # print("sample")

    with TicToc(key="    compute direct solution", active=True, do_print=False):
        # noinspection PyProtectedMember
        buffer_sol_direct, coef_direct = _sol.compute_direct_sample_solution(list(xi), pde=_pde, proj_basis=_proj_basis,
                                                                             coef_field=_coef_field,
                                                                             reference_m=reference_m,
                                                                             cache=cache, return_coef=True)
        buffer_sol_direct = buffer_sol_direct._fefunc
        buffer_sol_direct = interpolate(buffer_sol_direct, _ref_fs)
        coef_direct = interpolate(coef_direct, _ref_fs)

    with TicToc(key="    compute approx solution", active=True, do_print=False):
        # noinspection PyProtectedMember
        buffer_sol_approx, coef_approx = _sol.sample_at_coeff(list(xi), sol_index=sol_index, return_coef=True)
        buffer_sol_approx = buffer_sol_approx._fefunc
        coef_approx = coef_approx.fefunc
    with TicToc(key="    interpolate to finer space", active=True, do_print=False):
        buffer_sol_approx = interpolate(buffer_sol_approx, _ref_fs)

    from dolfin import File
    from alea.utils.timing import get_current_time_string
    curr_time = get_current_time_string()
    vtkfile = File('results/lognormal_sgfem/sol_coef_sampling/{}solution.pvd'.format(curr_time))
    vtkfile << buffer_sol_direct
    vtkfile = File('results/lognormal_sgfem/sol_coef_sampling/{}solution_approx.pvd'.format(curr_time))
    vtkfile << buffer_sol_approx
    vtkfile = File('results/lognormal_sgfem/sol_coef_sampling/{}coef.pvd'.format(curr_time))
    vtkfile << coef_direct
    vtkfile = File('results/lognormal_sgfem/sol_coef_sampling/{}coef_approx.pvd'.format(curr_time))
    vtkfile << coef_approx
    with open('results/lognormal_sgfem/sol_coef_sampling/{}sample.txt'.format(curr_time), 'w') as _f:
        _f.write(xi)

    # ########### Some norm calculation stuff

    # eu_norm = np.linalg.norm(buffer_sol_direct.vector()[:] - buffer_sol_approx.vector()[:])
    # print("euclidean norm: {}".format(eu_norm))
    from dolfin import errornorm
    if False:
        return errornorm(buffer_sol_direct, buffer_sol_approx), errornorm(buffer_sol_direct, buffer_sol_approx,
                                                                          norm_type='h1')

    with TicToc(key="    calculate errors", active=True, do_print=False):
        diff = np.array(buffer_sol_direct.vector()[:]) - np.array(buffer_sol_approx.vector()[:])
        errl2 = np.sqrt(np.dot(diff.T, sps.csr_matrix.dot(mass, diff)))
        errh1 = np.sqrt(np.dot(diff.T, sps.csr_matrix.dot(stiff, diff)))
        if relative:
            u = np.array(buffer_sol_direct.vector()[:])
            ref_norm_l2 = np.sqrt(np.dot(u.T, sps.csr_matrix.dot(mass, u)))
            ref_norm_h1 = np.sqrt(np.dot(u.T, sps.csr_matrix.dot(stiff, u)))
            errl2 *= ref_norm_l2**(-1)
            errh1 *= ref_norm_h1**(-1)
    # fs = _sol.SOL[-1]["CG"]
    # e = Function(fs)
    # print("len sol_direct = {}".format(len(buffer_sol_direct.vector()[:])))
    # print("len sol_approx = {}".format(len(buffer_sol_approx.vector()[:])))
    # print("len e = {}".format(len(e.vector()[:])))
    # e.vector()[:] = np.array(buffer_sol_direct.vector()[:]) - np.array(buffer_sol_approx.vector()[:])
    # e.vector()[:] = (buffer_sol_direct - buffer_sol_approx).vector()[:]
    # if relative:
    #     errl2, errh1 = _norm(e, norm_type="L2")/_norm(buffer_sol_direct, norm_type="L2"), \
    #                    _norm(e, norm_type="H1")/_norm(buffer_sol_direct, norm_type="H1")
    # else:
    #     errl2, errh1 = _norm(e, norm_type="L2"), _norm(e, norm_type="H1")

        # u = buffer_sol_direct
        # uh = buffer_sol_approx
        # err = assemble(u ** 2 * dx - 2 * u * uh * dx + uh ** 2 * dx)
        # err2 = assemble(e**2 * dx)
        # print("err1 = {}".format(err))
        # print("err2 = {}".format(err2))
    # print("relative " + "errl2 = {} errh1 = {}".format(errl2, errh1) if relative else "absolute " + "errl2 = {}
    # errh1 = {}".format(errl2, errh1))
    return errl2, errh1
