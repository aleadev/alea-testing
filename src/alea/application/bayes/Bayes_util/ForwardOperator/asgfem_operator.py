# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  Forward operator as solution of an adaptive stochastic Galerkin method with parametrized unknown coefficient
  G(u(x,y)) = TT Tensor of solution w.r.t. physical FEM basis functions and stochastic basis given by multidimensional
  Legendre Polynomials
"""
# region Import
from __future__ import division
import cPickle as Pickle
from dolfin import File, Mesh, FunctionSpace, solve, Function, Expression

import matplotlib.pyplot as plt
import numpy as np
import tt

from alea.application.bayes.Bayes_util.Interface.IForwardOperator import IForwardOperator
from alea.application.bayes.Bayes_util.lib.bayes_sgfem_als import sgfem_als, get_coefficient_field
from alea.application.bayes.Bayes_util.lib.interpolation_util import get_legendre_basis
from alea.application.egsz.sample_domains import SampleDomain
from alea.application.egsz.sample_problems2 import SampleProblem
from alea.fem.fenics.fenics_basis import FEniCSBasis
from alea.fem.fenics.fenics_vector import FEniCSVector
from alea.utils.plothelper import plot_mpl


# endregion
from dolfin import *

class AsgFemOperator(IForwardOperator):
    """
      asgfem operator
    """

    FILE_PATH = "results/asgfem/"

    # region Constructor
    def __init__(self):
        """
          instance constructor with properties
        """
        IForwardOperator.__init__(self)
        self.max_dofs = 1e6
        self.poly_sys = "L"
        self.femdegree = 1
        self.gpcps = 1
        self.domain = "square"
        self.decayexp = 2
        self.iterations = 1000
        self.refinements = 10
        self.thetax = 0.5
        self.thetay = 0.5
        self.srank = 2
        self.maxrank = 15
        self.urank = 1
        self.updater = 2
        self.coefficients = 10
        self.gamma = 0.9
        self.rvtype = "uniform"
        self.acc = 1e-14
        self.do_timing = True
        self.mesh_refine = 1
        self.mesh_dofs = -1
        self.problem = 0
        self.convergence = 1e-12
        self.max_new_dim = 1000
        self.no_longtail_zeta_marking = False
        self.resnorm_zeta_weight = 1
        self.rank_always = False
        self.eta_zeta_weight = 0.1
        self.coeff_field_type = "EF-square_cos"
        self.amp_type = "constant"
        self.SOL = []
        self.coefficient_field = None
        self.proj_basis = None
        self.sampling_error_l2 = None
        self.sampling_error_h1 = None
        self.field_mean = 1.0
    # endregion

    # region Function create_forward_operator
    def create_forward_operator(self, iterations=1000, refinements=10, max_dofs=1e6, thetax=0.5, thetay=0.5, srank=2,
                                maxrank=15, urank=1,
                                updater=2, m=10, femdegree=1, gpcdegree=1, polysys="L", decayexp=2, gamma=0.9,
                                rvtype="uniform",
                                acc=1e-14, do_timing=True, domain="square", mesh_refine=1, mesh_dofs=-1, problem=0,
                                convergence=1e-12, max_new_dim=1000, no_longtail_zeta_marking=False,
                                resnorm_zeta_weight=1,
                                rank_always=False, eta_zeta_weight=0.1, coeffield_type="EF-square-cos",
                                amp_type="constant", _print=False, n_solution_samples=100, field_mean=1.0
                                ):
        """
        calls the asgfem method
        """
        self.max_dofs = max_dofs
        self.poly_sys = polysys
        self.femdegree = femdegree
        self.gpcps = gpcdegree
        self.domain = domain
        self.decayexp = decayexp
        self.iterations = iterations
        self.refinements = refinements
        self.thetax = thetax
        self.thetay = thetay
        self.srank = srank
        self.maxrank = maxrank
        self.urank = urank
        self.updater = updater
        self.coefficients = m
        self.gamma = gamma
        self.rvtype = rvtype
        self.acc = acc
        self.do_timing = do_timing
        self.mesh_refine = mesh_refine
        self.mesh_dofs = mesh_dofs
        self.problem = problem
        self.convergence = convergence
        self.max_new_dim = max_new_dim
        self.no_longtail_zeta_marking = no_longtail_zeta_marking
        self.resnorm_zeta_weight = resnorm_zeta_weight
        self.rank_always = rank_always
        self.eta_zeta_weight = eta_zeta_weight
        self.coeff_field_type = coeffield_type
        self.amp_type = amp_type
        self.field_mean = field_mean
        if not self.read_from_file(self.FILE_PATH):
            self.SOL = sgfem_als(iterations=self.iterations, refinements=self.refinements, max_dofs=self.max_dofs,
                                 thetax=self.thetax, thetay=self.thetay, srank=self.srank, maxrank=self.maxrank,
                                 urank=self.urank, updater=self.updater, M=self.coefficients, femdegree=self.femdegree,
                                 gpcdegree=self.gpcps, polysys=self.poly_sys, decayexp=self.decayexp, gamma=self.gamma,
                                 rvtype=self.rvtype, acc=self.acc, do_timing=self.do_timing, domain=self.domain,
                                 mesh_refine=self.mesh_refine, mesh_dofs=self.mesh_dofs, problem=self.problem,
                                 convergence=self.convergence, max_new_dim=self.max_new_dim,
                                 no_longtail_zeta_marking=self.no_longtail_zeta_marking,
                                 resnorm_zeta_weight=self.resnorm_zeta_weight,
                                 rank_always=self.rank_always, eta_zeta_weight=self.eta_zeta_weight,
                                 coeffield_type=self.coeff_field_type,
                                 amp_type=self.amp_type, field_mean=field_mean,
                                 _print=_print
                                 )
            self.coefficients = len(self.SOL[-1]["V"].n)-1
            self.log.info("calculate solution object error")
            # xi_samples = [2*np.random.rand(self.coefficients)-1 for _ in range(n_solution_samples)]
            # self.sampling_error_l2, self.sampling_error_h1 = self.estimate_sampling_error(list(xi_samples))
            self.sampling_error_h1 = [-1]
            self.sampling_error_l2 = [-1]
            if not self.write_to_file(self.FILE_PATH):
                self.log.error("can not write asgfem file " + self.FILE_PATH + str(hash(self.filename)))
                raise IOError
            if not self.read_from_file(self.FILE_PATH):
                self.log.error("can not read from asgfem file " + self.FILE_PATH + str(hash(self.filename)))
                raise IOError
    # endregion

    # region Function overrides write_to_file
    def write_to_file(self, export_path):
        """
        writes all important information into the export file
        """

        for i, S in enumerate(self.SOL):
            v, cg = S["V"], S["CG"]
            # inject additional solution data
            v.CG = (cg.ufl_element().family(), cg.ufl_element().degree())
            del S["CG"]
            v.max_order = len(v.r) - 1
            v.max_rank = max(v.r)

            # export mesh of FunctionSpace
            mesh = cg.mesh()
            File(export_path + str(hash(self.filename)) + "-mesh-%i.xml" % i) << mesh

            # export mesh pdf
            if i == 0:
                plt.figure(1)
                plt.gca().set_aspect('equal')
                plt.gca().xaxis.set_ticklabels([])
                plt.gca().yaxis.set_ticklabels([])
                #                plt.show(block=False)
            plt.clf()
            plot_mpl(mesh)
            #            plt.title('iteration %i' % i)
            plt.draw()
            plt.savefig(export_path + str(hash(self.filename)) + "-mesh-%i.pdf" % i, dpi=None, facecolor='w',
                        edgecolor='w', orientation='portrait', papertype=None, format='pdf',
                        transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None)

        # write out
        inf = {"max_dofs": self.max_dofs, "polysys": self.poly_sys, "femdegree": self.femdegree, "gpcps": self.gpcps,
               "domain": self.domain, "decayexp": self.decayexp, "iterations": self.iterations,
               "refinements": self.refinements, "thetax": self.thetax, "thetay": self.thetay, 'srank': self.srank,
               "maxrank": self.maxrank, "urank": self.urank, "updater": self.updater, "coefficients": self.coefficients,
               "gamma": self.gamma, "rvtype": self.rvtype, "acc": self.acc, "do_timing": self.do_timing,
               "mesh_refine": self.mesh_refine, "mesh_dofs": self.mesh_dofs, "problem": self.problem,
               "convergence": self.convergence, "max_new_dim": self.max_new_dim,
               "no_longtail_zeta_marking": self.no_longtail_zeta_marking,
               "resnorm_zeta_weight": self.resnorm_zeta_weight, "rank_always": self.rank_always,
               "eta_zeta_weight": self.eta_zeta_weight, "coeff_field_type": self.coeff_field_type,
               "amp_type": self.amp_type, "sampling_error_l2": self.sampling_error_l2, "sampling_error_h1":
               self.sampling_error_h1, "field_mean": self.field_mean}
        inf_str = "This is just for informational purpose for the ASGFEM Object: \n"
        inf_str += "    " + self.filename + " : " + str(hash(self.filename)) + "\n"
        inf_str += "max_dofs: {} \n".format(self.max_dofs)
        inf_str += "polysys: {} \n".format(self.poly_sys)
        inf_str += "femdegree: {}\n".format(self.femdegree)
        inf_str += "gpcps: {}\n".format(self.gpcps)
        inf_str += "domain: {}\n".format(self.domain)
        inf_str += "decayexp: {}\n".format(self.decayexp)
        inf_str += "iterations: {}\n".format(self.iterations)
        inf_str += "refinements: {} \n".format(self.refinements)
        inf_str += "thetaX: {}\n".format(self.thetax)
        inf_str += "thetaY: {}\n".format(self.thetay)
        inf_str += "srank: {}\n".format(self.srank)
        inf_str += "gamma: {}\n".format(self.gamma)
        inf_str += "rvtype: {}\n".format(self.rvtype)
        inf_str += "acc: {}\n".format(self.acc)
        inf_str += "do_timing: {}\n".format(str(self.do_timing))
        inf_str += "maxrank: {}\n".format(self.maxrank)
        inf_str += "urank: {}\n".format(self.urank)
        inf_str += "updater: {}\n".format(self.updater)
        inf_str += "coefficients: {}\n".format(self.coefficients)
        inf_str += "mesh_refine: {}\n".format(self.mesh_refine)
        inf_str += "mesh_dofs: {}\n".format(self.mesh_dofs)
        inf_str += "problem: {}\n".format(self.problem)
        inf_str += "convergence: {}\n".format(self.convergence)
        inf_str += "max_new_dim: {}\n".format(self.max_new_dim)
        inf_str += "no_longtail_zeta_marking: {}\n".format(self.no_longtail_zeta_marking)
        inf_str += "resnorm_zeta_weight: {}\n".format(self.resnorm_zeta_weight)
        inf_str += "eta_zeta_weight: {}\n".format(self.eta_zeta_weight)
        inf_str += "coeff_field_type: {}\n".format(self.coeff_field_type)
        inf_str += "amp_type: {}\n".format(self.amp_type)
        inf_str += "sampling_error_l2: \n"
        for lia in range(len(self.sampling_error_l2)):
            inf_str += "    {} = {} \n".format(lia, self.sampling_error_l2[lia])
        inf_str += "sampling_error_H1: \n"
        for lia in range(len(self.sampling_error_h1)):
            inf_str += "    {} = {} \n".format(lia, self.sampling_error_h1[lia])
        inf_str += "field_mean: {}\n".format(self.field_mean)
        try:
            outfile = open(export_path + str(hash(self.filename)) + '.dat', 'wb')
            Pickle.dump((self.SOL, inf), outfile, Pickle.HIGHEST_PROTOCOL)
            outfile.close()
            outfile = open(export_path + str(hash(self.filename)) + '.conf', 'wb')
            outfile.write(inf_str)
            outfile.close()
        except IOError as ex:
            self.log.error("io error while export:" + ex.message)
            return False
        except Exception as ex:
            self.log.error("error while export: " + ex.message)
            return False
        return True
    # endregion

    # region Function overrides read_from_file
    def read_from_file(self, import_path):
        """
        tries to read all important objects from file
        """
        try:
            infile = open(import_path + str(hash(self.filename)) + '.dat', 'rb')
            self.SOL, inf = Pickle.load(infile)
            infile.close()
            self.max_dofs = inf["max_dofs"]
            self.poly_sys = inf["polysys"]
            self.femdegree = inf["femdegree"]
            self.gpcps = inf["gpcps"]
            self.domain = inf["domain"]
            self.decayexp = inf["decayexp"]
            self.iterations = inf["iterations"]
            self.refinements = inf["refinements"]
            self.thetax = inf["thetax"]
            self.thetay = inf["thetay"]
            self.srank = inf["srank"]
            self.maxrank = inf["maxrank"]
            self.urank = inf["urank"]
            self.updater = inf["updater"]
            self.coefficients = inf["coefficients"]
            self.gamma = inf["gamma"]
            self.rvtype = inf["rvtype"]
            self.acc = inf["acc"]
            self.do_timing = inf["do_timing"]
            self.mesh_refine = inf["mesh_refine"]
            self.mesh_dofs = inf["mesh_dofs"]
            self.problem = inf["problem"]
            self.convergence = inf["convergence"]
            self.max_new_dim = inf["max_new_dim"]
            self.no_longtail_zeta_marking = inf["no_longtail_zeta_marking"]
            self.resnorm_zeta_weight = inf["resnorm_zeta_weight"]
            self.rank_always = inf["rank_always"]
            self.eta_zeta_weight = inf["eta_zeta_weight"]
            self.coeff_field_type = inf["coeff_field_type"]
            self.amp_type = inf["amp_type"]
            self.sampling_error_l2 = inf["sampling_error_l2"]
            self.sampling_error_h1 = inf["sampling_error_h1"]
            self.field_mean = inf["field_mean"]

            for i, S in enumerate(self.SOL):
                # restore FunctionSpace and MultiindexSet
                mesh = Mesh(import_path + str(hash(self.filename)) + '-mesh-%i.xml' % i)
                v = S["V"]
                S["CG"] = FunctionSpace(mesh, v.CG[0], v.CG[1])
                # enrich V for sampling
                # cos = MultiindexSet.createFullTensorSet(len(V.r) - 2, gpcps[i])
                # mis = [Multiindex(x) for x in cos.arr]
                # V.active_indices = lambda: mis
                v.polysys = self.poly_sys
                v.gpcps = S["gpcps"][i]
                v.V = S["CG"]
                v.mesh = mesh
                # restrict iteration # to maximal # dofs if requested
                # if (self.max_dofs > 0) and (S["DOFS"] > self.max_dofs) and (i > 0):
                #    self.SOL = self.SOL[:i]
                #    print "considering solution up to iteration %i with %i dofs..." % (i - 1, self.SOL[-1]["DOFS"])
        except IOError as ex:
            self.log.error("IO error while import: " + ex.message)
            return False
        except Exception as ex:
            self.log.error("error while import: " + ex.message)
            return False
        return True
    # endregion

    # region overrides Function check_validity
    def check_validity(self):
        """
        check validity of instance data TODO
        """
        return True
    # endregion

    # region Property filename
    @property
    def filename(self):
        """
        returns the file name according to the members of the current instance
        """
        filename = "asgfem_"
        filename += "mD%i" % self.max_dofs
        filename += "pS%s" % self.poly_sys
        filename += "fD%i" % self.femdegree
        filename += "gD%i" % self.gpcps
        filename += "dom%s" % self.domain
        filename += "dexp%3.2f" % self.decayexp
        filename += "it%i" % self.iterations
        filename += "ref%i" % self.refinements
        filename += "tX%3.2f" % self.thetax
        filename += "tY%3.2f" % self.thetay
        filename += "sR%i" % self.srank
        filename += "mR%i" % self.maxrank
        filename += "uR%i" % self.urank
        filename += "up%i" % self.updater
        # filename += "M%i" % self.coefficients
        filename += "g%i" % self.gamma
        filename += "rv%s" % self.rvtype
        filename += "ac%.2e" % self.acc
        filename += "mRef%i" % self.mesh_refine
        filename += "prob%i" % self.problem
        filename += "conv%.2e" % self.convergence
        filename += "mnd%i" % self.max_new_dim
        filename += "lz%i" % self.no_longtail_zeta_marking
        filename += "rzw%.2f" % self.resnorm_zeta_weight
        filename += "ezw%.2f" % self.eta_zeta_weight
        filename += "Ra%i" % self.rank_always
        filename += "cFT%s" % self.coeff_field_type
        filename += "aT%s" % self.amp_type
        filename += "cM%s" % self.field_mean
        return str(filename)
    # endregion

    # region Property get_coefficient_field
    @property
    def get_coefficient_field(self):
        """
        returns the coefficient field belonging to the current instance settings
        @return: Parametric coefficient field
        """

        return get_coefficient_field(self.coeff_field_type, self.amp_type, self.decayexp,
                                     self.gamma, self.rvtype, coeff_mean=self.field_mean)
    # endregion

    # region function get_proj_basis
    def get_proj_basis(self, sol_index=-1):
        """
        creates the projection basis
        @return: FEniCSBasis
        """
        if sol_index > len(self.SOL):
            raise AssertionError("get_proj_basis: sol_index out of range")
        if self.proj_basis is None:
            assert isinstance(self.SOL[sol_index]["CG"], FunctionSpace)
            self.proj_basis = FEniCSBasis(self.SOL[sol_index]["CG"])
        return self.proj_basis
    # endregion

    # region refine function space
    def refine_function_space(self, sol_index, dofs):
        fs_old = self.SOL[sol_index]["CG"]
        if dofs > 0:
            from dolfin import refine
            while self.SOL[sol_index]["CG"].dim() < dofs:
                self.SOL[sol_index]["CG"] = FunctionSpace(refine(self.SOL[sol_index]["CG"].mesh()), "CG",
                                                          self.femdegree)
        return fs_old
    # endregion

    # region Property get_pde
    @property
    def get_pde(self):
        """
          creates the ALEA PDE Object from the current instance
        @return:
        """
        #                                           # only needed for the boundaries entry. Contains just min and max
        _, boundaries, _ = SampleDomain.setupDomain(self.domain, initial_mesh_N=10)
        #                                           # create ALEA PDE from SampleProblem
        pde, _, _, _, _, _ = SampleProblem.setupPDE(2, self.domain, self.problem, boundaries,
                                                    self.get_coefficient_field)
        return pde
    # endregion

    # region Function compute_direct_sample_solution

    def compute_direct_sample_solution(self, rv_samples, sol_index=-1, cache=None):
        """
          creates the solution object from the parametric pde setting for a given set of samples
        @param rv_samples: list of samples to evaluate the coefficient field at
        @param sol_index: int
        @param cache: Dict
        @return: FEniCSVector
        """
        from alea.utils.tictoc import TicToc
        if sol_index > len(self.SOL):
            raise AssertionError("compute_direct_sample_solution: sol_index out of range")
        try:
            start_lhs = cache.A
            operator_m = cache.A_m
            b = cache.b
        except AttributeError:
            #                                       # get mean function from coefficient field
            a = self.get_coefficient_field.mean_func   
            #                                       # assemble lhs from pde
            start_lhs = self.get_pde.assemble_lhs(basis=self.get_proj_basis(sol_index=sol_index), coeff=a,
                                                  withDirichletBC=False)
            #                                       # assemble rhs from pde
            b = self.get_pde.assemble_rhs(basis=self.get_proj_basis(sol_index=sol_index), coeff=a,
                                          withDirichletBC=False)
            #                                       # assign A_m for cache usage
            operator_m = [None] * self.coefficients
            if cache is not None:
                cache.A = start_lhs
                cache.A_m = operator_m
                cache.b = b
        lhs = start_lhs.copy()                      # copy left hand side of the pde

        if cache is not None:
            with TicToc(key=" set cached string with rv samples", active=True, do_print=False):
                for m in range(len(self.SOL[sol_index]["V"].n)-1):
                    if operator_m[m] is None:
                        a_m = self.get_coefficient_field[m][0]
                        operator_m[m] = self.get_pde.assemble_lhs(basis=self.get_proj_basis(sol_index=sol_index),
                                                                  coeff=a_m, withDirichletBC=False)
                    lhs += rv_samples[m] * operator_m[m]
        else:

            def getcpp(coeff_exp):
                """
                Create C++ Code string
                @param coeff_exp: coefficient field entry
                @return: String
                """
                cpp = coeff_exp.cppcode
                return cpp.replace("A",
                                   str(coeff_exp.A)).replace("B",
                                                             str(coeff_exp.B)).replace("freq",
                                                                                       str(coeff_exp.freq)).replace(
                    "m", str(coeff_exp.m)).replace("n", str(coeff_exp.n))
            #                                           # create operator message
            # print("  create Operator message")
            a_ex_m = "+".join(["A" + str(m) + "*" + getcpp(self.get_coefficient_field[m][0])
                               for m in range(len(self.SOL[sol_index]["V"].n)-1)])
            #                                           # extend operator message
            # print("  extend Operator message")
            a_op_m = ",".join(["A%i=0" % m for m in range(len(self.SOL[sol_index]["V"].n)-1)])
            #                                           # create c++ Expression string
            eval_m = "Expression('%s', %s, degree=10)" % (a_ex_m, a_op_m)
            # print("  evaluate Expression")
            operator_m = eval(eval_m)                   # evaluate expression
            # print("  set attributes to Operator")
            for m in range(len(self.SOL[sol_index]["V"].n)-1):          # loop through coefficients
                # print("    m={}/{}".format(m, len(self.SOL[sol_index]["V"].n)-2))
                operatorm = "A%i" % m                   # create additional operators
                setattr(operator_m, operatorm, rv_samples[m])
            #                                           # add the left out operators to the lhs
            # print("  assemble lhs")
            lhs += self.get_pde.assemble_lhs(basis=self.get_proj_basis(sol_index=sol_index), coeff=operator_m,
                                             withDirichletBC=False)
        #                                           # apply BC
        # print("  apply BC")
        #

        with TicToc(key="apply BC", active=True, do_print=False):
            lhs, b = self.get_pde.apply_dirichlet_bc(self.get_proj_basis(sol_index=sol_index)._fefs, lhs, b)

        x = 0 * b                                   # assign solution vector
        # print("  solve system")

        # laback = parameters.linear_algebra_backend
        # parameters['linear_algebra_backend'] = 'Eigen'
        # parameters.linear_algebra_backend = "Eigen"

        # print("shape of lhs {}".format(as_backend_type(lhs).size(0)))
        # parameters['linear_algebra_backend'] = laback
        # parameters.linear_algebra_backend = laback

        with TicToc(key="solve", active=True, do_print=False):
            solve(lhs, x, b)                            # solve parametric linear equation system
        #                                           # create and return a fenics vector of the resulting solution
        with TicToc(key="project solution to function", active=True, do_print=False):
            retval = FEniCSVector(Function(self.get_proj_basis(sol_index=sol_index)._fefs, x))
        TicToc.sortedTimes()
        return retval
    # endregion

    # region Function estimate sampling Error
    def estimate_sampling_error(self, xi_samples=None, n_xi_samples=10, sol_index=-1, sol_index_ref=-1, relative=False):
        """
        samples the error between the calculated solution object and the true solution from
        'compute_direct_sample_solution' in a Monte-Carlo fashion. Used norms are the L2 and the H1 norms.
        @param xi_samples: list of samples to create the sampling error at
        @type xi_samples: list
        @param sol_index: index of the solution refinement. finest solution at -1
        @type sol_index: int
        @:type sol_index_ref:
        @return: L2-error, H1-error
        @rtype: float, float
        """
        from dolfin import set_log_level, ERROR
        set_log_level(ERROR)
        if not self.SOL:
            raise TypeError("estimate_xitsampling_error: Solution object is not given")
        if xi_samples is None:
            xi_samples = np.array([np.random.rand(self.coefficients)*2-1 for _ in range(n_xi_samples)])
        if not isinstance(xi_samples, list) and not isinstance(xi_samples, np.ndarray):
            raise TypeError("estimate_sampling_error: sample list is not loaded")
        if len(xi_samples) < 1:
            raise AssertionError("estimate_sampling_error: to few samples in list")
        if self.domain != "square":
            raise NotImplementedError
        if sol_index > len(self.SOL):
            raise AssertionError("estimate_sampling_error: sol_index out of range")
        if sol_index_ref > len(self.SOL):
            raise AssertionError("estimate_sampling_error: sol_index_ref out of range")

        from joblib import Parallel, delayed
        from dolfin.fem.norms import errornorm, norm
        # print "compare solution {} with refSolution {}".format(sol_index, sol_index_ref)
        global global_sol
        global_sol = self
        global global_errornorm
        global_errornorm = errornorm
        global global_norm
        global_norm = norm
        _ = sample_sol_parallel(xi_samples[0], sol_index, sol_index_ref, relative)
        arr = np.array(Parallel(50)(delayed(sample_sol_parallel)(xi, sol_index, sol_index_ref, relative)
                                    for xi in xi_samples))
        errl2 = arr[:, 0]
        errh1 = arr[:, 1]

        return errl2, errh1
    # endregion

    # region Function Sample at Coefficients
    def sample_at_coeff(self, rv_samples, sol_index=-1):
        """
        samples the tensor in the stochastic space at given coefficients and returns a Fenics Function
        We assume the original solution object here.
        @param rv_samples: list of parameters in stochastic space
        @type rv_samples: list
        @return: list of measurements at given space points
        @rtype: FEniCSVector
        """

        if not isinstance(rv_samples, list):
            self.log.error('sample_at_coeff: rv_samples is not a list')
            raise TypeError('sample_at_coeff: rv_samples is not a list')

        if sol_index > len(self.SOL):
            raise AssertionError("sample_at_coeff: sol_index out of range")

        from alea.application.egsz.sampling import compute_parametric_sample_solution_TT
        if True:
            samples = [rv_samples]
            #print "samples: {}".format(samples)
            #print "SOL[sol_index]: {}, norm={}".format(self.SOL[sol_index]["V"], tt.vector.norm(self.SOL[sol_index]["V"]))
            #print "SOL[-1]: {}, norm={}".format(self.SOL[-1]["V"], tt.vector.norm(self.SOL[-1]["V"]))
            #print "tensor mean: {}".format(self.SOL[sol_index]["V"][[0]*len(self.SOL[sol_index]["V"].n)])
            #print "length of SOL: {}".format(len(self.SOL))
            #print "sol_index={}".format(sol_index)
            #print "norm(U-V)= {}".format(tt.vector.norm(self.SOL[-1]["V"]-self.SOL[sol_index]["V"]))
            return compute_parametric_sample_solution_TT(samples,self.SOL[sol_index]["V"],self.get_proj_basis(sol_index=sol_index),1)[0]
        #                                           # get legendre polynomials and norms
        poly, norms = get_legendre_basis(max(self.SOL[sol_index]["V"].n[1:]))
        #                                           # get list of tensor representation
        list_of_components = tt.tensor.to_list(self.SOL[sol_index]["V"])
        #                                           # first tt component has to be a row or one-dim for every x and 0
        buff = list_of_components[0][0, :, :]       # get first component tensor = rank 2 tensor = matrix
        #                                           # enumerate through every component
        for comp_index in range(len(list_of_components)):

            if comp_index == 0:                     # skip the first one
                continue
            comp_value = list_of_components[comp_index]
            #                                       # reached last component
            if comp_index == len(list_of_components) - 1:
                #                                   # evaluate polynomials first
                buff_3 = np.array([comp_value[:, mu, 0] * poly[mu](rv_samples[comp_index - 1])
                                   for mu in range(self.SOL[sol_index]["V"].n[comp_index])])
                #                                   # evaluate sum over all polynomials
                buff_2 = np.sum(buff_3, axis=0)
                #                                   # multiply with previous row
                buff = np.dot(buff, buff_2)
                continue                            # continue for loop, or we could quit it here
            buff = np.dot(buff, np.sum(
                np.array([comp_value[:, mu, :] * poly[mu](rv_samples[comp_index - 1])
                          for mu in range(self.SOL[sol_index]["V"].n[comp_index])]
                         ), axis=0))                # as above but as a one liner

        sample_sol = Function(self.SOL[sol_index]["CG"])
        sample_sol.vector()[:] = buff

        return self.get_proj_basis(sol_index=sol_index).project_onto(FEniCSVector(sample_sol))

    # endregion

    # region Function Sample at Coordinates and Coefficients
    def sample_at_cord_and_coeff(self, rv_samples, coordinates, sol_index=-1):
        """
        samples the tensor in physic and stochastic at given coordinates and coefficients.
        NOTE: Do not normalize the polynomials
        @param rv_samples: list of parameters in stochastic space
        @type rv_samples: list
        @param coordinates: list of degrees of freedom
        @type coordinates: list
        @return: list of measurements at given space points
        @rtype: list
        """

        if not isinstance(rv_samples, list):
            self.log.error('sample_at_cord_and_coeff: rv_samples is not a list')
            raise TypeError('sample_at_cord_and_coeff: rv_samples is not a list')
        if not isinstance(coordinates, list):
            self.log.error('sample_at_cord_and_coeff: coordinates is not a list')
            raise TypeError('sample_at_cord_and_coeff: coordinates is not a list')

        if sol_index > len(self.SOL):
            raise AssertionError("sample_at_cord_and_coeff: sol_index out of range")
        measurements = []                           # create empty list
        #                                               # get legendre polynomials and norms
        poly, norms = get_legendre_basis(max(self.SOL[sol_index]["V"].n[1:]))
        #                                           # get list of tensor representation
        list_of_components = tt.tensor.to_list(self.SOL[sol_index]["V"])
        for i, x in enumerate(coordinates):         # enumerate through all coordinates
            #                                       # first tt component has to be a row or one-dim for every x and 0
            buff = list_of_components[0][0, :, :]   # get first component tensor = rank 2 tensor = matrix
            #                                       # enumerate through every component
            for comp_index in range(len(list_of_components)):

                if comp_index == 0:                 # ok, skip the first one
                    continue
                comp_value = list_of_components[comp_index]
                #                                   # reached last component
                if comp_index == len(list_of_components) - 1:
                    #                               # evaluate polynomials first
                    buff_3 = np.array([comp_value[:, mu, 0] * poly[mu](rv_samples[comp_index - 1])
                                       for mu in range(self.SOL[sol_index]["V"].n[comp_index])])
                    #                               # evaluate sum over all polynomials
                    buff_2 = np.sum(buff_3, axis=0)
                    #                               # multiply with previous row
                    buff = np.dot(buff, buff_2)
                    continue                        # continue for loop, or we could quit it here
                buff = np.dot(buff, np.sum(
                    np.array([comp_value[:, mu, :] * poly[mu](rv_samples[comp_index - 1])
                             for mu in range(self.SOL[sol_index]["V"].n[comp_index])]), axis=0)
                            )                       # as above but as a one liner

            sample_sol = Function(self.SOL[sol_index]["CG"])
            sample_sol.vector()[:] = buff

            vec = self.get_proj_basis(sol_index=sol_index).project_onto(FEniCSVector(sample_sol))

            measurements.append(vec(x))

        return measurements
    # endregion

global_sol = None
global_errornorm = None
global_norm = None

def sample_sol_parallel(xi, sol_index, sol_index_ref, relative):
    _sol = global_sol
    _errornorm = global_errornorm
    _norm = global_norm
    buffer_sol_direct = _sol.compute_direct_sample_solution(list(xi), sol_index=sol_index_ref)._fefunc
    buffer_sol_approx = _sol.sample_at_coeff(list(xi), sol_index=sol_index)._fefunc

    if relative:
        errL2, errH1 = _errornorm(buffer_sol_direct, buffer_sol_approx, norm_type="l2", degree_rise=0)/_norm(buffer_sol_direct, norm_type="l2"), _errornorm(buffer_sol_direct, buffer_sol_approx, norm_type="H1", degree_rise=0)/_norm(buffer_sol_direct, norm_type="h1")
    else:
        errL2, errH1 = _errornorm(buffer_sol_direct, buffer_sol_approx, norm_type="l2", degree_rise=0), _errornorm(buffer_sol_direct, buffer_sol_approx, norm_type="H1", degree_rise=0)
    return errL2, errH1
