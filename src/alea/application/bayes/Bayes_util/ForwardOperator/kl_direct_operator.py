# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  special forward operator taking the expansion of the coefficient as solution operator itself.
  G(u(x,y)) = a_0(x) + sum_{m=1}^M a_m(x)y_m
"""
# region Import
import cPickle as Pickle

import numpy as np
import tt

from alea.application.bayes.Bayes_util.Interface.IForwardOperator import IForwardOperator
from alea.application.egsz.coefficient_field import ParametricCoefficientField
# endregion


class KlDirectOperatorTT(IForwardOperator):
    """
      special forward operator taking the expansion of the coefficient as solution operator itself as TT-tensor
    """
    # region overwrites Function Constructor
    def __init__(self, coeff_field, physical_dim_start, coefficients):
        """
          instance constructor with properties
        """
        if not isinstance(coeff_field, ParametricCoefficientField):
            self.log.error("invalid coefficient field")
            raise TypeError
        if not isinstance(physical_dim_start, int):
            self.log.error("physical dimension is not an integer")
            raise TypeError
        if physical_dim_start < 10 or physical_dim_start > 1e10:
            self.log.error("physical dimension is not in a recommended area: %16f" % physical_dim_start)
            raise TypeError
        if not isinstance(coefficients, int):
            self.log.error("number of coefficients is not an integer")
            raise TypeError
        if coefficients < 1 or coefficients > 1e4:
            self.log.error("number of coefficients is not in a recommended area: %16f" % coefficients)
            raise TypeError
        IForwardOperator.__init__(self)
        self.physical_dim_start = physical_dim_start
        self.coefficients = coefficients
        self.sol = 0
        self.coeff_field = coeff_field
        self.stoch_grid = 0
        self.physical_grid = 0
    # endregion

    # region Function: create forward operator as additional constructor
    def create_forward_operator(self, stoch_grid, physical_grid):
        """
          instance constructor with properties
        """
        if len(stoch_grid) < 1 or max(stoch_grid) > 1e5:
            self.log.error("stochastic grid is not valid: {0}".format(stoch_grid))
            raise TypeError
        if len(physical_grid) < 1 or max(physical_grid) > 1e5:
            self.log.error("physical grid is not valid: {0}".format(stoch_grid))
            raise TypeError

        self.stoch_grid = stoch_grid
        self.physical_grid = physical_grid

        if not self.check_validity():
            self.log.error("failed validity check")
            raise RuntimeError

        list_of_components = []
        y = [self.stoch_grid for _ in range(self.coefficients)]
        component = np.zeros((1, self.physical_dim_start, self.coefficients + 1))
        for lia in range(self.coefficients + 1):
            for i, x in enumerate(self.physical_grid):
                component[0, i, lia] = self.coeff_field.funcs(lia)(x)
        list_of_components.append(component)

        for lia in range(self.coefficients - 1):
            component = np.zeros((self.coefficients + 1, len(y[lia]), self.coefficients + 1))
            for lic in range(len(y[lia])):
                eye = np.identity(self.coefficients + 1)
                eye[lia + 1, lia + 1] = y[lia][lic]
                component[:, lic, :] = eye

            list_of_components.append(component)

        component = np.ones((self.coefficients + 1, len(y[-1]), 1))
        for lia in range(len(y[-1])):
            component[-1, lia, 0] = y[-1][lia]
        list_of_components.append(component)
        self.sol = tt.tensor.from_list(list_of_components)

    # endregion

    # region overwrites Function write to file
    def write_to_file(self, export_path):
        """
          writes the items of the current instance into a file using pickling
          @param export_path: path to the file
          @type export_path: str
          @return: success
          @rtype: bool
        """
        if not isinstance(export_path, str):
            self.log.error("export path is not a valid string")
            return False
        if not self.check_validity():
            self.log.error("validity check failed")
            return False

        inf = {"physical_dim_start": self.physical_dim_start, "coefficients": self.coefficients}

        try:
            outfile = open(export_path + str(hash(self.filename)) + '.dat', 'wb')
            Pickle.dump((self.sol, inf), outfile, Pickle.HIGHEST_PROTOCOL)
            outfile.close()
            return True
        except IOError as ex:
            self.log.Warning("IO({0}) Error while writing in {2} error: {1}".format(ex.errno, ex.strerror, export_path +
                                                                                    str(hash(self.filename))))
        return False

    # endregion

    # region overwrites Function read from file
    def read_from_file(self, import_path):
        """
          tries to read the instance from a file
          @param import_path: path to file to read from
          @type import_path: str
          @return: success
          @rtype: bool
        """
        try:
            infile = open(import_path + str(hash(self.filename)) + '.dat', 'rb')
            self.sol, inf = Pickle.load(infile)
            infile.close()
            if "physical_dim_start" in inf:
                self.physical_dim_start = inf["physical_dim_start"]
            else:
                self.log.error("physical_dim_start is missing in file")
                return False
            if "coefficients" in inf:
                self.coefficients = inf["coefficients"]
            else:
                self.log.error("coefficients is missing in file")
                return False
        except IOError as ex:
            self.log.Warning("IO({0}) Error while opening {2} error: {1}".format(ex.errno, ex.strerror, import_path +
                                                                                 str(hash(self.filename))))
            return False
        return True

    # endregion

    # region overwrites Function check validity of current object instance
    def check_validity(self):
        """
          checks current instance member on plausibility
          @return: success
          @rtype: bool
        """
        if not isinstance(self.coeff_field, ParametricCoefficientField):
            self.log.error("invalid coefficient field")
            return False
        if not isinstance(self.physical_dim_start, int):
            self.log.error("physical dimension is not an integer")
            return False
        if self.physical_dim_start < 10 or self.physical_dim_start > 1e10:
            self.log.error("physical dimension is not in a recommended area: %16f" % self.physical_dim_start)
            return False
        if not isinstance(self.coefficients, int):
            self.log.error("number of coefficients is not an integer")
            return False
        if self.coefficients < 1 or self.coefficients > 1e4:
            self.log.error("number of coefficients is not in a recommended area: %16f" % self.coefficients)
            return False

        return True
    # endregion

    # region overwrites Property Filename
    @property
    def filename(self):
        """
        returns the filename used for pickling
        @return: str
        """
        filename = ""
        filename += "pD{}_".format(self.physical_dim_start)
        filename += "coeff{}".format(self.coefficients)
        return str(filename)
    # endregion
