from dolfin import (assemble, dx, TrialFunction, TestFunction, nabla_grad, parameters, inner, as_backend_type,
                    Function, Constant, DirichletBC, cells, Measure, CellFunction, nabla_div, CellSize)
import warnings
import numpy as np
from scipy import sparse as sps


def dof2patch_dof_map(fs):
    patch_list = []
    cell2dof = [fs.dofmap().cell_dofs(i) for i in range(fs.mesh().num_cells())]
    for d in fs.dofmap().dofs():
        patch = []
        for c in cell2dof:
            if d not in c:
                continue
            patch.append(c)
        patch_list.append(patch)
    dof2patch_dofs = []
    for patch_dofs in patch_list:
        curr_dofs = []
        for patch in patch_dofs:
            for dof in patch:
                if dof not in curr_dofs:
                    curr_dofs.append(dof)
        dof2patch_dofs.append(curr_dofs)
    return dof2patch_dofs


# region def calculate mass matrix
def calculate_mass_matrix(fs):

    _u = TrialFunction(fs)
    _v = TestFunction(fs)
    laback = parameters.linear_algebra_backend
    parameters['linear_algebra_backend'] = 'Eigen'
    parameters.linear_algebra_backend = "Eigen"

    #                                           # create bi linear form of mass matrix
    BF = inner(_u, _v) * dx
    M = assemble(BF)
    rows, cols, values = as_backend_type(M).data()
    # print("convert to sps matrix")
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        M = sps.csr_matrix((values, cols, rows))
    parameters['linear_algebra_backend'] = laback
    parameters.linear_algebra_backend = laback
    return M
# endregion


# region def calculate stiffness matrix
def calculate_stiffness_matrix(fs, with_bc=False):
    _u = TrialFunction(fs)
    _v = TestFunction(fs)
    laback = parameters.linear_algebra_backend
    parameters['linear_algebra_backend'] = 'Eigen'
    parameters.linear_algebra_backend = "Eigen"

    #                                           # create bi linear form of stiffness matrix
    BF = inner(nabla_grad(_u), nabla_grad(_v)) * dx
    S = assemble(BF)
    if with_bc is True:
        def u0_boundary(x, on_boundary):
            return on_boundary

        from dolfin import Constant, DirichletBC
        bc = DirichletBC(fs, Constant(0), u0_boundary)
        bc.apply(S)
    rows, cols, values = as_backend_type(S).data()
    # print("convert to sps matrix")
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        S = sps.csr_matrix((values, cols, rows))
    parameters['linear_algebra_backend'] = laback
    parameters.linear_algebra_backend = laback
    return S
# endregion

# region def create order 4 stiffness operator

def create_order4_stiffness(fs):
    retval = {}

    laback = parameters.linear_algebra_backend
    parameters['linear_algebra_backend'] = 'Eigen'
    parameters.linear_algebra_backend = "Eigen"

    def u0_boundary(x, on_boundary):
        return on_boundary

    dof2patch_dofs = dof2patch_dof_map(fs)

    u = TrialFunction(fs)
    v = TestFunction(fs)
    phi_k = Function(fs)
    phi_l = Function(fs)
    for lia, d in enumerate(fs.dofmap().dofs()):
        vec_k = np.zeros(fs.dim())
        vec_k[d] = 1
        phi_k.vector()[:] = vec_k
        for lic in dof2patch_dofs[lia]:

            vec_l = np.zeros(fs.dim())
            vec_l[lic] = 1
            phi_l.vector()[:] = vec_l
            a = assemble(inner(phi_l, inner(phi_k, inner(nabla_grad(u), nabla_grad(v))))*dx(fs.mesh()))

            bc = DirichletBC(fs, Constant(0), u0_boundary)
            bc.apply(a)

            rows, cols, values = as_backend_type(a).data()

            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                mat = sps.csr_matrix((values, cols, rows))
            mat.eliminate_zeros()
            if mat.nnz > 0:
                retval[(d, lic)] = mat

    parameters['linear_algebra_backend'] = laback
    parameters.linear_algebra_backend = laback

    return retval.items()
# endregion

# region def compute overall error indicator
def compute_overall_error_indicator(eta, zeta, resnorm):
    return eta + zeta + resnorm
# endregion

# region def create order 4 volume term operator

def create_order4_volume_operator_list(fs):
    retval = []
    laback = parameters.linear_algebra_backend
    parameters['linear_algebra_backend'] = 'Eigen'
    parameters.linear_algebra_backend = "Eigen"

    def u0_boundary(x, on_boundary):
        return on_boundary

    dm = fs.dofmap()
    u = TrialFunction(fs)
    v = TestFunction(fs)

    phi_k = Function(fs)
    phi_l = Function(fs)

    h = CellSize(fs.mesh())
    p = fs.ufl_element().degree()
    for c in cells(fs.mesh()):
        result_dict = {}
        subdomain = CellFunction('size_t', fs.mesh(), 0)
        subdomain[c] = 1
        dA = Measure("dx", subdomain_data=subdomain)
        for d1 in dm.cell_dofs(c.index()):
            k_vec = np.zeros(fs.dim())
            k_vec[d1] = 1
            phi_k.vector()[:] = k_vec
            for d2 in dm.cell_dofs(c.index()):
                # if (d2, d1) in result_dict:
                #     continue
                l_vec = np.zeros(fs.dim())
                l_vec[d2] = 1
                phi_l.vector()[:] = l_vec

                a = assemble((h**2 * p**(-2)) * inner(nabla_div(phi_k * nabla_grad(u)), nabla_div(phi_l * nabla_grad(v))) * dA())
                bc = DirichletBC(fs, Constant(0), u0_boundary)
                bc.apply(a)

                rows, cols, values = as_backend_type(a).data()

                with warnings.catch_warnings():
                    warnings.simplefilter("ignore")
                    mat = sps.csr_matrix((values, cols, rows))
                mat.eliminate_zeros()
                result_dict[(d1, d2)] = mat
        retval.append(result_dict.items())
    parameters['linear_algebra_backend'] = laback
    parameters.linear_algebra_backend = laback

    return retval
    # endregion
