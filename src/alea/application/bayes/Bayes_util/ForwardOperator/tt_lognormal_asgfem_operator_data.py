"""
implementation of the forward operator (solution operator) of the lognormal problem considering a tensor train
decomposition of the coefficient field.
"""
# region imports
from dolfin import File, Mesh
from alea.application.tt_asgfem.tensorsolver.tt_sgfem_util import tt_dofs, tt_op_dofs, tt_op_tensor_dofs
from alea.application.tt_asgfem.tensorsolver.tt_lognormal import sample_lognormal_tt
from alea.application.bayes.Bayes_util.ForwardOperator.tt_forward_operator_util import compute_overall_error_indicator
import tt
import os
import cPickle as pickle
from copy import deepcopy
# endregion

FILE_PATH = os.path.dirname(__file__) + "/tmp/"


class TTLognormalAsgfemOperatorData(object):
    # region init
    def __init__(self,
                 ):
        """
        constructor
        """
        self.path = None
        self.filename = None

        self.iterations = None
        self.tolerance = None
        self.preconditioner = None

        self.solver_iterations = None
        self.solver_local_error_list = None
        self.solver_conv_list = None
        self.det_estimator_result = None

        self.max_norms = None
        self.theta = None
        self.rho = None
        self.solution = None
        self.mesh = None
        self.hdegs = None
        self.ranks = None

        self.theta_x = None
        self.theta_y = None
        self.resnorm_zeta_weight = None
        self.eta_zeta_weight = None
        self.rank_always = None
        self.sol_rank = None
        self.start_rank = None
        self.new_hdeg = None

        self.new_solution = None
        self.new_mesh = None
        self.new_hdegs = None
        self.new_ranks = None

        self.eta_global = None
        self.zeta_global = None
        self.resnorm = None

        self.dofs = None
        self.m_dofs = None
        self.overall_error = None

        self.coef_sample_path = None
        self.cont_coef_hermite_degree = None
        self.cont_coef_ranks = None
        self.cont_coef_m_dofs = None
        self.sto_estimator_result = None
        self.res_estimator_result = None
        self.estimator_result = None

        self.operator = None
        self.op_dofs = None
        self.op_ten_dofs = None

        self.sol_sampling_result = None
        self.coef_sampling_result = None
    # endregion

    # region def sample forward solution
    def sample_solution(self,
                        y                           # list
                        ):
        """
        simple function to sample the solution by calling a given sampling method
        :param y: sample in stochastic space
        :return: list of node evaluations
        """
        return sample_lognormal_tt(self.solution, y, self.max_norms, theta=self.theta,
                                   rho=self.rho)
    # endregion

    # region load
    def load(self, filename):
        """
        loads a pickled file that contains the solution tensor cores
        :return: boolean
        """
        try:
            self.path = FILE_PATH + str(filename)
            self.filename = str(filename)
            f = open(self.path + "sol.dat", 'rb')
            tmp_dict = pickle.load(f)
            f.close()
            self.solution = tmp_dict['solution']
            self.iterations = tmp_dict['iterations']
            self.tolerance = tmp_dict['tolerance']
            self.preconditioner = tmp_dict['preconditioner']

            self.max_norms = tmp_dict['max_norms']
            self.theta = tmp_dict['theta']
            self.rho = tmp_dict['rho']
            self.mesh = Mesh(self.path + "sol-mesh.xml")
            self.hdegs = tmp_dict['hdegs']
            self.ranks = tmp_dict['ranks']

            self.theta_x = tmp_dict['theta_x']
            self.theta_y = tmp_dict['theta_y']
            self.resnorm_zeta_weight = tmp_dict['resnorm_zeta_weight']
            self.eta_zeta_weight = tmp_dict['eta_zeta_weight']
            self.rank_always = tmp_dict['rank_always']
            self.sol_rank = tmp_dict['sol_rank']
            self.start_rank = tmp_dict['start_rank']
            self.new_hdeg = tmp_dict['new_hdeg']

            self.solver_iterations = tmp_dict['solver_iterations']
            self.solver_local_error_list = tmp_dict['solver_local_error_list']
            self.solver_conv_list = tmp_dict['solver_conv_list']
            self.det_estimator_result = tmp_dict['det_estimator_result']

            self.new_solution = tmp_dict['new_solution']
            self.new_mesh = Mesh(FILE_PATH + str(filename) + "new-sol-mesh.xml")
            self.new_hdegs = tmp_dict['new_hdegs']
            self.new_ranks = tmp_dict['new_ranks']

            self.eta_global = tmp_dict['eta_global']
            self.zeta_global = tmp_dict['zeta_global']
            self.resnorm = tmp_dict['resnorm']

            self.dofs = tt_dofs(tt.vector.from_list(self.solution))

            try:
                self.m_dofs = tmp_dict["m_dofs"]
            except Exception:
                self.m_dofs = self.solution[0].shape[1]
            try:
                self.coef_sample_path = tmp_dict["coef_sample_path"]
            except Exception:
                self.coef_sample_path = None
            try:
                self.cont_coef_hermite_degree = tmp_dict['coef_hdegs']
                self.cont_coef_ranks = tmp_dict['coef_ranks']
                self.cont_coef_m_dofs = tmp_dict['coef_m_dofs']
            except Exception:
                self.cont_coef_hermite_degree = None
                self.cont_coef_ranks = None
                self.cont_coef_m_dofs = None
            try:
                self.operator = tmp_dict['operator']
                self.op_dofs = tt_op_dofs(self.operator)
                self.op_ten_dofs = tt_op_tensor_dofs(self.operator)
            except Exception:
                self.operator = None
                self.op_dofs = 0
                self.op_ten_dofs = 0
            try:
                self.coef_sampling_result = tmp_dict["coef_sampling_result"]
                self.sol_sampling_result = tmp_dict["sol_sampling_result"]
            except Exception:
                self.coef_sampling_result = None
                self.sol_sampling_result = None
                print("no sol or coef sampling results: {}".format(self.filename))

            self.sto_estimator_result = tmp_dict["sto_estimator_result"]
            self.res_estimator_result = tmp_dict["res_estimator_result"]
            self.estimator_result = deepcopy(self.det_estimator_result)
            self.estimator_result.update(self.sto_estimator_result)
            self.estimator_result.update(self.res_estimator_result)
            self.overall_error = compute_overall_error_indicator(self.eta_global, self.zeta_global, self.resnorm)
        except Exception as ex:
            print("!!!!! {} exception: {} \n    {}".format(__name__, ex.message, ex))
            return False
        return True
    # endregion

    # region def update methods
    def update_coef_sampling(self, update_dict=None):
        try:
            f = open(self.path + "sol.dat", 'rb')
            tmp_dict = pickle.load(f)
            f.close()
            if update_dict is not None:
                self.coef_sampling_result = update_dict
            tmp_dict["coef_sampling_result"] = self.coef_sampling_result
            f = open(self.path + "sol.dat", 'wb')
            pickle.dump(tmp_dict, f, 2)
            f.close()
        except Exception as ex:
            print("!!!!! {} exception: {} \n    {}".format(__name__, ex.message, ex))
            return False
        return True

    def update_sol_sampling(self, update_dict=None):
        try:
            f = open(self.path + "sol.dat", 'rb')
            tmp_dict = pickle.load(f)
            f.close()
            if update_dict is not None:
                self.sol_sampling_result = update_dict
            tmp_dict["sol_sampling_result"] = self.sol_sampling_result
            f = open(self.path + "sol.dat", 'wb')
            pickle.dump(tmp_dict, f, 2)
            f.close()
        except Exception as ex:
            print("!!!!! {} exception: {} \n    {}".format(__name__, ex.message, ex))
            return False
        return True
    # endregion
