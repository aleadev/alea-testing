
from __future__ import division
import unittest
from alea.application.bayes.alea_util.configuration.asgfem_config import AsgfemConfig
from alea.application.bayes.alea_util.coefficient_field.tensor.tt_lognormal_semidiscrete_field import \
    read_coef_from_config
from dolfin import UnitSquareMesh, FunctionSpace, inner, dx, assemble, TestFunction, Constant, DirichletBC, \
     Expression, TrialFunction, Function, cells, nabla_grad, as_backend_type
from scipy import sparse as sps
from alea.application.bayes.Bayes_util.ForwardOperator.tt_lognormal_asgfem_operator import TTLognormalAsgfemOperator
from random import randint
from numpy import ravel, linalg, reshape
from alea.application.tt_asgfem.tensorsolver.tt_sparse_matrix import smatrix
import numpy as np
from alea.polyquad.polynomials import StochasticHermitePolynomials
from alea.application.egsz.sample_domains import SampleDomain
from alea.application.egsz.sample_problems2 import SampleProblem


class TestTTLognormalAsgfemOperator(unittest.TestCase):
    # region def setUp
    def setUp(self):
        problem_file = "../../../alea_util/configuration/problems/default.pro"
        config = AsgfemConfig(problem_file)
        if config.read_configuration() is False:
            exit()
        config.n_coefficients = 1
        config.num_coeff_in_coeff = 1
        config.hermite_degree = 3
        config.femdegree = 1
        config.init_mesh_dofs = 3
        config.theta = 0
        semi_discrete_coef = read_coef_from_config(config)

        mesh, boundaries, dim = SampleDomain.setupDomain(config.domain, initial_mesh_N=2)

        mesh = UnitSquareMesh(config.init_mesh_dofs, config.init_mesh_dofs)

        self.pde, _, _, _, _, _ = SampleProblem.setupPDE(2, config.domain, 0, boundaries, semi_discrete_coef)

        curr_fs = FunctionSpace(mesh, 'CG', config.femdegree)
        curr_hdeg = [config.hermite_degree] * config.n_coefficients

        self.curr_forward_operator = TTLognormalAsgfemOperator(semi_discrete_coef, curr_fs, curr_hdeg, config.domain)
        pass
    # endregion

    # region def test lhs
    def test_lhs(self):
        self.assertIsInstance(self.curr_forward_operator.tt_bilinearform, smatrix,
                              "tt linearform is of type: {}".format(type(self.curr_forward_operator.tt_bilinearform)))

        Att = self.curr_forward_operator.tt_bilinearform.full()

        n = self.curr_forward_operator.tt_bilinearform.n
        hdegs = self.curr_forward_operator.hdegs
        if len(hdegs) == 1:
            Att = reshape(Att, [n[0], (hdegs[0]), n[0], (hdegs[0])],
                          order='F')
        elif len(hdegs) == 2:
            Att = reshape(Att, [n[0], (hdegs[0]), (hdegs[1]), n[0], (hdegs[0]), (hdegs[1])],
                          order='F')
        else:
            exit()
        #compute_test_FE_matrix(self.curr_forward_operator.discrete_coef.sample_continuous_field, Att, hdegs,
        #                       self.curr_forward_operator.discrete_coef.mesh,
        #                       self.curr_forward_operator.cont_coef.theta, self.curr_forward_operator.cont_coef.rho,
        #                       self.curr_forward_operator.cont_coef.max_norms,
        #                       degree=self.curr_forward_operator.discrete_coef.femdegree)
    # endregion

    # region def test sampling methods
    def test_sampling(self):
        y_list = [self.curr_forward_operator.cont_coef.sample_rvs(5) for _ in range(10)]
        mp = self.curr_forward_operator.discrete_coef.fs.tabulate_dof_coordinates().reshape((-1,2))
        for y in y_list:
            slow_sample, slow_coef = self.curr_forward_operator.sample_analytic_solution_slow(y, self.pde, reference_m=len(y))
            fast_sample, fast_coef = self.curr_forward_operator.sample_analytic_solution(y, self.pde, reference_m=len(y))
            for x in mp:
                self.assertLessEqual(np.linalg.norm(fast_sample(x) - slow_sample(x)), 1e-10)
    # endregion

    # region def test rhs
    def test_rhs(self):
        self.assertIsInstance(self.curr_forward_operator.tt_linearform, list,
                              "tt linearform is of type: {}".format(type(self.curr_forward_operator.tt_linearform)))

        def boundary(x, on_boundary):
            return on_boundary
        f = Constant(1.0)
        v = TestFunction(self.curr_forward_operator.discrete_coef.fs)
        lf = assemble(inner(f, v)*dx())
        bc = DirichletBC(self.curr_forward_operator.discrete_coef.fs, Constant(0.0), boundary)
        bc.apply(lf)
        lf = lf.array()
        cores = self.curr_forward_operator.tt_linearform
        d = [core.shape[1] for core in cores]
        for k in range(10):
            vec = [randint(0, d[i]-1) for i in range(1, len(d))]
            right = cores[-1][:, int(vec[-1]), 0]
            for i in range(len(vec) - 1, 0, -1):
                left = cores[i][:, int(vec[i - 1]), :]
                right = left.dot(right)
            left = cores[0][0, :, :]
            right = ravel(left.dot(right))
            if max(vec) == 0:
                self.assertLessEqual(linalg.norm(lf - right), 1e-10, "error of linearform and true load vector to large"
                                                                     "at index: {} \n load:\n {} \n tt_load: \n {}"
                                                                     .format(vec, lf, right))
            else:
                self.assertAlmostEqual(linalg.norm(right), 0, "error of linearform and true load vector to large"
                                                              "at index: {} \n load:\n {} \n tt_load: \n {}"
                                                              .format(vec, lf, right))
    # endregion

    # region def test polynomials
    def test_polynomials(self):
        from alea.application.tt_asgfem.tensorsolver.tt_lognormal import hermite_triprod
        P, W = np.polynomial.hermite_e.hermegauss(100)
        Herm = StochasticHermitePolynomials(normalised=True)
        # tau = [np.exp(-self.theta * self.rho * bm) for bm in self.bmax]
        H = lambda mu, nu, k, y: (Herm.eval(nu, y) * Herm.eval(mu, y) * Herm.eval(k, y)) * np.sqrt(2*np.pi)**(-1)
        w = lambda y: np.exp(-0.5*y ** 2)
        for mu in range(3):
            for nu in range(3):
                for k in range(10):
                    # print("H({}, {}, {}, {}) = {}".format(mu, nu, k, P[10], H(nu, mu, k, P[10])))

                    value = sum([_w * H(nu, mu, k, _y) for _y, _w in zip(P, W)])
                    exact = hermite_triprod(mu, nu, k)
                    self.assertLessEqual(np.linalg.norm(value - exact), 1e-10)
    # endregion

def compute_test_FE_matrix(a, Aop, hdeg, mesh, theta, rho, bmax, degree=1, withDirichletBC=True):

    def u0_boundary(x, on_boundary):
        return on_boundary


    def zeta(theta, rho, alpha_m, x):
        retval = (sigma(theta, rho, alpha_m)) ** (-1) * np.exp(
            -0.5 * ((sigma(theta, rho, alpha_m) ** (-2)) - 1) * x ** 2)
        return retval

    def sigma(theta, rho, alpha_m):
        return np.exp(theta * rho * alpha_m)

    class HermiteCoefficient(Expression):

        def set_params(self, _nu, _mu, _c, theta, rho, bmax, hgdeg=200):
            self.P, self.W = np.polynomial.hermite.hermgauss(hgdeg)
            assert len(_nu) == len(_mu)
            self.nu = _nu
            self.mu = _mu
            self.c = _c
            self.theta = theta
            self.rho = rho
            self.bmax = bmax
            # print "MAX of c1", np.max(c1.vector().array())

        def eval(self, value, x):
            w = lambda y: np.exp(y**2/2)
            H = StochasticHermitePolynomials(normalised=True)
            tau = [np.exp(-self.theta*self.rho*bm) for bm in self.bmax]
            H12 = lambda y: self.c(np.array([x]), y) * \
                            np.prod([H.eval(_nu, tau[_lia]*_y) * H.eval(_mu, tau[_lia]*_y) * (1/np.sqrt(2*np.pi))
                                     for _lia, (_nu, _mu, _y) in
                                     enumerate(zip(self.nu, self.mu, y))])
            if len(self.nu) == 1:
                value[0] = sum([_w * H12([_y]) * w(_y) * zeta(theta, rho, bmax[0], _y)for _y, _w in zip(self.P, self.W)])
            elif len(self.nu) == 2:
                value[0] = 0
                for y2, ww2 in zip(self.P, self.W):
                    value[0] += sum([ww1 * ww2 * H12([y1, y2]) * w(y1) * w(y2) *
                                     zeta(theta, rho, bmax[0], y1) * zeta(theta, rho, bmax[1], y2) for y1, ww1 in
                                     zip(self.P, self.W)])
            else:
                exit()


    # setup FE functions
    V = FunctionSpace(mesh, 'CG', degree)
    u, v = TrialFunction(V), TestFunction(V)
    bc = DirichletBC(V, Constant(0), u0_boundary)
    dof2val = bc.get_boundary_values()
    bc_dofs = dof2val.keys()

    # setup piecewise constant coefficient
    # DG0 = FunctionSpace(mesh, 'DG', 0)
    # dm = DG0.dofmap()
    # c2d = [dm.cell_dofs(i)[0] for i in range(mesh.num_cells())]
    # c0, c1, c2, cc, c12 = Function(DG0), Function(DG0), Function(DG0), Function(DG0), Function(DG0)
    # c12 = Function(V)
    # get cell midpoints
    # mp = np.reshape(V.tabulate_dof_coordinates(), (-1, 2))
    # evaluate and set coefficient
    # use Eigen backend for conversion to scipy sparse matrices
    from dolfin import parameters
    laback = parameters.linear_algebra_backend
    parameters.linear_algebra_backend = "Eigen"

    # evaluate FEM matrices
    AA = None
    AA2 = None
    H12 = HermiteCoefficient(element=V.ufl_element())

    if len(hdeg) == 1:
        for nu in range(hdeg[0]):
            for mu in range(hdeg[0]):
                # set coefficients
                H12.set_params([nu], [mu], a, theta, rho, bmax)
                C12 = H12
                # assemble matrix
                A2 = assemble(C12 * inner(nabla_grad(u), nabla_grad(v)) * dx)
                # convert and store matrix
                rows2, cols2, values2 = as_backend_type(A2).data()
                # A0 = sps.csr_matrix((values, cols, rows))
                A02 = sps.csr_matrix((values2, cols2, rows2))

                # modify boundary dof entries
                if withDirichletBC:
                    # if nu1 == 0 and nu2 == 0:
                    for i in bc_dofs:
                        # J = cols[rows[i]:rows[i+1]]
                        J2 = cols2[rows2[i]:rows2[i + 1]]
                        # I = [[i]] * len(J)
                        I2 = [[i]] * len(J2)
                        # A0[I,J] = 0
                        # A0[J,I] = 0
                        A02[I2, J2] = 0
                        A02[J2, I2] = 0

                print "--------------------------------"
                print "TestMatrix:", nu, mu
                err = np.linalg.norm(Aop[:, nu, :, mu] - A02.todense())
                print("NormDiff: {}".format(err))
                if err >= 1e-5:
                    print("Att: \n {}".format(Aop[:, nu, :, mu]))
                    print("A: \n {}".format(A02.todense()))
                print "--------------------------------"

                # UU[:,nu1,nu2] = [H12(p) for p in mp]
                # UU[:,nu2,nu1] = [H12(p) for p in mp]
                if AA == None:
                    # AA = np.zeros([A0.shape[0],hdeg+1,A0.shape[1],hdeg+1])
                    AA2 = np.zeros([A02.shape[0], hdeg[0] + 1, A02.shape[1], hdeg[0] + 1])
                # AA[:,nu1,:,nu2] = A0.todense()
                # AA[:,nu2,:,nu1] = A0.todense()
                AA2[:, nu, :, mu] = A02.todense()
                AA2[:, mu, :, nu] = A02.todense()
    elif len(hdeg) == 2:
        for nu1 in range(hdeg[0]):
            for nu2 in range(nu1+1):
                for nu3 in range(hdeg[1]):
                    for nu4 in range(nu3+1):
                        # set coefficients
                        # cc.vector()[:] = U0[:,nu1,nu2][c2d]
                        # evaluate H12 at midpoints
                        H12.set_params([nu1, nu2], [nu3, nu4], a, theta, rho, bmax)
                        # evaluate and set coefficient
                        # c12.vector()[:] = np.array([H12(p) for p in mp])
                        # if False:
                        #     C12 = c12   # piecewise constant
                        # else:
                        #     C12 = H12   # continuous
                        C12 = H12

                        # assemble matrix
                        # A, B = assemble_system(cc * inner(nabla_grad(u), nabla_grad(v)) * dx, Expression('1') * v * dx, bc)
                        A2 = assemble(C12 * inner(nabla_grad(u), nabla_grad(v)) * dx)
                        # BB = assemble(Expression('1') * v * dx)

                        # apply bc
                        # bc.apply(A)
                        # bc.apply(A2)

                        # convert and store matrix
                        # rows, cols, values = as_backend_type(A).data()
                        rows2, cols2, values2 = as_backend_type(A2).data()
                        # A0 = sps.csr_matrix((values, cols, rows))
                        A02 = sps.csr_matrix((values2, cols2, rows2))

                        # modify boundary dof entries
                        if withDirichletBC:
                            # if nu1 == 0 and nu2 == 0:
                            for i in bc_dofs:
                                # J = cols[rows[i]:rows[i+1]]
                                J2 = cols2[rows2[i]:rows2[i+1]]
                                # I = [[i]] * len(J)
                                I2 = [[i]] * len(J2)
                                # A0[I,J] = 0
                                # A0[J,I] = 0
                                A02[I2,J2] = 0
                                A02[J2,I2] = 0

                        # print "*"*80
                        # print "direct:", np.linalg.norm(A0.todense() - A02.todense())
                        # print "Hdiff:", [c2(p) - H12(p) for p in mp]
                        # np.set_printoptions(threshold=np.nan, precision=2, linewidth=np.nan, suppress=True)
                        # print bc_dofs
                        # print BB.array()
                        # print B.array() - BB.array()
                        # print A0.todense() - A02.todense()
                        # if nu1 == 0:
                        #     if nu2 == 0:
                        #         adiag = np.around(np.diag(A0.todense() - A02.todense()),decimals=2)
                        #         print adiag
                        # np.set_printoptions(threshold=1000, precision=8, linewidth=75)
                        # print "*"*80

                        print "--------------------------------"
                        print "TestMatrix:", nu1, nu2, nu3, nu4
                        print "NormDiff:", np.linalg.norm(Aop[:,nu1,nu3,:,nu2,nu4] - A02.todense())
                        print "--------------------------------"

                        # UU[:,nu1,nu2] = [H12(p) for p in mp]
                        # UU[:,nu2,nu1] = [H12(p) for p in mp]
                        if AA == None:
                            # AA = np.zeros([A0.shape[0],hdeg+1,A0.shape[1],hdeg+1])
                            AA2 = np.zeros([A02.shape[0],hdeg[0]+1,hdeg[1]+1,A02.shape[1],hdeg[0]+1,hdeg[1]+1])
                        # AA[:,nu1,:,nu2] = A0.todense()
                        # AA[:,nu2,:,nu1] = A0.todense()
                        AA2[:,nu1,nu3,:,nu2,nu4] = A02.todense()
                        AA2[:,nu2,nu3,:,nu1,nu4] = A02.todense()
                        AA2[:,nu1,nu4,:,nu2,nu3] = A02.todense()
                        AA2[:,nu2,nu4,:,nu1,nu3] = A02.todense()

    # print "ComponentTest:", np.linalg.norm(U0 - UU)

    # restore backend
    parameters.linear_algebra_backend = laback

    return AA2,


if __name__ == '__main__':
    unittest.main()
