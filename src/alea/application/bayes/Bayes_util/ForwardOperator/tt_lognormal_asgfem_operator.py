"""
implementation of the forward operator (solution operator) of the lognormal problem considering a tensor train
decomposition of the coefficient field.
"""
# region imports
from copy import deepcopy
from dolfin import FunctionSpace, File

import numpy as np
from alea.application.bayes.alea_util.coefficient_field.tensor.tt_lognormal_semidiscrete_field import \
    TTLognormalSemidiscreteField
from numpy.testing import assert_almost_equal       # gets the almost equal operator for numpy arrays
from scipy import sparse as sps

from alea.application.bayes.alea_util.coefficient_field.tensor.tt_lognormal_fullydiscrete_field import \
    TTLognormalFullyDiscreteField
from alea.application.bayes.alea_util.solver.tt_sparse_als import TTSparseALS
from alea.application.egsz.tt_tail_estimator import ttMark_y
from alea.application.tt_asgfem.apps.sgfem_als_util import compute_FE_matrices, compute_test_FE_matrix
from alea.application.tt_asgfem.tensorsolver.tt_lognormal import generate_operator_tt
from alea.application.tt_asgfem.tensorsolver.tt_lognormal import sample_lognormal_tt
from alea.application.tt_asgfem.tensorsolver.tt_residual import TTResidual
from alea.application.tt_asgfem.tensorsolver.tt_sgfem_util import tt_add_stochastic_dimension, increase_tt_rank
from alea.application.tt_asgfem.tensorsolver.tt_sparse_matrix import smatrix
from alea.application.tt_asgfem.tensorsolver.tt_util import has_full_rank
import tt
import os
import cPickle as pickle
# endregion

FILE_PATH = os.path.dirname(__file__) + "/tmp/"


class solution_cache:
    def __init__(self):
        self.str = None


class TTLognormalAsgfemOperator(object):
    # region init
    def __init__(self,
                 coef,                              # type: TTLognormalSemidiscreteField
                 fs,                                # type: FunctionSpace
                 hdegs,                             # type: list
                 domain,                            # type: str
                 empty=False                        # type: bool
                 ):
        """
        constructor
        :param coef: continuous coefficient
        :param fs: function space containing the mesh and finite element discretisation
        """
        self.cont_coef = coef
        self.fs = fs
        self.discrete_coef = TTLognormalFullyDiscreteField(self.cont_coef, self.fs, domain, empty=empty)
        self.hdegs = hdegs

        self.coef_sample_path = None

        self.ranks = None
        self.iterations = None
        self.tolerance = None
        self.preconditioner = None

        self.tt_bilinearform = None
        self.tt_linearform = None
        self.solution = None

        self.theta_x = None
        self.theta_y = None
        self.resnorm_zeta_weight = None
        self.eta_zeta_weight = None
        self.rank_always = None
        self.sol_rank = None
        self.start_rank = None
        self.new_hdeg = None

        self.new_solution = None
        self.new_mesh = None
        self.new_hdegs = None
        self.new_ranks = None
        self.eta_global = None
        self.zeta_global = None
        self.resnorm = None

        self.solver = None

        self.det_estimator_result = None
        self.res_estimator_result = None
        self.sto_estimator_result = None

        self.solved = False
        self.computed = False
        self.refined = False
        if empty is False:
            self._create_discrete_operator()

        self.computed = False
        if empty is False:
            self._create_rhs()
    # endregion

    # region private def create discrete operator
    def _create_discrete_operator(self):
        #                                           # compute FE matrices for piecewise constant coefficients
        A0, bc_dofs, BB, CG = compute_FE_matrices(self.discrete_coef[0][0, :, :], self.discrete_coef.mesh,
                                                  degree=self.discrete_coef.femdegree, n_cpu=40)
        n_coeff = len(self.hdegs)
        if n_coeff > len(self.discrete_coef) - 1:
            raise ValueError("Dimension of solution exhibits dimension of coefficient -> exit()")

        opcores = generate_operator_tt(self.cont_coef.get_cont_cores(), self.discrete_coef.ranks, self.hdegs,
                                       len(self.hdegs))
        D = A0[0].shape[0]                          # get size of first matrix
        opcores[0] = []                             # re_init first operator core (it was None anyway)
        # A = None
        for r0, A in enumerate(A0):
            opcores[0].append(A)
            # print "A0", A.toarray()
            # print "midpoints", first_comp[:,r0]
            # print "eigsA0", np.linalg.eig(A.toarray())

        # generate lognormal operator tensor

        A = smatrix(opcores)

        bcopcores = (len(self.hdegs) + 1) * [[]]
        # bcdense = np.zeros([D, D])
        # bcdense[bc_dofs, bc_dofs] = 1
        bcsparse_direct = sps.csr_matrix((D, D))
        bcsparse_direct[bc_dofs, bc_dofs] = 1
        bcopcores[0] = []
        bcopcores[0].append(bcsparse_direct)
        for i in range(n_coeff):
            bcopcores[i + 1] = np.reshape(np.eye(opcores[i + 1].shape[1], opcores[i + 1].shape[2]),
                                          [1, opcores[i + 1].shape[1], opcores[i + 1].shape[2], 1], order='F')
        BCOP = smatrix(bcopcores)

        A += BCOP
        A = A.round(1e-16)  # ! important: do not cut ranks here. Just orthogonalize cores[1:]
        self.tt_bilinearform = A
        self.tt_linearform = BB
        self.computed = True

        if False:
            Aop = A.full()
            Aop = np.reshape(Aop, [A.n[0], (self.hdegs[0]), (self.hdegs[1]), A.n[0], (self.hdegs[0]), (self.hdegs[1])],
                             order='F')

            #############################################
            ####### Smaller Problem Tests ###############
            #############################################

            # # -Eigenvalue-Tester-For-Small-Problems-----------------------------
            Amat = A.full()
            #
            # eigs1, vecs = np.linalg.eig(Amat)
            # for s in range(eigs1.shape[0]):
            #     print eigs1[s]
            # ------------------------------------------------------------------

            # negvec = vecs[:,0]
            # E = np.reshape(negvec,A.n, order='F')
            # print E.shape
            # E = tt.vector(E, 1E-14)
            # print 'Testeig:', tt.dot(E,(A.matvec(E)))
            # U0 = np.zeros([cores[0].shape[1],gpcdegree+1,gpcdegree+1])
            # for i in range(cores[0].shape[1]):
            #     for nu1 in range(gpcdegree+1):
            #         for nu2 in range(gpcdegree+1):
            #             U0[i,nu1,nu2] = sum([cores[0][0,i,k]*opcores[1][k,nu1,nu2,0] for k in range(cores[0].shape[2])])
            Afull = compute_test_FE_matrix(self.cont_coef.affine_field[0][1],
                                           self.cont_coef.affine_field[0][0][0],
                                           self.cont_coef.affine_field[1][1],
                                           self.cont_coef.affine_field[1][0][0],
                                           self.cont_coef.affine_field[2][1],
                                           self.cont_coef.affine_field[2][0][0], Aop, self.hdegs[0], self.discrete_coef.mesh, degree=self.discrete_coef.femdegree)
            # Afull = np.reshape(Afull,[A.n[0]*(self.hdegs[0]),A.n[0]*(self.hdegs[1])], order='F')
            # eigs3 = np.linalg.eig(Afull)[0]
            # print "eigs3:", eigs3

            # Aerr = Afull - Amat
            # np.set_printoptions(threshold=np.nan)
            # print "Aerr:", Aerr

            # Atest = reshape(Atest,[A.n[0]*(gpcdegree+1),A.n[0]*(gpcdegree+1)])
            # eigs4 = np.linalg.eig(Atest)[0]
            # print "eigs4:", eigs4

            # Atesterr = Atest - Amat
            # print "Atesterr:", Atesterr
            # np.set_printoptions(threshold=4)

            # np.set_printoptions(threshold=np.nan, precision=2, linewidth=np.nan, suppress=True)
            # print Afull
            # np.set_printoptions(threshold=1000, precision=8, linewidth=75)
            Afull = np.reshape(Afull,[A.n[0],(self.hdegs[0]),(self.hdegs[1]),A.n[0],(self.hdegs[0]),(self.hdegs[1])],
                               order='F')
            Afull = np.transpose(Afull,axes=(0,3,1,4,2,5))
            Afull = np.reshape(Afull,[A.n[0]*A.n[0],(self.hdegs[0])*(self.hdegs[1]),(self.hdegs[0])*(self.hdegs[1])],
                               order='F')
            Afull = tt.matrix(Afull)
            afullcores = tt.matrix.to_list(Afull)
            core = []
            for r0 in range(afullcores[0].shape[3]):
                core.append(sps.csr_matrix(afullcores[0][0,:,:,r0]))
            afullcores[0] = core
            Afull = smatrix(afullcores)

            Afull = Afull + BCOP
            Afull = Afull.round(1e-14)
            print "AfullOp:", Afull
            Opdiff = A - Afull
            print "Opdiff:", Opdiff.norm()
            Afullmat = Afull.full()
            # A = Afull
            #
            # np.set_printoptions(threshold=np.nan, precision=2, linewidth=np.nan, suppress=True)
            # # print Atest - Amat
            # np.set_printoptions(threshold=1000, precision=8, linewidth=75)
            #
            print "FullError:", np.linalg.norm(Afullmat - Amat)
            # # print "TestError:", np.linalg.norm(Atest - Amat)
            # # print "TestError2:", np.linalg.norm(Atest - Afullmat)
            # # ------------------------------------------------------------------
            #
            # assert False
            exit()
    # endregion

    # region private def create rhs
    def _create_rhs(self):
        assert(self.tt_linearform is not None)
        assert (self.tt_bilinearform is not None)

        self.tt_linearform = np.reshape(self.tt_linearform, [1, self.tt_bilinearform.n[0], 1], order='F')
        self.tt_linearform = [self.tt_linearform] + len(self.hdegs) * [[]]
        for i in range(len(self.hdegs)):
            self.tt_linearform[i + 1] = np.reshape(np.eye(self.tt_bilinearform.n[i + 1], 1),
                                                   [1, self.tt_bilinearform.n[i + 1], 1],
                                                   order='F')
        self.computed = True

    # endregion

    # region def solve
    def solve(self,
              rank,                                 # type: list
              preconditioner,                       # type: str
              iterations,                           # type: int
              tolerance,                            # type: float
              init_value=None,                      # type: list
              ):
        """
        startes the solving process
        :param rank: desired rank
        :param preconditioner: name of the used preconditioner
        :param iterations: number of iterations in the solution process
        :param tolerance: desired tolerance
        :param init_value: prescribed start value
        """
        self.ranks = rank
        self.iterations = iterations
        self.tolerance = tolerance
        self.preconditioner = preconditioner

        # if self.load() is True:
        #     self.solved = True
        #     return
        self.solver = TTSparseALS(self.tt_bilinearform, self.tt_linearform, rank, als_iterations=iterations,
                                  convergence=tolerance)
        if max(rank) == 1:
            self.solution = self.solver.solve(start_rank_one=True, preconditioner={"name": preconditioner,
                                                                                   "coef": self.discrete_coef,
                                                                                   "operator": self})
        elif init_value is None or init_value == []:
            self.solution = self.solver.solve(preconditioner={"name": preconditioner,
                                                              "coef": self.discrete_coef,
                                                              "operator": self})
        else:

            self.solution = self.solver.solve(preconditioner={"name": preconditioner,
                                                              "coef": self.discrete_coef,
                                                              "operator": self},
                                              init_value=init_value, normalize_init=True)
        self.solved = True
        self.solved = self.save()
    # endregion

    # region def sample forward solution
    def sample_solution(self,
                        y                           # list
                        ):
        """
        simple function to sample the solution by calling a given sampling method
        :param y: sample in stochastic space
        :return: list of node evaluations
        """
        if self.solved is False:
            raise ValueError("solution is not created yet. Start solver first")
        return sample_lognormal_tt(self.solution, y, self.discrete_coef.max_norms, theta=self.discrete_coef.theta,
                                   rho=self.discrete_coef.rho)
    # endregion

    # region def sample analytic forward solution
    def sample_analytic_solution(self, y, pde, cache=None, reference_m=100, expression_degree=5, ref_fs=None):
        from alea.fem.fenics.fenics_basis import FEniCSBasis
        from dolfin import solve, Expression
        a_0 = self.cont_coef.mean_func

        if cache is None or cache.str is None:
            def getcpp(coeff_exp):
                cpp = coeff_exp.cppcode
                return cpp.replace("A", str(coeff_exp.A)).replace("B", str(coeff_exp.B)).replace("freq", str(
                    coeff_exp.freq)).replace("m", str(coeff_exp.m)).replace("n", str(coeff_exp.n))
                # or a_0.B as before? In log normal case a_0.A fits much better, since it is the mean

            a_ex_m = "+".join([str(a_0.A)] + ["A" + str(m) + "*" +
                                              getcpp(self.cont_coef.affine_field[m+1][0][0]) for m in
                                              range(reference_m)])
            a_A_m = ",".join(["A%i=0" % m for m in range(reference_m)])
            a_ex_m = "exp(" + a_ex_m + ")"

            cell_str = ',' if range(reference_m - 1) > 0 else ''
            cell_str += " cell='triangle', degree={}".format(expression_degree)

            eval_m = "Expression('%s', %s)" % (a_ex_m, a_A_m + cell_str)
            # print("=" * 80)
            # print(a_ex_m)
            # print("+" * 80)
            # print(a_A_m)
            # print("+" * 80)
            # print(eval_m)
            # print("=" * 80)
            if cache is not None:
                cache.str = eval_m
            exp_a = eval(eval_m)
            for m in range(reference_m):
                Am = "A%i" % m
                setattr(exp_a, Am, y[m])
        else:
            # print("string cache found")
            exp_a = eval(cache.str)
            for m in range(reference_m):
                Am = "A%i" % m
                setattr(exp_a, Am, y[m])

        if ref_fs is None:
            basis = FEniCSBasis(self.discrete_coef.fs)
        else:
            basis = FEniCSBasis(ref_fs)

        b = pde.assemble_rhs(basis=basis, coeff=exp_a, withDirichletBC=False)
        A = pde.assemble_lhs(basis=basis, coeff=exp_a, withDirichletBC=False)
        A, b = pde.apply_dirichlet_bc(basis._fefs, A, b)

        X = 0 * b
        # logger.info("compute_direct_sample_solution with %i dofs" % b.size())
        solve(A, X, b)
        return X.array(), exp_a

    def sample_analytic_solution_slow(self, y, pde, cache=None, reference_m=100, expression_degree=5, ref_fs=None):
        from alea.fem.fenics.fenics_basis import FEniCSBasis
        from dolfin import solve, Function, Expression

        # NOTE: this was replaced by the above code
        a = self.cont_coef.affine_field(np.reshape(self.discrete_coef.fs.tabulate_dof_coordinates(), (-1, 2)), y)
        # a = a_0
        # for m in range(reference_m):
        #     a += y[m] * y[m][0]
        exp_a = Function(self.discrete_coef.fs)
        exp_a.vector()[:] = np.exp(a)

        if ref_fs is None:
            basis = FEniCSBasis(self.discrete_coef.fs)
        else:
            basis = FEniCSBasis(ref_fs)

        b = pde.assemble_rhs(basis=basis, coeff=exp_a, withDirichletBC=False)
        A = pde.assemble_lhs(basis=basis, coeff=exp_a, withDirichletBC=False)
        A, b = pde.apply_dirichlet_bc(basis._fefs, A, b)

        X = 0 * b
        solve(A, X, b)
        return X.array(), exp_a
    # endregion

    # region def sample analytic forward solution with discrete coefficient
    def sample_analytic_solution_discrete_coef(self, y, pde, ref_fs=None):
        from alea.fem.fenics.fenics_basis import FEniCSBasis
        from dolfin import solve, Function, Expression
        # if self.solved is False:
        #     raise ValueError("solution is not constructed yet")
        if ref_fs is None:
            dofs = np.reshape(self.discrete_coef.fs.tabulate_dof_coordinates(), (-1, 2))
        else:
            dofs = np.reshape(ref_fs.tabulate_dof_coordinates(), (-1, 2))
        # exp_a = self.discrete_coef(dofs, y)
        exp_a = self.discrete_coef(dofs, y)
        if ref_fs is None:
            exp_a_fun = Function(self.discrete_coef.fs)
            basis = FEniCSBasis(self.discrete_coef.fs)
        else:
            exp_a_fun = Function(ref_fs)
            basis = FEniCSBasis(ref_fs)
        exp_a_fun.vector()[:] = exp_a

        b = pde.assemble_rhs(basis=basis, coeff=exp_a_fun, withDirichletBC=False)
        A = pde.assemble_lhs(basis=basis, coeff=exp_a_fun, withDirichletBC=False)
        A, b = pde.apply_dirichlet_bc(basis._fefs, A, b)

        X = 0 * b
        # logger.info("compute_direct_sample_solution with %i dofs" % b.size())
        solve(A, X, b)

        # if return_coef is True:
        #     from dolfin import project, Expression
        #     return FEniCSVector(Function(proj_basis(sol_index=-1)._fefs, X)), interpolate(exp_a, proj_basis(
        #         sol_index=-1)._fefs)
        return X.array(), exp_a_fun
    # endregion

    # region def sample analytic forward solution with semi discrete coefficient
    def sample_analytic_solution_continuous_coef(self, y, pde, ref_fs=None):
        from alea.fem.fenics.fenics_basis import FEniCSBasis
        from dolfin import solve, Function
        # if self.solved is False:
        #     raise ValueError("solution is not constructed yet")
        if ref_fs is None:
            dofs = np.reshape(self.discrete_coef.fs.tabulate_dof_coordinates(), (-1, 2))
        else:
            dofs = np.reshape(ref_fs.tabulate_dof_coordinates(), (-1, 2))
        # exp_a = self.discrete_coef(dofs, y)
        exp_a = self.cont_coef(dofs, y)
        if ref_fs is None:
            exp_a_fun = Function(self.discrete_coef.fs)
            basis = FEniCSBasis(self.discrete_coef.fs)
        else:
            exp_a_fun = Function(ref_fs)
            basis = FEniCSBasis(ref_fs)
        exp_a_fun.vector()[:] = exp_a

        b = pde.assemble_rhs(basis=basis, coeff=exp_a_fun, withDirichletBC=False)
        A = pde.assemble_lhs(basis=basis, coeff=exp_a_fun, withDirichletBC=False)
        A, b = pde.apply_dirichlet_bc(basis._fefs, A, b)

        X = 0 * b
        # logger.info("compute_direct_sample_solution with %i dofs" % b.size())
        solve(A, X, b)

        # if return_coef is True:
        #     from dolfin import project, Expression
        #     return FEniCSVector(Function(proj_basis(sol_index=-1)._fefs, X)), interpolate(exp_a, proj_basis(
        #         sol_index=-1)._fefs)
        return X.array(), exp_a_fun
    # endregion

    # region def get_filename
    def get_filename(self):
        """
        returns an artificial file name of the current object containing 'unique' informations about the operator
        :return: string
        """
        filename = "TTLognormalAsgfemOperator"
        filename += "discrete_field:{}".format(self.discrete_coef.get_filename())
        filename += "hdegs:{}".format(self.hdegs)
        filename += "ranks:{}".format(self.ranks)
        filename += "iterations:{}".format(self.iterations)
        filename += "tolerance:{}".format(self.tolerance)
        filename += "preconditioner:{}".format(self.preconditioner)
        return filename
    # endregion

    # region def get_hash
    def get_hash(self):
        """
        returns the hash of the filename
        :return: string
        """
        return hash(self.get_filename())
    # endregion

    # region save forward solution
    def save(self):
        if self.solved is False:
            raise ValueError(" can not save operator if solver was not successfully run")
        try:
            retval = dict()
            retval['solution'] = self.solution
            f = open(FILE_PATH + str(self.get_hash()) + "sol.dat", 'wb')
            pickle.dump(retval, f, 2)
            f.close()
        except Exception as ex:
            print("!!!!! {} exception: {} \n    {}".format(__name__, ex.message, ex))
            return False
        return True
    # endregion

    # region load
    def load(self):
        """
        loads a pickled file that contains the solution tensor cores
        :return: boolean
        """
        try:
            f = open(FILE_PATH + str(self.get_hash()) + "sol.dat", 'rb')
            tmp_dict = pickle.load(f)
            f.close()
            self.solution = tmp_dict['solution']
        except Exception as ex:
            # print("!!!!! {} exception: {} \n    {}".format(__name__, ex.message, ex))
            return False
        return True
    # endregion

    # region save all parameters to a given hash file
    def save_to_hash(self, filename):
        if self.solved is False:
            raise ValueError(" can not save operator if not solved already")
        if self.refined is False:
            raise ValueError(" can not save operator refinement parameters if not already refined")
        try:

            retval = dict()
            retval['solution'] = self.solution
            retval['ranks'] = self.ranks
            retval['iterations'] = self.iterations
            retval['tolerance'] = self.tolerance
            retval['preconditioner'] = self.preconditioner
            retval['max_norms'] = self.discrete_coef.max_norms
            retval['theta'] = self.discrete_coef.theta
            retval['rho'] = self.discrete_coef.rho
            retval['hdegs'] = self.hdegs
            retval['theta_x'] = self.theta_x
            retval['theta_y'] = self.theta_y
            retval['resnorm_zeta_weight'] = self.resnorm_zeta_weight
            retval['eta_zeta_weight'] = self.eta_zeta_weight
            retval['rank_always'] = self.rank_always
            retval['sol_rank'] = self.sol_rank
            retval['start_rank'] = self.start_rank
            retval['new_hdeg'] = self.new_hdeg
            retval['new_solution'] = self.new_solution
            retval['new_hdegs'] = self.new_hdegs
            retval['new_ranks'] = self.new_ranks
            retval['eta_global'] = self.eta_global
            retval['zeta_global'] = self.zeta_global
            retval['resnorm'] = self.resnorm
            retval['solver_iterations'] = self.solver.actual_iterations
            retval['solver_local_error_list'] = self.solver.local_error_list
            retval['solver_conv_list'] = self.solver.first_comp_convergence_list
            retval['det_estimator_result'] = self.det_estimator_result
            retval['m_dofs'] = self.fs.dim()
            retval['coef_sample_path'] = self.coef_sample_path
            retval['coef_hdegs'] = self.cont_coef.hermite_degree
            retval['coef_ranks'] = self.cont_coef.ranks
            retval['coef_m_dofs'] = self.discrete_coef.dofs
            retval["sto_estimator_result"] = self.sto_estimator_result
            retval["res_estimator_result"] = self.res_estimator_result
            retval["operator"] = self.tt_bilinearform

            filename_ad = "theta_x:{}".format(self.theta_x)
            filename_ad += "theta_y:{}".format(self.theta_y)
            filename_ad += "resnormzetaweight:{}".format(self.resnorm_zeta_weight)
            filename_ad += "etazetaweight:{}".format(self.eta_zeta_weight)
            filename_ad += "rank_always:{}".format(self.rank_always)
            filename_ad += "sol_rank:{}".format(self.sol_rank)
            filename_ad += "start_rank:{}".format(self.start_rank)
            filename_ad += "new_hdeg:{}".format(self.new_hdeg)
            filename_ad = str(hash(filename_ad))
            new_filename = "{}{}".format(self.get_hash(), filename_ad)

            f = open(filename, 'a')
            f.write("\n{}".format(new_filename))
            f.close()

            File(FILE_PATH + new_filename + "sol-mesh.xml") << self.discrete_coef.mesh
            File(FILE_PATH + new_filename + "new-sol-mesh.xml") << self.new_mesh
            f = open(FILE_PATH + new_filename + "sol.dat", 'wb')
            pickle.dump(retval, f, 2)
            f.close()

        except Exception as ex:
            print("!!!!! {} exception: {} \n    {}".format(__name__, ex.message, ex))
            return False
        return True
    # endregion

    # region def refine adaptive
    def refine_adaptive(self,
                        theta_x,                    # type: float
                        theta_y,                    # type: float
                        resnorm_zeta_weight,        # type: float
                        eta_zeta_weight,            # type: float
                        rank_always,                # type: bool
                        sol_rank,                   # type: int
                        start_rank,                 # type: int
                        new_hdeg,                   # type: int
                        order4_stiffness_tensor=None,
                        order4_volume_tensor_list=None,
                        refinement_cache=None,
                        reference_m=None,
                        refine_sto_uniform=False,
                        bound_sol_degree=None,
                        mesh_once=False,
                        sto_once=False
                        ):
        if self.solved is False:
            raise ValueError("solution is not created yet. Start solver first")
        # region Init operator values needed for caching
        self.theta_x = theta_x
        self.theta_y = theta_y
        self.resnorm_zeta_weight = resnorm_zeta_weight
        self.eta_zeta_weight = eta_zeta_weight
        self.rank_always = rank_always
        self.sol_rank = sol_rank
        self.start_rank = start_rank
        self.new_hdeg = new_hdeg
        # endregion
        last_refined = None
        # region Read values from existing cache
        if refinement_cache is not None:
            self.res_estimator_result = refinement_cache["res_estimator_result"]
            self.det_estimator_result = refinement_cache["det_estimator_result"]
            self.sto_estimator_result = refinement_cache["sto_estimator_result"]

            last_refined = refinement_cache["last"]
        else:
            self.res_estimator_result = dict()
            self.det_estimator_result = dict()
            self.sto_estimator_result = dict()
        # endregion
        ranks = [core.shape[0] for core in self.solution] + [1]
        # region Init residual object
        residual = TTResidual(deepcopy(self.cont_coef.get_cont_cores()), deepcopy(self.solution), self.tt_linearform,
                              self.discrete_coef.max_norms,
                              self.discrete_coef.femdegree, lognormal=True, mesh=self.discrete_coef.mesh,
                              af_type=self.discrete_coef.affine_field.coeff_type,
                              amp_type=self.discrete_coef.affine_field.amptype,
                              decay_exp_rate=self.discrete_coef.affine_field.decayexp,
                              sgfem_gamma=self.discrete_coef.affine_field.gamma,
                              freq_scale=self.discrete_coef.affine_field.freqscale,
                              freq_skip=self.discrete_coef.affine_field.freqskip,
                              scale=self.discrete_coef.affine_field.scale,
                              theta=self.discrete_coef.theta, rho=self.discrete_coef.rho,
                              order4_stiffness_tensor=order4_stiffness_tensor,
                              order4_volume_tensor_list=order4_volume_tensor_list)
        # endregion
        # region residual estimator
        #                      	                # calculate residual mean in energy norm
        self.res_estimator_result = residual.evaluate_resnorm(self.tt_bilinearform, self.tt_linearform,
                                                              self.discrete_coef.theta,
                                                              self.discrete_coef.rho,
                                                              self.discrete_coef.max_norms)
        # print("ALS residual = {}:".format(resnorm))
        # endregion
        # region deterministic estimator
        # print("theta x: {}".format(theta_x))
        # print("last_refined: {}".format(last_refined))
        if theta_x > 0 and (last_refined == "det" or last_refined is None):
            # with TicToc(sec_key="det-error-estimator", key="**** TT deterministic residual estimator ****",
            #             active=True, do_print=False):
            self.det_estimator_result = residual.eval_deterministic_error_estimator(self.discrete_coef.affine_field,
                                                                                    bmax=self.discrete_coef.max_norms,
                                                                                    theta=self.discrete_coef.theta,
                                                                                    rho=self.discrete_coef.rho,
                                                                                    femdegree=self.discrete_coef.
                                                                                    femdegree,
                                                                                    debug=False)

            # print("eta_global={}".format(eta_res_global))
            # print("rhs_global={}".format(rhs_global))
            # print("zero_global={}".format(zero_global))
            # print("sq_global={}".format(sq_global))
            # print("jump_global={}".format(jump_global))

            # assert_almost_equal(eta_res_global, np.sqrt(sum(eta_res_local ** 2)))
            # print eta_global, np.sqrt(sum(summed_eta_local**2))
            # assert_almost_equal(eta_global, np.sqrt(sum(summed_eta_local ** 2)))
        elif theta_x <= 0:
            self.det_estimator_result["eta_global"] = 0
            self.det_estimator_result["eta_local"] = 0

        # endregion
        # region stochastic error estimator
        # print("theta y: {}".format(theta_y))
        if theta_y > 0 and (last_refined == "sto" or last_refined is None):
            # with TicToc(sec_key="sto-error-estimator", key="**** TT stochastic residual estimator ****",
            #             active=True, do_print=False):
            self.sto_estimator_result = residual.eval_stochastic_error_estimator(self.discrete_coef.affine_field,
                                                                                 bmax=self.discrete_coef.max_norms,
                                                                                 theta=self.discrete_coef.theta,
                                                                                 rho=self.discrete_coef.rho,
                                                                                 femdegree=self.discrete_coef.femdegree,
                                                                                 reference_m=reference_m)

            #                                       # add all evaluated tail indicators to indicator set
            self.sto_estimator_result["zeta_list"].update(self.sto_estimator_result["zeta_tail_list"])
            #   		                            # calculate global zeta value
            self.sto_estimator_result["zeta_global_sum"] = np.sqrt(sum([z ** 2 for z in
                                                                        self.sto_estimator_result["zeta_list"].values()
                                                                        ]))
            # print zeta_res_local
            # print("zeta_res_local={}".format(zeta_res_local))
            print("zeta_sum = {} <= {} = zeta_global".format(self.sto_estimator_result["zeta_global_sum"],
                                                             self.sto_estimator_result["global_zeta"]))
            # print("   !!!! use zeta_sum from now on")
            # buf = self.sto_estimator_result["zeta_global_sum"]
            # self.sto_estimator_result["zeta_global_sum"] = self.sto_estimator_result["global_zeta"]
            # self.sto_estimator_result["global_zeta"] = buf
            # print("zeta_tail_list= {}".format(zeta_tail_list))
        elif theta_y <= 0:
            self.sto_estimator_result["global_zeta"] = 0
        # endregion
        # region decide for refinement step
        rank_done = False
        if self.sto_estimator_result["global_zeta"] >= resnorm_zeta_weight * self.res_estimator_result["resnorm"] \
                or rank_always or max(ranks) >= sol_rank or rank_done:
            refine_deterministic = eta_zeta_weight * self.det_estimator_result["eta_global"] > \
                                   self.sto_estimator_result["global_zeta"]
            refine_stochastic = not refine_deterministic
            if refine_deterministic or refine_stochastic:
                rank_done = True
        if self.sto_estimator_result["global_zeta"] >= resnorm_zeta_weight * self.res_estimator_result["resnorm"] \
                or rank_always or max(ranks) >= sol_rank or rank_done:
            refine_deterministic = eta_zeta_weight * self.det_estimator_result["eta_global"] > \
                                   self.sto_estimator_result["global_zeta"]
            refine_stochastic = not refine_deterministic
            if refine_deterministic or refine_stochastic:
                rank_done = True
        elif eta_zeta_weight * self.det_estimator_result["eta_global"] >= resnorm_zeta_weight * \
                self.res_estimator_result["resnorm"] \
                or max(ranks) >= sol_rank or rank_done:
            refine_deterministic = True
            refine_stochastic = False
            if refine_deterministic or refine_stochastic:
                rank_done = True
        else:  # no refinement
            refine_deterministic = False
            refine_stochastic = False
            rank_done = False
        # check if rank of solution is already full
        # print(has_full_rank(self.solution))
        if has_full_rank(self.solution) and rank_done is False:
            rank_done = True
            refine_deterministic = False
            refine_stochastic = True
            print("  refine stochastic since solution has already full rank and rank update was proposed?!")
        if rank_always is True:
            rank_done = False
        if mesh_once:
            refine_deterministic = True
            refine_stochastic = False
            rank_done = True
        if sto_once:
            refine_deterministic = False
            refine_sto_uniform = True
            refine_stochastic = True
            rank_done = True
        print("refine det: {}".format(refine_deterministic))
        print("refine sto: {}".format(refine_stochastic))
        print("rank done: {}".format(rank_done))
        # if last_refinement_sto is True and refine_stochastic is True \
        #         and last_refinement_sto_estimator <= zeta_global:
        #         refine_stochastic = False
        #         refine_deterministic = True
        #         print("refine physic instead of stochastic, since zeta got worse.")
        # endregion

        mesh = self.discrete_coef.fs.mesh()
        hdegs = [deg for deg in self.hdegs]

        solution = [core for core in residual.solution]
        # region refine mesh, stochastic index set or solution rank
        #                                   # deterministic adaptive refinement
        if theta_x > 0 and refine_deterministic:
            solution, mesh = refine_mesh_deterministic_log(self.det_estimator_result["eta_global"],
                                                           self.det_estimator_result["eta_local"],
                                                           self.discrete_coef.fs,
                                                           theta_x, solution,
                                                           self.discrete_coef.femdegree)
            if isinstance(solution, tt.vector):
                print("new mesh dofs: {}".format(solution.n[0]))
            else:
                print("new mesh dofs: {}".format(solution[0].shape[1]))
            refined = "det"
        # # stochastic adaptive refinement
        if isinstance(solution, tt.vector):
            loc_sol = tt.vector.to_list(solution)
        else:
            loc_sol = solution
        if theta_y > 0 and refine_stochastic:
            if bound_sol_degree > 1:
                for lia in range(len(loc_sol)-1):
                    if loc_sol[lia+1].shape[1] >= bound_sol_degree:
                        print("bound sol degree: {} at lia={}, since shape={}".format(bound_sol_degree, lia,
                                                                                      loc_sol[lia+1].shape[1]))
                        print("  zeta_list: {}".format(self.sto_estimator_result["zeta_list"]))
                        self.sto_estimator_result["zeta_list"][lia] = 0.0
                        print("  new zeta_list: {}".format(self.sto_estimator_result["zeta_list"]))
            if not refine_sto_uniform:
                #                               # use only zeta sum, since the global zeta can not be reached
                new_dim, marked_zeta, not_marked_zeta_ym = ttMark_y(self.sto_estimator_result["zeta_list"],
                                                                    self.sto_estimator_result["zeta_tail_list"],
                                                                    theta_y,
                                                                    self.sto_estimator_result["zeta_global_sum"], False)
                # print(new_dim)
                # print(marked_zeta)
                # print("prev solution: {}".format(tt.vector.from_list(residual.solution)))
            else:
                new_dim = []
                for lia, core in enumerate(loc_sol):
                    assert (len(core.shape) == 3)
                    if bound_sol_degree > 1 and lia > 0 and core.shape[1] >= bound_sol_degree:
                        continue
                    new_dim.append(lia)


            solution, hdegs, M = tt_add_stochastic_dimension(tt.vector.from_list(residual.solution),
                                                             new_dim, hdegs, start_rank,
                                                             new_gpcd=new_hdeg)
            print("new hdegs:{}".format(hdegs))
            refined = "sto"
            # print("post solution: {}".format(residual.solution))
        if isinstance(solution, list):
            solution = tt.vector.from_list(solution)
        ranks = solution.r
        if not rank_done:
            if max(ranks) < sol_rank and not refine_deterministic and not refine_stochastic:
                solution, rank = increase_tt_rank(solution, None, None, None, acc=1e-18)
                print("increase rank to {} since resnorm dominates".format(rank))
                refined = "rank"
            elif max(ranks) < sol_rank and rank_always:
                solution, rank = increase_tt_rank(solution, None, None, None, acc=1e-18)
                print("increase rank to {}, since we always increase the rank".format(rank))
                # refined = "rank"
        if len(ranks) > 2 and min(ranks[1:-1]) == 1:
            solution, rank = increase_tt_rank(solution, None, None, None, acc=1e-18)
            print("increase rank to {}, since there is one rank = 1".format(rank))
            refined = "rank"
        if max(solution.r) > start_rank:
            ranks = solution.r
        else:
            ranks = [1] + [start_rank]*len(solution.n) + [1]

        solution = tt.vector.to_list(solution)
        print("hdegs = {}".format(tt.vector.from_list(solution).n))
        print("ranks = {}".format(tt.vector.from_list(solution).r))
        print("eta = {}".format(self.det_estimator_result["eta_global"]))
        print("zeta = {}".format(self.sto_estimator_result["global_zeta"]))
        print("resnorm = {} \n".format(self.res_estimator_result["resnorm"])
              + "  algebraic resnorm = {} \n".format(self.res_estimator_result["resnorm_alg"])
              + "  normalized algebraic resnorm = {}".format(self.res_estimator_result["resnorm_alg_norm"]))
        # endregion
        self.new_solution = solution
        self.new_mesh = mesh
        self.new_hdegs = hdegs
        self.new_ranks = ranks
        self.eta_global = self.det_estimator_result["eta_global"]
        self.zeta_global = self.sto_estimator_result["global_zeta"]
        self.resnorm = self.res_estimator_result["resnorm"]
        self.new_ranks = ranks
        self.refined = True
        retval = {"det_estimator": self.det_estimator_result,
                  "sto_estimator": self.sto_estimator_result,
                  "res_estimator": self.res_estimator_result,
                  "solution": solution, "mesh": mesh, "hdegs": hdegs, "ranks": ranks, "refined": refined
                  }
        return retval
    # endregion

    # region def compute trace over stochastic space of tt operator
    def extract_trace(self):
        if self.tt_bilinearform is None:
            raise ValueError("  bilinearform is None")
        if not isinstance(self.tt_bilinearform, smatrix):
            raise ValueError("  bilinearform is not a sparse tensor train operator")

        return self.tt_bilinearform.trace()

    # endregion

    # region def compute trace over stochastic space of tt operator
    def extract_stochastic_dimension(self):
        if self.tt_bilinearform is None:
            raise ValueError("  bilinearform is None")
        if not isinstance(self.tt_bilinearform, smatrix):
            raise ValueError("  bilinearform is not a sparse tensor train operator")
        retval = 1
        for lia in range(len(self.tt_bilinearform.cores)):
            if lia == 0:
                continue
            retval *= self.tt_bilinearform.cores[lia].shape[1]
        return retval

    # endregion

# region def refine mesh deterministic for the log normal case
def refine_mesh_deterministic_log(eta_global, summed_eta_local, CG, thetaX, V, _femdegree):
    """
    refine the mesh adaptive
    @param eta_global:
    @param summed_eta_local:
    @param CG:
    @param thetaX:
    @param V:
    @param _femdegree:
    @return:
    """
    from operator import itemgetter  # enables items to pass the __getitem__() method
    from dolfin import MeshFunction, Function, refine, interpolate, FunctionSpace
    from alea.utils.tictoc import TicToc
    # sort error indicators
    eta_local_ind = [(x, i) for i, x in enumerate(summed_eta_local)]
    eta_local_ind = sorted(eta_local_ind, key=itemgetter(0), reverse=True)

    # setup marking set
    mesh = CG.mesh()
    cell_markers = MeshFunction("bool", mesh, mesh.topology().dim())
    cell_markers.set_all(False)
    marked_eta, cc = 0.0, 0

    for eta_cell in eta_local_ind:
        # break if sufficiently many cells are selected
        if thetaX * eta_global ** 2 <= marked_eta:
            break
        cell_markers[eta_cell[1]] = True
        marked_eta += eta_cell[0] ** 2
        cc += 1
    # print "+++++++ cells marked: %i/%i" % (cc, mesh.num_cells()), "\theta_global", eta_global, "
    # \tmarked_eta", marked_eta

    with TicToc(key="**** project solution ****", active=False, do_print=False):
        F = Function(CG)
        mesh = refine(mesh, cell_markers)
        CG = FunctionSpace(mesh, 'CG', _femdegree)
        # cores = V.to_list(V)
        cores = V
        r = cores[0].shape[2]
        newcore = np.zeros([1, CG.dim(), r])
        for d in range(r):
            # print "++++++++", CG.dim(), cores[0][0, :, d].shape
            F.vector().set_local(cores[0][0, :, d])
            # print "refinement", d, F.vector().array().shape,CG.dim()
            newF = interpolate(F, CG)
            newcore[0, :, d] = newF.vector().array()
        # update U
        cores[0] = newcore
        V = tt.vector.from_list(cores)
    return V, mesh
# endregion
