"""
implementation of the forward operator (solution operator) of the Galerkin problem considering a tensor train
decomposition of the coefficient field and the right hand side.
"""
# region imports
import numpy as np
from dolfin import (FunctionSpace, Function, nabla_grad, inner, dx, solve, TestFunction, TrialFunction, DirichletBC,
                    Constant, CellSize, FacetNormal, assemble, cells, nabla_div, avg, jump, dS, MeshFunction, refine,
                    interpolate)
from scipy import sparse as sps

from alea.application.bayes.alea_util.solver.tt_sparse_als import TTSparseALS
from alea.application.bayes.alea_util.solver.xerus_als import XerusALS
from alea.application.tt_asgfem.apps.sgfem_als_util import compute_FE_matrices_affine, generate_operator_tt_affine
from alea.application.tt_asgfem.tensorsolver.tt_sparse_matrix import smatrix
from alea.application.tt_asgfem.tensorsolver.tt_sgfem_util import increase_tt_rank
from alea.linalg.tensor.extended_fem_tt import ExtendedFEMTT, ExtendedTT, BasisType

from alea.application.tt_asgfem.tensorsolver.tt_util import ttNormalize
from alea.application.tt_asgfem.tensorsolver.tt_util import frob_norm
from alea.application.bayes.Bayes_util.ForwardOperator.tt_forward_operator_util import calculate_stiffness_matrix
from alea.utils.progress.bar import Bar
from alea.utils.tictoc import TicToc
from scipy.sparse.linalg import spsolve
from joblib import Parallel, delayed
import itertools as _iter
from operator import itemgetter                     # enables items to pass the __getitem__() method
import tt
import os
import gc
from copy import deepcopy

import xerus as xe
import cPickle as pickle
# endregion

FILE_PATH = os.path.dirname(__file__) + "/tmp/"


class solution_cache:
    def __init__(self):
        self.str = None


class TTAsgfemOperator(object):
    # region init
    def __init__(self,
                 coef,                              # type: ExtendedFEMTT
                 rhs,                               # type: ExtendedFEMTT
                 sol_rank,                          # type: list
                 sol_hdeg,                          # type: list
                 fs,                                # type: FunctionSpace
                 empty=False                        # type: bool
                 ):
        """
        constructor
        :param coef: continuous coefficient
        :param fs: function space containing the mesh and finite element discretisation
        """
        self.coef = coef.copy()
        self.rhs = rhs.copy()
        self.fs = fs

        self.sol_rank = sol_rank
        self.sol_hdeg = sol_hdeg

        self.coef_sample_path = None

        self.ranks = None
        self.iterations = None
        self.tolerance = None
        self.preconditioner = None

        self.tt_bilinearform = None
        self.tt_linearform = None
        self.solution = None

        self.solver = None

        self.solved = False
        self.computed = False
        self.refined = False
        if empty is False:
            self._create_discrete_operator()

        self.computed = False
        if empty is False:
            self._create_rhs()
    # endregion

    # region private def create discrete operator
    def _create_discrete_operator(self):
        assert self.coef.components[0].shape[0] == 1
        assert self.rhs.components[0].shape[0] == 1
        first_coef_comp = self.coef.components[0][0, :, :]
        first_rhs_comp = self.rhs.components[0][0, :, :]

        #                                           # compute FE matrices for piecewise constant coefficients
        A0, bc_dofs, BB, CG = compute_FE_matrices_affine(first_coef_comp, first_rhs_comp, self.fs,
                                                         self.coef.first_comp_fs, self.rhs.first_comp_fs,
                                                         n_cpu=40)

        opcores = generate_operator_tt_affine(self.coef.components[1:], self.sol_rank, self.sol_hdeg,
                                              len(self.sol_hdeg))
        D = A0[0].shape[0]  # get size of first matrix
        opcores[0] = []  # re_init first operator core (it was None anyway)
        # A = None
        for r0, A in enumerate(A0):
            opcores[0].append(A)
            # print "A0", A.toarray()
            # print "midpoints", first_comp[:,r0]
            # print "eigsA0", np.linalg.eig(A.toarray())

        # generate lognormal operator tensor

        A = smatrix(opcores)

        bcopcores = (len(self.sol_hdeg) + 1) * [[]]
        # bcdense = np.zeros([D, D])
        # bcdense[bc_dofs, bc_dofs] = 1
        bcsparse_direct = sps.csr_matrix((D, D))
        bcsparse_direct[bc_dofs, bc_dofs] = 1
        bcopcores[0] = []
        bcopcores[0].append(bcsparse_direct)
        for i in range(self.coef.dim - 1):
            bcopcores[i+1] = np.reshape(np.eye(opcores[i + 1].shape[1], opcores[i + 1].shape[2]),
                                        [1, opcores[i + 1].shape[1], opcores[i + 1].shape[2], 1], order='F')
        BCOP = smatrix(bcopcores)

        A += BCOP
        A = A.round(1e-16)  # ! important: do not cut ranks here. Just orthogonalize cores[1:]
        BB = np.array(BB)
        self.tt_linearform = self.rhs.copy()
        self.tt_linearform.components[0] = np.reshape(BB, (1, BB.shape[0], BB.shape[1]))
        self.tt_linearform.n[0] = BB.shape[0]
        self.tt_linearform.r[1] = BB.shape[1]

        self.tt_bilinearform = A
        self.computed = True

    # endregion

    # region private def create rhs
    def _create_rhs(self):
        """
        Since in the creation of the bilinear form, the rhs is a side product, we do not need this function here
        :return: void
        """
        assert(self.tt_linearform is not None)
        assert (self.tt_bilinearform is not None)

        # self.tt_linearform = np.reshape(self.tt_linearform, [1, self.tt_bilinearform.n[0], 1], order='F')
        # self.tt_linearform = [self.tt_linearform] + len(self.hdegs) * [[]]
        # for i in range(len(self.hdegs)):
        #     self.tt_linearform[i + 1] = np.reshape(np.eye(self.tt_bilinearform.n[i + 1], 1),
        #                                            [1, self.tt_bilinearform.n[i + 1], 1],
        #                                            order='F')
        self.computed = True

    # endregion

    # region def solve
    def solve(self,
              rank,                                 # type: list
              preconditioner,                       # type: str
              iterations,                           # type: int
              tolerance,                            # type: float
              init_value=None,                      # type: xe.TTTensor or ExtendedFEMTT or list
              solver="TTSparseALS",                 # type: str
              solver_options=None                   # type: dict
              ):
        """
        starts the solving process
        :param rank: desired rank
        :param preconditioner: name of the used preconditioner
        :param iterations: number of iterations in the solution process
        :param tolerance: desired tolerance
        :param init_value: prescribed start value
        :param solver: solver type. default is Max+Manuel TTSparseALS with supported preconditioner as in tt_sparse_als
                       get precondition. Alternatively use "XerusAls" or "XerusAlsSym"
        :param solver_options: dictionary containing additional information for the solver.
                               SparseTTALS does not need this dict
                               XerusALS needs the "method" and "symmetric" entry (see xerus_als for more information)

        """
        self.ranks = rank
        self.iterations = iterations
        self.tolerance = tolerance
        self.preconditioner = preconditioner

        # if self.load() is True:
        #     self.solved = True
        #     return
        if solver == "TTSparseALS":
            self.solver = TTSparseALS(self.tt_bilinearform, self.tt_linearform, rank, als_iterations=iterations,
                                      convergence=tolerance)
            if max(rank) == 1:
                self.solution = self.solver.solve(start_rank_one=True, preconditioner={"name": preconditioner,
                                                                                       "coef": self.coef,
                                                                                       "operator": self})
            elif init_value is None or init_value == []:
                self.solution = self.solver.solve(preconditioner={"name": preconditioner,
                                                                  "coef": self.coef,
                                                                  "operator": self})
            else:

                self.solution = self.solver.solve(preconditioner={"name": preconditioner,
                                                                  "coef": self.coef,
                                                                  "operator": self},
                                                  init_value=init_value, normalize_init=True)
        elif solver == "XerusALS":
            if solver_options is None:
                method = "ALS"
                symmetric = False
            else:
                assert "method" in solver_options
                assert "symmetric" in solver_options
                method = solver_options["method"]
                symmetric = solver_options["symmetric"]

            self.solver = XerusALS(self.tt_bilinearform, self.tt_linearform, rank, iterations=iterations,
                                   convergence=tolerance)
            if max(rank) == 1:
                self.solution = self.solver.solve(method=method, symmetric=symmetric, start_rank_one=True)
            elif init_value is None or init_value == []:
                self.solution = self.solver.solve(method=method, symmetric=symmetric, start_rank_one=False)
            else:
                if isinstance(init_value, ExtendedFEMTT):
                    init_value = init_value.to_xerus_tt()
                self.solution = self.solver.solve(method=method, symmetric=symmetric, start_rank_one=False,
                                                  init_value=init_value, normalize_init=True)

        self.solved = True
        self.solved = self.save()
    # endregion

    # region def get_filename
    def get_filename(self):
        """
        returns an artificial file name of the current object containing 'unique' informations about the operator
        :return: string
        """
        filename = "TTLognormalAsgfemOperator"
        filename += "hdegs:{}".format(self.coef.n)
        filename += "ranks:{}".format(self.coef.r)
        filename += "iterations:{}".format(self.iterations)
        filename += "tolerance:{}".format(self.tolerance)
        filename += "preconditioner:{}".format(self.preconditioner)
        return filename
    # endregion

    # region def get_hash
    def get_hash(self):
        """
        returns the hash of the filename
        :return: string
        """
        return hash(self.get_filename())
    # endregion

    # region save forward solution
    def save(self):
        if self.solved is False:
            raise ValueError(" can not save operator if solver was not successfully run")
        try:
            retval = dict()
            retval['solution'] = self.solution
            f = open(FILE_PATH + str(self.get_hash()) + "sol.dat", 'wb')
            pickle.dump(retval, f, 2)
            f.close()
            print("saved solution to: {}".format(FILE_PATH + str(self.get_hash()) + "sol.dat"))
        except Exception as ex:
            print("!!!!! {} exception: {} \n    {}".format(__name__, ex.message, ex))
            return False
        return True
    # endregion

    # region load
    def load(self, path=None):
        """
        loads a pickled file that contains the solution tensor cores
        :return: boolean
        """
        try:
            if path is None:
                f = open(FILE_PATH + str(self.get_hash()) + "sol.dat", 'rb')
            else:
                f = open(path, "rb")
            tmp_dict = pickle.load(f)
            f.close()
            self.solution = tmp_dict['solution']
        except Exception as ex:
            print("!!!!! {} exception: {} \n    {}".format(__name__, ex.message, ex))
            return False
        return True
    # endregion

    # region def solve problem for sample
    def solve_for_sample(self, y):
        rhs = Function(self.rhs.first_comp_fs)
        coef = Function(self.coef.first_comp_fs)
        solution = Function(self.fs)

        rhs.vector()[:] = np.ascontiguousarray(self.rhs.sample(y))
        coef.vector()[:] = np.ascontiguousarray(self.coef.sample(y))

        def boundary(x, on_boundary):
            return on_boundary

        bc = DirichletBC(self.fs, Constant(0.0), boundary)

        u, v = TrialFunction(self.fs), TestFunction(self.fs)

        bf = inner(coef*nabla_grad(u), nabla_grad(v)) * dx(self.fs.mesh())
        lf = rhs*v*dx(self.fs.mesh())

        solve(bf == lf, solution, bc)

        return solution, coef, rhs

    # endregion

# region def refine adaptive
    def refine_adaptive(self,
                        theta_x,                    # type: float
                        theta_y,                    # type: float
                        resnorm_zeta_weight,        # type: float
                        eta_zeta_weight,            # type: float
                        rank_always,                # type: bool
                        sol_rank,                   # type: int
                        start_rank,                 # type: int
                        mesh_once=False,
                        sto_once=False
                        ):

        if self.solved is False:
            raise ValueError("solution is not created yet. Start solver first")
        last_refined = None
        assert isinstance(self.solution, ExtendedFEMTT)
        sto_estimator_result = dict()
        det_estimator_result = dict()
        new_solution = None
        new_coef = None
        new_rhs = None
        refined = ""
        # region residual estimator
        #                      	                # calculate residual mean in energy norm
        res_estimator_result = evaluate_resnorm(self.tt_bilinearform, self.solution, self.tt_linearform)
        # endregion
        # region deterministic estimator
        # print("theta x: {}".format(theta_x))
        # print("last_refined: {}".format(last_refined))
        if theta_x > 0:
            det_estimator_result = compute_det_estimator(self.rhs, self.coef, self.solution)
            # print("eta_global={}".format(eta_res_global))
            # print("rhs_global={}".format(rhs_global))
            # print("zero_global={}".format(zero_global))
            # print("sq_global={}".format(sq_global))
            # print("jump_global={}".format(jump_global))
                    # assert_almost_equal(eta_res_global, np.sqrt(sum(eta_res_local ** 2)))
            # print eta_global, np.sqrt(sum(summed_eta_local**2))
            # assert_almost_equal(eta_global, np.sqrt(sum(summed_eta_local ** 2)))
        elif theta_x <= 0:
            det_estimator_result["eta_global"] = 0
            det_estimator_result["eta_local"] = 0
        # endregion

        # region stochastic error estimator
        # print("theta y: {}".format(theta_y))
        if theta_y > 0 and (last_refined == "sto" or last_refined is None):
            raise NotImplementedError("stochastic error estimator not implemented atm")
            # # with TicToc(sec_key="sto-error-estimator", key="**** TT stochastic residual estimator ****",
            # #             active=True, do_print=False):
            # self.sto_estimator_result = residual.eval_stochastic_error_estimator(self.discrete_coef.affine_field,
            #                                                                      bmax=self.discrete_coef.max_norms,
            #                                                                      theta=self.discrete_coef.theta,
            #                                                                      rho=self.discrete_coef.rho,
            #                                                                      femdegree=self.discrete_coef.femdegree,
            #                                                                      reference_m=reference_m)
            #         #                                       # add all evaluated tail indicators to indicator set
            # self.sto_estimator_result["zeta_list"].update(self.sto_estimator_result["zeta_tail_list"])
            # #   		                            # calculate global zeta value
            # self.sto_estimator_result["zeta_global_sum"] = np.sqrt(sum([z ** 2 for z in
            #                                                             self.sto_estimator_result["zeta_list"].values()
            #                                                             ]))
            # # print zeta_res_local
            # # print("zeta_res_local={}".format(zeta_res_local))
            # print("zeta_sum = {} <= {} = zeta_global".format(self.sto_estimator_result["zeta_global_sum"],
            #                                                  self.sto_estimator_result["global_zeta"]))
            # # print("   !!!! use zeta_sum from now on")
            # # buf = self.sto_estimator_result["zeta_global_sum"]
            # # self.sto_estimator_result["zeta_global_sum"] = self.sto_estimator_result["global_zeta"]
            # # self.sto_estimator_result["global_zeta"] = buf
            # # print("zeta_tail_list= {}".format(zeta_tail_list))
        elif theta_y <= 0:
            sto_estimator_result["global_zeta"] = 0
        # endregion
        # region decide for refinement step
        rank_done = False
        if sto_estimator_result["global_zeta"] >= resnorm_zeta_weight * res_estimator_result["resnorm"] \
                or rank_always or max(self.solution.r) >= sol_rank or rank_done:
            refine_deterministic = eta_zeta_weight * det_estimator_result["eta_global"] > \
                                   sto_estimator_result["global_zeta"]
            refine_stochastic = not refine_deterministic
            if refine_deterministic or refine_stochastic:
                rank_done = True
        if sto_estimator_result["global_zeta"] >= resnorm_zeta_weight * res_estimator_result["resnorm"] \
                or rank_always or max(self.solution.r) >= sol_rank or rank_done:
            refine_deterministic = eta_zeta_weight * det_estimator_result["eta_global"] > \
                                   sto_estimator_result["global_zeta"]
            refine_stochastic = not refine_deterministic
            if refine_deterministic or refine_stochastic:
                rank_done = True
        elif eta_zeta_weight * det_estimator_result["eta_global"] >= resnorm_zeta_weight * \
                res_estimator_result["resnorm"] \
                or max(self.solution.r) >= sol_rank or rank_done:
            refine_deterministic = True
            refine_stochastic = False
            if refine_deterministic or refine_stochastic:
                rank_done = True
        else:  # no refinement
            refine_deterministic = False
            refine_stochastic = False
            rank_done = False
        # check if rank of solution is already full
        # print(has_full_rank(self.solution))
        # if has_full_rank(self.solution) and rank_done is False:
        #     rank_done = True
        #     refine_deterministic = False
        #     refine_stochastic = True
        #     print("  refine stochastic since solution has already full rank and rank update was proposed?!")
        if rank_always is True:
            rank_done = False
        if mesh_once:
            refine_deterministic = True
            refine_stochastic = False
            rank_done = True
        if sto_once:
            refine_deterministic = False
            refine_sto_uniform = True
            refine_stochastic = True
            rank_done = True
        print("refine det: {}".format(refine_deterministic))
        print("refine sto: {}".format(refine_stochastic))
        print("rank done: {}".format(rank_done))
        # if last_refinement_sto is True and refine_stochastic is True \
        #         and last_refinement_sto_estimator <= zeta_global:
        #         refine_stochastic = False
        #         refine_deterministic = True
        #         print("refine physic instead of stochastic, since zeta got worse.")
        # endregion

        # region refine mesh, stochastic index set or solution rank
        #                                           # deterministic adaptive refinement
        if theta_x > 0 and refine_deterministic:
            mesh = refine_mesh(det_estimator_result["eta_local"], self.solution.first_comp_fs, theta_x)
            new_fs = FunctionSpace(mesh, self.solution.first_comp_fs.ufl_element().family(),
                                   self.solution.first_comp_fs.ufl_element().degree())
            new_solution = project_tensor_onto_fs(self.solution, new_fs)
            new_coef = project_tensor_onto_fs(self.coef, new_fs)
            new_rhs = project_tensor_onto_fs(self.rhs, new_fs)

            print("new mesh dofs: {}".format(new_solution.components[0].shape[1]))
            refined = "det"
        #                                           # stochastic adaptive refinement
        if theta_y > 0 and refine_stochastic:
            raise NotImplementedError("stochastic refinement is not implemented yet")
            # loc_sol = self.solution

            # if bound_sol_degree > 1:
            #     for lia in range(len(loc_sol)-1):
            #         if loc_sol[lia+1].shape[1] >= bound_sol_degree:
            #             print("bound sol degree: {} at lia={}, since shape={}".format(bound_sol_degree, lia,
            #                                                                           loc_sol[lia+1].shape[1]))
            #             print("  zeta_list: {}".format(self.sto_estimator_result["zeta_list"]))
            #             self.sto_estimator_result["zeta_list"][lia] = 0.0
            #             print("  new zeta_list: {}".format(self.sto_estimator_result["zeta_list"]))
            # if not refine_sto_uniform:
            #   #                               # use only zeta sum, since the global zeta can not be reached
            #     new_dim, marked_zeta, not_marked_zeta_ym = ttMark_y(self.sto_estimator_result["zeta_list"],
            #                                                         self.sto_estimator_result["zeta_tail_list"],
            #                                                         theta_y,
            #                                                       self.sto_estimator_result["zeta_global_sum"], False)
            #     # print(new_dim)
            #     # print(marked_zeta)
            #     # print("prev solution: {}".format(tt.vector.from_list(residual.solution)))
            # else:
            #     new_dim = []
            #     for lia, core in enumerate(loc_sol):
            #         assert (len(core.shape) == 3)
            #         if bound_sol_degree > 1 and lia > 0 and core.shape[1] >= bound_sol_degree:
            #             continue
            #         new_dim.append(lia)
            #                 solution, hdegs, M = tt_add_stochastic_dimension(tt.vector.from_list(residual.solution),
            #                                                  new_dim, hdegs, start_rank,
            #                                                  new_gpcd=new_hdeg)
            # print("new hdegs:{}".format(hdegs))
            # refined = "sto"
            # # print("post solution: {}".format(residual.solution))

        if not rank_done:
            if max(self.solution.r) < sol_rank and not refine_deterministic and not refine_stochastic:
                new_solution, rank = increase_tt_rank(self.solution.to_osel_tt(), None, None, None, acc=1e-18)
                print("increase rank to {} since resnorm dominates".format(rank))
                refined = "rank"
            elif max(self.solution.r) < sol_rank and rank_always:
                new_solution, rank = increase_tt_rank(self.solution.to_osel_tt(), None, None, None, acc=1e-18)
                print("increase rank to {}, since we always increase the rank".format(rank))
                # refined = "rank"
        if len(self.solution.r) > 2 and min(self.solution.r[1:-1]) == 1:
            new_solution, rank = increase_tt_rank(self.solution.to_osel_tt(), None, None, None, acc=1e-18)
            print("increase rank to {}, since there is one rank = 1".format(rank))
            refined = "rank"
        print("hdegs = {}".format(self.solution.n))
        # print("ranks = {}".format(tt.vector.from_list(solution).r))
        # print("eta = {}".format(self.det_estimator_result["eta_global"]))
        # print("zeta = {}".format(self.sto_estimator_result["global_zeta"]))
        # print("resnorm = {} \n".format(self.res_estimator_result["resnorm"])
        #       + "  algebraic resnorm = {} \n".format(self.res_estimator_result["resnorm_alg"])
        #       + "  normalized algebraic resnorm = {}".format(self.res_estimator_result["resnorm_alg_norm"]))
        # endregion
        self.refined = True
        retval = {"det_estimator": det_estimator_result,
                  "sto_estimator": sto_estimator_result,
                  "res_estimator": res_estimator_result,
                  "new_solution": new_solution,
                  "new_coef": new_coef,
                  "new_rhs": new_rhs,
                  "refined": refined
                  }
        return retval
    # endregion

# region def compute det estimator


def compute_det_estimator(rhs_tt,                   # type: ExtendedFEMTT
                          coef_tt,                  # type: ExtendedFEMTT
                          solution                  # type: ExtendedFEMTT
                          ):

    fs = solution.first_comp_fs
    dg0_fs = FunctionSpace(fs.mesh(), "DG", 0)
    h = CellSize(dg0_fs.mesh())
    nu = FacetNormal(fs.mesh())
    d_g0_dofs = dict([(c.index(), dg0_fs.dofmap().cell_dofs(c.index())[0]) for c in cells(dg0_fs.mesh())])
    dg0_test_fun = TestFunction(dg0_fs)

    est_f = np.zeros(len(d_g0_dofs))
    est_mix = np.zeros(len(d_g0_dofs))
    est_sq = np.zeros(len(d_g0_dofs))
    est_jump = np.zeros(len(d_g0_dofs))

    coef_tt_tail = ExtendedTT(coef_tt.components[1:], basis=coef_tt.basis[1:])
    rhs_tt_tail = ExtendedTT(rhs_tt.components[1:], basis=rhs_tt.basis[1:])
    sol_tt_tail = ExtendedTT(solution.components[1:], basis=solution.basis[1:])
    res = None

    if fs.ufl_element().degree() > 0:
        res_f = rhs_tt_tail.multandeval(rhs_tt_tail, [0]*rhs_tt_tail.dim)
        res_f = np.reshape(res_f, (rhs_tt.r[1], rhs_tt.r[1]), order="F")
        res_f = np.dot(rhs_tt.components[0][0, :, :], res_f)

        f1 = Function(fs)
        f2 = Function(fs)
        bar = Bar("compute rhs estimator portion", max=rhs_tt.r[1] ** 2)
        for lia in range(rhs_tt.r[1]):
            for lib in range(rhs_tt.r[1]):
                f1.vector()[:] = np.ascontiguousarray(rhs_tt.components[0][0, :, lia])
                f2.vector()[:] = np.ascontiguousarray(res_f[:, lib])
                vec = (h ** 2) * inner(f1, f2) * dg0_test_fun * dx
                vec = assemble(vec, form_compiler_parameters={'quadrature_degree': -1})
                vec = vec.array()[d_g0_dofs.values()]
                est_f += vec
                bar.next()
        bar.finish()

    if fs.ufl_element().degree() > 1:
        # print("compute mix estimator portion. p={}".format(fs.ufl_element().degree()))
        # print("  compute res")
        res = sol_tt_tail.multiply_with_extendedTT(coef_tt_tail)
        print(res)
        # print("  compute rhs x res")
        res_mix = rhs_tt_tail.multiply_with_extendedTT(res)
        # print("  evaluate at 0")
        res_mix = ExtendedTT(res_mix.components, basis=[BasisType.points] * res_mix.dim)
        res_mix = res_mix([0] * res_mix.dim)
        print(res_mix)
        res_mix = np.reshape(res_mix, (rhs_tt.r[1], solution.r[1], coef_tt.r[1]), order="F")
        # print("  compute remainder")
        res_mix = np.einsum('ik, abk->iab', coef_tt.components[0][0, :, :], res_mix)

        f1 = Function(fs)
        u1 = Function(fs)
        c = Function(fs)
        bar = Bar("compute mix estimator portion", max=rhs_tt.r[1] * solution.r[1])
        for lia in range(rhs_tt.r[1]):
            for lib in range(solution.r[1]):
                f1.vector()[:] = np.ascontiguousarray(rhs_tt.components[0][0, :, lia])
                u1.vector()[:] = np.ascontiguousarray(solution.components[0][0, :, lib])
                c.vector()[:] = np.ascontiguousarray(res_mix[:, lia, lib])
                vec = 2 * (h ** 2) * f1 * nabla_div(c*nabla_grad(u1)) * dg0_test_fun * dx
                vec = assemble(vec, form_compiler_parameters={'quadrature_degree': -1})
                vec = vec.array()[d_g0_dofs.values()]
                est_mix += vec
                bar.next()
        bar.finish()

        res_sq = res.multiply_with_extendedTT(res)
        res_sq = ExtendedTT(res_sq.components, basis=[BasisType.points] * res_sq.dim)
        res_sq = res_sq([0] * res_sq.dim)
        res_sq = np.reshape(res_sq, (solution.r[1], coef_tt.r[1], solution.r[1], coef_tt.r[1]), order="F")
        res_sq = np.einsum('ik, abkc->iabc', solution.components[0][0, :, :], res_sq)

        u1 = Function(fs)
        u2 = Function(fs)
        c1 = Function(fs)
        c2 = Function(fs)
        bar = Bar("compute sq and jump estimator portion", max=coef_tt.r[1]**2 * solution.r[1] )
        for lia in range(solution.r[1]):
            for lib in range(coef_tt.r[1]):
                u1.vector()[:] = np.ascontiguousarray(solution.components[0][0, :, lia])
                c1.vector()[:] = np.ascontiguousarray(coef_tt.components[0][0, :, lib])
                for lic in range(coef_tt.r[1]):
                    u2.vector()[:] = np.ascontiguousarray(res_sq[:, lia, lib, lic])
                    c2.vector()[:] = np.ascontiguousarray(coef_tt.components[0][0, :, lic])
                    vec = (h ** 2) * inner(nabla_div(c1*nabla_grad(u1)), nabla_div(c2*nabla_grad(u2))) * \
                        dg0_test_fun * dx
                    vec = assemble(vec, form_compiler_parameters={'quadrature_degree': -1})
                    vec = vec.array()[d_g0_dofs.values()]
                    est_sq += vec
                    vec = avg(h) * inner(jump(c1*nabla_grad(u1), nu), jump(c2*nabla_grad(u2), nu)) * \
                        avg(dg0_test_fun) * dS
                    vec = assemble(vec, form_compiler_parameters={'quadrature_degree': -1})
                    vec = vec.array()[d_g0_dofs.values()]
                    est_jump += vec
                    bar.next()
        bar.finish()

    else:
        print("Compute jump estimator portion")
        if res is None:
            print("  Compute sol x coef")
            res = sol_tt_tail.multiply_with_extendedTT(coef_tt_tail)
        print("  compute res x res")
        res_sq = res.multandeval(res, [0]*res.dim, progress=True)
        res_sq = np.reshape(res_sq, (solution.r[1], coef_tt.r[1], solution.r[1], coef_tt.r[1]), order="F")
        print("  compute remainder")
        res_sq = np.einsum('ik, abck->iabc', coef_tt.components[0][0, :, :], res_sq)
        # res_sq = np.einsum('ik, abkc->iabc', solution.components[0][0, :, :], res_sq)
        global _global_objects
        c = ComputerEstimator(fs, DG0=dg0_fs, DG0_dofs=d_g0_dofs, h=h, nu=nu)
        _global_objects = c
        print("  compute estimator over {} ranks in parallel".format(solution.r[1]**2*coef_tt.r[1]))
        est_jump = np.sum(Parallel(50)
                          (delayed(det_estimator_jump_parallel)
                           (solution.components[0][0, :, lia],
                            solution.components[0][0, :, lic],
                            res_sq[:, lia, lib, lic],
                            coef_tt.components[0][0, :, lib])
                          for lia, lib, lic in _iter.product(range(solution.r[1]),
                                                             range(coef_tt.r[1]),
                                                             range(solution.r[1]))),
                          axis=0)

    eta_local = np.sqrt(np.abs(est_f) + est_mix + est_sq + est_jump)
    # print("est f: {}".format(est_f))
    # print("est mix: {}".format(est_mix))
    # print("est sq: {}".format(est_sq))
    # print("est jump: {}".format(est_jump))
    retval = {"est_f": est_f,
              "est_mix": est_mix,
              "est_sq": est_sq,
              "est_jump": est_jump,
              "eta_local": eta_local,
              "eta_global": np.sum([eta for eta in eta_local])
              }
    return retval

# endregion


# region def refine mesh deterministic
def refine_mesh(eta_local,                          # type: list
                fs,                                 # type: FunctionSpace
                theta_x                             # type: float
                ):

    # sort error indicators
    eta_local_ind = [(x, i) for i, x in enumerate(eta_local)]
    eta_local_ind = sorted(eta_local_ind, key=itemgetter(0), reverse=True)
    # print(eta_local)
    eta_global = np.sum(eta_local)
    # setup marking set
    mesh = fs.mesh()
    cell_markers = MeshFunction("bool", mesh, mesh.topology().dim())
    cell_markers.set_all(False)
    marked_eta, cc = 0.0, 0

    for eta_cell in eta_local_ind:
        # break if sufficiently many cells are selected
        if theta_x * eta_global <= marked_eta:
            break
        cell_markers[eta_cell[1]] = True
        marked_eta += eta_cell[0]
        cc += 1
    print("++++++ cells marked: %i/%i" % (cc, mesh.num_cells()), "eta_global", eta_global, "tmarked_eta", marked_eta)

    mesh = refine(mesh, cell_markers)

    return mesh
# endregion

# region def project tensor onto new mesh


def project_tensor_onto_fs(ten,                     # type: ExtendedFEMTT
                           fs                       # type: FunctionSpace
                           ):
    assert isinstance(ten, ExtendedFEMTT)
    assert isinstance(fs, FunctionSpace)
    fun = Function(ten.first_comp_fs)
    fun.set_allow_extrapolation(True)
    cores = ten.components
    r = cores[0].shape[2]
    new_core = np.zeros([1, fs.dim(), r])
    for d in range(r):
        fun.vector().set_local(cores[0][0, :, d])
        new_f = interpolate(fun, fs)
        new_core[0, :, d] = new_f.vector().array()
    # update U
    cores[0] = new_core
    return ExtendedFEMTT(cores, ten.basis, fs, weights=ten.weights)

# endregion


# region def det_estimator_jump_parallel


def det_estimator_jump_parallel(sol1, sol2, coef1, coef2):

    o = _global_objects
    data = {"coef1": coef1, "sol1": sol1, "coef2": coef2, "sol2": sol2}
    result = o.compute_residual_error_edge(data)
    gc.collect()
    return result
# endregion
# region def class Computer estimator to compute residual error in parallel


class ComputerEstimator(object):
    # region init
    def __init__(self, fs, DG0=None, DG0_dofs=None, h=None, nu=None):
        self.FEM = self.setup_FEM(fs, DG0=DG0, DG0_dofs=DG0_dofs, h=h, nu=nu)

    # endregion

    # region setup FEM
    def setup_FEM(self, fs, DG0=None, DG0_dofs=None, h=None, nu=None):
        FEM = {'V': fs, 'assemble_quad_degree': -1,  "DG0": DG0, "DG0_dofs": DG0_dofs, "h": h, "nu": nu}
        return FEM
    # endregion

    def compute_residual_error_edge(self, _data):
        u1 = Function(self.FEM["V"])
        u2 = Function(self.FEM["V"])
        c1 = Function(self.FEM["V"])
        c2 = Function(self.FEM["V"])
        h = self.FEM["h"]
        nu = self.FEM["nu"]
        _dg0 = TestFunction(self.FEM["DG0"])

        u1.vector()[:] = np.ascontiguousarray(_data["sol1"])
        u2.vector()[:] = np.ascontiguousarray(_data["sol2"])
        c1.vector()[:] = np.ascontiguousarray(_data["coef1"])
        c2.vector()[:] = np.ascontiguousarray(_data["coef2"])
        vec = avg(h) * inner(jump(c1*nabla_grad(u1), nu), jump(c2*nabla_grad(u2), nu)) * avg(_dg0) * dS
        vec = assemble(vec, form_compiler_parameters={'quadrature_degree': self.FEM["assemble_quad_degree"]})
        vec = vec.array()[self.FEM["DG0_dofs"].values()]

        return vec
# endregion

# region def evaluate resnorm
def evaluate_resnorm(A, W, F):
    assert isinstance(A, smatrix)
    assert isinstance(W, ExtendedFEMTT)
    assert isinstance(F, ExtendedTT)

    retval_dict = dict()

    fs = W.first_comp_fs
    F = tt.vector.from_list(F.components)
    W = tt.vector.from_list(W.components)
    # print("ranks of W prior to F - and application of A")
    # print W.r
    with TicToc(sec_key="resnorm-error-estimator", key="F-AW", active=True, do_print=False):
        W = F - A.matvec(W)
        retval_dict["resnorm_alg"] = frob_norm(W)
    # print("ranks of W after application of A")
    # print W.r
    with TicToc(sec_key="resnorm-error-estimator", key="normalize", active=True, do_print=False):
        W = ttNormalize(W)
        retval_dict["resnorm_alg_norm"] = frob_norm(W)
    # print("ranks of W after normalize")
    # print W.r
    W = tt.vector.to_list(W)
    # region compute stiffness matrix
    with TicToc(sec_key="resnorm-error-estimator", key="compute stiffness matrix", active=True, do_print=False):
        stiff = calculate_stiffness_matrix(fs, with_bc=True)
        # TODO: Apply BC. May better incorporate the whole operator and let him handle that.
    # endregion
    # print stiff.toarray()[0:20, 0:20]
    # exit()
    core = [1]
    for lia in range(len(W)-1, -1, -1):
        assert (len(W[lia].shape) == 3)         # every component has order 3
        if lia == len(W)-1:                     # we start at the last component
            assert(W[lia].shape[2] == 1)        # the last rank should be 1
            if lia == 0:                        # no stochastic dimensions
                assert (W[lia].shape[0] == 1)   # hence first rank should be 1
                return np.sqrt(np.dot(W[lia][0, :, 0], spsolve(stiff, W[lia][0, :, 0])))
            #                                   # create base change matrix (Legendre case no base change)
            mat = np.eye(W[lia].shape[1], W[lia].shape[1])
            mat_inv = mat
            # mat_inv = np.linalg.inv(mat)        # invert it. # TODO: solve linear eq. system, catch Legendre case
            #  [k_M, mu, 0] x [mu, mu'] = [k_M, mu']
            core = np.dot(W[lia][:, :, 0], mat_inv)
            #  [k_M', mu', 0] x [mu', k_M] = [k_M, k_M']
            core = np.dot(W[lia][:, :, 0], core.T).T
            continue
        assert(len(core.shape) == 2)            # previous loop result is of order 2
        assert(core.shape[0] == core.shape[1])  # and symmetric
        #                                       # and of size of the rank of the current component
        assert(W[lia].shape[2] == core.shape[0])
        if lia > 0:                             # we are not at the first component
            #                                   # create base change matrix
            mat = np.eye(W[lia].shape[1], W[lia].shape[1])
            mat_inv = mat
            # mat_inv = np.linalg.inv(mat)        # invert it. # TODO: solve linear eq. system, catch Legendre case
            # [k_m-1, mu, k_m] x [k_m, k_m'] = [k_m-1, mu, k_m']
            newcore = np.einsum('ijk, kl->ijl', W[lia], core)
            # [k_m-1, mu, k_m'] x [mu, mu'] = [k_m-1, mu', k_m']
            newcore = np.einsum('ijk, jl->ilk', newcore, mat_inv)
            # [k_m-1, mu', k_m'] x [k_m-1', mu', k_m'] = [k_m-1, k_m-1']
            core = np.einsum('ijk, mjk->im', newcore, W[lia])
        else:
            assert(W[0].shape[0] == 1)          # we reached the first component. first rank = 1, W[0].shape[1]=N
            retval = 0                          # init return value
            with TicToc(sec_key="resnorm-error-estimator", key="stiffness sum over rank", active=True,
                        do_print=False):
                #                               # contract remainder onto first component
                core = np.dot(W[0][0, :, :], core)
                for k in range(W[0].shape[2]):  # sum over the remaining ranks
                    #                           # <R[k], S^{-1}((F-AW)[k])>
                    retval += np.dot(core[:, k], spsolve(stiff, W[0][0, :, k]))
            retval_dict["resnorm"] = np.sqrt(retval)
            return retval_dict
    raise RuntimeError("You should not be here")

# endregion
