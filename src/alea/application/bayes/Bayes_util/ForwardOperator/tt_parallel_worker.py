# region Imports
from joblib import Parallel, delayed
import gc
import numpy as np
from dolfin import FunctionSpace, Function, set_log_level, \
    interpolate, assemble, dx, TrialFunction, TestFunction, nabla_grad, parameters, inner, as_backend_type
from alea.application.bayes.Bayes_util.ForwardOperator.tt_lognormal_asgfem_operator import TTLognormalAsgfemOperator
from alea.application.bayes.Bayes_util.ForwardOperator.tt_lognormal_asgfem_operator_data import \
    TTLognormalAsgfemOperatorData
from scipy import sparse as sps
# endregion

# region Global variable declaration
_global_operator = None
_global_curr_operator = None
_global_pde = None
_global_ref_fs = None
_global_curr_fs = None
# endregion


# region class Cache
class Cache(object):
    def __init__(self):
        self.str = None
# endregion


# region Class SolParallelWorker
class TTParallelWorker(object):
    # region def init
    def __init__(self):
        self.reference_m = None
        self.expression_degree = None
        self.M = None
        self.S = None
        self.nCPU = None
        self.cache = None
    # endregion

    # region def prepare
    def prepare(self,
                true_operator,                      # type: TTLognormalAsgfemOperator
                curr_operator_data,                 # type: TTLognormalAsgfemOperatorData
                pde,                                # type: object
                ref_fs,                             # type: FunctionSpace
                args,                               # type: dict
                nCPU                                # type: int
                ):
        global _global_operator
        _global_operator = true_operator
        global _global_curr_operator
        _global_curr_operator = curr_operator_data
        global _global_pde
        _global_pde = pde
        global _global_ref_fs
        _global_ref_fs = ref_fs
        global _global_curr_fs
        _global_curr_fs = FunctionSpace(curr_operator_data.mesh, 'CG', args['femdegree'])

        self.reference_m = args["reference_m"] if "reference_m" in args else 100
        self.expression_degree = args["expression_degree"] if "expression_degree" in args else 5
        self.M = args["mass"]
        self.S = args["stiff"]
        self.nCPU = nCPU
        self.cache = Cache()
    # endregion

    # region def work
    def work(self, samples, method="cont"):
        # output = None
        if method == "cont":
            _ = _sampleSolInParallel(samples[0], self.reference_m, self.expression_degree, self.M, self.S,
                                     cache=self.cache)
            output = np.array(Parallel(self.nCPU)(delayed(_sampleSolInParallel)(sample,
                                                                                self.reference_m,
                                                                                self.expression_degree,
                                                                                self.M,
                                                                                self.S,
                                                                                cache=self.cache)
                                                  for sample in samples))
        elif method == "semi":
            _ = _sampleSolInParallelSemi(samples[0], self.M, self.S)
            output = np.array(Parallel(self.nCPU)(delayed(_sampleSolInParallelSemi)(sample,
                                                                                    self.M,
                                                                                    self.S)
                                                  for sample in samples))
        elif method == "disc":
            _ = _sampleSolInParallelDisc(samples[0], self.M, self.S)
            output = np.array(Parallel(self.nCPU)(delayed(_sampleSolInParallelDisc)(sample,
                                                                                    self.M,
                                                                                    self.S)
                                                  for sample in samples))
        else:
            raise ValueError("unknown sampling method")
        assert(output.shape[1] == 4)
        return output[:, 0], output[:, 1], output[:, 2], output[:, 3]
    # endregion


# region def sampleSolInParallel
def _sampleSolInParallel(y, reference_m, expression_degree, mass, stiff, cache=None):
    retval = dict()
    global_operator = _global_operator
    global_curr_operator = _global_curr_operator
    global_pde = _global_pde
    ref_fs = _global_ref_fs
    global_curr_fs = _global_curr_fs

    true_sol, true_coef = global_operator.sample_analytic_solution(y, global_pde, ref_fs=ref_fs,
                                                                   reference_m=reference_m,
                                                                   expression_degree=expression_degree,
                                                                   cache=cache)
    retval["sol"] = true_sol
    retval["coef"] = true_coef
    approx_sol = Function(global_curr_fs)
    # approx_sol.vector()[:] = global_curr_operator.sample_solution(y)
    approx_sol.vector()[:] = global_curr_operator.sample_solution(y)
    approx_sol = interpolate(approx_sol, ref_fs)
    approx_sol_vec = approx_sol.vector()[:]
    retval["sol_approx"] = approx_sol_vec
    diff = np.array(true_sol) - np.array(approx_sol_vec)
    errl2 = np.sqrt(np.dot(diff.T, sps.csr_matrix.dot(mass, diff)))
    errh1 = np.sqrt(np.dot(diff.T, sps.csr_matrix.dot(stiff, diff)))

    u = np.array(true_sol)
    ref_norm_l2 = np.sqrt(np.dot(u.T, sps.csr_matrix.dot(mass, u)))
    ref_norm_h1 = np.sqrt(np.dot(u.T, sps.csr_matrix.dot(stiff, u)))
    errl2_rel = errl2 * ref_norm_l2 ** (-1)
    errh1_rel = errh1 * ref_norm_h1 ** (-1)

    return errl2, errh1, errl2_rel, errh1_rel


# endregion

# region def sampleSolInParallelSemi
def _sampleSolInParallelSemi(y, mass, stiff):
    global_operator = _global_operator
    global_curr_operator = _global_curr_operator
    global_pde = _global_pde
    ref_fs = _global_ref_fs
    global_curr_fs = _global_curr_fs

    true_sol, true_coef = global_operator.sample_analytic_solution_continuous_coef(y, global_pde, ref_fs=ref_fs)
    approx_sol = Function(global_curr_fs)
    # approx_sol.vector()[:] = global_curr_operator.sample_solution(y)
    approx_sol.vector()[:] = global_curr_operator.sample_solution(y)
    approx_sol = interpolate(approx_sol, ref_fs)

    diff = np.array(true_sol) - np.array(approx_sol.vector()[:])
    errl2 = np.sqrt(np.dot(diff.T, sps.csr_matrix.dot(mass, diff)))
    errh1 = np.sqrt(np.dot(diff.T, sps.csr_matrix.dot(stiff, diff)))

    u = np.array(true_sol)
    ref_norm_l2 = np.sqrt(np.dot(u.T, sps.csr_matrix.dot(mass, u)))
    ref_norm_h1 = np.sqrt(np.dot(u.T, sps.csr_matrix.dot(stiff, u)))
    errl2_rel = errl2 * ref_norm_l2 ** (-1)
    errh1_rel = errh1 * ref_norm_h1 ** (-1)

    return errl2, errh1, errl2_rel, errh1_rel


# endregion

# region def sampleSolInParallelDisc
def _sampleSolInParallelDisc(y, mass, stiff):
    global_operator = _global_operator
    global_curr_operator = _global_curr_operator
    global_pde = _global_pde
    ref_fs = _global_ref_fs
    global_curr_fs = _global_curr_fs

    true_sol, true_coef = global_operator.sample_analytic_solution_discrete_coef(y, global_pde, ref_fs=ref_fs)
    approx_sol = Function(global_curr_fs)
    # approx_sol.vector()[:] = global_curr_operator.sample_solution(y)
    approx_sol.vector()[:] = global_curr_operator.sample_solution(y)
    approx_sol = interpolate(approx_sol, ref_fs)

    diff = np.array(true_sol) - np.array(approx_sol.vector()[:])
    errl2 = np.sqrt(np.dot(diff.T, sps.csr_matrix.dot(mass, diff)))
    errh1 = np.sqrt(np.dot(diff.T, sps.csr_matrix.dot(stiff, diff)))

    u = np.array(true_sol)
    ref_norm_l2 = np.sqrt(np.dot(u.T, sps.csr_matrix.dot(mass, u)))
    ref_norm_h1 = np.sqrt(np.dot(u.T, sps.csr_matrix.dot(stiff, u)))
    errl2_rel = errl2 * ref_norm_l2 ** (-1)
    errh1_rel = errh1 * ref_norm_h1 ** (-1)

    return errl2, errh1, errl2_rel, errh1_rel
# endregion
