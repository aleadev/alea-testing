# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  Simple class for Bayesian objects containing all important methods
"""
# region Import
from alea.application.bayes.Bayes_util.lib.bayes_lib import ensure_iterable

from alea.application.bayes.Bayes_util.ForwardOperator.kl_direct_operator import KlDirectOperatorTT
from alea.application.bayes.Bayes_util.Interface.IBayes import IBayes
from alea.application.bayes.Bayes_util.lib.bayes_lib_simple import (apply_observation, apply_inner_product,
                                                                    integrate_ydims_tt, create_rank_one)

# from Bayes_util.interpolation_util import evaluate_lagrange
from alea.application.egsz.coefficient_field import ParametricCoefficientField
from alea.stochastics.random_variable import UniformRV

import cPickle as Pickle
import numpy as np
from scipy.interpolate import interp1d
import tt
# endregion


class DirectBayes(IBayes):
    """
      Simple Bayesian inversion object implementing the Bayes interface
    """
    # region Constructor
    def __init__(self, solution, dofs, measurements, true_values, coeff_field, obs_precision, obs_rank, inp_precision,
                 inp_rank, inp_gamma, exp_method, global_exp_precision, eval_grid_points, prior_densities,
                 eval_densities, mean, stoch_grid, physical_grid):
        """
          constructor method using all objects needed for bayesian inversion
        @param solution: solution object of the forward operator
        @param measurements: measured values. z = (O o G)(u) + eta
        @param dofs: degrees of freedom to sample forward solution at
        @param eval_grid_points: 1D stoch domain grid to interpolate densities at
        @param eval_densities: list of densities for every marginal estimation
        @return:
        """
        self.solution = solution
        self.dofs = dofs
        self.measurements = measurements
        self.true_values = true_values
        self.coeff_field = coeff_field
        self.obs_precision = obs_precision
        self.obs_rank = obs_rank
        self.inp_precision = inp_precision
        self.inp_rank = inp_rank
        self.inp_gamma = inp_gamma
        self.exp_method = exp_method
        self.exp_method.global_precision = global_exp_precision
        self.global_exp_precision = global_exp_precision
        self.eval_grid_points = eval_grid_points
        self.prior_densities = prior_densities
        self.eval_densities = eval_densities
        self.mean = mean
        self.stoch_grid = stoch_grid
        self.physical_grid = physical_grid

    # endregion

    # region Function: init_solver
    @classmethod
    def init_solver(cls, dofs, measurements, true_values, coeff_field, obs_precision, obs_rank, inp_precision,
                    inp_rank, inp_gamma, exp_method, global_exp_precision, eval_grid_points, stoch_grid, physical_grid,
                    prior_densities):
        """
        initializes the Bayesian object with everything needed for the calculation process
        """
        return cls(0, dofs, measurements, true_values, coeff_field,
                   obs_precision, obs_rank, inp_precision, inp_rank, inp_gamma,
                   exp_method, global_exp_precision, eval_grid_points, prior_densities, [], 0,
                   stoch_grid, physical_grid)
    # endregion

    # region Function: calculate densities
    def calculate_densities(self):
        """
        starts the bayesian procedure and calculates the marginal densities
        """
        # TODO: check validity of all needed elements
        self.log.info("Create Tensor")

        self.create_forward_operator(self.coeff_field, len(self.true_values), self.physical_grid, self.stoch_grid)
        # solution = create_tensor_from_solution(a, mx, [my*(lia+1-lia) for lia in range(len(true_y))])
        self.log.info("Apply Observation")
        self.apply_observation()

        self.calculate_measurement_diff(self.solution, self.measurements)

        self.log.info("Apply Inner Product")
        self.apply_inner_product(self.solution, self.inp_gamma, self.inp_precision, self.inp_rank)
        self.exp_method.calculate_exponential_tt_precision(self.solution, self.global_exp_precision)
        self.solution = self.exp_method.solution
        self.log.info("calculate marginal densities")
        self.calculate_marginal_densities(self.exp_method.solution, self.prior_densities, self.stoch_grid, 1./10,
                                          self.eval_grid_points)

    # endregion

    # region Function: Import Bayes Object

    def import_bayes(self, import_path):
        """
        creates an instance of the current object using the information from the given file
        """
        try:
            infile = open(import_path + str(hash(self.file_name)) + '.dat', 'rb')
            (self.solution, inf) = Pickle.load(infile)
            self.dofs = inf["dofs"]
            self.measurements = inf["measurements"]
            self.true_values = inf["true_values"]
            self.obs_precision = inf["obs_precision"]
            self.obs_rank = inf["obs_rank"]
            self.inp_precision = inf["inp_precision"]
            self.inp_rank = inf["inp_rank"]
            self.inp_gamma = inf["inp_gamma"]
            self.exp_method = inf["exp_method"]
            self.global_exp_precision = inf["global_exp_precision"]
            self.eval_grid_points = inf["eval_grid_points"]
            self.eval_densities = inf["eval_densities"]
            self.mean = inf["mean"]
            self.stoch_grid = inf["stoch_grid"]
            infile.close()
        except IOError as ex:
            self.log.error("can not import from file " + import_path + str(hash(self.file_name)) +
                           " err: " + ex.message)
            return False
        except Exception as ex:
            self.log.error("can not assign value or anything else is wrong: " + ex.message)
            return False
        return True

    # endregion

    # region Function: Export Bayes Object

    def export_bayes(self, export_path):
        """
        exports all the important information of the bayesian procedure by pickling it to a file
        @param export_path: path to export file
        @return: success
        """
        # TODO: check validity
        # TODO: pickle coeff_field

        inf = {"dofs": self.dofs, "measurements": self.measurements, "true_values": self.true_values,
               "obs_precision": self.obs_precision, "obs_rank": self.obs_rank, "inp_precision": self.inp_precision,
               "inp_rank": self.inp_rank, "inp_gamma": self.inp_gamma, "exp_method": self.exp_method,
               "global_exp_precision": self.global_exp_precision, "eval_grid_points": self.eval_grid_points,
               "eval_densities": self.eval_densities,
               "mean": self.mean, "stoch_grid": self.stoch_grid}

        inf_str = "This is just a file for informational purpose for the direct Bayes Object: \n"
        inf_str += "    " + self.file_name + " = " + str(hash(self.file_name)) + "\n"
        inf_str += "Forward Operator: None needed\n"

        inf_str += "dofs"
        for lia in range(len(self.dofs)):
            inf_str += "    {} : {:2.4f}".format(lia, self.dofs[lia])

        inf_str += "measurements"
        for lia in range(len(self.measurements)):
            inf_str += "    {} : {:2.4f}".format(lia, self.measurements[lia])

        inf_str += "true values"
        for lia in range(len(self.true_values)):
            inf_str += "    {} : {:2.4f}".format(lia, self.true_values[lia])

        inf_str += "obs_precision: {}".format(self.obs_precision)
        inf_str += "obs_rank: {}".format(self.obs_rank)
        inf_str += "inp_precision: {}".format(self.inp_precision)
        inf_str += "inp_rank: {}".format(self.inp_rank)
        inf_str += "inp_gamma: {}".format(self.inp_gamma)
        inf_str += "exp_method: {}".format(str(self.exp_method))
        inf_str += "global_exp_precision: {}".format(self.global_exp_precision)
        inf_str += "eval_grid_points"
        for lia in range(len(self.eval_grid_points)):
            inf_str += "    {} : {:2.4f}".format(lia, self.eval_grid_points[lia])
        inf_str += "eval_densities not here"
        inf_str += "mean: {}".format(self.mean)
        inf_str += "stoch_grid"
        for lia in range(len(self.stoch_grid)):
            inf_str += "    {} : {:2.4f}".format(lia, self.stoch_grid[lia])
        try:
            outfile = open(export_path + str(hash(self.file_name)) + '.dat', 'wb')
            Pickle.dump((self.solution, inf), outfile, Pickle.HIGHEST_PROTOCOL)
            outfile.close()
            outfile = open(export_path + str(hash(self.file_name)) + ".conf", 'wb')
            outfile.write(inf_str)
            outfile.close()
        except IOError as ex:
            self.log.error("can not export file to " + export_path + str(hash(self.file_name)) +
                           "  err: " + str(ex.strerror))
            return False
        return True

    # endregion

    # region overwrites Function Create Coefficient Field
    def create_coeff_field(self, funcs, mean):
        """
        creates a coefficient field given by the amplitude functions and a mean function
        """
        self.coeff_field = ParametricCoefficientField(mean, funcs, lambda i: UniformRV(a=-1, b=1))
    # endregion

    # region overwrites Function Create Forward Operator
    def create_forward_operator(self, coeff_field, coefficients, physical_grid, stochastic_grid):
        """
        creates the forward solution operator from a given coefficient field
        """
        op = KlDirectOperatorTT(coeff_field, len(physical_grid), coefficients)
        op.create_forward_operator(stochastic_grid, physical_grid)
        self.solution = op.sol
    # endregion

    # region overwrites Function Apply Observation
    def apply_observation(self):
        """
        applies the observation operator at given dofs and round eventually to given precision and rank
        """
        self.solution = apply_observation(self.solution, self.dofs, _precision=self.precision, _maxrank=self.rank,
                                          _print=False)
    # endregion

    # region overwrites Calculate Measurement Difference
    def calculate_measurement_diff(self, solution, value_list):
        """
        calculates the left side of the inner product
        """
        for lia in range(len(solution)):
            value = create_rank_one(value_list[lia], solution[lia].n)
            solution[lia] = value - solution[lia]
        self.solution = solution
    # endregion

    # region overwrites Function Apply Inner Product
    def apply_inner_product(self, solution, gamma, precision, rank):
        """
        calculates the misfit function with given variance as gamma*I and rounds result
        """
        self.solution = -0.5 * apply_inner_product(solution, _gamma=gamma, _precision=precision, _maxrank=rank,
                                                   _print=False)
    # endregion

    # region overwrites Function Calculate Marginal Densities
    def calculate_marginal_densities(self, solution, eval_densities, stoch_grid, integration_step_size,
                                     eval_grid_points):
        """
        calculates the interpolated marginal densities
        """
        y_int, self.mean = integrate_ydims_tt(solution, eval_densities, integration_step_size, stoch_grid)

        self.eval_densities = [IBayes.extrap1d(interp1d(stoch_grid, y_int[:, k], kind='cubic'))(eval_grid_points)
                               for k in range(len(solution.n)-1)]

        # self.eval_densities = [np.sum(np.array([evaluate_lagrange(eval_grid_points, stoch_grid, j) * y_int[j, k]
        #                       for j in range(len(stoch_grid))]), axis=0) for k in range(len(solution.n)-1)]
    # endregion

    # region static Function evaluate Karhunen-Loeve
    @staticmethod
    def kl(coeff_field, x, y):
        """
        calculates the Karhunen-Loeve expansion of the given coefficient field
        """
        return coeff_field.funcs(0)(x) + np.sum([coeff_field.funcs(m + 1)(x) * yi
                                                 for m, yi in enumerate(ensure_iterable(y))])
    # endregion

    # region Property File Name
    @property
    def file_name(self):
        """
        returns the file name of the current object
        """
        file_name = "direct_"
        file_name += "dofs%i_" % len(self.dofs)
        file_name += "M%i_" % len(self.true_values)
        file_name += "oP%.2e_" % self.obs_precision
        file_name += "oR%i_" % self.obs_rank
        file_name += "iP%.2e_" % self.inp_precision
        file_name += "iR%i_" % self.inp_rank
        file_name += "iG%.2e_" % self.inp_gamma
        file_name += str(self.exp_method) + "_"
        file_name += "den%i_" % len(self.eval_grid_points)
        file_name += "sG%i_" % len(self.stoch_grid)
        file_name += "pG%i_" % len(self.physical_grid)
        return file_name
    # endregion
