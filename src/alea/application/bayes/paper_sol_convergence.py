# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  test script for the bayesian procedure using direct sampling of the Karhunen Loeve expansion
"""
# region Imports
from __future__ import division

from dolfin.fem.norms import errornorm, norm
from Bayes_util.lib.bayes_lib import create_noisy_measurement_data_dofs
from Bayes_util.exponential.implicit_euler import ImplicitEuler
from scipy.integrate import quad
from scipy.interpolate import interp1d

from Bayes_util.asgfem_bayes import ASGFEMBayes
from Bayes_util.exponential.embedded_runge_kutta import EmbeddedRungeKutta
from Bayes_util.exponential.explicit_euler import ExplicitEuler
from Bayes_util.lib.bayes_lib_simple import create_unique_dofs
from Bayes_util.lib.interpolation_util import get_cheb_points
import logging.config
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
__author__ = "marschall"
# endregion


METHODS = ["", "measurements"]
METHOD = METHODS[0]
# region Setup parameters
#############################
#   Testsets
#   Set 1   for Method quadrature
#       refinements = 1
#       srank = 8
#       gpcdegree = 10
#       femdegree = 3
#       max_dofs = 1e5
#       iterations = 500
#       n_coefficients = 10
#       init_mesh_dofs = 1e4
#
#   Set 2   for method euler
#       refinements = 1
#       srank = 8
#       gpcdegree = 10
#       femdegree = 3
#       max_dofs = 1e5
#       iterations = 600
#       n_coefficients = 4
#       init_mesh_dofs = 1e4
#
#   Set 3
#       refinements = 10
#       srank = 8
#       gpcdegree = 10
#       femdegree = 3
#       max_dofs = 1e7
#       iterations = 500
#       n_coefficients = 10
#       init_mesh_dofs = 1e5
#
#   Set 4
#       refinements = 1
#       srank = 8
#       gpcdegree = 10
#       femdegree = 1
#       max_dofs = 1e5
#       iterations = 500
#       n_coefficients = 4
#       init_mesh_dofs = 1e4
# Test for sol convergence
#   Set 5
#       refinements = 15
#       srank = 8
#       gpcdegree = 10
#       femdegree = 3
#       max_dofs = 1e8
#       iterations = 600
#       n_coefficients = 5
#       init_mesh_dofs = 10
# Test for sol convergence with fix stoch dimension
#   Set 6
#       refinements = 10
#       srank = 8
#       gpcdegree = 10
#       femdegree = 3
#       max_dofs = 1e8
#       iterations = 600
#       n_coefficients = 5
#       init_mesh_dofs = 10
#   region Parameters to use in stochastic Galerkin for the backward solution
refinements = 5 			                        # # of refinement steps in sg
srank = 10					                        # start ranks
gpcdegree = 10					                    # maximal # of gpcdegrees
femdegree = 1					                    # fem function degree
max_dofs = 1e8                                      # desired maximum of dofs to reach in refinement process
iterations = 502                                    # used iterations in tt ALS
polysys = "L"                                       # used polynomial system
domain = "square"                                   # used domain, square, L-Shape, ...
# coeffield_type = "EF-square-cos"
# coeffield_type = "EF-square-sin"
# coeffield_type = "monomial"
# coeffield_type = "linear"
# coeffield_type = "constant"                       # used coefficient field type
coeffield_type = "cos"
# amp_type = "constant"
amp_type = "decay-inf"
decay_exp_rate = 2                                # decay rate for non constant coefficient fields
sgfem_gamma = 0.9                                   # Gamma used in adaptive SG FEM
field_mean = 100
rvtype = "uniform"
init_mesh_dofs = 1e2                                 # initial mesh refinement. -1 : ignore
thetax = 0.5                                          # factor of deterministic refinement (0 : no refinement)
thetay = 0.5                                          # factor of stochastic refinement (0 : no refinement)
#   endregion

# region Solution parameter
n_coefficients = 5                                  # number of coefficients
USE_DOFS = False
n_dofs = 54                                         # number of dofs
n_dofs_list = [2**lia + 2 for lia in range(6)]      # list of increasing dofs

sample_coordinates = []
measurement_refinements = None
if METHOD == "measurements":
    measurement_refinements = 6                     # number of refinements, i.e. number of nodes squared start from 4
    for lia in range(measurement_refinements):
        sample_coordinates.append([[i/int(np.sqrt((lia+3)**2)+1), j/int(np.sqrt((lia+3)**2)+1)]
                                   for i in range(1, int(np.sqrt((lia+3)**2)))
                                  for j in range(1, int(np.sqrt((lia+3)**2)))])
else:
    # sample_coordinates = [[0.25, 0.25], [0.25, 0.5], [0.25, 0.75],
    #                      [0.5, 0.25],  [0.5, 0.5],  [0.5, 0.75],
    #                      [0.75, 0.25], [0.75, 0.5], [0.75, 0.75]]

    sample_coordinates = [[1/7, 1/7], [1/7, 2/7], [1/7, 3/7], [1/7, 4/7], [1/7, 5/7], [1/7, 6/7],
                          [2/7, 1/7], [2/7, 2/7], [2/7, 3/7], [2/7, 4/7], [2/7, 5/7], [2/7, 6/7],
                          [3/7, 1/7], [3/7, 2/7], [3/7, 3/7], [3/7, 4/7], [3/7, 5/7], [3/7, 6/7],
                          [4/7, 1/7], [4/7, 2/7], [4/7, 3/7], [4/7, 4/7], [4/7, 5/7], [4/7, 6/7],
                          [5/7, 1/7], [5/7, 2/7], [5/7, 3/7], [5/7, 4/7], [5/7, 5/7], [5/7, 6/7],
                          [6/7, 1/7], [6/7, 2/7], [6/7, 3/7], [6/7, 4/7], [6/7, 5/7], [6/7, 6/7]
                          ]
# endregion

# region Observation parameter
obs_precision = 1e-15
obs_rank = 100
# endregion

# region Inner Product parameter
inp_precision = 1e-15
inp_rank = 100
inp_gamma = 0.0001
# endregion

# region Euler Parameter
euler_precision = 1e-10                             # precision to use in euler rounding
euler_rank = 10                                     # rank to round down in euler method
euler_local_mc_samples = 1                          # samples in every euler step
euler_global_mc_samples = 1000                      # samples for the final result
euler_repetitions = 0                               # number of repetitions with halfed step size
euler_refinements = None
euler_steps = 1000                                  # start euler steps
euler_global_precision = 1e-5                       # global precision to reach after the euler scheme
euler_use_implicit = False
# endregion

# region Density parameters
n_eval_grid = 50
# endregion

# region setup discretisation
Nx, Ny = 10000, 10
dx, dy = 1/Nx, 1/Ny
eval_grid_points = np.linspace(-1, 1, n_eval_grid)
quadrature_refinements = None
if METHOD == "quadrature":
    quadrature_refinements = 10
    stochastic_grid = np.array([np.array(get_cheb_points([2*lia+2] * n_coefficients)[0])
                                for lia in range(1, quadrature_refinements)])
else:
    stochastic_grid = np.array(get_cheb_points([Ny] * n_coefficients)[0])
# endregion

# region MCMC parameter
n_walker = 50
burn_in = 10
n_samples = 100
covariance = 0.0001
# endregion

color_list = ['olive', 'gold', 'aqua', 'black', 'blue', 'brown', 'green', 'red', 'orange']

logging.config.fileConfig('logging.conf')           # load logger configuration
log = logging.getLogger('test_bayes')               # get new logger object

# endregion
file_path = "results/paper/sol/" + amp_type + "/"
if thetax == 1:
    file_path += "fix/"

bayes_obj = None
bayes_obj_list = []
unique_dofs = []
coeff_field = None
proj_basis = None
true_values = None
prior_densities = None

true_solution = None
true_field = None

field_error_h1 = []
field_error_l2 = []
solution_error_h1 = []
solution_error_l2 = []
tt_dofs = []
log.info("Create exponential method")
if euler_use_implicit:
    euler_method = ImplicitEuler(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
                                 euler_repetitions, euler_local_mc_samples)
else:
    if True:
        euler_method = EmbeddedRungeKutta(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
                                          euler_repetitions, euler_local_mc_samples)
        euler_method.method = "forth"
        euler_method.acc = euler_global_precision
        euler_method.min_stepsize = 1e-19
        euler_method.max_stepsize = 1.0 - 1e-10
    else:
        euler_method = ExplicitEuler(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
                                     euler_repetitions, euler_local_mc_samples)
    plt.clf()
    log.info("Create Bayes Object with {} measurement nodes".format(len(sample_coordinates)))
    dofs = []                                   # we use the coordinate sampling
    bayes_obj = ASGFEMBayes.init_solver(dofs, sample_coordinates, true_values, obs_precision,
                                        obs_rank, inp_precision, inp_rank, inp_gamma, euler_method,
                                        euler_global_precision, eval_grid_points, prior_densities,
                                        stochastic_grid, convergence=1e-7,
                                        iterations=iterations, refinements=refinements, max_dofs=max_dofs,
                                        thetax=thetax,
                                        thetay=thetay, srank=srank,  m=n_coefficients+1,
                                        femdegree=femdegree, gpcdegree=gpcdegree, polysys=polysys,
                                        gamma=sgfem_gamma, rvtype=rvtype, domain=domain,
                                        mesh_dofs=init_mesh_dofs, problem=0,
                                        coeffield_type=coeffield_type,
                                        amp_type=amp_type, field_mean=field_mean
                                        )

    if len(bayes_obj.forwardOp.SOL) > 0:
        solution_error_h1 = []
        solution_error_l2 = []
        solution_error_h1_rel = []
        solution_error_l2_rel = []
        refine_list = []
        print("estimate solution error")
        for lic in range(len(bayes_obj.forwardOp.SOL)):
            refine_list.append(lic)
            tt_dofs.append(bayes_obj.forwardOp.SOL[lic]["DOFS"])
            print("SOL.dofs: {}".format(bayes_obj.forwardOp.SOL[lic]["DOFS"]))
            _solution_error_l2, _solution_error_h1 = bayes_obj.forwardOp.estimate_sampling_error(sol_index=lic,
                                                                                                 relative=False)
            _solution_error_l2_rel, _solution_error_h1_rel = bayes_obj.forwardOp.estimate_sampling_error(sol_index=lic,
                                                                                                         relative=True)
            __solution_error_l2 = 0
            __solution_error_h1 = 0
            __solution_error_l2_rel = 0
            __solution_error_h1_rel = 0
            for lix in range(len(_solution_error_h1)):
                __solution_error_l2 += _solution_error_l2[lix]
                __solution_error_h1 += _solution_error_h1[lix]
                __solution_error_l2_rel += _solution_error_l2_rel[lix]
                __solution_error_h1_rel += _solution_error_h1_rel[lix]
            __solution_error_l2 /= len(_solution_error_l2)
            __solution_error_h1 /= len(_solution_error_h1)
            __solution_error_l2_rel /= len(_solution_error_l2_rel)
            __solution_error_h1_rel /= len(_solution_error_h1_rel)
            solution_error_l2.append(__solution_error_l2)
            solution_error_h1.append(__solution_error_h1)
            solution_error_l2_rel.append(__solution_error_l2_rel)
            solution_error_h1_rel.append(__solution_error_h1_rel)
        plt.clf()
        plt.loglog(refine_list, np.array(solution_error_l2), '-or', label="L2 error")
        plt.loglog(refine_list, np.array(solution_error_h1), '-ob', label="H1 error")
        plt.title("Sampled ERROR of the forward solution.")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("adaptive refinement step")
        plt.ylabel("Sampled ERROR of the forward solution object")
        plt.savefig(file_path + "sol_error_absolute_" + str(hash(bayes_obj.file_name)) + ".png")
        plt.clf()
        plt.loglog(tt_dofs, np.array(solution_error_l2), '-or', label="L2 error")
        plt.loglog(tt_dofs, np.array(solution_error_h1), '-ob', label="H1 error")
        plt.title("Sampled ERROR of the forward solution.")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("tt-dofs")
        plt.ylabel("Sampled ERROR of the forward solution object")
        plt.savefig(file_path + "sol_error_absolute_tt_" + str(hash(bayes_obj.file_name)) + ".png")
        plt.clf()
        plt.loglog(refine_list, np.array(solution_error_l2_rel), '-or', label="L2 error")
        plt.loglog(refine_list, np.array(solution_error_h1_rel), '-ob', label="H1 error")
        plt.title("Sampled relative ERROR of the forward solution.")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("adaptive refinement step")
        plt.ylabel("Sampled relative ERROR of the forward solution object")
        plt.savefig(file_path + "sol_error_relative_" + str(hash(bayes_obj.file_name)) + ".png")
        plt.clf()
        plt.loglog(tt_dofs, np.array(solution_error_l2_rel), '-or', label="L2 error")
        plt.loglog(tt_dofs, np.array(solution_error_h1_rel), '-ob', label="H1 error")
        plt.title("Sampled relative ERROR of the forward solution.")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("tt-dofs")
        plt.ylabel("Sampled relative ERROR of the forward solution object")
        plt.savefig(file_path + "sol_error_relative_tt_" + str(hash(bayes_obj.file_name)) + ".png")
        plt.clf()

        solution_error_l2 = []
        solution_error_h1 = []
        solution_error_l2_rel = []
        solution_error_h1_rel = []
        field_error_l2_rel = []
        field_error_h1_rel = []
        approx_field_list = []
        approx_solution_list = []
        tt_dofs = []
        for lia in range(len(bayes_obj.forwardOp.SOL)):
            bayes_obj.set_sol_index(lia)
            if bayes_obj.import_bayes(file_path):
                bayes_obj_list.append(bayes_obj)
            else:
                log.info('start Bayesian procedure with {} measurement nodes'.format(len(sample_coordinates)))
                # region  setup truth, noise and prior
                log.info("Create {} true parameters".format(bayes_obj.forwardOp.coefficients))
                # true_values = create_parameter_samples(bayes_obj.forwardOp.coefficients)
                # true_values = [0, 0.2, -0.2, 0.4]
                true_values = [0.2, -0.4, 0, 0.3, 0.4, 0.5, -0.1, -0.2, -0.3, 0.1, -0.5, 0.05, -0.05, 0.6, 0.7, 0.8,
                               0.9, -0.6, -0.7, -0.8, -0.9, 0, 0.04]
                if USE_DOFS:
                    unique_dofs = np.array(create_unique_dofs(n_dofs_list[-1], init_mesh_dofs))
                # endregion

                # region Setup Gaussian Prior
                Gauss_prior = False
                if Gauss_prior:
                    raise NotImplemented
                    # pi_y =
                    # [lambda x: 1/(np.sqrt(2*np.pi)*sigma_i)*np.exp(-0.5*((x-mu_i)/sigma_i)**2) for mu_i, sigma_i in
                    # zip(mu, sigma)]
                else:
                    prior_densities = [lambda x: 0.5 * np.ones_like(x)] * bayes_obj.forwardOp.coefficients
                # endregion
                if USE_DOFS:
                    measurements = create_noisy_measurement_data_dofs(bayes_obj.solution, true_values, dofs, inp_gamma)
                else:
                    buffer_true_solution = bayes_obj.forwardOp.compute_direct_sample_solution(true_values)
                    measurements = np.array([buffer_true_solution(sample_coordinate)
                                             for sample_coordinate in sample_coordinates])
                # measurements = create_noisy_measurement_data_dofs(bayes_obj.solution, true_values, dofs, inp_gamma)
                bayes_obj.measurements = measurements
                bayes_obj.true_values = true_values[:len(bayes_obj.solution.n)-1]
                bayes_obj.prior_densities = prior_densities
                bayes_obj.calculate_densities()

            tt_dofs.append(bayes_obj.forwardOp.SOL[lia]["DOFS"])

            log.info('end Bayesian procedure')

            log.info('export Bayes file %s', file_path + bayes_obj.file_name + '.dat')
            if not bayes_obj.export_bayes(file_path):
                log.error("can not save bayes object")
            bayes_obj_list.append(bayes_obj)

            print("Bayes obj {} loaded from file".format(lia))
            poly_mean = []
            for i, yi in enumerate(bayes_obj.eval_densities):
                scaled_density = []
                scaled_domain = []
                curr_mean = 0
                curr_mean_counter = 0
                max_value = max(yi)
                left_bound = yi[0]
                right_bound = yi[-1]
                if False:  # max_value - 0.5 < 0.01:
                    poly_mean.append(0)
                else:
                    if left_bound > right_bound:        # left bigger than right
                                                        # at least a bit curved
                        if max_value - left_bound > 1e-16:
                            curr_mean_counter = 0
                            for lic, entry in enumerate(yi):
                                if entry - left_bound >= 0:
                                                        # store only positive values
                                    scaled_density.append(entry - left_bound)
                                    # print "left_concave add density: {}".format(entry-left_bound)
                                    scaled_domain.append(bayes_obj.eval_grid_points[lic])
                                    curr_mean += entry - left_bound
                                    curr_mean_counter += 1
                            if curr_mean_counter > 4:
                                curr_mean *= curr_mean_counter**(-1)
                                scaled_density = np.array(scaled_density) * curr_mean**(-1)
                                I = interp1d(scaled_domain, scaled_density, kind='cubic')
                                poly_mean.append(quad(lambda _x: _x*I(_x), scaled_domain[0], scaled_domain[-1])[0])
                            else:
                                poly_mean.append(0)
                        else:                           # not that strongly curved, no scaling
                            scaled_density = yi
                            scaled_domain = bayes_obj.eval_grid_points
                            I = interp1d(scaled_domain, scaled_density, kind='cubic')
                            poly_mean.append(quad(lambda _x: _x*I(_x), scaled_domain[0], scaled_domain[-1])[0])
                    else:                               # right bigger than left
                        if max_value - right_bound > 1e-16:
                            curr_mean_counter = 0
                            for lic, entry in enumerate(yi):
                                if entry - right_bound >= 0:
                                                        # store only positive values
                                    scaled_density.append(entry - right_bound)
                                    # print "right_concave add density: {}".format(entry-right_bound)
                                    scaled_domain.append(bayes_obj.eval_grid_points[lic])
                                    curr_mean += entry - right_bound
                                    curr_mean_counter += 1
                            if curr_mean_counter > 4:
                                curr_mean *= curr_mean_counter**(-1)
                                scaled_density = np.array(scaled_density) * curr_mean**(-1)
                                I = interp1d(scaled_domain, scaled_density, kind='cubic')
                                poly_mean.append(quad(lambda _x: _x*I(_x), scaled_domain[0], scaled_domain[-1])[0])
                            else:
                                poly_mean.append(0)
                        else:
                            scaled_density = yi
                            scaled_domain = bayes_obj.eval_grid_points
                            I = interp1d(scaled_domain, scaled_density, kind='cubic')
                            poly_mean.append(quad(lambda _x: _x*I(_x), scaled_domain[0], scaled_domain[-1])[0])
                # print "poly_mean length:
                # {} vs true_values length: {}".format(len(poly_mean), len(bayes_obj.true_values))
                print("poly_mean: {} vs. true_value: {}".format(poly_mean[-1], bayes_obj.true_values[i]))
                plt.plot(bayes_obj.eval_grid_points, yi,
                         label="y_{0} should {1}".format(i, bayes_obj.true_values[i]),
                         color=color_list[i % len(color_list)])
                # print scaled_density
                # print scaled_domain
                plt.plot(scaled_domain, scaled_density, '--',
                         label="scaled y_{0} should {1}".format(i, bayes_obj.true_values[i]),
                         color=color_list[i % len(color_list)])
                plt.xlim([-1, 1])
                plt.vlines(bayes_obj.true_values[i], 0, max(scaled_density),
                           color=color_list[i % len(color_list)])
            # plt.legend(ncol=len(bayes_obj.eval_densities)*2, loc=3)
            plt.draw()
            plt.savefig(file_path + "IND{}_".format(lia) +
                        str(hash(bayes_obj.file_name)) + "_dens.pdf", dpi=None, facecolor='w', edgecolor='w',
                        orientation='portrait', papertype=None, format='pdf',
                        transparent=False, bbox_inches='tight', pad_inches=0.1,
                        frameon=None)
            plt.clf()
            # from tt_als.alea.fem.fenics.fenics_vector import plot
            # print "mid: {}".format(poly_mean)
            # print "exact: {}".format(bayes_obj.true_values)
            # print "max: {}".format(poly_max_arg)
            # print str(hash(bayes_obj.file_name))
            # if True:
            #     exit()
            true_field = bayes_obj.get_true_coefficient_field()._fefunc
            approx_field = bayes_obj.get_parametric_coefficient_field(poly_mean)._fefunc
            approx_field_list.append(approx_field)
            from dolfin import File
            # true_field_plot = plot(true_field, interactive=False)
            # true_field_plot.write_png(file_path + str(hash(bayes_obj.file_name)) + "true_field.png")
            # plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "true_field.png")

            approx_field_file = File(file_path + str(hash(bayes_obj.file_name)) +
                                     "IND{}_approx_field.pvd".format(lia))
            approx_field_file << approx_field

            field_error_l2.append(errornorm(true_field, approx_field, norm_type="l2", degree_rise=0))
            field_error_h1.append(errornorm(true_field, approx_field, norm_type="h1", degree_rise=0))
            field_error_l2_rel.append(errornorm(true_field, approx_field, norm_type="l2", degree_rise=0)
                                      / norm(true_field))
            field_error_h1_rel.append(errornorm(true_field, approx_field, norm_type="h1", degree_rise=0)
                                      / norm(true_field))
            print(bayes_obj.true_values)
            true_solution = bayes_obj.forwardOp.compute_direct_sample_solution(bayes_obj.true_values, sol_index=lia)._fefunc
            approx_solution = bayes_obj.forwardOp.compute_direct_sample_solution(poly_mean, sol_index=lia)._fefunc
            approx_solution_list.append(approx_solution)
            approx_solution_file = File(file_path + str(hash(bayes_obj.file_name)) +
                                        "IND{}_approx_solution.pvd".format(lia))
            approx_solution_file << approx_solution

            solution_error_h1.append(errornorm(true_solution, approx_solution, norm_type='h1', degree_rise=0))
            solution_error_l2.append(errornorm(true_solution, approx_solution, norm_type='l2', degree_rise=0))
            solution_error_h1_rel.append(errornorm(true_solution, approx_solution, norm_type='h1', degree_rise=0)
                                         / norm(true_solution))
            solution_error_l2_rel.append(errornorm(true_solution, approx_solution, norm_type='l2', degree_rise=0)
                                         / norm(true_solution))
        plt.clf()
        plt.loglog(np.array(field_error_l2), '-or', label="L2 error")
        plt.loglog(np.array(field_error_h1), '-ob', label="H1 error")
        plt.title("Error of the coefficient field. True vs Approx")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("refinement")
        plt.ylabel("ERROR of the coefficient field")
        plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "coef_error.png")
        plt.clf()
        plt.loglog(np.array(solution_error_l2), '-or', label="L2 error")
        plt.loglog(np.array(solution_error_h1), '-ob', label="H1 error")
        plt.title("Error of the posterior forward solution. True vs Approx")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("refinement")
        plt.ylabel("ERROR of the posterior forward solution")
        plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "sol_error.png")

        plt.clf()
        plt.loglog(np.array(field_error_l2_rel), '-or', label="L2 error")
        plt.loglog(np.array(field_error_h1_rel), '-ob', label="H1 error")
        plt.title("Relative Error of the coefficient field. True vs Approx")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("refinement")
        plt.ylabel("ERROR of the coefficient field")
        plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "coef_error_rel.png")
        plt.clf()
        plt.loglog(np.array(solution_error_l2_rel), '-or', label="L2 error")
        plt.loglog(np.array(solution_error_h1_rel), '-ob', label="H1 error")
        plt.title("Relative Error of the posterior forward solution. True vs Approx")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("refinement")
        plt.ylabel("ERROR of the posterior forward solution")
        plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "sol_error_rel.png")

        plt.clf()
        plt.loglog(tt_dofs, np.array(field_error_l2), '-or', label="L2 error")
        plt.loglog(tt_dofs, np.array(field_error_h1), '-ob', label="H1 error")
        plt.title("Error of the coefficient field. True vs Approx")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("refinement")
        plt.ylabel("ERROR of the coefficient field")
        plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "coef_error_tt.png")
        plt.clf()
        plt.loglog(tt_dofs, np.array(solution_error_l2), '-or', label="L2 error")
        plt.loglog(tt_dofs, np.array(solution_error_h1), '-ob', label="H1 error")
        plt.title("Error of the posterior forward solution. True vs Approx")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("refinement")
        plt.ylabel("ERROR of the posterior forward solution")
        plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "sol_error_tt.png")

        plt.clf()
        plt.loglog(tt_dofs, np.array(field_error_l2_rel), '-or', label="L2 error")
        plt.loglog(tt_dofs, np.array(field_error_h1_rel), '-ob', label="H1 error")
        plt.title("Relative Error of the coefficient field. True vs Approx")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("refinement")
        plt.ylabel("ERROR of the coefficient field")
        plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "coef_error_rel_tt.png")
        plt.clf()
        plt.loglog(tt_dofs, np.array(solution_error_l2_rel), '-or', label="L2 error")
        plt.loglog(tt_dofs, np.array(solution_error_h1_rel), '-ob', label="H1 error")
        plt.title("Relative Error of the posterior forward solution. True vs Approx")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("refinement")
        plt.ylabel("ERROR of the posterior forward solution")
        plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "sol_error_rel_tt.png")
        
        field_error_l2 = []
        field_error_h1 = []
        field_error_h1_rel = []
        field_error_l2_rel = []
        ref_field = approx_field_list[-1]
        for lia in range(len(approx_field_list)-1):
            curr_field = approx_field_list[lia]
            field_error_l2.append(errornorm(ref_field, curr_field, norm_type="l2", degree_rise=0))
            field_error_h1.append(errornorm(ref_field, curr_field, norm_type="h1", degree_rise=0))
            field_error_l2_rel.append(errornorm(ref_field, curr_field, norm_type="l2", degree_rise=0)/norm(ref_field))
            field_error_h1_rel.append(errornorm(ref_field, curr_field, norm_type="h1", degree_rise=0)/norm(ref_field))
                
        solution_error_l2 = []
        solution_error_h1 = []
        solution_error_h1_rel = []
        solution_error_l2_rel = []
        ref_solution = approx_solution_list[-1]
        for lia in range(len(approx_solution_list)-1):
            curr_solution = approx_solution_list[lia]
            solution_error_l2.append(errornorm(ref_solution, curr_solution, norm_type="l2", degree_rise=0))
            solution_error_h1.append(errornorm(ref_solution, curr_solution, norm_type="h1", degree_rise=0))
            solution_error_l2_rel.append(errornorm(ref_solution, curr_solution, norm_type="l2", degree_rise=0)
                                         / norm(ref_solution))
            solution_error_h1_rel.append(errornorm(ref_solution, curr_solution, norm_type="h1", degree_rise=0)
                                         / norm(ref_solution))

        plt.clf()
        plt.loglog(np.array(field_error_l2), '-or', label="L2 error")
        plt.loglog(np.array(field_error_h1), '-ob', label="H1 error")
        plt.title("Convergence of the coefficient field. Reference vs Approx")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("refinement")
        plt.ylabel("ERROR of the coefficient field")
        plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "coef_conv.png")
        plt.clf()
        plt.loglog(np.array(solution_error_l2), '-or', label="L2 error")
        plt.loglog(np.array(solution_error_h1), '-ob', label="H1 error")
        plt.title("Convergence of the posterior forward solution. Reference vs Approx")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("refinement")
        plt.ylabel("ERROR of the posterior forward solution")
        plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "sol_conv.png")

        plt.clf()
        plt.loglog(np.array(field_error_l2_rel), '-or', label="L2 error")
        plt.loglog(np.array(field_error_h1_rel), '-ob', label="H1 error")
        plt.title("Convergence of the coefficient field. Reference vs Approx")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("refinement")
        plt.ylabel("ERROR of the coefficient field")
        plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "coef_conv_rel.png")
        plt.clf()
        plt.loglog(np.array(solution_error_l2_rel), '-or', label="L2 error")
        plt.loglog(np.array(solution_error_h1_rel), '-ob', label="H1 error")
        plt.title("Convergence of the posterior forward solution. Reference vs Approx")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("refinement")
        plt.ylabel("convergence of the posterior forward solution")
        plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "sol_conv_rel.png")

        plt.clf()
        plt.loglog(tt_dofs[:-1], np.array(field_error_l2), '-or', label="L2 error")
        plt.loglog(tt_dofs[:-1], np.array(field_error_h1), '-ob', label="H1 error")
        plt.title("Convergence of the coefficient field. Reference vs Approx")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("refinement")
        plt.ylabel("ERROR of the coefficient field")
        plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "coef_conv_tt.png")
        plt.clf()
        plt.loglog(tt_dofs[:-1], np.array(solution_error_l2), '-or', label="L2 error")
        plt.loglog(tt_dofs[:-1], np.array(solution_error_h1), '-ob', label="H1 error")
        plt.title("Convergence of the posterior forward solution. Reference vs Approx")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("refinement")
        plt.ylabel("ERROR of the posterior forward solution")
        plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "sol_conv_tt.png")

        plt.clf()
        plt.loglog(tt_dofs[:-1], np.array(field_error_l2_rel), '-or', label="L2 error")
        plt.loglog(tt_dofs[:-1], np.array(field_error_h1_rel), '-ob', label="H1 error")
        plt.title("Convergence of the coefficient field. Reference vs Approx")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("refinement")
        plt.ylabel("ERROR of the coefficient field")
        plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "coef_conv_rel_tt.png")
        plt.clf()
        plt.loglog(tt_dofs[:-1], np.array(solution_error_l2_rel), '-or', label="L2 error")
        plt.loglog(tt_dofs[:-1], np.array(solution_error_h1_rel), '-ob', label="H1 error")
        plt.title("Convergence of the posterior forward solution. Reference vs Approx")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("refinement")
        plt.ylabel("convergence of the posterior forward solution")
        plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "sol_conv_rel_tt.png")

        field_error_l2 = []
        field_error_h1 = []
        field_error_h1_rel = []
        field_error_l2_rel = []
        for lia in range(len(approx_field_list)):
            curr_field = approx_field_list[lia]
            field_error_l2.append(errornorm(true_field, curr_field, norm_type="l2", degree_rise=0))
            field_error_h1.append(errornorm(true_field, curr_field, norm_type="h1", degree_rise=0))
            field_error_l2_rel.append(errornorm(true_field, curr_field, norm_type="l2", degree_rise=0)/norm(true_field))
            field_error_h1_rel.append(errornorm(true_field, curr_field, norm_type="h1", degree_rise=0)/norm(true_field))

        solution_error_l2 = []
        solution_error_h1 = []
        solution_error_h1_rel = []
        solution_error_l2_rel = []
        for lia in range(len(approx_solution_list)):
            curr_solution = approx_solution_list[lia]
            solution_error_l2.append(errornorm(true_solution, curr_solution, norm_type="l2", degree_rise=0))
            solution_error_h1.append(errornorm(true_solution, curr_solution, norm_type="h1", degree_rise=0))
            solution_error_l2_rel.append(errornorm(true_solution, curr_solution, norm_type="l2", degree_rise=0)
                                         / norm(true_solution))
            solution_error_h1_rel.append(errornorm(true_solution, curr_solution, norm_type="h1", degree_rise=0)
                                         / norm(true_solution))

        plt.clf()
        plt.loglog(np.array(field_error_l2), '-or', label="L2 error")
        plt.loglog(np.array(field_error_h1), '-ob', label="H1 error")
        plt.title("Convergence of the coefficient field. Fine True vs Approx")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("refinement")
        plt.ylabel("ERROR of the coefficient field")
        plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "coef_conv_true.png")
        plt.clf()
        plt.loglog(np.array(solution_error_l2), '-or', label="L2 error")
        plt.loglog(np.array(solution_error_h1), '-ob', label="H1 error")
        plt.title("Convergence of the posterior forward solution. Fine True vs Approx")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("refinement")
        plt.ylabel("ERROR of the posterior forward solution")
        plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "sol_conv_fine.png")

        plt.clf()
        plt.loglog(np.array(field_error_l2_rel), '-or', label="L2 error")
        plt.loglog(np.array(field_error_h1_rel), '-ob', label="H1 error")
        plt.title("Convergence of the coefficient field. Fine True vs Approx")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("refinement")
        plt.ylabel("ERROR of the coefficient field")
        plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "coef_conv_rel_fine.png")
        plt.clf()
        plt.loglog(np.array(solution_error_l2_rel), '-or', label="L2 error")
        plt.loglog(np.array(solution_error_h1_rel), '-ob', label="H1 error")
        plt.title("Convergence of the posterior forward solution. Fine True vs Approx")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("refinement")
        plt.ylabel("convergence of the posterior forward solution")
        plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "sol_conv_rel_fine.png")

        plt.clf()
        plt.loglog(tt_dofs, np.array(field_error_l2), '-or', label="L2 error")
        plt.loglog(tt_dofs, np.array(field_error_h1), '-ob', label="H1 error")
        plt.title("Convergence of the coefficient field. Fine True vs Approx")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("refinement")
        plt.ylabel("ERROR of the coefficient field")
        plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "coef_conv_tt_fine.png")
        plt.clf()
        plt.loglog(tt_dofs, np.array(solution_error_l2), '-or', label="L2 error")
        plt.loglog(tt_dofs, np.array(solution_error_h1), '-ob', label="H1 error")
        plt.title("Convergence of the posterior forward solution. Fine True vs Approx")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("refinement")
        plt.ylabel("ERROR of the posterior forward solution")
        plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "sol_conv_tt_fine.png")

        plt.clf()
        plt.loglog(tt_dofs, np.array(field_error_l2_rel), '-or', label="L2 error")
        plt.loglog(tt_dofs, np.array(field_error_h1_rel), '-ob', label="H1 error")
        plt.title("Convergence of the coefficient field. Fine True vs Approx")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("refinement")
        plt.ylabel("ERROR of the coefficient field")
        plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "coef_conv_rel_tt_fine.png")
        plt.clf()
        plt.loglog(tt_dofs, np.array(solution_error_l2_rel), '-or', label="L2 error")
        plt.loglog(tt_dofs, np.array(solution_error_h1_rel), '-ob', label="H1 error")
        plt.title("Convergence of the posterior forward solution. Fine True vs Approx")
        plt.legend(ncol=2, loc=1)
        plt.xlabel("refinement")
        plt.ylabel("convergence of the posterior forward solution")
        plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "sol_conv_rel_tt_fine.png")

        true_field_file = File(file_path + str(hash(bayes_obj.file_name)) + "true_field.pvd")
        true_field_file << true_field
        true_solution_file = File(file_path + str(hash(bayes_obj.file_name)) + "true_solution.pvd")
        true_solution_file << true_solution
