#!/bin/bash

# python Sampling/forward_solution/lognormal_sgfem.py  -at decay-inf -de 1.2 -fp results/lognormal_sgfem/ -pp results/lognormal_sgfem/plot/ -res -fm 0 -zmc 2 -cpu 50 -ir 20 -or 20 -er 21  -hd 10 -mr 10 -sr 4 -nc 3 -frsk 0 -sc 1.0

# python Sampling/forward_solution/lognormal_sgfem.py  -at decay-algebraic -de 2.0 -fp results/lognormal_sgfem/ -pp results/lognormal_sgfem/plot/ -coef -fm 0.1 -zmc 1 -cpu 50 -ir 20 -or 20 -er 21  -hd 10 -mr 11 -sr 4 -nc 5 -frsk 10 -plot -sca 20 -tx 0

mpirun -np 1 python Sampling/forward_solution/lognormal_sgfem.py  -de 2 -fp results/lognormal_sgfem/ -pp results/lognormal_sgfem/plot/ -sol -fm 0.0 -zmc 10 -cpu 40 -ir 20 -or 20 -er 21  -hd 2 -mr 50 -sr 50 -nc 1  -frsk 0 -sc 1.0 -it 100 -mer 0 -imd 10 -fd 3 -dom square -tol 1e-10 -adapt_it 5 -thx 0.5 -thy 0.5 -start_rank 3 -num_coef_coef 20 -max_hdegs_coef 20 -ezw 1.0 -rzw 1.0 -coef_mesh_dofs 10000 -coef_quad_degree 7 -new_hdeg 2 -tx 0.1 -md 100000
