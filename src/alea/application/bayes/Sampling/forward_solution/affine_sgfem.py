"""
test script to sample the forward solution of the asgfem using the affine tensor train description
@author: Manuel Marschall
"""

# region imports
from __future__ import division

import argparse  # use command parameter parsing
from dolfin import FunctionSpace, refine, Function, File, errornorm, norm, UnitSquareMesh, set_log_level, ERROR, \
    project, cells, Mesh, interpolate, Constant

import matplotlib.tri as tri
import matplotlib.pyplot as plt
import matplotlib2tikz as tikz
import tt
import numpy as np
import scipy.sparse as sps
from alea.utils.plothelper import plot_mpl
import os
import shutil
import errno
import argparse
from alea.application.bayes.Bayes_util.ForwardOperator.tt_asgfem_operator import TTAsgfemOperator

from alea.application.bayes.alea_util.coefficient_field.affine_field import (AffineField,
                                                                             get_extendedtt_from_affine_field)
from alea.linalg.tensor.extended_fem_tt import ExtendedFEMTT, BasisType
from alea.application.bayes.Bayes_util.ForwardOperator.tt_forward_operator_util import compute_overall_error_indicator
from alea.application.bayes.alea_util.configuration.asgfem_config import AsgfemConfig
from alea.application.egsz.sample_domains import SampleDomain
from alea.application.egsz.sample_problems2 import SampleProblem

from alea.application.tt_asgfem.tensorsolver.tt_sgfem_util import tt_dofs
from alea.utils.tictoc import TicToc
from alea.utils.timing import get_current_time_string
from alea.utils.plothelper import write_2d_png, write_3d_png, write_pvd_file
from alea.application.bayes.Bayes_util.ForwardOperator.tt_forward_operator_util import calculate_stiffness_matrix, \
    calculate_mass_matrix, create_order4_stiffness, create_order4_volume_operator_list

from copy import deepcopy

set_log_level(ERROR)
# endregion

# region parser arguments
parser = argparse.ArgumentParser(description="estimate the MC convergence against the TT-mean")
parser.add_argument("-f", "--file", type=str, default="default.pro",
                    help="problem file to load")
parser.add_argument("-sf", "--solutionFile", type=str, default="",
                    help="file containing hashes of the collected solutions to load")
parser.add_argument("-nsf", "--newSolutionFile", type=str, default="",
                    help="new file containing hashes of the collected solutions to load, starting at given value")
parser.add_argument("-ind", "--newSolutionFileIndex", type=int, default=0,
                    help="new file containing hashes of the collected solutions to load, starting at given value")
parser.add_argument("-bound", "--bound_sol", type=int, default=-1,
                    help="integer value to bound the maximal polynomial degree in the solution")
parser.add_argument("-sample", "--sample", action="store_true",
                    help="switch to deactivate the computation. Just sample from the given list")
parser.add_argument("-mesh_uniform", "--mesh_uniform", action="store_true",
                    help="switch to deactivate the adaptivity in the physical mesh")
parser.add_argument("-sto_uniform", "--sto_uniform", action="store_true",
                    help="switch to deactivate the adaptivity in the stochastic space")
parser.add_argument("-new", "--new_solution_file", action="store_true",
                    help="switch to overwrite the existing solution file")
parser.add_argument("-goo", "--go_on", action="store_true",
                    help="switch to use the last existing found file as input for the next step")
parser.add_argument("-ns", "--no_save", action="store_true",
                    help="switch to not store new results in the solution File provided (debug or simple testing)")
parser.add_argument("-mesh_once", "--mesh_once", action="store_true",
                    help="switch to refine the mesh only and make only one step")
parser.add_argument("-sto_once", "--sto_once", action="store_true",
                    help="switch to refine the stochastic only and make only one step")

args = parser.parse_args()
problem_file = "../../alea_util/configuration/problems/" + args.file
if args.solutionFile == "":
    cache_file = "../../Bayes_util/ForwardOperator/tmp/" + get_current_time_string() + "solutions.txt"
else:
    cache_file = "../../Bayes_util/ForwardOperator/tmp/" + args.solutionFile
sample_only = args.sample
overwrite = args.new_solution_file
refine_mesh_uniform = args.mesh_uniform
refine_sto_uniform = args.sto_uniform
go_on = args.go_on
no_save = args.no_save
new_solution_file = args.newSolutionFile
new_solution_file_index = args.newSolutionFileIndex
mesh_once = args.mesh_once
sto_once = args.sto_once
bound_sol = args.bound_sol
create_new_solution_file = False
if new_solution_file != "":
    assert (new_solution_file_index is not None)
    create_new_solution_file = True
    new_cache_file = "../../Bayes_util/ForwardOperator/tmp/" + new_solution_file
# endregion

# region pre-computations and object creation
# region Read configuration from problem file
with TicToc(sec_key="Configuration", key="read configuration", do_print=False, active=True):
    config = AsgfemConfig(problem_file)
    if config.read_configuration() is False:
        exit()

    if refine_mesh_uniform is True:
        config.theta_x = 0
# endregion

# region construct initial ranks and hermite degrees
if config.start_rank < config.n_coefficients:
    curr_rank = [1] + [config.start_rank] * config.n_coefficients + [1]
else:                                               # in affine case: M+1 = rank of coef
    curr_rank = [1] + [config.n_coefficients+1] * config.n_coefficients + [1]
if isinstance(config.gpc_degree, list):
    curr_hdeg = config.gpc_degree
else:
    curr_hdeg = [config.hermite_degree] * config.n_coefficients
curr_solution = []

# endregion
# region create initial fs
with TicToc(sec_key="initial function space", key="create", do_print=False, active=True):
    if config.init_mesh_dofs > 0:  # mesh dofs are defined
        mesh_refine = 0  # there are no mesh refinements
    else:
        mesh_refine = config.mesh_refine  # use configured mesh refinements

    mesh, boundaries, dim = SampleDomain.setupDomain(config.domain, initial_mesh_N=2)
    #                                               # refine mesh according to input
    mesh = SampleProblem.setupMesh(mesh, num_refine=mesh_refine)
    #                                               # refinement if you want to have a specific amount of dofs

    print("soll mesh_dofs={} haben dofs={}".format(config.init_mesh_dofs,
                                                   FunctionSpace(mesh, 'CG', config.femdegree).dim()))
    while config.init_mesh_dofs > 0 and FunctionSpace(mesh, 'CG', config.femdegree).dim() < config.init_mesh_dofs:
        mesh = refine(mesh)  # uniformly refine mesh

    curr_fs = FunctionSpace(mesh, 'CG', config.femdegree)
    # region read coefficient from configuration
    with TicToc(sec_key="Semi-Disc Coef", key="read from configuration", do_print=False, active=True):
        affine_field = AffineField(config.coeffield_type, "decay-algebraic", config.decay_exp_rate,
                                   config.sgfem_gamma, config.freq_scale, config.freq_skip, config.scale,
                                   config.field_mean)

        semi_discrete_coef = get_extendedtt_from_affine_field(affine_field, curr_fs, config.n_coefficients)
    # endregion

    pde, _, _, _, _, _ = SampleProblem.setupPDE(2, config.domain, 0, boundaries, affine_field.coeff_field)

# endregion

# region create rhs extended TT tensor
comp_list = []
basis_list = [BasisType.points] + [BasisType.NormalisedLegendre]*config.n_coefficients
curr_comp = np.ones(curr_fs.dim())                  # constant 1 rhs function
curr_comp = np.reshape(curr_comp, (1, curr_fs.dim(), 1))
comp_list.append(curr_comp)
for lia in range(config.n_coefficients):
    curr_comp = np.zeros((1, curr_hdeg[lia], 1))
    curr_comp[0, 0, 0] = 1                          # constant in every stochastic dimension
    comp_list.append(curr_comp)
curr_rhs = ExtendedFEMTT(comp_list, basis_list, curr_fs)
curr_rhs.canonicalize_left()
curr_rhs.normalise()

# endregion
# endregion

# region adaptive solution process
refined = None
n_samples = 0
tail_sample_factor = 1
curr_order4_stiffness_tensor = None
curr_order4_volume_tensor_list = []
refinement_cache = None
# y_list = [semi_discrete_coef.sample_rvs(config.num_coeff_in_coeff)*tail_sample_factor for _ in range(n_samples)]
for step in range(config.adaptive_iterations):
    # print(TicToc.sortedTimes(sec_sorted=True))
    print("=" * 30)
    print("iteration step {} / {}".format(step + 1, config.adaptive_iterations))

    print("rhs: {}".format(curr_rhs))
    print("coef: {}".format(semi_discrete_coef))
    print("sol: {}".format(curr_solution))
    # region create current function space
    with TicToc(sec_key="FunctionSpace", key="create new fs", do_print=False, active=True):
        curr_fs = FunctionSpace(mesh, 'CG', config.femdegree)
    # endregion

    # if refined == "det" or step == 0:
        # region Update order 4 stiffness tensor
        # with TicToc(sec_key="FunctionSpace", key="create order 4 residual tensor", do_print=True, active=True):
        #     curr_order4_stiffness_tensor = create_order4_stiffness(curr_fs)
        # with TicToc(sec_key="FunctionSpace", key="create order 4 sq residual tensor", do_print=True, active=True):
        #     curr_order4_volume_tensor_list = create_order4_volume_operator_list(curr_fs)
        # endregion
    # region update coefficient and rhs
    with TicToc(sec_key="FunctionSpace", key="project coefficient", do_print=True, active=True):
        new_coef_comp = np.zeros((1, curr_fs.dim(), semi_discrete_coef.components[0].shape[2]))
        curr_coef_fun = Function(semi_discrete_coef.first_comp_fs)
        for k in range(semi_discrete_coef.components[0].shape[2]):
            curr_coef_fun.vector()[:] = np.ascontiguousarray(semi_discrete_coef.components[0][0, :, k])
            new_coef_comp[0, :, k] = interpolate(curr_coef_fun, curr_fs).vector()[:]
        semi_discrete_coef.components[0] = new_coef_comp
        semi_discrete_coef.first_comp_fs = curr_fs
        semi_discrete_coef.n[0] = curr_fs.dim()

    with TicToc(sec_key="FunctionSpace", key="project rhs", do_print=True, active=True):
        new_rhs_comp = np.zeros((1, curr_fs.dim(), curr_rhs.components[0].shape[2]))
        curr_rhs_fun = Function(curr_rhs.first_comp_fs)
        for k in range(curr_rhs.components[0].shape[2]):
            curr_rhs_fun.vector()[:] = np.ascontiguousarray(curr_rhs.components[0][0, :, k])
            new_rhs_comp[0, :, k] = interpolate(curr_rhs_fun, curr_fs).vector()[:]
        curr_rhs.components[0] = new_rhs_comp
        curr_rhs.first_comp_fs = curr_fs
        curr_rhs.n[0] = curr_fs.dim()
    # endregion
    # region create current forward operator
    with TicToc(sec_key="Forward Operator", key="create new operator", do_print=False, active=True):
        for lia in range(len(curr_rank)):
            if curr_rank[lia] < semi_discrete_coef.r[lia]:
                print("solution ranks must be at least as big as the coef rank -> update min ranks")
                curr_rank = semi_discrete_coef.r
                break
        print("curr rank: {}".format(curr_rank))
        print("curr deg: {}".format(curr_hdeg))
        curr_forward_operator = TTAsgfemOperator(semi_discrete_coef, curr_rhs, curr_rank, curr_hdeg, curr_fs)
    # endregion

    # region solve the problem
    # if max(curr_rank) > max(tt.vector.from_list(curr_solution).r):
    #     curr_solution = []
    with TicToc(key="ALS solver", active=True, do_print=True):
        curr_forward_operator.solve(curr_rank, config.preconditioner, config.iterations, config.als_tol,
                                    curr_solution)
    assert isinstance(curr_forward_operator.solution, list)
    curr_forward_operator.solution = ExtendedFEMTT(curr_forward_operator.solution, basis=basis_list, fs=curr_fs)
    curr_solution = curr_forward_operator.solution
    # region sample solution

    for lia in range(1):
        y = np.random.rand(config.n_coefficients) * 2 - 1
        sol, coef, rhs = curr_forward_operator.solve_for_sample(y)
        sol_norm = norm(sol)
        sol_approx = curr_solution.sample(y, project_result=True)
        true_coef = Function(curr_fs)
        true_coef.vector()[:] = np.ascontiguousarray(
            affine_field.__call__(curr_fs.tabulate_dof_coordinates().reshape(-1, 2), list(y)))

        coef_diff = Function(curr_fs)
        coef_diff.vector()[:] = np.ascontiguousarray(true_coef.vector()[:] - interpolate(coef, curr_fs).vector()[:])

        true_rhs = interpolate(Constant(1.0), curr_fs)
        diff_rhs = Function(curr_fs)
        diff_rhs.vector()[:] = np.ascontiguousarray(true_rhs.vector()[:] - interpolate(rhs, curr_fs).vector()[:])

        diff_sol = Function(curr_fs)
        diff_sol.vector()[:] = np.abs(
            np.ascontiguousarray(sol.vector()[:] - interpolate(sol_approx, curr_fs).vector()[:]) * sol_norm ** (-1))

        # xe_sol_fun = xe_sol.sample(y, project_result=True)
        # diff_sol_xe = Function(curr_fs)
        # diff_sol_xe.vector()[:] = np.abs(np.ascontiguousarray(sol.vector()[:] - interpolate(xe_sol_fun, curr_fs).vector()[:]) * sol_norm**(-1))

        plt.figure()

        plt.subplot(3, 3, 1)
        plot_mpl(true_coef)
        plt.title("True coef")
        plt.colorbar()

        plt.subplot(3, 3, 2)
        plot_mpl(coef)
        plt.title("TT coef")
        plt.colorbar()

        plt.subplot(3, 3, 3)
        plot_mpl(coef_diff)
        plt.title("coef diff")
        plt.colorbar()

        plt.subplot(3, 3, 4)
        plot_mpl(true_rhs)
        plt.title("True rhs")
        plt.colorbar()

        plt.subplot(3, 3, 5)
        plot_mpl(rhs)
        plt.title("TT rhs")
        plt.colorbar()

        plt.subplot(3, 3, 6)
        plot_mpl(diff_rhs)
        plt.title("diff rhs")
        plt.colorbar()

        plt.subplot(3, 3, 7)
        plot_mpl(sol)
        plt.title("True sol")
        plt.colorbar()

        plt.subplot(3, 3, 8)
        plot_mpl(diff_sol)
        plt.title("TT sol diff")
        plt.colorbar()

        plt.subplot(3, 3, 9)
        plot_mpl(mesh)
        plt.title("curr mesh")
        # plt.colorbar()

        plt.tight_layout()
        plt.show()
    # endregion

    # tt_sol = curr_forward_operator.solution

    # with TicToc(key="xerus solver", active=True, do_print=False):
    #     curr_forward_operator.solve(curr_rank, config.preconditioner, config.iterations, config.als_tol,
    #                                 curr_solution, solver="XerusALS",
    #                                 solver_options={"method": "ALS", "symmetric": True})
    # xe_sol = curr_forward_operator.solution
    # print("xerus solution: {}".format(xe_sol))
    # print("TTSparseALS solution: {}".format(xe_sol))
    # TicToc.sortedTimes()
    # endregion
    # region refine forward solution
    loc_mesh_once = mesh_once if step < 1 else False
    loc_sto_once = sto_once if step < 1 else False
    refined_solution = curr_forward_operator.refine_adaptive(config.theta_x, config.theta_y,
                                                             config.resnorm_zeta_weight,
                                                             config.eta_zeta_weight,
                                                             config.rank_always, config.sol_rank,
                                                             config.start_rank, config.new_hdeg,
                                                             )
    curr_solution = refined_solution["new_solution"].components
    semi_discrete_coef = refined_solution["new_coef"]
    curr_rhs = refined_solution["new_rhs"]

    mesh = curr_rhs.first_comp_fs.mesh()

    # region uniform mesh refinement
    if refine_mesh_uniform is True:
        from dolfin import interpolate

        print("     refine mesh uniform")
        mesh = refine(mesh)
        CG = FunctionSpace(mesh, 'CG', config.femdegree)
        # cores = V.to_list(V)
        F = Function(curr_fs)
        cores = curr_solution
        print(cores)
        r = cores[0].shape[2]
        newcore = np.zeros([1, CG.dim(), r])
        for d in range(r):
            # print "++++++++", CG.dim(), cores[0][0, :, d].shape
            F.vector().set_local(cores[0][0, :, d])
            # print "refinement", d, F.vector().array().shape,CG.dim()
            newF = interpolate(F, CG)
            newcore[0, :, d] = newF.vector().array()
        # update U
        cores[0] = newcore
        curr_solution = cores
    # endregion

    curr_dofs = tt_dofs(tt.vector.from_list(curr_solution))

    TicToc.sortedTimes()
    print("refined: {}".format(refined_solution["refined"]))
    print("eta_global: {}".format(refined_solution["det_estimator"]["eta_global"]))
    print("resnorm: {}".format(refined_solution["res_estimator"]["resnorm"]))
    print("curr tt-dofs: {}".format(curr_dofs))
    TicToc.clear()
    if curr_dofs >= config.max_dofs:
        print("maximal dofs reached -> exit()")
        exit()
    if mesh_once and step >= 1:
        print("one mesh refinement done -> exit()")
        exit()
    if sto_once and step >= 1:
        print("one stochastic refinement done -> exit()")
        exit()
    # endregion
# endregion
