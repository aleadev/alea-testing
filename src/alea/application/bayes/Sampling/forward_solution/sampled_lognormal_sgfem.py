"""
test script to sample the forward solution of the asgfem using the lognormal tensor train description
@author: Manuel Marschall
"""

# region imports
from __future__ import division

import argparse                                     # use command parameter parsing
from dolfin import FunctionSpace, refine, Function, File, errornorm, norm, UnitSquareMesh, set_log_level, ERROR, \
    project
import matplotlib.pyplot as plt
import matplotlib2tikz as tikz
import tt
import numpy as np
from alea.utils.plothelper import plot_mpl
import os
import errno
from dolfin import *

from alea.application.bayes.Bayes_util.ForwardOperator.tt_lognormal_asgfem_operator import TTLognormalAsgfemOperator
from alea.application.bayes.Bayes_util.ForwardOperator.tt_lognormal_asgfem_operator_data import  \
    TTLognormalAsgfemOperatorData
from alea.application.bayes.alea_util.coefficient_field.tensor.tt_lognormal_fullydiscrete_field import \
    TTLognormalFullyDiscreteField
from alea.application.bayes.alea_util.coefficient_field.tensor.tt_lognormal_semidiscrete_field import \
    read_coef_from_config
from alea.application.bayes.Bayes_util.ForwardOperator.tt_parallel_worker import TTParallelWorker
from alea.application.bayes.Bayes_util.ForwardOperator.tt_forward_operator_util import compute_overall_error_indicator
from alea.application.bayes.alea_util.configuration.asgfem_config import AsgfemConfig
from alea.application.egsz.sample_domains import SampleDomain
from alea.application.egsz.sample_problems2 import SampleProblem

from alea.application.tt_asgfem.tensorsolver.tt_sgfem_util import tt_dofs
from alea.utils.tictoc import TicToc
from alea.utils.timing import get_current_time_string

from alea.application.bayes.Bayes_util.ForwardOperator.tt_forward_operator_util import calculate_stiffness_matrix, \
        calculate_mass_matrix

from copy import deepcopy

set_log_level(ERROR)
# endregion

# region parser arguments
parser = argparse.ArgumentParser(description="estimate the MC convergence against the TT-mean")
parser.add_argument("-f", "--file", type=str, default="default.pro",
                    help="problem file to load")
args = parser.parse_args()
problem_file = "../../alea_util/configuration/problems/" + args.file
# endregion

# region pre-computations and object creation
# region Read configuration from problem file
with TicToc(sec_key="Configuration", key="read configuration", do_print=False, active=True):
    config = AsgfemConfig(problem_file)
    if config.read_configuration() is False:
        exit()

# endregion

# region read coefficient from configuration
with TicToc(sec_key="Semi-Disc Coef", key="read from configuration", do_print=False, active=True):
    semi_discrete_coef = read_coef_from_config(config)
# endregion

# region create initial fs
with TicToc(sec_key="initial function space", key="create", do_print=False, active=True):
    if config.init_mesh_dofs > 0:  # mesh dofs are defined
        mesh_refine = 0  # there are no mesh refinements
    else:
        mesh_refine = config.mesh_refine  # use configured mesh refinements

    mesh, boundaries, dim = SampleDomain.setupDomain(config.domain, initial_mesh_N=2)
    #                                               # refine mesh according to input
    mesh = SampleProblem.setupMesh(mesh, num_refine=mesh_refine)
    #                                               # refinement if you want to have a specific amount of dofs

    print("soll mesh_dofs={} haben dofs={}".format(config.init_mesh_dofs,
                                                   FunctionSpace(mesh, 'CG', config.femdegree).dim()))
    while config.init_mesh_dofs > 0 and FunctionSpace(mesh, 'CG', config.femdegree).dim() < config.init_mesh_dofs:
        mesh = refine(mesh)  # uniformly refine mesh
    pde, _, _, _, _, _ = SampleProblem.setupPDE(2, config.domain, 0, boundaries, semi_discrete_coef)
# endregion
# endregion

refines = 3                                         # number of refinements
y = np.random.randn(config.num_coeff_in_coeff)      # create sample
# region prepare lists
refine_list = []
tt_dofs = []
err_l2_list = []
err_h1_list = []
err_l2_rel_list = []
err_h1_rel_list = []
# endregion

# region create reference mesh
ref_mesh = refine(mesh)
for _ in range(refines + 1):
    ref_mesh = refine(ref_mesh)                     # uniformly refine mesh
ref_fs = FunctionSpace(ref_mesh, 'CG', 3)
# endregion
# region create reference solution
print("create reference discrete coefficient: {}".format(ref_fs))
discrete_coef = TTLognormalFullyDiscreteField(semi_discrete_coef, ref_fs, config.domain)
curr_dofs = np.reshape(ref_fs.tabulate_dof_coordinates(), (-1, 2))
coef_fun = Function(ref_fs)
coef_fun.vector()[:] = discrete_coef(curr_dofs, y)

def boundary(x):
    # return near(x[0], 0, DOLFIN_EPS)
    return near(x[0], 0, DOLFIN_EPS) or near(x[1], 0, DOLFIN_EPS) \
           or near(x[0], 1, DOLFIN_EPS) or near(x[1], 1, DOLFIN_EPS)

u0 = Constant(0.0)
bc = DirichletBC(ref_fs, u0, boundary)

# define variational problem

u = TrialFunction(ref_fs)
v = TestFunction(ref_fs)
f = Constant(1)
# f = Expression("10*exp(-(pow(x[0] - 0.5, 2) + pow(x[1] - 0.5, 2)) / 0.02)", degree=10)
L = f * v * dx
a = coef_fun * inner(grad(u), grad(v)) * dx
u_ref = Function(ref_fs)
print("solve for reference solution on: {}".format(ref_fs))
solve(a == L, u_ref, bc)

# endregion
for lia in range(refines+1):
    refine_list.append(lia)
    print("step {}/ {}".format(lia+1, refines))
    curr_fs = FunctionSpace(mesh, 'CG', config.femdegree)
    curr_discrete_coef = TTLognormalFullyDiscreteField(semi_discrete_coef, curr_fs, config.domain)
    curr_dofs = np.reshape(curr_fs.tabulate_dof_coordinates(), (-1, 2))
    curr_coef_fun = Function(curr_fs)
    curr_coef_fun.vector()[:] = curr_discrete_coef(curr_dofs, y)
    # curr_coef_fun = interpolate(coef_fun, curr_fs)

    bc = DirichletBC(curr_fs, u0, boundary)

    # define variational problem
    u = TrialFunction(curr_fs)
    v = TestFunction(curr_fs)
    f = Constant(1)
    # f = Expression("10*exp(-(pow(x[0] - 0.5, 2) + pow(x[1] - 0.5, 2)) / 0.02)", degree=10)
    L = f * v * dx
    a = curr_coef_fun * inner(grad(u), grad(v)) * dx
    uy = Function(curr_fs)
    print("solve for current solution on: {}".format(curr_fs))
    solve(a == L, uy, bc)
    print("compute errors")
    tt_dofs.append(curr_fs.dim())
    err_l2_list.append(errornorm(u_ref, uy, 'l2'))
    err_h1_list.append(errornorm(u_ref, uy, 'H1'))
    err_l2_rel_list.append(errornorm(u_ref, uy, 'l2') / norm(u_ref, 'l2'))
    err_h1_rel_list.append(errornorm(u_ref, uy, 'H1') / norm(u_ref, 'H1'))
    mesh = refine(mesh)

# region create directory
file_ad = "thx{}_".format(config.theta_x) + \
          "thy{}_".format(config.theta_y) + \
          "adaptive_P{}_".format(config.femdegree) + \
          "dom{}_".format(config.domain) + \
          "scale{}_".format(config.scale) + \
          "ezw{}_".format(config.eta_zeta_weight) + \
          "rzw{}_".format(config.resnorm_zeta_weight)
directory_name = get_current_time_string() + "sample/"

try:
    os.makedirs("lognormal_sgfem_samples/" + directory_name)
except OSError as e:
    if e.errno != errno.EEXIST:
        raise OSError("can not create ranks directory")
# endregion
# region write sample result
with open("lognormal_sgfem_samples/" + directory_name + file_ad + "sol_error.dat", "w") as f:
    f.write("refine, tt_dof, L2, H1, L2_rel, H1_rel, resnorm, eta, zeta, overall \n")
    for lic in range(len(refine_list)):
        f.write(repr(refine_list[lic]) + "," +
                repr(tt_dofs[lic]) + "," +
                repr(err_l2_list[lic]) + "," +
                repr(err_h1_list[lic]) + "," +
                repr(err_l2_rel_list[lic]) + "," +
                repr(err_h1_rel_list[lic]) + "\n"
                )
# endregion
