"""
test script to sample the forward solution of the asgfem using the lognormal tensor train description
@author: Manuel Marschall
"""

# region imports
from __future__ import division

import argparse                                     # use command parameter parsing
from dolfin import FunctionSpace, refine, Function, File, errornorm, norm, UnitSquareMesh, set_log_level, ERROR, \
    project, cells, Mesh

import matplotlib.tri as tri
import matplotlib.pyplot as plt
import matplotlib2tikz as tikz
import tt
import numpy as np
import scipy.sparse as sps
from alea.utils.plothelper import plot_mpl
import os
import shutil
import errno
from alea.application.bayes.Bayes_util.ForwardOperator.tt_lognormal_asgfem_operator import TTLognormalAsgfemOperator
from alea.application.bayes.Bayes_util.ForwardOperator.tt_lognormal_asgfem_operator_data import  \
    TTLognormalAsgfemOperatorData
from alea.application.bayes.alea_util.coefficient_field.tensor.tt_lognormal_fullydiscrete_field import \
    TTLognormalFullyDiscreteField
from alea.application.bayes.alea_util.coefficient_field.tensor.tt_lognormal_semidiscrete_field import \
    read_coef_from_config
from alea.application.bayes.alea_util.coefficient_field.tensor.tt_lognormal_semidiscrete_field import \
    TTLognormalSemidiscreteField
from alea.application.bayes.Bayes_util.ForwardOperator.tt_parallel_worker import TTParallelWorker
from alea.application.bayes.Bayes_util.ForwardOperator.tt_forward_operator_util import compute_overall_error_indicator
from alea.application.bayes.alea_util.configuration.asgfem_config import AsgfemConfig
from alea.application.egsz.sample_domains import SampleDomain
from alea.application.egsz.sample_problems2 import SampleProblem

from alea.application.tt_asgfem.tensorsolver.tt_sgfem_util import tt_dofs
from alea.utils.tictoc import TicToc
from alea.utils.timing import get_current_time_string
from alea.utils.plothelper import write_2d_png, write_3d_png, write_pvd_file
from alea.application.bayes.Bayes_util.ForwardOperator.tt_forward_operator_util import calculate_stiffness_matrix, \
        calculate_mass_matrix, create_order4_stiffness, create_order4_volume_operator_list

from copy import deepcopy

set_log_level(ERROR)
# endregion

# region parser arguments
parser = argparse.ArgumentParser(description="estimate the MC convergence against the TT-mean")
parser.add_argument("-f", "--file", type=str, default="default.pro",
                    help="problem file to load")
parser.add_argument("-sf", "--solutionFile", type=str, default="",
                    help="file containing hashes of the collected solutions to load")
parser.add_argument("-nsf", "--newSolutionFile", type=str, default="",
                    help="new file containing hashes of the collected solutions to load, starting at given value")
parser.add_argument("-ind", "--newSolutionFileIndex", type=int, default=0,
                    help="new file containing hashes of the collected solutions to load, starting at given value")
parser.add_argument("-bound", "--bound_sol", type=int, default=-1,
                    help="integer value to bound the maximal polynomial degree in the solution")
parser.add_argument("-sample", "--sample", action="store_true",
                    help="switch to deactivate the computation. Just sample from the given list")
parser.add_argument("-mesh_uniform", "--mesh_uniform", action="store_true",
                    help="switch to deactivate the adaptivity in the physical mesh")
parser.add_argument("-sto_uniform", "--sto_uniform", action="store_true",
                    help="switch to deactivate the adaptivity in the stochastic space")
parser.add_argument("-new", "--new_solution_file", action="store_true",
                    help="switch to overwrite the existing solution file")
parser.add_argument("-goo", "--go_on", action="store_true",
                    help="switch to use the last existing found file as input for the next step")
parser.add_argument("-ns", "--no_save", action="store_true",
                    help="switch to not store new results in the solution File provided (debug or simple testing)")
parser.add_argument("-mesh_once", "--mesh_once", action="store_true",
                    help="switch to refine the mesh only and make only one step")
parser.add_argument("-sto_once", "--sto_once", action="store_true",
                    help="switch to refine the stochastic only and make only one step")

args = parser.parse_args()
problem_file = "../../alea_util/configuration/problems/" + args.file
if args.solutionFile == "":
    cache_file = "../../Bayes_util/ForwardOperator/tmp/" + get_current_time_string() + "solutions.txt"
else:
    cache_file = "../../Bayes_util/ForwardOperator/tmp/" + args.solutionFile
sample_only = args.sample
overwrite = args.new_solution_file
refine_mesh_uniform = args.mesh_uniform
refine_sto_uniform = args.sto_uniform
go_on = args.go_on
no_save = args.no_save
new_solution_file = args.newSolutionFile
new_solution_file_index = args.newSolutionFileIndex
mesh_once = args.mesh_once
sto_once = args.sto_once
bound_sol = args.bound_sol
create_new_solution_file = False
if new_solution_file != "":
    assert(new_solution_file_index is not None)
    create_new_solution_file = True
    new_cache_file = "../../Bayes_util/ForwardOperator/tmp/" + new_solution_file
# endregion

# region pre-computations and object creation
# region Read configuration from problem file
with TicToc(sec_key="Configuration", key="read configuration", do_print=False, active=True):
    config = AsgfemConfig(problem_file)
    if config.read_configuration() is False:
        exit()

    if refine_mesh_uniform is True:
        config.theta_x = 0
# endregion

# region read coefficient from configuration
with TicToc(sec_key="Semi-Disc Coef", key="read from configuration", do_print=False, active=True):
    semi_discrete_coef = read_coef_from_config(config)
    # print("compute 100 infty norms")
    # norms = semi_discrete_coef.affine_field.get_infty_norm(100)

    # print("first 10 norms: {}".format(norms[0:9]))
    # print("max of norms={}".format(np.max(norms)))
    # print("sum of norms={}".format(np.sum(norms)))
    # for lia in range(13):
    #     norms = semi_discrete_coef.affine_field.get_infty_norm(2**lia)
    #     print(" sum of first {} norms: {}".format(2**lia, np.sum(norms)))
    # exit()
# if True:
#     y_list = [semi_discrete_coef.sample_rvs(2) for _ in range(10)]
#     mesh_coef = UnitSquareMesh(20, 20)
#     fs_coef = FunctionSpace(mesh_coef, 'CG', 3)
#     func_coef_cont = Function(fs_coef)
#     func_coef_analytic = Function(fs_coef)
#     func_coef_discrete = Function(fs_coef)
#     discrete_coef = TTLognormalFullyDiscreteField(semi_discrete_coef, fs_coef)
#     dofs = fs_coef.tabulate_dof_coordinates()
#     dofs = dofs.reshape(-1, 2)
#     for y in y_list:
#         func_coef_cont.vector()[:] = semi_discrete_coef(dofs, y)
#         func_coef_analytic.vector()[:] = semi_discrete_coef.sample_continuous_field(dofs, y)
#         func_coef_discrete.vector()[:] = discrete_coef(dofs, y)
#         print("error cont-semi: {}".format(errornorm(func_coef_analytic, func_coef_cont)/norm(func_coef_analytic)))
#         print("error cont-discrete: {}".format(errornorm(func_coef_analytic, func_coef_discrete) /
# norm(func_coef_analytic)))
#         print("error semi-discrete: {}".format(errornorm(func_coef_cont, func_coef_discrete) / norm(func_coef_cont)))
# endregion

# region try to read solution data from file
try:
    read_solutions = []
    from os.path import isfile
    if not isfile(cache_file) or overwrite is True:
        with open(cache_file, 'wb') as f:
            print("create solution cache file list: {}".format(cache_file))
        if create_new_solution_file:
            with open(new_cache_file, "wb") as f:
                print("create new solution cache file list: {}".format(new_cache_file))
    f = open(cache_file, 'rb')
    lines = f.readlines()
    f.close()
    if create_new_solution_file:
        assert (len(lines) >= new_solution_file_index)
        lines = lines[:new_solution_file_index+1]
    for line in lines:
        sol = TTLognormalAsgfemOperatorData()
        line = line.strip()
        print("try to read file: {}".format(line))
        if len(line) <= 1:
            continue
        if sol.load(line) is False:
            print("can not read file")
            read_solution = None
            break
        if create_new_solution_file:
            f2 = open(new_cache_file, 'a')
            f2.write("\n" + line)
            f2.close()
        print("found solution: ")
        print("     hedgs: {}".format(sol.hdegs))
        print("     ranks: {}".format(sol.ranks))
        print("     m-dofs: {}".format(sol.m_dofs))
        read_solutions.append(sol)

except Exception as ex:
    print(ex)
    read_solutions = None
    exit()
# endregion

# region construct initial ranks and hermite degrees
if go_on is True and read_solutions is not None and len(read_solutions) > 0:
    curr_rank = read_solutions[-1].ranks
    curr_hdeg = read_solutions[-1].hdegs
    curr_solution = read_solutions[-1].solution
    if len(curr_solution) > len(curr_hdeg)+1:
        curr_solution = read_solutions[-1].solution
else:
    curr_rank = [1] + [config.start_rank]*config.n_coefficients + [1]
    if isinstance(config.gpc_degree, list):
        curr_hdeg = config.gpc_degree
    else:
        curr_hdeg = [config.hermite_degree]*config.n_coefficients
    curr_solution = []

# endregion
# region create initial fs
with TicToc(sec_key="initial function space", key="create", do_print=False, active=True):
    if config.init_mesh_dofs > 0:  # mesh dofs are defined
        mesh_refine = 0  # there are no mesh refinements
    else:
        mesh_refine = config.mesh_refine  # use configured mesh refinements

    mesh, boundaries, dim = SampleDomain.setupDomain(config.domain, initial_mesh_N=2)
    #                                               # refine mesh according to input
    mesh = SampleProblem.setupMesh(mesh, num_refine=mesh_refine)
    #                                               # refinement if you want to have a specific amount of dofs

    print("soll mesh_dofs={} haben dofs={}".format(config.init_mesh_dofs,
                                                   FunctionSpace(mesh, 'CG', config.femdegree).dim()))
    while config.init_mesh_dofs > 0 and FunctionSpace(mesh, 'CG', config.femdegree).dim() < config.init_mesh_dofs:
        mesh = refine(mesh)  # uniformly refine mesh
    pde, _, _, _, _, _ = SampleProblem.setupPDE(2, config.domain, 0, boundaries, semi_discrete_coef)

if go_on is True and read_solutions is not None and len(read_solutions) > 0:
    mesh = read_solutions[-1].new_mesh
# endregion

# region check coef dimension
semi_discrete_coef.update_coefficient(curr_hdeg, config.max_rank, add_order=4, new_dimension=2, add_dim=2,
                                      max_hdeg=config.max_hdegs_coef)
# endregion

# endregion

# region sample only region
if sample_only is True:
    if len(read_solutions) <= 0:
        print("nothing to sample.")
        exit()
    # region prepare function space and sampling operator
    mesh = read_solutions[-1].mesh                  # take the last mesh of the refinement process
    mesh = refine(mesh)                             # refine it once more
    print("refine mesh to  {} dofs".format(FunctionSpace(mesh, 'CG', 3).dim()))
    while FunctionSpace(mesh, 'CG', 3).dim() < 10000:
        mesh = refine(mesh)                         # uniformly refine mesh again if not enough dofs
        print("refine mesh to  {} dofs".format(FunctionSpace(mesh, 'CG', 3).dim()))
    dummy_fs = FunctionSpace(mesh, 'CG', 3)
    print(" reference fs: {}".format(dummy_fs))
    #                                               # create an forward operator just to sample the true solution
    dummy_operator = TTLognormalAsgfemOperator(semi_discrete_coef, dummy_fs, curr_hdeg, config.domain, empty=True)
    # endregion
    reference_m = config.reference_M
    expression_degree = config.reference_expression_degree
    n_samples = config.n_samples
    # region prepare samples
    y_list = [semi_discrete_coef.sample_rvs(reference_m) for _ in range(n_samples)]
    # endregion
    # region prepare lists
    refine_list = []
    tt_dofs = []
    tt_op_dofs = []
    tt_op_ten_dofs = []
    m_dofs = []
    err_l2_list = []
    err_h1_list = []
    err_l2_rel_list = []
    err_h1_rel_list = []
    err_l2_semi_list = []
    err_h1_semi_list = []
    err_l2_semi_rel_list = []
    err_h1_semi_rel_list = []
    err_l2_disc_list = []
    err_h1_disc_list = []
    err_l2_disc_rel_list = []
    err_h1_disc_rel_list = []

    err_rms_l2_list = []
    err_rms_h1_list = []
    err_rms_l2_rel_list = []
    err_rms_h1_rel_list = []
    err_rms_l2_semi_list = []
    err_rms_h1_semi_list = []
    err_rms_l2_semi_rel_list = []
    err_rms_h1_semi_rel_list = []
    err_rms_l2_disc_list = []
    err_rms_h1_disc_list = []
    err_rms_l2_disc_rel_list = []
    err_rms_h1_disc_rel_list = []

    overall_error_list = []
    zeta_list = []
    resnorm_list = []
    overall_error_list = []
    dim_list = []
    rank_list = []
    mesh_list = []
    coef_cont_list = []
    coef_semicont_list = []
    coef_discrete_list = []
    coef_cont_semi_err_list = []
    coef_cont_disc_err_list = []
    coef_semi_disc_err_list = []
    coef_cont_semi_rel_err_list = []
    coef_cont_disc_rel_err_list = []
    coef_semi_disc_rel_err_list = []
    coef_cont_semi_err_H1_list = []
    coef_cont_disc_err_H1_list = []
    coef_semi_disc_err_H1_list = []
    coef_cont_semi_rel_err_H1_list = []
    coef_cont_disc_rel_err_H1_list = []
    coef_semi_disc_rel_err_H1_list = []
    coef_cont_semi_err_L2_list = []
    coef_cont_disc_err_L2_list = []
    coef_semi_disc_err_L2_list = []
    coef_cont_semi_rel_err_L2_list = []
    coef_cont_disc_rel_err_L2_list = []
    coef_semi_disc_rel_err_L2_list = []
    als_iter_list = []
    als_first_comp_err_list = []
    als_sweep_err_list = []
    eta_list = []
    sampled_eta_list = []
    rhs_list = []
    sampled_rhs_list = []
    sq_list = []
    sampled_sq_list = []
    zero_list = []
    sampled_zero_list = []
    jump_list = []
    sampled_jump_list = []
    eta_list_local = []
    sampled_eta_list_local = []
    rhs_list_local = []
    sampled_rhs_list_local = []
    sq_list_local = []
    sampled_sq_list_local = []
    zero_list_local = []
    sampled_zero_list_local = []
    jump_list_local = []
    sampled_jump_list_local = []
    coef_hdeg_list = []
    coef_rank_list = []
    coef_m_dof_list = []
    coef_sample_path_list = []
    # endregion
    # region compute mass and stiffness matrix
    mass = calculate_mass_matrix(dummy_fs)
    stiff = calculate_stiffness_matrix(dummy_fs)
    # endregion
    print("start sampling")
    for lia, sol in enumerate(read_solutions):
        assert (isinstance(sol, TTLognormalAsgfemOperatorData))
        # region Sample different coefficient discretisation
        # region create current semi discrete coefficient
        curr_semi_disc_field = TTLognormalSemidiscreteField(semi_discrete_coef.affine_field,
                                                            sol.cont_coef_hermite_degree, sol.cont_coef_ranks,
                                                            semi_discrete_coef.affine_field.get_infty_norm(
                                                                len(sol.cont_coef_hermite_degree)),
                                                            mesh_dofs=config.coef_mesh_dofs,
                                                            quad_degree=config.coef_quad_degree,
                                                            theta=config.theta, rho=config.rho,
                                                            acc=config.coef_acc, scale=config.scale,
                                                            domain=config.domain
                                                            )
        # endregion
        # region create current function space
        curr_fs = FunctionSpace(sol.mesh, 'CG', config.femdegree)
        curr_dofs = np.reshape(curr_fs.tabulate_dof_coordinates(), (-1, curr_fs.mesh().geometry().dim()))
        curr_mass = calculate_mass_matrix(curr_fs)
        curr_stiff = calculate_stiffness_matrix(curr_fs)
        # endregion
        # region create current discrete coefficient
        curr_discrete_coef = TTLognormalFullyDiscreteField(curr_semi_disc_field, curr_fs, config.domain)
        # endregion

        loc_coef_cont_list = []
        loc_coef_semicont_list = []
        loc_coef_discrete_list = []

        for y in y_list:
            loc_coef_cont_list.append(semi_discrete_coef.sample_continuous_field(curr_dofs, y))
            loc_coef_semicont_list.append(curr_semi_disc_field(curr_dofs, y))
            loc_coef_discrete_list.append(curr_discrete_coef(curr_dofs, y))

        coef_mean_cont = np.sum(loc_coef_cont_list, axis=0) * n_samples ** (-1)
        coef_mean_semi = np.sum(loc_coef_semicont_list, axis=0) * n_samples ** (-1)
        coef_mean_disc = np.sum(loc_coef_discrete_list, axis=0) * n_samples ** (-1)

        coef_var_cont = np.sum([(loc_coef_cont_list[lic] - coef_mean_cont) ** 2 for lic in range(n_samples)],
                               axis=0) * (n_samples - 1) ** (-1)
        coef_var_semi = np.sum([(loc_coef_semicont_list[lic] - coef_mean_semi) ** 2 for lic in range(n_samples)],
                               axis=0) * (n_samples - 1) ** (-1)
        coef_var_disc = np.sum([(loc_coef_discrete_list[lic] - coef_mean_disc) ** 2 for lic in range(n_samples)],
                               axis=0) * (n_samples - 1) ** (-1)

        diff_cont_semi = [loc_coef_cont_list[lic] - loc_coef_semicont_list[lic] for lic in range(n_samples)]
        diff_cont_disc = [loc_coef_cont_list[lic] - loc_coef_discrete_list[lic] for lic in range(n_samples)]
        diff_semi_disc = [loc_coef_semicont_list[lic] - loc_coef_discrete_list[lic] for lic in range(n_samples)]

        # region continuous to semi discrete coefficient field error
        coef_cont_semi_err_list.append(np.sum([np.linalg.norm(diff_cont_semi[lic], ord=2)
                                               for lic in range(n_samples)])*n_samples**(-1))

        coef_cont_semi_err_L2_list.append(np.sum([np.dot(diff_cont_semi[lic], curr_mass.dot(diff_cont_semi[lic]))
                                                  for lic in range(n_samples)]) * n_samples ** (-1))

        coef_cont_semi_err_H1_list.append(np.sum([np.dot(diff_cont_semi[lic], curr_stiff.dot(diff_cont_semi[lic]))
                                                  for lic in range(n_samples)]) * n_samples ** (-1))

        coef_cont_semi_rel_err_list.append(np.sum([np.linalg.norm(diff_cont_semi[lic], ord=2) *
                                                   np.linalg.norm(loc_coef_cont_list[lic], ord=2) ** (-1)
                                                   for lic in range(n_samples)]) * n_samples ** (-1))

        coef_cont_semi_rel_err_L2_list.append(np.sum([np.dot(diff_cont_semi[lic], curr_mass.dot(diff_cont_semi[lic])) *
                                                      np.dot(loc_coef_cont_list[lic],
                                                             curr_mass.dot(loc_coef_cont_list[lic])) ** (-1)
                                                      for lic in range(n_samples)]) * n_samples ** (-1))

        coef_cont_semi_rel_err_H1_list.append(np.sum([np.dot(diff_cont_semi[lic], curr_stiff.dot(diff_cont_semi[lic])) *
                                                      np.dot(loc_coef_cont_list[lic],
                                                             curr_stiff.dot(loc_coef_cont_list[lic])) ** (-1)
                                                      for lic in range(n_samples)]) * n_samples ** (-1))
        # endregion
        # region continuous to fully discrete coefficient field error
        coef_cont_disc_err_list.append(np.sum([np.linalg.norm(diff_cont_disc[lic], ord=2)
                                               for lic in range(n_samples)]) * n_samples ** (-1))

        coef_cont_disc_err_L2_list.append(np.sum([np.dot(diff_cont_disc[lic], curr_mass.dot(diff_cont_disc[lic]))
                                                  for lic in range(n_samples)]) * n_samples ** (-1))

        coef_cont_disc_err_H1_list.append(np.sum([np.dot(diff_cont_disc[lic], curr_stiff.dot(diff_cont_disc[lic]))
                                                  for lic in range(n_samples)]) * n_samples ** (-1))

        coef_cont_disc_rel_err_list.append(np.sum([np.linalg.norm(diff_cont_disc[lic], ord=2) *
                                                   np.linalg.norm(loc_coef_cont_list[lic], ord=2) ** (-1)
                                                   for lic in range(n_samples)]) * n_samples ** (-1))

        coef_cont_disc_rel_err_L2_list.append(np.sum([np.dot(diff_cont_disc[lic], curr_mass.dot(diff_cont_disc[lic])) *
                                                      np.dot(loc_coef_cont_list[lic],
                                                             curr_mass.dot(loc_coef_cont_list[lic])) ** (-1)
                                                      for lic in range(n_samples)]) * n_samples ** (-1))

        coef_cont_disc_rel_err_H1_list.append(np.sum([np.dot(diff_cont_disc[lic], curr_stiff.dot(diff_cont_disc[lic])) *
                                                      np.dot(loc_coef_cont_list[lic],
                                                             curr_stiff.dot(loc_coef_cont_list[lic])) ** (-1)
                                                      for lic in range(n_samples)]) * n_samples ** (-1))
        # endregion
        # region semi discrete to fully discrete coefficient field error
        coef_semi_disc_err_list.append(np.sum([np.linalg.norm(diff_semi_disc[lic], ord=2)
                                               for lic in range(n_samples)]) * n_samples ** (-1))

        coef_semi_disc_err_L2_list.append(np.sum([np.dot(diff_semi_disc[lic], curr_mass.dot(diff_semi_disc[lic]))
                                                  for lic in range(n_samples)]) * n_samples ** (-1))

        coef_semi_disc_err_H1_list.append(np.sum([np.dot(diff_semi_disc[lic], curr_stiff.dot(diff_semi_disc[lic]))
                                                  for lic in range(n_samples)]) * n_samples ** (-1))

        coef_semi_disc_rel_err_list.append(np.sum([np.linalg.norm(diff_semi_disc[lic], ord=2) *
                                                   np.linalg.norm(loc_coef_semicont_list[lic], ord=2) ** (-1)
                                                   for lic in range(n_samples)]) * n_samples ** (-1))

        coef_semi_disc_rel_err_L2_list.append(np.sum([np.dot(diff_semi_disc[lic], curr_mass.dot(diff_semi_disc[lic])) *
                                                      np.dot(loc_coef_semicont_list[lic],
                                                             curr_mass.dot(loc_coef_semicont_list[lic])) ** (-1)
                                                      for lic in range(n_samples)]) * n_samples ** (-1))

        coef_semi_disc_rel_err_H1_list.append(np.sum([np.dot(diff_semi_disc[lic], curr_stiff.dot(diff_semi_disc[lic])) *
                                                      np.dot(loc_coef_semicont_list[lic],
                                                             curr_stiff.dot(loc_coef_semicont_list[lic])) ** (-1)
                                                      for lic in range(n_samples)]) * n_samples ** (-1))
        # endregion
        # endregion
        refine_list.append(lia)
        # region init parallel worker
        worker = TTParallelWorker()
        args = {"reference_m": reference_m, "expression_degree": expression_degree, 'femdegree': config.femdegree,
                "mass": mass, "stiff": stiff}
        worker.prepare(dummy_operator, sol, pde, dummy_fs, args, config.nCPU)
        # endregion
        # region start parallel error sampler
        results = worker.work(y_list, method="cont")
        err_l2_results, err_h1_results, err_l2_rel_results, err_h1_rel_results = results
        err_l2_list.append(np.sum(err_l2_results) * n_samples**(-1))
        err_h1_list.append(np.sum(err_h1_results) * n_samples ** (-1))
        err_l2_rel_list.append(np.sum(err_l2_rel_results) * n_samples**(-1))
        err_h1_rel_list.append(np.sum(err_h1_rel_results) * n_samples ** (-1))
        err_rms_l2_list.append(np.sqrt(np.sum([err_l2_results[lib] ** 2
                                               for lib in range(len(err_l2_results))])) * n_samples ** (-1))
        err_rms_h1_list.append(np.sqrt(np.sum([err_h1_results[lib] ** 2
                                               for lib in range(len(err_h1_results))])) * n_samples ** (-1))
        err_rms_l2_rel_list.append(np.sqrt(np.sum([err_l2_rel_results[lib] ** 2
                                                   for lib in range(len(err_l2_rel_results))])) * n_samples ** (-1))
        err_rms_h1_rel_list.append(np.sqrt(np.sum([err_h1_rel_results[lib] ** 2
                                                   for lib in range(len(err_h1_rel_results))])) * n_samples ** (-1))

        results = worker.work(y_list, method="semi")
        err_l2_results, err_h1_results, err_l2_rel_results, err_h1_rel_results = results
        err_l2_semi_list.append(np.sum(err_l2_results) * n_samples**(-1))
        err_h1_semi_list.append(np.sum(err_h1_results) * n_samples ** (-1))
        err_l2_semi_rel_list.append(np.sum(err_l2_rel_results) * n_samples**(-1))
        err_h1_semi_rel_list.append(np.sum(err_h1_rel_results) * n_samples ** (-1))
        err_rms_l2_semi_list.append(np.sqrt(np.sum([err_l2_results[lib] ** 2
                                                    for lib in range(len(err_l2_results))])) * n_samples ** (-1))
        err_rms_h1_semi_list.append(np.sqrt(np.sum([err_h1_results[lib] ** 2
                                                    for lib in range(len(err_h1_results))])) * n_samples ** (-1))
        err_rms_l2_semi_rel_list.append(np.sqrt(np.sum([err_l2_rel_results[lib] ** 2
                                                        for lib in range(len(err_l2_rel_results))])) *
                                        n_samples ** (-1))
        err_rms_h1_semi_rel_list.append(np.sqrt(np.sum([err_h1_rel_results[lib] ** 2
                                                        for lib in range(len(err_h1_rel_results))])) *
                                        n_samples ** (-1))
        
        results = worker.work(y_list, method="disc")
        err_l2_results, err_h1_results, err_l2_rel_results, err_h1_rel_results = results
        err_l2_disc_list.append(np.sum(err_l2_results) * n_samples**(-1))
        err_h1_disc_list.append(np.sum(err_h1_results) * n_samples ** (-1))
        err_l2_disc_rel_list.append(np.sum(err_l2_rel_results) * n_samples**(-1))
        err_h1_disc_rel_list.append(np.sum(err_h1_rel_results) * n_samples ** (-1))
        err_rms_l2_disc_list.append(np.sqrt(np.sum([err_l2_results[lib]**2
                                                    for lib in range(len(err_l2_results))])) * n_samples ** (-1))
        err_rms_h1_disc_list.append(np.sqrt(np.sum([err_h1_results[lib]**2
                                                    for lib in range(len(err_h1_results))])) * n_samples ** (-1))
        err_rms_l2_disc_rel_list.append(np.sqrt(np.sum([err_l2_rel_results[lib]**2
                                                        for lib in range(len(err_l2_rel_results))])) *
                                        n_samples ** (-1))
        err_rms_h1_disc_rel_list.append(np.sqrt(np.sum([err_h1_rel_results[lib]**2
                                                        for lib in range(len(err_h1_rel_results))])) *
                                        n_samples ** (-1))
        # endregion
        # region update solution object and save it back to file
        if sol.sol_sampling_result is None or True:
            sol.sol_sampling_result = dict()
            sol.sol_sampling_result["l2"] = err_l2_list[-1]
            sol.sol_sampling_result["l2_rel"] = err_l2_rel_list[-1]
            sol.sol_sampling_result["h1"] = err_h1_list[-1]
            sol.sol_sampling_result["h1_rel"] = err_h1_rel_list[-1]

            sol.sol_sampling_result["l2_semi"] = err_l2_semi_list[-1]
            sol.sol_sampling_result["l2_semi_rel"] = err_l2_semi_rel_list[-1]
            sol.sol_sampling_result["h1_semi"] = err_h1_semi_list[-1]
            sol.sol_sampling_result["h1_semi_rel"] = err_h1_semi_rel_list[-1]

            sol.sol_sampling_result["l2_disc"] = err_l2_disc_list[-1]
            sol.sol_sampling_result["l2_disc_rel"] = err_l2_disc_rel_list[-1]
            sol.sol_sampling_result["h1_disc"] = err_h1_disc_list[-1]
            sol.sol_sampling_result["h1_disc_rel"] = err_h1_disc_rel_list[-1]

            sol.sol_sampling_result["rms_l2"] = err_rms_l2_list[-1]
            sol.sol_sampling_result["rms_l2_rel"] = err_rms_l2_rel_list[-1]
            sol.sol_sampling_result["rms_h1"] = err_rms_h1_list[-1]
            sol.sol_sampling_result["rms_h1_rel"] = err_rms_h1_rel_list[-1]

            sol.sol_sampling_result["rms_l2_semi"] = err_rms_l2_semi_list[-1]
            sol.sol_sampling_result["rms_l2_semi_rel"] = err_rms_l2_semi_rel_list[-1]
            sol.sol_sampling_result["rms_h1_semi"] = err_rms_h1_semi_list[-1]
            sol.sol_sampling_result["rms_h1_semi_rel"] = err_rms_h1_semi_rel_list[-1]

            sol.sol_sampling_result["rms_l2_disc"] = err_rms_l2_disc_list[-1]
            sol.sol_sampling_result["rms_l2_disc_rel"] = err_rms_l2_disc_rel_list[-1]
            sol.sol_sampling_result["rms_h1_disc"] = err_rms_h1_disc_list[-1]
            sol.sol_sampling_result["rms_h1_disc_rel"] = err_rms_h1_disc_rel_list[-1]

            sol.sol_sampling_result["n_samples"] = len(y_list)
            sol.update_sol_sampling()
        if sol.coef_sampling_result is None or True:
            sol.coef_sampling_result = dict()
            sol.coef_sampling_result["cont_semi"] = coef_cont_semi_err_list[-1]
            sol.coef_sampling_result["cont_disc"] = coef_cont_disc_err_list[-1]
            sol.coef_sampling_result["semi_disc"] = coef_semi_disc_err_list[-1]

            sol.coef_sampling_result["cont_semi_rel"] = coef_cont_semi_rel_err_list[-1]
            sol.coef_sampling_result["cont_disc_rel"] = coef_cont_disc_rel_err_list[-1]
            sol.coef_sampling_result["semi_disc_rel"] = coef_semi_disc_rel_err_list[-1]

            sol.coef_sampling_result["cont_semi_L2"] = coef_cont_semi_err_L2_list[-1]
            sol.coef_sampling_result["cont_disc_L2"] = coef_cont_disc_err_L2_list[-1]
            sol.coef_sampling_result["semi_disc_L2"] = coef_semi_disc_err_L2_list[-1]

            sol.coef_sampling_result["cont_semi_rel_L2"] = coef_cont_semi_rel_err_L2_list[-1]
            sol.coef_sampling_result["cont_disc_rel_L2"] = coef_cont_disc_rel_err_L2_list[-1]
            sol.coef_sampling_result["semi_disc_rel_L2"] = coef_semi_disc_rel_err_L2_list[-1]

            sol.coef_sampling_result["cont_semi_H1"] = coef_cont_semi_err_H1_list[-1]
            sol.coef_sampling_result["cont_disc_H1"] = coef_cont_disc_err_H1_list[-1]
            sol.coef_sampling_result["semi_disc_H1"] = coef_semi_disc_err_H1_list[-1]

            sol.coef_sampling_result["cont_semi_rel_H1"] = coef_cont_semi_rel_err_H1_list[-1]
            sol.coef_sampling_result["cont_disc_rel_H1"] = coef_cont_disc_rel_err_H1_list[-1]
            sol.coef_sampling_result["semi_disc_rel_H1"] = coef_semi_disc_rel_err_H1_list[-1]

            sol.coef_sampling_result["tt_dofs"] = curr_discrete_coef.tt_dofs
            sol.coef_sampling_result["mesh_dofs"] = curr_discrete_coef.dofs

            sol.coef_sampling_result["sample_cont"] = loc_coef_cont_list
            sol.coef_sampling_result["sample_semi"] = loc_coef_semicont_list
            sol.coef_sampling_result["sample_disc"] = loc_coef_discrete_list

            sol.coef_sampling_result["mean_cont"] = coef_mean_cont
            sol.coef_sampling_result["mean_semi"] = coef_mean_semi
            sol.coef_sampling_result["mean_disc"] = coef_mean_disc

            sol.coef_sampling_result["var_cont"] = coef_var_cont
            sol.coef_sampling_result["var_semi"] = coef_var_semi
            sol.coef_sampling_result["var_disc"] = coef_var_disc
            sol.update_coef_sampling()
        # endregion

        # region store results in lists
        tt_dofs.append(sol.dofs)
        tt_op_dofs.append(sol.op_dofs)
        tt_op_ten_dofs.append(sol.op_ten_dofs)
        m_dofs.append(sol.m_dofs)
        eta_list.append(sol.eta_global)
        rhs_list.append(sol.det_estimator_result["rhs_global"])
        sq_list.append(sol.det_estimator_result["sq_global"])
        zero_list.append(sol.det_estimator_result["zero_global"])
        jump_list.append(sol.det_estimator_result["jump_global"])
        zeta_list.append(sol.zeta_global)
        resnorm_list.append(sol.resnorm)
        overall_error_list.append(sol.overall_error)
        dim_list.append(sol.hdegs)
        rank_list.append(sol.ranks)
        mesh_list.append(sol.mesh)
        als_iter_list.append(sol.solver_iterations)
        als_first_comp_err_list.append(sol.solver_local_error_list)
        als_sweep_err_list.append(sol.solver_conv_list)
        sampled_eta_list.append(sol.det_estimator_result["sampled_eta_global"])
        sampled_rhs_list.append(sol.det_estimator_result["sampled_rhs_global"])
        sampled_sq_list.append(sol.det_estimator_result["sampled_sq_global"])
        sampled_zero_list.append(sol.det_estimator_result["sampled_zero_global"])
        sampled_jump_list.append(sol.det_estimator_result["sampled_jump_global"])

        eta_list_local.append(sol.det_estimator_result["eta_local"])
        rhs_list_local.append(sol.det_estimator_result["rhs_local"])
        sq_list_local.append(sol.det_estimator_result["sq_local"])
        zero_list_local.append(sol.det_estimator_result["zero_local"])
        jump_list_local.append(sol.det_estimator_result["jump_local"])
        sampled_eta_list_local.append(sol.det_estimator_result["sampled_eta_local"])
        sampled_rhs_list_local.append(sol.det_estimator_result["sampled_rhs_local"])
        sampled_sq_list_local.append(sol.det_estimator_result["sampled_sq_local"])
        sampled_zero_list_local.append(sol.det_estimator_result["sampled_zero_local"])
        sampled_jump_list_local.append(sol.det_estimator_result["sampled_jump_local"])

        coef_hdeg_list.append(sol.cont_coef_hermite_degree)
        coef_rank_list.append(sol.cont_coef_ranks)
        coef_m_dof_list.append(sol.cont_coef_m_dofs)
        coef_sample_path_list.append(sol.coef_sample_path)

        coef_cont_list.append(loc_coef_cont_list)
        coef_semicont_list.append(loc_coef_semicont_list)
        coef_discrete_list.append(loc_coef_discrete_list)
        # endregion
        print("  sample solution {}/{}: \n".format(lia+1, len(read_solutions))
              + " "*5 + "L2 error={}  L2 rel={}\n".format(err_l2_list[-1], err_l2_rel_list[-1])
              + " "*5 + "H1 error={}  H1 rel={}\n".format(err_h1_list[-1], err_h1_rel_list[-1])
              + " "*5 + "coef cont_semi={}  coef cont_semi rel={} \n".format(coef_cont_semi_err_list[-1],
                                                                             coef_cont_semi_rel_err_list[-1])
              + " "*5 + "coef cont_disc={}  coef cont_disc rel={} \n".format(coef_cont_disc_err_list[-1],
                                                                             coef_cont_disc_rel_err_list[-1])
              + " "*5 + "coef semi_disc={}  coef semi_disc rel={} \n".format(coef_semi_disc_err_list[-1],
                                                                             coef_semi_disc_rel_err_list[-1]))

    # region some assertions
    assert len(refine_list) == len(tt_dofs)
    assert len(refine_list) == len(m_dofs)
    assert len(refine_list) == len(err_l2_list)
    assert len(refine_list) == len(err_h1_list)
    assert len(refine_list) == len(err_l2_rel_list)
    assert len(refine_list) == len(err_h1_rel_list)
    assert len(refine_list) == len(err_l2_semi_list)
    assert len(refine_list) == len(err_h1_semi_list)
    assert len(refine_list) == len(err_l2_semi_rel_list)
    assert len(refine_list) == len(err_h1_semi_rel_list)
    assert len(refine_list) == len(err_l2_disc_list)
    assert len(refine_list) == len(err_h1_disc_list)
    assert len(refine_list) == len(err_l2_disc_rel_list)
    assert len(refine_list) == len(err_h1_disc_rel_list)
    assert len(refine_list) == len(overall_error_list)
    assert len(refine_list) == len(eta_list)
    assert len(refine_list) == len(sampled_eta_list)
    assert len(refine_list) == len(rhs_list)
    assert len(refine_list) == len(sampled_rhs_list)
    assert len(refine_list) == len(zero_list)
    assert len(refine_list) == len(sampled_zero_list)
    assert len(refine_list) == len(sq_list)
    assert len(refine_list) == len(sampled_sq_list)
    assert len(refine_list) == len(jump_list)
    assert len(refine_list) == len(sampled_jump_list)
    assert len(refine_list) == len(zeta_list)
    assert len(refine_list) == len(resnorm_list)
    assert len(refine_list) == len(als_iter_list)
    assert len(refine_list) == len(als_first_comp_err_list)
    assert len(refine_list) == len(als_sweep_err_list)
    assert len(refine_list) == len(mesh_list)
    assert len(refine_list) == len(coef_hdeg_list)
    assert len(refine_list) == len(coef_rank_list)
    assert len(refine_list) == len(coef_m_dof_list)
    assert len(refine_list) == len(coef_sample_path_list)
    assert len(refine_list) == len(coef_cont_list)
    assert len(refine_list) == len(coef_semicont_list)
    assert len(refine_list) == len(coef_discrete_list)
    assert len(refine_list) == len(coef_cont_semi_err_list)
    assert len(refine_list) == len(coef_cont_disc_err_list)
    assert len(refine_list) == len(coef_semi_disc_err_list)
    # endregion

    # region create directory
    file_ad = ""
    if refine_mesh_uniform is False:
        file_ad += "adaptive_"
    else:
        file_ad += "uniform_"

    file_ad += "P{}_".format(config.femdegree) + \
               "dom{}_".format(config.domain)
    directory_name = get_current_time_string() + "sample/"

    try:
        os.makedirs("lognormal_sgfem_samples/" + directory_name)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise OSError("can not create ranks directory")
    # endregion
    # region write sample result
    with open("lognormal_sgfem_samples/" + directory_name + file_ad + "sol_error.dat", "w") as f:
        f.write("refine, tt_dof, tt_op_dof, L2, H1, L2_rel, H1_rel, resnorm, eta, sampled_eta, rhs, sampled_rhs, sq, "
                "sampled_sq, zero, sampled_zero, jump, sampled_jump, zeta, overall, als_iter, m_dofs, L2_semi, H1_semi,"
                " L2_semi_rel, H1_semi_rel, L2_disc, H1_disc, L2_disc_rel, H1_disc_rel, coef_cont_semi, coef_cont_disc,"
                "coef_semi_disc, rms_L2, rms_H1, rms_L2_rel, rms_H1_rel, rms_L2_semi, rms_H1_semi,"
                " rms_L2_semi_rel, rms_H1_semi_rel, rms_L2_disc, rms_H1_disc, rms_L2_disc_rel, rms_H1_disc_rel  \n")
        for lic in range(len(refine_list)):
            f.write(repr(refine_list[lic]) + "," +
                    repr(tt_dofs[lic]) + "," +
                    repr(tt_op_ten_dofs[lic]) + "," +
                    repr(err_l2_list[lic]) + "," +
                    repr(err_h1_list[lic]) + "," +
                    repr(err_l2_rel_list[lic]) + "," +
                    repr(err_h1_rel_list[lic]) + "," +
                    repr(resnorm_list[lic]) + "," +
                    repr(eta_list[lic]) + "," +
                    repr(sampled_eta_list[lic]) + "," +
                    repr(rhs_list[lic]) + "," +
                    repr(sampled_rhs_list[lic]) + "," +
                    repr(sq_list[lic]) + "," +
                    repr(sampled_sq_list[lic]) + "," +
                    repr(zero_list[lic]) + "," +
                    repr(sampled_zero_list[lic]) + "," +
                    repr(jump_list[lic]) + "," +
                    repr(sampled_jump_list[lic]) + "," +
                    repr(zeta_list[lic]) + "," +
                    repr(overall_error_list[lic]) + "," +
                    repr(als_iter_list[lic]) + "," +
                    repr(m_dofs[lic]) + "," +
                    repr(err_l2_semi_list[lic]) + "," +
                    repr(err_h1_semi_list[lic]) + "," +
                    repr(err_l2_semi_rel_list[lic]) + "," +
                    repr(err_h1_semi_rel_list[lic]) + "," +
                    repr(err_l2_disc_list[lic]) + "," +
                    repr(err_h1_disc_list[lic]) + "," +
                    repr(err_l2_disc_rel_list[lic]) + "," +
                    repr(err_h1_disc_rel_list[lic]) + "," +
                    repr(coef_cont_semi_err_list[lic]) + "," +
                    repr(coef_cont_disc_err_list[lic]) + "," +
                    repr(coef_semi_disc_err_list[lic]) + "," +
                    repr(err_rms_l2_list[lic]) + "," +
                    repr(err_rms_h1_list[lic]) + "," +
                    repr(err_rms_l2_rel_list[lic]) + "," +
                    repr(err_rms_h1_rel_list[lic]) + "," +
                    repr(err_rms_l2_semi_list[lic]) + "," +
                    repr(err_rms_h1_semi_list[lic]) + "," +
                    repr(err_rms_l2_semi_rel_list[lic]) + "," +
                    repr(err_rms_h1_semi_rel_list[lic]) + "," +
                    repr(err_rms_l2_disc_list[lic]) + "," +
                    repr(err_rms_h1_disc_list[lic]) + "," +
                    repr(err_rms_l2_disc_rel_list[lic]) + "," +
                    repr(err_rms_h1_disc_rel_list[lic]) + "\n"
                    )
    # endregion
    # region write sol spec
    with open("lognormal_sgfem_samples/" + directory_name + file_ad + "sol_spec.dat", "w") as f:
        for lic in range(len(refine_list)):
            f.write(repr(refine_list[lic]) + "," +
                    repr(dim_list[lic]) + "," +
                    repr(rank_list[lic]) + "\n"
                    )
    # endregion
    # region write configuration to file
    config.write_configuration_to_file("lognormal_sgfem_samples/" + directory_name + file_ad + "configuration.pro",
                                       append=False)
    # endregion

    # region create latex header, footer and variables
    header_str = "\\documentclass{standalone} \\usepackage[utf8]{inputenc}" \
                 "\\usepackage{graphicx} \\usepackage{pgfplots} \\usetikzlibrary{shapes.arrows} " \
                 "\\usepgfplotslibrary{groupplots} \\pgfplotsset{compat=newest} \\newlength\\figwidth" \
                 "\\begin{document}"
    footer_str = "\\end{document}"
    N_r = len(rank_list[-1])
    N_d = len(dim_list[-1])
    length_r = range(1, N_r+1)
    length_d = range(1, N_d+1)
    max_rank = max(rank_list[-1])
    max_dim = max(dim_list[-1])
    width = 0.2
    # endregion
    for lic in range(len(refine_list)):
        # region solution rank
        # region create latex pictures for the rank
        fig, ax = plt.subplots()
        rect = ax.bar(length_r, list(rank_list[lic]) + [0] * (N_r - len(rank_list[lic])), width, color='g')
        ax.set_ylabel('ranks')
        ax.set_ylim([0.1, max_rank + 1])
        ax.set_title('Rank distribution for step {}'.format(lic + 1))
        ax.set_xticks(length_r)
        # ax.set_xticklabels(('G1', 'G2', 'G3', 'G4', 'G5'))
        code = tikz.get_tikz_code('rank{}.tex'.format(lic + 1))
        plt.clf()
        # endregion
        # region create rank directory
        try:
            os.makedirs("lognormal_sgfem_samples/" + directory_name + "ranks/")
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise OSError("can not create ranks directory")
        file_path = "lognormal_sgfem_samples/" + directory_name + \
                    "ranks/" + file_ad + "rank{}.tex".format(lic + 1)
        # endregion
        # region write latex picture for the rank to file
        with open(file_path, "w") as f2:
            f2.write(header_str + "\n" + code + "\n" + footer_str)
        # endregion
        # endregion
        # region solution dimension
        # region create latex picture for the degree
        fig, ax = plt.subplots()

        rect = ax.bar(length_d, list(dim_list[lic]) + [0] * (N_d - len(dim_list[lic])), width, color='g')
        ax.set_ylabel('dimensions')
        ax.set_ylim([0.1, max_dim + 1])
        ax.set_title('Dimension distribution for step {}'.format(lic + 1))
        ax.set_xticks(length_d)
        # ax.set_xticklabels(('G1', 'G2', 'G3', 'G4', 'G5'))

        code = tikz.get_tikz_code('dim{}.tex'.format(lic + 1))
        plt.clf()
        # endregion
        # region create degree directory
        try:
            os.makedirs("lognormal_sgfem_samples/" + directory_name + "dimension/")
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise OSError("can not create dimension directory")

        file_path = "lognormal_sgfem_samples/" + directory_name + \
                    "dimension/" + file_ad + "dim{}.tex".format(lic + 1)
        # endregion
        # region write latex picture for the degrees to file
        with open(file_path, "w") as f2:
            f2.write(header_str + "\n" + code + "\n" + footer_str)
        # endregion
        # endregion
        # region coefficient rank
        # region create latex picture for the rank of the coefficient
        fig, ax = plt.subplots()
        rect = ax.bar(range(1, len(coef_rank_list[-1])+1),
                      list(coef_rank_list[lic]) + [0] * (len(coef_rank_list[-1]) - len(coef_rank_list[lic])), width,
                      color='g')
        ax.set_ylabel('coefficient ranks')
        ax.set_ylim([0.1, max(coef_rank_list[-1]) + 1])
        ax.set_title('Dimension distribution for step {}'.format(lic + 1))
        ax.set_xticks(range(1, len(coef_rank_list[-1])+1))
        # ax.set_xticklabels(('G1', 'G2', 'G3', 'G4', 'G5'))

        code = tikz.get_tikz_code('coef_rank{}.tex'.format(lic + 1))
        plt.clf()
        # endregion
        # region create coefficient rank directory
        try:
            os.makedirs("lognormal_sgfem_samples/" + directory_name + "coef_rank/")
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise OSError("can not create coefficient rank directory")

        file_path = "lognormal_sgfem_samples/" + directory_name + \
                    "coef_rank/" + file_ad + "coef_rank{}.tex".format(lic + 1)
        # endregion
        # region write latex picture for the coefficient rank to file
        with open(file_path, "w") as f2:
            f2.write(header_str + "\n" + code + "\n" + footer_str)
        # endregion
        # endregion
        # region coefficient dimension
        # region create latex picture for the degree of the coefficient
        fig, ax = plt.subplots()
        rect = ax.bar(range(1, len(coef_hdeg_list[-1])+1),
                      list(coef_hdeg_list[lic]) + [0] * (len(coef_hdeg_list[-1]) - len(coef_hdeg_list[lic])), width,
                      color='g')
        ax.set_ylabel('coefficient dimensions')
        ax.set_ylim([0.1, max(coef_hdeg_list[-1]) + 1])
        ax.set_title('Dimension distribution for step {}'.format(lic + 1))
        ax.set_xticks(range(1, len(coef_hdeg_list[-1])+1))
        # ax.set_xticklabels(('G1', 'G2', 'G3', 'G4', 'G5'))

        code = tikz.get_tikz_code('coef_dim{}.tex'.format(lic + 1))
        plt.clf()
        # endregion
        # region create coefficient degree directory
        try:
            os.makedirs("lognormal_sgfem_samples/" + directory_name + "coef_dimension/")
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise OSError("can not create coefficient dimension directory")

        file_path = "lognormal_sgfem_samples/" + directory_name + \
                    "coef_dimension/" + file_ad + "coef_dim{}.tex".format(lic + 1)
        # endregion
        # region write latex picture for the coefficient degrees to file
        with open(file_path, "w") as f2:
            f2.write(header_str + "\n" + code + "\n" + footer_str)
        # endregion
        # endregion
        # region create mesh directory
        try:
            os.makedirs("lognormal_sgfem_samples/" + directory_name + "mesh/")
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise OSError("can not create mesh directory")

        file_path = "lognormal_sgfem_samples/" + directory_name + \
                    "mesh/" + file_ad + "mesh{}.pdf".format(lic + 1)
        # endregion
        # region plot mesh
        plt.clf()
        plot_mpl(mesh_list[lic])
        plt.title('Physical mesh for step {}'.format(lic+1))
        plt.draw()
        plt.savefig(file_path, dpi=None, facecolor='w',
                    edgecolor='w', orientation='portrait', papertype=None, format='pdf',
                    transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None)
        # endregion
        # region create ALS sweeps directory
        try:
            os.makedirs("lognormal_sgfem_samples/" + directory_name + "ALS_sweeps/")
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise OSError("can not create ALS sweeps directory")

        file_path = "lognormal_sgfem_samples/" + directory_name + \
                    "ALS_sweeps/" + file_ad + "ALS_sweeps{}.dat".format(lic + 1)
        # endregion
        # region write data for ALS sweeps
        with open(file_path, 'w') as f2:
            f2.write("sweep, error \n")
            for lix in range(len(als_sweep_err_list[lic])):
                f2.write(repr(lix+1) + "," + repr(als_sweep_err_list[lic][lix]) + "\n")
        # endregion
        # region create ALS first component error directory
        try:
            os.makedirs("lognormal_sgfem_samples/" + directory_name + "ALS_first_comp/")
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise OSError("can not create ALS first comp directory")

        file_path = "lognormal_sgfem_samples/" + directory_name + \
                    "ALS_first_comp/" + file_ad + "ALS_first_comp{}.dat".format(lic + 1)
        # endregion
        # region write data for ALS sweeps
        with open(file_path, 'w') as f2:
            f2.write("iteration, error \n")
            for lix in range(len(als_first_comp_err_list[lic])):
                f2.write(repr(lix+1) + "," + repr(als_first_comp_err_list[lic][lix]) + "\n")
        # endregion
        # region det estimator
        # region create det estimator directory
        try:
            os.makedirs("lognormal_sgfem_samples/" + directory_name + "det_estimator/det_estimator_{}/".format(lic+1))
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise OSError("can not create det estimator {} directory".format(lic+1))

        file_path = lambda x: "lognormal_sgfem_samples/" + directory_name + \
                    "det_estimator/det_estimator_{}/".format(lic+1) + file_ad + "{}_{}".format(str(x), lic + 1)
        # endregion
        n = mesh_list[lic].num_vertices()
        d = mesh_list[lic].geometry().dim()

        # region Create the triangulation
        mesh_coordinates = mesh_list[lic].coordinates().reshape((n, d))
        triangles = np.asarray([cell.entities(0) for cell in cells(mesh_list[lic])])
        triangulation = tri.Triangulation(mesh_coordinates[:, 0], mesh_coordinates[:, 1], triangles)
        # endregion

        cmap = plt.cm.jet

        dg0_fun = Function(FunctionSpace(mesh_list[lic], 'DG', 0))

        # region def write det estimator file
        def write_det_estimator_file(path, item, _max_value):
            _f2 = File(file_path(path) + ".pvd")
            dg0_fun.vector()[:] = item
            _f2 << dg0_fun
            plt.figure()
            zfaces = np.asarray([dg0_fun(point) for point in mesh_coordinates])
            plt.tripcolor(triangulation, zfaces, cmap=cmap, vmin=0, vmax=_max_value, edgecolors='k')
            plt.colorbar()
            plt.savefig(file_path(path) + ".png")
            plt.clf()
        # endregion

        # region def write list of estimator
        def write_list_of_estimators(_item_list, _max_value):
            assert(len(_item_list) == 10)
            _fig, axs = plt.subplots(5, 2, facecolor='w', edgecolor='k')
            _fig.subplots_adjust(hspace=.5, wspace=.01)

            axs = axs.ravel()

            for i in range(10):
                dg0_fun.vector()[:] = _item_list[i]
                zfaces = np.asarray([dg0_fun(point) for point in mesh_coordinates])

                if i == 0:
                    axs[i].tripcolor(triangulation, zfaces, cmap=cmap, vmax=_max_value["rhs"], edgecolors='k')
                    axs[i].set_title("rhs")
                if i == 1:
                    a = axs[i].tripcolor(triangulation, zfaces, cmap=cmap, vmax=_max_value["rhs"],
                                         edgecolors='k')
                    axs[i].set_title("sampled rhs")
                    _fig.colorbar(a)
                if i == 2:
                    axs[i].tripcolor(triangulation, zfaces, cmap=cmap, vmax=_max_value["zero"], edgecolors='k')
                    axs[i].set_title("zero")
                if i == 3:
                    a = axs[i].tripcolor(triangulation, zfaces, cmap=cmap, vmax=_max_value["zero"],
                                         edgecolors='k')
                    axs[i].set_title("sampled zero")
                    _fig.colorbar(a)
                if i == 4:
                    axs[i].tripcolor(triangulation, zfaces, cmap=cmap, vmax=_max_value["sq"], edgecolors='k')
                    axs[i].set_title("sq")
                if i == 5:
                    a = axs[i].tripcolor(triangulation, zfaces, cmap=cmap, vmax=_max_value["sq"],
                                         edgecolors='k')
                    axs[i].set_title("sampled sq")
                    _fig.colorbar(a)
                if i == 6:
                    axs[i].tripcolor(triangulation, zfaces, cmap=cmap, vmax=_max_value["jump"], edgecolors='k')
                    axs[i].set_title("jump")
                if i == 7:
                    a = axs[i].tripcolor(triangulation, zfaces, cmap=cmap, vmax=_max_value["jump"],
                                         edgecolors='k')
                    axs[i].set_title("sampled jump")
                    _fig.colorbar(a)
                if i == 8:
                    axs[i].tripcolor(triangulation, zfaces, cmap=cmap, vmax=_max_value["eta"], edgecolors='k')
                    axs[i].set_title("eta")
                if i == 9:
                    a = axs[i].tripcolor(triangulation, zfaces, cmap=cmap, vmax=_max_value["eta"],
                                         edgecolors='k')
                    axs[i].set_title("sampled eta")
                    _fig.colorbar(a)

            plt.tight_layout()
            _fig.savefig(file_path("all") + ".png")
            _fig.clf()
        # endregion

        # region write det estimator results
        max_value_dict = {
            "eta": np.max(np.array([np.max(eta_list_local[lic]), np.max(sampled_eta_list_local[lic])])),
            "rhs": np.max(np.array([np.max(rhs_list_local[lic]), np.max(sampled_rhs_list_local[lic])])),
            "zero": np.max(np.array([np.max(zero_list_local[lic]), np.max(sampled_zero_list_local[lic])])),
            "sq": np.max(np.array([np.max(sq_list_local[lic]), np.max(sampled_sq_list_local[lic])])),
            "jump": np.max(np.array([np.max(jump_list_local[lic]), np.max(sampled_jump_list_local[lic])]))
        }

        write_det_estimator_file("eta", eta_list_local[lic], max_value_dict["eta"])
        write_det_estimator_file("sampled_eta", sampled_eta_list_local[lic], max_value_dict["eta"])
        write_det_estimator_file("rhs", rhs_list_local[lic], max_value_dict["rhs"])
        write_det_estimator_file("sampled_rhs", sampled_rhs_list_local[lic], max_value_dict["rhs"])
        write_det_estimator_file("zero", zero_list_local[lic], max_value_dict["zero"])
        write_det_estimator_file("sampled_zero", sampled_zero_list_local[lic], max_value_dict["zero"])
        write_det_estimator_file("sq", sq_list_local[lic], max_value_dict["sq"])
        write_det_estimator_file("sampled_sq", sampled_sq_list_local[lic], max_value_dict["sq"])
        write_det_estimator_file("jump", jump_list_local[lic], max_value_dict["jump"])
        write_det_estimator_file("sampled_jump", sampled_jump_list_local[lic], max_value_dict["jump"])
        item_list = [rhs_list_local[lic], sampled_rhs_list_local[lic],
                     zero_list_local[lic], sampled_zero_list_local[lic],
                     sq_list_local[lic], sampled_sq_list_local[lic],
                     jump_list_local[lic], sampled_jump_list_local[lic],
                     eta_list_local[lic], sampled_eta_list_local[lic]]
        write_list_of_estimators(item_list, max_value_dict)

        # endregion
        # endregion

        # region create coefficient sample directory
        try:
            os.makedirs("lognormal_sgfem_samples/" + directory_name + "coef_samples/coef_sample_{}/".format(lic + 1))
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise OSError("can not create coef_sample {} directory".format(lic + 1))

        file_path = lambda x: "lognormal_sgfem_samples/" + directory_name + \
                              "coef_samples/coef_sample_{}/".format(lic + 1) + "y_{}".format(str(x))

        # endregion

        # region write coefficient sample parameter to file
        with open("lognormal_sgfem_samples/" + directory_name + "coef_samples/00_samples.txt", "wb") as f2:
            f2.write("used sample coordinates in coefficient sampling: \n")
            for y in y_list:
                f2.write(str(y) + "\n")
        # endregion

        # region write coef samples to file
        assert (len(coef_cont_list[lic]) == len(coef_semicont_list[lic]))
        assert (len(coef_cont_list[lic]) == len(coef_discrete_list[lic]))
        curr_fs = FunctionSpace(mesh_list[lic], 'CG', config.femdegree)
        coordinates = curr_fs.tabulate_dof_coordinates().reshape((-1, curr_fs.mesh().geometry().dim()))
        for lib in range(len(coef_cont_list[lic])):
            _fig = plt.figure()

            ax = _fig.add_subplot(131, projection="3d")
            a = ax.plot_trisurf(coordinates[:, 0], coordinates[:, 1], coef_cont_list[lic][lib], cmap=cmap,
                                edgecolors='k')
            ax.set_title("continuous coefficient")
            _fig.colorbar(a)

            ax = _fig.add_subplot(132, projection="3d")
            a = ax.plot_trisurf(coordinates[:, 0], coordinates[:, 1], coef_semicont_list[lic][lib], cmap=cmap,
                                edgecolors='k')
            ax.set_title("semi-discrete coefficient")
            _fig.colorbar(a)

            ax = _fig.add_subplot(133, projection="3d")
            a = ax.plot_trisurf(coordinates[:, 0], coordinates[:, 1], coef_discrete_list[lic][lib], cmap=cmap,
                                edgecolors='k')
            ax.set_title("discrete coefficient")
            _fig.colorbar(a)
            _fig.savefig(file_path(lib) + ".png")
            _fig.clf()

        # endregion
    exit()
# endregion

# region adaptive solution process
err_compare_t_d = []
err_compare_t_a = []
err_compare_a_d = []
coef_err_compare_t_d = []
coef_err_compare_t_a = []
coef_err_compare_a_d = []
refined = None
n_samples = 0
tail_sample_factor = 1
curr_order4_stiffness_tensor = None
curr_order4_volume_tensor_list = []
refinement_cache = None
# y_list = [semi_discrete_coef.sample_rvs(config.num_coeff_in_coeff)*tail_sample_factor for _ in range(n_samples)]
for step in range(config.adaptive_iterations):
    # print(TicToc.sortedTimes(sec_sorted=True))
    print("=" * 30)
    print("iteration step {} / {}".format(step + 1, config.adaptive_iterations))

    # region create current function space
    with TicToc(sec_key="FunctionSpace", key="create new fs", do_print=False, active=True):
        curr_fs = FunctionSpace(mesh, 'CG', config.femdegree)
    # endregion

    if refined == "det" or step == 0:
        # region Update order 4 stiffness tensor
        with TicToc(sec_key="FunctionSpace", key="create order 4 residual tensor", do_print=True, active=True):
            curr_order4_stiffness_tensor = create_order4_stiffness(curr_fs)
        # with TicToc(sec_key="FunctionSpace", key="create order 4 sq residual tensor", do_print=True, active=True):
        #     curr_order4_volume_tensor_list = create_order4_volume_operator_list(curr_fs)
        # endregion
    # region create current forward operator
    with TicToc(sec_key="Forward Operator", key="create new operator", do_print=False, active=True):
        curr_forward_operator = TTLognormalAsgfemOperator(semi_discrete_coef, curr_fs, curr_hdeg, config.domain)
    # endregion

    # region write out coef samples
    with TicToc(sec_key="Coefficient", key="sample coefficient", do_print=False, active=True):
        # region create coef results directory
        directory_name = str(get_current_time_string()) + "-" + str(semi_discrete_coef.get_hash()) + "/"
        try:
            if os.path.exists("../Coeff/" + directory_name):
                shutil.rmtree("../Coeff/" + directory_name)
            os.makedirs("../Coeff/" + directory_name)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise OSError("can not create coef sampling directory")

        file_path = "../Coeff/" + directory_name

        with open(file_path + "00_info.txt", 'w') as f:
            f.write("#"*10 + "\n")
            f.write(" info file containing information about the coefficient sampled in this directory" + "\n")
            f.write("#"*10 + "\n\n")
            f.write("created samples in this directory: {}".format(n_samples))
            f.write("factor used to sample from tail  : {}   (1=default sampling from alea rv)".format(tail_sample_factor))
        config.write_configuration_to_file(file_path + "00_info.txt", append=True)

        # endregion
        curr_dofs = np.reshape(curr_fs.tabulate_dof_coordinates(), (-1, 2))
        y_list = [semi_discrete_coef.sample_rvs(config.reference_M) for _ in range(n_samples)]

        # region def write coef to file
        def write_coef_file(_path, _obj):
            write_pvd_file(_path, _obj)
            write_2d_png(_path, _obj)
            write_3d_png(_path, _obj)
        # endregion
        # region loop over samples of coefficient
        for lia, y in enumerate(y_list):
            with open(file_path + "00_info.txt", 'a') as f:
                f.write("\n used sample for realisation {}: \n {}".format(lia, y))
            coef = Function(curr_fs)
            coef.vector()[:] = curr_forward_operator.cont_coef.sample_continuous_field(curr_dofs, y)
            write_coef_file(file_path + "cont_coef{}-{}".format(step, lia), coef)
            coef.vector()[:] = curr_forward_operator.cont_coef(curr_dofs, y, fs=curr_fs)
            write_coef_file(file_path + "semicont_coef{}-{}".format(step, lia), coef)
            coef.vector()[:] = curr_forward_operator.discrete_coef(curr_dofs, y)
            write_coef_file(file_path + "discrete_coef{}-{}".format(step, lia), coef)
        # endregion
        # region Save path to operator for caching later
        curr_forward_operator.coef_sample_path = file_path
        # endregion
    # endregion

    # region solve the problem
    # if max(curr_rank) > max(tt.vector.from_list(curr_solution).r):
    #     curr_solution = []
    curr_forward_operator.solve(curr_rank, config.preconditioner, config.iterations, config.als_tol, curr_solution)
    # endregion
    # region refine forward solution
    loc_mesh_once = mesh_once if step < 1 else False
    loc_sto_once = sto_once if step < 1 else False
    refined_solution = curr_forward_operator.refine_adaptive(config.theta_x, config.theta_y,
                                                             config.resnorm_zeta_weight,
                                                             config.eta_zeta_weight,
                                                             config.rank_always, config.sol_rank,
                                                             config.start_rank, config.new_hdeg,
                                                             order4_stiffness_tensor=curr_order4_stiffness_tensor,
                                                             order4_volume_tensor_list=curr_order4_volume_tensor_list,
                                                             refinement_cache=refinement_cache,
                                                             reference_m=config.reference_M,
                                                             refine_sto_uniform=refine_sto_uniform,
                                                             bound_sol_degree=bound_sol,
                                                             mesh_once=loc_mesh_once,
                                                             sto_once=loc_sto_once
                                                             )
    curr_solution = refined_solution["solution"]
    mesh = refined_solution["mesh"]
    curr_hdeg = refined_solution["hdegs"]
    curr_rank = refined_solution["ranks"]
    det_estimator_result = refined_solution["det_estimator"]
    sto_estimator_result = refined_solution["sto_estimator"]
    res_estimator_result = refined_solution["res_estimator"]
    refined = refined_solution["refined"]

    if refined == "sto":
        # region Update semi discrete stochastic coefficient field
        with TicToc(sec_key="Coefficient", key="check validity of size", do_print=False, active=True):
            semi_discrete_coef.update_coefficient(curr_hdeg, config.max_rank, add_order=4, new_dimension=2, add_dim=2,
                                                  max_hdeg=config.max_hdegs_coef, same_length=(config.theta_y <= 0))
        # endregion
    # region uniform mesh refinement
    if refine_mesh_uniform is True:
        from dolfin import interpolate
        print("     refine mesh uniform")
        mesh = refine(mesh)
        CG = FunctionSpace(mesh, 'CG', config.femdegree)
        # cores = V.to_list(V)
        F = Function(curr_fs)
        cores = curr_solution
        r = cores[0].shape[2]
        newcore = np.zeros([1, CG.dim(), r])
        for d in range(r):
            # print "++++++++", CG.dim(), cores[0][0, :, d].shape
            F.vector().set_local(cores[0][0, :, d])
            # print "refinement", d, F.vector().array().shape,CG.dim()
            newF = interpolate(F, CG)
            newcore[0, :, d] = newF.vector().array()
        # update U
        cores[0] = newcore
        curr_solution = cores
    # endregion

    refinement_cache = dict()

    refinement_cache["res_estimator_result"] = res_estimator_result
    refinement_cache["det_estimator_result"] = det_estimator_result
    refinement_cache["sto_estimator_result"] = sto_estimator_result
    refinement_cache["last"] = refined

    if not no_save:
        curr_forward_operator.save_to_hash(cache_file)
    curr_overall_estimator = compute_overall_error_indicator(det_estimator_result["eta_global"],
                                                             sto_estimator_result["global_zeta"],
                                                             res_estimator_result["resnorm"])

    TicToc.sortedTimes(sec_sorted=True)
    print("curr estimator value: {}".format(curr_overall_estimator))
    curr_dofs = tt_dofs(tt.vector.from_list(curr_solution))
    print("curr tt-dofs: {}".format(curr_dofs))
    TicToc.clear()
    if curr_dofs >= config.max_dofs:
        print("maximal dofs reached -> exit()")
        exit()
    if mesh_once and lia >= 1:
        print("one mesh refinement done -> exit()")
        exit()
    if sto_once and lia >= 1:
        print("one stochastic refinement done -> exit()")
        exit()
    # endregion
# endregion
