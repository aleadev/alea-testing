"""
test script to sample the forward solution of the asgfem using the lognormal tensor train description
@author: Manuel Marschall
"""

# region imports
from __future__ import division

import argparse                                     # use command parameter parsing
from dolfin import FunctionSpace, refine, Function, File, errornorm, norm, UnitSquareMesh, set_log_level, ERROR, \
    project, cells, Mesh

import matplotlib.tri as tri
import matplotlib.pyplot as plt
import matplotlib2tikz as tikz
import tt
import numpy as np
from alea.utils.plothelper import plot_mpl
import os
import shutil
import errno
from alea.application.bayes.Bayes_util.ForwardOperator.tt_lognormal_asgfem_operator import TTLognormalAsgfemOperator
from alea.application.bayes.Bayes_util.ForwardOperator.tt_lognormal_asgfem_operator_data import  \
    TTLognormalAsgfemOperatorData
from alea.application.bayes.alea_util.coefficient_field.tensor.tt_lognormal_fullydiscrete_field import \
    TTLognormalFullyDiscreteField
from alea.application.bayes.alea_util.coefficient_field.tensor.tt_lognormal_semidiscrete_field import \
    read_coef_from_config
from alea.application.bayes.Bayes_util.ForwardOperator.tt_parallel_worker import TTParallelWorker
from alea.application.bayes.Bayes_util.ForwardOperator.tt_forward_operator_util import compute_overall_error_indicator
from alea.application.bayes.alea_util.configuration.asgfem_config import AsgfemConfig
from alea.application.egsz.sample_domains import SampleDomain
from alea.application.egsz.sample_problems2 import SampleProblem

from alea.application.tt_asgfem.tensorsolver.tt_sgfem_util import tt_dofs
from alea.utils.tictoc import TicToc
from alea.utils.timing import get_current_time_string

from alea.application.bayes.Bayes_util.ForwardOperator.tt_forward_operator_util import calculate_stiffness_matrix, \
        calculate_mass_matrix

from copy import deepcopy

set_log_level(ERROR)
# endregion

# region parser arguments
parser = argparse.ArgumentParser(description="estimate the MC convergence against the TT-mean")
parser.add_argument("-f", "--file", type=str, default="default.pro",
                    help="problem file to load")
parser.add_argument("-sf", "--solutionFile", type=str, default="",
                    help="file containing hashes of the collected solutions to load")
parser.add_argument("-sample", "--sample", action="store_true",
                    help="switch to deactivate the computation. Just sample from the given list")
parser.add_argument("-mesh_uniform", "--mesh_uniform", action="store_true",
                    help="switch to deactivate the adaptivity in the physical mesh")
parser.add_argument("-new", "--new_solution_file", action="store_true",
                    help="switch to overwrite the existing solution file")

args = parser.parse_args()
problem_file = "../../alea_util/configuration/problems/" + args.file
if args.solutionFile == "":
    cache_file = "../../Bayes_util/ForwardOperator/tmp/" + get_current_time_string() + "solutions.txt"
else:
    cache_file = "../../Bayes_util/ForwardOperator/tmp/" + args.solutionFile
sample_only = args.sample
new_solution_file = args.new_solution_file
refine_mesh_uniform = args.mesh_uniform
# endregion

# region pre-computations and object creation
# region Read configuration from problem file
with TicToc(sec_key="Configuration", key="read configuration", do_print=False, active=True):
    config = AsgfemConfig(problem_file)
    if config.read_configuration() is False:
        exit()

    if refine_mesh_uniform is True:
        config.theta_x = 0
# endregion

# region read coefficient from configuration
with TicToc(sec_key="Semi-Disc Coef", key="read from configuration", do_print=False, active=True):
    semi_discrete_coef = read_coef_from_config(config)
if False:
    rel_error = 0
    print(" create coordinate samples")
    with TicToc(sec_key="Semi-Disc Coef", key="create coordinate samples", do_print=True, active=True):
        y_list = [semi_discrete_coef.sample_rvs(config.reference_M) for _ in range(10)]
    mesh_coef = UnitSquareMesh(20, 20)
    fs_coef = FunctionSpace(mesh_coef, 'CG', 3)
    func_coef_cont = Function(fs_coef)
    # func_coef_cont_dolf = Function(fs_coef)
    func_coef_analytic = Function(fs_coef)
    func_coef_discrete = Function(fs_coef)
    discrete_coef = TTLognormalFullyDiscreteField(semi_discrete_coef, fs_coef, config.domain)
    dofs = fs_coef.tabulate_dof_coordinates()
    dofs = dofs.reshape(-1, 2)
    for y in y_list:
        with TicToc(sec_key="Semi-Disc Coef", key="sample semi-discrete coef dolfin", do_print=True, active=True):
            func_coef_cont.vector()[:] = semi_discrete_coef(dofs, y, fs=fs_coef)
        # with TicToc(sec_key="Semi-Disc Coef", key="sample semi-discrete coef", do_print=True, active=True):
        #     func_coef_cont.vector()[:] = semi_discrete_coef(dofs, y)
        with TicToc(sec_key="Semi-Disc Coef", key="sample continuous field", do_print=True, active=True):
            func_coef_analytic.vector()[:] = semi_discrete_coef.sample_continuous_field(dofs, y)
        with TicToc(sec_key="Semi-Disc Coef", key="sample discrete coef", do_print=True, active=True):
            func_coef_discrete.vector()[:] = discrete_coef(dofs, y)
        with TicToc(sec_key="Semi-Disc Coef", key="compute errors", do_print=True, active=True):
            rel_error += errornorm(func_coef_analytic, func_coef_cont)/norm(func_coef_analytic)
            print("error cont-semi: {}".format(errornorm(func_coef_analytic, func_coef_cont)/norm(func_coef_analytic)))
            print("error cont-discrete: {}".format(errornorm(func_coef_analytic, func_coef_discrete) /
                                                   norm(func_coef_analytic)))
            print("error semi-discrete: {}".format(errornorm(func_coef_cont, func_coef_discrete) / norm(func_coef_cont)))
            # print("error semi-semidolf: {}".format(errornorm(func_coef_cont, func_coef_cont_dolf) / norm(func_coef_cont)))
    print("relative error cont-discrete: {} after {} samples".format(rel_error/len(y_list), len(y_list)))
    exit()
# endregion

# region try to read solution data from file
try:
    from os.path import isfile
    if not isfile(cache_file) or new_solution_file is True:
        with open(cache_file, 'wb') as f:
            print("create solution cache file list: {}".format(cache_file))
    f = open(cache_file, 'rb')
    lines = f.readlines()
    f.close()
    read_solutions = []
    for line in lines:
        sol = TTLognormalAsgfemOperatorData()
        line = line.strip()
        print("try to read file: {}".format(line))
        if len(line) <= 1:
            continue
        if sol.load(line) is False:
            print("can not read file")
            read_solution = None
            break
        print("found solution: ")
        print("     hedgs: {}".format(sol.hdegs))
        print("     ranks: {}".format(sol.ranks))
        print("     m-dofs: {}".format(sol.m_dofs))
        read_solutions.append(sol)

except Exception as ex:
    print(ex)
    read_solutions = None
    exit()
# endregion

# region construct initial ranks and hermite degrees
curr_rank = [1] + [config.start_rank]*config.n_coefficients + [1]
if isinstance(config.gpc_degree, list):
    curr_hdeg = config.gpc_degree
else:
    curr_hdeg = [config.hermite_degree]*config.n_coefficients
curr_solution = []
# endregion
# region create initial fs
with TicToc(sec_key="initial function space", key="create", do_print=False, active=True):
    if config.init_mesh_dofs > 0:  # mesh dofs are defined
        mesh_refine = 0  # there are no mesh refinements
    else:
        mesh_refine = config.mesh_refine  # use configured mesh refinements

    mesh, boundaries, dim = SampleDomain.setupDomain(config.domain, initial_mesh_N=2)
    #                                               # refine mesh according to input
    mesh = SampleProblem.setupMesh(mesh, num_refine=mesh_refine)
    #                                               # refinement if you want to have a specific amount of dofs

    print("soll mesh_dofs={} haben dofs={}".format(config.init_mesh_dofs,
                                                   FunctionSpace(mesh, 'CG', config.femdegree).dim()))
    while config.init_mesh_dofs > 0 and FunctionSpace(mesh, 'CG', config.femdegree).dim() < config.init_mesh_dofs:
        mesh = refine(mesh)  # uniformly refine mesh
    pde, _, _, _, _, _ = SampleProblem.setupPDE(2, config.domain, 0, boundaries, semi_discrete_coef)
# endregion
# endregion

# region adaptive solution process
err_compare_t_d = []
err_compare_t_a = []
err_compare_a_d = []
coef_err_compare_t_d = []
coef_err_compare_t_a = []
coef_err_compare_a_d = []
eta = None
zeta = None
resnorm = None
refined = None
n_samples = 10
tail_sample_factor = 1
y_list = [semi_discrete_coef.sample_rvs(config.num_coeff_in_coeff)*tail_sample_factor for _ in range(n_samples)]

considered_sol = np.min([10, len(read_solutions), -2])



assert(len(read_solutions) > 1)
mesh = read_solutions[considered_sol].mesh
curr_fs = FunctionSpace(mesh, 'CG', config.femdegree)


curr_hdeg = read_solutions[considered_sol].hdegs
curr_rank = read_solutions[considered_sol].ranks
curr_solution = read_solutions[considered_sol-1].new_solution
if False: # refine the mesh
    mesh = refine(mesh)
    CG = FunctionSpace(mesh, 'CG', config.femdegree)
    # cores = V.to_list(V)
    F = Function(curr_fs)
    cores = curr_solution
    r = cores[0].shape[2]
    newcore = np.zeros([1, CG.dim(), r])
    from dolfin import interpolate
    for d in range(r):
        # print "++++++++", CG.dim(), cores[0][0, :, d].shape
        F.vector().set_local(cores[0][0, :, d])
        # print "refinement", d, F.vector().array().shape,CG.dim()
        newF = interpolate(F, CG)
        newcore[0, :, d] = newF.vector().array()
    # update U
    cores[0] = newcore
    curr_solution = cores
for step in range(config.adaptive_iterations):
    # print(TicToc.sortedTimes(sec_sorted=True))
    print("=" * 30)
    print("iteration step {} / {}".format(step + 1, config.adaptive_iterations))

    # region create current function space
    with TicToc(sec_key="FunctionSpace", key="create new fs", do_print=False, active=True):
        curr_fs = FunctionSpace(mesh, 'CG', config.femdegree)
    # endregion
    # region create current forward operator
    with TicToc(sec_key="Forward Operator", key="create new operator", do_print=False, active=True):
        curr_forward_operator = TTLognormalAsgfemOperator(semi_discrete_coef, curr_fs, curr_hdeg, config.domain)
    # endregion

    if False:
        # region write out coef samples
        # region create coef results directory
        directory_name = str(get_current_time_string()) + "-" + str(semi_discrete_coef.get_hash()) + "/"
        try:
            if os.path.exists("../Coeff/" + directory_name):
                shutil.rmtree("../Coeff/" + directory_name)
            os.makedirs("../Coeff/" + directory_name)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise OSError("can not create coef sampling directory")

        file_path = "../Coeff/" + directory_name

        with open(file_path + "00_info.txt", 'w') as f:
            f.write("#"*10 + "\n")
            f.write(" info file containing information about the coefficient sampled in this directory" + "\n")
            f.write("#"*10 + "\n\n")
            f.write("created samples in this directory: {}".format(n_samples))
            f.write("factor used to sample from tail  : {}   (1=default sampling from alea rv)".format(tail_sample_factor))
        config.write_configuration_to_file(file_path + "00_info.txt", append=True)

        # endregion
        curr_dofs = np.reshape(curr_fs.tabulate_dof_coordinates(), (-1, 2))
        y_list = [semi_discrete_coef.sample_rvs(config.reference_M) for _ in range(0)]

        # region def write coef to file
        def write_coef_file(path, obj):
            def mesh2triang(__mesh):
                xy = __mesh.coordinates()
                return tri.Triangulation(xy[:, 0], xy[:, 1], __mesh.cells())
            f2 = File(path + ".pvd")
            f2 << obj
            plt.figure()
            plt.gca().set_aspect('equal')
            if isinstance(obj, Function):
                _mesh = obj.function_space().mesh()
                if _mesh.geometry().dim() != 2:
                    raise AttributeError
                if obj.vector().size() == _mesh.num_cells():
                    C = obj.vector().array()
                    plt.tripcolor(mesh2triang(_mesh), C, edgecolors="k")
                else:
                    C = obj.compute_vertex_values(_mesh)
                    plt.tripcolor(mesh2triang(_mesh), C, shading='gouraud', edgecolors="k")
            elif isinstance(obj, Mesh):
                if obj.geometry().dim() != 2:
                    raise AttributeError
                plt.triplot(mesh2triang(obj), color='k')
            # TODO: make a 3D plot
            plt.colorbar()
            plt.title("min value of array: {}".format(np.min(obj.vector().array())))
            plt.savefig(path + ".png")
            plt.clf()
        # endregion
        # region loop over samples
        for lia, y in enumerate(y_list):
            with open(file_path + "00_info.txt", 'a') as f:
                f.write("\n used sample for realisation {}: \n {}".format(lia, y))
            coef = Function(curr_fs)
            coef.vector()[:] = curr_forward_operator.cont_coef.sample_continuous_field(curr_dofs, y)
            write_coef_file(file_path + "cont_coef{}-{}".format(step, lia), coef)
            coef.vector()[:] = curr_forward_operator.cont_coef(curr_dofs, y)
            write_coef_file(file_path + "semicont_coef{}-{}".format(step, lia), coef)
            coef.vector()[:] = curr_forward_operator.discrete_coef(curr_dofs, y)
            write_coef_file(file_path + "discrete_coef{}-{}".format(step, lia), coef)
        # endregion
        # endregion

    # region solve the problem
    # if max(curr_rank) > max(tt.vector.from_list(curr_solution).r):
    #     curr_solution = []
    curr_forward_operator.solve(curr_rank, config.preconditioner, config.iterations, config.als_tol, curr_solution)
    # endregion
    TicToc.sortedTimes(sec_sorted=True)
    exit()
    # region refine forward solution
    refined_solution = curr_forward_operator.refine_adaptive(config.theta_x, config.theta_y,
                                                             config.resnorm_zeta_weight,
                                                             config.eta_zeta_weight,
                                                             config.rank_always, config.sol_rank,
                                                             config.start_rank, config.new_hdeg)
    curr_solution, mesh, curr_hdeg, curr_rank, eta, zeta, resnorm, refined = refined_solution

    # region uniform mesh refinement
    if refine_mesh_uniform is True:
        from dolfin import interpolate
        print("     refine mesh uniform")
        mesh = refine(mesh)
        CG = FunctionSpace(mesh, 'CG', config.femdegree)
        # cores = V.to_list(V)
        F = Function(curr_fs)
        cores = curr_solution
        r = cores[0].shape[2]
        newcore = np.zeros([1, CG.dim(), r])
        for d in range(r):
            # print "++++++++", CG.dim(), cores[0][0, :, d].shape
            F.vector().set_local(cores[0][0, :, d])
            # print "refinement", d, F.vector().array().shape,CG.dim()
            newF = interpolate(F, CG)
            newcore[0, :, d] = newF.vector().array()
        # update U
        cores[0] = newcore
        curr_solution = cores
    # endregion
    # do not save something here since we want to reuse solutions
    # curr_forward_operator.save_to_hash(cache_file)
    curr_overall_estimator = compute_overall_error_indicator(eta, zeta, resnorm)
    print("curr estimator value: {}".format(curr_overall_estimator))
    curr_dofs = tt_dofs(tt.vector.from_list(curr_solution))
    print("curr tt-dofs: {}".format(curr_dofs))
    TicToc.sortedTimes(sec_sorted=True)
    TicToc.clear()
    if curr_dofs >= config.max_dofs:
        print("maximal dofs reached -> exit()")
        exit()
    # endregion
# endregion