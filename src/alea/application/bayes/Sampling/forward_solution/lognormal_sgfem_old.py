# !usr/bin/env python
# Hallo das ist ein Test
# -*- coding: latin-1 -*-
"""
  sample TT-tensor Bayesian inversion results in view of normalization constant (mean), density and solution errors
"""

# region Imports
# region Standard library imports
import numpy as np                                  # use Matlab like array notation
from numpy.testing import assert_almost_equal       # gets the almost equal operator for numpy arrays
import matplotlib                                   # use plot library
matplotlib.use('Agg')                               # use 'Agg' extension to use in cluster environment
# matplotlib.interactive(True)
import os                                           # use system directory and file library
import argparse                                     # use command parameter parsing
# import matplotlib.pyplot as plt                     # use python plotting library
# from scipy.integrate import quad                    # use quadrature integration
# from scipy.interpolate import interp1d              # use one dimensional interpolation
# from joblib import Parallel, delayed                # use parallel computing and function delaying
import copy
from alea.utils.tictoc import TicToc
# endregion

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import tt
# region Bayes libraries
from alea.application.bayes.Bayes_util.lib.bayes_lib import create_parameter_samples
from alea.application.bayes.Bayes_util.exponential.implicit_euler import ImplicitEuler
# from Bayes_util.asgfem_bayes import ASGFEMBayes
from alea.application.bayes.Bayes_util.lognormal_sgfem_bayes import LognormalSGFEMBayes
from alea.application.bayes.Bayes_util.exponential.embedded_runge_kutta import EmbeddedRungeKutta
from alea.application.bayes.Bayes_util.exponential.rank_adaptive_embedded_runge_kutta import \
    RankAdaptiveEmbeddedRungeKutta
from alea.application.tt_asgfem.tensorsolver.tt_residual import TTResidual
from alea.application.tt_asgfem.tensorsolver.tt_lognormal import sample_lognormal_tt, get_coeff_upper_bound
from alea.application.tt_asgfem.tensorsolver.tt_sgfem_util import tt_dofs
from alea.application.tt_asgfem.tensorsolver.tt_lognormal import sample_cont_coeff
# from Bayes_util.lib.interpolation_util import get_cheb_points
# endregion

from alea.utils.plothelper import PlotHelper
# from alea.application.egsz.sampling import compute_parametric_sample_solution_TT
from alea.application.egsz.affine_field import AffineField
import logging.config                               # use logging to file

from dolfin import *
from dolfin import set_log_level, as_backend_type, dx

# endregion
__author__ = "marschall"

# region Setup parameters
# region Parser Arguments
# region ASGFEM parameter
parser = argparse.ArgumentParser(description="estimate the MC convergence against the TT-mean")
parser.add_argument("-gd", "--gpcdegree", type=int, default="10",
                    help="maximal # of gpc degrees")
parser.add_argument("-fd", "--femdegree", type=int, default="1", choices=range(1, 4),
                    help="femdegree used in ASGFEM")
parser.add_argument("-md", "--max_dofs", type=float, default="1e6",
                    help="maximal # of dofs in ASGFEM")
parser.add_argument("-mer", "--mesh_refine", type=int, default="1",
                    help="number of mesh refinements")
parser.add_argument("-it", "--iterations", type=int, default="1000",
                    help="maximal # of als iterations")
parser.add_argument("-dom", "--domain", type=str, default="square", choices=["square", "lshape"],
                    help="domain used in ASGFEM")
parser.add_argument("-ct", "--coef_type", type=str, default="EF-square-cos-algebraic",
                    choices=["EF-square-cos-algebraic"], help="amplification and coefficient field function type")
parser.add_argument("-de", "--decay_exp_rate", type=float, default=2.0,
                    help="rate of decay in the coefficient field")
parser.add_argument("-fm", "--field_mean", type=float, default=1.0,
                    help="mean of the coefficient field")
parser.add_argument("-sg", "--sgfem_gamma", type=float, default=0.9,
                    help="value of gamma in the ASGFEM coefficient field")
parser.add_argument("-imd", "--init_mesh_dofs", type=int, default=-1,
                    help="# of initial physical mesh dofs (-1: ignore)")
parser.add_argument("-tx", "--theta", type=float, default=0.5,
                    help="weight of Gaussian measure")
parser.add_argument("-rho", "--rho", type=float, default=1.0,
                    help="weight of Gaussian measure > 1")
parser.add_argument("-hd", "--hermite_degree", type=int, default=8,
                    help="hermite polynomial degree on gPC expansion in coefficient field (only tensor size, hence "
                         "consider one more)")
parser.add_argument("-gpcd", "--gpc_degree", type=int, default=12,
                    help="maximal number of gpcd. for the solution representation")
parser.add_argument("-nc", "--n_coefficients", type=int, default=5,
                    help="# of coefficients in the stochastic field approximation (aka M)")
parser.add_argument("-mr", "--max_rank", type=int, default=15,
                    help="maximal rank in coefficient field approximation")
parser.add_argument("-sr", "--sol_rank", type=int, default=5,
                    help="maximal rank in solution approximation")
parser.add_argument("-frsc", "--freq_scale", type=float, default=1.0,
                    help="frequency scaling in the gPC expansion")
parser.add_argument("-frsk", "--freq_skip", type=int, default=1,
                    help="frequency skip in the gPC expansion")
parser.add_argument("-sca", "--scale", type=float, default=1.0,
                    help="scalinf of the whole solution")
parser.add_argument("-tol", "--als_tol", type=float, default=1e-12,
                    help="tolerance in the ALS algorithm")
parser.add_argument("-cacc", "--coef_tol", type=float, default=1e-10,
                    help="tolerance in the tensor rounding in the coefficient field approximation")
parser.add_argument("-adapt_it", "--adapt_it", type=int, default=10,
                    help="Number of iterations in the adaptive process until termination")
parser.add_argument("-thx", "--theta_x", type=float, default=0.5,
                    help="relation of deterministic residual")
parser.add_argument("-thy", "--theta_y", type=float, default=0.5,
                    help="relation of stochastic residual")
parser.add_argument("-start_rank", "--start_rank", type=int, default=2,
                    help="Rank in the first step of the adaptive solution process.")
parser.add_argument("-num_coef_coef", "--num_coeff_in_coeff", type=int, default=15,
                    help="Number of coefficients in the coefficient field expansion")
parser.add_argument("-max_hdegs_coef", "--max_hdegs_coef", type=int, default=15,
                    help="Number of maximal hermite degree in the coefficient field expansion")
parser.add_argument("-ezw", "--eta_zeta_weight", type=float, default=0.1,
                    help="relation between deterministic and stochastic estimator (overestimation constant)")
parser.add_argument("-rzw", "--resnorm_zeta_weight", type=float, default=1.0,
                    help="relation between ALS residual norm and stochastic estimator")
parser.add_argument("-coef_mesh_dofs", "--coef_mesh_dofs", type=int, default=300000,
                    help="number of physical degrees of freedom in the coefficient field representation")
parser.add_argument("-coef_quad_degree", "--coef_quad_degree", type=int, default=2,
                    help="quadrature degree in the creation process of the coefficient field")
parser.add_argument("-rank_always", "--rank_always", action="store_true",
                    help="switch to increase the tt rank in every iteration step.")
parser.add_argument("-new_hdeg", "--new_hdeg", type=int, default=2,
                    help="new dimension to take into account every time we add a stochastic dimension")

# endregion
# region Observation parameter
parser.add_argument("-ns", "--n_samples_sqrt", type=int, default=3,
                    help="# of sample points in physical domain. (ns --> ns^2 points)")
parser.add_argument("-op", "--obs_precision", type=float, default=0.0,
                    help="precision used in TT.round in the observation process")
parser.add_argument("-or", "--obs_rank", type=int, default=500,
                    help="maximal rank used in TT.round in the observation process")
# endregion
# region Inner Product parameter
parser.add_argument("-ip", "--inp_precision", type=float, default=0.0,
                    help="precision used in TT.round in the calculation of the inner product")
parser.add_argument("-ir", "--inp_rank", type=int, default=500,
                    help="maximal rank used in TT.round in the calculation of the inner product")
parser.add_argument("-ig", "--inp_gamma", type=float, default=1e-3,
                    help="gamma. constant value of covariance operator")
# endregion
# region Exponential calculation parameter
parser.add_argument("-ep", "--euler_precision", type=float, default=0.0,
                    help="precision used in TT.round in the calculation of the exponential")
parser.add_argument("-er", "--euler_rank", type=int, default=50,
                    help="maximal rank used in TT.round in the calculation of the exponential")
parser.add_argument("-els", "--euler_local_mc_samples", type=int, default=1,
                    help="# of MC samples to check the convergence of each euler step (highly optional)")
parser.add_argument("-egs", "--euler_global_mc_samples", type=int, default=1000,
                    help="# of MC samples to check the convergence of the resulting exponential (highly optional)")
parser.add_argument("-erep", "--euler_repetitions", type=int, default=0,
                    help="# of repetitions of the euler procedure if the desired accuracy is not reached (only euler)")
parser.add_argument("-es", "--euler_steps", type=int, default=1000,
                    help="# of steps done in the iterative process. For adaptive only start value.")
parser.add_argument("-egp", "--euler_global_precision", type=float, default=1e-10,
                    help="desired accuracy to reach in the exponential approximation")
parser.add_argument("-eui", "--euler_use_implicit", action="store_true",
                    help="switch to use the implicit euler scheme for more stability.")
parser.add_argument("-rm", "--runge_method", type=str, default="forth", choices=["linear", "square", "forth"],
                    help="used embedded runge kutta method")
parser.add_argument("-rn", "--runge_min_stepsize", type=float, default=1e-19,
                    help="minimal step size to check in adaptive step size control")
parser.add_argument("-rx", "--runge_max_stepsize", type=float, default=1-1e-10,
                    help="maximal step size to check in adaptive step size control")
# endregion
# region Density parameter
parser.add_argument("-ng", "--n_eval_grid", type=int, default=50,
                    help="# of evaluation points to estimate densities (for plots only)")
# endregion
# region Stochastic collocation parameter
parser.add_argument("-ny", "--Ny", type=int, default=10,
                    help="# of collocation points in one stochastic dimension")
# endregion
# region File saving paths
parser.add_argument("-fp", "--file_path", type=str, default="results/paper/mean/",
                    help="path to store plots and results")
parser.add_argument("-bp", "--bayes_path", type=str, default="results/bayes/",
                    help="path to store the results of the bayesian procedure")
parser.add_argument("-pp", "--plot_path", type=str, default="results/bayes/",
                    help="path to store the plots of the bayesian procedure. incl the .dat for latex plots")
# endregion

# region special mean convergence parameter
parser.add_argument("-zmc", "--z_mc_samples", type=int, default=100,
                    help="# of mc_samples for the TT-Mean convergence")
parser.add_argument("-cpu", "--n_cpu", type=int, default=40,
                    help="# CPUs used for parallel computation")
parser.add_argument("-sol", "--sample_sol", action="store_true",
                    help="switch to use the solution sampling with -zmc samples.")
parser.add_argument("-coef", "--sample_coef", action="store_true",
                    help="switch to use the coefficient field sampling with -zmc samples.")
parser.add_argument("-res", "--sample_res", action="store_true",
                    help="switch to use the residual sampling with -zmc samples.")
parser.add_argument("-mean", "--sample_mean", action="store_true",
                    help="switch to use the mean sampling with -zmc samples.")
parser.add_argument("-post", "--sample_post_mean", action="store_true",
                    help="switch to use the mean sampling of the solution w.r.t to the posterior with -zmc samples.")
parser.add_argument("-dens", "--sample_density", action="store_true",
                    help="switch to use the density interpolation with -ng grid points.")
parser.add_argument("-set", "--export_setting", action="store_true",
                    help="switch to export mesh and gpcdegrees")
parser.add_argument("-plot", "--do_plot", action="store_true",
                    help="switch to create plots and store them in the plot directory")
# endregion

parser.add_argument("-log", "--log", action="store_true",
                    help="switch to use the lognormal sgfem forward operator")

args = parser.parse_args()
print("========== PARAMETERS: {}".format(vars(args)))
# endregion
# region Store Parser Arguments
#   region Parameters to use in stochastic Galerkin for the forward solution operator
max_rank = args.max_rank                            # start ranks
sol_rank = args.sol_rank                            # maximal # of gpcdegrees
max_dofs = args.max_dofs                            # desired maximum of dofs to reach in refinement process
mesh_refine = args.mesh_refine                      # number of mesh refinements beforehand
iterations = args.iterations                        # used iterations in tt ALS
domain = args.domain                                # used domain, square, L-Shape, ...
femdegree = args.femdegree                          # used degree of finite element approach
coeffield_type = args.coef_type                # used coefficient field type
decay_exp_rate = args.decay_exp_rate                # decay rate for non constant coefficient fields
field_mean = args.field_mean                        # mean value of the coefficient field
sgfem_gamma = args.sgfem_gamma                      # Gamma used in adaptive SG FEM
init_mesh_dofs = args.init_mesh_dofs                # initial mesh refinement. -1 : ignore
theta = args.theta                                  # Gaussian measure weight < 1
n_coefficients = args.n_coefficients                # number of coefficients (aka M)
rho = args.rho                                      # gaussian measure weight > 1
freq_skip = args.freq_skip                          # frequency skip in gpce
freq_scale = args.freq_scale                        # frequency scaling in gpce
scale = args.scale                                  # scaling of the whole solution
hermite_degree = args.hermite_degree                # degree of the hermite polynomials
gpc_degree = args.gpc_degree                        # gpc degree
gpc_degree = hermite_degree                         # !!! There is no differentiation anymore. Ask Max
als_tol = args.als_tol                              # tolerance of the ALS algorithm
coef_acc = args.coef_tol                            # tolerance of tensor rounding in coefficient approximation
adaptive_iterations = args.adapt_it                 # number of iterations in the adaptive process
theta_x = args.theta_x                              # relation of physical refinement
theta_y = args.theta_y                              # relation of stochastic refinement
start_rank = args.start_rank
num_coeff_in_coeff = args.num_coeff_in_coeff        # number of coefficients in the coefficient expansion
max_hdegs_coef = args.max_hdegs_coef                # maximal hermite degree in the coefficient expansion
eta_zeta_weight = args.eta_zeta_weight              # weight of deterministic and stochastic residual
resnorm_zeta_weight = args.resnorm_zeta_weight      # weight of residual norm of ALS and stochastic residual
coef_mesh_dofs = args.coef_mesh_dofs                # degrees of freedom in the coefficient representation
coef_quad_degree = args.coef_quad_degree            # quadrature degree to use in coefficient expansion
rank_always = args.rank_always                      # switch if the rank of the solution should be increased always
new_hdeg = args.new_hdeg                            # new hermite degree added to the solution if necessary
#   endregion
# region Observation operator parameters

n_samples_sqrt = args.n_samples_sqrt                # number of nodes squared
sample_coordinates = [[i/int(n_samples_sqrt+1), j/int(n_samples_sqrt+1)]
                      for i in range(1, n_samples_sqrt+1) for j in range(1, n_samples_sqrt+1)]
print("sample_coordinates: {}".format(sample_coordinates))

obs_precision = args.obs_precision                  # precision used for rounding in the observation operator
obs_rank = args.obs_rank                            # maximal rank to round to in the observation operator
# endregion
# region Inner Product parameter
inp_precision = args.inp_precision                  # precision used in the calculation of the inner product
inp_rank = args.inp_rank                            # maximal rank used in the calculation of the inner product
inp_gamma = args.inp_gamma                          # covariance constant value
# endregion
# region Euler Parameter
euler_precision = args.euler_precision              # precision to use in euler rounding
euler_rank = args.euler_rank                        # rank to round down in euler method
#                                                   # samples in every euler step
euler_local_mc_samples = args.euler_local_mc_samples
#                                                   # samples for the final result
euler_global_mc_samples = args.euler_global_mc_samples
euler_repetitions = args.euler_repetitions          # number of repetitions with half step size
euler_steps = args.euler_steps                      # start euler steps
#                                                   # global precision to reach after the euler scheme
euler_global_precision = args.euler_global_precision
euler_use_implicit = args.euler_use_implicit        # switch to use the implicit euler method
runge_method = args.runge_method                    # name of the runge kutta method to use if to used method explicit
runge_min_stepsize = args.runge_min_stepsize        # minimal step size to check in adaptive step size control setup
runge_max_stepsize = args.runge_max_stepsize        # maximal step size to check in adaptive step size control setup
if euler_use_implicit:
    euler_method = ImplicitEuler(euler_steps, euler_precision, euler_rank, euler_global_mc_samples, euler_repetitions,
                                 euler_local_mc_samples)
else:
    if True:
        euler_method = RankAdaptiveEmbeddedRungeKutta(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
                                                      euler_repetitions, euler_local_mc_samples)
        euler_method.method = runge_method
        euler_method.acc = euler_global_precision
        euler_method.min_stepsize = runge_min_stepsize
        euler_method.max_stepsize = runge_max_stepsize
        euler_method.start_rank = 6
        euler_method.max_rank = 30
        euler_method.rounding_safety = 10.0
    else:
        euler_method = EmbeddedRungeKutta(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
                                          euler_repetitions, euler_local_mc_samples)
        euler_method.method = runge_method
        euler_method.acc = euler_global_precision
        euler_method.min_stepsize = runge_min_stepsize
        euler_method.max_stepsize = runge_max_stepsize
# endregion
# region Density parameters
n_eval_grid = args.n_eval_grid                      # number of evaluation points for the density plots
eval_grid_points = np.linspace(-1, 1, n_eval_grid)  # grid for density plots
# endregion
# region setup collocation
Ny = args.Ny                                        # # of collocation points in each stochastic dimension
#                                                   # one dimensional stochastic grid of Chebyshev nodes
# stochastic_grid = np.array(get_cheb_points([Ny])[0])
# endregion

# region Logging
logging.config.fileConfig('logging.conf')           # load logger configuration
log = logging.getLogger('test_bayes')               # get new logger object
# endregion
# region Path check and creation
file_path = args.file_path
try:
    if not os.path.isdir(file_path):
        os.mkdir(file_path)
except OSError:
    log.error("can not create file path: {}".format(file_path))
    exit()

bayes_path = args.bayes_path
try:
    if not os.path.isdir(bayes_path):
        os.mkdir(bayes_path)
except OSError:
    log.error("can not create bayes path: {}".format(bayes_path))
    exit()

plot_path = args.plot_path
try:
    if not os.path.isdir(plot_path):
        os.mkdir(plot_path)
except OSError:
    log.error("Can not create plot path: {}".format(plot_path))
    exit()
# endregion
# region special mean convergence parameter
z_mc_samples = args.z_mc_samples
n_cpu = args.n_cpu
sample_sol = args.sample_sol
sample_coef = args.sample_coef
sample_res = args.sample_res
sample_mean = args.sample_mean
sample_post_mean = args.sample_post_mean
sample_density = args.sample_density
export_setting = args.export_setting
do_plot = args.do_plot
# endregion
log_normal = args.log
# endregion
# endregion

# region Object Initialisation
bayes_obj = None
coeff_field = None
proj_basis = None
true_values = None
prior_densities = None

solution_error_h1 = None
solution_error_l2 = None
solution_error_h1_rel = None
solution_error_l2_rel = None
resnorm_list = None
eta_list = None
rhs_residual_list = None
zero_residual_list = None
sq_residual_list = None
jump_residual_list = None
zeta_list = None
overall_estimator_list = None
refine_list = None

bayes_obj_list = []                                 # list of Bayes objects due to multiple refinements in forward sol
bayes_fem_list = []
solution_error_h1_fem = []
solution_error_l2_fem = []
solution_error_h1_rel_fem = []
solution_error_l2_rel_fem = []
solution_tt_dofs_fem = []
solution_refinement_fem = []
dim_list = []
rank_list = []

mean_error_h1_fem = []
mean_error_h1_rel_fem = []
mean_error_l2_fem = []
mean_error_l2_rel_fem = []
mean_tt_dofs_fem = []
mean_refinements_fem = []

file_list_x = []
file_list_y = []
file_list_Z = []

poly_degree_p1 = []
poly_degree_p2 = []
poly_degree_p3 = []

fem_degrees = [1]
mesh_refines = [0]  # , 1, 2, 3]
force_recalculation = False
used_item = -1

# mc_runs = 10
# endregion

# region Dummy Class for caching


class CacheItem(object):
    pass

# endregion

# region Create Bayes Object (solve or load forward solution and solve or load inverse solution)
for lib, _mesh_refine in enumerate(mesh_refines):
    z_mc_sample_list = []                           # list of mc samples
    x_achses = []                                   # x-achses list
    dofs = []                                       # we use the coordinate sampling
    stochastic_grid = []                            # !!!!! needs replacement with GaussHermite
    # region Init Bayes Object
    bayes_obj = LognormalSGFEMBayes.init_solver(dofs, sample_coordinates, true_values, obs_precision,
                                                obs_rank, inp_precision, inp_rank, inp_gamma, euler_method,
                                                euler_global_precision, eval_grid_points, prior_densities,
                                                stochastic_grid, maxrank=max_rank, n_coeff=n_coefficients,
                                                femdegree=femdegree, gpcdegree=gpc_degree, decayexp=decay_exp_rate,
                                                gamma=sgfem_gamma, domain=domain, mesh_refine=mesh_refine,
                                                mesh_dofs=init_mesh_dofs, sol_rank=sol_rank, freq_skip=freq_skip,
                                                freq_scale=freq_scale, scale=scale, theta=theta, rho=rho,
                                                hermite_degree=hermite_degree, als_iterations=iterations,
                                                convergence=als_tol, coeffield_type=coeffield_type,
                                                _print=False, field_mean=field_mean,
                                                force_recalculation=force_recalculation, coef_acc=coef_acc,
                                                iterations=adaptive_iterations, theta_x=theta_x, theta_y=theta_y,
                                                start_rank=start_rank, num_coeff_in_coeff=num_coeff_in_coeff,
                                                max_hdegs_coef=max_hdegs_coef, eta_zeta_weight=eta_zeta_weight,
                                                resnorm_zeta_weight=resnorm_zeta_weight,
                                                coef_mesh_dofs=coef_mesh_dofs, coef_quad_degree=coef_quad_degree,
                                                rank_always=rank_always, max_dofs=max_dofs, new_hdeg=new_hdeg
                                                )

    cache = CacheItem()
    eval_string_cache = CacheItem()
    eval_string_cache.str = None
    eval_string_cache.M = None
    eval_string_cache.S = None
    # endregion

    if bayes_obj.import_bayes(bayes_path):          # bayes object was found an can be loaded
        bayes_obj_list.append(copy.copy(bayes_obj))
    else:
        # region Create new Bayes Object and calculate all the steps
        # region Setup truth
        if False:
            if not log:
                log.info("Create {} true parameters".format(bayes_obj.forwardOp.coefficients))
                true_values = create_parameter_samples(bayes_obj.forwardOp.coefficients)
            else:
                log.info("Create {} true parameters".format(bayes_obj.forwardOp.n_coeff))
                true_values = create_parameter_samples(bayes_obj.forwardOp.n_coeff, lognormal=True, mu=0, sigma=1)
        else:
            true_values = [0]*bayes_obj.forwardOp.n_coeff
        bayes_obj.true_values = true_values
        # endregion
        # region Setup Gaussian Prior
        Gauss_prior = False
        if log:
            # !!!!!!!!!!!!!!!!!!!!!!!!!!!!
            prior_densities = [lambda _x: 0.5 * np.ones_like(_x)] * bayes_obj.forwardOp.n_coeff
        else:
            prior_densities = [lambda _x: 0.5 * np.ones_like(_x)] * bayes_obj.forwardOp.coefficients

        bayes_obj.prior_densities = prior_densities
        # endregion
        # region Setup noisy measurements
        log.info("Create {} measurements".format(len(sample_coordinates)))
        if False:
            measurements = np.array([bayes_obj.forwardOp.compute_direct_sample_solution(true_values,
                                                                                        cache=eval_string_cache,
                                                                                        reference_m=len(true_values))
                                     (sample_coordinate)
                                     for sample_coordinate in sample_coordinates])
        else:
            measurements = [0]*len(sample_coordinates)
        log.info("  add noise to the measurements")
        for lic in range(len(measurements)):
            measurements[lic] += np.random.randn(1, 1)[0, 0] * inp_gamma

        bayes_obj.measurements = measurements
        # endregion
        print("start calculation for SOL[{}] / {} with mesh_refine {}".format(lib+1, len(mesh_refines), mesh_refine))
        if sample_sol or sample_coef or sample_res:
            bayes_obj.calculate_densities(stop_at_observation=True)
        else:
            bayes_obj.calculate_densities(stop_at_observation=False)
        bayes_obj_list.append(copy.copy(bayes_obj))
        log.info('export Bayes file %s', bayes_path + bayes_obj.file_name + '.dat')
        if not sample_sol:
            if not bayes_obj.export_bayes(bayes_path):
                log.error("can not save Bayes object")
        print("mr{} tt dofs of solution: {}".format(mesh_refine, bayes_obj.forwardOp.SOL[-1]["DOFS"]))
        print("mr{} solution.n: {}".format(mesh_refine, bayes_obj.forwardOp.SOL[-1]["V"].n))
        print("mr{} solution.r: {}".format(mesh_refine, bayes_obj.forwardOp.SOL[-1]["V"].r))
        # print("mr{} coef_field.n: {}".format(mesh_refine, bayes_obj.forwardOp.SOL[-1]["coeff_field"].n))
        # print("mr{} coef_field.r: {}".format(mesh_refine, bayes_obj.forwardOp.SOL[-1]["coeff_field"].r))
        print("mr{} coef_cores.n: {}".format(mesh_refine, tt.vector.from_list(
            bayes_obj.forwardOp.SOL[-1]["coeff_cores"]).n))
        print("mr{} coef_cores.r: {}".format(mesh_refine, tt.vector.from_list(
            bayes_obj.forwardOp.SOL[-1]["coeff_cores"]).r))
        print("mr{} cont_coef_cores.n: {}".format(mesh_refine, tt.vector.from_list(
            bayes_obj.forwardOp.SOL[-1]["cont_coeff_cores"]).n))
        print("mr{} cont_coef_cores.r: {}".format(mesh_refine, tt.vector.from_list(
            bayes_obj.forwardOp.SOL[-1]["cont_coeff_cores"]).r))
        print("mr{} rhs.n: {}".format(mesh_refine, bayes_obj.forwardOp.SOL[-1]["rhs"].n))
        # endregion
# endregion

# region sample coefficient field
if sample_coef:
    ph = None
    if do_plot:
        ph = PlotHelper()

    err_list = []
    err_rel_list = []
    tt_dof_list = []
    assert (len(bayes_obj_list) == len(mesh_refines))
    for lia, bayes_obj in enumerate(bayes_obj_list):
        err = 0
        err_rel = 0
        nor = 0
        err2 = 0
        nor2 = 0
        # region receive coefficient field from solution object
        #                                           # old coefficient tensor by sampling at midpoints
        coef_ten = bayes_obj.forwardOp.SOL[-1]["coeff_field"]
        #                                           # cores of the fully discrete coefficient field
        coef_cores = tt.vector.from_list(bayes_obj.forwardOp.SOL[-1]["coeff_cores"])
        #                                           # cores of the semi-discrete coefficient field (cont. in space)
        cont_coef_cores = tt.vector.from_list(bayes_obj.forwardOp.SOL[-1]["cont_coeff_cores"])
        # endregion
        # region Create Mesh from midpoints

        #                                           # create midpoint list since we interpolate onto an DG 0 mesh
        midpoints = np.array([(x, y) for (x, y) in zip(bayes_obj.forwardOp.SOL[-1]["mesh_midpoint"][:, 0],
                                                       bayes_obj.forwardOp.SOL[-1]["mesh_midpoint"][:,
                                                                                                    1])])

        # endregion

        # region create mass matrix to compute l2 norm
        V = FunctionSpace(bayes_obj.forwardOp.SOL[-1]["V"].mesh, "DG", 0)
        _u = TrialFunction(V)
        _v = TestFunction(V)
        BF = inner(_u, _v) * dx(bayes_obj.forwardOp.SOL[-1]["V"].mesh)
        parameters['linear_algebra_backend'] = 'Eigen'
        M = assemble(BF)
        M = as_backend_type(M)
        # eigen, _ = np.linalg.eig(M.array())
        # endregion

        print(" Sample coefficient field with {} samples".format(z_mc_samples))
        print("   coef_ten.n= {}".format(coef_ten.n))
        print("   coef_ten.r= {}".format(coef_ten.r))
        print("   coef_cores.n= {}".format(coef_cores.n))
        print("   coef_cores.r= {}".format(coef_cores.r))
        print("   cont_coef_cores.n= {}".format(cont_coef_cores.n))
        print("   cont_coef_cores.r= {}".format(cont_coef_cores.r))
        coef_cores_list = tt.vector.to_list(cont_coef_cores)
        ranks = [1] + list(cont_coef_cores.r)

        bmax = bayes_obj.forwardOp.SOL[-1]["bmax"]
        # bmax = [1]*n_coefficients
        if n_cpu > 1 and not do_plot and False:
            # region parallel sampling of the coefficient field. PETSC error
            print("Sample {} evaluations parallel".format(z_mc_samples))
            from joblib import delayed, Parallel
            buffer_list = []
            y_samples_list = [np.array(np.random.randn(n_coefficients)) for _ in range(z_mc_samples)]

            # region Sample coefficient parallel

            def sample_coefficient_parallel(_y_samples):

                _af = AffineField("EF-square-cos", amptype=amp_type, decayexp=decay_exp_rate, gamma=sgfem_gamma,
                                  freqscale=freq_scale, freqskip=freq_skip, scale=scale)
                _coef = sample_cont_coeff(tt.vector.to_list(coef_cores)[1:], _af, midpoints, _y_samples,
                                          ranks, coef_cores.n[1:],
                                          bmax=bmax, theta=theta, rho=rho)

                _field = np.exp(_af(midpoints, _y_samples))
                return np.linalg.norm(_field, ord=2), np.linalg.norm(_coef - _field, ord=2)

            # endregion
            buffer_list = (Parallel(n_cpu)(delayed(sample_coefficient_parallel)(xi) for xi in y_samples_list))
            buff = np.sum(buffer_list, axis=0)
            nor = buff[0]
            err = buff[1]
            # endregion
        else:
            # region sampling of the coefficient field
            for lic in range(z_mc_samples):
                if lic % 10 == 0:
                    print("     sample {}/{}".format(lic, z_mc_samples))
                # if do_plot:
                #     # noinspection PyProtectedMember
                #     ph["sol {} ref {}".format(lic+1, lia+1)].plot(sol._fefunc)

                # coef_obj.SOL[-1]["V"] = bayes_obj.forwardOp.SOL[-1]["coeff_field"]
                # coef = compute_parametric_sample_solution_TT([list(y_samples)], coef_ten, None, 1, no_proj=True)[0]

                af = AffineField("EF-square-cos", amptype=amp_type, decayexp=decay_exp_rate, gamma=sgfem_gamma,
                                 freqscale=freq_scale, freqskip=freq_skip, scale=scale)

                y_samples = 25*af.sample_rvs(25)
                # print("samples: {}".format(y_samples))
                # print("gpcdegree= {}".format())

                # sol = bayes_obj.forwardOp.sample_at_coeff(y_samples[:n_coefficients])
                coef = sample_cont_coeff(coef_cores_list, af, midpoints, y_samples[:n_coefficients],
                                         ranks, cont_coef_cores.n, bmax=bmax, theta=theta, rho=rho)

                # if do_plot:
                #   # fig = plt.figure(lic)
                #    # ax = fig.gca(projection='3d')
                #    # ax.set_zlim(0, 5)
                #    # surf = ax.plot_trisurf(midpoints[:, 0], midpoints[:, 1], coef)

                field = np.exp(af(midpoints, y_samples[:n_coefficients]))

                # if do_plot:
                #     #fig = plt.figure(lic+3)
                #     #ax = fig.gca(projection='3d')
                #     #ax.set_zlim(0, 5)
                #     #surf = ax.plot_trisurf(midpoints[:, 0], midpoints[:, 1], field)
                # print bayes_obj.forwardOp.SOL[-1]["V"].mesh
                coef_function = Function(V)
                coef_function.vector()[:] = coef
                coef_function_exact = Function(V)
                coef_function_exact.vector()[:] = field
                if do_plot:
                    # noinspection PyProtectedMember
                    ph["coef {} ref {}".format(lic+1, lia+1)].plot(coef_function)
                    if lia == len(bayes_obj_list)-1:
                        ph["coef {} ref {} exact".format(lic+1, lia+1)].plot(coef_function_exact, interactive=True)
                    else:
                        ph["coef {} ref {} exact".format(lic + 1, lia + 1)].plot(coef_function_exact)
                # from alea.utils.tictoc import TicToc
                # with TicToc(key="**** Calculate by hand ****", active=True, do_print=True):
                err += np.sqrt(np.dot(np.dot(np.array(coef-field), M.array()), np.array(coef-field)))
                nor += np.sqrt(np.dot(np.dot(np.array((coef-field)/field), M.array()), np.array((coef - field)/field)))
                # print err, nor
                # print("Robert err: {}".format(np.sqrt(np.dot(np.dot(np.array(coef-field), M.array()),
                # (np.array(coef-field))))))
                # print("Max err: {}".format(np.sqrt(eigen[0]) * np.linalg.norm(coef-field)))
                # print("Manuel err: {}".format(errornorm(coef_function_exact, coef_function, norm_type='l2')))
                # set_log_level(ERROR)                # ignore sufficiency of the DG 0 mesh
                #                                   # calculate coefficient field error
                # with TicToc(key="**** Let fenics calculate ****", active=True, do_print=True):
                #     err2 += errornorm(coef_function_exact, coef_function, norm_type='l2')
                #                                         # calculate relative coefficient field error
                #     nor2 += errornorm(coef_function_exact, coef_function, norm_type='l2') / \
                #          norm(coef_function_exact, norm_type='l2')
                # print err2, nor2
                # set_log_level(WARNING)              # activate WARNINGS again

            # endregion

        err_list.append(err * z_mc_samples**(-1))   # save MC sampling error
        err_rel_list.append(nor * z_mc_samples**(-1))
        tt_dof_list.append(tt_dofs(coef_ten))       # store number of tt dofs of the coefficient field tensor

    assert len(mesh_refines) == len(tt_dof_list)
    assert len(mesh_refines) == len(err_list)
    assert len(mesh_refines) == len(err_rel_list)
    with open(plot_path + "coef_error_decayinf_R{}_M{}_hd{}.dat".format(max_rank, n_coefficients, hermite_degree),
              "w") as f:
        f.write("========== PARAMETERS: {} \n reference coefficients = 25 \n".format(vars(args)))
        f.write("refine, tt_dof, err, err_rel\n")
        for lic, mesh_refine in enumerate(mesh_refines):
            f.write(repr(mesh_refine) + "," +
                    repr(tt_dof_list[lic]) + "," +
                    repr(err_list[lic]) + "," +
                    repr(err_rel_list[lic]) + "\n")
    if True:
        exit()

# endregion

# region Sample current forward solution error
if sample_sol:
    solution_error_h1 = []
    solution_error_l2 = []
    solution_error_h1_rel = []
    solution_error_l2_rel = []
    resnorm_list = []
    eta_list = []
    rhs_residual_list = []
    zero_residual_list = []
    sq_residual_list = []
    jump_residual_list = []
    zeta_list = []
    refine_list = []
    overall_estimator_list = []
    tt_dofs = []
    dim_list = []
    rank_list = []

    ref_pde = bayes_obj_list[-1].forwardOp.get_pde
    ref_coef_field = bayes_obj_list[-1].forwardOp.get_coefficient_field
    bayes_obj_list[-1].forwardOp.refine_function_space(-1, dofs=5e4)
    ref_proj_basis = bayes_obj_list[-1].forwardOp.get_proj_basis
    assert len(mesh_refines) == len(bayes_obj_list)

    reference_m = 100
    eval_string_cache = CacheItem()
    eval_string_cache.str = None
    eval_string_cache.M = None
    eval_string_cache.S = None
    for lib in range(len(bayes_obj_list[-1].forwardOp.SOL)):
    # for lib in range(13):
        print("sample SOL[{}] / {} using {} samples".format(lib+1, len(bayes_obj_list[-1].forwardOp.SOL), z_mc_samples))
        print("  solution.n {}".format(bayes_obj_list[-1].forwardOp.SOL[lib]["V"].n))
        dim_list.append(bayes_obj_list[-1].forwardOp.SOL[lib]["V"].n)
        print("  solution.r {}".format(bayes_obj_list[-1].forwardOp.SOL[lib]["V"].r))
        rank_list.append(bayes_obj_list[-1].forwardOp.SOL[lib]["V"].r)
        refine_list.append(lib)
        tt_dofs.append(bayes_obj_list[-1].forwardOp.SOL[lib]["DOFS"])
        # print("SOL.dofs: {}".format(bayes_obj.forwardOp.SOL[lic]["DOFS"]))
        # print("gpcps={}".format(bayes_obj_list[-1].forwardOp.SOL[-1]["gpcps"]))
        # print("gpcps[0]={}".format(bayes_obj_list[-1].forwardOp.SOL[-1]["gpcps"][0]))
        # print("V.n = {}".format(bayes_obj_list[-1].forwardOp.SOL[-1]["V"].n))
        xi_samples = np.array([np.random.randn(reference_m) for
                               _ in range(z_mc_samples)])
        # xi_samples = np.array([[0, 0, 0]])
        _solution_error_l2, _solution_error_h1 = \
            bayes_obj_list[-1].forwardOp.estimate_sampling_error(pde=ref_pde, coef_field=ref_coef_field,
                                                                 proj_basis=ref_proj_basis, relative=False,
                                                                 n_xi_samples=z_mc_samples, xi_samples=xi_samples,
                                                                 sol_index=lib, reference_m=reference_m,
                                                                 cache=eval_string_cache)
        _solution_error_l2_rel, _solution_error_h1_rel = [0]*len(_solution_error_l2), [0]*len(_solution_error_h1)
            # bayes_obj_list[-1].forwardOp.estimate_sampling_error(pde=ref_pde, coef_field=ref_coef_field,
            #                                                      proj_basis=ref_proj_basis, relative=True,
            #                                                      sol_index=lib, n_xi_samples=z_mc_samples,
            #                                                      xi_samples=xi_samples, reference_m=reference_m,
            #                                                      cache=eval_string_cache)

        __solution_error_l2 = 0
        __solution_error_h1 = 0
        __solution_error_l2_rel = 0
        __solution_error_h1_rel = 0
        for lix in range(len(_solution_error_h1)):
            __solution_error_l2 += _solution_error_l2[lix]
            __solution_error_h1 += _solution_error_h1[lix]
            __solution_error_l2_rel += _solution_error_l2_rel[lix]
            __solution_error_h1_rel += _solution_error_h1_rel[lix]
        __solution_error_l2 /= len(_solution_error_l2)
        __solution_error_h1 /= len(_solution_error_h1)
        __solution_error_l2_rel /= len(_solution_error_l2_rel)
        __solution_error_h1_rel /= len(_solution_error_h1_rel)
        solution_error_l2.append(__solution_error_l2)
        solution_error_h1.append(__solution_error_h1)
        solution_error_l2_rel.append(__solution_error_l2_rel)
        solution_error_h1_rel.append(__solution_error_h1_rel)
        eta_list.append(bayes_obj_list[-1].forwardOp.SOL[lib]["eta_global"])
        rhs_residual_list.append(bayes_obj_list[-1].forwardOp.SOL[lib]["rhs_global"])
        zero_residual_list.append(bayes_obj_list[-1].forwardOp.SOL[lib]["zero_global"])
        sq_residual_list.append(bayes_obj_list[-1].forwardOp.SOL[lib]["sq_global"])
        jump_residual_list.append(bayes_obj_list[-1].forwardOp.SOL[lib]["jump_global"])
        resnorm_list.append(bayes_obj_list[-1].forwardOp.SOL[lib]["resnorm"])
        zeta_list.append(bayes_obj_list[-1].forwardOp.SOL[lib]["zeta_global"])
        overall_estimator_list.append(bayes_obj_list[-1].forwardOp.SOL[lib]["overall-estimator"])
# endregion

# region Export solution error to file
if sample_sol:
    # assert len(solution_refinement_fem) == 3
    # assert len(solution_tt_dofs_fem) == 3
    # assert len(solution_error_l2_rel_fem) == 3
    # assert len(solution_error_l2_fem) == 3
    # assert len(solution_error_h1_rel_fem) == 3
    # assert len(solution_error_h1_fem) == 3

    assert len(refine_list) == len(tt_dofs)
    assert len(refine_list) == len(solution_error_l2)
    assert len(refine_list) == len(solution_error_h1)
    assert len(refine_list) == len(solution_error_l2_rel)
    assert len(refine_list) == len(solution_error_h1_rel)
    assert len(refine_list) == len(overall_estimator_list)
    assert len(refine_list) == len(eta_list)
    assert len(refine_list) == len(zeta_list)
    assert len(refine_list) == len(resnorm_list)

    with open(plot_path + "thx{}_thy{}_adaptive_P{}_{}_scale{}_ezw{}_rzw{}_sol_error.dat".format(theta_x, theta_y,
                                                                                                 femdegree, domain,
                                                                                                 scale, eta_zeta_weight,
                                                                                                 resnorm_zeta_weight),
              "w") as f:
        f.write("refine, tt_dof, L2, H1, L2_rel, H1_rel, resnorm, eta, zeta, overall, eta_rhs, eta_zero, eta_sq, "
                "eta_jump\n")
        for lic in range(len(refine_list)):
            f.write(repr(refine_list[lic]) + "," +
                    repr(tt_dofs[lic]) + "," +
                    repr(solution_error_l2[lic]) + "," +
                    repr(solution_error_h1[lic]) + "," +
                    repr(solution_error_l2_rel[lic]) + "," +
                    repr(solution_error_h1_rel[lic]) + "," +
                    repr(resnorm_list[lic]) + "," +
                    repr(eta_list[lic]) + "," +
                    repr(zeta_list[lic]) + "," +
                    repr(overall_estimator_list[lic]) + "," +
                    repr(rhs_residual_list[lic]) + "," +
                    repr(zero_residual_list[lic]) + "," +
                    repr(sq_residual_list[lic]) + "," +
                    repr(jump_residual_list[lic]) + "\n"
                    )
    with open(plot_path + "thx{}_thy{}_adaptive_P{}_{}_scale{}_ezw{}_rzw{}_sol_spec.dat".format(theta_x, theta_y,
                                                                                                femdegree, domain,
                                                                                                scale, eta_zeta_weight,
                                                                                                resnorm_zeta_weight),
              "w") as f:
        for lic in range(len(refine_list)):
            f.write(repr(refine_list[lic]) + "," +
                    repr(dim_list[lic]) + "," +
                    repr(rank_list[lic]) + "\n"
                    )
        N_r = len(rank_list[-1])
        N_d = len(dim_list[-1])
        length_r = range(N_r)
        length_d = range(N_d)
        width = 0.2
        import matplotlib.pyplot as plt
        import matplotlib2tikz as tikz
        header_str = "\\documentclass{standalone} \\usepackage[utf8]{inputenc}" \
                     "\\usepackage{graphicx} \\usepackage{pgfplots} \\usetikzlibrary{shapes.arrows} " \
                     "\\usepgfplotslibrary{groupplots} \\pgfplotsset{compat=newest} \\newlength\\figwidth" \
                     "\\begin{document}"
        footer_str = "\\end{document}"
        for lic in range(len(refine_list)):
            fig, ax = plt.subplots()
            rect = ax.bar(length_r, list(rank_list[lic]) + [0]*(N_r - len(rank_list[lic])), width, color='g')
            ax.set_ylabel('ranks')
            ax.set_ylim([0.1, max_rank])
            ax.set_title('Rank distribution for step {}'.format(lic+1))
            ax.set_xticks(length_r)
            # ax.set_xticklabels(('G1', 'G2', 'G3', 'G4', 'G5'))
            code = tikz.get_tikz_code('test_rank{}.tex'.format(lic+1))
            plt.clf()
            file_path = plot_path + "ranks/" + "thx{}_thy{}_adaptive_P{}_{}_scale{}_rank{}.tex".format(theta_x, theta_y,
                                                                                                       femdegree,
                                                                                                       domain, scale,
                                                                                                       lic+1)
            with open(file_path, "w") as f2:
                f2.write(header_str + "\n" + code + "\n" + footer_str)
            fig, ax = plt.subplots()

            rect = ax.bar(length_d, list(dim_list[lic][1:]) + [0] * (N_d - len(dim_list[lic][1:])), width, color='g')
            ax.set_ylabel('dimensions')
            ax.set_ylim([0.1, max_hdegs_coef + 1])
            ax.set_title('Dimension distribution for step {}'.format(lic + 1))
            ax.set_xticks(length_d)
            # ax.set_xticklabels(('G1', 'G2', 'G3', 'G4', 'G5'))

            code = tikz.get_tikz_code('test_dim{}.tex'.format(lic+1))
            plt.clf()
            file_path = plot_path + "dimension/" + "thx{}_thy{}_adaptive_P{}_{}_scale{}_dim{}.tex".format(theta_x,
                                                                                                          theta_y,
                                                                                                          femdegree,
                                                                                                          domain, scale,
                                                                                                          lic + 1)
            with open(file_path, "w") as f2:
                f2.write(header_str + "\n" + code + "\n" + footer_str)
# endregion

# # region Plot results
# if do_plot:
#     # region estimate reference solution by taking the last computed element
#     def _sample_z_parallel(xi):
#         sol_buffer = bayes_fem_list[-1][-1].forwardOp.compute_direct_sample_solution(list(xi[1:]), sol_index=-1)
#         sol_measurements = np.array([sol_buffer(loc_dof) for loc_dof in bayes_obj.sample_coordinates])
#         # print("     (O o G)(u) at {0} = {1}".format(bayes_obj.sample_coordinates, sol_measurements))
#         diff = np.array(bayes_obj.measurements) - np.array(sol_measurements)
#         # print("     given measurements: {}".format(bayes_obj.measurements))
#         _Z = np.exp(-0.5 * np.sum((diff ** 2) * (inp_gamma ** (-1))))
#         return _Z
#
#     xi_samples = np.array([np.random.rand(bayes_obj.forwardOp.coefficients + 1, 1)[:, 0] * 2 - 1 for __
#                            in range(z_mc_samples)])
#     for lix in range(len(xi_samples)):
#         xi_samples[lix][0] = 0
#
#     z_true = np.sum(np.array(Parallel(n_cpu)(delayed(_sample_z_parallel)(xi) for xi in xi_samples)))
#     z_true *= 0.5 ** (len(bayes_obj.solution.n) - 1)
#     z_true *= len(xi_samples) ** (-1)
#     # endregion
#
#     # region Plot error of Z
#     for lia in range(len(bayes_fem_list)):
#         print("Bayes FEM {} / {}".format(lia, len(bayes_fem_list)))
#         x_achses = []
#         z_error = []
#         for lic in range(len(bayes_fem_list[lia])):
#             print("  Bayes Obj {} / {}".format(lic, len(bayes_fem_list[lia])))
#
#             x_achses.append(bayes_fem_list[lia][lic].forwardOp.SOL[lic]["DOFS"])
#             z_error.append(np.abs(z_true - bayes_fem_list[lia][lic].mean))
#         plt.loglog(np.array(x_achses), np.array(z_error), label="P{}".format(lia))
#
#     plt.title("Development of the Mean sampled with MC vs our TT-Mean.")
#     plt.legend(ncol=3, loc=1)
#     plt.xlabel("TT-Dofs")
#     plt.ylabel("$\|Z-Z_h\|$")
#     plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "mean_error.png")
#     plt.clf()
#     # endregion
#
#     # region Plot solution errors against tt_dofs
#     fem_marker = ["o", "^", "d"]
#
#     for lia in range(3):                                # fem degrees P = 1,2,3
#         plt.semilogy(solution_refinement_fem[lia], np.array(solution_error_l2_fem[lia]), "b", marker=fem_marker[lia],
#                      label="$L^2$ error p{}" + str(lia+1))
#         plt.semilogy(solution_refinement_fem[lia], np.array(solution_error_h1_fem[lia]), "r", marker=fem_marker[lia],
#                      label="$H^1_0$ error p{}" + str(lia+1))
#     plt.title("Sampled ERROR of the forward solution.")
#     plt.legend(bbox_to_anchor=(0., -.202, 1., .102), loc=2,
#                ncol=3, mode="expand", borderaxespad=0.)
#     plt.xlabel("adaptive refinement step")
#     plt.ylabel("Sampled ERROR of the forward solution object")
#     plt.savefig(file_path + "sol_error_absolute_" + str(hash(bayes_obj.file_name)) + ".pdf",
#                 dpi=200, facecolor='w', edgecolor='w',
#                 orientation='portrait', papertype=None, format='pdf',
#                 transparent=False, bbox_inches='tight', pad_inches=0.1,
#                 frameon=None)
#     plt.clf()
#     for lia in range(3):                                # fem degrees P = 1,2,3
#         plt.loglog(solution_tt_dofs_fem[lia], np.array(solution_error_l2_fem[lia]), "b", marker=fem_marker[lia],
#                    label="$L^2$ error p{}".format(lia+1))
#         plt.loglog(solution_tt_dofs_fem[lia], np.array(solution_error_h1_fem[lia]), "r", marker=fem_marker[lia],
#                    label="$H^1_0$ error p{}".format(lia+1))
#     plt.title("Sampled ERROR of the forward solution.")
#     plt.legend(bbox_to_anchor=(0., -.202, 1., .102), loc=2,
#                ncol=3, mode="expand", borderaxespad=0.)
#     plt.xlabel("TT-dofs")
#     plt.ylabel("Sampled ERROR of the forward solution object")
#     plt.savefig(file_path + "sol_error_absolute_tt_" + str(hash(bayes_obj.file_name)) + ".pdf",
#                 dpi=200, facecolor='w', edgecolor='w',
#                 orientation='portrait', papertype=None, format='pdf',
#                 transparent=False, bbox_inches='tight', pad_inches=0.1,
#                 frameon=None)
#     plt.clf()
#     for lia in range(3):                                # fem degrees P = 1,2,3
#         plt.semilogy(solution_refinement_fem[lia], np.array(solution_error_l2_rel_fem[lia]), "b",
#                      marker=fem_marker[lia], label="$L^2$ error p{}".format(lia+1))
#         plt.semilogy(solution_refinement_fem[lia], np.array(solution_error_h1_rel_fem[lia]), "r",
#                      marker=fem_marker[lia], label="$H^1_0$ error p{}".format(lia+1))
#     plt.title("Sampled relative ERROR of the forward solution.")
#     plt.legend(bbox_to_anchor=(0., -.202, 1., .102), loc=2,
#                ncol=3, mode="expand", borderaxespad=0.)
#     plt.xlabel("adaptive refinement step")
#     plt.ylabel("Sampled relative ERROR of the forward solution object")
#     plt.savefig(file_path + "sol_error_relative_" + str(hash(bayes_obj.file_name)) + ".pdf",
#                 dpi=200, facecolor='w', edgecolor='w',
#                 orientation='portrait', papertype=None, format='pdf',
#                 transparent=False, bbox_inches='tight', pad_inches=0.1,
#                 frameon=None)
#     plt.clf()
#     for lia in range(3):                                # fem degrees P = 1,2,3
#         plt.loglog(solution_tt_dofs_fem[lia], np.array(solution_error_l2_rel_fem[lia]), "b", marker=fem_marker[lia],
#                    label="$L^2$ error p{}".format(lia+1))
#         plt.loglog(solution_tt_dofs_fem[lia], np.array(solution_error_h1_rel_fem[lia]), "r", marker=fem_marker[lia],
#                    label="$H^1_0$ error p{}".format(lia+1))
#     plt.title("Sampled relative ERROR of the forward solution.")
#     plt.legend(bbox_to_anchor=(0., -.202, 1., .102), loc=2,
#                ncol=3, mode="expand", borderaxespad=0.)
#     plt.xlabel("tt-dofs")
#     plt.ylabel("Sampled relative ERROR of the forward solution object")
#     plt.savefig(file_path + "sol_error_relative_tt_" + str(hash(bayes_obj.file_name)) + ".pdf",
#                 dpi=200, facecolor='w', edgecolor='w',
#                 orientation='portrait', papertype=None, format='pdf',
#                 transparent=False, bbox_inches='tight', pad_inches=0.1,
#                 frameon=None)
#     plt.clf()
#     # endregion
# # endregion

