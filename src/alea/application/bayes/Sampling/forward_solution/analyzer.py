"""
test script to sample the forward solution of the asgfem using the lognormal tensor train description
@author: Manuel Marschall
"""

# region imports
from __future__ import division

import argparse                                     # use command parameter parsing
from dolfin import FunctionSpace, refine, Function, File, errornorm, norm, UnitSquareMesh, set_log_level, ERROR, \
    project, cells, Mesh

import matplotlib.tri as tri
import matplotlib.pyplot as plt
import matplotlib2tikz as tikz
import tt
import numpy as np
from alea.utils.plothelper import plot_mpl
import os
import shutil
import errno
from alea.application.bayes.Bayes_util.ForwardOperator.tt_lognormal_asgfem_operator import TTLognormalAsgfemOperator
from alea.application.bayes.Bayes_util.ForwardOperator.tt_lognormal_asgfem_operator_data import  \
    TTLognormalAsgfemOperatorData
from alea.application.bayes.alea_util.coefficient_field.tensor.tt_lognormal_fullydiscrete_field import \
    TTLognormalFullyDiscreteField
from alea.application.bayes.alea_util.coefficient_field.tensor.tt_lognormal_semidiscrete_field import \
    read_coef_from_config
from alea.application.bayes.alea_util.coefficient_field.tensor.tt_lognormal_semidiscrete_field import \
    TTLognormalSemidiscreteField
from alea.application.bayes.Bayes_util.ForwardOperator.tt_parallel_worker import TTParallelWorker
from alea.application.bayes.Bayes_util.ForwardOperator.tt_forward_operator_util import compute_overall_error_indicator
from alea.application.bayes.alea_util.configuration.asgfem_config import AsgfemConfig
from alea.application.egsz.sample_domains import SampleDomain
from alea.application.egsz.sample_problems2 import SampleProblem

from alea.application.tt_asgfem.tensorsolver.tt_sgfem_util import tt_dofs
from alea.utils.tictoc import TicToc
from alea.utils.timing import get_current_time_string
from alea.utils.plothelper import write_2d_png, write_3d_png, write_pvd_file
from alea.application.bayes.Bayes_util.ForwardOperator.tt_forward_operator_util import calculate_stiffness_matrix, \
        calculate_mass_matrix, create_order4_stiffness, create_order4_volume_operator_list

from copy import deepcopy
import ttk as tk
from Tkinter import *
from alea.utils.plothelper import draw_figure
from tkFileDialog import askopenfilename


set_log_level(ERROR)
# endregion


# region global variables for multi function communication
# global bl_read_solution, bl_solution_frames_packed, bl_read_configuration, bl_curr_item_changed
# global read_solutions, loc_read_solutions, curr_item

bl_read_configuration = False
bl_solution_frames_packed = False
bl_coef_frames_packed = False
bl_read_solution = False
bl_curr_item_changed = False
bl_curr_coef_item_changed = False
bl_curr_coef_realisation_item_changed = False

read_solutions = []
loc_read_solutions = []
config = None

curr_item = None
curr_coef_item = None
curr_coef_realisation_item = None
curr_item_index = None
curr_coef_item_index = None
curr_coef_realisation_item_index = None

mesh_photo = None
err_plot_photo = None
est_plot_photo = None
res_plot_photo = None
coef_plot_photo = None
coef_cont_photo = None
coef_semi_photo = None
coef_disc_photo = None
coef_mean_cont_plot_photo = None
coef_mean_semi_plot_photo = None
coef_mean_disc_plot_photo = None
coef_var_cont_plot_photo = None
coef_var_semi_plot_photo = None
coef_var_disc_plot_photo = None

canvas_x_padding = 5
canvas_y_padding = 5
canvas_photo_width = 380
canvas_photo_height = 310
canvas_figure_width = 4.0
canvas_figure_height = 3.0

problem_file = "/Home/optimier/marschall/Projects/alea-testing/src/alea/application/bayes" \
               "/alea_util/configuration/problems/paper_lognormal_P3-sigma2-thy05-thx05.pro"
cache_file = "/Home/optimier/marschall/Projects/alea-testing/src/alea/application/bayes" \
             "/Bayes_util/ForwardOperator/tmp/P3-sigma2-ezw3.txt"
# endregion

root = Tk()

root.title("Adaptive stochastic Galerkin finite Element analyzer")
root.geometry("1910x950")

notebook = tk.Notebook(root)

pane_config = Frame(notebook)
pane_coef = Frame(notebook)
pane_main = Frame(notebook)

notebook.add(pane_main, text="main")
notebook.add(pane_coef, text="coefficient")
notebook.add(pane_config, text="config")


# region configuration pane
# region problem config
def read_configuration(sv):
    global bl_read_configuration, problem_file, config
    if bl_read_configuration is True:
        return True
    problem_file = sv.get()
    config_entry.config(background="white")
    with TicToc(sec_key="Configuration", key="read configuration", do_print=False, active=True):
        config = AsgfemConfig(problem_file)
        if config.read_configuration() is False:
            print("can not read configuration: {}".format(problem_file))
            config_entry.config(background="red")
        # print("configuration {} read".format(problem_file))
        else:
            bl_read_configuration = True
            config_entry.config(background="green")

tk.Label(pane_config, text="configuration: ").grid(row=0, column=0)
config_entry_var = StringVar()
config_entry_var.trace("w", lambda name, index, mode, sv=config_entry_var: read_configuration(sv))
config_entry = Entry(pane_config, textvariable=config_entry_var, width=100)
config_entry.grid(row=0, column=1)


def btnfun_read_config():
    config_entry.delete(0, END)
    name = askopenfilename(initialdir="/Home/optimier/marschall/Projects/alea-testing/src/alea/application/bayes"
                                      "/alea_util/configuration/problems/")
    config_entry.insert(0, name)

errmsg = 'Error!'
tk.Button(pane_config, text='File Open', command=btnfun_read_config).grid(row=0, column=2)

config_entry.insert(0, problem_file)


# endregion
# region solution list
def read_solution(sv):
    global bl_read_solution, cache_file, read_solutions
    if bl_read_solution:
        return True
    cache_file = sv.get()
    solution_entry.config(background="white")
    try:
        read_solutions = []
        from os.path import isfile
        f = open(cache_file, 'rb')
        lines = f.readlines()
        f.close()
        for line in lines:
            sol = TTLognormalAsgfemOperatorData()
            line = line.strip()
            # print("try to read file: {}".format(line))
            if len(line) <= 1:
                continue
            if sol.load(line) is False:
                print("can not read file")
                bl_read_solution = False
                solution_entry.config(background="red")
                break
            # do not take solutions into account that are not sampled. DO NOT DELETE THEM FROM FILE
            if sol.coef_sampling_result is None or sol.sol_sampling_result is None:
                continue
            # print("found solution: ")
            # print("     hdegs: {}".format(sol.hdegs))
            # print("     ranks: {}".format(sol.ranks))
            # print("     m-dofs: {}".format(sol.m_dofs))
            read_solutions.append(sol)

    except Exception as ex:
        print(ex)
        solution_entry.config(background="red")
        return False
    solution_entry.config(background="green")
    bl_read_solution = True

tk.Label(pane_config, text="Solution file: ").grid(row=1, column=0)
solution_entry_var = StringVar()
solution_entry_var.trace("w", lambda name, index, mode, sv=solution_entry_var: read_solution(solution_entry_var))
solution_entry = Entry(pane_config, textvariable=solution_entry_var, width=100)
solution_entry.grid(row=1, column=1)


def btnfun_read_solution():
    solution_entry.delete(0, END)
    name = askopenfilename(initialdir="/Home/optimier/marschall/Projects/alea-testing/src/alea/application/bayes"
                                      "/Bayes_util/ForwardOperator/tmp/")
    solution_entry.insert(0, name)

tk.Button(pane_config, text='File Open', command=btnfun_read_solution).grid(row=1, column=2)
solution_entry.insert(0, cache_file)


# endregion
# endregion

# region Main pane
frame_btn = Frame(pane_main)
frame_btn.pack(fill=X, expand=1, anchor=NW)
frame_middle = Frame(pane_main)
frame_middle.pack(fill=X, expand=1, anchor=NW)
frame_bottom = Frame(pane_main)
frame_bottom.pack(fill=X, expand=1, anchor=NW)

# region buttons in top frame
# region button press functions
# region button read data


def btnfun_read_data():
    global bl_read_configuration, bl_read_solution, bl_solution_frames_packed, bl_coef_frames_packed
    global loc_read_solutions, problem_file, cache_file

    problem_file = config_entry_var.get()
    config_entry.delete(0, END)
    bl_read_configuration = False
    config_entry.insert(0, problem_file)
    bl_read_configuration = True

    cache_file = solution_entry_var.get()
    solution_entry.delete(0, END)
    bl_read_solution = False
    solution_entry.insert(0, cache_file)
    bl_read_solution = True

    loc_read_solutions = []
    sol_lb.delete(0, last=END)
    bl_solution_frames_packed = False

    lb_coef.delete(0, last=END)
    lb_coef_realisation.delete(0, last=END)
    bl_coef_frames_packed = False


# endregion

# region button refine mesh uniformly
def btnfun_refine_det():
    print("TODO: refine mesh and start new calculation")


# endregion

# region button refine stochastic space
def btnfun_refine_sto():
    print("TODO: refine stochastic space (how?) and start new calculation")


# endregion

# region button refine rank by one
def btnfun_refine_rank():
    print("TODO: do a rank one update and start new calculation")


# endregion

# region button delete current item
def btnfun_delete_curr_item():
    global curr_item, curr_item_index, bl_solution_frames_packed
    if curr_item is None or curr_item_index is None:
        return False
    del loc_read_solutions[curr_item_index]
    curr_item = None
    sol_lb.delete(curr_item_index, curr_item_index)
    curr_item_index = None
    bl_solution_frames_packed = False


# endregion
# region button save current list
def btnfun_save_curr_list():
    cache_file_path = "/Home/optimier/marschall/Projects/alea-testing/src/alea/application/bayes" \
                      "/Bayes_util/ForwardOperator/tmp/"
    filename = get_current_time_string()
    f = open(cache_file_path + filename, 'a')
    for item in loc_read_solutions:
        f.write("\n{}".format(item.filename))
    f.close()
    print("your new solution file is: \n {} \n copy that into the configuration pane to load".format(cache_file_path +
                                                                                                     filename))
# endregion
# endregion

btn_load_data = tk.Button(frame_btn, text='Load Data', command=btnfun_read_data)
btn_load_data.pack(side=LEFT, padx=5, pady=5, anchor=NW)
btn_refine_det = tk.Button(frame_btn, text='refine mesh', command=btnfun_refine_det)
btn_refine_det.pack(side=LEFT, padx=5, pady=5, anchor=NW)
btn_refine_sto = tk.Button(frame_btn, text='refine stoc', command=btnfun_refine_sto)
btn_refine_sto.pack(side=LEFT, padx=5, pady=5, anchor=NW)
btn_refine_rank = tk.Button(frame_btn, text='refine rank', command=btnfun_refine_rank)
btn_refine_rank.pack(side=LEFT, padx=5, pady=5, anchor=NW)
# endregion

# region solution frame and listbox
# region def sol selected


def sol_selected(evt):
    global bl_curr_item_changed, curr_item, curr_item_index
    w = evt.widget
    if len(w.curselection()) < 1:
        return False
    index = int(w.curselection()[0])
    if index >= len(loc_read_solutions):
        print("!!!!ERROR: List index to long for sol list: {} > {}".format(index, len(loc_read_solutions)))
        return False
    curr_item = loc_read_solutions[index]
    curr_item_index = index
    bl_curr_item_changed = True
# endregion
# region solution list frame
frame_sol = Frame(frame_middle)
frame_sol.pack(side=LEFT, fill=Y, expand=1, anchor=NW, padx=5, pady=5)
scrollbar = Scrollbar(frame_sol, orient=VERTICAL)
sol_lb = Listbox(frame_sol, selectmode=SINGLE, yscrollcommand=scrollbar.set, width=30)
sol_lb.bind('<<ListboxSelect>>', sol_selected)
scrollbar.config(command=sol_lb.yview)
scrollbar.pack(side=RIGHT, fill=Y, expand=Y)
sol_lb.pack(side=LEFT, fill=BOTH, expand=1)
# endregion
# endregion

# region top frame of second row containing mesh, dimension and rank plots
frame_top = Frame(frame_middle)
frame_top.pack(side=LEFT, fill=X, expand=1, anchor=NW)

frame_options = Frame(frame_top)
frame_options.pack(side=LEFT, expand=1, anchor=NW, padx=5, pady=5)
btn_delete_curr_item = tk.Button(frame_options, text="delete current item", command=btnfun_delete_curr_item)
btn_delete_curr_item.pack(padx=5, pady=5, fill=X)
btn_save_curr_list = tk.Button(frame_options, text="save current list", command=btnfun_save_curr_list)
btn_save_curr_list.pack(padx=5, pady=5, fill=X)

frame_mesh = Frame(frame_top)
frame_mesh.pack(side=LEFT, expand=1, padx=5, pady=5)
mesh_canvas = Canvas(frame_mesh, width=canvas_photo_width, height=canvas_photo_height)
mesh_canvas.pack(expand=1)


frame_dim = Frame(frame_top)
frame_dim.pack(side=LEFT, expand=1, anchor=NW, padx=5, pady=5, fill=Y)

sb_dim = Scrollbar(frame_dim)
tb_dim = Text(frame_dim, state=DISABLED, width=20)
tb_dim.config(yscrollcommand=sb_dim.set)
tb_dim.pack(side=LEFT, fill=BOTH)
sb_dim.config(command=tb_dim.yview)
sb_dim.pack(side=RIGHT, fill=Y)

frame_dim_coef = Frame(frame_top)
frame_dim_coef.pack(side=LEFT, expand=1, anchor=NW, padx=5, pady=5, fill=Y)

sb_dim_coef = Scrollbar(frame_dim_coef)
tb_dim_coef = Text(frame_dim_coef, state=DISABLED, width=20)
tb_dim_coef.config(yscrollcommand=sb_dim_coef.set)
tb_dim_coef.pack(side=LEFT, fill=BOTH)
sb_dim_coef.config(command=tb_dim_coef.yview)
sb_dim_coef.pack(side=RIGHT, fill=Y)

frame_rank = Frame(frame_top)
frame_rank.pack(side=LEFT, expand=1, anchor=NW, padx=5, pady=5, fill=Y)

sb_rank = Scrollbar(frame_rank)
tb_rank = Text(frame_rank, state=DISABLED, width=20)
tb_rank.config(yscrollcommand=sb_rank.set)
tb_rank.pack(side=LEFT, fill=BOTH)
sb_rank.config(command=tb_rank.yview)
sb_rank.pack(side=RIGHT, fill=Y)

frame_rank_coef = Frame(frame_top)
frame_rank_coef.pack(side=LEFT, expand=1, anchor=NW, padx=5, pady=5, fill=Y)

sb_rank_coef = Scrollbar(frame_rank_coef)
tb_rank_coef = Text(frame_rank_coef, state=DISABLED, width=20)
tb_rank_coef.config(yscrollcommand=sb_rank_coef.set)
tb_rank_coef.pack(side=LEFT, fill=BOTH)
sb_rank_coef.config(command=tb_rank_coef.yview)
sb_rank_coef.pack(side=RIGHT, fill=Y)

frame_data = Frame(frame_top)
frame_data.pack(side=LEFT, expand=1, anchor=NW, padx=5, pady=5, fill=Y)

sb_data = Scrollbar(frame_data)
tb_data = Text(frame_data, state=DISABLED, width=40)
tb_data.config(yscrollcommand=sb_data.set)
tb_data.pack(side=LEFT, fill=BOTH)
sb_data.config(command=tb_data.yview)
sb_data.pack(side=RIGHT, fill=Y)
# endregion

# region frame error plot
frame_err_plot = Frame(frame_bottom)
frame_err_plot.pack(side=LEFT, expand=1, anchor=NW, padx=5, pady=5)
err_plot_canvas = Canvas(frame_err_plot, width=canvas_photo_width, height=canvas_photo_height)
err_plot_canvas.pack(expand=1)

var_op_err_plot_1 = StringVar(root)
var_op_err_plot_2 = StringVar(root)
var_op_err_plot_dof = StringVar(root)
var_op_err_plot_1.set("h1_rel")
var_op_err_plot_2.set("3((2.5 eta+zeta+res)^2+res^2)^0.5")
var_op_err_plot_dof.set("tt-dofs")
op_err_plot_dict = {"h1", "h1_semi", "h1_disc", "h1_rel", "h1_semi_rel", "h1_disc_rel", "l2", "l2_semi", "l2_disc",
                    "l2_rel", "l2_semi_rel", "l2_disc_rel", "3((2.5 eta+zeta+res)^2+res^2)^0.5"}
op_err_plot_dof_dict = {"tt-dofs", "mesh-dofs", "operator-dofs"}
op_err_plot_1 = OptionMenu(frame_err_plot, var_op_err_plot_1, *op_err_plot_dict)
op_err_plot_1.config(bg="blue")
op_err_plot_1.pack(expand=1, padx=5, pady=5, fill=X)
op_err_plot_2 = OptionMenu(frame_err_plot, var_op_err_plot_2, *op_err_plot_dict)
op_err_plot_2.config(bg="red")
op_err_plot_2.pack(expand=1, padx=5, pady=5, fill=X)

op_err_plot_dof = OptionMenu(frame_err_plot, var_op_err_plot_dof, *op_err_plot_dof_dict)
op_err_plot_dof.pack(expand=1, padx=5, pady=5, fill=X)

# region btnfun update error plot


def btnfun_update_err_plot(_curr_item=None):
    global err_plot_photo
    if curr_item is not None and _curr_item is None:
        _curr_item = curr_item
    fig_err_plot = plt.figure(figsize=(canvas_figure_width, canvas_figure_height))
    dof = [0]
    if var_op_err_plot_dof.get() == "tt-dofs":
        dof_list = [item.dofs for item in loc_read_solutions]
        if _curr_item is not None:
            dof = _curr_item.dofs
    elif var_op_err_plot_dof.get() == "mesh-dofs":
        dof_list = [item.m_dofs for item in loc_read_solutions]
        if _curr_item is not None:
            dof = _curr_item.m_dofs
    elif var_op_err_plot_dof.get() == "operator-dofs":
        dof_list = [item.op_dofs for item in loc_read_solutions]
        if _curr_item is not None:
            dof = _curr_item.op_dofs
    else:
        raise ValueError("unknown dof type")
    if var_op_err_plot_1.get() == "3((2.5 eta+zeta+res)^2+res^2)^0.5":
        plt.loglog(dof_list,
                   [3 * ((2.5 * item.eta_global + item.zeta_global + item.resnorm) ** 2 + item.resnorm ** 2) ** 0.5
                    for item in loc_read_solutions])
        if _curr_item is not None:
            plt.loglog(dof, 3 * ((2.5 * _curr_item.eta_global + _curr_item.zeta_global + _curr_item.resnorm) ** 2 +
                                 _curr_item.resnorm ** 2) ** 0.5, 'x', color="black")

    else:
        plt.loglog(dof_list, [item.sol_sampling_result[str(var_op_err_plot_1.get())]
                              for item in loc_read_solutions])
        if _curr_item is not None:
            plt.loglog(dof, _curr_item.sol_sampling_result[str(var_op_err_plot_1.get())], 'x', color="black")

    if var_op_err_plot_2.get() == "3((2.5 eta+zeta+res)^2+res^2)^0.5":
        plt.loglog(dof_list,
                   [3 * ((2.5 * item.eta_global + item.zeta_global + item.resnorm) ** 2 + item.resnorm ** 2) ** 0.5
                    for item in loc_read_solutions])
        if _curr_item is not None:
            plt.loglog(dof, 3 * ((2.5 * _curr_item.eta_global + _curr_item.zeta_global + _curr_item.resnorm) ** 2 +
                                 _curr_item.resnorm ** 2) ** 0.5, 'x', color="black")
    else:
        plt.loglog(dof_list, [item.sol_sampling_result[str(var_op_err_plot_2.get())]
                              for item in loc_read_solutions])
        if _curr_item is not None:
            plt.loglog(dof, _curr_item.sol_sampling_result[str(var_op_err_plot_2.get())], 'x', color="black")
    import matplotlib as mpl
    print(mpl.rcParams["figure.dpi"])
    for item in err_plot_canvas.pack_slaves():
        item.destroy()
    err_plot_photo = draw_figure(err_plot_canvas, fig_err_plot, loc=(canvas_x_padding, canvas_y_padding))
# endregion

btn_update_err_plot = tk.Button(frame_err_plot, text="update plot", command=btnfun_update_err_plot)
btn_update_err_plot.pack(padx=5, pady=5, fill=X)
# endregion

# region frame estimator plot
frame_est_plot = Frame(frame_bottom)
frame_est_plot.pack(side=LEFT, expand=1, anchor=NW, padx=5, pady=5)
est_plot_canvas = Canvas(frame_est_plot, width=canvas_photo_width, height=canvas_photo_height)
est_plot_canvas.pack(expand=1)

var_op_est_plot_1 = StringVar(root)
var_op_est_plot_2 = StringVar(root)
var_op_est_plot_dof = StringVar(root)
var_op_est_plot_1.set("eta_global")
var_op_est_plot_2.set("global_zeta")
var_op_est_plot_dof.set("tt-dofs")
op_est_plot_dict = {"rhs_global", "zero_global", "sq_global", "jump_global", "eta_global", "sampled_rhs_global",
                    "sampled_zero_global", "sampled_sq_global", "sampled_jump_global", "sampled_eta_global",
                    "global_zeta"}
op_est_plot_dof_dict = {"tt-dofs", "mesh-dofs", "operator-dofs"}
op_est_plot_1 = OptionMenu(frame_est_plot, var_op_est_plot_1, *op_est_plot_dict)
op_est_plot_1.config(bg="blue")
op_est_plot_1.pack(expand=1, padx=5, pady=5, fill=X)
op_est_plot_2 = OptionMenu(frame_est_plot, var_op_est_plot_2, *op_est_plot_dict)
op_est_plot_2.config(bg="red")
op_est_plot_2.pack(expand=1, padx=5, pady=5, fill=X)
op_est_plot_dof = OptionMenu(frame_est_plot, var_op_est_plot_dof, *op_est_plot_dof_dict)
op_est_plot_dof.pack(expand=1, padx=5, pady=5, fill=X)


# region btnfun update estimator plot
def btnfun_update_est_plot(_curr_item=None):
    global est_plot_photo
    if curr_item is not None and _curr_item is None:
        _curr_item = curr_item
    fig_est_plot = plt.figure(figsize=(canvas_figure_width, canvas_figure_height))
    dof = [0]
    if var_op_est_plot_dof.get() == "tt-dofs":
        dof_list = [item.dofs for item in loc_read_solutions]
        if _curr_item is not None:
            dof = _curr_item.dofs
    elif var_op_est_plot_dof.get() == "mesh-dofs":
        dof_list = [item.m_dofs for item in loc_read_solutions]
        if _curr_item is not None:
            dof = _curr_item.m_dofs
    elif var_op_est_plot_dof.get() == "operator-dofs":
        dof_list = [item.op_dofs for item in loc_read_solutions]
        if _curr_item is not None:
            dof = _curr_item.op_dofs
    else:
        raise ValueError("unknown dof type")

    plt.loglog(dof_list, [item.estimator_result[str(var_op_est_plot_1.get())] for item in loc_read_solutions])
    plt.loglog(dof_list, [item.estimator_result[str(var_op_est_plot_2.get())] for item in loc_read_solutions])

    if _curr_item is not None:
        plt.loglog(dof, _curr_item.estimator_result[str(var_op_est_plot_1.get())], 'x', color="black")
        plt.loglog(dof, _curr_item.estimator_result[str(var_op_est_plot_2.get())], 'x', color="black")

    for item in est_plot_canvas.pack_slaves():
        item.destroy()
    est_plot_photo = draw_figure(est_plot_canvas, fig_est_plot, loc=(canvas_x_padding, canvas_y_padding))
# endregion

btn_update_est_plot = tk.Button(frame_est_plot, text="update plot", command=btnfun_update_est_plot)
btn_update_est_plot.pack(padx=5, pady=5, fill=X)
# endregion

# region frame resnorm plot
frame_res_plot = Frame(frame_bottom)
frame_res_plot.pack(side=LEFT, expand=1, anchor=NW, padx=5, pady=5)
res_plot_canvas = Canvas(frame_res_plot, width=canvas_photo_width, height=canvas_photo_height)
res_plot_canvas.pack(expand=1)
var_op_res_plot_1 = StringVar(root)
var_op_res_plot_dof = StringVar(root)
var_op_res_plot_1.set("resnorm")
var_op_res_plot_dof.set("tt-dofs")
op_res_plot_dict = {"resnorm", "resnorm_alg", "resnorm_alg_norm"}

op_res_plot_dof_dict = {"tt-dofs", "mesh-dofs", "operator-dofs"}
op_res_plot_1 = OptionMenu(frame_res_plot, var_op_res_plot_1, *op_res_plot_dict)
op_res_plot_1.config(bg="blue")
op_res_plot_1.pack(expand=1, padx=5, pady=5, fill=X)

op_res_plot_dof = OptionMenu(frame_res_plot, var_op_res_plot_dof, *op_res_plot_dof_dict)
op_res_plot_dof.pack(expand=1, padx=5, pady=5, fill=X)

# region btnfun update residual estimator plot


def btnfun_update_res_plot(_curr_item=None):
    global res_plot_photo
    if curr_item is not None and _curr_item is None:
        _curr_item = curr_item
    fig_res_plot = plt.figure(figsize=(canvas_figure_width, canvas_figure_height))
    dof = [0]
    if var_op_res_plot_dof.get() == "tt-dofs":
        dof_list = [item.dofs for item in loc_read_solutions]
        if _curr_item is not None:
            dof = _curr_item.dofs
    elif var_op_res_plot_dof.get() == "mesh-dofs":
        dof_list = [item.m_dofs for item in loc_read_solutions]
        if _curr_item is not None:
            dof = _curr_item.m_dofs
    elif var_op_res_plot_dof.get() == "operator-dofs":
        dof_list = [item.op_dofs for item in loc_read_solutions]
        if _curr_item is not None:
            dof = _curr_item.op_dofs
    else:
        raise ValueError("unknown dof type")

    plt.loglog(dof_list, [item.res_estimator_result[str(var_op_res_plot_1.get())] for item in loc_read_solutions])

    if _curr_item is not None:
        plt.loglog(dof, _curr_item.res_estimator_result[str(var_op_res_plot_1.get())], 'x', color="black")

    for item in res_plot_canvas.pack_slaves():
        item.dresroy()
    res_plot_photo = draw_figure(res_plot_canvas, fig_res_plot, loc=(canvas_x_padding, canvas_y_padding))
# endregion

btn_update_res_plot = tk.Button(frame_res_plot, text="update plot", command=btnfun_update_res_plot)
btn_update_res_plot.pack(padx=5, pady=5, fill=X)
# endregion

# endregion

# region Coefficient pane
frame_coef_middle = Frame(pane_coef)
frame_coef_middle.pack(fill=X, expand=1, anchor=NW)
frame_coef_bottom = Frame(pane_coef)
frame_coef_bottom.pack(fill=X, expand=1, anchor=NW)
frame_coef_bottom_var = Frame(pane_coef)
frame_coef_bottom_var.pack(fill=X, expand=1, anchor=NW)
# region coefficient frame and listbox
# region def coef selected


def coef_selected(evt):
    global bl_curr_coef_item_changed, curr_coef_item, curr_coef_item_index
    w = evt.widget
    if len(w.curselection()) < 1:
        return False
    index = int(w.curselection()[0])
    if index >= len(loc_read_solutions):
        print("!!!!ERROR: List index to long for coef list: {} > {}".format(index, len(loc_read_solutions)))
        return False
    curr_coef_item = loc_read_solutions[index]
    curr_coef_item_index = index
    lb_coef_realisation.delete(0, END)
    bl_curr_coef_item_changed = True
# endregion
# region solution list frame
frame_coef_list = Frame(frame_coef_middle)
frame_coef_list.pack(side=LEFT, fill=Y, expand=1, anchor=NW, padx=5, pady=5)
sb_coef_list = Scrollbar(frame_coef_list, orient=VERTICAL)
lb_coef = Listbox(frame_coef_list, selectmode=SINGLE, yscrollcommand=sb_coef_list.set, width=30)
lb_coef.bind('<<ListboxSelect>>', coef_selected)
sb_coef_list.config(command=lb_coef.yview)
sb_coef_list.pack(side=RIGHT, fill=Y, expand=Y)
lb_coef.pack(side=LEFT, fill=BOTH, expand=1)
# endregion
# endregion
# region coefficient realisation frame and listbox
# region def coef realisation selected


def coef_realisation_selected(evt):
    global bl_curr_coef_realisation_item_changed, curr_coef_realisation_item, curr_coef_realisation_item_index
    global curr_coef_item, curr_coef_item_index
    if curr_coef_item is None or curr_coef_item_index is None:
        return False
    w = evt.widget
    if len(w.curselection()) < 1:
        return False
    realisation_index = int(w.curselection()[0])
    if realisation_index >= len(curr_coef_item.coef_sampling_result["sample_cont"]):
        print("!!!!ERROR: List index to long for coef sample list: "
              "{} > {}".format(realisation_index, len(curr_coef_item.coef_sampling_result["sample_cont"])))
        return False
    curr_coef_realisation_item = dict()
    curr_coef_realisation_item["cont"] = curr_coef_item.coef_sampling_result["sample_cont"][realisation_index]
    curr_coef_realisation_item["semi"] = curr_coef_item.coef_sampling_result["sample_semi"][realisation_index]
    curr_coef_realisation_item["disc"] = curr_coef_item.coef_sampling_result["sample_disc"][realisation_index]
    curr_coef_realisation_item_index = realisation_index
    bl_curr_coef_realisation_item_changed = True
# endregion
# region coefficient realisation list frame
frame_coef_realisation_list = Frame(frame_coef_middle)
frame_coef_realisation_list.pack(side=LEFT, fill=Y, expand=1, anchor=NW, padx=5, pady=5)
sb_coef_realisation_list = Scrollbar(frame_coef_realisation_list, orient=VERTICAL)
lb_coef_realisation = Listbox(frame_coef_realisation_list, selectmode=SINGLE, 
                              yscrollcommand=sb_coef_realisation_list.set, width=30)
lb_coef_realisation.bind('<<ListboxSelect>>', coef_realisation_selected)
sb_coef_realisation_list.config(command=lb_coef_realisation.yview)
sb_coef_realisation_list.pack(side=RIGHT, fill=Y, expand=Y)
lb_coef_realisation.pack(side=LEFT, fill=BOTH, expand=1)
# endregion
# endregion
# region coefficient sample plots
frame_cont_coef = Frame(frame_coef_middle)
frame_cont_coef.pack(side=LEFT, expand=1, padx=5, pady=5)
cont_coef_canvas = Canvas(frame_cont_coef, width=canvas_photo_width, height=canvas_photo_height)
cont_coef_canvas.pack(expand=1)
frame_semi_coef = Frame(frame_coef_middle)
frame_semi_coef.pack(side=LEFT, expand=1, padx=5, pady=5)
semi_coef_canvas = Canvas(frame_semi_coef, width=canvas_photo_width, height=canvas_photo_height)
semi_coef_canvas.pack(expand=1)
frame_disc_coef = Frame(frame_coef_middle)
frame_disc_coef.pack(side=LEFT, expand=1, padx=5, pady=5)
disc_coef_canvas = Canvas(frame_disc_coef, width=canvas_photo_width, height=canvas_photo_height)
disc_coef_canvas.pack(expand=1)
# endregion

# region frame coefficient plot
frame_coef_plot = Frame(frame_coef_bottom)
frame_coef_plot.pack(side=LEFT, expand=1, anchor=NW, padx=5, pady=5)
coef_plot_canvas = Canvas(frame_coef_plot, width=canvas_photo_width, height=canvas_photo_height)
coef_plot_canvas.pack(expand=1)
var_op_coef_plot_1 = StringVar(root)
var_op_coef_plot_dof = StringVar(root)
var_op_coef_plot_1.set("cont_semi_rel_L2")
var_op_coef_plot_dof.set("tt_dofs")
op_coef_plot_dict = {"cont_semi", "cont_disc", "semi_disc", "cont_semi_rel", "cont_disc_rel", "semi_disc_rel",
                     "cont_semi_L2", "cont_disc_L2", "semi_disc_L2", "cont_semi_rel_L2", "cont_disc_rel_L2",
                     "semi_disc_rel_L2", "cont_semi_H1", "cont_disc_H1", "semi_disc_H1", "cont_semi_rel_H1",
                     "cont_disc_rel_H1", "semi_disc_rel_H1"}

op_coef_plot_dof_dict = {"tt_dofs", "mesh_dofs"}
op_coef_plot_1 = OptionMenu(frame_coef_plot, var_op_coef_plot_1, *op_coef_plot_dict)
op_coef_plot_1.config(bg="blue")
op_coef_plot_1.pack(expand=1, padx=5, pady=5, fill=X)

op_coef_plot_dof = OptionMenu(frame_coef_plot, var_op_coef_plot_dof, *op_coef_plot_dof_dict)
op_coef_plot_dof.pack(expand=1, padx=5, pady=5, fill=X)

# region btnfun update coefficient plot


def btnfun_update_coef_plot(_curr_item=None):
    global coef_plot_photo
    if curr_item is not None and _curr_item is None:
        _curr_item = curr_item
    fig_coef_plot = plt.figure(figsize=(canvas_figure_width, canvas_figure_height))
    dof = [0]
    dof_list = [item.coef_sampling_result[str(var_op_coef_plot_dof.get())] for item in loc_read_solutions]
    if _curr_item is not None:
        dof = _curr_item.coef_sampling_result[str(var_op_coef_plot_dof.get())]

    plt.loglog(dof_list, [item.coef_sampling_result[str(var_op_coef_plot_1.get())] for item in loc_read_solutions])

    if _curr_item is not None:
        plt.loglog(dof, _curr_item.coef_sampling_result[str(var_op_coef_plot_1.get())], 'x', color="black")

    for item in coef_plot_canvas.pack_slaves():
        item.destroy()
    coef_plot_photo = draw_figure(coef_plot_canvas, fig_coef_plot, loc=(canvas_x_padding, canvas_y_padding))
# endregion

btn_update_coef_plot = tk.Button(frame_coef_plot, text="update plot", command=btnfun_update_coef_plot)
btn_update_coef_plot.pack(padx=5, pady=5, fill=X)
# endregion

# region frame coefficient mean plot
frame_coef_mean_cont_plot = Frame(frame_coef_bottom)
frame_coef_mean_cont_plot.pack(side=LEFT, expand=1, anchor=NW, padx=5, pady=5)
coef_mean_cont_plot_canvas = Canvas(frame_coef_mean_cont_plot, width=canvas_photo_width, height=canvas_photo_height)
coef_mean_cont_plot_canvas.pack(expand=1)
frame_coef_mean_semi_plot = Frame(frame_coef_bottom)
frame_coef_mean_semi_plot.pack(side=LEFT, expand=1, anchor=NW, padx=5, pady=5)
coef_mean_semi_plot_canvas = Canvas(frame_coef_mean_semi_plot, width=canvas_photo_width, height=canvas_photo_height)
coef_mean_semi_plot_canvas.pack(expand=1)
frame_coef_mean_disc_plot = Frame(frame_coef_bottom)
frame_coef_mean_disc_plot.pack(side=LEFT, expand=1, anchor=NW, padx=5, pady=5)
coef_mean_disc_plot_canvas = Canvas(frame_coef_mean_disc_plot, width=canvas_photo_width, height=canvas_photo_height)
coef_mean_disc_plot_canvas.pack(expand=1)
# endregion
# region frame coefficient variance plot
frame_coef_var_cont_plot = Frame(frame_coef_bottom_var)
frame_coef_var_cont_plot.pack(side=LEFT, expand=1, anchor=NW, padx=5, pady=5)
coef_var_cont_plot_canvas = Canvas(frame_coef_var_cont_plot, width=canvas_photo_width, height=canvas_photo_height)
coef_var_cont_plot_canvas.pack(expand=1)
frame_coef_var_semi_plot = Frame(frame_coef_bottom_var)
frame_coef_var_semi_plot.pack(side=LEFT, expand=1, anchor=NW, padx=5, pady=5)
coef_var_semi_plot_canvas = Canvas(frame_coef_var_semi_plot, width=canvas_photo_width, height=canvas_photo_height)
coef_var_semi_plot_canvas.pack(expand=1)
frame_coef_var_disc_plot = Frame(frame_coef_bottom_var)
frame_coef_var_disc_plot.pack(side=LEFT, expand=1, anchor=NW, padx=5, pady=5)
coef_var_disc_plot_canvas = Canvas(frame_coef_var_disc_plot, width=canvas_photo_width, height=canvas_photo_height)
coef_var_disc_plot_canvas.pack(expand=1)
# endregion
# endregion


def draw_results():
    global bl_curr_coef_item_changed, bl_curr_coef_realisation_item_changed, bl_curr_item_changed
    global bl_solution_frames_packed, bl_coef_frames_packed
    global mesh_photo, err_plot_photo, est_plot_photo, res_plot_photo, coef_plot_photo
    global coef_mean_cont_plot_photo, coef_mean_semi_plot_photo, coef_mean_disc_plot_photo
    global coef_var_cont_plot_photo, coef_var_semi_plot_photo, coef_var_disc_plot_photo
    global coef_cont_photo, coef_semi_photo, coef_disc_photo

    if not bl_read_configuration or not bl_read_solution:
        root.after(1000, draw_results)
        return False
    
    if not bl_solution_frames_packed and not bl_coef_frames_packed:
        if loc_read_solutions is None or len(loc_read_solutions) < 1:
            for lia, sol in enumerate(read_solutions):
                sol_lb.insert(END, "solution {}".format(lia+1))
                lb_coef.insert(END, "coefficient {}".format(lia + 1))
            # copy solution list to local list that can be modified.
            for item in read_solutions:
                loc_read_solutions.append(item)
        bl_solution_frames_packed = True
        bl_coef_frames_packed = True
        
        # region init main pane
        # region frame error plot
        try:
            fig_err_plot = plt.figure(figsize=(canvas_figure_width, canvas_figure_height))
            plt.loglog([item.dofs for item in loc_read_solutions],
                       [item.sol_sampling_result["h1_rel"] for item in loc_read_solutions])
            plt.loglog([item.dofs for item in loc_read_solutions],
                       [3 * ((2.5 * item.eta_global + item.zeta_global + item.resnorm) ** 2 + item.resnorm ** 2) ** 0.5
                        for item in loc_read_solutions])
            for item in err_plot_canvas.pack_slaves():
                item.destroy()
            err_plot_photo = draw_figure(err_plot_canvas, fig_err_plot, loc=(canvas_x_padding, canvas_y_padding))
        except TypeError as ex:
            print("!!! {} : {}".format(__name__, ex.message))
        # endregion
        # region frame estimator plot
        try:
            fig_est_plot = plt.figure(figsize=(canvas_figure_width, canvas_figure_height))

            plt.loglog([item.dofs for item in loc_read_solutions], [item.estimator_result["eta_global"]
                                                                    for item in loc_read_solutions])
            plt.loglog([item.dofs for item in loc_read_solutions], [item.estimator_result["global_zeta"]
                                                                    for item in loc_read_solutions])
            for item in est_plot_canvas.pack_slaves():
                item.destroy()
            est_plot_photo = draw_figure(est_plot_canvas, fig_est_plot, loc=(canvas_x_padding, canvas_y_padding))
        except TypeError as ex:
            print("!!! {} : {}".format(__name__, ex.message))
        # endregion
        # region frame resnorm plot
        try:
            fig_res_plot = plt.figure(figsize=(canvas_figure_width, canvas_figure_height))
            plt.loglog([item.dofs for item in loc_read_solutions], [item.resnorm for item in loc_read_solutions])
            for item in res_plot_canvas.pack_slaves():
                item.destroy()
            res_plot_photo = draw_figure(res_plot_canvas, fig_res_plot, loc=(canvas_x_padding, canvas_y_padding))
        except TypeError as ex:
            print("!!! {} : {}".format(__name__, ex.message))
        # endregion
        # endregion
        
        # region init coefficient pane
        # region frame coefficient plot
        try:
            fig_coef_plot = plt.figure(figsize=(canvas_figure_width, canvas_figure_height))
            plt.loglog([item.coef_sampling_result["tt_dofs"] for item in loc_read_solutions],
                       [item.coef_sampling_result["cont_semi_rel_L2"] for item in loc_read_solutions])
            for item in coef_plot_canvas.pack_slaves():
                item.destroy()
            coef_plot_photo = draw_figure(coef_plot_canvas, fig_coef_plot, loc=(canvas_x_padding, canvas_y_padding))
        except TypeError as ex:
            print("!!! {} : {}".format(__name__, ex.message))
        # endregion
        # endregion

    if bl_coef_frames_packed and curr_coef_item is not None and bl_curr_coef_item_changed:
        assert (len(curr_coef_item.coef_sampling_result["sample_cont"]) ==
                len(curr_coef_item.coef_sampling_result["sample_semi"]))
        assert (len(curr_coef_item.coef_sampling_result["sample_cont"]) ==
                len(curr_coef_item.coef_sampling_result["sample_disc"]))
        for lia, sample in enumerate(curr_coef_item.coef_sampling_result["sample_cont"]):
            lb_coef_realisation.insert(END, "realisation {}".format(lia+1))
        curr_fs = FunctionSpace(curr_coef_item.mesh, 'CG', config.femdegree)
        fun = Function(curr_fs)
        # region frame coef mean continuous
        try:
            fig_coef_mean_cont = plt.figure(figsize=(canvas_figure_width, canvas_figure_height))
            fun.vector()[:] = curr_coef_item.coef_sampling_result["mean_cont"]
            plot_mpl(fun)

            plt.title("mean continuous")
            plt.colorbar()
            for item in coef_mean_cont_plot_canvas.pack_slaves():
                item.destroy()
            coef_mean_cont_plot_photo = draw_figure(coef_mean_cont_plot_canvas, fig_coef_mean_cont,
                                                    loc=(canvas_x_padding, canvas_y_padding))
        except TypeError as ex:
            print("!!! {} : {}".format(__name__, ex.message))
        # endregion

        # region frame coef mean semidiscrete
        try:
            fig_coef_mean_semi = plt.figure(figsize=(canvas_figure_width, canvas_figure_height))
            fun.vector()[:] = curr_coef_item.coef_sampling_result["mean_semi"]
            plot_mpl(fun)

            plt.title("mean semi-discrete")
            plt.colorbar()
            for item in coef_mean_semi_plot_canvas.pack_slaves():
                item.destroy()
            coef_mean_semi_plot_photo = draw_figure(coef_mean_semi_plot_canvas, fig_coef_mean_semi,
                                                    loc=(canvas_x_padding, canvas_y_padding))
        except TypeError as ex:
            print("!!! {} : {}".format(__name__, ex.message))
        # endregion

        # region frame coef mean discrete
        try:
            fig_coef_mean_disc = plt.figure(figsize=(canvas_figure_width, canvas_figure_height))
            fun.vector()[:] = curr_coef_item.coef_sampling_result["mean_disc"]
            plot_mpl(fun)

            plt.title("mean fully-discrete")
            plt.colorbar()
            for item in coef_mean_disc_plot_canvas.pack_slaves():
                item.destroy()
            coef_mean_disc_plot_photo = draw_figure(coef_mean_disc_plot_canvas, fig_coef_mean_disc,
                                                    loc=(canvas_x_padding, canvas_y_padding))
        except TypeError as ex:
            print("!!! {} : {}".format(__name__, ex.message))
        # endregion
        # region frame coef var continuous
        try:
            fig_coef_var_cont = plt.figure(figsize=(canvas_figure_width, canvas_figure_height))
            fun.vector()[:] = curr_coef_item.coef_sampling_result["var_cont"]
            plot_mpl(fun)

            plt.title("variance continuous")
            plt.colorbar()
            for item in coef_var_cont_plot_canvas.pack_slaves():
                item.destroy()
            coef_var_cont_plot_photo = draw_figure(coef_var_cont_plot_canvas, fig_coef_var_cont,
                                                   loc=(canvas_x_padding, canvas_y_padding))

        except TypeError as ex:
            print("!!! {} : {}".format(__name__, ex.message))
        # endregion

        # region frame coef var semidiscrete
        try:
            fig_coef_var_semi = plt.figure(figsize=(canvas_figure_width, canvas_figure_height))
            fun.vector()[:] = curr_coef_item.coef_sampling_result["var_semi"]
            plot_mpl(fun)

            plt.title("variance semi discrete")
            plt.colorbar()
            for item in coef_var_semi_plot_canvas.pack_slaves():
                item.destroy()
            coef_var_semi_plot_photo = draw_figure(coef_var_semi_plot_canvas, fig_coef_var_semi,
                                                   loc=(canvas_x_padding, canvas_y_padding))
        except TypeError as ex:
            print("!!! {} : {}".format(__name__, ex.message))
        # endregion

        # region frame coef var discrete
        try:
            fig_coef_var_disc = plt.figure(figsize=(canvas_figure_width, canvas_figure_height))
            fun.vector()[:] = curr_coef_item.coef_sampling_result["var_disc"]
            plot_mpl(fun)

            plt.title("variance fully-discrete")
            plt.colorbar()
            for item in coef_var_disc_plot_canvas.pack_slaves():
                item.destroy()
            coef_var_disc_plot_photo = draw_figure(coef_var_disc_plot_canvas, fig_coef_var_disc,
                                                   loc=(canvas_x_padding, canvas_y_padding))

        except TypeError as ex:
            print("!!! {} : {}".format(__name__, ex.message))
        # endregion

        bl_curr_coef_item_changed = False
    if bl_coef_frames_packed and curr_coef_realisation_item is not None and bl_curr_coef_realisation_item_changed \
            and curr_coef_item is not None:
        curr_fs = FunctionSpace(curr_coef_item.mesh, 'CG', config.femdegree)
        fun = Function(curr_fs)
        # region frame coef_cont
        try:
            fig_coef_cont = plt.figure(figsize=(canvas_figure_width, canvas_figure_height))
            fun.vector()[:] = curr_coef_realisation_item["cont"]
            plot_mpl(fun)
            plt.title("continuous")
            plt.colorbar()
            for item in cont_coef_canvas.pack_slaves():
                item.destroy()
            coef_cont_photo = draw_figure(cont_coef_canvas, fig_coef_cont, loc=(5, 0))
        except TypeError as ex:
            print("!!! {} : {}".format(__name__, ex.message))
        # endregion
        # region frame coef_semi
        try:
            fig_coef_semi = plt.figure(figsize=(canvas_figure_width, canvas_figure_height))
            fun.vector()[:] = curr_coef_realisation_item["semi"]
            plot_mpl(fun)
            plt.title("semi-continuous")
            plt.colorbar()
            for item in semi_coef_canvas.pack_slaves():
                item.destroy()
            coef_semi_photo = draw_figure(semi_coef_canvas, fig_coef_semi, loc=(5, 0))
        except TypeError as ex:
            print("!!! {} : {}".format(__name__, ex.message))
        # endregion
        # region frame coef_disc
        try:
            fig_coef_disc = plt.figure(figsize=(canvas_figure_width, canvas_figure_height))
            fun.vector()[:] = curr_coef_realisation_item["disc"]
            plot_mpl(fun)
            plt.title("fully-discrete")
            plt.colorbar()
            for item in disc_coef_canvas.pack_slaves():
                item.destroy()
            coef_disc_photo = draw_figure(disc_coef_canvas, fig_coef_disc, loc=(5, 0))
        except TypeError as ex:
            print("!!! {} : {}".format(__name__, ex.message))
        # endregion
        bl_curr_coef_realisation_item_changed = False

    if bl_solution_frames_packed and curr_item is not None and bl_curr_item_changed:

        # region frame mesh
        try:
            fig_mesh = plt.figure(figsize=(canvas_figure_width, canvas_figure_height))
            plot_mpl(curr_item.mesh)
            for item in mesh_canvas.pack_slaves():
                item.destroy()
            mesh_photo = draw_figure(mesh_canvas, fig_mesh, loc=(5, 0))
        except TypeError as ex:
            print("!!! {} : {}".format(__name__, ex.message))
        # endregion
        # region frame dim
        tb_dim_text = "current solution dimension:\n\n"
        if isinstance(curr_item.solution, tt.vector):
            tb_dim_text = curr_item.solution.n
        else:
            for lia, core in enumerate(curr_item.solution):
                assert(len(core.shape) == 3)
                tb_dim_text += "n({}) = {} \n".format(lia, core.shape[1])
        tb_dim.config(state=NORMAL)
        tb_dim.delete(1.0, END)
        tb_dim.insert(END, tb_dim_text)
        tb_dim.config(state=DISABLED)
        # endregion
        # region frame dim_coef
        tb_dim_coef_text = "discrete coefficient dimension:\n\n"
        tb_dim_coef_text += "n(0) = {}\n".format(curr_item.cont_coef_m_dofs)
        for lia, value in enumerate(curr_item.cont_coef_hermite_degree):
            tb_dim_coef_text += "n({}) = {} \n".format(lia+1, value)
        tb_dim_coef.config(state=NORMAL)
        tb_dim_coef.delete(1.0, END)
        tb_dim_coef.insert(END, tb_dim_coef_text)
        tb_dim_coef.config(state=DISABLED)
        # endregion
        # region frame rank
        tb_rank_text = "current solution ranks:\n\n"
        if isinstance(curr_item.solution, tt.vector):
            tb_rank_text = curr_item.solution.r
        else:
            for lia, core in enumerate(curr_item.solution):
                assert (len(core.shape) == 3)
                tb_rank_text += "r({}) = {} \n".format(lia, core.shape[0])
            tb_rank_text += "r({}) = {}".format(len(curr_item.solution), curr_item.solution[-1].shape[2])
        tb_rank.config(state=NORMAL)
        tb_rank.delete(1.0, END)
        tb_rank.insert(END, tb_rank_text)
        tb_rank.config(state=DISABLED)
        # endregion
        # region frame rank_coef
        tb_rank_coef_text = "discrete coefficient ranks:\n\n"
        for lia, value in enumerate(curr_item.cont_coef_ranks):
            tb_rank_coef_text += "r({}) = {} \n".format(lia, value)
        tb_rank_coef.config(state=NORMAL)
        tb_rank_coef.delete(1.0, END)
        tb_rank_coef.insert(END, tb_rank_coef_text)
        tb_rank_coef.config(state=DISABLED)
        # endregion

        # region frame data
        tb_data_text = ""
        tb_data_text += "{:15} = {}\n".format("sampled_eta", curr_item.det_estimator_result["sampled_eta_global"])
        tb_data_text += "{:15} = {}\n".format("eta", curr_item.eta_global)
        tb_data_text += "{:15} = {}\n".format("zeta", curr_item.zeta_global)
        tb_data_text += "{:15} = {}\n".format("resnorm", curr_item.resnorm)
        tb_data_text += "\n"
        tb_data_text += "{:15}\n".format("zeta list estimators:")
        try:
            for key, value in curr_item.sto_estimator_result["zeta_list"].iteritems():
                tb_data_text += "{:15d} = {}\n".format(key, value)
        except TypeError as ex:
            print("!!! {} : {}".format(__name__, ex.message))


        tb_data.config(state=NORMAL)
        tb_data.delete(1.0, END)
        tb_data.insert(END, tb_data_text)
        tb_data.config(state=DISABLED)
        # endregion

        btnfun_update_est_plot(_curr_item=curr_item)
        btnfun_update_err_plot(_curr_item=curr_item)
        btnfun_update_res_plot(_curr_item=curr_item)
        btnfun_update_coef_plot(_curr_item=curr_item)

        bl_curr_item_changed = False
    root.after(1000, draw_results)
            
notebook.pack(fill=BOTH, expand=1)

root.after(1000, draw_results)
root.mainloop()
