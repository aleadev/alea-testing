# region imports
from __future__ import division

import argparse                                     # use command parameter parsing
from dolfin import FunctionSpace, refine, Function, File, errornorm, norm, UnitSquareMesh, set_log_level, ERROR, \
    interpolate, cells, Mesh

import numpy as np
from alea.application.bayes.alea_util.coefficient_field.tensor.tt_lognormal_semidiscrete_field import \
    read_coef_from_config
from alea.application.bayes.alea_util.coefficient_field.tensor.tt_lognormal_fullydiscrete_field import \
    TTLognormalFullyDiscreteField
from alea.application.bayes.alea_util.configuration.asgfem_config import AsgfemConfig
from alea.utils.tictoc import TicToc
from alea.utils.timing import get_current_time_string
from alea.application.bayes.Bayes_util.ForwardOperator.tt_forward_operator_util import calculate_mass_matrix

from alea.utils.progress.bar import Bar
from alea.application.tt_asgfem.tensorsolver.tt_sgfem_util import tt_dofs
import sys
set_log_level(ERROR)
# endregion

# region parser arguments
parser = argparse.ArgumentParser(description="estimate the MC convergence against the TT-mean")
parser.add_argument("-f", "--file", type=str, default="default.pro",
                    help="problem file to load")
parser.add_argument("-sf", "--solutionFile", type=str, default="",
                    help="file containing the sample results. Automatic overwrite mode active!")

args = parser.parse_args()
problem_file = "../../alea_util/configuration/problems/" + args.file
if args.solutionFile == "":
    cache_file = "../../alea_util/coefficient_field/tmp/" + get_current_time_string() + "solutions.txt"
else:
    cache_file = "../../alea_util/coefficient_field/tmp/" + args.solutionFile
overwrite = args.solutionFile
# endregion

timeit = False
sample = True

# region pre-computations and object creation
# region Read configuration from problem file
with TicToc(sec_key="Configuration", key="read configuration", do_print=False, active=True):
    config = AsgfemConfig(problem_file)
    if config.read_configuration() is False:
        exit()
# endregion
if sample:
    # region create discrete function space
    with TicToc(sec_key="Semi-Disc Coef", key="create function space", do_print=False, active=True):

        mesh_coef = UnitSquareMesh(150, 150)
        fs_coef = FunctionSpace(mesh_coef, 'CG', 3)
        mass = calculate_mass_matrix(fs_coef)
        dofs = fs_coef.tabulate_dof_coordinates()
        dofs = dofs.reshape(-1, 2)
    # endregion
    # region Create dummy coefficient to sample rvs from
    dummy_semi_discrete_coef = read_coef_from_config(config)

    with TicToc(sec_key="Semi-Disc Coef", key="create parameter samples", do_print=False, active=True):
        y_list = [dummy_semi_discrete_coef.sample_rvs(config.reference_M) for _ in range(config.n_samples)]
    # endregion

    # region create cache elements for faster access later
    class true_coef_cache:
        def __init__(self):
            self.func_list = []
            self.fs = None


    bar = Bar("  Sample reference", max=len(y_list)+1)
    func_coef_cont_list = []
    func_coef_rel_denom_list = []
    cache = true_coef_cache()
    cache.fs = fs_coef
    bar.next()
    for y in y_list:
        func_coef_cont_list.append(dummy_semi_discrete_coef.sample_continuous_field(dofs, y, cache=cache))
        func_coef_rel_denom_list.append(np.dot(func_coef_cont_list[-1], mass.dot(func_coef_cont_list[-1])))
        bar.next()
    bar.finish()


    class affine_field_cache:
        def __init__(self, _fs, _M, _af):
            self.M = _M
            self.af = []
            self.b_fun = [None] * (_M + 1)

            def sigma(m):
                return np.exp(config.theta * config.rho * dummy_semi_discrete_coef.affine_field.get_infty_norm(m)[-1])

            def value(__func, __sigmam, __nu):
                return (np.power(__func * __sigmam, __nu) / np.sqrt(float(np.math.factorial(__nu)))) * \
                        np.exp(np.power(__func * __sigmam, 2) / 2)

            for _lia in range(_M + 1):
                _func = interpolate(_af[_lia][0][0], _fs)
                self.af.append(_func.vector()[:])
            for _lia in range(1, _M + 1):
                    _sigmam = sigma(_lia)
                    self.b_fun[_lia] = np.array([value(self.af[_lia], _sigmam, nu) for nu in range(config.max_hdegs_coef)])
    # endregion

# af_cache = affine_field_cache(fs_coef, len(dummy_semi_discrete_coef.get_cont_cores()),
#                               dummy_semi_discrete_coef.affine_field)
# endregion


ranks = [10, 20, 50, 100]
for rank in ranks:
    print("run with rank {}".format(rank))
    
    # region prepare return values
    err_cont_semi = 0
    err_cont_disc = 0
    err_semi_disc = 0
    # endregion
    
    print("  create coefficient")
    # region read coefficient from configuration
    config.max_rank = rank

    if timeit:
        for lia in range(10):
            with TicToc(sec_key="Semi-Disc Coef", key="create coefficient rank: {}".format(rank), do_print=True,
                        active=True):
                semi_discrete_coef = read_coef_from_config(config)
    elif sample:
        with TicToc(sec_key="Semi-Disc Coef", key="create coefficient rank: {}".format(rank), do_print=True,
                    active=True):
            semi_discrete_coef = read_coef_from_config(config)
    else:
        exit()

    TicToc.sortedTimes(sec_sorted=True)
    print(float(TicToc.get("create coefficient rank: {}".format(rank))) * 0.1)
    if not sample:
        continue
        # print("compute 100 infty norms")
        # norms = semi_discrete_coef.affine_field.get_infty_norm(100)

        # print("first 10 norms: {}".format(norms[0:9]))
        # print("max of norms={}".format(np.max(norms)))
        # print("sum of norms={}".format(np.sum(norms)))
        # for lia in range(13):
        #     norms = semi_discrete_coef.affine_field.get_infty_norm(2**lia)
        #     print(" sum of first {} norms: {}".format(2**lia, np.sum(norms)))
        # exit()
    # endregion
    # print("curr coefficient: \n{}".format(semi_discrete_coef))

    # region sample coefficient
    with TicToc(sec_key="Semi-Disc Coef", key="project coef rank: {}".format(rank), do_print=True, active=True):
        discrete_coef = TTLognormalFullyDiscreteField(semi_discrete_coef, fs_coef, domain=config.domain)

    with TicToc(sec_key="Semi-Disc Coef", key="sample rank {}".format(rank), do_print=False, active=True):
        bar = Bar("  sample", max=len(y_list))
        for lia, y in enumerate(y_list):
            func_coef_cont = func_coef_cont_list[lia]
            func_coef_rel_denom = func_coef_rel_denom_list[lia]
            # func_coef_semi = semi_discrete_coef(dofs, y, fs=fs_coef, af_cache=af_cache)
            func_coef_disc = discrete_coef(dofs, y, at_mesh_nodes=True)
            # diff_cont_semi = func_coef_cont - func_coef_semi
            diff_cont_disc = func_coef_cont - func_coef_disc
            # diff_semi_disc = func_coef_semi - func_coef_disc
            
            # err_cont_semi += np.dot(diff_cont_semi, mass.dot(diff_cont_semi)) / func_coef_rel_denom
            err_cont_disc += np.dot(diff_cont_disc, mass.dot(diff_cont_disc)) / func_coef_rel_denom
            # err_semi_disc += np.dot(diff_semi_disc, mass.dot(diff_semi_disc)) / (np.dot(func_coef_cont,
            #                                                                             mass.dot(func_coef_semi)))
            
            bar.next()
        bar.finish()
        
    sys.stdout.write("\n")
    print("rms err cont disc: {}".format(np.sqrt(err_cont_disc) * len(y_list)**(-1)))
    print("tt-dofs: {}".format(discrete_coef.tt_dofs))
    print("full-dofs: {}".format(discrete_coef.full_dofs))
    # print("err cont disc: {}".format(err_cont_disc * len(y_list) ** (-1)))
    # print("err semi disc: {}".format(err_semi_disc * len(y_list) ** (-1)))
    # endregion
TicToc.sortedTimes(sec_sorted=True)
