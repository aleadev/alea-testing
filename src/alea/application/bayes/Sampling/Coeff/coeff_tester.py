from __future__ import division
import argparse
import numpy as np
from scipy.special import zeta

# alea imports
from alea.application.egsz.sample_problems2 import SampleProblem
from alea.application.egsz.sample_domains import SampleDomain
from alea.application.egsz.affine_field import AffineField
from alea.utils.tictoc import TicToc

# FEniCS imports
from dolfin import cells, FunctionSpace, interactive, refine, CellFunctionBool

# logger
import logging, sys
import logging.config

# tt imports
from tensorsolver.tt_lognormal import generate_lognormal_tt, sample_lognormal_tt, get_coeff_upper_bound, tt_cont_coeff, sample_cont_coeff

def setup_logger():
    # configure logger
    logger = logging.getLogger('coeff_tester.log')
    sh = logging.StreamHandler(sys.stdout)
    sh.setLevel(logging.INFO)
    als_logger = logging.getLogger('tensorsolver.tt_als')
    als_logger.setLevel(logging.INFO)
    als_logger.addHandler(sh)
    return logger, als_logger

# ###############
# A problem setup
# ###############

parser = argparse.ArgumentParser()
parser.add_argument("-ct", "--coeff-type", type=str, default="EF-square-cos",
                    help="coefficient type")
parser.add_argument("-M", "--M-terms", type=int, default=10,
                    help="terms in realisation")
parser.add_argument("-at", "--amptype", type=str, default="decay-inf",
                    help="coefficient decay type")
parser.add_argument("-de", "--decay-exponent", type=float, default=2.0,
                    help="decay exponent")
parser.add_argument("-g", "--gamma", type=float, default=0.9,
                    help="gamma")
parser.add_argument("-fk", "--freq-skip", type=int, default=0,
                    help="frequency skip")
parser.add_argument("-fs", "--freq-scale", type=float, default=1.0,
                    help="frequency scaling")
parser.add_argument("-s", "--scale", type=float, default=1.0,
                    help="scaling")
parser.add_argument("-fd", "--fem-degree", type=int, default=1,
                    help="fem degree (default 1)")
parser.add_argument("-dom", "--domain", type=int, choices=[0,1], default=0,
                    help="domain (default 0=square, 1=L-shaped)")
parser.add_argument("-mref", "--mesh-refine", type=int, default=0,
                    help="initial refinement (default 0)")
parser.add_argument("-md", "--mesh-dofs", type=int, default=-1,
                    help="initial mesh dofs, overrides mesh-refine (default -1 = ignore)")
parser.add_argument("-th", "--theta", type=int, default=0.1,
                    help="weight of gauss measure (between 0 and 1)")
parser.add_argument("-rh", "--rho", type=int, default=1,
                    help="weight of gauss measure (greater than 0)")
parser.add_argument("-hdeg", "--hermite-degree", type=int, default=8,
                    help="degree of hermite polynomials for approximation of coefficient")
parser.add_argument("-gpc", "--gpc-degree", type=int, default=3,
                    help="degree of hermite polynomials for approximation of operator")
parser.add_argument("-mr", "--max-rank", type=int, default=20,
                    help="maximum rank for operator")
# parser.add_argument("-bn", "--basename", type=str, default="",
#                     help="export basename")
# parser.add_argument("-mmc", "--max-mesh-cells", type=int, default=50000, dest="mmc",
#                     help="maximal mesh cells for export (default 50000)")
args = parser.parse_args()
print "========== PARAMETERS:", vars(args)

# setup logger
logger, als_logger = setup_logger()

# set some flags
# save_fn = None
# WITH_EXPORT_SOLUTION = args.basename != ""

# set export name of experiment
# EXPERIMENT = args.basename

# set discretisation parameters
M, femdegree, gpcdegree, polysys = args.M_terms, args.fem_degree, args.gpc_degree, 'H'
amptype, decayexp, gamma, rvtype = args.amptype, args.decay_exponent, args.gamma, 'normal'

assert femdegree == 1       # currently only affine linear elements
assert args.domain == 0     # currently only square mesh

# setup domain and meshes
domain, mesh_refine, mesh_dofs = ["square", "lshape"][args.domain], args.mesh_refine, args.mesh_dofs
if mesh_dofs > 0:
    mesh_refine = 0
mesh, boundaries, dim = SampleDomain.setupDomain(domain, initial_mesh_N=10)
mesh = SampleProblem.setupMesh(mesh, num_refine=mesh_refine)
while mesh_dofs > 0 and FunctionSpace(mesh, 'CG', femdegree).dim() < mesh_dofs:
    # uniformly refine mesh
    mesh = refine(mesh)
logger.info("initial mesh has %i dofs with p%i FEM" % (FunctionSpace(mesh, 'CG', femdegree).dim(), femdegree))


#### CODE STARTS HERE! #################################################################################################

# #######################
# B construct coefficient
# #######################

# create field
af = AffineField(args.coeff_type, amptype=amptype, decayexp=decayexp, gamma=gamma, freqscale=args.freq_scale, freqskip=args.freq_skip, scale=args.scale)
coeff_field = af.coeff_field
theta, rho = args.theta, args.rho
hdeg = args.hermite_degree
hdegs = M * [hdeg]
maxrank = args.max_rank
ranks = [1] + M * [4*maxrank] + [1]
ranks2 = [1] + M * [4*maxrank] + [1]

start = SampleProblem.get_decay_start(decayexp, gamma)
amp = gamma / zeta(decayexp, start)
bxmax_m = [amp * (i + start) ** (-decayexp) for i in range(M)]
bxmax = np.array(bxmax_m)

###############################################
######## Full Discretization Method ###########
###############################################

U = []
non_uniform = True

# get cell midpoints
mp = np.zeros((mesh.num_cells(), 2))
for ci, c in enumerate(cells(mesh)):
    mpi = c.midpoint()
    mp[ci] = mpi.x(), mpi.y()

TicToc.clear()
with TicToc(key="**** CreateFullyDiscreteCoefficient ****", active=True, do_print=True):
    # evaluate M basis functions at cell midpoints
    B = af.evaluate_basis(mp, M)
    # # TODO: better bxmax, for now with theta=0
    # bxmax = get_coeff_upper_bound(B)

    U.append(generate_lognormal_tt(B, hdeg, ranks, bxmax, theta, rho, acc=1e-7))
    print "U", U[0]

###############################################
######## Semi Discretized Method ##############
###############################################

with TicToc(key="**** CreateSemiDiscreteCoefficient ****", active=True, do_print=True):
    coeff_cores = tt_cont_coeff(af, mesh, M, ranks2, hdegs, bxmax, theta, rho, fe_h=1e-2, fe_p=2, acc=1e-7)


###############################################
######## Compute Sample Errors ################
###############################################

def evaluate_realisation_error(mp, y, UU):
    V1 = af(mp, y)                  # exact values at mp
    V1 = np.exp(V1)
    V2 = sample_lognormal_tt(UU, y, bxmax, theta, rho)  # TT values at mp
    V3 = sample_cont_coeff(coeff_cores, af, mp, y, ranks2, hdegs, bxmax, theta, rho)
    return np.linalg.norm(V1-V2),np.linalg.norm(V1-V3),np.linalg.norm(V2-V3)

N = 10
coeff_err1 = 0
coeff_err2 = 0
coeff_err3 = 0
for _ in range(N):
    err1,err2,err3= evaluate_realisation_error(mp, af.sample_rvs(M), U[0])
    coeff_err1 += err1**2
    coeff_err2 += err2**2
    coeff_err3 += err3**2
coeff_err1 = np.sqrt(coeff_err1)
coeff_err2 = np.sqrt(coeff_err2)
coeff_err3 = np.sqrt(coeff_err3)
print "MC sample error", 0, coeff_err1, coeff_err2, coeff_err3


# repeat this for refined mesh several times with constant ranks and calculate new U
for i in range(10):
    if non_uniform:
        cf = CellFunctionBool(mesh)
        cf.set_all(False)
        cids = np.random.randint(0, mesh.num_cells()-1, mesh.num_cells()//3)
        for cid in cids:
            cf[cid] = True
        mesh = refine(mesh, cf)
    else:
        mesh = refine(mesh)

    # get cell midpoints
    mp = np.zeros((mesh.num_cells(), 2))
    for ci, c in enumerate(cells(mesh)):
        mpi = c.midpoint()
        mp[ci] = mpi.x(), mpi.y()

    # evaluate M basis functions at cell midpoints
    B = af.evaluate_basis(mp, M)
    # # TODO: better bxmax
    # bxmax = get_coeff_upper_bound(B)

    U.append(generate_lognormal_tt(B, hdeg, ranks, bxmax, theta, rho, acc=1e-7))
    print "U", U[i+1]

    N = 10
    coeff_err1 = 0
    coeff_err2 = 0
    coeff_err3 = 0
    for _ in range(N):
        err1,err2,err3= evaluate_realisation_error(mp, af.sample_rvs(M), U[i+1])
        coeff_err1 += err1**2
        coeff_err2 += err2**2
        coeff_err3 += err3**2
    coeff_err1 = np.sqrt(coeff_err1)
    coeff_err2 = np.sqrt(coeff_err2)
    coeff_err3 = np.sqrt(coeff_err3)
    print "MC sample error", i+1, coeff_err1, coeff_err2, coeff_err3
