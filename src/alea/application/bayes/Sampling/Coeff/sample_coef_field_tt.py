from __future__ import division                     # float division as standard
from copy import deepcopy
from dolfin import refine, FunctionSpace, cells, Function, interpolate
from dolfin.fem.norms import norm, errornorm
import numpy as np
from mpi4py import MPI
# region ALEA imports
from alea.application.egsz.sample_problems2 import SampleProblem
from alea.application.egsz.sample_domains import SampleDomain
from alea.application.egsz.affine_field import AffineField
from alea.utils.tictoc import TicToc
from alea.application.tt_asgfem.tensorsolver.tt_lognormal import tt_cont_coeff, sample_cont_coeff
from alea.utils.plothelper import PlotHelper
# endregion

coef_mesh_dofs = 1000
sample_mesh_dofs = 100
coef_quad_degree = 5
domain = "square"
femdegree = 1
coeffield_type = "EF-square-cos-algebraic"
amptype = "decay-inf"
gamma = 1
decayexp = 2
freq_scale = 1.0
freq_skip = 0
scale = 1.0
field_mean = 0.0
theta = 0.1
rho = 1.0
coef_acc = 1e-12

n_samples = 1

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

maxrank = 2**(rank+3)
maxrank = 40
# region Setup domain and mesh
# ###############################################
# A: setup domain and meshes
# ###############################################

# # call sampleDomain and get boundaries, mesh and dimension
mesh, boundaries, dim = SampleDomain.setupDomain(domain, initial_mesh_N=10)
#                                               # refine mesh according to input
mesh = SampleProblem.setupMesh(mesh, num_refine=2)
#                                               # further refinement if you want to have a specific amount of dofs

#print("soll mesh_dofs={} haben dofs={}".format(coef_mesh_dofs, FunctionSpace(mesh, 'CG', femdegree).dim()))
  # uniformly refine mesh
# logger.info("initial mesh has %i dofs with p%i FEM" % (FunctionSpace(mesh, 'CG', femdegree).dim(), femdegree))
#                                               # obtain all coordinates of degrees of freedom
mp = np.array(FunctionSpace(mesh, 'DG', 0).tabulate_dof_coordinates())
mp = mp.reshape((-1, mesh.geometry().dim()))
# endregion

# region Construct coefficient
# ##############################################
# B construct coefficient
# ##############################################
# region Construct affine Field for exponent and PDE
#                                               # create affine coefficient field as logarithm of the used field
af = AffineField(coeffield_type, amptype=amptype, decayexp=decayexp, gamma=gamma, freqscale=freq_scale,
                 freqskip=freq_skip, scale=scale, coef_mean=field_mean)
#                                               # coefficient mean is 0 anyway. therefore do not set it later
#                                               # prepare the PDE problem
pde, _, _, _, _, _ = SampleProblem.setupPDE(2, domain, 0, boundaries, af.coeff_field)
# endregion

# region define hermite degrees, tensor ranks and evaluate field for old version of coefficient tensor
hdegs_coef = [6, 6, 6, 6, 6, 5, 5, 5, 5, 4, 4, 3, 3, 3, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1]
# hdegs_coef = [6, 6, 6]
n_coef_coef = len(hdegs_coef)
ranks_coef = [1] + len(hdegs_coef) * [maxrank] + [1]
B = af.evaluate_basis(mp, n_coef_coef)  # evaluate physical basis functions at cell midpoints
# endregion

# region Create or Load Samples
import cPickle as Pickle

try:
    raise ValueError
    f = open('y-list-file.dat', 'rb')
    sample_y_list = Pickle.load(f)
    f.close()
    if len(sample_y_list) < n_samples:
        for lia in range(len(sample_y_list), n_samples):
            sample_y_list.append(af.sample_rvs(len(hdegs_coef)))
    f = open('y-list-file.dat', 'wb')
    Pickle.dump(sample_y_list, f, Pickle.HIGHEST_PROTOCOL)
    f.close()
except:
    sample_y_list = []
    for lia in range(n_samples):
        sample_y_list.append(af.sample_rvs(len(hdegs_coef)))
    f = open('y-list-file.dat', 'wb')
    Pickle.dump(sample_y_list, f, Pickle.HIGHEST_PROTOCOL)
    f.close()

# endregion

# region Calculate scaling parameter bmax
# TODO: better bxmax for other amp types

start = SampleProblem.get_decay_start(decayexp, gamma)
if coeffield_type == "EF-square-cos":
    from scipy.special import zeta
    amp = gamma / zeta(decayexp, start)
    bxmax_m = [scale * amp * (i + start) ** (-decayexp) for i in range(B.shape[1] - 1)]
elif coeffield_type == "EF-square-cos-algebraic":
    bxmax_m = [scale * (i + start) ** (-decayexp) for i in range(B.shape[1] - 1)]
bxmax = np.array(bxmax_m)

print("bxmax: {}".format(bxmax))
# endregion
# region Create old version tensor (not used anymore)
#                                               # generate log normal affine coefficient (old version for reference)
# U = generate_lognormal_tt(B, hermite_degree, deepcopy(ranks_coef), bxmax, theta=theta, rho=rho)
# endregion
# region Create reduced basis coefficient tensor
###############################################
# Semi Discretized Method
###############################################
#                                               # create semi discrete coefficient with reduced basis approach
with TicToc(sec_key="Create coefficient", key="**** CreateSemiDiscreteCoefficient ****", active=True,
            do_print=True):
    cont_coeff_cores = tt_cont_coeff(af, mesh, n_coef_coef, ranks_coef, hdegs_coef, bxmax, theta=theta, rho=rho,
                                     acc=coef_acc,
                                     #mesh_dofs=coef_mesh_dofs, quad_degree=coef_quad_degree,
                                     #coef_mesh_cache=None, domain=domain, scale=scale
                                     )
# endregion
# region Create reduced coefficient TT by cross-approximation

with TicToc(sec_key="Create coefficient", key="cross approximation", active=True,
            do_print=True):
    import TensorToolbox as DT
    samples = 2**(rank+1)
    samples = 8
    points = FunctionSpace(mesh, 'CG', femdegree).tabulate_dof_coordinates()
    points = points.reshape((-1, mesh.geometry().dim()))
    y_grid = [np.arange(len(points))]  # init dof grid with FEM size
    if False:
        import SpectralToolbox as ST
        polytype = ST.Spectral1D.HERMITEP_PROB
        quadtype = ST.Spectral1D.GAUSS
        polyparam = None
        polyspan = (-np.inf, np.inf)
        orders = []
        for _ in range(len(hdegs_coef)):
            y_grid.append((polytype, quadtype, polyparam, polyspan))      # append stochastic grid
            orders.append(samples)
        def solution_func(y, params=None):
            return np.exp(af(points, list(y)))

        #                                                   # create Spectral TT-Tensor via cross approximation
        approx_ten = DT.STT(solution_func,y_grid, None, method='ttdmrgcross', surrogate_type=DT.PROJECTION,
                            range_dim=1, surrogateONOFF=True, orderAdapt=True, orders=orders # marshal_f=True,
                            )
        print("STT created. next: build() with {} samples and {} points".format(samples, len(points)))
        approx_ten.build(50)
        print("STT build with dimensions {} and rank {}!".format(approx_ten.TW.get_view_shape(), np.max([core.ranks() for core in approx_ten.TTapprox])))
# endregion

# region Create new sample mesh
with TicToc(key="  1. Sample coefficient field: create Mesh", active=True, do_print=True):
    # if coef_mesh_cache is None:
    mesh2 = mesh  # refine(mesh)
    while FunctionSpace(mesh2, 'DG', 0).dim() < coef_mesh_dofs:
        mesh2 = refine(mesh2)  # uniformly refine mesh
    sample_dofs = FunctionSpace(mesh2, 'DG', 0).dim()
    # else:
    #     mesh2 = coef_mesh_cache
    #     sample_dofs = FunctionSpace(mesh2, 'DG', 0).dim()
# endregion
# region obtain cell midpoints
with TicToc(key="  2. Get cell midpoints", active=True, do_print=True):
    mp = np.zeros((mesh2.num_cells(), 2))
    for ci, c in enumerate(cells(mesh2)):
        mpi = c.midpoint()
        mp[ci] = mpi.x(), mpi.y()
# endregion
minimal_coef_value = np.inf
for lia in range(n_samples):


    # region Sample true and approximated coefficient field
    fs = FunctionSpace(mesh, 'CG', femdegree)
    dg_fun = Function(FunctionSpace(mesh, 'DG', 0))

    y = af.sample_rvs(len(hdegs_coef))
    y = np.random.randn(len(hdegs_coef))
    V1_ = af(mp, list(y))
    V1_ = np.exp(V1_)
    dg_fun.vector()[:] = V1_
    fun_v1 = Function(fs)
    fun_v2 = Function(fs)
    fun_v3 = Function(fs)
    fun_v1 = interpolate(dg_fun, fs)
    print("sample_error with ranks={}, hdegs={} on mesh with {} DG0 dofs".format(ranks_coef, hdegs_coef,
                                                                                 sample_dofs))
    # from alea.application.tt_asgfem.tensorsolver.tt_lognormal import sample_cont_coeff_2
    with TicToc(key="  3. Sample cont core", active=True, do_print=True):
        V3_ = sample_cont_coeff(cont_coeff_cores, af, mp, y, ranks_coef, hdegs_coef, bxmax, theta, rho,
                                # dg_mesh=None
                                )
    dg_fun.vector()[:] = V3_
    fun_v3 = interpolate(dg_fun, fs)
    if False:
        with TicToc(key="  3. Sample cross tt", active=True, do_print=True):
            V2_ = approx_ten(np.array(y))
            fun_v2.vector()[:] = V2_
    real_rank = np.max([core.shape[0] for core in cont_coeff_cores])
    print("error y={} \n for rank {} and real maxrank {}: {} - \n".format(y, maxrank,
                                                                                          # approx_ten.TW.get_view_shape(),
                                                                                          real_rank,
                                                                             errornorm(fun_v1, fun_v3, degree_rise=1)/norm(fun_v1),
                                                                             # errornorm(fun_v1, fun_v2, degree_rise=1)/norm(fun_v1)))
                                                                                          ))
import matplotlib.pyplot as plt
from dolfin.common.plotting import mesh2triang
from mpl_toolkits.mplot3d import Axes3D

ax = plt.gca(projection='3d')
ax.set_aspect('equal')
ax.plot_trisurf(mesh2triang(mesh), fun_v1.compute_vertex_values(mesh), cmap=plt.cm.CMRmap)
plt.savefig("true_coef{}.png".format(rank))
plt.clf()
ax = plt.gca(projection='3d')
ax.set_aspect('equal')
ax.plot_trisurf(mesh2triang(mesh), fun_v2.compute_vertex_values(mesh), cmap=plt.cm.CMRmap)
plt.savefig("ttcross_coef{}.png".format(rank))
plt.clf()
ax = plt.gca(projection='3d')
ax.set_aspect('equal')
ax.plot_trisurf(mesh2triang(mesh), fun_v3.compute_vertex_values(mesh), cmap=plt.cm.CMRmap)
plt.savefig("ttcont_coef{}.png".format(rank))
plt.clf()
# endregion
# endregion