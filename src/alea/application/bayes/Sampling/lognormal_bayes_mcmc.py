"""
test script to sample the forward solution of the asgfem using the lognormal tensor train description
@author: Manuel Marschall
"""

# region imports
from __future__ import division, print_function
import numpy as np
import argparse                                     # use command parameter parsing
from alea.application.bayes.alea_util.coefficient_field.tensor.tt_lognormal_semidiscrete_field import \
    read_coef_from_config
from alea.application.bayes.alea_util.configuration.bayes_config import BayesConfig
from alea.application.bayes.Bayes_util.ForwardOperator.tt_lognormal_asgfem_operator_data import \
    TTLognormalAsgfemOperatorData
from alea.application.bayes.Bayes_util.ForwardOperator.tt_lognormal_asgfem_operator import TTLognormalAsgfemOperator
from dolfin import FunctionSpace, refine, Function, File, errornorm, norm, UnitSquareMesh, set_log_level, ERROR, \
    project, cells, Mesh, interpolate
from alea.application.egsz.sample_domains import SampleDomain
from alea.application.egsz.sample_problems2 import SampleProblem

from alea.utils.tictoc import TicToc

import matplotlib.pyplot as plt

import emcee
from alea.utils.plot import corner
# endregion

# region parser arguments
parser = argparse.ArgumentParser(description="estimate the MC convergence against the TT-mean")
parser.add_argument("-f", "--file", type=str, default="default.pro",
                    help="problem file to load")

args = parser.parse_args()
problem_file = "../alea_util/configuration/bayes_problems/" + args.file
# endregion

# region Read configuration from problem file
with TicToc(sec_key="Configuration", key="read configuration", do_print=False, active=True):
    config = BayesConfig(problem_file)
    if config.read_configuration() is False:
        exit()
    config.y_true[0] = 1
    config.y_true[1] = np.log(4)
    config.y_true[2] = np.log(32)
    config.y_true[3] = np.log(100)
# endregion

# region read coefficient from configuration
with TicToc(sec_key="Semi-Disc Coef", key="read from configuration", do_print=False, active=True):
    semi_discrete_coef = read_coef_from_config(config)
    #if len(config.y_true) > len(semi_discrete_coef.hermite_degree):
    #    hermite_degree = [config.hdegs_coef] * len(config.y_true)
    #    ranks = [1] + [config.max_rank] * len(config.y_true) + [1]
    #    semi_discrete_coef.update_coefficient(new_hdeg=hermite_degree, new_rank=semi_discrete_coef.ranks)
# endregion

# region try to read solution data from file
read_solutions = []
try:
    cache_file = "../Bayes_util/ForwardOperator/tmp/" + "paper_lognormal_P3-sigma2-thy05-thx05-resnorm.txt"
    from os.path import isfile
    if not isfile(cache_file):
        print("   can not find cache file: {} -> exit()".format(cache_file))
        exit()
    f = open(cache_file, 'rb')
    lines = f.readlines()
    f.close()
    for line in lines:
        sol = TTLognormalAsgfemOperatorData()
        line = line.strip()
        # print("try to read file: {}".format(line))
        if len(line) <= 1:
            continue
        if sol.load(line) is False:
            print("can not read file")
            read_solution = None
            break
        # print("found solution: ")
        # print("     hedgs: {}".format(sol.hdegs))
        # print("     ranks: {}".format(sol.ranks))
        # print("     m-dofs: {}".format(sol.m_dofs))
        read_solutions.append(sol)

except Exception as ex:
    print(ex)
    read_solutions = None
    exit()

print("found solution: ")
print("     hedgs: {}".format(read_solutions[-1].hdegs))
print("     ranks: {}".format(read_solutions[-1].ranks))
print("     m-dofs: {}".format(read_solutions[-1].m_dofs))

config.y_true = config.y_true[0:len(read_solutions[-1].hdegs)]


# endregion

# region create initial fs
with TicToc(sec_key="initial function space", key="create", do_print=False, active=True):
    if config.init_mesh_dofs > 0:  # mesh dofs are defined
        mesh_refine = 0  # there are no mesh refinements
    else:
        mesh_refine = config.mesh_refine  # use configured mesh refinements

    mesh, boundaries, dim = SampleDomain.setupDomain(config.domain, initial_mesh_N=2)
    #                                               # refine mesh according to input
    mesh = SampleProblem.setupMesh(mesh, num_refine=mesh_refine)
    #                                               # refinement if you want to have a specific amount of dofs

    print("soll mesh_dofs={} haben dofs={}".format(config.init_mesh_dofs,
                                                   FunctionSpace(mesh, 'CG', config.femdegree).dim()))
    while config.ref_mesh_dof > 0 and FunctionSpace(mesh, 'CG', config.femdegree).dim() < config.ref_mesh_dof:
        mesh = refine(mesh)  # uniformly refine mesh
    pde, _, _, _, _, _ = SampleProblem.setupPDE(2, config.domain, 0, boundaries, semi_discrete_coef)
# endregion

# region create current forward operator
curr_fs = FunctionSpace(mesh, "CG", 3)
curr_hdeg = [2]
with TicToc(sec_key="Forward Operator", key="create new operator", do_print=False, active=True):
    curr_forward_operator = TTLognormalAsgfemOperator(semi_discrete_coef, curr_fs, curr_hdeg, config.domain, empty=True)
# endregion

u_true, a_true = curr_forward_operator.sample_analytic_solution(config.y_true, pde, reference_m=len(config.y_true),
                                                                ref_fs=curr_fs)
u_true_fun = Function(curr_fs)
u_true_fun.vector().set_local(u_true)

a_true_fun = interpolate(a_true, curr_fs)
# a_true_fun.vector().set_local(a_true)

solution_to_fit = read_solutions[-1]
fs = FunctionSpace(solution_to_fit.mesh, 'CG', 3)
u_approx = solution_to_fit.sample_solution(config.y_true)
u_approx_fun = Function(fs)
u_approx_fun.vector().set_local(u_approx)
u_approx_fun = interpolate(u_approx_fun, curr_fs)

File("a_true.pvd") << a_true_fun
File("u_true.pvd") << u_true_fun

for n_samples_sqrt in [2, 4, 8, 12, 16, 20]:
    # n_samples_sqrt = 16
    measurement_nodes = [[i/int(n_samples_sqrt+1), j/int(n_samples_sqrt+1)]
                         for i in range(1, n_samples_sqrt+1) for j in range(1, n_samples_sqrt+1)]
    print("sample_coordinates: {}".format(measurement_nodes))
    noise = 0.0001
    measurements = []
    for lia, point in enumerate(measurement_nodes):
        measurements.append(u_true_fun(point) + np.random.randn()*noise)
    #     print("u_true({}) = {},     u_approx({}) = {}".format(point, u_true_fun(point), point, u_approx_fun(point)))



    def ln_prior(_loc_xi):
        retval = 0
        for _lia, param in enumerate(_loc_xi):
            retval += -0.5*param**2
        return retval


    def lnprob(__loc_xi):

        loc_xi = []
        for lia in range(len(config.y_true)):
            if lia < len(__loc_xi):
                loc_xi.append(__loc_xi[lia])
            else:
                loc_xi.append(config.y_true[lia])

        u_approx_fun = Function(fs)
        values = solution_to_fit.sample_solution(loc_xi)
        # print("vector size: {}, space size: {} ".format(len(values), u_approx_fun.ufl_function_space().dim()))
        u_approx_fun.vector().set_local(values)
        # u_approx_fun = interpolate(u_approx_fun, curr_fs)
        potential = 0
        for lia, point in enumerate(measurement_nodes):
            potential += -0.5/noise*((measurements[lia] - u_approx_fun(point))**2)

            # sol_measurements = np.array([sol_measurement + np.random.randn() * loc_cov
            #                             for sol_measurement in sol_measurements])
        # mean = calculate_mean(solution)
        # return np.log(1/mean * np.exp(potential))
        return ln_prior(loc_xi) + potential


    # dimension = len(config.y_true)
    dimension = 4

    chain_walker = dimension * 2 + 20
    burn_in = 10
    mc_steps = 1000

    p0 = np.random.randn(dimension * chain_walker).reshape((chain_walker, dimension))
    sampler = emcee.EnsembleSampler(chain_walker, dimension, lnprob,
                                    args=[])
    # test = lnprob(sample_data_list, true_value_data_list, solution, 0.001, dof_list)
    print("run burn in")
    pos, prob, state = sampler.run_mcmc(p0, burn_in)
    print("run mcmc")
    for i, _ in enumerate(sampler.sample(pos, iterations=mc_steps)):
        if (i+1) % 10 == 0:
            print("{0:5.1%}".format(float(i+1) / mc_steps), end="\r")
    # sampler.run_mcmc(pos, mc_steps)

    samples = sampler.chain[:, burn_in, :].reshape((-1, dimension))

    _fig, axs = plt.subplots(dimension, 1, facecolor='w', edgecolor='k')
    axs = axs.ravel()
    for lia in range(dimension):
        axs[lia].plot([config.y_true[lia]]*sampler.chain.shape[1], color='r')
        for lic in range(chain_walker):
            axs[lia].plot(sampler.chain[lic, :, lia], color='b')
    _fig.savefig("chain_walker_{}.png".format(len(measurement_nodes)))
    _fig.clf()
    fig = corner.corner(samples, labels=["$y_{}$".format(lia+1) for lia in range(dimension)],
                        truths=config.y_true[:dimension])
    fig.savefig("triangle_{}.png".format(len(measurement_nodes)))
    fig.clf()

    y_approx = []
    for lia in range(len(config.y_true)):
        y_approx.append(config.y_true[lia])
    for lia in range(dimension):
        quant = samples[:, lia]
        quant = np.percentile(quant, [15, 50, 85], axis=0)
        mean = quant[1]
        upper = quant[2] - quant[1]
        lower = quant[1] - quant[0]

        print("mean={}, upper={}, lower={}".format(mean, upper, lower))
        y_approx[lia] = mean

    u_approx, a_approx = curr_forward_operator.sample_analytic_solution(y_approx, pde, reference_m=len(config.y_true),
                                                                        ref_fs=curr_fs)
    u_approx_fun = Function(curr_fs)
    u_approx_fun.vector().set_local(u_approx)

    a_approx_fun = interpolate(a_approx, curr_fs)
    # a_approx_fun.vector().set_local(a_true)

    File("a_approx_{}.pvd".format(len(measurement_nodes))) << a_approx_fun
    File("u_approx_{}.pvd".format(len(measurement_nodes))) << u_approx_fun
