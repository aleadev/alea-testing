# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  sample TT-tensor Bayesian inversion results in view of normalization constant (mean), density and solution errors
"""
# region Imports
# region Standard library imports
from __future__ import division                     # use float division without dot
import os                                           # use system directory and file library
import argparse                                     # use command parameter parsing
# import matplotlib.pyplot as plt                     # use python plotting library
# from scipy.integrate import quad                    # use quadrature integration
# from scipy.interpolate import interp1d              # use one dimensional interpolation
from joblib import Parallel, delayed                # use parallel computing and function delaying
import numpy as np                                  # use Matlab like array notation
import copy
# endregion
# region Bayes libraries
from Bayes_util.lib.bayes_lib import create_parameter_samples
from Bayes_util.exponential.implicit_euler import ImplicitEuler

# from Bayes_util.asgfem_bayes import ASGFEMBayes
from Bayes_util.lognormal_sgfem_bayes import LognormalSGFEMBayes
from Bayes_util.exponential.embedded_runge_kutta import EmbeddedRungeKutta
from Bayes_util.exponential.rank_adaptive_embedded_runge_kutta import RankAdaptiveEmbeddedRungeKutta

from tt_als.tensorsolver.tt_sgfem_util import  tt_dofs
# from Bayes_util.lib.interpolation_util import get_cheb_points
# endregion
import logging.config                               # use logging to file
import matplotlib                                   # use plot library
matplotlib.use('Agg')                               # use 'Agg' extension to use in cluster environment
# endregion
__author__ = "marschall"

# region Setup parameters
# region Parser Arguments
# region ASGFEM parameter
parser = argparse.ArgumentParser(description="estimate the MC convergence against the TT-mean")
parser.add_argument("-gd", "--gpcdegree", type=int, default="10",
                    help="maximal # of gpc degrees")
parser.add_argument("-fd", "--femdegree", type=int, default="1", choices=range(1, 4),
                    help="femdegree used in ASGFEM")
parser.add_argument("-md", "--max_dofs", type=float, default="1e6",
                    help="maximal # of dofs in ASGFEM")
parser.add_argument("-mer", "--mesh_refine", type=int, default="1",
                    help="numer of mesh refinements")
parser.add_argument("-it", "--iterations", type=int, default="1000",
                    help="maximal # of als iterations")
parser.add_argument("-dom", "--domain", type=str, default="square", choices=["square"],
                    help="domain used in ASGFEM")
parser.add_argument("-ct", "--coeffield-type", type=str, default="cos", choices=["cos"],
                    help="coefficient type")
parser.add_argument("-at", "--amp_type", type=str, default="constant", choices=["constant", "decay-inf"],
                    help="amplification function type")
parser.add_argument("-de", "--decay_exp_rate", type=float, default=2.0,
                    help="rate of decay in the coefficient field")
parser.add_argument("-fm", "--field_mean", type=float, default=1.0,
                    help="mean of the coefficient field")
parser.add_argument("-sg", "--sgfem_gamma", type=float, default=0.9,
                    help="value of gamma in the ASGFEM coefficient field")
parser.add_argument("-imd", "--init_mesh_dofs", type=int, default=-1,
                    help="# of initial physical mesh dofs (-1: ignore)")
parser.add_argument("-tx", "--theta", type=float, default=0.5,
                    help="weight of Gaussian measure")
parser.add_argument("-rho", "--rho", type=float, default=1.0,
                    help="weight of Gaussian measure > 1")
parser.add_argument("-hd", "--hermite_degree", type=int, default=8,
                    help="hermite polynomial degree on gPC expansion")
parser.add_argument("-gpcd", "--gpc_degree", type=int, default=11,
                    help="maximal number of gpcd. used in sampling to create parameter samples ")
parser.add_argument("-nc", "--n_coefficients", type=int, default=5,
                    help="# of coefficients in the stochastic field approximation (aka M)")
parser.add_argument("-mr", "--max_rank", type=int, default=15,
                    help="maximal rank in forward solution")
parser.add_argument("-sr", "--sol_rank", type=int, default=5,
                    help="solution rank")
parser.add_argument("-frsc", "--freq_scale", type=float, default=1.0,
                    help="frequency scaling in the gPC expansion")
parser.add_argument("-frsk", "--freq_skip", type=int, default=1,
                    help="frequency skip in the gPC expansion")
parser.add_argument("-sca", "--scale", type=float, default=1.0,
                    help="scalinf of the whole solution")
parser.add_argument("-tol", "--als_tol", type=float, default=1e-12,
                    help="tolerance in the ALS algorithm")
# endregion
# region Observation parameter
parser.add_argument("-ns", "--n_samples_sqrt", type=int, default=3,
                    help="# of sample points in physical domain. (ns --> ns^2 points)")
parser.add_argument("-op", "--obs_precision", type=float, default=0.0,
                    help="precision used in TT.round in the observation process")
parser.add_argument("-or", "--obs_rank", type=int, default=500,
                    help="maximal rank used in TT.round in the observation process")
# endregion
# region Inner Product parameter
parser.add_argument("-ip", "--inp_precision", type=float, default=0.0,
                    help="precision used in TT.round in the calculation of the inner product")
parser.add_argument("-ir", "--inp_rank", type=int, default=500,
                    help="maximal rank used in TT.round in the calculation of the inner product")
parser.add_argument("-ig", "--inp_gamma", type=float, default=1e-3,
                    help="gamma. constant value of covariance operator")
# endregion
# region Exponential calculation parameter
parser.add_argument("-ep", "--euler_precision", type=float, default=0.0,
                    help="precision used in TT.round in the calculation of the exponential")
parser.add_argument("-er", "--euler_rank", type=int, default=50,
                    help="maximal rank used in TT.round in the calculation of the exponential")
parser.add_argument("-els", "--euler_local_mc_samples", type=int, default=1,
                    help="# of MC samples to check the convergence of each euler step (highly optional)")
parser.add_argument("-egs", "--euler_global_mc_samples", type=int, default=1000,
                    help="# of MC samples to check the convergence of the resulting exponential (highly optional)")
parser.add_argument("-erep", "--euler_repetitions", type=int, default=0,
                    help="# of repetitions of the euler procedure if the desired accuracy is not reached (only euler)")
parser.add_argument("-es", "--euler_steps", type=int, default=1000,
                    help="# of steps done in the iterative process. For adaptive only start value.")
parser.add_argument("-egp", "--euler_global_precision", type=float, default=1e-10,
                    help="desired accuracy to reach in the exponential approximation")
parser.add_argument("-eui", "--euler_use_implicit", action="store_true",
                    help="switch to use the implicit euler scheme for more stability.")
parser.add_argument("-rm", "--runge_method", type=str, default="forth", choices=["linear", "square", "forth"],
                    help="used embedded runge kutta method")
parser.add_argument("-rn", "--runge_min_stepsize", type=float, default=1e-19,
                    help="minimal step size to check in adaptive step size control")
parser.add_argument("-rx", "--runge_max_stepsize", type=float, default=1-1e-10,
                    help="maximal step size to check in adaptive step size control")
# endregion
# region Density parameter
parser.add_argument("-ng", "--n_eval_grid", type=int, default=50,
                    help="# of evaluation points to estimate densities (for plots only)")
# endregion
# region Stochastic collocation parameter
parser.add_argument("-ny", "--Ny", type=int, default=10,
                    help="# of collocation points in one stochastic dimension")
# endregion
# region File saving paths
parser.add_argument("-fp", "--file_path", type=str, default="results/paper/mean/",
                    help="path to store plots and results")
parser.add_argument("-bp", "--bayes_path", type=str, default="results/bayes/",
                    help="path to store the results of the bayesian procedure")
parser.add_argument("-pp", "--plot_path", type=str, default="results/bayes/",
                    help="path to store the plots of the bayesian procedure. incl the .dat for latex plots")
# endregion

# region special mean convergence parameter
parser.add_argument("-mc", "--mc_samples", type=int, default=100,
                    help="# of mc_samples for the MC convergence")
parser.add_argument("-cpu", "--n_cpu", type=int, default=40,
                    help="# CPUs used for parallel computation")
parser.add_argument("-pot", "--sample_pot", action="store_true",
                    help="switch to use the potential sampling with -mc samples.")
parser.add_argument("-mean", "--sample_mean", action="store_true",
                    help="switch to use the mean sampling with -zmc samples.")
parser.add_argument("-post", "--sample_post_mean", action="store_true",
                    help="switch to use the mean sampling of the solution w.r.t to the posterior with -zmc samples.")
parser.add_argument("-dens", "--sample_density", action="store_true",
                    help="switch to use the density interpolation with -ng grid points.")
parser.add_argument("-set", "--export_setting", action="store_true",
                    help="switch to export mesh and gpcdegrees")
parser.add_argument("-plot", "--do_plot", action="store_true",
                    help="switch to create plots and store them in the plot directory")
# endregion

parser.add_argument("-log", "--log", action="store_true",
                    help="switch to use the lognormal sgfem forward operator")

args = parser.parse_args()
print("========== PARAMETERS: {}".format(vars(args)))
# endregion
# region Store Parser Arguments
#   region Parameters to use in stochastic Galerkin for the forward solution operator
max_rank = args.max_rank    		                # start ranks
sol_rank = args.sol_rank                            # maximal # of gpcdegrees
max_dofs = args.max_dofs                            # desired maximum of dofs to reach in refinement process
mesh_refine = args.mesh_refine                      # number of mesh refinements beforehand
iterations = args.iterations                        # used iterations in tt ALS
domain = args.domain                                # used domain, square, L-Shape, ...
coeffield_type = args.coeffield_type                # used coefficient field type
amp_type = args.amp_type                            # used amplification functions
decay_exp_rate = args.decay_exp_rate                # decay rate for non constant coefficient fields
field_mean = args.field_mean                        # mean value of the coefficient field
sgfem_gamma = args.sgfem_gamma                      # Gamma used in adaptive SG FEM
init_mesh_dofs = args.init_mesh_dofs                # initial mesh refinement. -1 : ignore
theta = args.theta                                  # Gaussian measure weight < 1
n_coefficients = args.n_coefficients                # number of coefficients (aka M)
rho = args.rho                                      # gaussian measure weight > 1
freq_skip = args.freq_skip                          # frequency skip in gpce
freq_scale = args.freq_scale                        # frequency scaling in gpce
scale = args.scale                                  # scalaing of the whole solution
hermite_degree = args.hermite_degree                # degree of the hermite polynomials
gpc_degree = args.gpc_degree                        # gpc degree
als_tol = args.als_tol                              # tolerance of the ALS algorithm

#   endregion
# region Observation operator parameters

n_samples_sqrt = args.n_samples_sqrt                # number of nodes squared
sample_coordinates = [[i/int(n_samples_sqrt+1), j/int(n_samples_sqrt+1)]
                      for i in range(1, n_samples_sqrt+1) for j in range(1, n_samples_sqrt+1)]
print("sample_coordinates: {}".format(sample_coordinates))

obs_precision = args.obs_precision                  # precision used for rounding in the observation operator
obs_rank = args.obs_rank                            # maximal rank to round to in the observation operator
# endregion
# region Inner Product parameter
inp_precision = args.inp_precision                  # precision used in the calculation of the inner product
inp_rank = args.inp_rank                            # maximal rank used in the calculation of the inner product
inp_gamma = args.inp_gamma                          # covariance constant value
# endregion
# region Euler Parameter
euler_precision = args.euler_precision              # precision to use in euler rounding
euler_rank = args.euler_rank                        # rank to round down in euler method
#                                                   # samples in every euler step
euler_local_mc_samples = args.euler_local_mc_samples
#                                                   # samples for the final result
euler_global_mc_samples = args.euler_global_mc_samples
euler_repetitions = args.euler_repetitions          # number of repetitions with half step size
euler_steps = args.euler_steps                      # start euler steps
#                                                   # global precision to reach after the euler scheme
euler_global_precision = args.euler_global_precision
euler_use_implicit = args.euler_use_implicit        # switch to use the implicit euler method
runge_method = args.runge_method                    # name of the runge kutta method to use if to used method explicit
runge_min_stepsize = args.runge_min_stepsize        # minimal step size to check in adaptive step size control setup
runge_max_stepsize = args.runge_max_stepsize        # maximal step size to check in adaptive step size control setup
if euler_use_implicit:
    euler_method = ImplicitEuler(euler_steps, euler_precision, euler_rank, euler_global_mc_samples, euler_repetitions,
                                 euler_local_mc_samples)
else:
    if True:
        euler_method = RankAdaptiveEmbeddedRungeKutta(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
                                                      euler_repetitions, euler_local_mc_samples)
        euler_method.method = runge_method
        euler_method.acc = euler_global_precision
        euler_method.min_stepsize = runge_min_stepsize
        euler_method.max_stepsize = runge_max_stepsize
        euler_method.start_rank = 6
        euler_method.max_rank = 30
        euler_method.rounding_safety = 10.0
    else:
        euler_method = EmbeddedRungeKutta(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
                                          euler_repetitions, euler_local_mc_samples)
        euler_method.method = runge_method
        euler_method.acc = euler_global_precision
        euler_method.min_stepsize = runge_min_stepsize
        euler_method.max_stepsize = runge_max_stepsize
# endregion
# region Density parameters
n_eval_grid = args.n_eval_grid                      # number of evaluation points for the density plots
eval_grid_points = np.linspace(-1, 1, n_eval_grid)  # grid for density plots
# endregion
# region setup collocation
Ny = args.Ny                                        # # of collocation points in each stochastic dimension
#                                                   # one dimensional stochastic grid of Chebyshev nodes
# stochastic_grid = np.array(get_cheb_points([Ny])[0])
# endregion

# region Logging
logging.config.fileConfig('logging.conf')           # load logger configuration
log = logging.getLogger('test_bayes')               # get new logger object
# endregion
# region Path check and creation
file_path = args.file_path
try:
    if not os.path.isdir(file_path):
        os.mkdir(file_path)
except OSError:
    log.error("can not create file path: {}".format(file_path))
    exit()

bayes_path = args.bayes_path
try:
    if not os.path.isdir(bayes_path):
        os.mkdir(bayes_path)
except OSError:
    log.error("can not create bayes path: {}".format(bayes_path))
    exit()

plot_path = args.plot_path
try:
    if not os.path.isdir(plot_path):
        os.mkdir(plot_path)
except OSError:
    log.error("Can not create plot path: {}".format(plot_path))
    exit()
# endregion
# region special mean convergence parameter
n_cpu = args.n_cpu
sample_pot = args.sample_pot
sample_mean = args.sample_mean
sample_post_mean = args.sample_post_mean
sample_density = args.sample_density
export_setting = args.export_setting
do_plot = args.do_plot
# endregion
log_normal = args.log
# endregion
# endregion

# region Object Initialisation
bayes_obj = None
coeff_field = None
proj_basis = None
true_values = None
prior_densities = None

bayes_fem_list = []
solution_error_h1_fem = []
solution_error_l2_fem = []
solution_error_h1_rel_fem = []
solution_error_l2_rel_fem = []
solution_tt_dofs_fem = []
solution_refinement_fem = []

mean_error_h1_fem = []
mean_error_h1_rel_fem = []
mean_error_l2_fem = []
mean_error_l2_rel_fem = []
mean_tt_dofs_fem = []
mean_refinements_fem = []

file_list_x = []
file_list_y = []
file_list_Z = []

poly_degree_p1 = []
poly_degree_p2 = []
poly_degree_p3 = []

fem_degrees = [1]
ranks = [5, 6, 7, 8, 9, 12, 14, 16, 20, 150]
used_items = [-1]

mc_samples = args.mc_samples
# endregion


class CacheItem(object):
    pass


bayes_obj_list = []  # list of Bayes objects due to multiple refinements in forward sol

for lib, rank in enumerate(ranks):
    z_mc_sample_list = []                           # list of mc samples
    x_achses = []                                   # x-achses list
    dofs = []                                       # we use the coordinate sampling
    stochastic_grid = []    # !!!!! needs replacement with GaussHermite
    # region Init Bayes Object
    bayes_obj = LognormalSGFEMBayes.init_solver(dofs, sample_coordinates, true_values, obs_precision,
                                                obs_rank, inp_precision, rank, inp_gamma, euler_method,
                                                euler_global_precision, eval_grid_points, prior_densities,
                                                stochastic_grid, maxrank=max_rank, n_coeff=n_coefficients,
                                                femdegree=1, gpcdegree=gpc_degree, decayexp=decay_exp_rate,
                                                gamma=sgfem_gamma, domain=domain, mesh_refine=mesh_refine,
                                                mesh_dofs=init_mesh_dofs, sol_rank=sol_rank, freq_skip=freq_skip,
                                                freq_scale=freq_scale, scale=scale, theta=theta, rho=rho,
                                                hermite_degree=hermite_degree, als_iterations=iterations,
                                                convergence=als_tol, coeffield_type="EF-square-cos",
                                                _print=False, field_mean=field_mean)
    cache = CacheItem()
    # endregion

    # for lia in range(used_item, used_item+1):
    # if fem_degree == 3:
    #     print "file path: {}".format(hash(bayes_obj.file_name))
    if bayes_obj.import_bayes(bayes_path):
        bayes_obj_list.append(copy.copy(bayes_obj))
    else:
        # region Setup truth
        if not log:
            log.info("Create {} true parameters".format(bayes_obj.forwardOp.coefficients))
            true_values = create_parameter_samples(bayes_obj.forwardOp.coefficients)
        else:
            log.info("Create {} true parameters".format(bayes_obj.forwardOp.n_coeff))
            true_values = create_parameter_samples(bayes_obj.forwardOp.n_coeff, lognormal=True, mu=0, sigma=1)
        bayes_obj.true_values = true_values
        # endregion
        # region Setup Gaussian Prior
        Gauss_prior = False
        if log:
            # !!!!!!!!!!!!!!!!!!!!!!!!!!!!
            prior_densities = [lambda x: 0.5 * np.ones_like(x)] * bayes_obj.forwardOp.n_coeff
        else:
            prior_densities = [lambda x: 0.5 * np.ones_like(x)] * bayes_obj.forwardOp.coefficients

        bayes_obj.prior_densities = prior_densities
        # endregion
        # region Setup noisy measurements
        log.info("Create {} measurements".format(len(sample_coordinates)))
        measurements = np.array([bayes_obj.forwardOp.compute_direct_sample_solution(true_values, cache=cache)
                                 (sample_coordinate)
                                 for sample_coordinate in sample_coordinates])
        log.info("  add noise to the measurements")
        for lic in range(len(measurements)):
            measurements[lic] += np.random.randn(1, 1)[0, 0] * inp_gamma

        bayes_obj.measurements = measurements
        # endregion
        print("start calculation for SOL[{}] / {} with rank {}".format(lib+1, len(ranks), rank))
        if sample_pot:
            bayes_obj.calculate_densities(stop_at_potential=True)
        else:
            bayes_obj.calculate_densities(stop_at_potential=False)
        bayes_obj_list.append(copy.copy(bayes_obj))
        log.info('export Bayes file %s', bayes_path + bayes_obj.file_name + '.dat')
        if not sample_pot:
            if not bayes_obj.export_bayes(bayes_path):
                log.error("can not save Bayes object")
        print("mr{} solution.n: {}".format(mesh_refine, bayes_obj.forwardOp.SOL[-1]["V"].n))
        print("mr{} solution.r: {}".format(mesh_refine, bayes_obj.forwardOp.SOL[-1]["V"].r))
        print("mr{} dofs: {}".format(mesh_refine, bayes_obj.forwardOp.SOL[-1]["DOFS"]))

# region Sample current forward solution error
solution_error_h1 = []
solution_error_l2 = []
solution_error_h1_rel = []
solution_error_l2_rel = []
refine_list = []
tt_dofs_list = []

for lia in range(len(ranks)):
    file_tt_dofs = []
    file_tt_refine = []
    file_list_y = []
    # region sample potential
    for lic, used_item in enumerate(used_items):
        file_tt_refine.append(used_item)

        bayes_obj = copy.copy(bayes_obj_list[lic + lia*(len(used_items))])
        if True:
            import tt
            print("re-round the tensor ({}, {}) / ({}, {}) index {}".format(lia, lic, len(ranks), len(used_items), lic + lia*(len(used_items))))
            bayes_obj.solution = tt.vector.round(bayes_obj.solution, 0, rmax=100000)
        file_tt_dofs.append(tt_dofs(bayes_obj.solution))
        print("current tensor specs:")
        print("dimensions: {}".format(bayes_obj.solution.n))
        print("ranks: {}".format(bayes_obj.solution.r))
        print("current forward tensor specs:")
        print("dimensions: {}".format(bayes_obj.forwardOp.SOL[-1]["V"].n))
        print("ranks: {}".format(bayes_obj.forwardOp.SOL[-1]["V"].r))
        print("start potential sampling with rank={}/{}, item={}/{} using {} steps on {} threads".format(ranks[lia],
                                                                                                         ranks[-1],
                                                                                                         used_item,
                                                                                                         used_items[-1],
                                                                                                         mc_samples,
                                                                                                         n_cpu))

        xi_samples = np.array([np.random.rand(bayes_obj_list[-1].forwardOp.coefficients+1, 1)[:, 0]*2 - 1
                               for _ in range(mc_samples)])
        #xi_samples =[]
        #if lic == 0:
        #    for lix in range(len(eval_grid_points)):
        #        print"step {}".format(lix)
        #        for lit in range(len(eval_grid_points)):
        #            xi_samples.append([eval_grid_points[lix] , eval_grid_points[lit]])
        #else:
        #    for lix in range(len(eval_grid_points)):
        #        print"step {}".format(lix)
        #        for lit in range(len(eval_grid_points)):
        #            for lim in range(len(eval_grid_points)):
        #                xi_samples.append([eval_grid_points[lix] , eval_grid_points[lit] , eval_grid_points[lim]])

        def _sample_pot_ref_parallel_tt(xi, _id):
            sol_buffer = bayes_obj_list[_id].forwardOp.sample_at_coeff(list(xi[:len(bayes_obj_list[_id].solution.n)-1]), sol_index=used_item)
            sol_measurements = np.array([sol_buffer(loc_dof) for loc_dof in bayes_obj_list[-1].sample_coordinates])
            # print("     (O o G)(u) at {0} = {1}".format(bayes_obj.sample_coordinates, sol_measurements))
            diff = np.array(bayes_obj_list[-1].measurements) - np.array(sol_measurements)
            # print("     given measurements: {}".format(bayes_obj.measurements))
            _pot_ref = -0.5*np.sum((diff ** 2) * (inp_gamma ** (-1)))
            ##### change me for single e wert calculation
            _pot = bayes_obj_list[_id].sample_potential(list(xi[:len(bayes_obj_list[_id].solution.n)-1]))
            return (_pot_ref-_pot)**2

        def _sample_pot_parallel_tt(xi, _id):
            _pot = bayes_obj_list[_id].sample_potential(list(xi[:len(bayes_obj_list[_id].solution.n)-1]))
            # _pot = bayes_obj_list[index].solution[list([0]*len(bayes_obj_list[index].solution.n))]
            # print("pot: {} vs. ref: {}".format(_pot, _pot_ref))
            #
            # if True:
            #    exit()
            return _pot
        #mc_sample_ref = np.sum(np.array(Parallel(n_cpu)(delayed(_sample_pot_ref_parallel_tt)(xi, lic+lia*(len(used_items)))
        #                                            for xi in xi_samples)))
        #mc_sample_ref = np.abs(mc_sample_ref)
        #mc_sample_ref *= mc_samples**(-1)
        #mc_sample_approx = np.sum(np.array(Parallel(n_cpu)(delayed(_sample_pot_parallel_tt)(xi, lic+lia*(len(used_items)))
        #                                            for xi in xi_samples)))
        #mc_sample_ref = np.abs(mc_sample_ref)
        #mc_sample_approx *= mc_samples**(-1)
        #file_list_y.append(np.abs(mc_sample_approx-mc_sample_ref))               # sampled Z (list[# mc samples])

        mc_sample = np.sum(np.array(Parallel(n_cpu)(delayed(_sample_pot_ref_parallel_tt)(xi, lic+lia*(len(used_items)))
                                                    for xi in xi_samples)))**0.5
        mc_sample *= mc_samples**(-1)
        file_list_y.append(mc_sample)
    # endregion

    # region Export mean error to file
    with open(plot_path + "pot_error_rank{}_log.dat".format(ranks[lia]), "w") as f:
        f.write("r, tt_dofs, tt_refine, err_pot\n")
        assert len(file_list_y) == len(used_items)
        assert len(file_tt_dofs) == len(used_items)
        assert len(file_tt_refine) == len(used_items)
        for lic in range(len(used_items)):
            f.write(repr(ranks[lia]) + "," +
                    repr(file_tt_dofs[lic]) + "," +
                    repr(file_tt_refine[lic]) + "," +
                    repr(file_list_y[lic]) +
                    "\n")
    # endregion
file_ranks = []
file_dofs = []
file_refine = []
file_err = []
for rank in ranks:
    with open(plot_path + "pot_error_rank{}_log.dat".format(rank), "r") as f:
        for lia, line in enumerate(f):
            if lia == 0:
                continue
            line_array = line.split(",")
            assert len(line_array) == 4
            file_ranks.append(int(line_array[0]))
            file_dofs.append(int(line_array[1]))
            file_refine.append(int(line_array[2]))
            file_err.append(float(line_array[3]))

for rank in ranks:
    with open(plot_path + "pot_error_ranks_log.dat".format(rank), "w") as f:
        f.write("r, tt_dofs, tt_refine, err_pot\n")
        for lia in range(len(file_ranks)):
            f.write(repr(file_ranks[lia]) + "," +
                    repr(file_dofs[lia]) + "," +
                    repr(file_refine[lia]) + "," +
                    repr(file_err[lia]) +
                    "\n")