import os
import cPickle as pickle
from alea.utils.plothelper import plot_mpl
from dolfin.cpp.io import File
try:
    WITH_MPL = False
    import matplotlib.pyplot as plt
except:
    WITH_MPL = False


def export_solution(SOL, iterations, refinements, polysys, femdegree, gpcps, domain, decayexp, gamma, rvtype,
                    basename="", max_mesh_cells=-1):
    # determine filename
    M = len(SOL[-1]["V"].r) - 1
    max_rank = max(SOL[-1]["V"].r)
    export_file = 'results/{0}-solution_M{1}_rank{2}_iter{3}_refine{4}_sys{5}_degD{6}_degS{7}_exp{8}_{9}'.format(basename, M, max_rank,
                                                                      iterations, refinements, polysys, femdegree, max(gpcps[-1]), decayexp, domain)
    export_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), export_file)

    for i, S in enumerate(SOL):
        v, CG = S["V"], S["CG"]
        # inject additional solution data
        v.CG = (CG.ufl_element().family(), CG.ufl_element().degree())
        del S["CG"]
        v.max_order = len(v.r) - 1
        v.max_rank = max(v.r)

        # export mesh of FunctionSpace
        mesh = CG.mesh()
        File(export_file + "-mesh-%i.xml" % i) << mesh

        # export mesh pdf
        if WITH_MPL and (max_mesh_cells == -1 or mesh.num_cells() <= max_mesh_cells):
            if i == 0:
                plt.figure(1)
                plt.gca().set_aspect('equal')
                plt.gca().xaxis.set_ticklabels([])
                plt.gca().yaxis.set_ticklabels([])
#                plt.show(block=False)
            plt.clf()
            plot_mpl(mesh)
#            plt.title('iteration %i' % i)
            plt.draw()
            plt.savefig(export_file + "-mesh-%i.pdf" % i, dpi=None, facecolor='w', edgecolor='w',
                        orientation='portrait', papertype=None, format='pdf',
                        transparent=False, bbox_inches='tight', pad_inches=0.1,
                        frameon=None)

    # write out
    INF = {"polysys": polysys, "femdegree": femdegree, "gpcps": gpcps,
           "domain": domain, "decayexp": decayexp, "gamma": gamma, "rvtype": rvtype}
    with open(export_file + '.dat', 'wb') as outfile:
        pickle.dump((SOL, INF), outfile, pickle.HIGHEST_PROTOCOL)

