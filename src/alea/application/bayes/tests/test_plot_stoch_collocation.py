# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  test script for the bayesian procedure using direct sampling of the Karhunen Loeve expansion
"""
# region Imports
from __future__ import division

import tt_als
import logging.config
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from Bayes_util.lib.bayes_lib import create_noisy_measurement_data_dofs
from Bayes_util.exponential.implicit_euler import ImplicitEuler
from scipy.integrate import quad
from scipy.interpolate import interp1d

from Bayes_util.asgfem_bayes import ASGFEMBayes
from Bayes_util.exponential.embedded_runge_kutta import EmbeddedRungeKutta
from Bayes_util.lib.bayes_lib_simple import create_unique_dofs
from Bayes_util.lib.interpolation_util import get_cheb_points

from scipy.interpolate import interp2d
from scipy.integrate import dblquad
__author__ = "marschall"
# endregion


METHODS = ["", "measurements"]
METHOD = METHODS[1]
# region Setup parameters
#############################
#   Testsets
#   Set 1   for Method quadrature
#       refinements = 1
#       srank = 8
#       gpcdegree = 10
#       femdegree = 3
#       max_dofs = 1e5
#       iterations = 500
#       n_coefficients = 10
#       init_mesh_dofs = 1e4
#
#   Set 2   for method euler
#       refinements = 1
#       srank = 8
#       gpcdegree = 10
#       femdegree = 3
#       max_dofs = 1e5
#       iterations = 600
#       n_coefficients = 4
#       init_mesh_dofs = 1e4
#
#   Set 3
#       refinements = 10
#       srank = 8
#       gpcdegree = 10
#       femdegree = 3
#       max_dofs = 1e7
#       iterations = 500
#       n_coefficients = 10
#       init_mesh_dofs = 1e5
#
#   Set 4
#       refinements = 1
#       srank = 8
#       gpcdegree = 10
#       femdegree = 1
#       max_dofs = 1e5
#       iterations = 500
#       n_coefficients = 4
#       init_mesh_dofs = 1e4
# Test for sol convergence
#   Set 5
#       refinements = 15
#       srank = 8
#       gpcdegree = 10
#       femdegree = 3
#       max_dofs = 1e8
#       iterations = 600
#       n_coefficients = 5
#       init_mesh_dofs = 10
# Test for sol convergence with fix stoch dimension
#   Set 6
#       refinements = 10
#       srank = 8
#       gpcdegree = 10
#       femdegree = 3
#       max_dofs = 1e8
#       iterations = 600
#       n_coefficients = 5
#       init_mesh_dofs = 10
#   region Parameters to use in stochastic Galerkin for the backward solution
refinements = 5 			                        # # of refinement steps in sg
srank = 8					                        # start ranks
gpcdegree = 10					                    # maximal # of gpcdegrees
femdegree = 1					                    # fem function degree
max_dofs = 1e5                                      # desired maximum of dofs to reach in refinement process
iterations = 501                                    # used iterations in tt ALS
polysys = "L"                                       # used polynomial system
domain = "square"                                   # used domain, square, L-Shape, ...
# coeffield_type = "EF-square-cos"
# coeffield_type = "EF-square-sin"
# coeffield_type = "monomial"
# coeffield_type = "linear"
# coeffield_type = "constant"                       # used coefficient field type
coeffield_type = "cos"
amp_type = "constant"
# amp_type = "decay-inf"
field_mean = 2
decay_exp_rate = 0                                  # decay rate for non constant coefficient fields
sgfem_gamma = 0.9                                   # Gamma used in adaptive SG FEM
rvtype = "uniform"
init_mesh_dofs = 10                                # initial mesh refinement. -1 : ignore
thetax = 0.5                                        # factor of deterministic refinement (0 : no refinement)
thetay = 0.5                                        # factor of stochastic refinement (0 : no refinement)
#   endregion

# region Solution parameter
n_coefficients = 2                                  # number of coefficients
USE_DOFS = False
n_dofs = 54                                         # number of dofs
n_dofs_list = [2**lia + 2 for lia in range(6)]      # list of increasing dofs

sample_coordinates = []
measurement_refinements = None
if METHOD == "measurements":
    measurement_refinements = 2                     # number of refinements, i.e. number of nodes squared start from 4
    for lia in range(measurement_refinements):
        sample_coordinates.append([[i/int(np.sqrt((lia+3)**2)+1), j/int(np.sqrt((lia+3)**2)+1)]
                                   for i in range(1, int(np.sqrt((lia+3)**2)))
                                  for j in range(1, int(np.sqrt((lia+3)**2)))])
else:
    # sample_coordinates = [[0.25, 0.25], [0.25, 0.5], [0.25, 0.75],
    #                      [0.5, 0.25],  [0.5, 0.5],  [0.5, 0.75],
    #                      [0.75, 0.25], [0.75, 0.5], [0.75, 0.75]]

    #sample_coordinates = [[1/7, 1/7], [1/7, 2/7], [1/7, 3/7], [1/7, 4/7], [1/7, 5/7], [1/7, 6/7],
    #                      [2/7, 1/7], [2/7, 2/7], [2/7, 3/7], [2/7, 4/7], [2/7, 5/7], [2/7, 6/7],
    #                      [3/7, 1/7], [3/7, 2/7], [3/7, 3/7], [3/7, 4/7], [3/7, 5/7], [3/7, 6/7],
    #                      [4/7, 1/7], [4/7, 2/7], [4/7, 3/7], [4/7, 4/7], [4/7, 5/7], [4/7, 6/7],
    #                      [5/7, 1/7], [5/7, 2/7], [5/7, 3/7], [5/7, 4/7], [5/7, 5/7], [5/7, 6/7],
    #                      [6/7, 1/7], [6/7, 2/7], [6/7, 3/7], [6/7, 4/7], [6/7, 5/7], [6/7, 6/7]
    #                      ]
    sample_coordinates = [[i/int(np.sqrt((9)**2)+1), j/int(np.sqrt((9)**2)+1)]
                                   for i in range(1, int(np.sqrt((8)**2)))
                                  for j in range(1, int(np.sqrt((8)**2)))]
# endregion

# region Observation parameter
obs_precision = 1e-15
obs_rank = 100
# endregion

# region Inner Product parameter
inp_precision = 1e-15
inp_rank = 100
inp_gamma = 1e-5
# endregion

# region Euler Parameter
euler_precision = 1e-10                             # precision to use in euler rounding
euler_rank = 10                                     # rank to round down in euler method
euler_local_mc_samples = 1                          # samples in every euler step
euler_global_mc_samples = 1000                      # samples for the final result
euler_repetitions = 0                               # number of repetitions with halfed step size
euler_refinements = None
euler_steps = 1000                                  # start euler steps
euler_global_precision = 1e-8                       # global precision to reach after the euler scheme
euler_use_implicit = False
# endregion

# region Density parameters
n_eval_grid = 10
# endregion

# region setup discretisation
Nx, Ny = 10000, 10
dx, dy = 1/Nx, 1/Ny
eval_grid_points = np.linspace(-1, 1, n_eval_grid)
quadrature_refinements = None

stochastic_grid = np.array(get_cheb_points([Ny] * n_coefficients)[0])
# endregion

# region MCMC parameter
n_walker = 50
burn_in = 10
n_samples = 100
covariance = 0.0001
# endregion

color_list = ['olive', 'gold', 'aqua', 'black', 'blue', 'brown', 'green', 'red', 'orange']

logging.config.fileConfig('logging.conf')           # load logger configuration
log = logging.getLogger('test_bayes')               # get new logger object

# endregion
file_path = "results/paper/collocation/" + amp_type + "/"
if thetax == 1:
    file_path += "fix/"

bayes_obj = None
bayes_obj_list = []
unique_dofs = []
coeff_field = None
proj_basis = None
true_values = None
prior_densities = None

true_solution = None
true_field = None

field_error_h1 = []
field_error_l2 = []
solution_error_h1 = []
solution_error_l2 = []
tt_dofs = []
list_z_means = []
list_true_z_means = []
log.info("Create exponential method")
if euler_use_implicit:
    euler_method = ImplicitEuler(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
                                 euler_repetitions, euler_local_mc_samples)
else:
    euler_method = EmbeddedRungeKutta(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
                                      euler_repetitions, euler_local_mc_samples)
    euler_method.method = "forth"
    euler_method.acc = euler_global_precision
    euler_method.min_stepsize = 1e-10
    euler_method.max_stepsize = 1.0 - euler_method.min_stepsize
plt.clf()
physical_test_nodes = [
    [0.25, 0.25], [0.3, 0.8], [0.75, 0.1], [0.6, 0.5]
]
dofs = []                                   # we use the coordinate sampling
for lia_measurements in range(measurement_refinements):
    log.info("Create Bayes Object with {} measurement nodes".format(len(sample_coordinates[lia_measurements])))
    bayes_obj = ASGFEMBayes.init_solver(dofs, sample_coordinates[lia_measurements], true_values, obs_precision,
                                        obs_rank, inp_precision, inp_rank, inp_gamma, euler_method,
                                        euler_global_precision, eval_grid_points, prior_densities,
                                        stochastic_grid,
                                        iterations=iterations, refinements=refinements, max_dofs=max_dofs,
                                        thetax=thetax,
                                        thetay=thetay, srank=srank,  m=n_coefficients+1,
                                        femdegree=femdegree, gpcdegree=gpcdegree, polysys=polysys,
                                        gamma=sgfem_gamma, rvtype=rvtype, domain=domain,
                                        mesh_dofs=init_mesh_dofs, problem=0,
                                        coeffield_type=coeffield_type,
                                        amp_type=amp_type, field_mean=field_mean
                                        )
    for lia in range(len(bayes_obj.forwardOp.SOL)):
        bayes_obj.set_sol_index(lia)
        if bayes_obj.import_bayes(file_path):
            bayes_obj_list.append(bayes_obj)
        else:
            log.info('start Bayesian procedure with {} measurement nodes'.format(
                len(sample_coordinates[lia_measurements])))
            # region  setup truth, noise and prior
            log.info("Create {} true parameters".format(bayes_obj.forwardOp.coefficients))
            # true_values = create_parameter_samples(bayes_obj.forwardOp.coefficients)
            # true_values = [0, 0.2, -0.2, 0.4]
            true_values = [0.2, -0.4, 0, 0.3, 0.4, 0.5, -0.1, -0.2, -0.3, 0.1, -0.5, 0.05, -0.05, 0.6, 0.7, 0.8, 0.9,
                           -0.6, -0.7, -0.8, -0.9, 0, 0.04]
            if USE_DOFS:
                unique_dofs = np.array(create_unique_dofs(n_dofs_list[-1], init_mesh_dofs))
            # endregion

            # region Setup Gaussian Prior
            Gauss_prior = False
            if Gauss_prior:
                raise NotImplemented
                # pi_y =
                # [lambda x: 1/(np.sqrt(2*np.pi)*sigma_i)*np.exp(-0.5*((x-mu_i)/sigma_i)**2) for mu_i, sigma_i in
                # zip(mu, sigma)]
            else:
                prior_densities = [lambda x: 0.5 * np.ones_like(x)] * bayes_obj.forwardOp.coefficients
            # endregion
            if USE_DOFS:
                measurements = create_noisy_measurement_data_dofs(bayes_obj.solution, true_values, dofs, inp_gamma)
            else:
                buffer_true_solution = bayes_obj.forwardOp.compute_direct_sample_solution(true_values[:len(bayes_obj.solution.n)-1])
                measurements = np.array([buffer_true_solution(sample_coordinate)
                                         for sample_coordinate in sample_coordinates[lia_measurements]])
            # measurements = create_noisy_measurement_data_dofs(bayes_obj.solution, true_values, dofs, inp_gamma)
            bayes_obj.measurements = measurements
            bayes_obj.true_values = true_values[:len(bayes_obj.solution.n)-1]
            bayes_obj.prior_densities = prior_densities
            bayes_obj.calculate_densities(stop_at_collocation=True)
            if not bayes_obj.export_bayes(file_path):
                log.error("can not save bayes object")
#            bayes_obj_list.append(bayes_obj)
        print("Bayes obj {} loaded from file".format(lia))
        plt.clf()
        error_list = []
        from dolfin.fem.norms import errornorm, norm
        for lib in range(len(stochastic_grid)):
            for lic in range(len(stochastic_grid)):
                print("sample at ({}, {})".format(lib, lic))
                # noinspection PyProtectedMember
                approx_solution = bayes_obj_list[-1].sample_collocation([lib, lic])._fefunc
                # noinspection PyProtectedMember
                true_solution = bayes_obj_list[-1].forwardOp.sample_at_coeff([stochastic_grid[lib],
                                                                     stochastic_grid[lic]], sol_index=lia)._fefunc
                print("approx(0.2) = {}, true(0.2) = {}".format(approx_solution([0.2,0.2]), true_solution([0.2, 0.2])))
                #from dolfin import plot
                #plot(true_solution)
                #plot(approx_solution, interactive=True)
                error_list.append(errornorm(true_solution, approx_solution, norm_type='L2')/norm(true_solution))
                print "ERROR({},{})={} for tt_dofs {}".format(lib, lic, error_list[-1], bayes_obj.forwardOp.SOL[lia]["DOFS"])

        plt.semilogy(error_list)
        plt.savefig(file_path + "errorL2_ASGFEM{}_Measure{}".format(lia, len(bayes_obj.measurements)) +
                    str(hash(bayes_obj.file_name)) + ".png")
        plt.clf()
