from alea.application.egsz.affine_field import AffineField
from dolfin import UnitSquareMesh, cells
import numpy as np

# Test script for the affine field and how scaling interacts with the a negative mean value.
# The mean is scaled as well!

coeffield_type = "EF-square-cos"
amptype = "decay-inf"
decayexp = 2
gamma = 0.9
freq_scale = 1.0
freq_skip = 0
scale = 10.0
field_mean = -0.1

N = 5                                               # number of nodes in every dimension of physical grid
n_coef = 5                                          # length of affine parametrisation
iters = 4                                           # number of samples to look at

mesh = UnitSquareMesh(N, N)                         # create unit square mesh from dolfin
#                                                   # construct alea affine field
af = AffineField(coeffield_type, amptype=amptype, decayexp=decayexp, gamma=gamma, freqscale=freq_scale,
                 freqskip=freq_skip, scale=scale, coef_mean=field_mean)

mp = np.zeros((mesh.num_cells(), 2))                # get cell midpoints
for ci, c in enumerate(cells(mesh)):
    mpi = c.midpoint()
    mp[ci] = mpi.x(), mpi.y()

for lia in range(iters):
    y = list(np.array(af.sample_rvs(n_coef)))       # create sample
    values = af(mp, y)                              # obtain values at center points for the given sample
    print(values)
    print("\n ++++++++++++ \n")
