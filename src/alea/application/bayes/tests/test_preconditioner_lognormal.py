# -*- coding: latin-1 -*-
"""
  adaptive stochastic Galerkin finite element method for parametric partial differential equations
  @author: M. Eigel, M Marschall, M. Pfeffer,

"""

# region Imports
from __future__ import division                     # float division as standard
from copy import deepcopy

# region ALEA imports
from alea.application.egsz.sample_problems2 import SampleProblem
from alea.application.egsz.sample_domains import SampleDomain
from alea.application.egsz.affine_field import AffineField
from alea.fem.fenics.fenics_basis import FEniCSBasis
from alea.fem.fenics.fenics_vector import FEniCSVector
from alea.application.egsz.tt_residual_estimator import ttEvaluateResidualEstimator
from alea.application.egsz.tt_tail_estimator import ttEvaluateUpperTailBound, ttMark_y
from alea.utils.timing import get_current_time_string
# endregion
# region tensor SGFEM discretisation
#                                                   # deterministic and stochastic operator creation
from alea.application.tt_asgfem.apps.sgfem_als_util import (compute_FE_matrices,  compute_FE_matrix_coeff)
# endregion

# region FEniCS imports
#                                                   # used FEniCS functions
from dolfin import FunctionSpace, Function, interpolate, refine, project, cells, MeshFunction, Mesh
#                                                   # currently unused FEniCS functions
# from dolfin import Mesh, Constant, set_log_level, WARNING, DEBUG, INFO, interactive, File
# endregion
# region TT ALS
import tt                                           # Oseledets TT tensor library
import alea.application.tt_asgfem.tensorsolver.tt_sparse_matrix as ttsmat
from alea.application.tt_asgfem.tensorsolver.tt_lognormal import (generate_lognormal_tt, sample_lognormal_tt,
                                                                  generate_operator_tt, extract_mean,
                                                                  get_coeff_upper_bound, tt_cont_coeff,
                                                                  sample_cont_coeff, fully_disc_first_core)
#                                                   # ALS solver for parametric PDEs
from alea.application.tt_asgfem.tensorsolver.tt_param_pde import ttParamPDEALS
#                                                   # orthogonalization and raveled (vectorized)
from alea.application.tt_asgfem.tensorsolver.tt_util import ttRightOrthogonalize, ravel
#                                                   # adaptive reaction methods
from alea.application.tt_asgfem.tensorsolver.tt_sgfem_util import (evaluate_residual_mean_energy_norm, tt_dofs,
                                                                   tt_add_stochastic_dimension, increase_tt_rank,
                                                                   ttRandomDeterministicRankOne)
from alea.application.tt_asgfem.tensorsolver.tt_als import ttALS
from alea.application.tt_asgfem.tensorsolver.tt_util import reshape, ttNormalize

# endregion
# region useful python packages
import numpy as np                                  # standard numpy library
from numpy.testing import assert_almost_equal       # gets the almost equal operator for numpy arrays
from operator import itemgetter                     # enables items to pass the __getitem__() method
# import argparse                                   # enable argument parsing from command line
from alea.utils.tictoc import TicToc                # time measurement library https://github.com/tsroten/ticktock
from math import isnan                              # get the isnan operator to check values for existing
import logging
import logging.config
import cPickle as Pickle
# endregion
# region System settings
import scipy.sparse as sps
import sys                                          # import system library
sys.getrecursionlimit()                             # prepare recursion limit
sys.setrecursionlimit(10000)                        # increase limit for large M > 100
# endregion

# endregion

# region Function: refine mesh deterministic log normal


def refine_mesh_deterministic_log(eta_global, summed_eta_local, CG, thetaX, V, _pde, _femdegree):
    """
    refine the mesh adaptive
    @param eta_global:
    @param summed_eta_local:
    @param CG:
    @param thetaX:
    @param V:
    @param do_timing:
    @param _pde:
    @param _femdegree:
    @return:
    """
    from operator import itemgetter  # enables items to pass the __getitem__() method
    from dolfin import MeshFunction
    # sort error indicators
    eta_local_ind = [(x, i) for i, x in enumerate(summed_eta_local)]
    eta_local_ind = sorted(eta_local_ind, key=itemgetter(0), reverse=True)

    # setup marking set
    mesh = CG.mesh()
    cell_markers = MeshFunction("bool", mesh, mesh.topology().dim())
    cell_markers.set_all(False)
    marked_eta, cc = 0.0, 0

    for eta_cell in eta_local_ind:
        # break if sufficiently many cells are selected
        if thetaX * eta_global ** 2 <= marked_eta:
            break
        cell_markers[eta_cell[1]] = True
        marked_eta += eta_cell[0] ** 2
        cc += 1
    # print "+++++++ cells marked: %i/%i" % (cc, mesh.num_cells()), "\theta_global", eta_global, "
    # \tmarked_eta", marked_eta

    with TicToc(key="**** project solution ****", active=False, do_print=False):
        F = Function(CG)
        mesh = refine(mesh, cell_markers)
        CG = _pde.function_space(mesh, _femdegree)
        # cores = V.to_list(V)
        cores = V
        r = cores[0].shape[2]
        newcore = np.zeros([1, CG.dim(), r])
        for d in range(r):
            # print "++++++++", CG.dim(), cores[0][0, :, d].shape
            F.vector().set_local(cores[0][0, :, d])
            # print "refinement", d, F.vector().array().shape,CG.dim()
            newF = interpolate(F, CG)
            newcore[0, :, d] = newF.vector().array()
        # update U
        cores[0] = newcore
        V = tt.vector.from_list(cores)
    return V, mesh


# endregion

# region Function: Residual estimator evaluation


def evaluate_residual_estimator(V, D, CG, coeff_field, f, _mesh, gpcp=None, assemble_quad_degree=15, do_timing=True,
                                ncpu=1):
    """
    calculates the error estimator corresponding to the residual
    @param V: TT-Tensor solution object
    @param D:
    @param CG:
    @param coeff_field: coefficient field for stochastic representation
    @param f: rhs
    @param _mesh: finite dimensional mesh
    @param gpcp: general polynomial chaos polynomials
    @param assemble_quad_degree: degree of used quadrature
    @param do_timing:
    @return:
    """
    fp = ''
    try:
        _f = open('logging.conf')
        _f.close()
        fp = 'logging.conf'
    except IOError:
        print("logging not in working directory")
        try:
            _f = open('/home/marschall/WIAS/alea-testing/src/alea/application/bayes/logging.conf')
            _f.close()
            fp = '/home/marschall/WIAS/alea-testing/src/alea/application/bayes/logging.conf'
        except IOError:
            print("No logging file found. -> exit()")
            exit()
    logging.config.fileConfig(fp)
    log = logging.getLogger('test_bayes')           # get new logger object
    summed_eta_local, eta_global = None, 0

    with TicToc(key="**** TT residual estimator ****", active=do_timing, do_print=False):
        eta_res_local, eta_res_global = ttEvaluateResidualEstimator(V, D, CG, coeff_field, f,
                                                                    assemble_quad_degree=assemble_quad_degree,
                                                                    with_volume=True, with_edge=True, ncpu=ncpu)

    # print ">>>>>>>>>>>>>>eta_res_global>>>>>>>>>>>>", eta_res_global, np.sqrt(sum(eta_res_local**2))
    assert_almost_equal(eta_res_global, np.sqrt(sum(eta_res_local**2)))
    if summed_eta_local is None:
        summed_eta_local = eta_res_local
    else:
        summed_eta_local = np.sqrt(summed_eta_local**2 + eta_res_local**2)
    eta_global += eta_res_global
    # print eta_global, np.sqrt(sum(summed_eta_local**2))
    assert_almost_equal(eta_global, np.sqrt(sum(summed_eta_local**2)))

    return eta_global, eta_res_global, eta_res_local, summed_eta_local
# endregion

# region Function: refine mesh deterministic


def refine_mesh_deterministic(eta_global, summed_eta_local, CG, thetaX, V, do_timing, _pde, _femdegree):
    """
    refine the mesh adaptive
    @param eta_global:
    @param summed_eta_local:
    @param CG:
    @param thetaX:
    @param V:
    @param do_timing:
    @param _pde:
    @param _femdegree:
    @return:
    """
    # sort error indicators
    eta_local_ind = [(x, i) for i, x in enumerate(summed_eta_local)]
    eta_local_ind = sorted(eta_local_ind, key=itemgetter(0), reverse=True)

    # setup marking set
    mesh = CG.mesh()
    cell_markers = MeshFunction("bool", mesh, mesh.topology().dim())
    cell_markers.set_all(False)
    marked_eta, cc = 0.0, 0

    for eta_cell in eta_local_ind:
        # break if sufficiently many cells are selected
        if thetaX * eta_global**2 <= marked_eta:
            break
        cell_markers[eta_cell[1]] = True
        marked_eta += eta_cell[0] ** 2
        cc += 1
    # print "+++++++ cells marked: %i/%i" % (cc, mesh.num_cells()), "\theta_global", eta_global, "
    # \tmarked_eta", marked_eta

    with TicToc(key="**** project solution ****", active=do_timing, do_print=False):
        F = Function(CG)
        mesh = refine(mesh, cell_markers)
        CG = _pde.function_space(mesh, _femdegree)
        cores = V.to_list(V)
        r = cores[0].shape[2]
        newcore = np.zeros([1, CG.dim(), r])
        for d in range(r):
            # print "++++++++", CG.dim(), cores[0][0, :, d].shape
            F.vector().set_local(cores[0][0, :, d])
            # print "refinement", d, F.vector().array().shape,CG.dim()
            newF = interpolate(F, CG)
            newcore[0,:,d] = newF.vector().array()
        # update U
        cores[0] = newcore
        V = tt.vector.from_list(cores)
    return V, mesh

# endregion

# region Function: stochastic Galerkin with ALS for lognormal coefficient


def log_normal_sgfem_als(maxrank=50, n_coeff=15, femdegree=2, gpcdegree=3, decayexp=2, gamma=0.9, amptype="decay-inf",
                         domain="square", mesh_refine=1, mesh_dofs=4500, sol_rank=5, freq_skip=0,
                         freq_scale=1.0, scale=1.0, theta=0.01, rho=1.0, hermite_degree=5, als_iterations=1000,
                         convergence=1e-12, coeffield_type="EF-square-cos-algebraic", _print=False, field_mean=0.0,
                         coef_acc=1e-10, iterations=10, theta_x=0.5,
                         theta_y=0.0, start_rank=1, n_coef_coef=20,
                         max_hdegs_coef=20, eta_zeta_weight=0.1, resnorm_zeta_weight=1, coef_mesh_dofs=3000,
                         coef_quad_degree=7, rank_always=True, max_dofs=1e6, new_hdeg=4, coef_mesh_cache=None,
                         cont_coef_cache=None):

    # region Switches to show additional info
    plot_coef_samples = 0                            # plot coef samples to file
    plot_coef_terminate = False                     # termination switch for coef sample plotting
    print_min_of_coef_samples = 0                   # if < 1 nothing is shown. If x>0, the minimum value of x coef
    #                                               # samples is printed to console
    print_min_of_coef_samples_terminate = False
    quant_factor = 1

    n_samples = max(print_min_of_coef_samples, plot_coef_samples)

    # endregion

    use_preconditioner = False
    # region Setup domain and mesh
    # ###############################################
    # A: setup domain and meshes
    # ###############################################

    if mesh_dofs > 0:                               # mesh dofs are defined
        mesh_refine = 0                             # there are no mesh refinements
    #                                               # call sampleDomain and get boundaries, mesh and dimension

    mesh, boundaries, dim = SampleDomain.setupDomain(domain, initial_mesh_N=2)
    #                                               # refine mesh according to input
    mesh = SampleProblem.setupMesh(mesh, num_refine=mesh_refine)
    #                                               # further refinement if you want to have a specific amount of dofs
    from dolfin import FunctionSpace                # WTF here again? Why doesn't python detect the import in the head
    print("soll mesh_dofs={} haben dofs={}".format(mesh_dofs, FunctionSpace(mesh, 'CG', femdegree).dim()))
    while mesh_dofs > 0 and FunctionSpace(mesh, 'CG', femdegree).dim() < mesh_dofs:
        mesh = refine(mesh)                         # uniformly refine mesh
    # logger.info("initial mesh has %i dofs with p%i FEM" % (FunctionSpace(mesh, 'CG', femdegree).dim(), femdegree))
    #                                               # obtain all coordinates of degrees of freedom
    mp = np.array(FunctionSpace(mesh, 'DG', femdegree-1).tabulate_dof_coordinates())
    mp = mp.reshape((-1, mesh.geometry().dim()))
    # endregion

    # region Construct coefficient
    # ##############################################
    # B construct coefficient
    # ##############################################

    import cPickle as Pickle
    try:
        coef_mesh_cache = Mesh('{}-{}-mesh.xml'.format(str(domain), str(scale)))
    except RuntimeError:
        coef_mesh_cache = None
    try:
        infile = open('{}-{}-{}-cont_coef_cores.dat'.format(domain, str(scale), str(field_mean)),
                      'rb')
        cont_coef_cache = Pickle.load(infile)
        infile.close()
    except RuntimeError:
        cont_coef_cache = None
    except IOError:
        cont_coef_cache = None

    # region Construct affine Field for exponent and PDE
    #                                               # create affine coefficient field as logarithm of the used field
    af = AffineField(coeffield_type, amptype=amptype, decayexp=decayexp, gamma=gamma, freqscale=freq_scale,
                     freqskip=freq_skip, scale=scale, coef_mean=field_mean)
    #                                               # coefficient mean is 0 anyway. therefore do not set it later
    #                                               # prepare the PDE problem
    pde, _, _, _, _, _ = SampleProblem.setupPDE(2, domain, 0, boundaries, af.coeff_field)
    # endregion
    # region define hermite degrees, tensor ranks and evaluate field for old version of coefficient tensor
    hdegs = n_coeff * [hermite_degree]
    hdegs_coef = n_coef_coef * [max_hdegs_coef]
    # hdegs_coef = [6, 6, 6, 6, 6, 5, 5, 5, 5, 4, 4, 3, 3, 3, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1]
    n_coef_coef = len(hdegs_coef)
    ranks = [1] + n_coeff * [start_rank] + [1]      # create rank vector (1, MAXRANK, MAXRANK, ..., 1)
    ranks_coef = [1] + len(hdegs_coef) * [maxrank] + [1]
    B = af.evaluate_basis(mp, n_coef_coef)          # evaluate physical basis functions at cell midpoints
    # endregion

    # region Calculate scaling parameter bmax
    # TODO: better bxmax for other amp types
    if coeffield_type == 'EF-square-cos':
        from scipy.special import zeta

        start = SampleProblem.get_decay_start(decayexp, gamma)
        amp = gamma / zeta(decayexp, start)
        bxmax_m = [scale * amp * (i + start) ** (-decayexp) for i in range(B.shape[1]-1)]
        # print bxmax_M
        bxmax = np.array(bxmax_m)
    elif coeffield_type == 'EF-square-cos-algebraic':
        start = SampleProblem.get_decay_start(decayexp, gamma)
        amp = 1
        bxmax_m = [scale * amp * (i + start) ** (-decayexp) for i in range(B.shape[1]-1)]
        # print bxmax_M
        bxmax = np.array(bxmax_m)
    else:
        bxmax = get_coeff_upper_bound(B)

    print("bxmax: {}".format(bxmax))
    print("old bxmax: {}".format(get_coeff_upper_bound(B)))
    # endregion
    # region Create old version tensor (not used anymore)
    #                                               # generate log normal affine coefficient (old version for reference)
    # U = generate_lognormal_tt(B, hermite_degree, deepcopy(ranks_coef), bxmax, theta=theta, rho=rho)
    # endregion
    # region Create reduced basis coefficient tensor
    ###############################################
    # Semi Discretized Method
    ###############################################
    #                                               # create semi discrete coefficient with reduced basis approach
    with TicToc(sec_key="Create coefficient", key="**** CreateSemiDiscreteCoefficient ****", active=True,
                do_print=True):
        if cont_coef_cache is None:
            cont_coeff_cores = tt_cont_coeff(af, mesh, n_coef_coef, ranks_coef, hdegs_coef, bxmax, theta=theta, rho=rho,
                                             acc=coef_acc, mesh_dofs=coef_mesh_dofs, quad_degree=coef_quad_degree,
                                             coef_mesh_cache=coef_mesh_cache, domain=domain, scale=scale)
            import cPickle as Pickle
            outfile = open('{}-{}-{}-cont_coef_cores.dat'.format(domain, scale, field_mean), 'wb')
            Pickle.dump(cont_coeff_cores, outfile, Pickle.HIGHEST_PROTOCOL)
            outfile.close()
        else:
            # cont_coeff_cores = tt.vector.to_list(tt.vector.round(tt.vector.from_list(cont_coef_cache), 1e-16))
            cont_coeff_cores = cont_coef_cache
            ranks_coef = [1] + list(tt.vector.from_list(cont_coeff_cores).r)
            hdegs_coef = tt.vector.from_list(cont_coeff_cores).n
    # endregion
    # endregion

    # region Create or Load Samples
    import cPickle as Pickle
    try:
        raise ValueError
        f = open('y-list-file.dat', 'rb')
        sample_y_list = Pickle.load(f)
        f.close()
        if len(sample_y_list) < n_samples:
            for lia in range(len(sample_y_list), n_samples):
                sample_y_list.append(af.sample_rvs(len(hdegs_coef)))
        f = open('y-list-file.dat', 'wb')
        Pickle.dump(sample_y_list, f, Pickle.HIGHEST_PROTOCOL)
        f.close()
    except:
        sample_y_list = []
        for lia in range(n_samples):
            sample_y_list.append(af.sample_rvs(len(hdegs_coef)))
        f = open('y-list-file.dat', 'wb')
        Pickle.dump(sample_y_list, f, Pickle.HIGHEST_PROTOCOL)
        f.close()

    # endregion

    # region old sample method (not used anymore)
    # y = af.sample_rvs(n_coeff)                      # generate sample y and evaluate KL at y
    # V = af(mp, y)                                   # get coefficient function at sample points
    # V = np.exp(V)                                   # calculate the exponential values
    # print "V", V, "y", y

    # bxmax = get_coeff_upper_bound(B)                # get the upper bound for the coefficient field
    #                                               # create samples from tt lognormal coefficient field
    # VV = sample_lognormal_tt(U, y, bxmax, theta, rho)
    #                                               # check the error
    # print "coefficient sample approx error", np.linalg.norm(V-VV)

    # def evaluate_realisation_error(mp, y, U):
    #     V1 = af(mp, y)  # exact values at mp
    #     V1 = np.exp(V1)
    #     V2 = sample_lognormal_tt(U, y, bxmax, theta, rho)  # TT values at mp
    #     V3 = sample_cont_coeff(cont_coeff_cores, af, mp, y, ranks_coef, hdegs_coef, bxmax, theta, rho)
    #     return np.linalg.norm(V1 - V2), np.linalg.norm(V1 - V3), np.linalg.norm(V2 - V3)
    '''
    N = 10
    coeff_err1 = 0
    coeff_err2 = 0
    coeff_err3 = 0
    for _ in range(N):
        err1, err2, err3 = evaluate_realisation_error(mp, af.sample_rvs(n_coeff), U)
        coeff_err1 += err1 ** 2
        coeff_err2 += err2 ** 2
        coeff_err3 += err3 ** 2
    coeff_err1 = np.sqrt(coeff_err1)
    coeff_err2 = np.sqrt(coeff_err2)
    coeff_err3 = np.sqrt(coeff_err3)
    print("MC sample error {}, {}, {}".format(coeff_err1, coeff_err2, coeff_err3))
    '''
    # endregion

    # region define sigma as scaling parameter
    sigma = np.exp(-theta*rho*bxmax)
    # print("sigma: {}".format(sigma))
    # print("theta: {}, rho: {}".format(theta, rho))
    # endregion

    sol_list = []

    rank = start_rank
    base_change_cache = None
    # region optional: create some information about the coefficient field
    if print_min_of_coef_samples or plot_coef_samples:
        # region Refine solution mesh to coef mesh resolution
        with TicToc(key="  1. Sample coefficient field: create Mesh", active=True, do_print=True):
            if coef_mesh_cache is None:
                mesh2 = mesh  # refine(mesh)
                while FunctionSpace(mesh2, 'DG', 0).dim() < coef_mesh_dofs:
                    mesh2 = refine(mesh2)  # uniformly refine mesh
                sample_dofs = FunctionSpace(mesh2, 'DG', 0).dim()
            else:
                mesh2 = coef_mesh_cache
                sample_dofs = FunctionSpace(mesh2, 'DG', 0).dim()
        # endregion
        # region obtain cell midpoints
        with TicToc(key="  2. Get cell midpoints", active=True, do_print=True):

            mp = np.zeros((mesh2.num_cells(), 2))
            for ci, c in enumerate(cells(mesh2)):
                mpi = c.midpoint()
                mp[ci] = mpi.x(), mpi.y()
        # endregion
        n_samples = max(print_min_of_coef_samples, plot_coef_samples)
        minimal_coef_value = np.inf
        for lia in range(n_samples):
            # region Sample true and approximated coefficient field
            y = list(sample_y_list[lia])
            V1_ = af(mp, y)
            V1_ = np.exp(V1_)
            print("sample_error with ranks={}, hdegs={} on mesh with {} DG0 dofs".format(ranks_coef, hdegs_coef,
                                                                                         sample_dofs))
            # from alea.application.tt_asgfem.tensorsolver.tt_lognormal import sample_cont_coeff_2
            with TicToc(key="  3. Sample cont core", active=True, do_print=True):
                V3_ = sample_cont_coeff(cont_coeff_cores, af, mp, y, ranks_coef, hdegs_coef, bxmax, theta, rho,
                                        dg_mesh=mesh2
                                        )
            print("error simple: {}".format(np.linalg.norm(V1_ - V3_, ord=2)))
            # endregion
            # region Prepare Fenics utilities
            with TicToc(key="  4. create Function Spaces", active=True, do_print=True):
                from dolfin import TrialFunction, TestFunction, parameters, inner, dx, assemble, as_backend_type
                fs = FunctionSpace(mesh2, "DG", 0)
                fs_cg = FunctionSpace(mesh2, "CG", 1)

                _u = TrialFunction(fs_cg)
                _v = TestFunction(fs_cg)
                parameters['linear_algebra_backend'] = 'Eigen'
            with TicToc(key="  5. Assemble Mass matrix", active=True, do_print=True):
                #                                       # create bilinear form of mass matrix
                BF = inner(_u, _v) * dx(mesh2)
                M = assemble(BF)
                M = as_backend_type(M)
                M = M.array()
            with TicToc(key="  6. interpolate to CG mesh", active=True, do_print=True):
                fun_v3 = Function(fs)
                fun_v1 = Function(fs)
                fun_v3.vector()[:] = V3_
                fun_v1.vector()[:] = V1_
                fun_v3 = interpolate(fun_v3, fs_cg)
                fun_v1 = interpolate(fun_v1, fs_cg)
                plot_fun_v3 = fun_v3.compute_vertex_values(mesh2)
                plot_fun_v1 = fun_v1.compute_vertex_values(mesh2)
                plot_fun_diff = Function(fs_cg)
                plot_fun_diff.vector()[:] = np.array(fun_v3.vector()[:]) - np.array(fun_v1.vector()[:])
                plot_fun_diff = plot_fun_diff.compute_vertex_values(mesh2)
            # endregion
            # region Plot coefficient field samples to file
            if plot_coef_samples:
                import matplotlib.pyplot as plt
                from dolfin.common.plotting import mesh2triang
                ax = plt.gca(projection='3d')
                ax.set_aspect('equal')
                ax.plot_trisurf(mesh2triang(mesh2), plot_fun_v3, cmap=plt.cm.CMRmap)
                if domain == "square":
                    plt.savefig("mean{}_quantile{}_S_{}_scal{}_de{}_coef.png".format(field_mean, quant_factor,
                                                                                        lia, scale,
                                                                                        decayexp),
                                format='png')
                elif domain == "lshape":
                    plt.savefig("mean{}_qauntile{}_L_{}_scal{}_de{}_coef.png".format(field_mean, quant_factor,
                                                                                        lia, scale,
                                                                                        decayexp),
                                format='png')
                plt.clf()
                ax = plt.gca(projection='3d')
                ax.set_aspect('equal')
                ax.plot_trisurf(mesh2triang(mesh2), plot_fun_diff, cmap=plt.cm.CMRmap)
                if domain == "square":
                    plt.savefig("mean{}_quantile{}_S_{}_scal{}_de{}_coef_diff.png".format(field_mean,
                                                                                             quant_factor, lia,
                                                                                             scale, decayexp),
                                format='png')
                elif domain == "lshape":
                    plt.savefig("mean{}_quantile{}_L_{}_scal{}_de{}_coef_diff.png".format(field_mean,
                                                                                             quant_factor, lia,
                                                                                             scale, decayexp),
                                format='png')
                plt.clf()
                ax = plt.gca(projection='3d')
                ax.set_aspect('equal')
                ax.plot_trisurf(mesh2triang(mesh2), plot_fun_v1, cmap=plt.cm.CMRmap)
                if domain == "square":
                    plt.savefig("True_mean{}_quantile{}_S_{}_scal{}_de{}_coef.png".format(field_mean,
                                                                                             quant_factor,
                                                                                             lia, scale, decayexp),
                                format='png')
                elif domain == "lshape":
                    plt.savefig("True_mean{}_qauntile{}_L_{}_scal{}_de{}_coef.png".format(field_mean,
                                                                                          quant_factor,
                                                                                          lia, scale, decayexp),
                                format='png')
                plt.clf()

                # ph = PlotHelper()
                # ph["coef it {}".format(step+1)].plot(fun_v3)
                diff = np.array(fun_v3.vector()[:]) - np.array(fun_v1.vector()[:])
                errl2 = np.sqrt(np.dot(diff.T, np.dot(M, diff)))
                normalization = np.sqrt(np.dot(fun_v1.vector()[:], np.dot(M, fun_v1.vector()[:])))
                print("semi-discrete error: {}".format(errl2))
                print("semi-discrete relative error: {}".format(errl2 * normalization**(-1)))
            # endregion
            # region Print minimal value of coefficient field to console
            if print_min_of_coef_samples:
                curr_min_value = np.min(plot_fun_v3)
                print("minimal value of coefficient sample {}: {}".format(lia, curr_min_value))
                if curr_min_value < minimal_coef_value:
                    minimal_coef_value = curr_min_value
                    # endregion
        # region early termination switches
        if plot_coef_terminate is True:
            exit()
        if print_min_of_coef_samples_terminate is True:
            exit()
        # endregion
    # endregion

    # region Get cell midpoints to discretize semi-discrete coef tensor
    with TicToc(sec_key="ASGFEM-solve", key="1. Get cell midpoints and dofs", active=True, do_print=False):

        cg_space = FunctionSpace(mesh, 'CG', femdegree)
        # dg_space = FunctionSpace(mesh, 'DG', femdegree-1)

        # region obtain dofs of dg space
        mp = np.array(cg_space.tabulate_dof_coordinates())  # obtain all coordinates of degrees of freedom
        mp = mp.reshape((-1, mesh.geometry().dim()))
        mesh_midpoint = mp
        # endregion

    # endregion

    # region Create fully discrete coefficient tensor

    with TicToc(sec_key="ASGFEM-solve", key="2. create fully discrete first core", active=True, do_print=False):
        core0 = fully_disc_first_core(cont_coeff_cores, af, mesh_midpoint, ranks_coef, hdegs_coef, bxmax,
                                      theta=theta, rho=rho, cg_mesh=mesh, femdegree=femdegree)
        coeff_cores = deepcopy(cont_coeff_cores)
        coeff_cores.insert(0, core0)
        first_comp = np.reshape(core0, [len(mesh_midpoint), ranks_coef[1]])
        # print("coeficient tensor: {}".format(tt.vector.from_list(coeff_cores)))
    # endregion
    # region Create discrete operators

    with TicToc(sec_key="ASGFEM-solve", key="3. Create operators", active=True, do_print=False):
        #                                           # compute FE matrices for piecewise constant coefficients
        with TicToc(sec_key="ASGFEM-solve", key="  3.1. compute FE matrices", active=True, do_print=False):
            A0, bc_dofs, BB, CG = compute_FE_matrices(first_comp, mesh, degree=femdegree, n_cpu=40)
        # print len(A0), A0[0].shape
        n_coeff = len(hdegs)
        if n_coeff > n_coef_coef:
            print("Dimension of solution exhibits dimension of coefficient -> exit()")
            return sol_list

        with TicToc(sec_key="ASGFEM-solve", key="  3.2 generate TT operator", active=True, do_print=False):
            opcores = generate_operator_tt(cont_coeff_cores, ranks_coef, hdegs, len(hdegs))
        D = A0[0].shape[0]                          # get size of first matrix
        opcores[0] = []                             # re_init first operator core (it was None anyway)
        for r0, A in enumerate(A0):
            opcores[0].append(A)
            # print "A0", A.toarray()
            # print "midpoints", first_comp[:,r0]
            # print "eigsA0", np.linalg.eig(A.toarray())

        # generate lognormal operator tensor

        with TicToc(sec_key="ASGFEM-solve", key="  3.3. Create sparse TT operator", active=True, do_print=False):
            A = ttsmat.smatrix(opcores)

            bcopcores = (len(hdegs)+1)*[[]]
            # bcdense = np.zeros([D, D])
            # bcdense[bc_dofs, bc_dofs] = 1
            bcsparse_direct = sps.csr_matrix((D, D))
            bcsparse_direct[bc_dofs, bc_dofs] = 1
            bcopcores[0] = []
            bcopcores[0].append(bcsparse_direct)
            for i in range(n_coeff):
                bcopcores[i+1] = reshape(np.eye(opcores[i+1].shape[1], opcores[i+1].shape[2]),
                                         [1, opcores[i+1].shape[1], opcores[i+1].shape[2], 1])
            BCOP = ttsmat.smatrix(bcopcores)

            # Aop = A.full()
            # Aop = reshape(Aop,[A.n[0],(gpcdegree+1),(gpcdegree+1),A.n[0],(gpcdegree+1),(gpcdegree+1)])
            A += BCOP
            A = A.round(1e-16)                  # ! important: do not cut ranks here. Just orthogonalize cores[1:]
        # print "A", A
        # pass those to robert !!!!!!!!!!!!!!! A[0][:, :, k] for k in range(A[0].shape[2]
    # endregion
    print(A)
    # region Create rhs
    BB = reshape(BB, [1, A.n[0], 1])
    F = [BB] + len(hdegs) * [[]]
    for i in range(len(hdegs)):
        F[i+1] = reshape(np.eye(A.n[i+1], 1), [1, A.n[i+1], 1])
    F = tt.vector.from_list(F)
    # print "F", F
    # endregion

    # region Create preconditioner for solver
    # P = compute_FE_matrix_coeff(B,mesh,exp_a=True)
    with TicToc(sec_key="ASGFEM-solve", key="4. create preconditioner", active=True, do_print=False):
        # core0 = fully_disc_first_core(cont_coeff_cores, af, fem_dofs, ranks_coef, hdegs_coef, bxmax, theta=theta,
        #                               rho=rho)

        # precond_coeff_cores = deepcopy(cont_coeff_cores)
        # precond_coeff_cores.insert(0, core0)
        Pvec = extract_mean(tt.vector.from_list(coeff_cores))
        P = compute_FE_matrix_coeff(Pvec, mesh, exp_a=True, degree=femdegree)

    # endregion
    # region Create starting vector for solver
    # -Start-Vector------------------------------------------
    while True:
        if rank == 1:
            W = ttRandomDeterministicRankOne(A.n)     # create random deterministic rank one tensor
        else:
            random_ten = ttRandomDeterministicRankOne(A.n)    # create tensor with given rank and size
            # W = tt.rand(A.n, A.d, start_rank)  # create tensor with given rank and size
            W = ttNormalize(W + random_ten, approxtol=1e-18)

            print("rank of starting tensor : {}".format(max(W.r)))
        # with TicToc(key="**** ttRightOrthogonalize ****", active=True, do_print=True):
        #     W = ttRightOrthogonalize(W)             # orthogonalize the solution Tensor from the right
        # endregion

        print("solve reference with solution tensor spec: {}".format(W))
        converged = [False]
        with TicToc(sec_key="ALS-solver", key="**** ALS reference", active=True, do_print=False):
            ref_retval, _ = ttALS(A, F, W, conv=convergence, maxit=als_iterations, P=P, converged_by_ref=converged)
        rank = max(W.r) + 1
        if max(W.r) >= 5:
            break
        else:
            W = ref_retval
        TicToc.sortedTimes(sec_sorted=True)
    print("="*10)

    # region Solver (ALS)

    if isinstance(W, list):
        W = tt.vector.from_list(W)
    with TicToc(sec_key="ALS-solver", key="**** precond ALS conv:{}, iterations:{}, "
                                          "init rank:{}****".format(convergence, als_iterations, max(W.r)),
                active=True, do_print=True):
        retval, error_list_precond, first_comp_error_list_precond = ttALS(A, F, W, conv=convergence, maxit=100, P=P,
                                                                          converged_by_ref=converged,
                                                                          return_local_error=True,
                                                                          reference_solution=ref_retval)
    print retval
    with TicToc(sec_key="ALS-solver", key="**** ALS w/o precond conv:{}, iterations:{}, "
                                          "init rank:{}****".format(convergence, als_iterations, max(W.r)),
                active=True, do_print=True):
        retval, error_list, first_comp_error_list = ttALS(A, F, W, conv=convergence, maxit=100, P=None,
                                                          converged_by_ref=converged, return_local_error=True,
                                                          reference_solution=ref_retval)
    print retval
    print("error: {}".format(error_list))
    print("error precond: {}".format(error_list_precond))
    print("first_comp: {}".format(first_comp_error_list))
    print("first_comp precond: {}".format(first_comp_error_list_precond))
    sol_list = {'error': error_list, 'error precond': error_list_precond, 'ref_norm': tt.vector.norm(ref_retval),
                'first_comp_error_list': first_comp_error_list,
                'first_comp_error_list_precond': first_comp_error_list_precond,
                'solution_tensor': retval}

    return sol_list
# endregion

SOL = log_normal_sgfem_als()
# print SOL
with open('error_list_{}'.format(get_current_time_string()), mode='w') as f:
    for lia in range(len(SOL['error'])):
        line = "{}, {}\n".format(lia+1, SOL['error'][lia])
        f.write(line)
with open('error_list_precond_{}'.format(get_current_time_string()), mode='w') as f:
    for lia in range(len(SOL['error precond'])):
        line = "{}, {}\n".format(lia+1, SOL['error precond'][lia])
        f.write(line)
runs = len(SOL['first_comp_error_list'])
for run in range(runs):
    with open('error_list_first_comp_run{}_{}'.format(run+1, get_current_time_string()), mode='w') as f:
        for lia in range(len(SOL['first_comp_error_list'][run])):
            line = "{}, {}\n".format(lia+1, SOL['first_comp_error_list'][run][lia])
            f.write(line)
runs = len(SOL['first_comp_error_list_precond'])
for run in range(runs):
    with open('error_list_precond_first_comp_run{}_{}'.format(run+1, get_current_time_string()), mode='w') as f:
        for lia in range(len(SOL['first_comp_error_list_precond'][run])):
            line = "{}, {}\n".format(lia+1, SOL['first_comp_error_list_precond'][run][lia])
            f.write(line)
with open('error_stats_{}'.format(get_current_time_string()), mode='w') as f:
    f.write("{}\n \n".format(SOL['solution_tensor']))
    f.write("maxrank=5, n_coeff=15, femdegree=2, gpcdegree=3, decayexp=2, gamma=0.9, amptype=decay-inf " +
            "domain=square, mesh_refine=1, mesh_dofs=500, sol_rank=5, freq_skip=0," +
            " freq_scale=1.0, scale=1.0, theta=0.01, rho=1.0, hermite_degree=5, als_iterations=1000," +
            " convergence=1e-12, coeffield_type=EF-square-cos-algebraic, _print=False, field_mean=0.0, " +
            " coef_acc=1e-10, iterations=10, theta_x=0.5, theta_y=0.0, start_rank=1, n_coef_coef=20, " +
            " max_hdegs_coef=20, eta_zeta_weight=0.1, resnorm_zeta_weight=1, coef_mesh_dofs=3000, coef_quad_degree=7\n")
