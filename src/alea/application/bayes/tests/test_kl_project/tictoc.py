import time
from collections import defaultdict

_TICTOCDICT = defaultdict(float)

class TicToc(object):
    """
    A simple code timer.
 
    Example
    -------
    >>> with TicToc():
    ...     time.sleep(2)
    Elapsed time is 2.000073 seconds.
    """
    def __init__(self, active=True, do_print = True, key ='', accumulate = True, clear = False):
        if clear:
            self.clear(key)
        self.active = active
        self.do_print = do_print
        self.key = key
        self.accumulate = accumulate

    def __enter__(self):
        if self.active:
            self.start_time = time.time()
        return self

    def __exit__(self, type, value, traceback):
        if self.active:
            dt = time.time() - self.start_time
            if self.accumulate:
                _TICTOCDICT[self.key] += dt
            else:
                _TICTOCDICT[self.key] = dt
            if self.do_print:
                overall = " (overall %f)" % _TICTOCDICT[self.key] if self.accumulate else ""
                print self.key + " Elapsed time is %f seconds%s." % (dt, overall)

    @staticmethod
    def get(key):
        return _TICTOCDICT[key]

    @staticmethod
    def keys():
        return _TICTOCDICT.keys()

    @staticmethod
    def items():
        return _TICTOCDICT.items()

    @staticmethod
    def clear(key=None):
        if key is None:
            _TICTOCDICT.clear()
        else:
            _TICTOCDICT.pop(key)
