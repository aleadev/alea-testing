from __future__ import division
import numpy as np
from functools import partial
from plothelper import PlotHelper
from testfield import TestField
from fem_utils import setup_FEM, evaluate_u
from dolfin import set_log_level, WARNING, Function

set_log_level(WARNING)

mean = 1                                            # mean value of the affine field
expfield = False                                    # switch to go to the log-normal field
field = TestField(mean=mean, expfield=expfield)     # create field object
N = 10                                             # define mesh resolution
FEM = setup_FEM(N)                                  # setup variational formulation and boundary conditions
eval_u = partial(evaluate_u, FEM=FEM, field=field)  # create callable function object from FEM solution

#                                                   # evaluate sample solution
K, M = 1, 10  	                                    # number samples, KL modes

ph = PlotHelper()                                   # create plothelper object for simple fenics function plots
cords = [tuple((np.random.rand(2))) for lia in range(N)]
cords = FEM["mesh"].coordinates()
# for cord in cords:
#     print cord
for i in range(K):                                  # loop through number of samples
    y = 2 * np.random.rand(M) - 1                   # uniform realization of y in [-1,1]
    y = [0.5, 0.1, 0.2, -0.4, -0.2, 0.1, 0.0, -0.9, 0.8, -0.2]
    uy, ay  = eval_u([y])                           # evaluate solution at y
    #                                               # create Fenics Function on the given FunctionSpace
    Uy = Function(FEM["V"])
    Ay = Function(FEM["V"])                         # create Fenics Function for field realisation
    Uy.vector()[:] = uy[0]                          # Store solution in Function
    Ay.vector()[:] = ay[0]                          # Store current field realisation in Function
    def base(i, x_i):
        # print("floor({}+2) = {}".format(i, np.floor((i+2)/2)))
        # print("ceil({}+2) = {}".format(i, np.ceil((i+2)/2)))
        return 0.1* (np.sin(1*np.pi*np.floor((i+2)/2)*x_i[0])*np.sin(1*np.pi*np.ceil((i+2)/2)*x_i[1]) +  np.sin(1*np.pi*(np.floor((i+2)/2)+1)*x_i[0])*np.sin(1*np.pi*(np.ceil((i+2)/2)+1)*x_i[1]))
    A = np.array([[base(lia, cord) for lia in range(0, M)] for cord in cords])
    b = np.array([Ay(cord) for cord in cords]) - mean
     
    # print("shape A={}".format(A.shape))
    # print("shape b={}".format(b.shape))
    y_approx, res, rank, s = np.linalg.lstsq(A, b)
    print res
    print rank
    print("y={}".format(y))
    print("y_approx={}".format(y_approx))
    print("err={}".format(np.linalg.norm(y-y_approx)))
