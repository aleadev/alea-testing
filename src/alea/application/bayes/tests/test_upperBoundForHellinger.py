from __future__ import division
import numpy as np

a = np.arange(0, 1, 0.01)
b = np.arange(0, 1, 0.01)
c = np.arange(1.1, 10.1, 0.1)
d = np.arange(1.1, 10.1, 0.1)

for lia in range(len(a)):
    for lib in range(len(b)):
        for lic in range(len(c)):
            for lid in range(len(d)):
                buff1 = (np.sqrt(c[lic]*a[lia]) - np.sqrt(d[lid]*b[lib]))**2
                buff2 = 2*c[lic]*(a[lia]-b[lib])**2 + 2*(d[lid]**(-1))*(np.sqrt(c[lic])-np.sqrt(d[lid]))**2
                if buff1 > buff2:
                    print "Error {}>{} , a[{}]={}, b[{}]={}, c[{}]={}, d[{}]={}".format(buff1, buff2, lia, a[lia],
                                                                                        lib, b[lib], lic, c[lic],
                                                                                        lid, d[lid])
