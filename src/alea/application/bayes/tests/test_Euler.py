import numpy as np
import tt
import math
import matplotlib.pyplot as plt


class MyIterator:
    nodes = []
    CurrentIt = []
    def __init__(self, n):
        self.nodes = n
        self.CurrentIt = [0 for lia in range(len(n))]

    def Increment(self):
        if (len(self.nodes) == 0):
            return
        for lia in range(len(self.nodes)-1,-1,-1):
            if (self.CurrentIt[lia] < self.nodes[lia]-1):
              self.CurrentIt[lia] = self.CurrentIt[lia] +1
              break
            else:
              self.CurrentIt[lia] = 0

    def Reset(self):
        self.CurrentIt = [0 for lia in range(len(self.nodes))]

def func(x):
    sum = 0
    sum = x[0]
    for lia in range(1,len(x)):
        sum = sum + 1/(lia*lia)*x[lia]
    return sum*sum


def ExpEuler(tensor, steps, precision, maxRank):
                                                    # create random rank 1 tt
  EulerTestBuffer = tt.rand(tensor.n, len(tensor.n), [1 for lia in range(len(tensor.n)+1)])
                                                    # store tensor as list of 3d array
  EulerTestBuffer = EulerTestBuffer.to_list(EulerTestBuffer)
  EulerTestBuffer[0][0,:,0] = np.ones(tensor.n[0])  # store true values in first comp
  for lia in range(1,len(tensor.n)):	            # replace other comp with first unitvector
    #EulerTestBuffer[lia][0,:,0] = [1] + [0 for lic in range(tensor.n[lia]-1)]
    EulerTestBuffer[lia][0, :, 0] = np.ones(tensor.n[lia])
  print("Start Tensor has rank {0}").format(EulerTest.r)
  EulerTestBuffer = tt.tensor.from_list(EulerTestBuffer)


  iter = MyIterator(tensor.n)
  for lia in range(100):
        if EulerTestBuffer[iter.CurrentIt] != 1:
            print "Start Tensor is not 1 at {0} but {1}".format(iter.CurrentIt, EulerTestBuffer[iter.CurrentIt])
        iter.Increment()


  print("Start Euler with precision {0}").format(precision)
  for lia in range(1,steps+1):
    print("Step {0} with ranks {1}").format(lia,EulerTestBuffer.r)
    if (max(EulerTestBuffer.r)>maxRank):
      print("  Round")
    EulerTestBuffer = tt.tensor.round(EulerTestBuffer,precision,maxRank)
    EulerTestBuffer = EulerTestBuffer + 1./eulerSteps*EulerTest*EulerTestBuffer

    for lic in range(0,ValueCount):
      Values[lia][lic] = EulerTestBuffer[itter.CurrentIt]
      itter.Increment()
    itter.Reset()
  return EulerTestBuffer, Values

dimensions = [6,7,8,8,8,8,3]
maxInitRang = 5
ValueCount = 20
eulerSteps = 10
precision = 1./100000
maxRank = 20

EulerTest = tt.rand(dimensions, len(dimensions), maxInitRang)
EulerTest = -1./100000*EulerTest*EulerTest
itter = MyIterator(dimensions)
Values = [[0 for lic in range(ValueCount)] for lia in range(eulerSteps+1)]
ExactValues = [0 for lia in range(ValueCount)]

for lia in range(0,ValueCount):
    Values[0][lia] = EulerTest[itter.CurrentIt]
    print(EulerTest[itter.CurrentIt])
    ExactValues[lia] = math.exp(EulerTest[itter.CurrentIt])
    itter.Increment()
itter.Reset()

EulerTestBuffer, Values = ExpEuler(EulerTest,eulerSteps,precision,maxRank)

plt.figure(1)
plt.plot(Values)
plt.show()
print(Values[0])
print(Values[eulerSteps-1])
print(np.array(Values[-1])-np.array(ExactValues))