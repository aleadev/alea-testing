import numpy as np
import tt

n = [2,4,9,3]
r_u = [1] + [5,4,6] + [1]
r_v = [1] + [8,8,9] + [1]
U = tt.rand(n, d=len(n), r=r_u)
V = tt.rand(n, d=len(n), r=r_v)

tt_sum = U + V

def own_tt_sum(ten1, ten2):
    retval = []
    assert(len(ten1) == len(ten2))
    for lia in range(len(ten1)):
        if lia == 0:
            core = np.zeros((1, ten1[lia].shape[1], ten1[lia].shape[2] + ten2[lia].shape[2]))
            core[:, :, :ten1[lia].shape[2]] = ten1[lia]
            core[:, :, ten1[lia].shape[2]:] = ten2[lia]
            retval.append(core)
            continue
        if lia == len(ten1)-1:
            core = np.zeros((ten1[lia].shape[0] + ten2[lia].shape[0], ten1[lia].shape[1], 1))
            core[:ten1[lia].shape[0], :, :] = ten1[lia]
            core[ten1[lia].shape[0]:, :, :] = ten2[lia]
            retval.append(core)
            continue
        core = np.zeros((ten1[lia].shape[0] + ten2[lia].shape[0], ten1[lia].shape[1],
                         ten1[lia].shape[2] + ten2[lia].shape[2]))
        core[:ten1[lia].shape[0], :, :ten1[lia].shape[2]] = ten1[lia]
        core[ten1[lia].shape[0]:, :, ten1[lia].shape[2]:] = ten2[lia]
        retval.append(core)
    return retval

own_sum = tt.vector.from_list(own_tt_sum(tt.vector.to_list(U), tt.vector.to_list(V)))

assert(tt.vector.norm(own_sum - tt_sum) < 1e-10)
