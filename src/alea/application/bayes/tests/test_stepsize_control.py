from __future__ import division
import numpy as np
import tt
import tt.amen
# import matplotlib.pyplot as plt

dimensions = [1, 3, 5, 2, 7]                        # dimensions of the random tensor
ranks = [1, 8, 6, 7, 5, 1]                          # ranks of the random tensor

euler_global_mc_samples = 100
euler_repetitions = 0
euler_local_mc_samples = 100

exp_euler_steps = 10                                # number of steps to do in euler method
exp_euler_round_rank = 50                           # maximal rank to start rounding at
exp_euler_precision = 1e-5                          # precision to round tensor to

#                                                   # number of sample points for the monte carlos sampling
MC_steps = int(np.prod(dimensions)/4)

y = [[np.random.random_integers(0, high=dimensions[lic]-1) for lic in range(len(dimensions))] for lia in range(MC_steps)]


init_stepsize = 0.1
acc = 1e-6
max_steps = 100
max_stepsize_change = 0.1
max_stepsize = 0.5
min_stepsize= 1e-14
beta = 0.9

method = "fehlberg"
if method == "1(2)":                                # heun / euler
    mid = [
           [0, 0],
           [1, 0]
          ]
    b_1 = [1, 0]
    b_2 = [0.5, 0.5]
elif method == "2(3)":                              # Bogacki / Shampine
    mid = [
           [0  , 0  , 0,   0],
           [1/2, 0  , 0,   0],
           [0  , 3/4, 0,   0],
           [2/3, 1/3, 4/9, 0]
          ]
    b_1 = [2/9 , 1/3, 4/9,  0 ]
    b_2 = [7/24, 1/4, 1/3, 1/8]
elif method == "4(5)":                              # Runge-Kutta / Fehlberg
    # c = [0, 1/2, 1]
    mid = [
           [0        , 0         , 0         , 0        , 0     , 0],
           [1/4      , 0         , 0         , 0        , 0     , 0],
           [3/32     , 9/32      , 0         , 0        , 0     , 0],
           [1932/2197, -7200/2197, 7296/2197 , 0        , 0     , 0],
           [439/216  , -8        , 3680/513  , -845/4104, 0     , 0],
           [-8/27    , 2         , -3544/2565, 1859/4104, -11/40, 0]
          ]
    b_1 = [25/216, 0, 1408/2565 ,  2197/4104 , -1/5 , 0 ]
    b_2 = [16/135, 0, 6656/12825, 28561/56430, -9/50, 2/55]
A = tt.rand(dimensions, len(dimensions), ranks)
A = -0.005*A*A
def f(x):
    return A*x
#print "A={0}".format(A)
diff_rk = []
step_size = []
t1 = 0

#                                           # create random rank 1 tt
eulertestbuffer = tt.rand(A.n, len(A.n), [1 + 0*_lia for _lia in range(len(A.n)+1)])
#                                           # store tensor as list of 3d array
eulertestbuffer = tt.vector.to_list(eulertestbuffer)
#                                           # store true values in first comp
eulertestbuffer[0][0, :, 0] = np.ones(A.n[0])
for _lia in range(1, len(A.n)):	    # replace other comp with vector of ones
    eulertestbuffer[_lia][0, :, 0] = np.ones(A.n[_lia])
    # log.info("start tensor has rank {0}".format(tensor.r))
eulertestbuffer = tt.vector.from_list(eulertestbuffer)
exp_A_rk_1 = tt.vector.copy(eulertestbuffer)
exp_A_rk_2 = tt.vector.copy(eulertestbuffer)
h = []
h.append(init_stepsize)
while True:
    if np.sum(np.array(h)) == 1:
        result = eulertestbuffer
        break
    if len(h) > max_steps :
        raise AssertionError("no convergence after {} steps".format(max_steps))
        
    summ = 0
    k = []
    for j in range(0, len(b_1)):
        for i in range(0, j):
            if summ == 0:
                summ = mid[j][i] * k[i]
            elif isinstance(summ, tt.vector):
                summ += mid[j][i] * k[i] 
        #k[j,lia,lic] = h*(t1+h*c[j])*(A[lia,lic]*(exp_A_rk[lia,lic])+summ)
        if summ == 0:
            k.append(h[-1]*(f(exp_A_rk_2)))
        elif isinstance(summ, tt.vector):
            if max(summ.r) > exp_euler_round_rank:
                summ = tt.vector.round(summ, exp_euler_precision, exp_euler_round_rank)
            k.append(h[-1]*(f(exp_A_rk_2+summ)))
        else: 
            raise TypeError
        summ = 0
    summ1=0
    summ2=0
    for j in range(0, len(b_1)):
        if summ1 == 0:
            summ1 = b_1[j]*k[j]
            summ2 = b_2[j]*k[j]
        elif isinstance(summ1, tt.vector):
            if max(summ1.r) > exp_euler_round_rank:
                summ1 = tt.vector.round(summ1, exp_euler_precision, exp_euler_round_rank)
            summ1 += b_1[j]*k[j]
            if max(summ2.r) > exp_euler_round_rank:
                summ2 = tt.vector.round(summ2, exp_euler_precision, exp_euler_round_rank)
            summ2 += b_2[j]*k[j]
        else: 
            raise TypeError

    exp_A_rk_2 = exp_A_rk_1 + summ2
    exp_A_rk_1 += summ1
    
    if max(exp_A_rk_1.r) > exp_euler_round_rank:
        exp_A_rk_1 = tt.vector.round(exp_A_rk_1, exp_euler_precision, exp_euler_round_rank)
    if max(exp_A_rk_2.r) > exp_euler_round_rank:
        exp_A_rk_2 = tt.vector.round(exp_A_rk_2, exp_euler_precision, exp_euler_round_rank)
        
    eps = tt.vector.norm(exp_A_rk_1 - exp_A_rk_2)
    if eps >= acc:
        h.append(beta*h[-1]*((acc/eps)**0.2))
    else:
        h.append(beta*h[-1]*((acc/eps)**0.25))
    if h[-1] / h[-2] > max_stepsize_change:
        h[-1] *= max_stepsize_change
    elif h[-1]/h[-2] < 1./max_stepsize_change:
        h[-1] /= max_stepsize_change
    
    if np.sum(np.array(h)) > 1:
        h[-1] = 1 - (np.sum(np.array(h)) - h[-1])
    if h[-1] < min_stepsize:
        raise AssertionError("stepsize to small")
    if h[-1] > max_stepsize:
        raise AssertionError("stepsize to big")
        
    print "new stepsize = {}. remaining: {}".format(h[-1], 1-np.sum(np.array(h)))
    
exp_A_rk = result
MC_mean_exp = 0
for lia, y_k in enumerate(y):
    #print "A(y_k)={2}, exp(A(y_k))={0}, e^A(y_k)_T={1}".format(np.exp(A[y_k]),exp_A[y_k],A[y_k])
    MC_mean_exp += np.abs(exp_A_rk[y_k] - np.exp(A[y_k]))
MC_mean_exp /= MC_steps
diff_rk.append(MC_mean_exp)

print "error: {}".format(diff_rk)
print "stepsizes: {}".format(h)
#print diff_imp
#print diff_exp
#plt.figure()
#plt.plot(np.array(h))
#plt.figure()
#plt.loglog(diff_rk, label="runge kutta")
#plt.legend(ncol =2, loc=0)