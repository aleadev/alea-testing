# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  test script for the bayesian procedure using direct sampling of the Karhunen Loeve expansion
"""
# region Imports
from __future__ import division

import logging.config

import matplotlib.pyplot as plt
import numpy as np
from Bayes_util.lib.bayes_lib import create_parameter_samples
from Bayes_util.exponential.implicit_euler import ImplicitEuler
from scipy.integrate import quad
from scipy.interpolate import interp1d

from Bayes_util.direct_bayes import DirectBayes
from Bayes_util.exponential.explicit_euler import ExplicitEuler
from Bayes_util.lib.bayes_lib_simple import create_unique_dofs
from Bayes_util.lib.interpolation_util import get_cheb_points
from tt_als.alea.application.egsz.coefficient_field import ParametricCoefficientField
from tt_als.alea.stochastics.random_variable import UniformRV

# endregion

# region Setup parameters

# region Solution parameter
KL_type = 0                                         # type of KL to choose
n_coefficients = 3                                  # number of coefficients
n_dofs = 5                                         # number of dofs
n_dofs_list = [2**lia + 2 for lia in range(8)]          # list of increasing dofs
n_dofs_list = [n_dofs]
alpha = 2                                           # scaling parameter from Schwab
# endregion

# region Observation parameter
obs_precision = 1e-10
obs_rank = 50
# endregion

# region Inner Product parameter
inp_precision = 1e-10
inp_rank = 50
inp_gamma = 0.1
# endregion

# region Euler Parameter
euler_precision = 1e-10                             # precision to use in euler rounding
euler_rank = 50                                     # rank to round down in euler method
euler_local_mc_samples = 1000                       # samples in every euler step
euler_global_mc_samples = 1000                      # samples for the final result
euler_repetitions = 5                               # number of repetitions with half step size
euler_steps = 3                                     # start euler steps
euler_global_precision = 1e-5                       # global precision to reach after the euler scheme
euler_use_implicit = False
# endregion

# region Density parameters
n_eval_grid = 50
# endregion

# region setup discretisation
Nx, Ny = 10000, 10
dx, dy = 1/Nx, 1/Ny
physical_grid = np.linspace(0, 1, Nx)
eval_grid_points = np.linspace(-1, 1, n_eval_grid)
stochastic_grid = np.array(get_cheb_points([Ny] * n_coefficients)[0])
# endregion

color_list = ['olive', 'gold', 'aqua', 'black', 'blue', 'brown', 'green', 'red', 'orange']

logging.config.fileConfig('logging.conf')           # load logger configuration
log = logging.getLogger('test_bayes')               # get new logger object

# endregion
# region setup KL coefficients


def a1(i_1):
    """
    spectral approach with increasing frequencies
    """
    return lambda x: np.sin(2 * np.pi * (i_1 + 1) * x)


def a2(i_1):
    """
    linear approach from Schwab
    """
    return lambda x: 0.95*x*(-alpha*i_1)

a = [a1, a2][KL_type]
# endregion

# region  setup truth, noise and prior
log.info("Create %i true parameters", n_coefficients)
true_values = create_parameter_samples(n_coefficients)
# endregion

# region Setup Gaussian Prior
Gauss_prior = False
if Gauss_prior:
    raise NotImplemented
    # pi_y =
    # [lambda x: 1/(np.sqrt(2*np.pi)*sigma_i)*np.exp(-0.5*((x-mu_i)/sigma_i)**2) for mu_i, sigma_i in zip(mu, sigma)]

else:
    prior_densities = [lambda x: 0.5*np.ones_like(x)]*n_coefficients
# endregion

log.info("Create coefficient field")
coeff_field = ParametricCoefficientField(0, a, lambda i_1: UniformRV(a=-1, b=1))

log.info("Create exponential method")
if euler_use_implicit:
    euler_method = ImplicitEuler(euler_steps, euler_precision, euler_rank, euler_global_mc_samples, euler_repetitions,
                                 euler_local_mc_samples)
else:
    euler_method = ExplicitEuler(euler_steps, euler_precision, euler_rank, euler_global_mc_samples, euler_repetitions,
                                 euler_local_mc_samples)

file_path = 'results/simple/'

log.info('Create %i unique random dofs', n_dofs)

max_dofs = np.array(create_unique_dofs(n_dofs_list[-1], Nx))


bayes_obj_list = []
for step, n_dofs in enumerate(n_dofs_list):
    # region setup measure points
    # endregion
    dofs = max_dofs[:n_dofs]
    log.info("Create measurements")
    measurements = [DirectBayes.kl(coeff_field, physical_grid[dofs[lia]], true_values) + np.random.randn() * inp_gamma
                    for lia in range(n_dofs)]

    log.info("Create Bayes Object")
    bayes_obj = DirectBayes.init_solver(dofs, measurements, true_values, coeff_field, obs_precision,
                                        obs_rank, inp_precision, inp_rank, inp_gamma, euler_method,
                                        euler_global_precision, eval_grid_points, stochastic_grid, physical_grid,
                                        prior_densities)

    # region Create Bayes solution either from pickling or calculation
    fig_set = 0
    fig = plt.figure()
    if bayes_obj.import_bayes(file_path):
        log.info('open Bayes file %s', file_path + bayes_obj.file_name + '.dat')
        bayes_obj_list.append(bayes_obj)
        # region results presentation area
        plt.clf()
        if fig_set != 0:
            fig.clf()
        try:
            # region Plot euler results

            fig, arr = plt.subplots(2, 2)
            fig_set = 1
            for lia in range(len(bayes_obj.exp_method.local_convergence)):
                arr[0, 0].plot(np.array([lic for lic in range(1, len(bayes_obj.exp_method.local_convergence[lia])+1)]),
                               bayes_obj.exp_method.local_convergence[lia])
                arr[0, 0].set_title('error of euler method with different step sizes')
                # arr[0, 0].xlabel('# of steps')
                # arr[0, 0].ylabel('MC error')
                arr[1, 0].loglog(np.array([lic for lic in
                                           range(1, len(bayes_obj.exp_method.local_convergence[lia])+1)]),
                                 bayes_obj.exp_method.local_convergence[lia])
                arr[1, 0].set_title('logarithmic error development')
                # arr[0, 0].xlabel('# of steps')
                # arr[0, 0].ylabel('log MC error')
                arr[0, 1].plot(np.array([lic for lic in
                                         range(1, len(bayes_obj.exp_method.local_convergence[-1][100:])+1)]),
                               bayes_obj.exp_method.local_convergence[-1][100:])
                arr[0, 1].set_title('last steps of error size with smallest step size')
                # arr[0, 0].xlabel('# of steps')
                # arr[0, 0].ylabel('MC error')
            arr[1, 1].plot(bayes_obj.exp_method.step_size_list)
            arr[1, 1].set_title('step sizes used in Euler method')
            # arr[0, 0].ylabel('step size')
            plt.draw()
            plt.savefig(file_path + bayes_obj.file_name + "_euler.pdf", dpi=None, facecolor='w', edgecolor='w',
                        orientation='portrait', papertype=None, format='pdf',
                        transparent=False, bbox_inches='tight', pad_inches=0.1,
                        frameon=None)
            # plt.show()
            # endregion

            # region Plot densities to file
            plt.clf()
            poly_mean = []
            poly_var = []
            poly_max_arg = []
            for i, yi in enumerate(bayes_obj.eval_densities):
                plt.plot(bayes_obj.eval_grid_points, yi/bayes_obj.mean/2,
                         label="y_{0} should {1}".format(i, bayes_obj.true_values[i]),
                         color=color_list[i % len(color_list)])
                plt.xlim([-1, 1])
                plt.vlines(bayes_obj.true_values[i], 0, max(yi/bayes_obj.mean/2), color=color_list[i % len(color_list)])
                I = interp1d(bayes_obj.eval_grid_points, yi, kind='cubic')
                poly_mean.append(quad(lambda _x: _x*I(_x)/bayes_obj.mean*0.5, -1, bayes_obj.eval_grid_points[-1])[0])
                poly_var.append(quad(lambda _x2: (_x2 - poly_mean[-1])**2 * I(_x2) / bayes_obj.mean * 0.5, -1,
                                     bayes_obj.eval_grid_points[-1])[0])

                poly_max_arg.append(bayes_obj.eval_grid_points[np.argmax(yi)])
            plt.legend(ncol=len(bayes_obj.eval_densities), loc=3)
            plt.draw()
            plt.savefig(file_path + bayes_obj.file_name + "_dens.pdf", dpi=None, facecolor='w', edgecolor='w',
                        orientation='portrait', papertype=None, format='pdf',
                        transparent=False, bbox_inches='tight', pad_inches=0.1,
                        frameon=None)
            # plt.show()
            # endregion

            # region Plot density means
            plt.clf()
            x_dim = np.linspace(1, len(poly_mean), num=len(poly_mean), endpoint=True)
            plt.semilogy(x_dim, np.abs(np.array(poly_mean) - np.array(bayes_obj.true_values)), 'or', label='mean')
            plt.semilogy(x_dim, np.abs(np.array(poly_max_arg) - np.array(bayes_obj.true_values)), 'ob', label='argmax')
            plt.semilogy(x_dim, [np.linalg.norm(np.array(poly_mean) - np.array(bayes_obj.true_values))]*len(x_dim),
                         label='l2 norm mean')
            plt.semilogy(x_dim, [np.linalg.norm(np.array(poly_max_arg) - np.array(bayes_obj.true_values))]*len(x_dim),
                         label='l2 norm argmax')
            plt.legend(ncol=2, loc=1)
            plt.xlim([0.9, x_dim[-1]+0.1])
            plt.title('error of estimated parameters to correct values')
            plt.xlabel('Parameter')
            plt.ylabel('Error')
            plt.draw()
            plt.savefig(file_path + bayes_obj.file_name + "_error.pdf", dpi=None, facecolor='w', edgecolor='w',
                        orientation='portrait', papertype=None, format='pdf',
                        transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None)
            # plt.show()

            # endregion
            # region Plot density variances
            plt.clf()
            x_dim = np.linspace(1, len(poly_var), num=len(poly_var), endpoint=True)
            plt.plot(x_dim, poly_var, 'or')
            # plt.legend(ncol=1, loc=1)
            plt.xlim([0.9, x_dim[-1] + 0.1])
            plt.title('variance of estimations')
            plt.xlabel('Parameter')
            plt.ylabel('Variance')
            plt.draw()
            plt.savefig(file_path + bayes_obj.file_name + "_var.pdf", dpi=None, facecolor='w', edgecolor='w',
                        orientation='portrait', papertype=None, format='pdf',
                        transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None)
            # plt.show()

            # endregion

        except Exception as ex:
            print "no convergence " + ex.message
        # endregion

    else:
        log.info('start Bayesian procedure')

        bayes_obj.calculate_densities()

        log.info('end Bayesian procedure')

        log.info('export Bayes file %s', file_path + bayes_obj.file_name + '.dat')
        if not bayes_obj.export_bayes(file_path):
            log.error("can not save bayes object")
        bayes_obj_list.append(bayes_obj)
    # endregion

fig, arr = plt.subplots(2, 1)
fig_set = 1

for lia in range(len(bayes_obj.exp_method.local_convergence)):

    arr[0].loglog(np.array([lic for lic in
                            range(1, len(bayes_obj.exp_method.local_convergence[lia]) + 1)]),
                  bayes_obj.exp_method.local_convergence[lia])
    arr[1].set_title('logarithmic error development')
    # arr[0, 0].xlabel('# of steps')
    # arr[0, 0].ylabel('log MC error')
    # arr[0, 0].xlabel('# of steps')
    # arr[0, 0].ylabel('MC error')
arr[1].plot(bayes_obj.exp_method.step_size_list, 'or')
arr[1].set_title('step sizes used in Euler method')
# arr[0, 0].ylabel('step size')
plt.draw()
plt.savefig(file_path + bayes_obj.file_name + "_euler.pdf", dpi=None, facecolor='w', edgecolor='w',
            orientation='portrait', papertype=None, format='pdf',
            transparent=False, bbox_inches='tight', pad_inches=0.1,
            frameon=None)

assert len(bayes_obj_list) == len(n_dofs_list)      # plot only if enough data is found
poly_mean = []
poly_max_arg = []
poly_mean_error = []
poly_max_arg_error = []
poly_mean_error_l2 = []
poly_max_arg_error_l2 = []
poly_var = []
for lia in range(len(bayes_obj_list)):
    poly_mean_local = []
    poly_max_arg_local = []
    poly_var_local = []
    for i, yi in enumerate(bayes_obj_list[lia].eval_densities):
        I_i = interp1d(bayes_obj_list[lia].eval_grid_points, yi, kind='cubic')
        poly_mean_local.append(quad(lambda _x3: _x3 * I_i(_x3) / bayes_obj_list[lia].mean * 0.5, -1,
                                    bayes_obj_list[lia].eval_grid_points[-1])[0])
        poly_max_arg_local.append(bayes_obj_list[lia].eval_grid_points[np.argmax(yi)])
        poly_var_local.append(quad(lambda _x4:
                                   (_x4 - poly_mean_local[-1]) ** 2 * I_i(_x4) / bayes_obj_list[lia].mean * 0.5, -1,
                                   bayes_obj_list[lia].eval_grid_points[-1])[0])

    poly_mean.append(poly_mean_local)
    poly_max_arg.append(poly_max_arg_local)
    poly_var.append(poly_var_local)
    poly_mean_error.append(np.abs(np.array(poly_mean_local) - np.array(bayes_obj_list[lia].true_values)))
    poly_max_arg_error.append(np.abs(np.array(poly_max_arg_local) - np.array(bayes_obj_list[lia].true_values)))
    poly_mean_error_l2.append(np.linalg.norm(np.array(poly_mean_local) - np.array(bayes_obj_list[lia].true_values)))
    poly_max_arg_error_l2.append(np.linalg.norm(np.array(poly_max_arg_local) -
                                                np.array(bayes_obj_list[lia].true_values)))

plt.clf()

plt.semilogy(n_dofs_list, poly_mean_error_l2, '-r', label='l2 error mean')
plt.semilogy(n_dofs_list, poly_max_arg_error_l2, '-b', label='l2 error argmax')
plt.legend(ncol=2, loc=1)
plt.title('mean error of estimated parameters to correct values for increasing measurements in l2 norm')
plt.xlabel('# of measurements')
plt.ylabel('Error')
plt.draw()
plt.savefig(file_path + bayes_obj_list[-1].file_name + "_l2error_meas.pdf", dpi=None, facecolor='w', edgecolor='w',
            orientation='portrait', papertype=None, format='pdf',
            transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None)

poly_mean_error = np.array(poly_mean_error)
poly_max_arg_error = np.array(poly_max_arg_error)
poly_var = np.array(poly_var)
plt.clf()
for lia in range(n_coefficients):
    plt.semilogy(n_dofs_list, poly_mean_error[:, lia], label='mean error coeff %i' % (lia+1))
plt.legend(ncol=2, loc=1)
plt.title('mean error of estimated parameters to correct values for increasing measurements')
plt.xlabel('# of measurements')
plt.ylabel('Error')
plt.draw()
plt.savefig(file_path + bayes_obj_list[-1].file_name + "_error_mean_meas.pdf", dpi=None, facecolor='w',
            edgecolor='w', orientation='portrait', papertype=None, format='pdf',
            transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None)
plt.clf()
for lia in range(n_coefficients):
    plt.semilogy(n_dofs_list, poly_max_arg_error[:, lia], label='argmax error coeff %i' % (lia + 1))
plt.legend(ncol=2, loc=1)
plt.title('mean error of estimated parameters to correct values for increasing measurements')
plt.xlabel('# of measurements')
plt.ylabel('Error')
plt.draw()
plt.savefig(file_path + bayes_obj_list[-1].file_name + "_error_argmax_meas.pdf", dpi=None, facecolor='w',
            edgecolor='w',
            orientation='portrait', papertype=None, format='pdf',
            transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None)
plt.clf()
for lia in range(n_coefficients):
    plt.semilogy(n_dofs_list, poly_var[:, lia], label='variance of coeff %i' % (lia + 1))
plt.legend(ncol=2, loc=1)
plt.title('variance of estimated parameters for increasing measurements')
plt.xlabel('# of measurements')
plt.ylabel('variance')
plt.draw()
plt.savefig(file_path + bayes_obj_list[-1].file_name + "_var_meas.pdf", dpi=None, facecolor='w',
            edgecolor='w',
            orientation='portrait', papertype=None, format='pdf',
            transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None)

# for i, yi in enumerate(y_int_poly):
#    plt.plot(eval_grid, yi/mean/2, label="y_{0} soll {1}".format(i, true_y[i]), color=color_list[i % len(color_list)])
#    plt.vlines(true_y[i], 0, max(yi/mean/2), color=color_list[i % len(color_list)])
#    plt.legend(ncol=len(y_int_poly), loc=3)
# plt.show()
