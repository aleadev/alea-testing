# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  test script for the bayesian procedure using direct sampling of the Karhunen Loeve expansion
"""
# region Imports
from __future__ import division

import logging.config

import matplotlib.pyplot as plt
import numpy as np
from Bayes_util.lib.bayes_lib import create_parameter_samples, create_noisy_measurement_data_dofs

from Bayes_util.asgfem_mcmc_bayes import ASGFEMBayesMCMC
from Bayes_util.lib.bayes_lib_simple import create_unique_dofs

import corner                                       # needed for cool corner plots
# endregion

# region Setup parameters

#   region Parameters to use in stochastic Galerkin for the backward solution
refinements = 10 			                        # # of refinement steps in sg
srank = 8					                        # start ranks
gpcdegree = 10					                    # maximal # of gpcdegrees
femdegree = 3					                    # fem function degree
max_dofs = 1e5                                      # desired maximum of dofs to reach in refinement process
iterations = 500                                    # used iterations in tt ALS
polysys = "L"                                       # used polynomial system
domain = "square"                                   # used domain, square, L-Shape, ...
# coeffield_type = "EF-square-cos"
# coeffield_type = "EF-square-sin"
# coeffield_type = "monomial"
# coeffield_type = "linear"
# coeffield_type = "constant"                       # used coefficient field type
coeffield_type = "cos"
amp_type = "decay-inf"
decay_exp_rate = 2                                  # decay rate for non constant coefficient fields
sgfem_gamma = 0.9                                   # Gamma used in adaptive SG FEM
rvtype = "uniform"
init_mesh_dofs = 1e4                                # initial mesh refinement. -1 : ignore
thetax = 1                                          # factor of deterministic refinement (0 : no refinement)
thetay = 0                                          # factor of stochastic refinement (0 : no refinement)
#   endregion

# region Solution parameter
n_coefficients = 10                                 # number of coefficients
USE_DOFS = False
n_dofs = 54                                         # number of dofs
n_dofs_list = [2**lia + 2 for lia in range(6)]      # list of increasing dofs

sample_coordinates = [[0.25, 0.25], [0.25, 0.5], [0.25, 0.75],
                      [0.5, 0.25],  [0.5, 0.5],  [0.5, 0.75],
                      [0.75, 0.25], [0.75, 0.5], [0.75, 0.75]]

#sample_coordinates = [[1/7, 1/7], [1/7, 2/7], [1/7, 3/7], [1/7, 4/7], [1/7, 5/7], [1/7, 6/7],
#                      [2 / 7, 1 / 7], [2 / 7, 2 / 7], [2 / 7, 3 / 7], [2 / 7, 4 / 7], [2 / 7, 5 / 7], [2 / 7, 6 / 7],
#                      [3 / 7, 1 / 7], [3 / 7, 2 / 7], [3 / 7, 3 / 7], [3 / 7, 4 / 7], [3 / 7, 5 / 7], [3 / 7, 6 / 7],
#                      [4 / 7, 1 / 7], [4 / 7, 2 / 7], [4 / 7, 3 / 7], [4 / 7, 4 / 7], [4 / 7, 5 / 7], [4 / 7, 6 / 7],
#                      [5 / 7, 1 / 7], [5 / 7, 2 / 7], [5 / 7, 3 / 7], [5 / 7, 4 / 7], [5 / 7, 5 / 7], [5 / 7, 6 / 7],
#                      [6 / 7, 1 / 7], [6 / 7, 2 / 7], [6 / 7, 3 / 7], [6 / 7, 4 / 7], [6 / 7, 5 / 7], [6 / 7, 6 / 7]
#                      ]
# n_dofs_list = [n_dofs]
# endregion

# region MCMC parameter
n_walker = 24
burn_in = 10
n_samples = 500
covariance = 0.0001
# endregion

color_list = ['olive', 'gold', 'aqua', 'black', 'blue', 'brown', 'green', 'red', 'orange']

logging.config.fileConfig('logging.conf')           # load logger configuration
log = logging.getLogger('test_bayes')               # get new logger object

# endregion

# region  setup truth, noise and prior
log.info("Create %i true parameters", n_coefficients)
true_values = create_parameter_samples(n_coefficients)
unique_dofs = []
if USE_DOFS:
    unique_dofs = np.array(create_unique_dofs(n_dofs_list[-1], init_mesh_dofs))
# endregion

# region Setup Gaussian Prior
Gauss_prior = False
if Gauss_prior:
    raise NotImplemented
    # pi_y =
    # [lambda x: 1/(np.sqrt(2*np.pi)*sigma_i)*np.exp(-0.5*((x-mu_i)/sigma_i)**2) for mu_i, sigma_i in zip(mu, sigma)]

else:
    prior_densities = [lambda x: 0.5*np.ones_like(x)]*n_coefficients
# endregion

file_path = 'results/mcmc/'

bayes_obj_list = []
coeff_field = None
proj_basis = None
for step, n_dofs in enumerate(n_dofs_list):

    if USE_DOFS:
        log.info('Create %i unique random dofs', n_dofs)
        dofs = unique_dofs[:n_dofs]
    else:
        dofs = []
    log.info("Create Bayes Object")
    bayes_obj = ASGFEMBayesMCMC.init_solver(dofs, sample_coordinates,  true_values, n_walker, covariance=covariance,
                                            burn_in=burn_in, mc_steps=n_samples, iterations=iterations,
                                            refinements=refinements, max_dofs=max_dofs, thetax=thetax, thetay=thetay,
                                            srank=srank, femdegree=femdegree, gpcdegree=gpcdegree, polysys=polysys,
                                            gamma=sgfem_gamma, rvtype=rvtype, domain=domain, mesh_dofs=init_mesh_dofs,
                                            problem=0, coeffield_type=coeffield_type, amp_type=amp_type,
                                            m=n_coefficients+1)

    # region Create Bayes solution either from pickling or calculation
    fig_set = 0
    fig = plt.figure()
    if bayes_obj.import_bayes(file_path):
        log.info('open Bayes file %s', file_path + bayes_obj.file_name + '.dat')
        bayes_obj_list.append(bayes_obj)
        # region results presentation area
        plt.clf()
        if fig_set != 0:
            fig.clf()
        try:

            plt.clf()
            # region Plot MCMC results
            for i in range(len(bayes_obj.true_values)):
                plt.figure()
                plt.hist(bayes_obj.sampler.flatchain[:, i], 100, color="k", histtype="step")
                plt.title("MCMC surrogate Bayes Result. Coefficient {0:d}, "
                          "should be {1:f}".format(i + 1, bayes_obj.true_values[i]))

            # plt.show()
            fig_set = 1
            # plt.waitforbuttonpress()
            fig = corner.corner(bayes_obj.sampler.chain[:, 50:, :].reshape((-1, len(bayes_obj.true_values))),
                                labels=["$xi_1$", "$xi_2$", "$xi_3$"],
                                truths=[bayes_obj.true_values[0], bayes_obj.true_values[1], bayes_obj.true_values[2]])
            fig.savefig(file_path + str(hash(bayes_obj.file_name)) + "MCMC_triangle.png")
            # endregion

            # region Plot ForwardOperator Error
            from mpl_toolkits.mplot3d import Axes3D
            from matplotlib import cm
            from matplotlib.ticker import LinearLocator, FormatStrFormatter

            fig = plt.figure()

            ax = fig.gca(projection='3d')
            ax.text2D(0.05, 1, "MC ASGFEM Operator error", transform=ax.transAxes)
            X = np.linspace(0, 1, 10, endpoint=True)
            Y = np.linspace(0, 1, 10, endpoint=True)
            X, Y = np.meshgrid(X, Y)
            surf = ax.plot_surface(X, Y, np.array(bayes_obj.forwardOp.sampling_error).reshape(10, 10), rstride=1,
                                   cstride=1, cmap=cm.coolwarm,
                                   linewidth=0, antialiased=False)
            ax.set_zlim(0, 0.1)

            #ax.zaxis.set_major_locator(LinearLocator(10))
            #ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

            fig.colorbar(surf, shrink=0.5, aspect=5)
            fig.savefig(file_path + str(hash(bayes_obj.file_name)) + "ASGFEM_error.png")
            # endregion
            # region Plot Coefficient Field
            plt.clf()
            fig.clf()

            #if coeff_field is not None:
            #    bayes_obj.forwardOp.coefficient_field = coeff_field
            #if proj_basis is not None:
            #    bayes_obj.forwardOp.proj_basis = proj_basis
            #bayes_obj.get_true_coefficient_field.plot(title="True coefficient Field")
            #if coeff_field is None:
            #    coeff_field = bayes_obj.forwardOp.get_coefficient_field
            #if proj_basis is None:
            #    proj_basis = bayes_obj.forwardOp.get_proj_basis
            #bayes_obj.get_parametric_coefficient_field(poly_mean).plot(title="Approx coefficient Field")
            # endregion

            # region Plot density means
            #plt.clf()
            #x_dim = np.linspace(1, len(poly_mean), num=len(poly_mean), endpoint=True)
            #plt.semilogy(x_dim, np.abs(np.array(poly_mean) - np.array(bayes_obj.true_values)), 'or', label='mean')
            #plt.semilogy(x_dim, np.abs(np.array(poly_max_arg) - np.array(bayes_obj.true_values)), 'ob', label='argmax')
            #plt.semilogy(x_dim, [np.linalg.norm(np.array(poly_mean) - np.array(bayes_obj.true_values))]*len(x_dim),
            #             label='l2 norm mean')
            #plt.semilogy(x_dim, [np.linalg.norm(np.array(poly_max_arg) - np.array(bayes_obj.true_values))]*len(x_dim),
            #             label='l2 norm argmax')
            #plt.legend(ncol=2, loc=1)
            #plt.xlim([0.9, x_dim[-1]+0.1])
            #plt.title('error of estimated parameters to correct values')
            #plt.xlabel('Parameter')
            #plt.ylabel('Error')
            #plt.draw()
            #plt.savefig(file_path + "M{0}_g{1}_Dof{2}".format(len(bayes_obj.true_values), bayes_obj.inp_gamma, len(bayes_obj.measurements)) + bayes_obj.file_name + "_error.pdf", dpi=None, facecolor='w', edgecolor='w',
            #            orientation='portrait', papertype=None, format='pdf',
            #            transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None)
            ## plt.show()#

            # endregion
            # region Plot density variances
            #plt.clf()
            #x_dim = np.linspace(1, len(poly_var), num=len(poly_var), endpoint=True)
            #plt.plot(x_dim, poly_var, 'or')
            ## plt.legend(ncol=1, loc=1)
            #plt.xlim([0.9, x_dim[-1] + 0.1])
            #plt.title('variance of estimations')
            #plt.xlabel('Parameter')
            #plt.ylabel('Variance')
            #plt.draw()
            #plt.savefig(file_path + "M{0}_g{1}_Dof{2}".format(len(bayes_obj.true_values), bayes_obj.inp_gamma, len(bayes_obj.measurements)) + bayes_obj.file_name + "_var.pdf", dpi=None, facecolor='w', edgecolor='w',
            #            orientation='portrait', papertype=None, format='pdf',
            #            transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None)
            ## plt.show()

            # endregion

        except Exception as ex:
            print "no convergence " + ex.message
        # endregion

    else:
        log.info('start Bayesian procedure')
        if USE_DOFS:
            measurements = create_noisy_measurement_data_dofs(bayes_obj.solution, true_values, dofs, covariance)
        else:
            measurements = np.array([bayes_obj.forwardOp.compute_direct_sample_solution(true_values)(sample_coordinate)
                                     for sample_coordinate in sample_coordinates])
        bayes_obj.measurements = measurements
        bayes_obj.calculate_samples_direct()

        log.info('end Bayesian procedure')

        log.info('export Bayes file %s', file_path + bayes_obj.file_name + '.dat')
        if not bayes_obj.export_bayes(file_path):
            log.error("can not save bayes object")
        bayes_obj_list.append(bayes_obj)
    if not USE_DOFS:
        break
    # endregion
