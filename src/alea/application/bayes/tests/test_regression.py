from __future__ import division
import numpy as np
from os import path as os_path
from sys import path as sys_path
sys_path.insert(0, os_path.pardir)
import matplotlib.pyplot as plt
import itertools

KERNELS = ["GAUSS", "POLY"]
KERNEL = KERNELS[0]

n_samples = 600
nodes1 = np.random.rand(int(n_samples/6), 2)*0.2-0.9
values1 = np.random.rand(int(n_samples/6))*0.2

nodes2 = np.random.rand(int(n_samples/6), 2)*0.2-0.2
values2 = np.random.rand(int(n_samples/6))*0.1+0.8

nodes3 = np.random.rand(int(n_samples/6), 2)*0.2
values3 = np.random.rand(int(n_samples/6))*0.2+0.3

nodes4 = np.random.rand(int(n_samples/6), 2)*0.2-0.6
values4 = np.random.rand(int(n_samples/6))*0.2+0.4

nodes5 = np.random.rand(int(n_samples/6), 2)*0.2-0.4
values5 = np.random.rand(int(n_samples/6))*0.2

nodes6 = np.random.rand(int(n_samples/6), 2)*2 - 1
values6 = np.random.rand(int(n_samples/6))

nodes = np.concatenate([nodes1, nodes2, nodes3, nodes4, nodes5, nodes6])
values = np.concatenate([values1, values2, values3, values4, values5, values6])

nX, nY = np.meshgrid(np.linspace(-1, 1, int(np.sqrt(n_samples)), endpoint=True), np.linspace(-1, 1, int(np.sqrt(n_samples)), endpoint=True))

nodes = np.vstack((nX.flatten(), nY.flatten())).T
values = np.array([0.9 - nodes[lia][0] + np.random.randn()*0.3 for lia in range(len(nodes)) ])
# print "f({}) = {}".format(nodes,values)
lamb = 0.5
if KERNEL == "GAUSS":
    delta = 0.5

    def k(x, y):
        if not isinstance(x, list):
            x = [x]
        if not isinstance(y, list):
            y = [y]
        assert len(x) == len(y)
        return np.prod([np.exp(-delta*(x[lia_x] - y[lia_x])**2) for lia_x in range(len(x))])
if KERNEL == "POLY":
    n = 6

    def k(x, y):
        if not isinstance(x, list):
            x = [x]
        if not isinstance(y, list):
            y = [y]
        assert len(x) == len(y)

        # print "T[{},{}]={}".format(x,y,np.sum(np.array([x[lia_x]*y[lia_x] for lia_x in range(len(x))]),axis=1))
        return (1 + np.sum(np.array([x[lia_x]*y[lia_x] for lia_x in range(len(x))]),axis=1)[0])**n
T = np.array([[k(x_i, x_j) for x_i in nodes] for x_j in nodes])

beta = np.linalg.solve((T+lamb*np.eye(T.shape[0])), values)

func = lambda x: np.sum(np.array([k(x, x_k)*beta[lix] for lix, x_k in enumerate(nodes)]), axis=0)
x = np.linspace(-1, 1, 5, endpoint=True)
y = np.linspace(-1, 1, 5, endpoint=True)
X, Y = np.meshgrid(x,y)

val = [[func(tuple([x_, y_])) for x_ in x] for y_ in y]
from mpl_toolkits.mplot3d import Axes3D
fig = plt.figure()
ax = fig.gca(projection='3d')
#plt.xlim([-1, 1])
#plt.ylim([-0.2, 1.2])
for lia in range(len(nodes)):
    ax.scatter(nodes[lia][0],nodes[lia][1], values[lia], c="r", marker="o")
surf = ax.plot_surface(X, Y, val, rstride=1, cstride=1,
                       linewidth=0, antialiased=False)
ax.set_zlim(-1.01, 1.01)

#plt.plot(x, val)
plt.show()
