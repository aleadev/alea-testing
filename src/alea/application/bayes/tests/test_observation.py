# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  test script for the bayesian procedure using direct sampling of the Karhunen Loeve expansion
"""
# region Imports
from __future__ import division

import tt_als
import logging.config
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from Bayes_util.lib.bayes_lib import create_parameter_samples, create_noisy_measurement_data_dofs
from Bayes_util.exponential.implicit_euler import ImplicitEuler
from scipy.integrate import quad
from scipy.interpolate import interp1d

from Bayes_util.asgfem_bayes import ASGFEMBayes
from Bayes_util.asgfem_mcmc_bayes import ASGFEMBayesMCMC
from Bayes_util.exponential.explicit_euler import ExplicitEuler
from Bayes_util.exponential.embedded_runge_kutta import EmbeddedRungeKutta
from Bayes_util.lib.bayes_lib_simple import create_unique_dofs
from Bayes_util.lib.interpolation_util import get_cheb_points
# endregion

###################### ! Deactivate sampling in stochastic space first ! ###########################
#   region Parameters to use in stochastic Galerkin for the backward solution
refinements = 5 			                        # # of refinement steps in sg
srank = 10					                        # start ranks
gpcdegree = 10					                    # maximal # of gpcdegrees
femdegree = 1					                    # fem function degree
max_dofs = 1e8                                      # desired maximum of dofs to reach in refinement process
iterations = 502                                    # used iterations in tt ALS
polysys = "L"                                       # used polynomial system
domain = "square"                                   # used domain, square, L-Shape, ...
# coeffield_type = "EF-square-cos"
# coeffield_type = "EF-square-sin"
# coeffield_type = "monomial"
# coeffield_type = "linear"
# coeffield_type = "constant"                       # used coefficient field type
coeffield_type = "cos"
# amp_type = "constant"
amp_type = "decay-inf"
decay_exp_rate = 0
field_mean = 2                                    # decay rate for non constant coefficient fields
sgfem_gamma = 0.9                                   # Gamma used in adaptive SG FEM
rvtype = "uniform"
init_mesh_dofs = 1e2                                # initial mesh refinement. -1 : ignore
thetax = 0.5                                        # factor of deterministic refinement (0 : no refinement)
thetay = 0.5                                        # factor of stochastic refinement (0 : no refinement)
#   endregion

# region Solution parameter
n_coefficients = 5                                  # number of coefficients
USE_DOFS = False
n_dofs = 54                                         # number of dofs
n_dofs_list = [2**lia + 2 for lia in range(6)]      # list of increasing dofs

sample_coordinates = [[0.25, 0.25], [0.25, 0.5], [0.25, 0.75],
                      [0.5, 0.25],  [0.5, 0.5],  [0.5, 0.75],
                      [0.75, 0.25], [0.75, 0.5], [0.75, 0.75]]

# sample_coordinates = [[1/7, 1/7], [1/7, 2/7], [1/7, 3/7], [1/7, 4/7], [1/7, 5/7], [1/7, 6/7],
#                      [2 / 7, 1 / 7], [2 / 7, 2 / 7], [2 / 7, 3 / 7], [2 / 7, 4 / 7], [2 / 7, 5 / 7], [2 / 7, 6 / 7],
#                      [3 / 7, 1 / 7], [3 / 7, 2 / 7], [3 / 7, 3 / 7], [3 / 7, 4 / 7], [3 / 7, 5 / 7], [3 / 7, 6 / 7],
#                      [4 / 7, 1 / 7], [4 / 7, 2 / 7], [4 / 7, 3 / 7], [4 / 7, 4 / 7], [4 / 7, 5 / 7], [4 / 7, 6 / 7],
#                      [5 / 7, 1 / 7], [5 / 7, 2 / 7], [5 / 7, 3 / 7], [5 / 7, 4 / 7], [5 / 7, 5 / 7], [5 / 7, 6 / 7],
#                      [6 / 7, 1 / 7], [6 / 7, 2 / 7], [6 / 7, 3 / 7], [6 / 7, 4 / 7], [6 / 7, 5 / 7], [6 / 7, 6 / 7]
#                      ]
# endregion

# region Observation parameter
obs_precision = 1e-15
obs_rank = 500
# endregion

# region Inner Product parameter
inp_precision = 1e-15
inp_rank = 500
inp_gamma = 0.0001
# endregion

# region Euler Parameter
euler_precision = 1e-10                             # precision to use in euler rounding
euler_rank = 50                                     # rank to round down in euler method
euler_local_mc_samples = 1                          # samples in every euler step
euler_global_mc_samples = 1000                      # samples for the final result
euler_repetitions = 0                               # number of repetitions with half step size
euler_refinements = None

euler_steps = 1000                              # start euler steps
euler_global_precision = 1e-5                       # global precision to reach after the euler scheme
euler_use_implicit = True
# endregion

# region Density parameters
n_eval_grid = 50
# endregion

# region setup discretisation
Nx, Ny = 10000, 20
dx, dy = 1/Nx, 1/Ny
eval_grid_points = np.linspace(-1, 1, n_eval_grid)
quadrature_refinements = None
stochastic_grid = np.array(get_cheb_points([Ny] * n_coefficients)[0])
# endregion

# endregion

file_path = "tests/ObservationResults/"

bayes_obj = None
bayes_obj_list = []
unique_dofs = []
coeff_field = None
proj_basis = None
true_values = None
prior_densities = None

potential_mc_samples = 100                           # number of mc samples to estimate the error
potential_mc_sample = None                          # current mc samples
potential_mc_sample_list = []                       # list of mc samples of the potential
potential_x_achses = []

euler_method = EmbeddedRungeKutta(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
                                              euler_repetitions, euler_local_mc_samples)
euler_method.method = "forth"
euler_method.acc = euler_global_precision
euler_method.min_stepsize = 1e-19
euler_method.max_stepsize = 1.0 - 1e-10
dofs = []
bayes_obj = ASGFEMBayes.init_solver(dofs, sample_coordinates, true_values, obs_precision,
                                            obs_rank, inp_precision, inp_rank, inp_gamma, euler_method,
                                            euler_global_precision, eval_grid_points, prior_densities,
                                            stochastic_grid,
                                            iterations=iterations, refinements=refinements, max_dofs=max_dofs,
                                            thetax=thetax,
                                            thetay=thetay, srank=srank,  m=n_coefficients+1,
                                            femdegree=femdegree, gpcdegree=gpcdegree, polysys=polysys,
                                            gamma=sgfem_gamma, rvtype=rvtype, domain=domain,
                                            mesh_dofs=init_mesh_dofs, problem=0,
                                            coeffield_type=coeffield_type,
                                            amp_type=amp_type, sol_index=-1
                                            )

true_values = create_parameter_samples(bayes_obj.forwardOp.coefficients)
if USE_DOFS:
    measurements = create_noisy_measurement_data_dofs(bayes_obj.solution, true_values, dofs, inp_gamma)
else:
    measurements = np.array([bayes_obj.forwardOp.compute_direct_sample_solution(true_values)(sample_coordinate)
                             for sample_coordinate in sample_coordinates])
    for lic in range(len(measurements)):
        measurements[lic] += np.random.randn() * inp_gamma
# measurements = create_noisy_measurement_data_dofs(bayes_obj.solution, true_values, dofs, inp_gamma)
bayes_obj.measurements = measurements
bayes_obj.true_values = true_values
bayes_obj.prior_densities = prior_densities

bayes_obj.set_sol_index(-1)
bayes_obj.calculate_densities(stop_at_observation=True)

from joblib import Parallel, delayed
n_cpu = 40
xi_samples = np.array([np.random.rand(bayes_obj.forwardOp.coefficients+1)*2 - 1 for _
                               in range(potential_mc_samples)])
for lix in range(potential_mc_samples):
    xi_samples[lix][0] = 0

observation_list = bayes_obj.solution
from tt_als.alea.stochastics.random_variable import UniformRV
from tt_als.tensorsolver.tt_param_pde import ttRightHalfdot
def _samplePotentialParallel(xi):

    observed_measurements = []
    sol_buffer = bayes_obj.forwardOp.sample_at_coeff(list(xi[1:]))
    sol_measurements = np.array([sol_buffer(loc_dof) for loc_dof in bayes_obj.sample_coordinates])
    # get polys
    rv = UniformRV()
    # evaluate stochastic polynomials for sample
    poly = rv.orth_polys
    for lix in range(len(observation_list)):
        px = np.array(poly.eval(max(observation_list[lix].n), xi[1:], all_degrees=True))
        sample_sol = ttRightHalfdot(observation_list[lix], px)
        observed_measurements.append(sample_sol[0])
    diff = np.abs(sol_measurements - observed_measurements)
    return diff
potential_mc_sample = np.sum(np.array(Parallel(n_cpu)(delayed(_samplePotentialParallel)(xi) for xi in xi_samples)), axis=0)
potential_mc_sample /= len(xi_samples)
plt.semilogy(np.array(potential_mc_sample),
                 '-or', label="error sampling of the observation")
plt.title("Error of the observation per measurement. MC sampled with the true values.")
#plt.legend(ncol=2, loc=1)
plt.xlabel("measurement index")
plt.ylabel("ERROR of the observation")
plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "observation_error.png")
plt.clf()
