# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  test script for the bayesian procedure using direct sampling of the Karhunen Loeve expansion
"""
# region Imports
from __future__ import division

import tt_als
import logging.config
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from Bayes_util.lib.bayes_lib import create_noisy_measurement_data_dofs
from Bayes_util.exponential.implicit_euler import ImplicitEuler
from scipy.integrate import quad
from scipy.interpolate import interp1d

from Bayes_util.asgfem_bayes import ASGFEMBayes
from Bayes_util.exponential.embedded_runge_kutta import EmbeddedRungeKutta
from Bayes_util.lib.bayes_lib_simple import create_unique_dofs
from Bayes_util.lib.interpolation_util import get_cheb_points

from scipy.interpolate import interp2d
from scipy.integrate import dblquad
__author__ = "marschall"
# endregion


METHODS = ["", "measurements"]
METHOD = METHODS[1]
# region Setup parameters
#############################
#   Testsets
#   Set 1   for Method quadrature
#       refinements = 1
#       srank = 8
#       gpcdegree = 10
#       femdegree = 3
#       max_dofs = 1e5
#       iterations = 500
#       n_coefficients = 10
#       init_mesh_dofs = 1e4
#
#   Set 2   for method euler
#       refinements = 1
#       srank = 8
#       gpcdegree = 10
#       femdegree = 3
#       max_dofs = 1e5
#       iterations = 600
#       n_coefficients = 4
#       init_mesh_dofs = 1e4
#
#   Set 3
#       refinements = 10
#       srank = 8
#       gpcdegree = 10
#       femdegree = 3
#       max_dofs = 1e7
#       iterations = 500
#       n_coefficients = 10
#       init_mesh_dofs = 1e5
#
#   Set 4
#       refinements = 1
#       srank = 8
#       gpcdegree = 10
#       femdegree = 1
#       max_dofs = 1e5
#       iterations = 500
#       n_coefficients = 4
#       init_mesh_dofs = 1e4
# Test for sol convergence
#   Set 5
#       refinements = 15
#       srank = 8
#       gpcdegree = 10
#       femdegree = 3
#       max_dofs = 1e8
#       iterations = 600
#       n_coefficients = 5
#       init_mesh_dofs = 10
# Test for sol convergence with fix stoch dimension
#   Set 6
#       refinements = 10
#       srank = 8
#       gpcdegree = 10
#       femdegree = 3
#       max_dofs = 1e8
#       iterations = 600
#       n_coefficients = 5
#       init_mesh_dofs = 10
#   region Parameters to use in stochastic Galerkin for the backward solution
refinements = 5 			                        # # of refinement steps in sg
srank = 8					                        # start ranks
gpcdegree = 10					                    # maximal # of gpcdegrees
femdegree = 1					                    # fem function degree
max_dofs = 1e8                                      # desired maximum of dofs to reach in refinement process
iterations = 506                                    # used iterations in tt ALS
polysys = "L"                                       # used polynomial system
domain = "square"                                   # used domain, square, L-Shape, ...
# coeffield_type = "EF-square-cos"
# coeffield_type = "EF-square-sin"
# coeffield_type = "monomial"
# coeffield_type = "linear"
# coeffield_type = "constant"                       # used coefficient field type
coeffield_type = "cos"
amp_type = "constant"
# amp_type = "decay-inf"
field_mean = 2
decay_exp_rate = 0                                  # decay rate for non constant coefficient fields
sgfem_gamma = 0.9                                   # Gamma used in adaptive SG FEM
rvtype = "uniform"
init_mesh_dofs = 1e2                                # initial mesh refinement. -1 : ignore
thetax = 0.5                                        # factor of deterministic refinement (0 : no refinement)
thetay = 0.5                                        # factor of stochastic refinement (0 : no refinement)
#   endregion

# region Solution parameter
n_coefficients = 2                                  # number of coefficients
USE_DOFS = False
n_dofs = 54                                         # number of dofs
n_dofs_list = [2**lia + 2 for lia in range(6)]      # list of increasing dofs

sample_coordinates = []
measurement_refinements = None
if METHOD == "measurements":
    measurement_refinements = 1                     # number of refinements, i.e. number of nodes squared start from 4
    for lia in range(measurement_refinements):
        sample_coordinates.append([[i/int(np.sqrt((lia+3)**2)+1), j/int(np.sqrt((lia+3)**2)+1)]
                                   for i in range(1, int(np.sqrt((lia+3-1)**2)))
                                  for j in range(1, int(np.sqrt((lia+3-1)**2)))])
else:
    # sample_coordinates = [[0.25, 0.25], [0.25, 0.5], [0.25, 0.75],
    #                      [0.5, 0.25],  [0.5, 0.5],  [0.5, 0.75],
    #                      [0.75, 0.25], [0.75, 0.5], [0.75, 0.75]]

    #sample_coordinates = [[1/7, 1/7], [1/7, 2/7], [1/7, 3/7], [1/7, 4/7], [1/7, 5/7], [1/7, 6/7],
    #                      [2/7, 1/7], [2/7, 2/7], [2/7, 3/7], [2/7, 4/7], [2/7, 5/7], [2/7, 6/7],
    #                      [3/7, 1/7], [3/7, 2/7], [3/7, 3/7], [3/7, 4/7], [3/7, 5/7], [3/7, 6/7],
    #                      [4/7, 1/7], [4/7, 2/7], [4/7, 3/7], [4/7, 4/7], [4/7, 5/7], [4/7, 6/7],
    #                      [5/7, 1/7], [5/7, 2/7], [5/7, 3/7], [5/7, 4/7], [5/7, 5/7], [5/7, 6/7],
    #                      [6/7, 1/7], [6/7, 2/7], [6/7, 3/7], [6/7, 4/7], [6/7, 5/7], [6/7, 6/7]
    #                      ]
    sample_coordinates = [[i/int(np.sqrt((3)**2)+1), j/int(np.sqrt((3)**2)+1)]
                                   for i in range(1, int(np.sqrt((2)**2)))
                                  for j in range(1, int(np.sqrt((2)**2)))]
# endregion

# region Observation parameter
obs_precision = 1e-15
obs_rank = 100000
# endregion

# region Inner Product parameter
inp_precision = 1e-15
inp_rank = 100000
inp_gamma = 1.0
# endregion

# region Euler Parameter
euler_precision = 1e-10                             # precision to use in euler rounding
euler_rank = 10                                     # rank to round down in euler method
euler_local_mc_samples = 1                          # samples in every euler step
euler_global_mc_samples = 1000                      # samples for the final result
euler_repetitions = 0                               # number of repetitions with halfed step size
euler_refinements = None
euler_steps = 1001                                  # start euler steps
euler_global_precision = 1e-8                       # global precision to reach after the euler scheme
euler_use_implicit = False
# endregion

# region Density parameters
n_eval_grid = 10
# endregion

# region setup discretisation
Nx, Ny = 10000, 10
dx, dy = 1/Nx, 1/Ny
eval_grid_points = np.linspace(-1, 1, n_eval_grid)
quadrature_refinements = None

stochastic_grid = np.array(get_cheb_points([Ny] * n_coefficients)[0])
# endregion

# region MCMC parameter
n_walker = 50
burn_in = 10
n_samples = 100
covariance = 0.0001
# endregion

color_list = ['olive', 'gold', 'aqua', 'black', 'blue', 'brown', 'green', 'red', 'orange']

logging.config.fileConfig('logging.conf')           # load logger configuration
log = logging.getLogger('test_bayes')               # get new logger object

# endregion
file_path = "results/paper/contour/" + amp_type + "/inner_product/"
if thetax == 1:
    file_path += "fix/"

bayes_obj = None
bayes_obj_list = []
unique_dofs = []
coeff_field = None
proj_basis = None
true_values = None
prior_densities = None

true_solution = None
true_field = None

field_error_h1 = []
field_error_l2 = []
solution_error_h1 = []
solution_error_l2 = []
tt_dofs = []
list_z_means = []
list_true_z_means = []
log.info("Create exponential method")
if euler_use_implicit:
    euler_method = ImplicitEuler(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
                                 euler_repetitions, euler_local_mc_samples)
else:
    euler_method = EmbeddedRungeKutta(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
                                      euler_repetitions, euler_local_mc_samples)
    euler_method.method = "forth"
    euler_method.acc = euler_global_precision
    euler_method.min_stepsize = 1e-10
    euler_method.max_stepsize = 1.0 - euler_method.min_stepsize
plt.clf()
dofs = []                                   # we use the coordinate sampling
for lia_measurements in range(measurement_refinements):
    log.info("Create Bayes Object with {} measurement nodes".format(len(sample_coordinates[lia_measurements])))
    bayes_obj = ASGFEMBayes.init_solver(dofs, sample_coordinates[lia_measurements], true_values, obs_precision,
                                        obs_rank, inp_precision, inp_rank, inp_gamma, euler_method,
                                        euler_global_precision, eval_grid_points, prior_densities,
                                        stochastic_grid,
                                        iterations=iterations, refinements=refinements, max_dofs=max_dofs,
                                        thetax=thetax,
                                        thetay=thetay, srank=srank,  m=n_coefficients+1,
                                        femdegree=femdegree, gpcdegree=gpcdegree, polysys=polysys,
                                        gamma=sgfem_gamma, rvtype=rvtype, domain=domain,
                                        mesh_dofs=init_mesh_dofs, problem=0,
                                        coeffield_type=coeffield_type,
                                        amp_type=amp_type, field_mean=field_mean
                                        )
    for lia in range(len(bayes_obj.forwardOp.SOL)):
        bayes_obj.set_sol_index(lia)
        if bayes_obj.import_bayes(file_path):
            bayes_obj_list.append(bayes_obj)
        else:
            log.info('start Bayesian procedure with {} measurement nodes'.format(
                len(sample_coordinates[lia_measurements])))
            # region  setup truth, noise and prior
            log.info("Create {} true parameters".format(bayes_obj.forwardOp.coefficients))
            # true_values = create_parameter_samples(bayes_obj.forwardOp.coefficients)
            # true_values = [0, 0.2, -0.2, 0.4]
            true_values = [0.2, -0.4, 0, 0.3, 0.4, 0.5, -0.1, -0.2, -0.3, 0.1, -0.5, 0.05, -0.05, 0.6, 0.7, 0.8, 0.9,
                           -0.6, -0.7, -0.8, -0.9, 0, 0.04]
            if USE_DOFS:
                unique_dofs = np.array(create_unique_dofs(n_dofs_list[-1], init_mesh_dofs))
            # endregion

            # region Setup Gaussian Prior
            Gauss_prior = False
            if Gauss_prior:
                raise NotImplemented
                # pi_y =
                # [lambda x: 1/(np.sqrt(2*np.pi)*sigma_i)*np.exp(-0.5*((x-mu_i)/sigma_i)**2) for mu_i, sigma_i in
                # zip(mu, sigma)]
            else:
                prior_densities = [lambda x: 0.5 * np.ones_like(x)] * bayes_obj.forwardOp.coefficients
            # endregion
            if USE_DOFS:
                measurements = create_noisy_measurement_data_dofs(bayes_obj.solution, true_values, dofs, inp_gamma)
            else:
                buffer_true_solution = bayes_obj.forwardOp.compute_direct_sample_solution(true_values[:len(bayes_obj.solution.n)-1])
                measurements = np.array([buffer_true_solution(sample_coordinate)
                                         for sample_coordinate in sample_coordinates[lia_measurements]])
            # measurements = create_noisy_measurement_data_dofs(bayes_obj.solution, true_values, dofs, inp_gamma)
            bayes_obj.measurements = measurements
            bayes_obj.true_values = true_values[:len(bayes_obj.solution.n)-1]
            bayes_obj.prior_densities = prior_densities
            #bayes_obj.calculate_densities(stop_at_potential=True)
            #if not bayes_obj.export_bayes(file_path):
            #    log.error("can not save bayes object")
        #print("Bayes obj {} loaded from file".format(lia))
            bayes_obj.calculate_densities(stop_at_observation=True)
            if not bayes_obj.export_bayes(file_path):
                log.error("can not save bayes object")

    #print "sampling error: {}".format(bayes_obj.forwardOp.estimate_sampling_error(sol_index=lia))
    #continue

        from Bayes_util.lib.bayes_lib import apply_bayes_inner_product_new
        bayes_obj.solution = apply_bayes_inner_product_new(bayes_obj.solution, bayes_obj.measurements, 1.0, 1e-10, 1000, _round=False)

        plt.clf()
        Z = np.zeros((len(bayes_obj.eval_grid_points),len(bayes_obj.eval_grid_points)))
        from Bayes_util.lib.bayes_lib import evaluate
        X, Y = np.meshgrid(eval_grid_points, eval_grid_points)
        max_value = 0
        max_lix = 0
        max_lic = 0
        for lix in range(len(eval_grid_points)):
            for lic in range(len(eval_grid_points)):
                #Z[lix,lic] = bayes_obj.sample_potential([0]+[eval_grid_points[lix], eval_grid_points[lic]])
                Z[lix, lic] = evaluate(bayes_obj.solution, stochastic_grid, _poly="Lagrange", _samples=[0, eval_grid_points[lix], eval_grid_points[lic]])
                if Z[lix,lic] > max_value:
                    max_value = Z[lix,lic]
                    max_lix = lix
                    max_lic = lic

        CS = plt.contour(X, Y, Z)
        plt.plot(bayes_obj.true_values[1], bayes_obj.true_values[0], 'xb', label="Truth")
        plt.plot(eval_grid_points[max_lic], eval_grid_points[max_lix], 'oc', label="max")
        plt.clabel(CS, inline=1, fontsize=10)
        plt.xlim([-1,1])
        plt.ylim([-1,1])
        plt.legend()
        plt.savefig(file_path + "contour_ASGFEM{}_Measure{}".format(lia, len(bayes_obj.measurements)) +
                    str(hash(bayes_obj.file_name)) + ".png")
        plt.clf()
        Z_no_interp = np.zeros((len(bayes_obj.stoch_grid),len(bayes_obj.stoch_grid)))
        from Bayes_util.lib.bayes_lib import evaluate
        X, Y = np.meshgrid(stochastic_grid, stochastic_grid)
        max_value = 0
        max_lix = 0
        max_lic = 0
        for lix in range(len(stochastic_grid)):
            for lic in range(len(stochastic_grid)):
                Z_no_interp[lix,lic] = bayes_obj.sample_potential_no_interpolation([lix, lic])
                if Z_no_interp[lix,lic] > max_value:
                    max_value = Z_no_interp[lix,lic]
                    max_lix = lix
                    max_lic = lic

        CS = plt.contour(X, Y, Z_no_interp)
        plt.plot(bayes_obj.true_values[1], bayes_obj.true_values[0], 'xb', label="Truth")
        plt.plot(eval_grid_points[max_lic], eval_grid_points[max_lix], 'oc', label="max")
        plt.clabel(CS, inline=1, fontsize=10)
        plt.xlim([-1,1])
        plt.ylim([-1,1])
        plt.legend()
        plt.savefig(file_path + "contour_ASGFEM_no_interp{}_Measure{}".format(lia, len(bayes_obj.measurements)) +
                    str(hash(bayes_obj.file_name)) + ".png")
        plt.clf()
        Z_direct = np.zeros((len(bayes_obj.stoch_grid),len(bayes_obj.stoch_grid)))
        X, Y = np.meshgrid(stochastic_grid, stochastic_grid)
        max_value = 0
        max_lix = 0
        max_lic = 0
        for lix in range(len(stochastic_grid)):
            for lic in range(len(stochastic_grid)):
                Z_direct[lix,lic] = bayes_obj.solution[0, lix, lic]
                if Z_direct[lix,lic] > max_value:
                    max_value = Z_direct[lix,lic]
                    max_lix = lix
                    max_lic = lic

        CS = plt.contour(X, Y, Z_direct)
        plt.plot(bayes_obj.true_values[1], bayes_obj.true_values[0], 'xb', label="Truth")
        plt.plot(eval_grid_points[max_lic], eval_grid_points[max_lix], 'oc', label="max")
        plt.clabel(CS, inline=1, fontsize=10)
        plt.xlim([-1,1])
        plt.ylim([-1,1])
        plt.legend()
        plt.savefig(file_path + "contour_ASGFEM_direct{}_Measure{}".format(lia, len(bayes_obj.measurements)) +
                    str(hash(bayes_obj.file_name)) + ".png")
        plt.clf()
        Z_true = np.zeros((len(bayes_obj.eval_grid_points), len(bayes_obj.eval_grid_points)))
        for lix in range(len(eval_grid_points)):
            print"step {}".format(lix)
            for lic in range(len(eval_grid_points)):
                true_solution = bayes_obj.forwardOp.compute_direct_sample_solution([eval_grid_points[lix],eval_grid_points[lic]], sol_index=lia)
                Z_true[lix,lic] = (np.sum([(bayes_obj.measurements[lib]-(true_solution(sample_coordinates[lia_measurements][lib])))**2 for lib in range(len(sample_coordinates[lia_measurements]))]))
                if Z_true[lix,lic] > max_value:
                    max_value = Z_true[lix,lic]
                    max_lix = lix
                    max_lic = lic
        Z_error = Z - Z_true
        print "true error:  {}".format(np.linalg.norm(Z_error)/np.linalg.norm(Z_true))
        I_approx = interp2d(bayes_obj.eval_grid_points, bayes_obj.eval_grid_points, Z_true, kind='cubic', bounds_error=True)

        from mpl_toolkits.mplot3d import Axes3D
        from matplotlib import cm
        from matplotlib.ticker import LinearLocator, FormatStrFormatter

        fig = plt.figure()
        ax = fig.gca(projection='3d')
        X1 = np.arange(-1, 1, 0.1)
        Y1 = np.arange(-1, 1, 0.1)
        X2, Y2 = np.meshgrid(X1, Y1)
        surf = ax.plot_surface(X2, Y2, I_approx(X1, Y1), rstride=1, cstride=1, cmap=cm.coolwarm,
                               linewidth=0, antialiased=False)
        ax.set_zlim(-0.01, 2.01)

        ax.zaxis.set_major_locator(LinearLocator(10))
        ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

        fig.colorbar(surf, shrink=0.5, aspect=5)

        plt.savefig(file_path + "ZMEAN{}".format(len(bayes_obj.measurements)) +
                    str(hash(bayes_obj.file_name)) + ".png")
        plt.clf()
        X, Y = np.meshgrid(eval_grid_points, eval_grid_points)
        CS = plt.contour(X, Y, Z_true)
        plt.plot(bayes_obj.true_values[1], bayes_obj.true_values[0], 'xb', label="Truth")
        #plt.plot(poly_mean[1], poly_mean[0], 'xr', label="transformed approx")
        #plt.plot(poly_mean_orig[1], poly_mean_orig[0], 'xg', label="approx")
        print "max at ({},{})".format(eval_grid_points[max_lix], eval_grid_points[max_lic])
        plt.plot(eval_grid_points[max_lic], eval_grid_points[max_lix], 'oc', label="max")
        plt.clabel(CS, inline=1, fontsize=10)
        plt.xlim([-1,1])
        plt.ylim([-1,1])
        plt.legend()
        plt.savefig(file_path + "contour_true_Measure{}".format(len(bayes_obj.measurements)) +
                    str(hash(bayes_obj.file_name)) + ".png")
        plt.clf()

        CS = plt.contour(X, Y, Z_error)
        plt.plot(bayes_obj.true_values[1], bayes_obj.true_values[0], 'xb', label="Truth")
        #plt.plot(poly_mean[1], poly_mean[0], 'xr', label="transformed approx")
        #plt.plot(poly_mean_orig[1], poly_mean_orig[0], 'xg', label="approx")
        plt.plot(eval_grid_points[max_lic], eval_grid_points[max_lix], 'oc', label="max")
        plt.clabel(CS, inline=1, fontsize=10)
        plt.xlim([-1,1])
        plt.ylim([-1,1])
        plt.legend()
        plt.savefig(file_path + "contour_errorToTrue_Measure{}".format(len(bayes_obj.measurements)) +
                    str(hash(bayes_obj.file_name)) + ".png")

        plt.clf()
        Z_approx = np.zeros((len(bayes_obj.eval_grid_points), len(bayes_obj.eval_grid_points)))
        for lix in range(len(eval_grid_points)):
            print"step {}".format(lix)
            for lic in range(len(eval_grid_points)):
                true_solution = bayes_obj.forwardOp.sample_at_coeff([eval_grid_points[lix],eval_grid_points[lic]], sol_index=lia)
                Z_approx[lix,lic] = (np.sum([(bayes_obj.measurements[lib]-(true_solution(sample_coordinates[lia_measurements][lib])))**2 for lib in range(len(sample_coordinates[lia_measurements]))]))
                if Z_approx[lix,lic] > max_value:
                    max_value = Z_approx[lix,lic]
                    max_lix = lix
                    max_lic = lic
        Z_approx2 = np.zeros((len(bayes_obj.stoch_grid), len(bayes_obj.stoch_grid)))
        for lix in range(len(stochastic_grid)):
            print"step {}".format(lix)
            for lic in range(len(stochastic_grid)):
                true_solution = bayes_obj.forwardOp.sample_at_coeff([stochastic_grid[lix],stochastic_grid[lic]])
                Z_approx2[lix,lic] = (np.sum([(bayes_obj.measurements[lib]-(true_solution(sample_coordinates[lia_measurements][lib])))**2 for lib in range(len(sample_coordinates[lia_measurements]))]))
        print "error at interpolation nodes: {}".format(np.linalg.norm(Z_direct - Z_approx2)/np.linalg.norm(Z_approx2))

        CS = plt.contour(X, Y, Z_approx)
        plt.plot(bayes_obj.true_values[1], bayes_obj.true_values[0], 'xb', label="Truth")
        #plt.plot(poly_mean[1], poly_mean[0], 'xr', label="transformed approx")
        #plt.plot(poly_mean_orig[1], poly_mean_orig[0], 'xg', label="approx")
        print "max at ({},{})".format(eval_grid_points[max_lix], eval_grid_points[max_lic])
        plt.plot(eval_grid_points[max_lic], eval_grid_points[max_lix], 'oc', label="max")
        plt.clabel(CS, inline=1, fontsize=10)
        plt.xlim([-1,1])
        plt.ylim([-1,1])
        plt.legend()
        plt.savefig(file_path + "contour_approx_Measure{}".format(len(bayes_obj.measurements)) +
                    str(hash(bayes_obj.file_name)) + ".png")
        plt.clf()

        Z_error = Z - Z_approx

        print "Approx error:  {}".format(np.linalg.norm(Z_error)/np.linalg.norm(Z_approx))
        CS = plt.contour(X, Y, Z_error)
        plt.plot(bayes_obj.true_values[1], bayes_obj.true_values[0], 'xb', label="Truth")
        #plt.plot(poly_mean[1], poly_mean[0], 'xr', label="transformed approx")
        #plt.plot(poly_mean_orig[1], poly_mean_orig[0], 'xg', label="approx")
        plt.plot(eval_grid_points[max_lic], eval_grid_points[max_lix], 'oc', label="max")
        plt.clabel(CS, inline=1, fontsize=10)
        plt.xlim([-1,1])
        plt.ylim([-1,1])
        plt.legend()
        plt.savefig(file_path + "contour_errorToApprox_Measure{}".format(len(bayes_obj.measurements)) +
                    str(hash(bayes_obj.file_name)) + ".png")

        plt.clf()
        Z_error = Z_true - Z_approx

        print "Approx vs. True error:  {}".format(np.linalg.norm(Z_error)/np.linalg.norm(Z_approx))

        CS = plt.contour(X, Y, Z_error)
        plt.plot(bayes_obj.true_values[1], bayes_obj.true_values[0], 'xb', label="Truth")
        #plt.plot(poly_mean[1], poly_mean[0], 'xr', label="transformed approx")
        #plt.plot(poly_mean_orig[1], poly_mean_orig[0], 'xg', label="approx")
        plt.plot(eval_grid_points[max_lic], eval_grid_points[max_lix], 'oc', label="max")
        plt.clabel(CS, inline=1, fontsize=10)
        plt.xlim([-1,1])
        plt.ylim([-1,1])
        plt.legend()
        plt.savefig(file_path + "contour_errorTrueToApprox_Measure{}".format(len(bayes_obj.measurements)) +
                    str(hash(bayes_obj.file_name)) + ".png")