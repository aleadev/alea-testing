# !usr/bin/env python
# -*- coding: latin-1 -*-
from __future__ import division
__author__ = "marschall"

import tt
import numpy as np
import time
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

from Bayes_util.lib.bayes_lib import create_rank_one_tensor_from_vector, evaluate, apply_bayes_inner_product_new
from Bayes_util.lib.bayes_lib_simple import apply_inner_product
from Bayes_util.lib.interpolation_util import get_cheb_points



observation_nodes = 30                              # set the number of observations
cheb_dim = [5]                                     # define number of nodes in the stochastic grid. method expects list
cheb_points = get_cheb_points(cheb_dim)[0]          # get the Chebyshev nodes and only the first entry of the retval
#                                                   # first components contains the observation info
dimensions = [observation_nodes]+[cheb_dim[0] for _ in range(5)]
ranks = [1, 25, 3, 4, 3, 2, 1]                      # we need len(dimensions)+1 many ranks (just something random)

gamma = 0.01                                        # used covariance in the inner product
precision = 1e-10                                   # used precision for tensor rounding
measurements = np.random.rand(observation_nodes)    # create random measurements just to illustrate the procedure

ten = tt.rand(dimensions, len(dimensions), ranks)   # create random tensor. Assume observation is already done in comp 1
ten *= tt.vector.norm(ten)**(-1)                    # normalize the tensor to obtain somewhat smooth behaviour
ten_orig = tt.vector.copy(ten)                      # buffer the original tensor to sample later from it
ten = tt.vector.to_list(ten)                        # create list or order 3 tensors to iterate through
ten_list = []                                       # instance the list of observations
for lia in range(observation_nodes):                # create list of tt tensors over all observations
    #                                               # buffer needed for tensor creation
    buffer_list = [np.zeros((1, 1, ten[0].shape[2]))]
    #                                               # set dimension 2 & 3 of first component
    buffer_list[-1][0, :, :] = np.array(ten[0][0, lia, :])
    for lic in range(1, len(dimensions)):           # loop through the remaining component tensors
        buffer_list.append(np.array(ten[lic]))      # add them to the list
        #                                           # add new tensor with n[0]=1 repr. observations
    ten_list.append(tt.vector.from_list(buffer_list))

N = 10                                              # define number of steps in the stochastic grid to use for sampling
x = np.arange(-1, 1+1/N, 1/N)                       # define sampling grid in [-1, 1]. In each dimension equidistant
maxrank = 150                                        # define maximal rank to start rounding at

assert len(measurements) == len(ten_list)           # logical assumption on the measurements and observations
start_old = time.time()                             # start timer (tic)
ten_list_new = []                                   # create a new list to store the measurement difference in
for lia in range(len(ten_list)):                    # loop through the tensor list
                                                    # create a rank one tensor, constant in the stochastic dimensions
    value = create_rank_one_tensor_from_vector(measurements[lia], ten_list[lia].n)
    ten_list_new.append(value - ten_list[lia])      # append the difference of the measurements and the observation
#                                                   # calculate the inner product in the old fashion
ten_old = apply_inner_product([tt.vector.copy(ten_list_new[lia])
                               for lia in range(len(ten_list_new))], gamma, _round=False, _precision=precision,
                              _maxrank=maxrank, _print=False)
end_old = time.time()                               # stop timer (toc)

print "old. needed time: {}".format(end_old - start_old)
print "old. resulting ranks: {}".format(ten_old.r)

start_new = time.time()                             # start timer (tic)
#                                                   # apply the inner product in the new parallel, stable fashion
ten_new = apply_bayes_inner_product_new([tt.vector.copy(ten_list[lia]) for lia in range(len(ten_list))], measurements,
                                        gamma, precision, maxrank, _print=False, n_cpu=1, _round=False)
end_new = time.time()                               # stop timer (toc)

print "new needed time: {}".format(end_new - start_new)
print "new resulting ranks: {}".format(ten_new.r)
#                                                   # evaluate the tensors at some nodes to obtain the difference
values_ten_old = [evaluate(ten_old, cheb_points, _poly="Lagrange",
                           _samples=[0, cheb_points[1], x[lia], cheb_points[2], cheb_points[2], cheb_points[3]])
                  for lia in range(0, len(x))]
values_ten_old_cheb = [evaluate(ten_old, cheb_points, _poly="Lagrange",
                                _samples=[0, cheb_points[1], cheb_points[lia], cheb_points[2], cheb_points[2],
                                          cheb_points[3]]) for lia in range(0, len(cheb_points))]
values_ten_new = [evaluate(ten_new, cheb_points, _poly="Lagrange",
                           _samples=[0, cheb_points[1], x[lia], cheb_points[2], cheb_points[2], cheb_points[3]])
                  for lia in range(0, len(x))]
values_ten_new_cheb = [evaluate(ten_new, cheb_points, _poly="Lagrange",
                                _samples=[0, cheb_points[1], cheb_points[lia], cheb_points[2], cheb_points[2],
                                          cheb_points[3]]) for lia in range(0, len(cheb_points))]

values_orig = []                                    # create a list of reference values from the original tensor
for lia in range(len(cheb_points)):                 # loop through the sample nodes
    #                                               # calculate the inner product by hand to obtain the true values
    values_orig.append(gamma**(-1)*np.sum([(measurements[lic]-ten_list[lic][0, 1, lia, 2, 2, 3])**2
                                           for lic in range(len(measurements))]))
#                                                   # look at the cheb nodes of the old inner product (act. not needed)
values_ten = [ten_old[0, 1, lia, 2, 2, 3] for lia in range(ten_old.n[2])]

plt.plot(x, values_ten_old, '-b', label='old w/ Legendre')
plt.plot(x, values_ten_new, '-r', label='new w/ Legendre')
plt.plot(cheb_points, values_ten, 'xg', label='old at cheb Nodes')
plt.plot(cheb_points, values_orig, 'oy', label='sampled true values')
plt.legend()
plt.savefig("tests/InnerProductResult/TestInnerProduct.png")
plt.figure()
plt.semilogy(np.abs(np.array(values_ten_new) - np.array(values_ten_old)))
plt.title("error of the two approximations")
plt.savefig("tests/InnerProductResult/TestInnerProductError.png")
plt.figure()
plt.semilogy([max(1e-16, np.abs(values_ten_new_cheb[lia] - values_ten[lia])) for lia in range(len(values_ten))])
plt.title("error of the new method vs the true nodes")
plt.savefig("tests/InnerProductResult/TestInnerProductErrorNew.png")
plt.figure()
plt.semilogy([max(1e-16, np.abs(values_ten_old_cheb[lia] - values_ten[lia])) for lia in range(len(values_ten))])
plt.title("error of the old method vs the true nodes")
plt.savefig("tests/InnerProductResult/TestInnerProductErrorOld.png")
