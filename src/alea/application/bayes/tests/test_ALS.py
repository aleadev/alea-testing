# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
    test script for testing the ALS implementation of M. Pfeffer in tt_als
"""

# region Imports
from __future__ import division
import tt
# from tt_als.tensorsolver.tt_als import ttALS
from tt_als.tensorsolver.tt_util import tt_round_qr, tt_round

__author__ = 'marschall'
# endregion

dimension = [10, 12, 14, 16, 13]
ranks = [1, 8, 10, 20, 15, 1]

start_rank = 2

A = tt.vector.round(tt.rand(dimension, d=len(dimension), r=10), 0.0,rmax=10000) 
A = A * tt.vector.norm(A)**(-1)
b = tt.vector.round(tt.rand(dimension, d=len(dimension), r=start_rank), 0.0, rmax=10000)
A_approx = tt.vector.from_list(tt_round(tt.vector.to_list(A), 5))
A_approx_qr, err = tt_round_qr(tt.vector.to_list(A), 5)
A_approx_qr = tt.vector.from_list(A_approx_qr)
A_round = tt.vector.round(A, 0, rmax=5)
print("A_approx: {}".format(A_approx))
print("A_approx_qr: {}".format(A_approx_qr))
print("A_round: {}".format(A_round))
# A_mat = tt.matrix.to_list(tt.diag(A))
# A_mat[0] = csr_matrix(([0], ([0], [0])), shape=(1, 1))
# A_mat = smatrix(A_mat)
# A_approx = ttALS(A_mat, A, b)
# # A_approx = ttALS(tt.diag(A), A, b)
# returns an error since the ALS is only implemented for sparse tensor matrices and there is no documentation
# of how to create a sparse tensor matrix from an arbitrary tensor matrix using a somewhat dummy first sparse core...
# Later more...
print(" error_own: {}".format(tt.vector.norm(A-A_approx)))
print(" error_own_qr: {}".format(tt.vector.norm(A-A_approx_qr)))
print(" error_osel: {}".format(tt.vector.norm(A-A_round)))
print(" error_own_osel: {}".format(tt.vector.norm(A_approx_qr-A_round)))
print("norm tensor {}".format(tt.vector.norm(A)))
print("norm tensor1 {}".format(tt.vector.norm(A_approx)))
print("norm tensor2 {}".format(tt.vector.norm(A_approx_qr)))
