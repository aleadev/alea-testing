from __future__ import division
import alea.polyquad.polynomials as polys
import numpy as np
from numpy.testing import assert_almost_equal

alpha = 0.8
rho = 1
theta = 0.5
herms = polys.StochasticHermitePolynomials(0, 1, normalised=True)
# region def sigma


def sigma(_theta, _alpha):
    return np.exp(_theta*_alpha)
# endregion
# region def base change constant


def base_change_constant(mu, k, _sigma1, _sigma2):
    """
    In contrast to the previous implementation we needed to get rid of the sqrt(2) weight in the representation of the
    hermite polynomials, i.e.
    \sum_{i=0}^\infty H_i^\sigma(y) \frac{s^i}{i!} = e^{sy - 0.5 s^2 \sigma^2
    :param mu: index of hermite polynomial to represent
    :param k: summation index of current coupled hermite polynomial
    :param _sigma1: weight of prior hermite base
    :param _sigma2: weight of posterior hermite base
    :return: constant c_{mu, k}^{sigma1, sigma2}
    """
    return (np.sqrt(np.math.factorial(mu))*(_sigma2**2 - _sigma1**2)**k * _sigma2**(mu-2*k)) * \
           (2**k * np.sqrt(np.math.factorial(mu-2*k))*np.math.factorial(k)*_sigma1**mu)**(-1)
# endregion
# region Reducer Functions

# region def hermite polynomial


def herm(_deg, _y):
    return herms.eval(_deg, _y, all_degrees=False)
# endregion
# region Sigmatheta


def sigma1():
    return sigma(theta, alpha)
# endregion
# region Sigmarho


def sigma2():
    return sigma(rho, alpha)
# endregion
# endregion
samples = np.random.rand(10)

# region Check the formula for hermite generating function
s_list = np.linspace(-1, 1, 20)
y_list = np.linspace(-2, 2, 50)
trunc = 20
# for s in s_list:
#     for y in y_list:
#         lhs = sum([herms.eval(i, y/sigma1(), all_degrees=False)*(sigma1()*s)**i/np.sqrt(np.math.factorial(i))
#                    for i in range(trunc+1)])
#         print("lhs={} == {}=rhs".format(lhs, np.exp(s*y - 0.5*sigma1()**2*s**2)))
# endregion

for deg in range(0, 15):
    for sample in samples:
        lhs = herm(deg, sample/sigma1())
        rhs = sum([base_change_constant(deg, i, sigma1(), sigma2()) * herm(deg - 2*i, sample/sigma2())
                   for i in range(int(np.floor(deg/2))+1)])
        # print("lhs = {} == {} = rhs".format(lhs, rhs))
        assert_almost_equal(lhs, rhs)

# region def triple hermite coefficient

import math
def triple_hermite_coef(nu, mu, xi):
    """
    calculates the coefficient of Hermite polynomial expansion when used as triple product
    :param nu: coefficient index
    :param mu: solution index
    :param xi: contraction index
    :return: triple hermite coefficient
    """
    if (nu + mu - xi) % 2 != 0:
        return 0                                    # hermite indices form an odd sum
    if np.abs(nu - mu) > xi:
        return 0                                    # hermite indices do not couple
    if xi > nu + mu:
        return 0                                    # again no coupling
    # if xi > min(nu, mu):                            # new: check for correct deterministic estimator
    #     return 0
    eta = (nu + mu - xi) * 0.5                      # return coefficient as in
    # return (np.math.factorial(eta) * np.math.factorial(nu - eta) * np.math.factorial(mu - eta)) ** (-1)

    return (math.sqrt(np.math.factorial(nu) * np.math.factorial(mu) * np.math.factorial(xi))
            * (np.math.factorial(eta) * np.math.factorial(nu - eta) * np.math.factorial(mu - eta)) ** (-1))
# endregion
sigma = sigma1()
for deg1 in range(10):
    for deg2 in range(10):
        for sample in samples:
            lhs = herm(deg1, sample/sigma) * herm(deg2, sample/sigma)
            rhs = sum([triple_hermite_coef(deg1, deg2, deg1 + deg2 - 2*eta) * herm(deg1 + deg2 - 2*eta, sample/sigma)
                       for eta in range(min(deg1, deg2)+1)])
            assert_almost_equal(lhs, rhs)
