""" Template to parallelize jobs which involve non-pickle-able
    objects (like a FEniCS solver), and thus cannot be handled
    by the multiprocessing library.
    Circumvents this by putting such objects into a global variable
"""

from joblib import Parallel, delayed            # Can be compiled (as user) on the cluster with
                                                # $ python setup.py install --user
                                                # see https://pythonhosted.org/joblib
import gc                                       # Garbage collection
import multiprocessing as mp
import numpy as np
import dolfin as df

_global_objects = None                          # Global variable which holds non-pickle-able objects


class Parallelizer(object):
    def __init__(self):
        self.nCPU = None                        # number of used cpu cores


    def prepare(self, global_objects, nCPU=mp.cpu_count()):
        global _global_objects                  # set the global variable(s) with
        _global_objects = global_objects        # non-pickle-able objects
        self.nCPU = nCPU


    def start(self, np_array, initial_call=False):
        if initial_call:
            _call_parallel(np_array[0, :])          # solve once to prevent FEniCS problems in parallel.
                                                # This may not be necessary if FEniCS isn't used
        # print "XXX1", np_array.shape
        # global _global_objects
        # C = _global_objects
        # print "XXX2", C.FEM['V'].dim()

        print "="*10 + (" starting  %i parallel jobs for %i tasks... " % (self.nCPU, np_array.shape[0])) + "="*10
        output = np.array(Parallel(self.nCPU)(delayed(_call_parallel)(np_array_i) for np_array_i in np_array))
                                                # joblib handles the job starting, isn't really delayed, starts as soon
                                                # as a cpu core is ready
                                                # in this example, we iterate over an array rowwise
                                                # the output array is NOT SORTED! Keep this in mind and sort if necessary!
        return output


def _call_parallel(data):                       # function which is parallelized
    O = _global_objects                         # get the non-pickle-able object
    result = O.compute(data)                    # do your thing...
    gc.collect()                                # garbage collection
    return result                               # we assume a row-vector here, this is the reason for catching
                                                # the output of all worker processes in a numpy array


if __name__ == "__main__":
    class Computer(object):
        def __init__(self):
            self.FEM = self.setup_FEM()

        def setup_FEM(self):
            print "setup FEM"
            N = 10
            mesh = df.UnitSquareMesh(N,N)
            V = df.FunctionSpace(mesh, 'CG', 1)
            u, v = df.TrialFunction(V), df.TestFunction(V)
            a = df.inner(df.nabla_grad(u), df.nabla_grad(v))*df.dx
            FEM = {'mesh': mesh, 'V': V, 'a': a}
            return FEM

        def compute(self, a):
            print "XXX3", a.shape, self.FEM['V'].dim()
            f = df.Function(self.FEM['V'])

            np.linalg.norm(f.vector().get_local())

            f.vector().set_local(a)
            v = df.assemble(f**2*df.dx)
            return v


    print "START main"

    C = Computer()
    dofs = C.FEM['V'].dim()
    D = np.random.rand(4, dofs)
    print "shape", D.shape
    P = Parallelizer()
    P.prepare(C)
    O = P.start(D)
    print "=== output ===\n", O