#!/usr/bin/env python                                                           
import timeit


setup = "import numpy;\
        import scipy.linalg as linalg;\
        import numpy.linalg as nplinalg;\
        import dask.array as da;\
        x = numpy.random.random((1000,1000));\
        z = numpy.dot(x, x.T)"
count = 5

print("Start sampling for QR and SVD error sampling with different packages and averaging over {} runs".format(count))
t = timeit.Timer("linalg.qr(z, mode='economic', pivoting=False, overwrite_a=True)", setup=setup)
# t = timeit.Timer("linalg.cholesky(z)", setup=setup)  # numpy library takes much longer
print("QR Scipy:", t.timeit(count)/count, "sec")

t = timeit.Timer("nplinalg.qr(z, mode='economic')", setup=setup)
# t = timeit.Timer("linalg.cholesky(z)", setup=setup)  # numpy library takes much longer
print("QR numpy:", t.timeit(count)/count, "sec")

t = timeit.Timer("linalg.svd(z, full_matrices=0)", setup=setup)
print("svd:", t.timeit(count)/count, "sec")

t = timeit.Timer("da.linalg.svd(z)", setup=setup)
print("dask svd:", t.timeit(count)/count, "sec")