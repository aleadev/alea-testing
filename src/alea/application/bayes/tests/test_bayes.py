# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  test script for the bayesian procedure using direct sampling of the Karhunen Loeve expansion
"""
# region Imports
from __future__ import division

import matplotlib
matplotlib.use('Agg')
import tt_als
import logging.config
import matplotlib.pyplot as plt
import numpy as np
from Bayes_util.lib.bayes_lib import create_parameter_samples, create_noisy_measurement_data_dofs
from Bayes_util.exponential.implicit_euler import ImplicitEuler
from scipy.integrate import quad
from scipy.interpolate import interp1d

from Bayes_util.asgfem_bayes import ASGFEMBayes
from Bayes_util.asgfem_mcmc_bayes import ASGFEMBayesMCMC
from Bayes_util.exponential.explicit_euler import ExplicitEuler
from Bayes_util.lib.bayes_lib_simple import create_unique_dofs
from Bayes_util.lib.interpolation_util import get_cheb_points

# endregion

METHODS = ["TT-Bayes", "MCMC", "TT-MCMC"]
METHOD = METHODS[1]

# region Setup parameters

#   region Parameters to use in stochastic Galerkin for the backward solution
refinements = 1 			                        # # of refinement steps in sg
srank = 8					                        # start ranks
gpcdegree = 10					                    # maximal # of gpcdegrees
femdegree = 1					                    # fem function degree
max_dofs = 1e5                                      # desired maximum of dofs to reach in refinement process
iterations = 500                                    # used iterations in tt ALS
polysys = "L"                                       # used polynomial system
domain = "square"                                   # used domain, square, L-Shape, ...
# coeffield_type = "EF-square-cos"
# coeffield_type = "EF-square-sin"
# coeffield_type = "monomial"
# coeffield_type = "linear"
# coeffield_type = "constant"                       # used coefficient field type
coeffield_type = "cos"
amp_type = "constant"
#amp_type = "decay-inf"
decay_exp_rate = 2                                  # decay rate for non constant coefficient fields
sgfem_gamma = 0.9                                   # Gamma used in adaptive SG FEM
rvtype = "uniform"
init_mesh_dofs = 1e4                                # initial mesh refinement. -1 : ignore
thetax = 0.5                                        # factor of deterministic refinement (0 : no refinement)
thetay = 0.5                                        # factor of stochastic refinement (0 : no refinement)
#   endregion

# region Solution parameter
n_coefficients = 4                                 # number of coefficients
USE_DOFS = False
n_dofs = 54                                         # number of dofs
n_dofs_list = [2**lia + 2 for lia in range(6)]      # list of increasing dofs

#sample_coordinates = [[0.25, 0.25], [0.25, 0.5], [0.25, 0.75],
#                      [0.5, 0.25],  [0.5, 0.5],  [0.5, 0.75],
#                      [0.75, 0.25], [0.75, 0.5], [0.75, 0.75]]

sample_coordinates = [[1/7, 1/7], [1/7, 2/7], [1/7, 3/7], [1/7, 4/7], [1/7, 5/7], [1/7, 6/7],
                      [2 / 7, 1 / 7], [2 / 7, 2 / 7], [2 / 7, 3 / 7], [2 / 7, 4 / 7], [2 / 7, 5 / 7], [2 / 7, 6 / 7],
                      [3 / 7, 1 / 7], [3 / 7, 2 / 7], [3 / 7, 3 / 7], [3 / 7, 4 / 7], [3 / 7, 5 / 7], [3 / 7, 6 / 7],
                      [4 / 7, 1 / 7], [4 / 7, 2 / 7], [4 / 7, 3 / 7], [4 / 7, 4 / 7], [4 / 7, 5 / 7], [4 / 7, 6 / 7],
                      [5 / 7, 1 / 7], [5 / 7, 2 / 7], [5 / 7, 3 / 7], [5 / 7, 4 / 7], [5 / 7, 5 / 7], [5 / 7, 6 / 7],
                      [6 / 7, 1 / 7], [6 / 7, 2 / 7], [6 / 7, 3 / 7], [6 / 7, 4 / 7], [6 / 7, 5 / 7], [6 / 7, 6 / 7]
                      ]
# endregion

# region Observation parameter
obs_precision = 1e-10
obs_rank = 50
# endregion

# region Inner Product parameter
inp_precision = 1e-5
inp_rank = 30
inp_gamma = 0.0001
# endregion

# region Euler Parameter
euler_precision = 1e-10                             # precision to use in euler rounding
euler_rank = 50                                     # rank to round down in euler method
euler_local_mc_samples = 1000                       # samples in every euler step
euler_global_mc_samples = 1000                      # samples for the final result
euler_repetitions = 0                               # number of repetitions with half step size
euler_steps = 4                                     # start euler steps
euler_global_precision = 1e-5                       # global precision to reach after the euler scheme
euler_use_implicit = True
# endregion

# region Density parameters
n_eval_grid = 50
# endregion

# region setup discretisation
Nx, Ny = 10000, 20
dx, dy = 1/Nx, 1/Ny
eval_grid_points = np.linspace(-1, 1, n_eval_grid)
stochastic_grid = np.array(get_cheb_points([Ny] * n_coefficients)[0])
# endregion

# region MCMC parameter
n_walker = 50
burn_in = 10
n_samples = 500
covariance = 0.0001
# endregion

color_list = ['olive', 'gold', 'aqua', 'black', 'blue', 'brown', 'green', 'red', 'orange']

logging.config.fileConfig('logging.conf')           # load logger configuration
log = logging.getLogger('test_bayes')               # get new logger object

# endregion

log.info("Create exponential method")
if euler_use_implicit:
    euler_method = ImplicitEuler(euler_steps, euler_precision, euler_rank, euler_global_mc_samples, euler_repetitions,
                                 euler_local_mc_samples)
else:
    euler_method = ExplicitEuler(euler_steps, euler_precision, euler_rank, euler_global_mc_samples, euler_repetitions,
                                 euler_local_mc_samples)
if METHOD == "TT-Bayes":
    file_path = 'results/asgfem/new/'
elif METHOD == "MCMC":
    file_path = 'results/mcmc/direct/Constant/36Dofs/'
elif METHOD == "TT-MCMC":
    file_path = 'results/mcmc/bigDecayInf/36Dofs/'
else:
    file_path = 'results/'


bayes_obj = None
bayes_obj_list = []
unique_dofs = []
coeff_field = None
proj_basis = None
true_values = None
prior_densities = None
for step, n_dofs in enumerate(n_dofs_list):

    if USE_DOFS:
        log.info('Create %i unique random dofs', n_dofs)
        dofs = unique_dofs[:n_dofs]
    else:
        dofs = []
    log.info("Create Bayes Object")
    if METHOD == "TT-Bayes":
        bayes_obj = ASGFEMBayes.init_solver(dofs, sample_coordinates, true_values, obs_precision,
                                            obs_rank, inp_precision, inp_rank, inp_gamma, euler_method,
                                            euler_global_precision, eval_grid_points, prior_densities, stochastic_grid,
                                            iterations=iterations, refinements=refinements, max_dofs=max_dofs,
                                            thetax=thetax,
                                            thetay=thetay, srank=srank,  m=n_coefficients+1,
                                            femdegree=femdegree, gpcdegree=gpcdegree, polysys=polysys,
                                            gamma=sgfem_gamma, rvtype=rvtype, domain=domain,
                                            mesh_dofs=init_mesh_dofs, problem=0,
                                            coeffield_type=coeffield_type,
                                            amp_type=amp_type
                                            )
    elif METHOD == "MCMC" or METHOD == "TT-MCMC":
        bayes_obj = ASGFEMBayesMCMC.init_solver(dofs, sample_coordinates, true_values, n_walker, covariance=covariance,
                                                burn_in=burn_in, mc_steps=n_samples, iterations=iterations,
                                                refinements=refinements, max_dofs=max_dofs, thetax=thetax,
                                                thetay=thetay,
                                                srank=srank, femdegree=femdegree, gpcdegree=gpcdegree, polysys=polysys,
                                                gamma=sgfem_gamma, rvtype=rvtype, domain=domain,
                                                mesh_dofs=init_mesh_dofs,
                                                problem=0, coeffield_type=coeffield_type, amp_type=amp_type,
                                                m=n_coefficients + 1)
    else:
        log.info("Unknown method -> abort")
        exit()

    # region Create Bayes solution either from pickling or calculation
    fig_set = 0
    fig = plt.figure()
    bayes_obj.true_values = [0 for _ in range(23)]
    if bayes_obj.import_bayes(file_path):
        log.info('open Bayes file %s', file_path + bayes_obj.file_name + '.dat')
        bayes_obj_list.append(bayes_obj)

        # region results presentation area
        plt.clf()
        if fig_set != 0:
            fig.clf()
        try:
            plt.clf()

            # region Plot Solution Object for true coefficients
            # from tt_als.alea.fem.fenics.fenics_vector import plot
            # bayes_obj.forwardOp.sample_at_coeff(bayes_obj.true_values).plot(title="Approximated solution")
            # bayes_obj.forwardOp.compute_direct_sample_solution(bayes_obj.true_values).plot(title="Direct solution")
            # endregion

            # region Plot ForwardOperator Error
            if not bayes_obj.forwardOp.sampling_error_h1 or not bayes_obj.forwardOp.sampling_error_l2:
                xi_samples = np.array([np.random.rand(bayes_obj.forwardOp.coefficients) for _ in range(100)])
                log.info("Calculate forward operator sampling error")
                l2_error, h1_error = bayes_obj.forwardOp.estimate_sampling_error(list(xi_samples))
                l2_error_mean = 0
                h1_error_mean = 0
                for lia in range(len(xi_samples)):
                    l2_error_mean += l2_error[lia]
                    h1_error_mean += h1_error[lia]
                l2_error_mean /= len(xi_samples)
                h1_error_mean /= len(xi_samples)
                plt.plot(l2_error, '-r', label="L2-Error")
                plt.plot(list(np.arange(0, xi_samples.shape[0])),
                         [l2_error_mean]*xi_samples.shape[0], '-b', label="Mean")
                plt.title("Error sampling of the forward solution with true coefficient values")
                plt.legend(ncol=2, loc=1)
                plt.xlabel("Sampling number")
                plt.ylabel("Error in L2 norm")
                plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "ASGFEM_L2_error.png")
                plt.clf()
                plt.plot(h1_error, '-r', label="H1-Error")
                plt.plot(list(np.arange(0, xi_samples.shape[0])),
                         [l2_error_mean]*xi_samples.shape[0], '-b', label="Mean")
                plt.title("Error sampling of the forward solution with true coefficient values")
                plt.legend(ncol=2, loc=1)
                plt.xlabel("Sampling number")
                plt.ylabel("Error in H1 norm")
                plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "ASGFEM_H1_error.png")
                plt.clf()
            else:
                l2_error_mean = 0
                h1_error_mean = 0
                for lia in range(len(bayes_obj.forwardOp.sampling_error_l2)):
                    l2_error_mean += bayes_obj.forwardOp.sampling_error_l2[lia]
                l2_error_mean /= len(bayes_obj.forwardOp.sampling_error_l2)
                for lia in range(len(bayes_obj.forwardOp.sampling_error_h1)):
                    h1_error_mean += bayes_obj.forwardOp.sampling_error_h1[lia]
                h1_error_mean /= len(bayes_obj.forwardOp.sampling_error_h1)
                plt.plot(bayes_obj.forwardOp.sampling_error_l2, '-r', label="L2-Error")
                plt.plot(np.arange(0, len(bayes_obj.forwardOp.sampling_error_l2)),
                         [l2_error_mean]*len(bayes_obj.forwardOp.sampling_error_l2), '-b', label="Mean")
                plt.title("Error sampling of the forward solution with true coefficient values")
                plt.legend(ncol=2, loc=1)
                plt.xlabel("Sampling number")
                plt.ylabel("Error in L2 norm")
                plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "ASGFEM_L2_error.png")
                plt.clf()
                plt.plot(bayes_obj.forwardOp.sampling_error_h1, '-r', label="H1-Error")
                plt.plot(np.arange(0, len(bayes_obj.forwardOp.sampling_error_h1)),
                         [h1_error_mean]*len(bayes_obj.forwardOp.sampling_error_h1), '-b', label="Mean")
                plt.title("Error sampling of the forward solution with true coefficient values")
                plt.legend(ncol=2, loc=1)
                plt.xlabel("Sampling number")
                plt.ylabel("Error in H1 norm")
                plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "ASGFEM_H1_error.png")
                plt.clf()
            # endregion

            if METHOD == "MCMC" or METHOD == "TT-MCMC":

                # region Plot MCMC results
                for i in range(len(bayes_obj.true_values)):
                    plt.clf()
                    plt.figure()
                    plt.hist(bayes_obj.sampler.flatchain[:, i], 100, color="k", histtype="step")
                    if METHOD == "MCMC":
                        plt.title("MCMC Bayes Result. Coefficient {0:d}, "
                                  "should be {1:f}".format(i + 1, bayes_obj.true_values[i]))
                        plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "MCMC_Result_coef{}.png".format(i))
                        print("figure saved")
                    else:
                        plt.title("MCMC surrogate Bayes Result. Coefficient {0:d}, "
                                  "should be {1:f}".format(i + 1, bayes_obj.true_values[i]))
                        plt.savefig(file_path + str(hash(bayes_obj.file_name)) +
                                    "MCMC_SURROGATE_Result_coef{}.png".format(i))

                # plt.show()
                fig_set = 1
                # plt.waitforbuttonpress()
                #import corner
                #fig = corner.corner(bayes_obj.sampler.chain[:, 5:, :].reshape((-1, len(bayes_obj.true_values))),
                #                    labels=["$xi_1$", "$xi_2$", "$xi_3$"],
                #                    truths=[bayes_obj.true_values[0], bayes_obj.true_values[1],
                #                            bayes_obj.true_values[2], bayes_obj.true_values[3]])
               #
                #fig.savefig(file_path + str(hash(bayes_obj.file_name)) + "MCMC_triangle.png")
                #print "corner done"
                approx_truth = []
                for lic in range(len(bayes_obj.true_values)):
                    buff = bayes_obj.sampler.chain[:, 5, :].reshape((-1, len(bayes_obj.true_values)))[lic][-1]
                    approx_truth.append(buff)
                print approx_truth
                print bayes_obj.true_values
                # noinspection PyProtectedMember
                true_field = bayes_obj.get_true_coefficient_field()._fefunc
                # noinspection PyProtectedMember
                approx_field = bayes_obj.get_parametric_coefficient_field(approx_truth)._fefunc

                from dolfin.fem.norms import errornorm

                field_error_l2 = errornorm(true_field, approx_field, norm_type="l2")
                field_error_h1 = errornorm(true_field, approx_field, norm_type="h1")

                print field_error_l2
                print field_error_h1
                # endregion
            elif METHOD == "TT-Bayes":
                # region Plot euler results

                fig, arr = plt.subplots(2, 2)
                fig_set = 1
                for lia in range(len(bayes_obj.exp_method.local_convergence)):
                    arr[0, 0].plot(np.array([lic for lic in range(1,
                                                                  len(bayes_obj.exp_method.local_convergence[lia])+1)]),
                                   bayes_obj.exp_method.local_convergence[lia])
                    arr[0, 0].set_title('error of euler method with different step sizes')
                    # arr[0, 0].xlabel('# of steps')
                    # arr[0, 0].ylabel('MC error')
                    arr[1, 0].loglog(np.array([lic for lic in
                                               range(1, len(bayes_obj.exp_method.local_convergence[lia])+1)]),
                                     bayes_obj.exp_method.local_convergence[lia])
                    arr[1, 0].set_title('logarithmic error development')
                    # arr[0, 0].xlabel('# of steps')
                    # arr[0, 0].ylabel('log MC error')
                    arr[0, 1].plot(np.array([lic for lic in
                                             range(1, len(bayes_obj.exp_method.local_convergence[-1][100:])+1)]),
                                   bayes_obj.exp_method.local_convergence[-1][100:])
                    arr[0, 1].set_title('last steps of error size with smallest step size')
                    # arr[0, 0].xlabel('# of steps')
                    # arr[0, 0].ylabel('MC error')
                arr[1, 1].plot(bayes_obj.exp_method.step_size_list)
                arr[1, 1].set_title('step sizes used in Euler method')
                # arr[0, 0].ylabel('step size')
                plt.draw()
                plt.savefig(file_path + "M{0}_g{1}_Dof{2}".format(len(bayes_obj.true_values), bayes_obj.inp_gamma,
                                                                  len(bayes_obj.measurements)) +
                            str(hash(bayes_obj.file_name)) + "_euler.pdf", dpi=None, facecolor='w', edgecolor='w',
                            orientation='portrait', papertype=None, format='pdf',
                            transparent=False, bbox_inches='tight', pad_inches=0.1,
                            frameon=None)
                # plt.show()
                # endregion

                # region Plot densities to file
                plt.clf()
                poly_mean = []
                poly_var = []
                poly_max_arg = []
                for i, yi in enumerate(bayes_obj.eval_densities):
                    plt.plot(bayes_obj.eval_grid_points, yi/bayes_obj.mean/2,
                             label="y_{0} should {1}".format(i, bayes_obj.true_values[i]),
                             color=color_list[i % len(color_list)])
                    plt.xlim([-1, 1])
                    plt.vlines(bayes_obj.true_values[i], 0, max(yi/bayes_obj.mean/2),
                               color=color_list[i % len(color_list)])
                    I = interp1d(bayes_obj.eval_grid_points, yi, kind='cubic')
                    poly_mean.append(quad(lambda _x: _x*I(_x)/bayes_obj.mean*0.5, -1,
                                          bayes_obj.eval_grid_points[-1])[0])
                    poly_var.append(quad(lambda _x2: (_x2 - poly_mean[-1])**2 * I(_x2) / bayes_obj.mean * 0.5, -1,
                                         bayes_obj.eval_grid_points[-1])[0])

                    poly_max_arg.append(bayes_obj.eval_grid_points[np.argmax(yi)])
                plt.legend(ncol=len(bayes_obj.eval_densities), loc=3)
                plt.draw()
                plt.savefig(file_path + "M{0}_g{1}_Dof{2}".format(len(bayes_obj.true_values), bayes_obj.inp_gamma,
                                                                  len(bayes_obj.measurements)) +
                            str(hash(bayes_obj.file_name)) + "_dens.pdf", dpi=None, facecolor='w', edgecolor='w',
                            orientation='portrait', papertype=None, format='pdf',
                            transparent=False, bbox_inches='tight', pad_inches=0.1,
                            frameon=None)
                # plt.show()
                # endregion
                # region Plot density means
                plt.clf()
                x_dim = np.linspace(1, len(poly_mean), num=len(poly_mean), endpoint=True)
                plt.semilogy(x_dim, np.abs(np.array(poly_mean) - np.array(bayes_obj.true_values)), 'or',
                             label='mean')
                plt.semilogy(x_dim, np.abs(np.array(poly_max_arg) - np.array(bayes_obj.true_values)), 'ob',
                             label='argmax')
                plt.semilogy(x_dim, [np.linalg.norm(np.array(poly_mean) - np.array(bayes_obj.true_values))] *
                             len(x_dim),
                             label='l2 norm mean')
                plt.semilogy(x_dim, [np.linalg.norm(np.array(poly_max_arg) - np.array(bayes_obj.true_values))] *
                             len(x_dim),
                             label='l2 norm argmax')
                plt.legend(ncol=2, loc=1)
                plt.xlim([0.9, x_dim[-1] + 0.1])
                plt.title('error of estimated parameters to correct values')
                plt.xlabel('Parameter')
                plt.ylabel('Error')
                plt.draw()
                plt.savefig(file_path + "M{0}_g{1}_Dof{2}".format(len(bayes_obj.true_values), bayes_obj.inp_gamma,
                                                                  len(bayes_obj.measurements)) +
                            str(hash(bayes_obj.file_name)) + "_error.pdf", dpi=None, facecolor='w', edgecolor='w',
                            orientation='portrait', papertype=None, format='pdf',
                            transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None)
                # plt.show()

                # endregion
                # region Plot density variances
                plt.clf()
                x_dim = np.linspace(1, len(poly_var), num=len(poly_var), endpoint=True)
                plt.plot(x_dim, poly_var, 'or')
                # plt.legend(ncol=1, loc=1)
                plt.xlim([0.9, x_dim[-1] + 0.1])
                plt.title('variance of estimations')
                plt.xlabel('Parameter')
                plt.ylabel('Variance')
                plt.draw()
                plt.savefig(file_path + "M{0}_g{1}_Dof{2}".format(len(bayes_obj.true_values), bayes_obj.inp_gamma,
                                                                  len(bayes_obj.measurements)) +
                            str(hash(bayes_obj.file_name)) + "_var.pdf", dpi=None, facecolor='w', edgecolor='w',
                            orientation='portrait', papertype=None, format='pdf',
                            transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None)
                # plt.show()

                # endregion
                # region Plot Coefficient Field
                plt.clf()

                if coeff_field is not None:
                    bayes_obj.forwardOp.coefficient_field = coeff_field
                if proj_basis is not None:
                    bayes_obj.forwardOp.proj_basis = proj_basis
                from tt_als.alea.fem.fenics.fenics_vector import plot

                # noinspection PyProtectedMember
                true_field = bayes_obj.get_true_coefficient_field()._fefunc
                if coeff_field is None:
                    coeff_field = bayes_obj.forwardOp.get_coefficient_field
                if proj_basis is None:
                    proj_basis = bayes_obj.forwardOp.get_proj_basis
                # noinspection PyProtectedMember
                approx_field = bayes_obj.get_parametric_coefficient_field(poly_mean)._fefunc

                from dolfin.fem.norms import errornorm

                field_error_l2 = errornorm(true_field, approx_field, norm_type="l2")
                field_error_h1 = errornorm(true_field, approx_field, norm_type="h1")
                plt.plot(np.arange(0, 1, 0.5), [field_error_l2] * 2, '-r', label="L2 Coefficient field error")
                plt.plot(np.arange(0, 1, 0.5), [field_error_h1] * 2, '-b', label="H1 Coefficient field error")
                plt.legend(ncol=2, loc=1)
                plt.title("Error coefficient field")
                plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "ERROR_coeff_field.png")
            # endregion

        except Exception as ex:
            print "error while solution analysis " + ex.message
        # endregion

    else:
        log.info('start Bayesian procedure')
        # region  setup truth, noise and prior
        log.info("Create %i true parameters", bayes_obj.forwardOp.coefficients)
        true_values = create_parameter_samples(bayes_obj.forwardOp.coefficients)
        if USE_DOFS:
            unique_dofs = np.array(create_unique_dofs(n_dofs_list[-1], init_mesh_dofs))
        # endregion

        # region Setup Gaussian Prior
        Gauss_prior = False
        if Gauss_prior:
            raise NotImplemented
            # pi_y =
            # [lambda x: 1/(np.sqrt(2*np.pi)*sigma_i)*np.exp(-0.5*((x-mu_i)/sigma_i)**2) for mu_i, sigma_i in
            # zip(mu, sigma)]

        else:
            prior_densities = [lambda x: 0.5 * np.ones_like(x)] * bayes_obj.forwardOp.coefficients
        # endregion
        if USE_DOFS:
            measurements = create_noisy_measurement_data_dofs(bayes_obj.solution, true_values, dofs, inp_gamma)
        else:
            measurements = np.array([bayes_obj.forwardOp.compute_direct_sample_solution(true_values)(sample_coordinate)
                                     for sample_coordinate in sample_coordinates])
        # measurements = create_noisy_measurement_data_dofs(bayes_obj.solution, true_values, dofs, inp_gamma)
        bayes_obj.measurements = measurements
        bayes_obj.true_values = true_values
        bayes_obj.prior_densities = prior_densities
        if METHOD == "TT-Bayes":
            bayes_obj.calculate_densities()
        elif METHOD == "MCMC":
            bayes_obj.calculate_samples_direct()
        elif METHOD == "TT-MCMC":
            bayes_obj.calculate_samples()

        log.info('end Bayesian procedure')

        log.info('export Bayes file %s', file_path + bayes_obj.file_name + '.dat')
        if not bayes_obj.export_bayes(file_path):
            log.error("can not save bayes object")
        bayes_obj_list.append(bayes_obj)

    if not USE_DOFS:
        break
    # endregion

if USE_DOFS:
    assert len(bayes_obj_list) == len(n_dofs_list)      # plot only if enough data is found
