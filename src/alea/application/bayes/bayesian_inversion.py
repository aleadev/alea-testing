# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  bayesian procedure steps
"""

# region Imports
from __future__ import division

from Bayes_util.lib.bayes_lib import *              # import library containing the main functions
#                                                   # import marginal density integration
from Bayes_util.lib.bayes_lib_simple import integrate_ydims_tt_sgfem

import logging
import logging.config
from Bayes_util.lib.bayes_lib import exp_euler, imp_euler

from Bayes_util.lib.interpolation_util import evaluate_lagrange

# endregion

# region Euler procedure


def bayesian_procedure(solution, dof_list, true_value_data_list, pi_y, cheb_nodes=10, gamma=0.9, monte_carlo_steps=1000,
                       euler_check_number=5, use_imp_euler=False, euler_steps=50, euler_precision=1e-5,
                       euler_max_rank=50, density_sample_points=50, global_euler_precision=1e-5, _print=False):
    """
    goes through the bayesian procedure using a tt tensor as solution operator
    @param solution:
    @param dof_list:
    @param true_value_data_list:
    @param pi_y:
    @param cheb_nodes:
    @param gamma:
    @param monte_carlo_steps:
    @param euler_check_number:
    @param use_imp_euler:
    @param euler_steps:
    @param euler_precision:
    @param euler_max_rank:
    @param density_sample_points:
    @param global_euler_precision:
    @param _print:
    @return:
    """

    logging.config.fileConfig('logging.conf')       # load logger configuration
    log = logging.getLogger('test_bayes')           # get new logger object

    #   The following has to be done, since in the sgfem one manipulated the tensor itself. Using the conversion to and
    #   from a list, the internal structure needed for rounding is created again
    solution = tt.tensor.to_list(solution)
    solution = tt.tensor.from_list(solution)

# ##### start bayesian inversion #####

    if _print:
        print("Sample in stochastic space")
    log.info('  Sample in stochastic space')
#                                                   Get cheb. points on [-1,1]
    samples = [np.cos(2*lia-1/(2*cheb_nodes)*np.pi) for lia in range(0, cheb_nodes)]
#                                                   sample solution tensor at Cheb nodes
    solution = sample_in_stoch_space(solution, samples, _samples=list(samples))

    if _print:
        print("Calculate misfit function")
    log.info("  Calculate misfit function")

    solution = calculate_inner_product(solution, gamma, dof_list, true_value_data_list, _round=True,
                                       _round_precision=euler_precision, _round_rank=euler_max_rank,
                                       _print=_print)
    solution_prev = solution
    monte_carlo_mean = [1e16]                       # prepare mean list of MC calculation
    euler_step_size = [1./euler_steps]              # prepare list of step sizes for euler method
    euler_counter = 0                               # prepare counter for MC repetition
    step_size = []                                  # prepare step size list
    tau = []                                        # prepare truncation error list
    conv = []                                       # convergence list
    while global_euler_precision < monte_carlo_mean[-1] and euler_counter < euler_check_number:

        if euler_counter > 0:
            euler_steps += euler_steps
            euler_step_size.append(1./euler_steps)
            monte_carlo_mean.append(1e16)
            log.debug("    euler repetition # {0} for MC Mean {1} > {2} with {3} steps".format(euler_counter,
                                                                                               monte_carlo_mean,
                                                                                               global_euler_precision,
                                                                                               euler_steps))
        else:
            log.info("    initially calculate exponential")
        if use_imp_euler:
            #                                           # calculate pointwise exponential using implicit euler
            solution, convergence = imp_euler(solution_prev, euler_steps, euler_precision, euler_max_rank,
                                              _print=_print)
        else:
            #                                           # calculate pointwise exponential using explicit euler
            solution, convergence = exp_euler(solution_prev, euler_steps, euler_precision, euler_max_rank,
                                              _print=_print)
        # solution, step_size, tau = adaptive_exp_euler(solution_prev, euler_precision, euler_max_rank, _print=_print)
            #                                           # create new index samples for MC
        y = [
             [np.random.random_integers(0, high=solution.n[lic]-1) for lic in range(len(solution.n))]
             for _ in range(monte_carlo_steps)
            ]
        monte_carlo_mean[-1] = 0
        for lia, y_k in enumerate(y):
            monte_carlo_mean[-1] += np.abs(solution[y_k] - np.exp(solution_prev[y_k]))
        monte_carlo_mean[-1] /= monte_carlo_steps
        conv.append(convergence)
        if _print:
            print("    MC mean = {0} should be at least {1}".format(monte_carlo_mean, global_euler_precision))
        log.debug("    MC mean = {0} should be at least {1}".format(monte_carlo_mean, global_euler_precision))
        euler_counter += 1

    if _print:
        print("Calculate marginal densities")
    log.info("  Calculate marginal densities")
    y_int, mean = integrate_ydims_tt_sgfem(solution, pi_y, 1/cheb_nodes, samples)

    if _print:
        print("Calculate polynomial interpolation of density")
    log.info("  Calculate polynomial interpolation of density")
    y_int_poly = [np.sum(np.array([evaluate_lagrange(np.linspace(-1, 1, density_sample_points), samples, j)*y_int[j, k]
                                   for j in range(len(samples))]), axis=0) for k in range(len(solution.n)-1)]

    return solution, y_int_poly, mean, samples, euler_step_size, monte_carlo_mean, step_size, tau, conv
