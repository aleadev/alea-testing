#!/bin/bash
# python paper_conv_mean.py -re 1000 -md 100000 -sr 2 -gd 1 -nc 2 -at decay-inf -de 2 -fp results/bayes/ -imd 10 -pp results/ -sol -fm 1 -zmc 200 -cpu 50 -ir 20 -or 20 -er 21 -ny 8 #&
# python paper_conv_mean.py -re 1000 -md 150000 -sr 2 -gd 1 -nc 2 -at decay-inf -de 2 -fp results/lognormal_sgfem/ -imd 10 -pp ../paper-tt-bayesian-inversion/results/lognormal_sgfem/ -sol -fm 1 -zmc 200 -cpu 50 -ir 20 -or 20 -er 21 -ny 8 -log #&


# test for posterior density sampling with 2 coef and increasing number of measurement points
#python paper_conv_mean.py -re 1 -md 1000 -ig 0.1 -sr 5 -gd 4 -ns 2 -nc 2 -at decay-inf -de 2 -fp results/bayes/ -imd 10 -pp results/ -postDen -fm 1 -zmc 200 -cpu 50 -ir 20 -or 20 -er 21 -ny 9 #&
#python paper_conv_mean.py -re 1 -md 1000 -ig 0.1 -sr 5 -gd 4 -ns 9 -nc 2 -at decay-inf -de 2 -fp results/bayes/ -imd 10 -pp results/ -postDen -fm 1 -zmc 200 -cpu 50 -ir 20 -or 20 -er 21 -ny 9 #&

#python paper_conv_mean.py -re 1 -md 1000 -ig 0.01 -sr 5 -gd 4 -ns 2 -nc 2 -at decay-inf -de 2 -fp results/bayes/ -imd 10 -pp results/ -postDen -fm 1 -zmc 200 -cpu 50 -ir 20 -or 20 -er 21 -ny 9 #&
#python paper_conv_mean.py -re 1 -md 1000 -ig 0.01 -sr 5 -gd 4 -ns 9 -nc 2 -at decay-inf -de 2 -fp results/bayes/ -imd 10 -pp results/ -postDen -fm 1 -zmc 200 -cpu 50 -ir 20 -or 20 -er 21 -ny 9 #&

#python paper_conv_mean.py -re 1 -md 1000 -ig 0.001 -sr 5 -gd 4 -ns 2 -nc 2 -at decay-inf -de 2 -fp results/bayes/ -imd 10 -pp results/ -postDen -fm 1 -zmc 200 -cpu 50 -ir 20 -or 20 -er 21 -ny 9 #&
#python paper_conv_mean.py -re 1 -md 1000 -ig 0.001 -sr 5 -gd 4 -ns 9 -nc 2 -at decay-inf -de 2 -fp results/bayes/ -imd 10 -pp results/ -postDen -fm 1 -zmc 200 -cpu 50 -ir 20 -or 20 -er 21 -ny 9 #&

#python paper_conv_mean.py -re 1 -md 1000 -ig 0.0001 -sr 5 -gd 4 -ns 2 -nc 2 -at decay-inf -de 2 -fp results/bayes/ -imd 10 -pp results/ -postDen -fm 1 -zmc 200 -cpu 50 -ir 20 -or 20 -er 21 -ny 9 #&

#python paper_conv_mean.py -re 1 -md 1000 -ig 0.0001 -sr 5 -gd 4 -ns 9 -nc 2 -at decay-inf -de 2 -fp results/bayes/ -imd 10 -pp results/ -postDen -fm 1 -zmc 200 -cpu 50 -ir 20 -or 20 -er 21 -ny 9 #&

# python paper_conv_mean.py -re 1 -md 1000 -ig 0.0001 -sr 5 -gd 4 -ns 9 -nc 6 -at decay-inf -de 2 -fp results/bayes/ -imd 10 -pp results/ -postDen -fm 1 -zmc 200 -cpu 50 -ir 20 -or 20 -er 21 -ny 9 #&
python paper_conv_mean.py -re 1 -md 1000 -ig 0.0001 -sr 5 -gd 4 -ns 9 -nc 6 -at decay-inf -de 2 -fp results/bayes/ -imd 10 -pp results/contour/EUI10R20/ -postDen -fm 1 -zmc 200 -cpu 50 -ir 20 -or 20 -er 20 -ny 9 -eui -es 10 &
python paper_conv_mean.py -re 1 -md 1000 -ig 0.0001 -sr 5 -gd 4 -ns 9 -nc 6 -at decay-inf -de 2 -fp results/bayes/ -imd 10 -pp results/contour/EUI20R30/ -postDen -fm 1 -zmc 200 -cpu 50 -ir 30 -or 30 -er 30 -ny 9 -eui -es 20 &
python paper_conv_mean.py -re 1 -md 1000 -ig 0.0001 -sr 5 -gd 4 -ns 9 -nc 6 -at decay-inf -de 2 -fp results/bayes/ -imd 10 -pp results/contour/EUI40R40/ -postDen -fm 1 -zmc 200 -cpu 50 -ir 40 -or 40 -er 40 -ny 9 -eui -es 40