"""
This class implements the analytic representation of the lognormal coefficient field
@author: M. Marschall
"""
# region imports
from __future__ import division
import numpy as np

from alea.stochastics.random_variable import NormalRV
from alea.application.egsz.affine_field import AffineField

import os
import cPickle as pickle
# endregion

FILE_PATH = os.path.dirname(__file__) + '/tmp/' 


class LognormalField(object):
    # region def init
    def __init__(self,
                 _af                                # type: AffineField
                 ):
        """
        Constructor
        :param _af: Affine field to construct the log-normal field from
        """
        self.affine_field = _af
        assert(isinstance(self.affine_field.coeff_field.rvs(0), NormalRV))
    # endregion

    # region property mean_func
    @property
    def mean_func(self):
        return self.affine_field.coeff_field.mean_func
    # endregion

    # region property rvs
    @property
    def rvs(self):
        return self.affine_field.coeff_field.rvs
    # endregion

    # region property funcs
    @property
    def funcs(self):
        return self.affine_field.coeff_field.funcs
    # endregion

    # region def sample_rvs
    def sample_rvs(self, M):
        return self.affine_field.sample_rvs(M)
    # endregion

    # region def call
    def __call__(self,
                 x,                                 # type: np.ndarray
                 y,                                 # type: list
                 cache=None                         # type: object
                 ):                                 # return: np.array
        """
        calls the affine field and evaluates the exponential of it at given sample points y and physical mesh nodes x
        :param x: physical mesh node
        :param y: sample node in stochastic space
        :param cache: class instance that contains the object func_list
        :return: value of exp(a(x,y))
        """
        return np.exp(self.affine_field(x, y, cache=cache))
    # endregion

    # region def get_filename
    def get_filename(self):
        filename = "LognormalField"
        filename += "/af:" + self.affine_field.get_filename()
        return filename
    # endregion

    # region def get_hash
    def get_hash(self):
        return hash(self.get_filename())
    # endregion
