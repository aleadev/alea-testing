"""
This class implements the analytic representation of an generalized affine coefficient field of the form
$$
  a(x,y) = a_0(x) + \sum_{m=1}^M a_m(x) f(y_m)
$$
for some functions (a_m)_m and (f_m)_m defined as (fenics) expressions depending on the variable m, represented as list
@author: M. Marschall
"""
# region imports
from __future__ import division
import numpy as np

from alea.stochastics.random_variable import NormalRV, UniformRV
from alea.application.egsz.affine_field import AffineField
from alea.linalg.tensor.extended_tt import BasisType
from alea.linalg.tensor.extended_fem_tt import ExtendedFEMTT
from dolfin import Expression, FunctionSpace, interpolate

import os
# endregion

FILE_PATH = os.path.dirname(__file__) + '/tmp/'


def get_extendedtt_from_affine_field(af,            # AffineField
                                     fs,            # FunctionSpace
                                     M              # int
                                     ):             # -> ExtendedFEMTT
    """
    creates a Legendre representation of the affine sum expansion
    a_0(x) + sum_k^M a_k(x)y_k
    projected onto the given function space.
    The Legendre representation is of the form
    a(x, y) = \sum_{k_0, k_M=0}^{[M]} A_0[x, k_0] \prod_{m=1}^M \sum_{\mu_m=0}^1 A_m[k_{m-1},\mu_m, k_m] P_{\mu_m}(y_m)
    where
    A_0[x, k_0] = a_k(x) projected onto fs
    and
    A_m[:, 0, :] = diag((1, ..., 1)) - e_{m, m}
    A_m[:, 0, :] = e_{m ,m}
    with e_{i, j} is the matrix of all zeros, except for the (i, j) entry, which is equal to 1
    :param af: AffineField
    :param fs: FunctionSpace
    :param M: expansion Length parameter
    :return: ExtendedFEMTT
    """
    assert isinstance(af, AffineField)
    assert isinstance(fs, FunctionSpace)
    first_comp = np.zeros((1, fs.dim(), M+1))
    comp_list = []
    basis_list = [BasisType.points] + [BasisType.NormalisedLegendre] * M
    for lia in range(M+2):                          # loop through all components of the desired tensor
        if lia == M + 1:
            comp_list[-1] = np.einsum('ijk, k-> ij', comp_list[-1], np.ones(M+1)).reshape((M+1, 2, 1))
            continue
        amp_func = interpolate(af[lia][0][0], fs)   # interpolate current coef function onto fs
        #                                           # add the function vector into the first component
        first_comp[0, :, lia] = amp_func.vector()[:]
        if lia == 0:                                # the first component is only the mean value
            continue
        # TODO: better sparse
        curr_comp = np.zeros((M+1, 2, M+1))         # other components are constructed as described above
        curr_comp[:, 0, :] = np.eye(M+1, M+1)
        curr_comp[lia, 0, lia] = 0
        curr_comp[lia, 1, lia] = np.sqrt(1/3)       # due to normalisation of the polynomials
        comp_list.append(curr_comp)
    comp_list.insert(0, first_comp)
    #                                               # return an extended finite element tensor
    retval = ExtendedFEMTT(comp_list, basis_list, fs)
    retval.canonicalize_left()
    return retval


class GeneralizedAffineField(object):
    # region def init
    def __init__(self,
                 a,                                 # type: list
                 f,                                 # type: list
                 rv_type="uniform",                 # type: str
                 mu=0,                              # type: float
                 sigma=1,                           # type: float
                 ):
        """
        Constructor
        :param a: physical basis functions
        :param f: stochastic basis functions
        """
        for _a in a:
            assert(isinstance(_a, Expression))
        for _f in f:
            assert(isinstance(_f, Expression))
        assert(len(a) == len(f) + 1)
        if rv_type == "uniform":
            self._rvs = UniformRV
        elif rv_type == "normal":
            self._rvs = NormalRV(mu=mu, sigma=sigma)
        else:
            raise ValueError("unknown  rv type: {}".format(rv_type))
        self.a = a
        self.f = f

    # endregion

    # region property mean_func
    @property
    def mean_func(self):
        return self.a[0]
    # endregion

    # region property rvs
    @property
    def rvs(self):
        return self._rvs
    # endregion

    # region property deterministic funcs
    @property
    def det_funcs(self):
        return self.a
    # endregion

    # region property stochastic funcs
    @property
    def sto_funcs(self):
        return self.f
    # endregion

    # region def sample_rvs
    def sample_rvs(self, M):
        return [self.rvs(m).sample(1)[0] for m in range(M)]
    # endregion

    # region def call
    def __call__(self,
                 x,                                 # type: np.ndarray
                 y                                  # type: list
                 ):                                 # return: np.array
        """
        calls the affine field and evaluates it at given sample points y and physical mesh nodes x
        :param x: physical mesh node
        :param y: sample node in stochastic space
        :return: value of exp(a(x,y))
        """
        assert(len(y) == len(self.f))
        return self.a[0](x) + sum([_a(x) * _f(y) for (_a, _f) in zip(self.a[1:], self.f)])
    # endregion

    # region def get_filename
    def get_filename(self):
        raise NotImplemented("filename is not accessible for generic functions")
    # endregion

    # region def get_hash
    def get_hash(self):
        raise NotImplemented("has is not accessible for generic functions")
    # endregion
