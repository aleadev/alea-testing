"""
This class implements the semi discrete representation of the lognormal coefficient field in tensor train format.
Creation of the field is described in M. Pfeffer dissertation or briefly in EMPS1.
Fully-discrete refers to a discrete stochastic space in the sense of an underlying, finite dimensional polynomial basis
in each stochastic dimension.
@author: M. Marschall
"""
# region imports
from __future__ import division
from alea.application.bayes.alea_util.coefficient_field.lognormal_field import LognormalField
from alea.application.egsz.affine_field import AffineField
from alea.application.tt_asgfem.tensorsolver.tt_lognormal import tt_cont_coeff, sample_cont_coeff
from alea.application.bayes.alea_util.configuration.asgfem_config import AsgfemConfig
from alea.application.bayes.alea_util.configuration.bayes_config import BayesConfig

from dolfin import UnitSquareMesh, Mesh, File, FunctionSpace
import os
import numpy as np
import cPickle as pickle
import tt
# endregion

FILE_PATH = os.path.dirname(__file__) + "/tmp/"


class TTLognormalSemidiscreteField(LognormalField):
    # region def init
    def __init__(self,
                 _af,                               # type: AffineField
                 hdegs,                             # type: list
                 ranks,                             # type: list
                 max_norms,                         # type: list
                 mesh_dofs=100000,                  # type: int
                 quad_degree=7,                     # type: int
                 theta=0.5,                         # type: float
                 rho=1,                             # type: float
                 acc=1e-10,                         # type: float
                 scale=1.0,                         # type: float
                 domain='square',                   # type: str
                 no_compute=False
                 ):
        """
        Constructor to create a semi-discrete coefficient field in tensor train format
        :param hdegs: list of hermite degrees
        :param ranks: list of ranks
        :param max_norms: infinity norm of the affine coefficient functions
        :param mesh_dofs: number of mesh dofs to minimal reach while initial refinement
        :param quad_degree: quadrature degree of the rule to integrate the occuring quadruple integrals
        :param theta: scale 1 from GS (acta)
        :param rho: scale 2 from GS (acta)
        :param acc: accuracy to reach while tensor eigenvalue truncation
        """
        LognormalField.__init__(self, _af)
        self.hermite_degree = hdegs
        self.ranks = ranks
        self.max_norms = max_norms
        self.mesh_dofs = mesh_dofs
        self.quad_degree = quad_degree
        self.theta = theta
        self.rho = rho
        self.quad_mesh = UnitSquareMesh(5, 5)       # create some initial mesh of small size
        self.acc = acc
        self.scale = scale
        self.domain = domain
        self.cont_coef_cores = []
        self.computed = False
        if not no_compute:
            if True:
                self._create_cont_coeff()                   # start mesh calculation
                self.computed = True
            else:
                self.computed = True
            # cores = self.get_cont_cores()
            # import tt
            # cores = tt.vector.from_list(cores)
            # print(" coefficient: {}".format(cores))
    # endregion

    # region def private create continuous cores
    def _create_cont_coeff(self):
        if len(self.max_norms) < len(self.hermite_degree):
            self.max_norms = self.affine_field.get_infty_norm(len(self.hermite_degree))
        if self.load():
            self.computed = True
            return
        cont_coeff_cores = tt_cont_coeff(self.affine_field, self.quad_mesh, len(self.hermite_degree),
                                         self.ranks, self.hermite_degree, self.max_norms, theta=self.theta,
                                         rho=self.rho, acc=self.acc, mesh_dofs=self.mesh_dofs,
                                         quad_degree=self.quad_degree, coef_mesh_cache=None, domain=self.domain,
                                         scale=self.scale)
        self.cont_coef_cores = cont_coeff_cores
        self.save()
        self.computed = True
    # endregion

    # region def sample semi-discrete field
    def __call__(self,
                 points,                            # type: np.array
                 sample,                            # type: list
                 fs=None,                           # type: FunctionSpace
                 af_cache=None                      # type: object
                 ):
        """
        sample the continuous coefficient at given physical points for the sample nodes
        :param points: nodes in physical space
        :param sample: samples in stochastic space
        :return: list of evaluations
        """
        return sample_cont_coeff(self.cont_coef_cores, self.affine_field, points, sample, self.ranks,
                                 self.hermite_degree, self.max_norms, theta=self.theta, rho=self.rho, fs=fs,
                                 af_cache=af_cache)
    # endregion

    # region def get continuous coefficient cores
    def get_cont_cores(self):
        if self.computed is True:
            return self.cont_coef_cores
        else:
            raise ValueError("Cont-cores are not computed yet")
    # endregion

    # region def __repr__ / __str__
    def __repr__(self):
        return "TT semi discrete coefficient field: \n tensor size: " \
               "\n{}".format(tt.vector.from_list(self.get_cont_cores()))

    def __str__(self):
        return self.__repr__()
    # endregion

    # region def sample continuous field
    def sample_continuous_field(self,
                                x,                  # type: np.ndarray
                                y,                  # type: list
                                cache=None          # type: object
                                ):                  # return: np.array
        return LognormalField.__call__(self, x, y, cache=cache)
    # endregion

    # region def get_filename
    def get_filename(self):
        filename = "TTLognormalSemiDiscreteField"
        filename += "lognormal_af:{}".format(super(TTLognormalSemidiscreteField, self).get_filename())
        filename += "hermite_degree={}".format(self.hermite_degree)
        # filename += "ranks={}".format(self.ranks)
        filename += "max_norms={}".format(self.max_norms)
        filename += "mesh_dofs={}".format(self.mesh_dofs)
        filename += "quad_degree={}".format(self.quad_degree)
        filename += "theta={}".format(self.theta)
        filename += "rho={}".format(self.rho)
        filename += "acc={}".format(self.acc)
        filename += "scale={}".format(self.scale)
        filename += "domain={}".format(self.domain)
        return filename
    # endregion

    # region load
    def load(self):
        """
        loads a pickled file that contains the continuous tensor cores, as well as the mesh
        :return: boolean
        """
        try:
            f = open(FILE_PATH + str(self.get_hash()) + ".dat", 'rb')
            tmp_dict = pickle.load(f)
            f.close()
            self.cont_coef_cores = tmp_dict['coef_cores']
            self.ranks = [1] + [core.shape[0] for core in self.cont_coef_cores] + [1]
            self.quad_mesh = Mesh(FILE_PATH + str(self.get_hash()) + '-quad_mesh.xml')
        except Exception as ex:
            print("!!!!! {} exception: {} \n    {}".format(__name__, ex.message, ex))
            return False
        return True
    # endregion

    # region def save
    def save(self):
        """
        pickles the cores of the continuous coefficient tensor and the mesh into a file
        :return: boolean
        """
        try:
            File(FILE_PATH + str(self.get_hash()) + "-quad_mesh.xml") << self.quad_mesh
            retval = dict()
            retval['coef_cores'] = self.cont_coef_cores
            f = open(FILE_PATH + str(self.get_hash()) + ".dat", 'wb')
            pickle.dump(retval, f, 2)
            f.close()
            print("coefficient field saved to: {}".format(FILE_PATH + str(self.get_hash()) + ".dat"))
        except Exception as ex:
            print("exception: {}".format(ex.message))
            return False
        return True
    # endregion

    # region update coefficient

    def update_coefficient(self, new_hdeg, new_rank, add_order=4, new_dimension=5, add_dim=2, max_hdeg=1000,
                           same_length=False):
        update_coef = False
        if not len(new_hdeg) < len(self.hermite_degree):
            print(" coefficient hdegs to short")
            print("     previous: {}".format(self.hermite_degree))
            if same_length is True:
                self.hermite_degree += [new_dimension] *(len(new_hdeg) - len(self.hermite_degree))
            else:
                while len(self.hermite_degree) <= len(new_hdeg):
                    self.hermite_degree += [new_dimension] * add_order
            print("     new: {}".format(self.hermite_degree))
            update_coef = True
        for lia in range(len(new_hdeg)):
            if not new_hdeg[lia] * 2 < self.hermite_degree[lia]:
                print("  coefficient at {} to small".format(lia + 1))
                print("     update from {} to {}".format(self.hermite_degree[lia],
                                                         new_hdeg[lia] * 2 + add_dim))
                if new_hdeg[lia] * 2 + add_dim > max_hdeg:
                    print("!!!!!! hermite degree {} at {} exceeds the limit of {}".format(new_hdeg[lia] * 2 + add_dim,
                                                                                          lia, max_hdeg))
                self.hermite_degree[lia] = new_hdeg[lia] * 2 + add_dim
                update_coef = True
        if update_coef is True:
            self.ranks = [1] + [new_rank] * len(self.hermite_degree) + [1]
            self.max_norms = self.affine_field.get_infty_norm(
                len(self.hermite_degree))
            self._create_cont_coeff()
            print("  new coefficient dimension: {}".format(self.hermite_degree))
    # endregion


# region def read coef from config
def read_coef_from_config(
                          config,               # type: AsgfemConfig or BayesConfig
                          path=None             # type: str
                          ):
    """
    trys to read a complete coefficient from a given AsgfemConfig file
    :param config: configuration file containing all necessary information
    :param path: loading path
    :return: TTLognormalSemidiscreteField
    """
    assert(isinstance(config, AsgfemConfig) or isinstance(config, BayesConfig))
    affine_field = AffineField(config.coeffield_type, "decay-algebraic", config.decay_exp_rate,
                               config.sgfem_gamma, config.freq_scale, config.freq_skip, config.scale,
                               config.field_mean)
    # TODO: Allow for list in coefficient field hermite degree. --> check extensively how this affects the approximation
    if isinstance(config.hdegs_coef, list):
        hermite_degree = config.hdegs_coef
        assert(len(hermite_degree) <= config.num_coeff_in_coeff)
    elif config.hdegs_coef is None:
        hermite_degree = [config.max_hdegs_coef]*config.num_coeff_in_coeff
    elif isinstance(config.hdegs_coef, int):
        hermite_degree = [config.hdegs_coef]*config.num_coeff_in_coeff
    else:
        raise ValueError("can not find information about coefficient setting (hdegs)")
    ranks = [1] + [config.max_rank]*config.num_coeff_in_coeff + [1]
    max_norms = affine_field.get_infty_norm(config.num_coeff_in_coeff)

    retval = TTLognormalSemidiscreteField(affine_field, hermite_degree, ranks, max_norms,
                                          mesh_dofs=config.coef_mesh_dofs, quad_degree=config.coef_quad_degree,
                                          theta=config.theta, rho=config.rho, acc=config.coef_acc, scale=config.scale,
                                          domain=config.domain)
    return retval
# endregion
