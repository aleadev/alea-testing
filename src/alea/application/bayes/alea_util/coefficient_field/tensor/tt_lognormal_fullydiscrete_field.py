"""
This class implements the fully discrete representation of the lognormal coefficient field in tensor train format.
Creation of the field is described in M. Pfeffer dissertation or briefly in EMPS1.
Semi-discrete refers to a discrete stochastic space in the sense of an underlying, finite dimensional polynomial basis
in each stochastic dimension and a discrete physical space in the sense of fenics finite element functions
@author: M. Marschall
"""
# region imports
from __future__ import division

import os
from copy import deepcopy
from dolfin import FunctionSpace, Function, File, Mesh, project
import cPickle as pickle

import numpy as np

from alea.application.bayes.alea_util.coefficient_field.tensor.tt_lognormal_semidiscrete_field import \
    TTLognormalSemidiscreteField
from alea.application.tt_asgfem.apps.sgfem_als_util import compute_FE_matrix_coeff
from alea.application.tt_asgfem.tensorsolver.tt_lognormal import extract_mean
from alea.application.tt_asgfem.tensorsolver.tt_lognormal import fully_disc_first_core, sample_lognormal_tt
from alea.application.tt_asgfem.tensorsolver.tt_sgfem_util import tt_dofs

import tt
# endregion

FILE_PATH = os.path.dirname(__file__) + "/tmp/"


class TTLognormalFullyDiscreteField(TTLognormalSemidiscreteField):
    # region def init
    def __init__(self,
                 semi_cont_field,                   # type: TTLognormalSemidiscreteField
                 fs,                                # type: FunctionSpace
                 domain,                            # type: str
                 empty=False                        # type: bool
                 ):
        """
        create a fully discrete coefficient field in TT format using the given continuous cores and the physical
        fenics function space
        :param semi_cont_field: semi discrete coefficient field
        :param fs: function space of physical description
        :param domain: name of the domain to distinguish different meshes
        """
        self.fs = fs
        # print("init fs: {}".format(self.fs))
        self.mesh = self.fs.mesh()
        self.femdegree = self.fs.ufl_element().degree()
        self.domain = domain
        if self.fs.ufl_element().family() == "Lagrange":
            self.family = "CG"
        elif self.fs.ufl_element().family() == "Discontinuous Lagrange":
            self.family = "DG"
        else:
            raise NotImplementedError("Other spaces than CG or DG are not implemented")

        # print("loaded fs: {}".format(FunctionSpace(self.mesh, self.family, self.femdegree)))
        if self.family == "DG" and not self.femdegree == 0:
            raise NotImplementedError("For DG spaces we need to have constant elements -> go over to CG")
        self.dofs = self.fs.dim()

        TTLognormalSemidiscreteField.__init__(self, semi_cont_field.affine_field, semi_cont_field.hermite_degree,
                                              semi_cont_field.ranks, semi_cont_field.max_norms,
                                              semi_cont_field.mesh_dofs, semi_cont_field.quad_degree,
                                              semi_cont_field.theta, semi_cont_field.rho, semi_cont_field.acc,
                                              semi_cont_field.scale, semi_cont_field.domain, no_compute=True)

        self.cont_coef_cores = semi_cont_field.get_cont_cores()
        self.discrete_coef_cores = None
        self.computed = False
        if self.load() is False or True:
            self._create_discrete_coeff()
        else:
            self.computed = True

    # endregion

    # region def private create discrete cores
    def _create_discrete_coeff(self):
        if self.family == "CG":
            mp = np.array(self.fs.tabulate_dof_coordinates())
            #                                       # reshape dof vector according to physical dimension
            mp = mp.reshape((-1, self.mesh.geometry().dim()))
            first_core = fully_disc_first_core(self.cont_coef_cores, self.affine_field, mp, self.ranks,
                                               self.hermite_degree, self.max_norms, theta=self.theta, rho=self.rho,
                                               cg_mesh=self.mesh, femdegree=self.femdegree)
        elif self.family == "DG":
            mp = np.array(self.fs.tabulate_dof_coordinates())
            #                                       # reshape dof vector according to physical dimension
            mp = mp.reshape((-1, self.mesh.geometry().dim()))

            first_core = fully_disc_first_core(self.cont_coef_cores, self.affine_field, mp, self.ranks,
                                               self.hermite_degree, self.max_norms, theta=self.theta, rho=self.rho,
                                               dg_mesh=self.mesh, femdegree=1)
        else:
            raise NotImplemented("other types of meshes then CG and DG are not implemented")
        self.discrete_coef_cores = deepcopy(self.cont_coef_cores)
        self.discrete_coef_cores.insert(0, first_core)
        self.tt_dofs = tt_dofs(tt.vector.from_list(self.discrete_coef_cores))
        self.full_dofs = np.prod(tt.vector.from_list(self.discrete_coef_cores).n)
        self.computed = True
        self.computed = self.save()
    # endregion

    # region def __repr__ / __str__
    def __repr__(self):
        print("TT fully discrete coefficient field: \n tensor size: "
              "\n{}".format(tt.vector.from_list(self.discrete_coef_cores)))

    def __str__(self):
        self.__repr__()
    # endregion

    # region def sample fully-discrete field
    def __call__(self,
                 points,                            # type: list
                 sample,                            # type: list
                 at_mesh_nodes=False                # type: bool
                 ):
        """
        sample the fully discrete tensor train coefficient at given physical points for the sample nodes
        :param points: nodes in physical space
        :param sample: samples in stochastic space
        :return: list of evaluations
        """
        if self.computed is False:
            raise ValueError("Discrete Field is not computed at the moment")
        vec = sample_lognormal_tt(self.discrete_coef_cores, sample, self.max_norms, theta=self.theta, rho=self.rho)
        if at_mesh_nodes is True:
            return vec
        fun = Function(self.fs)
        fun.vector()[:] = vec
        retval = np.array([fun(p) for p in points])
        # print retval.shape
        return retval
    # endregion

    # region def get continuous coefficient cores
    def get_cont_cores(self):
        if self.computed is True:
            return self.cont_coef_cores
        else:
            raise ValueError("Cont-cores are not computed yet")
    # endregion

    # region def sample semi-discrete field
    def sample_semidiscrete_field(self,
                                  x,                # type: np.array
                                  y                 # type: list
                                  ):                # return: np.array
        return TTLognormalSemidiscreteField.__call__(self, x, y)
    # endregion

    # region def get_filename
    def get_filename(self):
        filename = "TTLognormalFullyDiscreteField"
        filename += "SemidiscreteField:{}".format(super(TTLognormalFullyDiscreteField, self).get_filename())
        filename += "family:{}".format(self.family)
        filename += "degree:{}".format(self.femdegree)
        filename += "domain:{}".format(self.domain)
        filename += "dofs:{}".format(self.dofs)
        return filename
    # endregion

    # region def __getitem__
    def __getitem__(self, i):
        """
        enables the use of the [] operator to get access to the cores more quickly
        :param i: index
        :return: core at index
        """
        if self.computed is not True:
            raise ValueError("discrete cores are not computed yet")
        if 0 <= i < len(self.discrete_coef_cores):
            return self.discrete_coef_cores[i]
    # endregion

    # region def __len__
    def __len__(self):
        """
        computes and returns the len of discrete coefficient cores
        :return: number of cores (incl the first deterministic one)
        """
        if self.computed is not True:
            raise ValueError("discrete cores are not computed yet")
        return len(self.discrete_coef_cores)
    # endregion

    # region def load
    def load(self):
        """
            loads a pickled file that contains the discrete tensor cores
            :return: boolean
        """
        try:
            f = open(FILE_PATH + str(self.get_hash()) + ".dat", 'rb')
            tmp_dict = pickle.load(f)
            f.close()
            self.discrete_coef_cores = tmp_dict['coef_cores']
        except Exception as ex:
            # print("!!!!! {} exception: {} \n    {}".format(__name__, ex.message, ex))
            return False
        return True

    # endregion

    # region def save
    def save(self):
        """
        pickles the cores of the discrete coefficient tensor into a file
        :return: boolean
        """
        try:
            retval = dict()
            retval['coef_cores'] = self.discrete_coef_cores
            f = open(FILE_PATH + str(self.get_hash()) + ".dat", 'wb')
            pickle.dump(retval, f, 2)
            f.close()
        except Exception as ex:
            print("{} exception: {} \n {}".format(__name__, ex.message, ex))
            return False
        return True
    # endregion

    # region def get coef cores
    def get_coef_cores(self):
        """
        returns the computed discrete coefficient cores
        :return: list of cores
        """
        if self.computed is False:
            raise ValueError("coef cores are not computed yet")
        return self.discrete_coef_cores
    # endregion

    # region extract mean
    def extract_mean(self):
        """
        computes the mean of the current coefficient field as a function dependent on the space variable
        :return: list containing the mean stiffness matrix
        """
        import tt
        Pvec = extract_mean(tt.vector.from_list(self.discrete_coef_cores))
        P = compute_FE_matrix_coeff(Pvec, self.mesh, exp_a=False, degree=self.femdegree)

        # mean_func = Function(self.fs)
        # mean_func.vector()[:] = Pvec
        # f = File("mean.pvd")
        # f << mean_func

        return P
    # endregion

    # region extract variance
    def extract_variance(self):
        """
        computes the variance of the current coefficient field as a function dependent on the space variable
        :return: list containing the variance stiffness matrix
        """
        import tt
        exp2_field = TTLognormalSemidiscreteField(self.affine_field, self.hermite_degree, self.ranks, self.max_norms,
                                                  mesh_dofs=self.mesh_dofs, quad_degree=self.quad_degree,
                                                  theta=self.theta, rho=self.rho, acc=self.acc, scale=2.0,
                                                  domain=self.domain)
        exp2_field = TTLognormalFullyDiscreteField(exp2_field, self.fs, self.domain)
        Pvec = extract_mean(tt.vector.from_list(exp2_field.discrete_coef_cores))
        sec_mom = compute_FE_matrix_coeff(Pvec, self.mesh, exp_a=False, degree=self.femdegree)

        # sec_mom_fun = Function(self.fs)
        # sec_mom_fun.vector()[:] = Pvec

        Pvec = extract_mean(tt.vector.from_list(self.discrete_coef_cores))
        # mean2_fun = Function(self.fs)
        # mean2_fun.vector()[:] = Pvec**2
        # f = File("var.pvd")
        # resul = project((sec_mom_fun - mean2_fun), self.fs)
        # f << resul
        first_mom2 = compute_FE_matrix_coeff(Pvec**2, self.mesh, exp_a=False, degree=self.femdegree)

        return sec_mom - first_mom2
    # endregion

    # region extract trace
    def extract_trace(self):
        """
        computes the trace of the current coefficient field as a function dependent on the space variable
        :return: list containing the trace of the field plugged into the stiffness matrix
        """

        retval = 1
        Pvec = 0
        for lia in range(len(self.discrete_coef_cores)-1, -1, -1):
            core = self.discrete_coef_cores[lia]
            if lia > 0:
                if lia == len(self.discrete_coef_cores)-1:
                    retval = np.squeeze(np.sum(core, axis=1))
                    continue
                retval = np.dot(np.sum(core, axis=1), retval)
                continue
            Pvec = np.dot(self.discrete_coef_cores[0][0, :, :], retval)
        P = compute_FE_matrix_coeff(Pvec, self.mesh, exp_a=False, degree=self.femdegree)

        # mean_func = Function(self.fs)
        # mean_func.vector()[:] = Pvec
        # f = File("mean.pvd")
        # f << mean_func

        return P
    # endregion
