"""
This class implements the semi discrete representation of the lognormal coefficient field in tensor train format.
Creation of the field is described in M. Pfeffer dissertation or briefly in EMPS1.
Fully-discrete refers to a discrete stochastic space in the sense of an underlying, finite dimensional polynomial basis
in each stochastic dimension.
@author: M. Marschall
"""
# region imports
from __future__ import division
from alea.application.bayes.alea_util.coefficient_field.lognormal_field import LognormalField
from alea.application.egsz.affine_field import AffineField
from alea.application.tt_asgfem.tensorsolver.tt_lognormal import tt_cont_coeff, sample_cont_coeff
from alea.application.bayes.alea_util.configuration.asgfem_config import AsgfemConfig

from dolfin import UnitSquareMesh, Mesh, File
import os
import numpy as np
import cPickle as pickle
from copy import deepcopy
# endregion

FILE_PATH = os.path.dirname(__file__) + "/tmp/"


class TTLognormalSemidiscreteField(LognormalField):
    # region def init
    def __init__(self,
                 _af,                               # type: AffineField
                 hdegs,                             # type: list
                 ranks,                             # type: list
                 max_norms,                         # type: list
                 mesh_dofs=100000,                  # type: int
                 quad_degree=7,                     # type: int
                 theta=0.5,                         # type: float
                 rho=1,                             # type: float
                 acc=1e-10,                         # type: float
                 scale=1.0,                         # type: float
                 domain='square'                    # type: str
                 ):
        """
        Constructor to create a semi-discrete coefficient field in tensor train format
        :param hdegs: list of hermite degrees
        :param ranks: list of ranks
        :param max_norms: infinity norm of the affine coefficient functions
        :param mesh_dofs: number of mesh dofs to minimal reach while initial refinement
        :param quad_degree: quadrature degree of the rule to integrate the occuring quadruple integrals
        :param theta: scale 1 from GS (acta)
        :param rho: scale 2 from GS (acta)
        :param acc: accuracy to reach while tensor eigenvalue truncation
        """
        LognormalField.__init__(self, _af)
        self.hermite_degree = hdegs
        self.ranks = ranks
        self.max_norms = max_norms
        self.mesh_dofs = mesh_dofs
        self.quad_degree = quad_degree
        self.theta = theta
        self.rho = rho
        self.mesh = UnitSquareMesh(5, 5)            # create some initial mesh of small size
        self.acc = acc
        self.scale = scale
        self.domain = domain
        self.cont_coef_cores = []
        self.computed = False
        if self.load() is False:
            self._create_cont_coeff()                   # start mesh calculation
        else:
            self.computed = True

    # endregion

    # region def private create continuous cores
    def _create_cont_coeff(self):
        cont_coeff_cores = tt_cont_coeff(self.affine_field, self.mesh, len(self.hermite_degree), deepcopy(self.ranks),
                                         self.hermite_degree, self.max_norms, theta=self.theta, rho=self.rho,
                                         acc=self.acc, mesh_dofs=self.mesh_dofs, quad_degree=self.quad_degree,
                                         coef_mesh_cache=None, domain=self.domain, scale=self.scale)
        self.cont_coef_cores = cont_coeff_cores
        self.save()
        self.computed = True
    # endregion

    # region def sample semi-discrete field
    def __call__(self,
                 points,                            # type: np.array
                 sample                             # type: list
                 ):
        """
        sample the continuous coefficient at given physical points for the sample nodes
        :param points: nodes in physical space
        :param sample: samples in stochastic space
        :return: list of evaluations
        """
        return sample_cont_coeff(self.cont_coef_cores, self.affine_field, points, sample, self.ranks,
                                 self.hermite_degree, self.max_norms, theta=self.theta, rho=self.rho)
    # endregion

    # region def get continuous coefficient cores
    def get_cont_cores(self):
        if self.computed is True:
            return self.cont_coef_cores
        else:
            raise ValueError("Cont-cores are not computed yet")
    # endregion

    # region def sample continuous field
    def sample_continuous_field(self,
                                x,                  # type: np.ndarray
                                y                   # type: list
                                ):                  # return: np.array
        return LognormalField.__call__(self, x, y)
    # endregion

    # region def get_filename
    def get_filename(self):
        filename = "TTLognormalSemiDiscreteField"
        filename += "lognormal_af:{}".format(super(TTLognormalSemidiscreteField, self).get_filename())
        filename += "hermite_degree={}".format(self.hermite_degree)
        filename += "ranks={}".format(self.ranks)
        filename += "max_norms={}".format(self.max_norms)
        filename += "mesh_dofs={}".format(self.mesh_dofs)
        filename += "quad_degree={}".format(self.quad_degree)
        filename += "theta={}".format(self.theta)
        filename += "rho={}".format(self.rho)
        filename += "acc={}".format(self.acc)
        filename += "scale={}".format(self.scale)
        filename += "domain={}".format(self.domain)
        return filename
    # endregion

    # region load
    def load(self):
        """
        loads a pickled file that contains the continuous tensor cores, as well as the mesh
        :return: boolean
        """
        try:
            f = open(FILE_PATH + str(self.get_hash()) + ".dat", 'rb')
            tmp_dict = pickle.load(f)
            f.close()
            self.cont_coef_cores = tmp_dict['coef_cores']
            self.mesh = Mesh(FILE_PATH + str(self.get_hash()) + '-mesh.xml')
        except Exception as ex:
            print("exception: {}".format(ex.message))
            return False
        return True
    # endregion

    # region def save
    def save(self):
        """
        pickles the cores of the continuous coefficient tensor and the mesh into a file
        :return: boolean
        """
        try:
            File(FILE_PATH + str(self.get_hash()) + "-mesh.xml") << self.mesh
            retval = dict()
            retval['coef_cores'] = self.cont_coef_cores
            f = open(FILE_PATH + str(self.get_hash()) + ".dat", 'wb')
            pickle.dump(retval, f, 2)
            f.close()
        except Exception as ex:
            print("exception: {}".format(ex.message))
            return False
        return True
    # endregion


# region def read coef from config
def read_coef_from_config(
                          config                # type: AsgfemConfig
                          ):
    """
    trys to read a complete coefficient from a given AsgfemConfig file
    :param config: configuration file containing all necessary information
    :return: TTLognormalSemidiscreteField
    """
    assert(isinstance(config, AsgfemConfig))
    affine_field = AffineField(config.coeffield_type, "decay-algebraic", config.decay_exp_rate,
                               config.sgfem_gamma, config.freq_scale, config.freq_skip, config.scale,
                               config.field_mean)
    hermite_degree = [config.hermite_degree]*config.num_coeff_in_coeff
    ranks = [1] + [config.max_rank]*config.num_coeff_in_coeff + [1]
    max_norms = affine_field.get_infty_norm(config.num_coeff_in_coeff)
    retval = TTLognormalSemidiscreteField(affine_field, hermite_degree, ranks, max_norms,
                                          mesh_dofs=config.coef_mesh_dofs, quad_degree=config.coef_quad_degree,
                                          theta=config.theta, rho=config.rho, acc=config.coef_acc, scale=config.scale,
                                          domain=config.domain)
    return retval
# endregion
