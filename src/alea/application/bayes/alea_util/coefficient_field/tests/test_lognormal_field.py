import unittest
import numpy as np
from alea.application.bayes.alea_util.coefficient_field.lognormal_field import LognormalField
from alea.application.egsz.affine_field import AffineField
from alea.utils.parametric_array import ParametricArray
from alea.stochastics.random_variable import NormalRV
from dolfin import Expression
import itertools as _iter


class TestLognormalField(unittest.TestCase):
    def setUp(self):
        coeffield_type = "EF-square-cos-algebraic"      # just to use the new implemented algebraic decay
        amptype = "decay-algebraic"                     # not really needed in that case above
        decayexp = 2                                    # some algebraic decay
        gamma = 0.9                                     # some scaling (not really needed here)
        freq_scale = 1.0                                # scaling of the frequencies only
        freq_skip = 0                                   # skip the first x frequencies
        scale = 1.0                                     # scale the whole coefficient field
        field_mean = 0.0                                # set some mean value of the field
        # TODO: Use random numbers for the above defined ones to ensure more general cases are fine
        #                                               # create affine coefficient field as logarithm of the used field
        af = AffineField(coeffield_type, amptype=amptype, decayexp=decayexp, gamma=gamma, freqscale=freq_scale,
                         freqskip=freq_skip, scale=scale, coef_mean=field_mean)
        self.LognormalField = LognormalField(af)
        pass

    def test_mean_func(self):
        self.assertIsInstance(self.LognormalField.mean_func, Expression,
                              "Mean Function is of type: {}".format(type(self.LognormalField.mean_func)))

    def test_rvs(self):
        self.assertIsInstance(self.LognormalField.rvs(0), NormalRV,
                              "rv is of type: {}".format(type(self.LognormalField.rvs(0))))

    def test_funcs(self):
        self.assertIsInstance(self.LognormalField.funcs, ParametricArray,
                              "funcs is of type: {}".format(type(self.LognormalField.funcs)))

    def test_sample_rvs(self):
        M = 5
        sample = self.LognormalField.sample_rvs(M)
        self.assertIsInstance(sample, list,
                              "sample_rvs({}) is of type: {}".format(M, type(self.LognormalField.funcs)))
        for lia in range(M):

            self.assertIsInstance(sample[lia], float, "  sample {} is of type {}".format(lia, type(sample[lia])))

    def test_at_samples(self):
        for lia in range(10):
            y = list(np.random.randn(10))
            x = np.linspace(0, 1, 25)
            x = np.array([[i, j] for i, j in _iter.product(x, x)])

            affine = np.exp(self.LognormalField.affine_field(x, y))
            log_normal = self.LognormalField(x, y)
            self.assertAlmostEqual(np.linalg.norm(affine-log_normal), 0,
                                   "sample: {} at {} e^Affine: {} == {} :Lognormal".format(lia, y, affine, log_normal))


if __name__ == '__main__':
    unittest.main()
