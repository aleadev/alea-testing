import itertools as _iter
import unittest
from dolfin import UnitSquareMesh, FunctionSpace

import numpy as np
from alea.application.bayes.alea_util.coefficient_field.tt_lognormal_semidiscrete_field import \
    TTLognormalSemidiscreteField

from alea.application.bayes.alea_util.coefficient_field.tensor.tt_lognormal_fullydiscrete_field import \
    TTLognormalFullyDiscreteField
from alea.application.egsz.affine_field import AffineField


class TestTTLognormalField(unittest.TestCase):
    def setUp(self):
        # region setup for affine field
        coeffield_type = "EF-square-cos-algebraic"      # just to use the new implemented algebraic decay
        amptype = "decay-algebraic"                     # not really needed in that case above
        decayexp = 2                                    # some algebraic decay
        gamma = 0.9                                     # some scaling (not really needed here)
        freq_scale = 1.0                                # scaling of the frequencies only
        freq_skip = 0                                   # skip the first x frequencies
        scale = 1.0                                     # scale the whole coefficient field
        field_mean = 0.0                                # set some mean value of the field
        # endregion
        self.M = 5                                      # length of both field representations
        # region setup for tensor setting
        hdegs = [20]*self.M                             # hermite degrees used in every dimension
        ranks = [1] + [80]*self.M + [1]                 # ranks of the tensor cores
        mesh_dofs = 10000                               # mesh nodes of the quadrature grid
        quad_degree = 7                                 # quadrature degree for the coefficient quadrature
        theta = 0.5                                     # scale 1
        rho = 1                                         # scale 2
        acc = 1e-10                                     # accuracy for tensor rounding
        scale_coef = 1.0                                # scaling factor
        domain = 'square'                               # domain of physical space
        # endregion
        # region setup for fem discretisation
        mesh = UnitSquareMesh(100, 100)                 # some deterministic mesh, moderately fine
        femdegree = 3                                   # finite element order
        family = "CG"                                   # continuous Galerkin FEM
        fs = FunctionSpace(mesh, family, femdegree)     # fenics function space
        # endregion

        # TODO: Use random numbers for the above defined ones to ensure more general cases are fine
        #                                               # create affine coefficient field as logarithm of the used field
        af = AffineField(coeffield_type, amptype=amptype, decayexp=decayexp, gamma=gamma, freqscale=freq_scale,
                         freqskip=freq_skip, scale=scale, coef_mean=field_mean)
        max_norms = af.get_infty_norm(self.M)
        LognormalField = TTLognormalSemidiscreteField(af, hdegs, ranks, max_norms, mesh_dofs=mesh_dofs,
                                                      quad_degree=quad_degree, theta=theta, rho=rho, acc=acc,
                                                      scale=scale_coef, domain=domain)
        self.discrete_lognormal_field = TTLognormalFullyDiscreteField(LognormalField, fs)
        pass

    def test_sample_rvs(self):
        sample = self.discrete_lognormal_field.sample_rvs(self.M)
        self.assertIsInstance(sample, list,
                              "sample_rvs({}) is of type: {}".format(self.M, type(self.discrete_lognormal_field.funcs)))
        for lia in range(self.M):

            self.assertIsInstance(sample[lia], float, "  sample {} is of type {}".format(lia, type(sample[lia])))

    def test_at_samples(self):
        print("\n")
        print("#"*20 + " sample true, semi-discrete and fully-discrete coefficient field " + "#"*20)
        err_c_s = 0
        err_c_f = 0
        err_s_f = 0
        n_samples = 100
        for lia in range(n_samples):
            if lia % 10 == 0:
                print("sample {} / {}".format(lia, n_samples))
            y = list(np.random.randn(self.M))
            x = np.linspace(0, 1, 25)
            x = np.array([[i, j] for i, j in _iter.product(x, x)])

            continuous = self.discrete_lognormal_field.sample_continuous_field(x, y)
            semi_discrete = self.discrete_lognormal_field.sample_semidiscrete_field(x, y)
            fully_discrete = self.discrete_lognormal_field(x, y)
            err_c_s += np.linalg.norm(continuous-semi_discrete)
            err_c_f += np.linalg.norm(continuous - fully_discrete)
            err_s_f += np.linalg.norm(semi_discrete - fully_discrete)
        err_c_s *= n_samples*(-1)
        err_c_f *= n_samples * (-1)
        err_s_f *= n_samples * (-1)

        self.assertLessEqual(err_c_s, 1e-10,
                             "L1(\Omega, L2(D)) error of cont to semi-discrete is only: {}".format(err_c_s))
        self.assertLessEqual(err_c_f, 1e-10,
                             "L1(\Omega, L2(D)) error of cont to fully-discrete is only: {}".format(err_c_f))
        self.assertLessEqual(err_s_f, 1e-10,
                             "L1(\Omega, L2(D)) error of semi-discrete to fully-discrete is only: {}".format(err_s_f))
        print("#"*20 + " END sample true and semi discrete coefficient field " + "#"*20)
        print("\n")

if __name__ == '__main__':
    unittest.main()
