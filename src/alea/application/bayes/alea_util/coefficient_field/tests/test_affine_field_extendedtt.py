import unittest
import numpy as np
from alea.application.bayes.alea_util.coefficient_field.affine_field import get_extendedtt_from_affine_field
from alea.application.egsz.affine_field import AffineField
from alea.utils.parametric_array import ParametricArray
from alea.stochastics.random_variable import NormalRV
from dolfin import Expression, UnitSquareMesh, FunctionSpace
import itertools as _iter


class TestLognormalField(unittest.TestCase):
    def setUp(self):
        coeffield_type = "EF-square-cos-algebraic"      # just to use the new implemented algebraic decay
        amptype = "decay-algebraic"                     # not really needed in that case above
        decayexp = 2                                    # some algebraic decay
        gamma = 0.9                                     # some scaling (not really needed here)
        freq_scale = 1.0                                # scaling of the frequencies only
        freq_skip = 0                                   # skip the first x frequencies
        scale = 1.0                                     # scale the whole coefficient field
        field_mean = 2.5                                # set some mean value of the field
        # TODO: Use random numbers for the above defined ones to ensure more general cases are fine
        #                                               # create affine coefficient field as logarithm of the used field
        self.af = AffineField(coeffield_type, amptype=amptype, decayexp=decayexp, gamma=gamma, freqscale=freq_scale,
                              freqskip=freq_skip, scale=scale, coef_mean=field_mean)
        mesh = UnitSquareMesh(20, 20)
        self.M = 15
        self.fs = FunctionSpace(mesh, 'CG', 1)
        self.coef = get_extendedtt_from_affine_field(self.af, self.fs, self.M)
        pass

    def test_at_samples(self):
        dofs = self.fs.tabulate_dof_coordinates().reshape(-1, 2)
        for lia in range(10):
            y = list(np.random.randn(self.M))

            affine = self.af(dofs, y)
            approx = self.coef.sample(y[:self.M])
            self.assertAlmostEqual(np.linalg.norm(affine-approx), 0,
                                   msg="sample: {} at \n {} \n Affine: {} \n== \n{} :Lognormal".format(lia, y, affine,
                                                                                                       approx))


if __name__ == '__main__':
    unittest.main()
