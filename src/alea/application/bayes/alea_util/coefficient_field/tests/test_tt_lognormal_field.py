import itertools as _iter
import unittest

import numpy as np

from alea.application.bayes.alea_util.coefficient_field.tensor.tt_lognormal_semidiscrete_field import \
    TTLognormalSemidiscreteField
from alea.application.egsz.affine_field import AffineField


class TestTTLognormalField(unittest.TestCase):
    def setUp(self):
        # region setup for affine field
        coeffield_type = "EF-square-cos-algebraic"      # just to use the new implemented algebraic decay
        amptype = "decay-algebraic"                     # not really needed in that case above
        decayexp = 2                                    # some algebraic decay
        gamma = 0.9                                     # some scaling (not really needed here)
        freq_scale = 1.0                                # scaling of the frequencies only
        freq_skip = 0                                   # skip the first x frequencies
        scale = 1.0                                     # scale the whole coefficient field
        field_mean = 0.0                                # set some mean value of the field
        # endregion
        self.M = 5                                      # length of both field representations
        # region setup for tensor setting
        hdegs = [20]*self.M                             # hermite degrees used in every dimension
        ranks = [1] + [80]*self.M + [1]                 # ranks of the tensor cores
        max_norms = []                                  # list of norms that has to be set later
        mesh_dofs = 10000                              # mesh nodes of the quadrature grid
        quad_degree = 7                                 # quadrature degree for the coefficient quadrature
        theta = 0.5                                     # scale 1
        rho = 1                                         # scale 2
        acc = 1e-10                                     # accuracy for tensor rounding
        scale_coef = 1.0                                # scaling factor
        domain = 'square'                               # domain of physical space
        # endregion

        # TODO: Use random numbers for the above defined ones to ensure more general cases are fine
        #                                               # create affine coefficient field as logarithm of the used field
        af = AffineField(coeffield_type, amptype=amptype, decayexp=decayexp, gamma=gamma, freqscale=freq_scale,
                         freqskip=freq_skip, scale=scale, coef_mean=field_mean)
        max_norms = af.get_infty_norm(self.M)
        self.LognormalField = TTLognormalSemidiscreteField(af, hdegs, ranks, max_norms, mesh_dofs=mesh_dofs,
                                                           quad_degree=quad_degree, theta=theta, rho=rho, acc=acc,
                                                           scale=scale_coef, domain=domain)

        pass

    def test_sample_rvs(self):
        sample = self.LognormalField.sample_rvs(self.M)
        self.assertIsInstance(sample, list,
                              "sample_rvs({}) is of type: {}".format(self.M, type(self.LognormalField.funcs)))
        for lia in range(self.M):

            self.assertIsInstance(sample[lia], float, "  sample {} is of type {}".format(lia, type(sample[lia])))

    def test_at_samples(self):
        print("\n")
        print("#"*20 + " sample true and semi discrete coefficient field " + "#"*20)
        err = 0
        n_samples = 100
        for lia in range(n_samples):
            if lia % 10 == 0:
                print("sample {} / {}".format(lia, n_samples))
            y = list(np.random.randn(self.M))
            x = np.linspace(0, 1, 25)
            x = np.array([[i, j] for i, j in _iter.product(x, x)])

            continuous = self.LognormalField(x, y)
            semi_discrete = self.LognormalField.sample_continuous_field(x, y)
            err += np.linalg.norm(continuous-semi_discrete)
        err *= n_samples*(-1)
        self.assertLessEqual(err, 1e-10, "L1(\Omega, L2(D)) error of coeff approximation is only: {}".format(err))
        print("#"*20 + " ENDE sample true and semi discrete coefficient field " + "#"*20)
        print("\n")

if __name__ == '__main__':
    unittest.main()
