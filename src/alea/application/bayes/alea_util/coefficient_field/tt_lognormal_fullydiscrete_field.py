"""
This class implements the fully discrete representation of the lognormal coefficient field in tensor train format.
Creation of the field is described in M. Pfeffer dissertation or briefly in EMPS1.
Semi-discrete refers to a discrete stochastic space in the sense of an underlying, finite dimensional polynomial basis
in each stochastic dimension and a discrete physical space in the sense of fenics finite element functions
@author: M. Marschall
"""
# region imports
from __future__ import division
import numpy as np
from alea.application.bayes.alea_util.coefficient_field.tt_lognormal_semidiscrete_field import \
    TTLognormalSemidiscreteField
from alea.application.tt_asgfem.tensorsolver.tt_lognormal import fully_disc_first_core, sample_lognormal_tt
from alea.application.tt_asgfem.tensorsolver.tt_lognormal import extract_mean
from alea.application.tt_asgfem.apps.sgfem_als_util import compute_FE_matrix_coeff

from dolfin import FunctionSpace, Function
import os
from copy import deepcopy
# endregion

FILE_PATH = os.path.dirname(__file__) + "/tmp/"


class TTLognormalFullyDiscreteField(TTLognormalSemidiscreteField):
    # region def init
    def __init__(self,
                 semi_cont_field,                   # type: TTLognormalSemidiscreteField
                 fs                                 # type: FunctionSpace
                 ):
        """
        create a fully discrete coefficient field in TT format using the given continuous cores and the physical
        fenics function space
        :param semi_cont_field: semi discrete coefficient field
        :param fs: function space of physical description
        """
        TTLognormalSemidiscreteField.__init__(self, semi_cont_field.affine_field, semi_cont_field.hermite_degree,
                                              semi_cont_field.ranks, semi_cont_field.max_norms,
                                              semi_cont_field.mesh_dofs, semi_cont_field.quad_degree,
                                              semi_cont_field.theta, semi_cont_field.rho, semi_cont_field.acc,
                                              semi_cont_field.scale, semi_cont_field.domain)
        self.fs = fs
        self.mesh = self.fs.ufl_domain()
        self.femdegree = self.fs.ufl_element().degree()
        if self.fs.ufl_element().family() == "Lagrange":
            self.family = "CG"
        elif self.fs.ufl_element().family() == "Discontinuous Lagrange":
            self.family = "DG"
        else:
            raise NotImplemented("Other spaces than CG or DG are not implemented")
        if self.family == "DG" and not self.femdegree == 0:
            raise NotImplemented("For DG spaces we need to have constant elements -> go over to CG")
        self.dofs = self.fs.dim()
        self.computed = False
        self._create_discrete_coeff()

    # endregion

    # region def private create discrete cores
    def _create_discrete_coeff(self):
        if self.family == "CG":
            mp = np.array(self.fs.tabulate_dof_coordinates())
            #                                       # reshape dof vector according to physical dimension
            mp = mp.reshape((-1, self.mesh.geometry().dim()))

            first_core = fully_disc_first_core(self.cont_coef_cores, self.affine_field, mp, self.ranks,
                                               self.hermite_degree, self.max_norms, theta=self.theta, rho=self.rho,
                                               cg_mesh=self.mesh, femdegree=self.femdegree)
        elif self.family == "DG":
            mp = np.array(self.fs.tabulate_dof_coordinates())
            #                                       # reshape dof vector according to physical dimension
            mp = mp.reshape((-1, self.mesh.geometry().dim()))

            first_core = fully_disc_first_core(self.cont_coef_cores, self.affine_field, mp, self.ranks,
                                               self.hermite_degree, self.max_norms, theta=self.theta, rho=self.rho,
                                               cg_mesh=self.mesh, femdegree=1)
        else:
            raise NotImplemented("other types of meshes then CG and DG are not implemented")
        self.discrete_coef_cores = deepcopy(self.cont_coef_cores)
        self.discrete_coef_cores.insert(0, first_core)
        self.computed = True
    # endregion

    # region def sample fully-discrete field
    def __call__(self,
                 points,                            # type: list
                 sample                             # type: list
                 ):
        """
        sample the fully discrete tensor train coefficient at given physical points for the sample nodes
        :param points: nodes in physical space
        :param sample: samples in stochastic space
        :return: list of evaluations
        """
        if self.computed is False:
            raise ValueError("Discrete Field is not computed at the moment")
        vec = sample_lognormal_tt(self.discrete_coef_cores, sample, self.max_norms, theta=self.theta, rho=self.rho)
        fun = Function(self.fs)
        fun.vector()[:] = vec
        retval = [fun(p) for p in points]
        return retval
    # endregion

    # region def get continuous coefficient cores
    def get_cont_cores(self):
        if self.computed is True:
            return self.cont_coef_cores
        else:
            raise ValueError("Cont-cores are not computed yet")
    # endregion

    # region def sample semi-discrete field
    def sample_semidiscrete_field(self,
                                  x,                # type: np.array
                                  y                 # type: list
                                  ):                # return: np.array
        return TTLognormalSemidiscreteField.__call__(self, x, y)
    # endregion

    # region def get_filename
    def get_filename(self):
        filename = "TTLognormalSemiDiscreteField"
        filename += "lognormal_af:{}".format(super(TTLognormalFullyDiscreteField, self).get_filename())
        filename += "family:{}".format(self.family)
        filename += "degree:{}".format(self.femdegree)
        filename += "dim:{}".format(self.dofs)
        return filename
    # endregion

    # region def __getitem__
    def __getitem__(self, i):
        """
        enables the use of the [] operator to get access to the cores more quickly
        :param i: index
        :return: core at index
        """
        if self.computed is not True:
            raise ValueError("discrete cores are not computed yet")
        if 0 < i < len(self.discrete_coef_cores):
            return self.discrete_coef_cores[i]
    # endregion

    # region def __len__
    def __len__(self):
        """
        computes and returns the len of discrete coefficient cores
        :return: number of cores (incl the first deterministic one)
        """
        if self.computed is not True:
            raise ValueError("discrete cores are not computed yet")
        return len(self.discrete_coef_cores)
    # endregion

    # region extract mean
    def extract_mean(self):
        """
        computes the mean of the current coefficient field as a function dependent on the space variable
        :return: list containing the mean value at the mesh node
        """
        import tt
        Pvec = extract_mean(tt.vector.from_list(self.discrete_coef_cores))
        P = compute_FE_matrix_coeff(Pvec, self.mesh, exp_a=True, degree=self.femdegree)
        return P
    # endregion
