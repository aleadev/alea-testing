"""
Test script to ensure the functionality of the iterative als solver using a sparse first component
@author: Manuel Marschall
"""

import unittest
from alea.application.bayes.alea_util.solver.tt_sparse_als import TTSparseALS
from alea.application.tt_asgfem.tensorsolver.tt_sparse_matrix import smatrix
from dolfin import FunctionSpace, UnitSquareMesh, Function, interpolate, Expression, Constant, UnitIntervalMesh, \
    errornorm, TrialFunction, TestFunction, assemble, dx
import numpy as np
import tt


class TestTTSparseALS(unittest.TestCase):
    def setUp(self):
        mesh = UnitSquareMesh(100, 100)             # create a simple mesh
        #                                           # create a mesh for the stochastic space
        mesh_sto = UnitIntervalMesh(100)
        self.fs = FunctionSpace(mesh, 'CG', 2)      # create a function space using cont. Galerkin and FEM order 2
        self.fs_dg = FunctionSpace(mesh_sto, 'CG', 2)    # create a DG 0 function space for the stochastic space
        self.k = Constant(1.0)                      # set a constant
        #                                           # define physical basis functions depending on the constant
        self.fun = Expression("sin(k*2*pi*x[0])*sin(2*pi*x[1])", degree=5, k=self.k)
        self.g = Expression("(x[0]*x[0] + x[0])*k", degree=2, k=self.k)
        self.M = 5                                 # set number of stochastic dimensions
        M = self.M
        cores = []                                  # define list of cores
        #                                           # create a zero first core
        first_core = np.zeros((1, self.fs.dim(), M))
        for m in range(M-1):                        # run through all the stochastic dimensions
            self.k.assign(m+1)
            fun = interpolate(self.fun, self.fs)    # interpolate the function to the function space
            first_core[0, :, m] = fun.vector()[:]   # set values into the core
            g = interpolate(self.g, self.fs_dg)     # interpolate g to disc. galerkin function space
            # print(g.vector()[:].array())
            next_core = np.zeros((M, len(g.vector()[:]), M))
            for d in range(len(g.vector()[:])):
                eye = np.eye(M)
                eye[m, m] = g.vector()[d]
                next_core[:, d, :] = eye
            cores.append(next_core)
        self.k.assign(M)
        fun = interpolate(self.fun, self.fs)        # interpolate the function to the function space
        first_core[0, :, M-1] = fun.vector()[:]     # set values into the core
        g = interpolate(self.g, self.fs_dg)         # interpolate g to disc. galerkin function space
        next_core = np.zeros((M, len(g.vector()[:]), 1))
        for d in range(len(g.vector()[:])):
            eye = np.ones(M)
            eye[M-1] = g.vector()[d]
            next_core[:, d, 0] = eye
        cores.append(next_core)
        cores.insert(0, first_core)
        # self.true_tt = tt.vector.from_list(cores)
        self.true_tt = cores
        # iden = tt.eye([self.fs.dim()] + [self.fs_dg.dim()]*M)

    def exact_fun(self, y):
        assert(len(y) == self.M)
        retval = Function(self.fs)
        for m in range(self.M):
            self.k.assign(m+1)
            fun = interpolate(self.fun, self.fs)
            retval.vector()[:] += fun.vector()[:]*self.g(y[m])
        return retval.vector()[:].array()

    def eval_tt(self, y, _tt):
        assert(len(y) == self.M)

        retval = 0
        # cores = tt.vector.to_list(_tt)
        cores = _tt
        fun = Function(self.fs_dg)
        for m in range(self.M, 0, -1):
            core = cores[m]
            if m == self.M:
                assert(core.shape[2] == 1)
                retval = core[:, :, 0]
                for k in range(core.shape[0]):
                    fun.vector()[:] = retval[k, :]
                    buf = fun(y[m-1])
                    retval[k, 0] = buf
                retval = retval[:, 0]
                continue
            retval = np.einsum('ijk, k->ij', core, retval)
            for k in range(core.shape[0]):
                fun.vector()[:] = retval[k, :]
                buf = fun(y[m - 1])
                retval[k, 0] = buf
            retval = retval[:, 0]

        return np.dot(cores[0][0, :, :], retval)

    def test_setup(self):
        print("#"*10)
        N = 100

        # dg_dofs = self.fs_dg.tabulate_dof_coordinates()
        # g_dg = [self.g(dof) for dof in dg_dofs]
        # g_ex = interpolate(self.g, self.fs_dg)
        # print("g_dg:{}".format(g_dg))
        # print("g_ex:{}".format(g_ex.vector()[:].array()))
        # print("err: {}".format(np.linalg.norm(g_ex.vector()[:].array() - g_dg)))
        # exit()

        u = TrialFunction(self.fs)
        v = TestFunction(self.fs)
        M = assemble(u*v*dx())
        M = M.array()
        error = 0
        for n in range(N):
            y = np.random.rand(self.M)
            _tt = self.eval_tt(y, self.true_tt)
            ex = self.exact_fun(y)
            # tt_fun = Function(self.fs)
            # ex_fun = Function(self.fs)
            # tt_fun.vector()[:] = _tt
            # ex_fun.vector()[:] = ex
            diff = ex - _tt
            err_ = np.dot(diff.T, np.dot(M, diff))
            norm_ = np.dot(ex.T, np.dot(M, ex))
            err_ *= norm_**(-1)
            error += err_
            # err_ = errornorm(ex_fun, tt_fun)
        error *= N**(-1)
        self.assertLessEqual(error, 1e-10, "error to large".format(error))
        print("error: {}".format(error))
if __name__ == '__main__':
    unittest.main()
