"""
Test script to ensure the functionality of the iterative als solver using a sparse first component
@author: Manuel Marschall
"""

import unittest
import tt
import tt.amen
import numpy as np
import tt


class TestRobert(unittest.TestCase):
    def setUp(self):
        A = []
        core = np.zeros((1,2, 2,1))
        core[0, 0, 0, 0] = 1
        core[0, 1, 1, 0] = 1
        A.append(core)
        core = np.zeros((1,3,3,1))
        core[0, 0, 0, 0] = 1
        core[0, 1, 1, 0] = 1
        core[0, 0, 2, 0] = 1
        core[0, 1, 2, 0] = 1
        core[0, 2, 2, 0] = 2
        core[0, 2, 0, 0] = 1
        core[0, 2, 1, 0] = 1
        A.append(core)
        print("First core: \n {}".format(A[0][0, :, :, 0]))
        print("second core: \n {}".format(A[1][0, :, :, 0]))

        F = []
        core = np.zeros((1, 2, 1))
        core[0, 0, 0] = 1
        core[0, 1, 0] = 1
        F.append(core)
        core = np.zeros((1, 3, 1))
        core[0, 0, 0] = 1
        core[0, 1, 0] = 1
        core[0, 2, 0] = 2
        F.append(core)
        lhs = tt.matrix.from_list(A)
        rhs = tt.vector.from_list(F)

        print lhs
        print rhs

        x_0 = tt.rand([2, 3], 2, r=[1, 1, 1])
        print x_0

        # x = tt.amen.amen_solve(lhs, rhs, x_0, eps=1e-15, kickrank=0)
        # print x

        from alea.application.tt_asgfem.tensorsolver.tt_als import ttALS

        x = ttALS(lhs, rhs, x_0)

    def test_setup(self):
        print("#"*10)

if __name__ == '__main__':
    unittest.main()

