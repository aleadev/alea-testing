"""
Implementing a wrapper for the ALS implementation of xerus wrapping the python interface
@author: Manuel Marschall
"""
# region Imports
from alea.application.bayes.alea_util.solver.les_solver import LesSolver
from alea.application.tt_asgfem.tensorsolver.tt_sparse_matrix import smatrix
from alea.application.tt_asgfem.tensorsolver.tt_param_pde import ttRandomDeterministicRankOne
from alea.application.tt_asgfem.tensorsolver.tt_als import ttALS
from alea.linalg.tensor.extended_fem_tt import ExtendedTT, ExtendedFEMTT
from alea.utils.tictoc import TicToc
import xerus as xe

# endregion


class XerusALS(LesSolver):
    # region init
    def __init__(self,
                 lhs,                               # type: smatrix
                 rhs,                               # type: ExtendedTT
                 ranks,                             # type: list
                 iterations=0,                      # type: int
                 convergence=1e-10                  # type: float
                 ):
        """
        constructor
        :param lhs: TT-sparse operator
        :param rhs: TT-tensor cores as list
        :param ranks: list of tensor ranks to solve with
        :param iterations: number of iteration sweeps. 0 means infinitly many
        :param convergence: tolerance parameter when to stop sweeping
        """
        LesSolver.__init__(self, lhs, rhs)
        self.ranks = ranks
        self.rank = max(self.ranks)
        self.iterations = iterations
        self.convergence = convergence
        self.local_error_list = []
        self.first_comp_convergence_list = []
        self.actual_iterations = -1
        self.xe_op = None
        self.xe_rhs = None
        self.als_solver = None
        self.pd = xe.PerformanceData(True)          # activates information printing
    # endregion

    # region def solve
    def solve(self,
              method="ALS",                         # type: str
              symmetric=True,                       # type: bool
              start_rank_one=False,                 # type: bool
              init_value=None,                      # type: xe.TTTensor
              normalize_init=False                  # type: bool
              ):
        """
        wrapper for the xerus ALS method creating initial value
        :param method: used Alternating method, try ALS, DMRG or ASD
        :param symmetric: flag to use symmetric local solver. speed up but needs symmetric systems
        :param start_rank_one: flag to use a rank one initial tensor
        :param init_value: given initial tensor
        :param normalize_init: flag to normalize the initial tensor
        :return: solution tensor
        """

        self.xe_op = self.lhs.to_xerus_op()         # cast smatrix operator to xerus operator using sparsity
        if method == "ALS":
            method_idx = 1
            method_solver = xe.ALSVariant.lapack_solver
        elif method == "DMRG":
            method_idx = 2
            method_solver = xe.ALSVariant.lapack_solver
        elif method == "ASD":
            method_idx = 1
            method_solver = xe.ALSVariant.ASD_solver
        else:
            raise ValueError("unknown xerus als method: {}\n use ALS, DMRG or ASD".format(method))

        self.als_solver = xe.ALSVariant(method_idx, self.iterations, method_solver, symmetric)
        self.als_solver.convergenceEpsilon = self.convergence
        #                                           # if symmetric = True
        #                                           #   set the following to true to use \| Ax-b\|_F/\|b\|_F
        #                                           #   set the following to false to use 0.5*<x, Ax> - b
        #                                           # else:
        #                                           #   use always <Ax, Ax> - 2*<Ax, b> + <b,b>
        self.als_solver.useResidualForEndCriterion = True

        # als_solver = xe.SteepestDescentVariant(0, config.als_tol, True, xe.SubmanifoldRetractionII)
        self.xe_rhs = self.rhs.to_xerus_tt()

        xe_sol = self.create_start_tensor(start_rank_one, init_value, normalize_init)
        print("start_tensor: {}".format(xe_sol))

        self.als_solver(self.xe_op, xe_sol, self.xe_rhs, self.pd)

        xe_sol.canonicalize_left()
        xe_sol = ExtendedFEMTT.from_xerus_tt(xe_sol, self.rhs.basis, self.rhs.first_comp_fs)
        return xe_sol
    # endregion

    # region def create start tensor
    def create_start_tensor(self,
                            start_rank_one=False,   # type: bool
                            init_value=None,        # type: xe.TTTensor
                            normalize_init=False,   # type: bool
                            ):
        """
        function to create a starting tensor according to given flags. if nothing is specified, return a random tensor
        of the current rank
        :param start_rank_one: create a rank one tensor and return it
        :param init_value: predefined initial value
        :param normalize_init: flag to normalize the initial value
        :return: start tensor
        """
        if start_rank_one is True:
            #                                       # create random deterministic rank one tensor
            start_ten = xe.TTTensor.random(self.rhs.n, [1] * (len(self.rhs.ranks)-2))
            return start_ten

        if init_value is not None:
            if normalize_init is True:
                W = (1 / xe.frob_norm(init_value))*init_value

                # print("rank of W : {}".format(max(tt.vector.from_list(W).r)))
                return W
            else:
                return init_value
        #                                           # create tensor with given rank and size
        # print("random tensor")
        last_rank = 0
        W = xe.TTTensor.random(self.rhs.n, [1] * (len(self.rhs.r) - 2))
        while True:
            self.als_solver(self.xe_op, W, self.xe_rhs, self.pd)
            if max(W.ranks()) >= self.rank-1:
                W += (xe.TTTensor.random(self.xe_rhs.dimensions, [1] * len(self.xe_rhs.ranks())))
                break
            if max(W.ranks()) <= last_rank:
                print("WARNING: Create start tensor: the rank decreased in the last iteration step")
                break
            last_rank = max(W.ranks())
            W += (xe.TTTensor.random(self.xe_rhs.dimensions, [1] * len(self.xe_rhs.ranks())))
            print("start tensor rank increased: \n   {}".format(W))
        return W
    # endregion

