"""
Implementing a wrapper for the ALS algorithm using a sparse first component.
@author: Manuel Marschall
"""
# region Imports
from alea.application.bayes.alea_util.solver.les_solver import LesSolver
from alea.application.tt_asgfem.tensorsolver.tt_sparse_matrix import smatrix
from alea.application.tt_asgfem.tensorsolver.tt_param_pde import ttRandomDeterministicRankOne
from alea.application.tt_asgfem.tensorsolver.tt_als import ttALS
from alea.linalg.tensor.extended_fem_tt import ExtendedTT
from alea.utils.tictoc import TicToc

import numpy as np

import scipy.sparse as sps
# endregion


class TTSparseALS(LesSolver):
    # region init
    def __init__(self,
                 lhs,                               # type: smatrix
                 rhs,                               # type: list or ExtendedTT
                 ranks,                             # type: list
                 als_iterations=1000,               # type: int
                 convergence=1e-10                  # type: float
                 ):
        """
        constructor
        :param lhs: TT-sparse operator
        :param rhs: TT-tensor cores as list
        :param ranks: list of tensor ranks to solve with
        """
        LesSolver.__init__(self, lhs, rhs)
        self.ranks = ranks
        self.rank = max(self.ranks)
        self.als_iterations = als_iterations
        self.convergence = convergence
        self.local_error_list = []
        self.first_comp_convergence_list = []
        self.actual_iterations = -1
    # endregion

    # region def solve
    def solve(self,
              preconditioner=None,                  # type: dict
              start_rank_one=False,                 # type: bool
              init_value=None,                      # type: list
              normalize_init=False                  # type: bool
              ):
        """
        wrapper for the Sparse ALS method creating initial value and preconditioner
        :param preconditioner: dictionary containing information about the used preconditioner
        :param start_rank_one: flag to use a rank one initial tensor
        :param init_value: given initial tensor
        :param normalize_init: flag to normalize the initial tensor
        :return: solution tensor
        """

        if isinstance(self.rhs, list):
            import tt
            self.rhs = tt.vector.from_list(self.rhs)
        if isinstance(self.rhs, ExtendedTT):
            import tt
            self.rhs = tt.vector.from_list(self.rhs.components)
        W = self.create_start_tensor(start_rank_one, init_value, normalize_init, preconditioner)
        print("start_tensor: {}".format(W))
        P = self.create_precondition(preconditioner)
        i = 0
        while True:
            result = ttALS(self.lhs, self.rhs, W, conv=self.convergence, maxit=self.als_iterations, P=P,
                           return_local_error=True)
            W, self.local_error_list, self.first_comp_convergence_list, self.actual_iterations, restart = result
            if restart is True and i < 10:
                print("  create a rank - 1 update and restart the algorithm {} / {}".format(i, 10))
                W += ttRandomDeterministicRankOne(self.lhs.n)
            else:
                break
        import tt
        if isinstance(W, tt.vector):
            W = tt.vector.to_list(W)
        return W
    # endregion

    # region def create start tensor
    def create_start_tensor(self,
                            start_rank_one=False,   # type: bool
                            init_value=None,        # type: list
                            normalize_init=False,   # type: bool
                            preconditioner=None     # type: dict
                            ):
        """
        function to create a starting tensor according to given flags. if nothing is specified, return a random tensor
        of the current rank
        :param start_rank_one: create a rank one tensor and return it
        :param init_value: predefined initial value
        :param normalize_init: flag to normalize the initial value
        :param preconditioner: dict containing information about the used preconditioner
        :return: start tensor
        """
        if start_rank_one is True:
            #                                       # create random deterministic rank one tensor
            start_ten = ttRandomDeterministicRankOne(self.lhs.n)
            return start_ten

        if init_value is not None:
            if normalize_init is True:
                # TODO: do not use the tt toolbox for norm calculation and structure
                import tt
                W = tt.vector.from_list(init_value)
                W *= (1 / W.norm())  # normalize
                # W = tt.vector.to_list(W)

                # print("rank of W : {}".format(max(tt.vector.from_list(W).r)))
                return W
            else:
                return init_value
        #                                           # create tensor with given rank and size
        # print("random tensor")
        last_rank = 0
        W = ttRandomDeterministicRankOne(self.lhs.n)
        P = self.create_precondition(preconditioner)
        while True:
            W, _, restart = ttALS(self.lhs, self.rhs, W, conv=self.convergence, maxit=self.als_iterations, P=P, no_print=True)
            if max(W.r) >= self.rank-1 and not restart:
                W += ttRandomDeterministicRankOne(self.lhs.n)
                break
            if max(W.r) <= last_rank and not restart:
                print("WARNING: Create start tensor: the rank decreased in the last iteration step")
                break
            last_rank = max(W.r)
            W += ttRandomDeterministicRankOne(self.lhs.n)
            print("start tensor rank increased: \n   {}".format(W))
        return W
    # endregion

    # region create precondition
    def create_precondition(self,
                            preconditioner          # type: dict
                            ):
        """
        implements a wrapper for different kinds of precondition. Currently supports only the field mean method
        :param preconditioner:
        :return:
        """
        retval = dict()
        if preconditioner is None:
            retval["precond"] = np.eye(self.lhs.n[0], self.lhs.n[0])
            retval["method"] = "spsolve"
            return retval
        # print preconditioner
        assert("name" in preconditioner)
        # print(preconditioner["name"].strip().capitalize())
        if preconditioner["name"].strip() == "field mean":
            assert("coef" in preconditioner)
            assert(hasattr(preconditioner["coef"], "extract_mean"))
            extract_mean = getattr(preconditioner["coef"], "extract_mean", None)
            assert(callable(extract_mean))
            print("  use precondition: {}".format(preconditioner["name"]))
            retval["method"] = "cg"
            retval["precond"] = extract_mean()
            return retval
        if preconditioner["name"].strip() == "diag field mean":
            assert ("coef" in preconditioner)
            assert (hasattr(preconditioner["coef"], "extract_mean"))
            extract_mean = getattr(preconditioner["coef"], "extract_mean", None)
            assert (callable(extract_mean))
            print("  use precondition: {}".format(preconditioner["name"]))
            retval["method"] = "direct diag"
            mean_mat = extract_mean().diagonal()
            mean_mat_inv = sps.spdiags(mean_mat**(-1), 0, mean_mat.size, mean_mat.size)
            mean_mat = sps.spdiags(mean_mat, 0, mean_mat.size, mean_mat.size)
            retval["precond"] = mean_mat
            retval["precond_inv"] = mean_mat_inv
            return retval
        if preconditioner["name"].strip() == "stored field mean":
            assert ("coef" in preconditioner)
            assert (hasattr(preconditioner["coef"], "extract_mean"))
            extract_mean = getattr(preconditioner["coef"], "extract_mean", None)
            assert (callable(extract_mean))
            print("  use precondition: {}".format(preconditioner["name"]))
            print("    invert mean matrix")
            mat = extract_mean()
            with TicToc(sec_key="preconditioner", key="invert mean matrix", do_print=True, active=True):
                mat_inv = np.linalg.inv(mat.todense())
            retval["precond"] = mat
            retval["precond_inv"] = mat_inv
            retval["method"] = "stored"
            return retval
        if preconditioner["name"].strip() == "stored field trace":
            assert ("coef" in preconditioner)
            assert (hasattr(preconditioner["coef"], "extract_trace"))
            extract_trace = getattr(preconditioner["coef"], "extract_trace", None)
            assert (callable(extract_trace))
            print("  use precondition: {}".format(preconditioner["name"]))
            print("    invert trace matrix")
            mat = extract_trace()
            with TicToc(sec_key="preconditioner", key="invert mean matrix", do_print=True, active=True):
                mat_inv = np.linalg.inv(mat.todense())
            retval["precond"] = mat
            retval["precond_inv"] = mat_inv
            retval["method"] = "stored"
            return retval
        if preconditioner["name"].strip() == "stored field mean var":
            assert ("coef" in preconditioner)
            assert (hasattr(preconditioner["coef"], "extract_mean"))
            assert (hasattr(preconditioner["coef"], "extract_variance"))
            extract_mean = getattr(preconditioner["coef"], "extract_mean", None)
            extract_var = getattr(preconditioner["coef"], "extract_variance", None)
            assert (callable(extract_mean))
            assert (callable(extract_var))
            print("  use precondition: {}".format(preconditioner["name"]))
            print("    invert mean matrix")
            mat_mean = extract_mean()
            mat_var = extract_var()
            mat = mat_mean + mat_var
            with TicToc(sec_key="preconditioner", key="invert mean matrix", do_print=True, active=True):
                mat_inv = np.linalg.inv(mat.todense())
            retval["precond"] = mat
            retval["precond_inv"] = mat_inv
            retval["method"] = "stored"
            return retval
        if preconditioner["name"].strip() == "stored trace":
            assert ("operator" in preconditioner)
            assert (hasattr(preconditioner["operator"], "extract_trace"))
            trace = getattr(preconditioner["operator"], "extract_trace", None)
            assert (callable(trace))
            assert (hasattr(preconditioner["operator"], "extract_stochastic_dimension"))
            dim = getattr(preconditioner["operator"], "extract_stochastic_dimension", None)
            assert (callable(dim))
            print("  use precondition: {}".format(preconditioner["name"]))
            mat = trace() * dim()**(-1)

            with TicToc(sec_key="preconditioner", key="invert mean matrix", do_print=True, active=True):
                mat_inv = np.linalg.inv(mat.todense())
            retval["precond"] = mat
            retval["precond_inv"] = mat_inv
            retval["method"] = "stored"
            return retval
        if preconditioner["name"].strip() == "stored rank":
            assert ("operator" in preconditioner)
            assert ("coef" in preconditioner)
            assert (hasattr(preconditioner["coef"], "extract_mean"))
            extract_mean = getattr(preconditioner["coef"], "extract_mean", None)
            assert (callable(extract_mean))
            print("  use precondition: {}".format(preconditioner["name"]))
            mat = preconditioner["operator"].tt_bilinearform.cores[0]

            # for lia in range(len(mat)):
            #     mat[lia] = sps.eye(mat[lia].shape[0], mat[lia].shape[1])
            mat_inv = []
            with TicToc(sec_key="preconditioner", key="invert mean matrix", do_print=True, active=True):
                for lia in range(len(mat)):
                    mat_inv.append(np.linalg.inv(mat[lia].todense()))
            retval["precond"] = mat
            retval["precond_inv"] = mat_inv
            retval["method"] = "stored rank"
            return retval
        if preconditioner["name"].strip() == "direct":
            assert ("operator" in preconditioner)
            # assert ("coef" in preconditioner)
            # assert (hasattr(preconditioner["coef"], "extract_mean"))
            # extract_mean = getattr(preconditioner["coef"], "extract_mean", None)
            # assert (callable(extract_mean))
            print("  use precondition: {}".format(preconditioner["name"]))
            retval["data"] = np.array([preconditioner["operator"].tt_bilinearform.cores[0][lia].data
                                       for lia in range(len(preconditioner["operator"].tt_bilinearform.cores[0]))])
            retval["data"] = retval["data"].reshape(len(preconditioner["operator"].tt_bilinearform.cores[0]),
                                                    preconditioner["operator"].tt_bilinearform.cores[0][0].nnz)
            retval["method"] = "direct"
            return retval
        if preconditioner["name"].strip() == "scipy cg":
            assert ("coef" in preconditioner)
            assert (hasattr(preconditioner["coef"], "extract_mean"))
            extract_mean = getattr(preconditioner["coef"], "extract_mean", None)
            assert (callable(extract_mean))
            retval["precond"] = np.linalg.inv(extract_mean().todense())
            retval["method"] = "scipy cg"
            return retval
        if preconditioner["name"].strip() == "LU":
            print("  use precondition: {}".format(preconditioner["name"]))
            retval["method"] = "direct lu"
            return retval
        if preconditioner["name"].strip() == "none":
            retval["precond"] = 1
            retval["method"] = "none"
            return retval
        if preconditioner["name"].strip() == "sylvester glcr":
            # if True:
            #     raise ValueError("Does not converge")
            retval["precond"] = None
            retval["method"] = "sylvester glcr"
            return retval
        print("use no preconditioner")
        retval["precond"] = np.eye(self.lhs.n[0], self.lhs.n[0])
        retval["method"] = "spsolve"
        return retval

    # endregion
