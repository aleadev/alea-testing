from alea.utils.tictoc import TicToc
import numpy as np
import scipy
from copy import deepcopy

class SylvesterIterativeSolver(object):
    """
    Iterative Solver using

            - global conjugate gradient method   (GLCG)
            - global conjugate residual method   (GCCR)


    iterativly seeking for X such that

                    S(X):= sum_i^q  A_i X B_i = C

    with $A_i \in \mathbb{R}^{n,n} and $B_i\in\mathbb{R}^{m,m}$.

    """
    def __init__(self, Alist, Blist, Cmat):
        """
        Constructor creates the linear opeartor S.
        :param Alist:
        :param Blist:
        :param Cmat:
        """
        self.S = generalized_Sylvester_Operator(Alist,Blist)

        self.C = Cmat

        self.Xshape = (Alist[0].shape[0], Blist[0].shape[0])

        self.X = None # the solution matrix

    def updateB(self, Blist):
        """
        Changes the Bs in the underlying linear operator S
        :param Blist:
        :return:
        """
        self.S.updateB(Blist)

    def GLCG(self, tol, itMax):



        X = np.random.rand(self.Xshape[0], self.Xshape[1])
        R = self.C - self.S.apply(X)
        P = R

        gamma = scipy.linalg.norm(R) ** 2

        it = 0
        err= np.inf
        ret_err = [err]
        while err > tol and it < itMax:


            SP  = self.S.apply(P)



            alpha = gamma  / np.trace(SP.transpose().dot(P))

            X += alpha * P


            R -= alpha * SP


            gamma_old = gamma
            gamma = scipy.linalg.norm(R) ** 2

            beta = gamma / gamma_old

            P = R + beta * P

            it+=1
            err = np.sqrt(gamma)
            ret_err.append(err)
            #print("iter={it}, err = {err}, X=\n{X}".format(it=it, err=err, X=X))
            # print("iter={it}, err = {err}".format(it=it, err=err))

        return X, ret_err

    def GLCR(self, tol, itMax, X=None):
        if X is None:
            X = np.random.rand(self.Xshape[0], self.Xshape[1])
        R = self.C - self.S.apply(X)
        P = self.C - self.S.apply(X)

        SP = self.S.apply(P)
        SR = self.S.apply(R)

        gamma = np.trace(SR.transpose().dot(R))

        it = 0
        err = np.inf
        ret_err = [err]
        while err > tol and it < itMax:

            alpha = gamma / scipy.linalg.norm(SP) ** 2

            X+= alpha * P
            R-= alpha * SP


            SR = self.S.apply(R)

            gamma_old = gamma

            with TicToc(key="compute (S(R),R)_F", active=True, do_print=False):
                gamma = np.trace(SR.transpose().dot(R))

            beta = gamma / gamma_old

            P = R + beta*P

            SP = SR + beta*SP



            it += 1
            with TicToc(key="compute residuum", active=True, do_print=False):
                err = np.linalg.norm(R)
            ret_err.append(err)
            # print("iter={it}, err = {err}, X=\n{X}".format(it=it, err=err, X=X))
            # print("iter={it}, err = {err}".format(it=it, err=err))
        if (ret_err[-1] > tol):
            print("error: {}".format(ret_err[-1]))
        return X, ret_err




class generalized_Sylvester_Operator(object):
    """
    class representing
         S(X):= sum_i^q  A_i X B_i

    as application
    """
    def __init__(self, Alist, Blist):

        self.q = len(Alist)
        assert len(Alist) == len(Blist)
        self.Alist = Alist
        self.Blist = Blist

    def updateB(self, Blist):
        self.Blist = Blist

    def apply(self,X):
        with TicToc(key="operator application", active=True, do_print=False):

            res =sum(self.Alist[k].dot(X.dot(self.Blist[k])) for k in range(self.q))
        return res


