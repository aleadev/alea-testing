"""
This class implements the general use of a config file and defines needed objects and methods
"""
# region Imports
import os

# endregion

FILE_PATH = os.path.dirname(__file__) + "/problems/"


class ProblemConfig(object):
    def __init__(self,
                 problem_file                       # type: str
                 ):
        """
        constructor creates the path to the problem file
        :param problem_file: string to the file to read
        """
        self.problem_file = problem_file

    def __problem_file_exists(self):
        """
        check method to ensure that the given problem file is readable
        :return: boolean
        """

        try:
            f = open(self.problem_file, 'rb')
            f.close()
            return True
        except Exception as ex:
            print("can not open {}: \n    {}".format(self.problem_file, ex.message))
        return False

    def read_configuration(self):
        """
        stub for the configuration reader
        :return: not implemented exception
        """
        raise NotImplemented("This class can not read a file")

    def write_configuration(self):
        """
        stub for writing the configuration to a file
        :return: not implemented
        """
        raise NotImplemented("This class can not write a file")

    def validate(self):
        """
        stub to validate the read configuration
        :return: not implemented
        """
        raise NotImplemented("This class can not validate its configuration")
