import unittest
import os
from alea.application.bayes.alea_util.configuration.asgfem_config import AsgfemConfig

FILE_PATH = os.path.dirname(__file__) + "/../problems/"


class TestTTLognormalField(unittest.TestCase):
    def setUp(self):
        self.config = AsgfemConfig(FILE_PATH + "default.pro")
        self.config.max_rank = 50
        self.config.sol_rank = 10
        self.config.max_dofs = 150000
        self.config.mesh_refine = -1
        self.config.iterations = 1000
        self.config.domain = "square"
        self.config.femdegree = 3
        self.config.coeffield_type = "EF-square-cos-algebraic"
        self.config.decay_exp_rate = 2.0
        self.config.field_mean = 0.0
        self.config.sgfem_gamma = 0.9
        self.config.init_mesh_dofs = 10
        self.config.theta = 0.5
        self.config.n_coefficients = 2
        self.config.rho = 1.0
        self.config.freq_skip = 0
        self.config.freq_scale = 1.0
        self.config.scale = 1.0
        self.config.hermite_degree = 2
        self.config.gpc_degree = 2
        self.config.als_tol = 1e-10
        self.config.coef_acc = 1e-10
        self.config.adaptive_iterations = 1000
        self.config.theta_x = 0.5
        self.config.theta_y = 0.5
        self.config.start_rank = 2
        self.config.num_coeff_in_coeff = 20
        self.config.max_hdegs_coef = 20
        self.config.eta_zeta_weight = 1.0
        self.config.resnorm_zeta_weight = 1.0
        self.config.coef_mesh_dofs = 10000
        self.config.coef_quad_degree = 7
        self.config.rank_always = False
        self.config.new_hdeg = 2
        
    def test_write_config(self):
        self.assertTrue(self.config.write_configuration(), 'can not write configuration')

    def test_read_config(self):
        self.assertTrue(self.config.read_configuration(), 'can not read configuration')


if __name__ == '__main__':
    unittest.main()