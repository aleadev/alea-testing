"""
This class implements the general use of a config file and defines objects, needed to solve the adaptive sgfem
problem in tensor train format, especially for the log-normal case
TODO: check affine and general case
"""
# region Imports
from alea.application.bayes.alea_util.configuration.problem_config import ProblemConfig
import os
import ast
from alea.utils.except_helper import exceptionTB
# endregion

FILE_PATH = os.path.dirname(__file__) + "/bayes_problems/"


class BayesConfig(ProblemConfig):
    # region init
    def __init__(self,
                 problem_file                       # type: str
                 ):
        """
        constructor creates the path to the problem file
        :param problem_file: string to the file to read
        """
        ProblemConfig.__init__(self, problem_file)
        self.max_rank = None
        self.sol_rank = None
        self.max_dofs = None
        self.mesh_refine = None
        self.iterations = None
        self.domain = None
        self.femdegree = None
        self.coeffield_type = None
        self.decay_exp_rate = None
        self.field_mean = None
        self.sgfem_gamma = None
        self.init_mesh_dofs = None
        self.theta = None
        self.n_coefficients = None
        self.rho = None
        self.freq_skip = None
        self.freq_scale = None
        self.scale = None
        self.hermite_degree = None
        self.gpc_degree = None
        self.als_tol = None
        self.coef_acc = None
        self.adaptive_iterations = None
        self.theta_x = None
        self.theta_y = None
        self.start_rank = None
        self.num_coeff_in_coeff = None
        self.max_hdegs_coef = None
        self.hdegs_coef = None
        self.eta_zeta_weight = None
        self.resnorm_zeta_weight = None
        self.coef_mesh_dofs = None
        self.coef_quad_degree = None
        self.rank_always = None
        self.new_hdeg = None
        self.preconditioner = None
        self.nCPU = None
        self.reference_expression_degree = None
        self.reference_M = None
        self.n_samples = None

        self.y_true = None
        self.ref_mesh_dof = None
    # endregion
    
    # region def Problem File exists
    def __problem_file_exists(self):
        """
        check method to ensure that the given problem file is readable
        :return: boolean
        """
        pass
    # endregion
    
    # region def validate
    def validate(self):
        """
        validate all entries by checking if the important items are not None anymore
        :return: boolean
        """
        # region max rank
        if self.max_rank is None:
            print("max rank is none")
            return False
        if isinstance(self.max_rank, int) is False:
            print("max rank is not an int but {}".format(type(self.max_rank)))
            return False
        # endregion
        # region sol rank
        if self.sol_rank is None:
            print("sol rank is none")
            return False
        if isinstance(self.sol_rank, int) is False:
            print("sol rank is not an int but {}".format(type(self.sol_rank)))
            return False
        # endregion
        # region max dofs
        if self.max_dofs is None:
            print("max dofs is none")
            return False
        if isinstance(self.max_dofs, int) is False:
            print("max dofs is not an int but {}".format(type(self.max_dofs)))
            return False
        # endregion
        # region mesh refine
        if self.mesh_refine is None:
            print("mesh refine is none")
            return False
        if isinstance(self.mesh_refine, int) is False:
            print("mesh refine is not an int but {}".format(type(self.mesh_refine)))
            return False
        # endregion
        # region iterations
        if self.iterations is None:
            print("iterations is none")
            return False
        if isinstance(self.iterations, int) is False:
            print("iterations is not an int but {}".format(type(self.iterations)))
            return False
        # endregion
        # region domain
        if self.domain is None:
            print("Domain is None")
            return False
        if not self.domain == "square" and not self.domain=="lshape":
            print("Domain is unknown: {}".format(self.domain))
            return False
        # endregion
        # region femdegree
        if self.femdegree is None:
            print("Femdegree is None")
            return False
        if isinstance(self.femdegree, int) is False:
            print("Femdegree is not an int: {}".format(type(self.femdegree)))
            return False
        if self.femdegree < 1 or self.femdegree > 3:
            print("Femdegree not 1,2 or 3: {}".format(self.femdegree))
            return False
        # endregion
        # region coefffield type
        if isinstance(self.coeffield_type, str) is False:
            print("Coefficient field type is not a string: {}".format(type(self.coeffield_type)))
            return False
        # endregion
        # region decay exp rate
        if isinstance(self.decay_exp_rate, float) is False:
            print("Decay exp rate is not a float: {}".format(type(self.decay_exp_rate)))
            return False
        # endregion
        # region field mean
        if isinstance(self.field_mean, float) is False:
            print("Field mean is not a float: {}".format(type(self.field_mean)))
            return False
        if self.field_mean < 0:
            print("Field mean is smaller than 0: {}".format(self.field_mean))
            return False
        # endregion
        # region sgfem gamma
        if isinstance(self.sgfem_gamma, float) is False:
            print("SGFEM Gamma is not a float: {}".format(type(self.sgfem_gamma)))
            return False
        if self.sgfem_gamma > 1 or self.sgfem_gamma < 0:
            print("Sgfem Gamma is not in (0, 1): {}".format(self.sgfem_gamma))
            return False
        # endregion
        # region init mesh dofs
        if isinstance(self.init_mesh_dofs, int) is False:
            print("init mesh dofs is not an int: {}".format(type(self.init_mesh_dofs)))
            return False
        if self.init_mesh_dofs < 0:
            print("init mesh dofs is negative: {}".format(self.init_mesh_dofs))
            return False
        # endregion
        # region Theta
        if isinstance(self.theta, float) is False:
            print("Theta is not a float: {}".format(type(self.theta)))
            return False
        # endregion
        # region N coefficients
        if isinstance(self.n_coefficients, int) is False:
            print("n coefficients is not an int: {}".format(type(self.n_coefficients)))
            return False
        if self.n_coefficients < 0:
            print("n coefficients is negative: {}".format(self.n_coefficients))
            return False
        # endregion
        # region rho
        if isinstance(self.rho, float) is False:
            print("rho is not a float: {}".format(type(self.rho)))
            return False
        if self.rho < 0:
            print("rho is negative: {}".format(self.rho))
            return False
        # endregion
        # region freq skip 
        if isinstance(self.freq_skip, int) is False:
            print("freq skip is not an int: {}".format(type(self.freq_skip)))
            return False
        if self.freq_skip < 0:
            print("freq skip is negative: {}".format(self.freq_skip))
            return False
        # endregion
        # region freq scale 
        if isinstance(self.freq_scale, float) is False:
            print("freq scale is not a float: {}".format(type(self.freq_scale)))
            return False
        if self.freq_scale < 0:
            print("freq scale is negative: {}".format(self.freq_scale))
            return False
        # endregion
        # region scale
        if isinstance(self.scale, float) is False:
            print("scale is not an float: {}".format(type(self.scale)))
            return False
        if self.scale < 0:
            print("scale is negative: {}".format(self.scale))
            return False
        # endregion
        # region hermite_degree
        if isinstance(self.hermite_degree, int) is False:
            print("hermite_degree is not an int: {}".format(type(self.hermite_degree)))
            return False
        if self.hermite_degree < 0:
            print("hermite_degree is negative: {}".format(self.hermite_degree))
            return False
        # endregion
        # region gpc_degree
        if isinstance(self.gpc_degree, int) is False:
            print("gpc_degree is not an int: {}".format(type(self.gpc_degree)))
            if isinstance(self.gpc_degree, list) is False:
                print(" gpc_degree is not a list as well")
                return False
            else:
                for item in self.gpc_degree:
                    if isinstance(item, int) is False:
                        print("  item is not an int: {}".format(item))
                        return False
        else:
            if self.gpc_degree < 0:
                print("gpc_degree is negative: {}".format(self.gpc_degree))
                return False
        # endregion
        # region als_tol
        if isinstance(self.als_tol, float) is False:
            print("als_tol is not a float: {}".format(type(self.als_tol)))
            return False
        if self.als_tol < 0:
            print("als_tol is negative: {}".format(self.als_tol))
            return False
        if self.als_tol > 1:
            print("als_tol is > 1: {}".format(self.als_tol))
            return False
        # endregion
        # region coef_tol
        if isinstance(self.coef_acc, float) is False:
            print("coef_acc is not an float: {}".format(type(self.coef_acc)))
            return False
        if self.coef_acc < 0:
            print("coef_tol is negative: {}".format(self.coef_acc))
            return False
        if self.coef_acc > 1:
            print("coef_acc is > 1: {}".format(self.coef_acc))
            return False
        # endregion
        # region adaptive iterations
        if isinstance(self.adaptive_iterations, int) is False:
            print("adaptive iterations is not an int: {}".format(type(self.adaptive_iterations)))
            return False
        if self.coef_acc < 0:
            print("adaptive iterations is negative: {}".format(self.adaptive_iterations))
            return False
        # endregion
        # region Theta X
        if isinstance(self.theta_x, float) is False:
            print("Theta x is not an float: {}".format(type(self.theta_x)))
            return False
        if self.theta_x < 0:
            print("theta x is negative: {}".format(self.theta_x))
            return False
        if self.coef_acc > 1:
            print("theta x is > 1: {}".format(self.theta_x))
            return False
        # endregion
        # region Theta X
        if isinstance(self.theta_y, float) is False:
            print("Theta y is not an float: {}".format(type(self.theta_y)))
            return False
        if self.theta_y < 0:
            print("theta y is negative: {}".format(self.theta_y))
            return False
        if self.coef_acc > 1:
            print("theta y is > 1: {}".format(self.theta_y))
            return False
        # endregion
        # region start rank
        if isinstance(self.start_rank, int) is False:
            print("start rank is not an int: {}".format(type(self.start_rank)))
            return False
        if self.start_rank < 0:
            print("start rank is negative: {}".format(self.start_rank))
            return False
        # endregion
        # region num coef in coef
        if isinstance(self.num_coeff_in_coeff, int) is False:
            print("num coef in coef is not an int: {}".format(type(self.num_coeff_in_coeff)))
            return False
        if self.num_coeff_in_coeff < 0:
            print("num coef in coef is negative: {}".format(self.num_coeff_in_coeff))
            return False
        # endregion
        # region gpc_degree
        if isinstance(self.hdegs_coef, int) is False:
            print("hdegs_coef is not an int: {}".format(type(self.hdegs_coef)))
            if isinstance(self.hdegs_coef, list) is False:
                print(" hdegs_coef is not a list as well")
                return False
            else:
                for item in self.hdegs_coef:
                    if isinstance(item, int) is False:
                        print("  item is not an int: {}".format(item))
                        return False
        else:
            if self.hdegs_coef < 0:
                print("hdegs_coef is negative: {}".format(self.hdegs_coef))
                return False
        # endregion
        # region rank always
        if isinstance(self.rank_always, bool) is False:
            print("rank always is not a bool: {}".format(type(self.rank_always)))
            return False
        # endregion
        # region reference expression degree
        if isinstance(self.reference_expression_degree, int) is False:
            print("reference expression degree is not an int: {}".format(type(self.reference_expression_degree)))
            return False
        if self.reference_expression_degree < 0:
            print("reference expression degree is negative: {}".format(self.reference_expression_degree))
            return False
        # endregion
        #  region reference M
        if isinstance(self.reference_M, int) is False:
            print("reference M is not an int: {}".format(type(self.reference_M)))
            return False
        if self.reference_M < 0:
            print("reference M is negative: {}".format(self.reference_M))
            return False
        # endregion
        #  region number of samples
        if isinstance(self.n_samples, int) is False:
            print("number of samples is not an int: {}".format(type(self.n_samples)))
            return False
        if self.n_samples < 0:
            print("number of samples is negative: {}".format(self.n_samples))
            return False
        # endregion
        # region nCPU
        if isinstance(self.nCPU, int) is False:
            print("nCPU is not an int: {}".format(type(self.nCPU)))
            return False
        if self.nCPU < -1:
            print("nCPU is negative: {}".format(self.nCPU))
            return False
        # endregion
        # TODO: remaining parameter
        # region preconditioner
        if not isinstance(self.preconditioner, str):
            print("precondition is not a string: {}".format(type(self.preconditioner)))
            return False
        # endregion
        return True
    # endregion
    
    # region read configuration
    def read_configuration(self):
        """
        function that actually reads the configuration and returns success as boolean
        :return: boolean
        """
        try:
            f = open(self.problem_file, 'rb')
            lines = f.readlines()
            for line in lines:
                line_items = line.split("=")
                if len(line_items) != 2:
                    continue
                item, value = line_items[0].upper().strip(), line_items[1].strip()
                print("item: {} - value: {}".format(item, value))
                if item == "MAX RANK":
                    self.max_rank = int(value)
                    continue
                if item == "SOL RANK":
                    self.sol_rank = int(value)
                    continue
                if item == "MAX DOFS":
                    self.max_dofs = int(value)
                    continue
                if item == "MESH REFINE":
                    self.mesh_refine = int(value)
                    continue
                if item == "ITERATIONS":
                    self.iterations = int(value)
                    continue
                if item == "DOMAIN":
                    self.domain = str(value)
                    continue
                if item == "FEMDEGREE":
                    self.femdegree = int(value)
                    continue
                if item == "COEFFIELD TYPE":
                    self.coeffield_type = str(value)
                    continue
                if item == "DECAY EXP RATE":
                    self.decay_exp_rate = float(value)
                    continue
                if item == "FIELD MEAN":
                    self.field_mean = float(value)
                    continue
                if item == "SGFEM GAMMA":
                    self.sgfem_gamma = float(value)
                    continue
                if item == "INIT MESH DOFS":
                    self.init_mesh_dofs = int(value)
                    continue
                if item == "THETA":
                    self.theta = float(value)
                    continue
                if item == "N COEFFICIENTS":
                    self.n_coefficients = int(value)
                    continue
                if item == "RHO":
                    self.rho = float(value)
                    continue
                if item == "FREQ SKIP":
                    self.freq_skip = int(value)
                    continue
                if item == "FREQ SCALE":
                    self.freq_scale = float(value)
                    continue
                if item == "SCALE":
                    self.scale = float(value)
                    continue
                if item == "HERMITE DEGREE":
                    self.hermite_degree = int(value)
                    continue
                if item == "GPC DEGREE":
                    try:
                        self.gpc_degree = int(value)
                    except ValueError as ex:
                        error_string = "config: gpc_degree is not an int: {} \n but \n {}".format(ex, value)
                        try:
                            self.gpc_degree = ast.literal_eval(value)
                            if isinstance(self.gpc_degree, list):
                                for item in self.gpc_degree:
                                    if not isinstance(item, int):
                                        error_string += "\n config: item: {} of gpc_degree is not an int".format(item)
                                        break
                                continue
                            else:
                                error_string += "config: gpc_degree is not a list but {}".format(value)
                        except SyntaxError as ex:
                            error_string += "\n config: gpc_degree, ast raise syntax error: {}".format(ex)
                        raise ValueError(error_string)
                    continue
                if item == "ALS TOL":
                    self.als_tol = float(value)
                    continue
                if item == "COEF ACC":
                    self.coef_acc = float(value)
                    continue
                if item == "ADAPTIVE ITERATIONS":
                    self.adaptive_iterations = int(value)
                    continue
                if item == "THETA X":
                    self.theta_x = float(value)
                    continue
                if item == "THETA Y":
                    self.theta_y = float(value)
                    continue
                if item == "START RANK":
                    self.start_rank = int(value)
                    continue
                if item == "NUM COEFF IN COEFF":
                    self.num_coeff_in_coeff = int(value)
                    continue
                if item == "MAX HDEGS COEF":
                    self.max_hdegs_coef = int(value)
                    continue
                if item == "HDEGS COEF":
                    try:
                        self.hdegs_coef = int(value)
                    except ValueError as ex:
                        error_string = "config: hdegs coef is not an int: {} \n but \n {}".format(ex, value)
                        try:
                            self.hdegs_coef = ast.literal_eval(value)
                            if isinstance(self.hdegs_coef, list):
                                for item in self.hdegs_coef:
                                    if not isinstance(item, int):
                                        error_string += "\n config: item: {} of hdegs coef is not an int".format(item)
                                        break
                                continue
                            else:
                                error_string += "config: hdegs coef is not a list but {}".format(value)
                        except SyntaxError as ex:
                            error_string += "\n config: hdegs coef, ast raise syntax error: {}".format(ex)
                        raise ValueError(error_string)
                    continue
                if item == "ETA ZETA WEIGHT":
                    self.eta_zeta_weight = float(value)
                    continue
                if item == "RESNORM ZETA WEIGHT":
                    self.resnorm_zeta_weight = float(value)
                    continue
                if item == "COEF MESH DOFS":
                    self.coef_mesh_dofs = int(value)
                    continue
                if item == "COEF QUAD DEGREE":
                    self.coef_quad_degree = int(value)
                    continue
                if item == "RANK ALWAYS":
                    if value.upper() == "FALSE":
                        self.rank_always = False
                    elif value.upper() == "0":
                        self.rank_always = False
                    elif value.upper() == "TRUE":
                        self.rank_always = True
                    elif value.upper() == "1":
                        self.rank_always = True
                    else:
                        raise ValueError("Rank Always value: {} can not be casted".format(value))
                    continue
                if item == "NEW HDEG":
                    self.new_hdeg = int(value)
                    continue
                if item == "PRECONDITIONER":
                    self.preconditioner = str(value)
                    continue
                if item == "REFERENCE_EXPRESSION_DEGREE":
                    self.reference_expression_degree = int(value)
                    continue
                if item == "REFERENCE_M":
                    self.reference_M = int(value)
                    continue
                if item == "N_SAMPLES":
                    self.n_samples = int(value)
                    continue
                if item == "NCPU":
                    self.nCPU = int(value)
                    continue
                if item == "Y TRUE":
                    try:
                        self.y_true = int(value)
                    except ValueError as ex:
                        error_string = "config: y_true is not an int: {} \n but \n {}".format(ex, value)
                        try:
                            self.y_true = ast.literal_eval(value)
                            if isinstance(self.y_true, list):
                                for item in self.y_true:
                                    if not isinstance(item, float):
                                        error_string += "\n config: item: {} of y_true is not a float".format(item)
                                        break
                                continue
                            else:
                                error_string += "config: y true is not a list but {}".format(value)
                        except SyntaxError as ex:
                            error_string += "\n config: y true, ast raise syntax error: {}".format(ex)
                        raise ValueError(error_string)
                    continue
                if item == "REF MESH DOF":
                    self.ref_mesh_dof = int(value)
                    continue
        except Exception as ex:
            print("Can not read {} properly: {}\n   {}".format(self.problem_file, ex.message, ex))
            print(exceptionTB())
            return False

        return self.validate()
    # endregion

    # region write configuration
    def write_configuration(self):
        """
        this function writes all the currently set items to a file
        :return: boolean
        """
        try:
            f = open(self.problem_file, 'wb')
            f.write("### configuration file {} for a model problem of the asgfem ### \n \n".format(self.problem_file))
            f.write("MAX RANK = " + str(self.max_rank) + "\n")
            f.write("SOL RANK = " + str(self.sol_rank) + "\n")
            f.write("MAX DOFS = " + str(self.max_dofs) + "\n")
            f.write("MESH REFINE = " + str(self.mesh_refine) + "\n")
            f.write("ITERATIONS = " + str(self.iterations) + "\n")
            f.write("DOMAIN = " + str(self.domain) + "\n")
            f.write("FEMDEGREE = " + str(self.femdegree) + "\n")
            f.write("COEFFIELD TYPE = " + str(self.coeffield_type) + "\n")
            f.write("DECAY EXP RATE = " + str(self.decay_exp_rate) + "\n")
            f.write("FIELD MEAN = " + str(self.field_mean) + "\n")
            f.write("SGFEM GAMMA = " + str(self.sgfem_gamma) + "\n")
            f.write("INIT MESH DOFS = " + str(self.init_mesh_dofs) + "\n")
            f.write("THETA = " + str(self.theta) + "\n")
            f.write("N COEFFICIENTS = " + str(self.n_coefficients) + "\n")
            f.write("RHO = " + str(self.rho) + "\n")
            f.write("FREQ SKIP = " + str(self.freq_skip) + "\n")
            f.write("FREQ SCALE = " + str(self.freq_scale) + "\n")
            f.write("SCALE = " + str(self.scale) + "\n")
            f.write("HERMITE DEGREE = " + str(self.hermite_degree) + "\n")
            f.write("GPC DEGREE = " + str(self.gpc_degree) + "\n")
            f.write("ALS TOL = " + str(self.als_tol) + "\n")
            f.write("COEF ACC = " + str(self.coef_acc) + "\n")
            f.write("ADAPTIVE ITERATIONS = " + str(self.adaptive_iterations) + "\n")
            f.write("THETA X = " + str(self.theta_x) + "\n")
            f.write("THETA Y = " + str(self.theta_y) + "\n")
            f.write("START RANK = " + str(self.start_rank) + "\n")
            f.write("NUM COEFF IN COEFF = " + str(self.num_coeff_in_coeff) + "\n")
            f.write("MAX HDEGS COEF = " + str(self.max_hdegs_coef) + "\n")
            f.write("HDEGS COEF = " + str(self.hdegs_coef) + "\n")
            f.write("ETA ZETA WEIGHT = " + str(self.eta_zeta_weight) + "\n")
            f.write("RESNORM ZETA WEIGHT = " + str(self.resnorm_zeta_weight) + "\n")
            f.write("COEF MESH DOFS = " + str(self.coef_mesh_dofs) + "\n")
            f.write("COEF QUAD DEGREE = " + str(self.coef_quad_degree) + "\n")
            f.write("RANK ALWAYS = " + str(self.rank_always) + "\n")
            f.write("NEW HDEG = " + str(self.new_hdeg) + "\n")
            f.write("PRECONDITIONER = " + str(self.preconditioner) + "\n")
            f.write("REFERENCE_EXPRESSION_DEGREE = " + str(self.reference_expression_degree) + "\n")
            f.write("REFERENCE_M = " + str(self.reference_M) + "\n")
            f.write("NCPU = " + str(self.nCPU) + "\n")
            f.write("N_SAMPLES = " + str(self.n_samples) + "\n")
            f.write("Y TRUE = " + str(self.y_true) + "\n")
            f.write("REF MESH DOF = " + str(self.ref_mesh_dof))
            f.close()
        except Exception as ex:
            print("Can not write configuration {}:".format(self.problem_file, ex.message))
            return False
        return True
    # endregion

    # region write configuration to file
    def write_configuration_to_file(self, _file, append=False):
        """
        this function writes all the currently set items to a file
        :param _file: path to a file to write to
        :param append: switch to turn append mode on, otherwise overwrite an existing file
        :return: boolean
        """
        try:
            if append is not True:
                f = open(_file, 'wb')
            else:
                f = open(_file, "a")
            f.write("### configuration file {} for a model problem of the asgfem ### \n \n".format(self.problem_file))
            f.write("MAX RANK = " + str(self.max_rank) + "\n")
            f.write("SOL RANK = " + str(self.sol_rank) + "\n")
            f.write("MAX DOFS = " + str(self.max_dofs) + "\n")
            f.write("MESH REFINE = " + str(self.mesh_refine) + "\n")
            f.write("ITERATIONS = " + str(self.iterations) + "\n")
            f.write("DOMAIN = " + str(self.domain) + "\n")
            f.write("FEMDEGREE = " + str(self.femdegree) + "\n")
            f.write("COEFFIELD TYPE = " + str(self.coeffield_type) + "\n")
            f.write("DECAY EXP RATE = " + str(self.decay_exp_rate) + "\n")
            f.write("FIELD MEAN = " + str(self.field_mean) + "\n")
            f.write("SGFEM GAMMA = " + str(self.sgfem_gamma) + "\n")
            f.write("INIT MESH DOFS = " + str(self.init_mesh_dofs) + "\n")
            f.write("THETA = " + str(self.theta) + "\n")
            f.write("N COEFFICIENTS = " + str(self.n_coefficients) + "\n")
            f.write("RHO = " + str(self.rho) + "\n")
            f.write("FREQ SKIP = " + str(self.freq_skip) + "\n")
            f.write("FREQ SCALE = " + str(self.freq_scale) + "\n")
            f.write("SCALE = " + str(self.scale) + "\n")
            f.write("HERMITE DEGREE = " + str(self.hermite_degree) + "\n")
            f.write("GPC DEGREE = " + str(self.gpc_degree) + "\n")
            f.write("ALS TOL = " + str(self.als_tol) + "\n")
            f.write("COEF ACC = " + str(self.coef_acc) + "\n")
            f.write("ADAPTIVE ITERATIONS = " + str(self.adaptive_iterations) + "\n")
            f.write("THETA X = " + str(self.theta_x) + "\n")
            f.write("THETA Y = " + str(self.theta_y) + "\n")
            f.write("START RANK = " + str(self.start_rank) + "\n")
            f.write("NUM COEFF IN COEFF = " + str(self.num_coeff_in_coeff) + "\n")
            f.write("MAX HDEGS COEF = " + str(self.max_hdegs_coef) + "\n")
            f.write("HDEGS COEF = " + str(self.hdegs_coef) + "\n")
            f.write("ETA ZETA WEIGHT = " + str(self.eta_zeta_weight) + "\n")
            f.write("RESNORM ZETA WEIGHT = " + str(self.resnorm_zeta_weight) + "\n")
            f.write("COEF MESH DOFS = " + str(self.coef_mesh_dofs) + "\n")
            f.write("COEF QUAD DEGREE = " + str(self.coef_quad_degree) + "\n")
            f.write("RANK ALWAYS = " + str(self.rank_always) + "\n")
            f.write("NEW HDEG = " + str(self.new_hdeg) + "\n")
            f.write("PRECONDITIONER = " + str(self.preconditioner) + "\n")
            f.write("REFERENCE_EXPRESSION_DEGREE = " + str(self.reference_expression_degree) + "\n")
            f.write("REFERENCE_M = " + str(self.reference_M) + "\n")
            f.write("NCPU = " + str(self.nCPU) + "\n")
            f.write("N_SAMPLES = " + str(self.n_samples) + "\n")
            f.write("Y TRUE = " + str(self.y_true) + "\n")
            f.write("REF MESH DOF = " + str(self.ref_mesh_dof) + "\n")
            f.close()
        except Exception as ex:
            print("Can not write configuration {}:".format(self.problem_file, ex.message))
            return False
        return True
    # endregion
