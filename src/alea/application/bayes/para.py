from dolfin import *
from joblib import Parallel, delayed
from petsc4py.PETSc import Mat
from alea.utils.tictoc import TicToc

import petsc4py.PETSc as PETSc

gbforms = None
gDoSth = None
gMats = None

n_cup = 4
S = 1


def main():
    N = 1
    set_log_level(50)
    with TicToc(key="- Meshes: ", active=True, do_print=False):
        meshes = [UnitSquareMesh(N, N) for s in range(S)]
    with TicToc(key="- fs: ", active=True, do_print=False):
        V = [FunctionSpace(meshes[s], 'CG', 1) for s in range(S)]
    with TicToc(key="- bforms: ", active=True, do_print=False):

        bforms = [inner(nabla_grad(TrialFunction(V[s])),

                        nabla_grad(TestFunction(V[s]))) * dx(meshes[s]) for s in range(S)]

    global gbforms
    gbforms = bforms

    global gbackend
    gbackend = as_backend_type

    global gDoSth
    gDoSth = doSth

    global gMats

    with TicToc(key="- parallel threading : ", active=True, do_print=False):

        mats_tmp = [PETSc.Mat().createAIJ([V[s].dim(), V[s].dim()]) for s in range(S)]

        for s in range(S):
            mats_tmp[s].setUp()

            mats_tmp[s].assemble()
        gMats = mats_tmp
        doSthParallel()

        # for s in range(S):
        #     print(s.array())

    # with TicToc(key="- serial             : ", active=True, do_print=False):
    #     mats_tmp2 = [PETSc.Mat().createAIJ([V[s].dim(), V[s].dim()]) for s in range(S)]
    #     for s in range(S):
    #         mats_tmp2[s].setUp()
    #         mats_tmp2[s].assemble()
    #     gMats = mats_tmp2
    #     for s in range(S):
    #         doSth(s)


            # for s in range(S):

            # print(dolfin.PETScMatrix(mats_tmp2[s]).array())

    TicToc.sortedTimes()

state = None

def doSthParallel():

    import cPickle as pickle

    # new_mat = PickablePETScMatrix(0)
    # loc_bf = gbforms[0]
    # retval = assemble(loc_bf, tensor=new_mat)
    # retval = PickablePETScMatrix(new_mat)
    # new_mat.args = retval
    # mat = as_backend_type(mat)
    # print(mat.get(mat))
    # if True:
    #     exit()
    # buff = pickle.dumps(new_mat, protocol=pickle.HIGHEST_PROTOCOL)
    # print("pickle dumped")
    # pickle.loads(buff)
    # print("pickle load")

    global state
    result = Parallel(n_cup
                      # , backend="threading"
                      )(delayed(doSth)(s) for s in range(S))
    print result
    # print result[0]
    for res in result:
        print res
        # print(dolfin.PETScMatrix(res).array())


class PickalableSWIG:

    def __setstate__(self, _state):
        print("call PickablePETSMatrix with args {}".format(type(_state['args'])))
        print("state={}".format(state))
        self.__init__(*state)
        # self.__init__(*_state['args'])

    def __getstate__(self):
        state = self.args[0]
        print(" call get State and return args: {} of type {}".format(self.args, type(self.args)))
        # print("state = {}".format(self.args.array()))
        return {'args': (0,)}


class PickablePETScMatrix(dolfin.cpp.la.PETScMatrix, PickalableSWIG):
    def __init__(self, *args):
        self.args = args
        #print("args={}".format(args))
        print(args)
        # if args != (0,):
            # self.args = args[0]
            # print("args[0] = {}".format(args[0]))
            # print(type(args.mat()))
        #     dolfin.cpp.la.PETScMatrix.__init__(self, args[0])
        # else:
        dolfin.cpp.la.PETScMatrix.__init__(self, args[0])

    def __del__(self):
        print("delete")

def doSth(s):
    loc_bf = gbforms[s]
    # mat = assemble(loc_bf)

    # mat = as_backend_type(mat)
    # print("mat {}".format(mat))
    new_mat = dolfin.PETScMatrix()
    print("uninit: {}".format(new_mat))
    print("type of new_mat = {}".format(type(new_mat)))
    retval = assemble(loc_bf, tensor=new_mat, finalize_tensor=True)
    new_mat = PickablePETScMatrix(retval)
    print("type of new_mat after assemble = {}".format(type(new_mat)))
    # new_mat.apply()
    # new_mat = PickablePETScMatrix(loc_bf)
    # print("init: {}".format(new_mat))
    # dolfin.cpp.la.PETScMatrix.args = mat
    # dolfin.cpp.la.PETScMatrix.__setstate__ = set_state
    # dolfin.cpp.la.PETScMatrix.__getstate__ = get_state
    # print(type(mat))

    print("new_mat={}".format(new_mat.array()))
    # retval = loc_backend(mat)
    # add_mat(loc_mat, mat.mat())
    # print(loc_mat)
    # print(dolfin.PETScMatrix(loc_mat).array())
    # gMats[s].axpy(1.0,mat.mat())

    return new_mat


main()
