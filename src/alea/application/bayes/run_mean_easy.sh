#!/bin/bash
#python paper_conv_mean.py -re 2 -sr 2 -gd 1 -nc 2 -at constant -fm 2 -de 0 -fp results/paper/mean/constant/ -imd 10 & 
#python paper_conv_mean.py -re 2 -sr 2 -gd 1 -nc 2 -at decay-inf -fm 2 -de 2 -fp results/paper/mean/decay-inf/ -imd 10  
#python paper_conv_mean.py -re 2 -sr 2 -gd 1 -nc 2 -at constant -fm 2 -de 0 -fp results/paper/mean/constant/ -imd 10 -pp ../paper-tt-bayesian-inversion/results/constant/ -mean -zmc 50 -cpu 45 &
#python paper_conv_mean.py -re 2 -sr 2 -gd 1 -nc 2 -at constant -fm 2 -de 0 -fp results/paper/mean/constant/ -imd 10 -pp ../paper-tt-bayesian-inversion/results/constant/2/ -dens -zmc 2 -cpu 40 -ng 40 & 
#python paper_conv_coef.py -re 20 -sr 2 -gd 1 -nc 2 -at constant -fm 2 -de 0 -fp results/paper/mean/constant/ -imd 10 -pp ../paper-tt-bayesian-inversion/results/constant/2/ -zmc 200 -cpu 40 -ng 40 -mref 3 & 
#python paper_conv_mean.py -re 2 -sr 2 -gd 1 -nc 2 -at decay-inf -fm 2 -de 2 -fp results/paper/mean/decay-inf/ -imd 10 -pp ../paper-tt-bayesian-inversion/results/decay-inf/ -mean -zmc 50 -cpu 45 & 
python paper_conv_mean.py -re 100 -md 10001 -sr 3  -gd 1 -nc 2 -at decay-inf -fm 2 -de 2 -fp results/paper/mean/decay-inf/ -imd 10 -pp ../paper-tt-bayesian-inversion/results/decay-inf/testSol_2/ -sol -zmc 10 -cpu 1 -ny 2 
#python paper_conv_mean.py -re 100 -md 10000 -sr 1 -gd 1 -nc 2 -at decay-inf -fm 2 -de 2 -fp results/paper/mean/decay-inf/ -imd 10 -pp ../paper-tt-bayesian-inversion/results/decay-inf/testZ/ -post -zmc 100 -cpu 1 
#python paper_conv_mean.py -re 2 -sr 2 -gd 1 -nc 2 -at decay-inf -fm 2 -de 2 -fp results/paper/mean/decay-inf/ -imd 10 -pp ../paper-tt-bayesian-inversion/results/decay-inf/2/ -dens -zmc 2 -cpu 40 
