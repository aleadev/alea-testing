# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  sample TT-tensor Bayesian inversion results in view of normalization constant (mean), density and solution errors
"""
# region Imports
from __future__ import division                     # use float division without dot
import matplotlib                                   # use plot library
matplotlib.use('Agg')                               # use 'Agg' extension to use in cluster environment

# region Bayes libraries
from Bayes_util.lib.bayes_lib import create_parameter_samples
from Bayes_util.exponential.implicit_euler import ImplicitEuler

from Bayes_util.asgfem_bayes import ASGFEMBayes
from Bayes_util.lognormal_sgfem_bayes import LognormalSGFEMBayes
from Bayes_util.exponential.embedded_runge_kutta import EmbeddedRungeKutta
from Bayes_util.exponential.rank_adaptive_embedded_runge_kutta import RankAdaptiveEmbeddedRungeKutta
from Bayes_util.lib.interpolation_util import get_cheb_points
# endregion

# region Standard library imports
from scipy.integrate import quad                    # use quadrature integration
from scipy.interpolate import interp1d              # use one dimensional interpolation
from joblib import Parallel, delayed                # use parallel computing and function delaying
import matplotlib.pyplot as plt                     # use python plotting library
import numpy as np                                  # use Matlab like array notation
import copy
import os                                           # use system directory and file library
import argparse                                     # use command parameter parsing
import logging.config                               # use logging to file
# endregion
# endregion
__author__ = "marschall"


# region Setup parameters
# region Parser Arguments
# region ASGFEM parameter
parser = argparse.ArgumentParser(description="estimate the MC convergence against the TT-mean")
parser.add_argument("-re", "--refinements", type=int, default="5",
                    help="# of refinements in ASGFEM")
parser.add_argument("-sr", "--srank", type=int, default="10",
                    help="start rank")
parser.add_argument("-gd", "--gpcdegree", type=int, default="10",
                    help="maximal # of gpc degrees")
parser.add_argument("-fd", "--femdegree", type=int, default="1", choices=range(1, 4),
                    help="femdegree used in ASGFEM")
parser.add_argument("-md", "--max_dofs", type=float, default="1e6",
                    help="maximal # of dofs in ASGFEM")
parser.add_argument("-it", "--iterations", type=int, default="1000",
                    help="maximal # of iterations")
parser.add_argument("-ps", "--polysys", type=str, default="L", choices=["L"],
                    help="system of polynomials used in ASGFEM")
parser.add_argument("-dom", "--domain", type=str, default="square", choices=["square"],
                    help="domain used in ASGFEM")
parser.add_argument("-ct", "--coeffield-type", type=str, default="cos", choices=["cos"],
                    help="coefficient type")
parser.add_argument("-at", "--amp_type", type=str, default="constant", choices=["constant", "decay-inf"],
                    help="amplification function type")
parser.add_argument("-de", "--decay_exp_rate", type=float, default=0,
                    help="rate of decay in the coefficient field")
parser.add_argument("-fm", "--field_mean", type=float, default=2.0,
                    help="mean of the coefficient field")
parser.add_argument("-sg", "--sgfem_gamma", type=float, default=0.9,
                    help="value of gamma in the ASGFEM coefficient field")
parser.add_argument("-rv", "--rvtype", type=str, default="uniform", choices=["uniform"],
                    help="random variable type used in algorithm")
parser.add_argument("-imd", "--init_mesh_dofs", type=int, default=-1,
                    help="# of initial physical mesh dofs (-1: ignore)")
parser.add_argument("-tx", "--thetax", type=float, default=0.5,
                    help="factor of deterministic refinement (0: no refinement)")
parser.add_argument("-ty", "--thetay", type=float, default=0.5,
                    help="factor of stochastic refinement (0: no refinement)")
parser.add_argument("-nc", "--n_coefficients", type=int, default=5,
                    help="# of coefficients in the stochastic field approximation (aka M)")
# endregion
# region Observation parameter
parser.add_argument("-ns", "--n_samples_sqrt", type=int, default=3,
                    help="# of sample points in physical domain. (ns --> ns^2 points)")
parser.add_argument("-op", "--obs_precision", type=float, default=0.0,
                    help="precision used in TT.round in the observation process")
parser.add_argument("-or", "--obs_rank", type=int, default=500,
                    help="maximal rank used in TT.round in the observation process")
# endregion
# region Inner Product parameter
parser.add_argument("-ip", "--inp_precision", type=float, default=0.0,
                    help="precision used in TT.round in the calculation of the inner product")
parser.add_argument("-ir", "--inp_rank", type=int, default=500,
                    help="maximal rank used in TT.round in the calculation of the inner product")
parser.add_argument("-ig", "--inp_gamma", type=float, default=1e-3,
                    help="gamma. constant value of covariance operator")
# endregion
# region Exponential calculation parameter
parser.add_argument("-ep", "--euler_precision", type=float, default=0.0,
                    help="precision used in TT.round in the calculation of the exponential")
parser.add_argument("-er", "--euler_rank", type=int, default=50,
                    help="maximal rank used in TT.round in the calculation of the exponential")
parser.add_argument("-els", "--euler_local_mc_samples", type=int, default=1,
                    help="# of MC samples to check the convergence of each euler step (highly optional)")
parser.add_argument("-egs", "--euler_global_mc_samples", type=int, default=1000,
                    help="# of MC samples to check the convergence of the resulting exponential (highly optional)")
parser.add_argument("-erep", "--euler_repetitions", type=int, default=0,
                    help="# of repetitions of the euler procedure if the desired accuracy is not reached (only euler)")
parser.add_argument("-es", "--euler_steps", type=int, default=10000,
                    help="# of steps done in the iterative process. For adaptive only start value.")
parser.add_argument("-egp", "--euler_global_precision", type=float, default=1e-10,
                    help="desired accuracy to reach in the exponential approximation")
parser.add_argument("-eui", "--euler_use_implicit", action="store_true",
                    help="switch to use the implicit euler scheme for more stability.")
parser.add_argument("-rm", "--runge_method", type=str, default="forth", choices=["linear", "square", "forth"],
                    help="used embedded runge kutta method")
parser.add_argument("-rn", "--runge_min_stepsize", type=float, default=1e-19,
                    help="minimal step size to check in adaptive step size control")
parser.add_argument("-rx", "--runge_max_stepsize", type=float, default=1-1e-10,
                    help="maximal step size to check in adaptive step size control")
# endregion
# region Density parameter
parser.add_argument("-ng", "--n_eval_grid", type=int, default=50,
                    help="# of evaluation points to estimate densities (for plots only)")
# endregion
# region Stochastic collocation parameter
parser.add_argument("-ny", "--Ny", type=int, default=10,
                    help="# of collocation points in one stochastic dimension")
# endregion
# region File saving paths
parser.add_argument("-fp", "--file_path", type=str, default="results/paper/mean/",
                    help="path to store plots and results")
parser.add_argument("-bp", "--bayes_path", type=str, default="results/bayes/",
                    help="path to store the results of the bayesian procedure")
parser.add_argument("-pp", "--plot_path", type=str, default="results/bayes/",
                    help="path to store the plots of the bayesian procedure. incl the .dat for latex plots")
# endregion

# region special mean convergence parameter
parser.add_argument("-zmc", "--z_mc_samples", type=int, default=100,
                    help="# of mc_samples for the TT-Mean convergence")
parser.add_argument("-cpu", "--n_cpu", type=int, default=40,
                    help="# CPUs used for parallel computation")
parser.add_argument("-sol", "--sample_sol", action="store_true",
                    help="switch to use the solution sampling with -zmc samples.")
parser.add_argument("-mean", "--sample_mean", action="store_true",
                    help="switch to use the mean sampling with -zmc samples.")
parser.add_argument("-post", "--sample_post_mean", action="store_true",
                    help="switch to use the mean sampling of the solution w.r.t to the posterior with -zmc samples.")
parser.add_argument("-postDen", "--sample_post_den", action="store_true",
                    help="switch to use the posterior density sampling.")
parser.add_argument("-dens", "--sample_density", action="store_true",
                    help="switch to use the density interpolation with -ng grid points.")
parser.add_argument("-set", "--export_setting", action="store_true",
                    help="switch to export mesh and gpcdegrees")
parser.add_argument("-plot", "--do_plot", action="store_true",
                    help="switch to create plots and store them in the plot directory")
# endregion

parser.add_argument("-log", "--log", action="store_true",
                    help="switch to use the lognormal sgfem forward operator")

args = parser.parse_args()
print("========== PARAMETERS: {}".format(vars(args)))
# endregion
# region Store Parser Arguments
#   region Parameters to use in stochastic Galerkin for the forward solution operator
refinements = args.refinements 			            # # of refinement steps in sg
srank = args.srank					                # start ranks
gpcdegree = args.gpcdegree					        # maximal # of gpcdegrees
femdegree = args.femdegree  	                    # fem function degree
max_dofs = args.max_dofs                            # desired maximum of dofs to reach in refinement process
iterations = args.iterations                        # used iterations in tt ALS
polysys = args.polysys                              # used polynomial system
domain = args.domain                                # used domain, square, L-Shape, ...
coeffield_type = args.coeffield_type                # used coefficient field type
amp_type = args.amp_type                            # used amplification functions
decay_exp_rate = args.decay_exp_rate                # decay rate for non constant coefficient fields
field_mean = args.field_mean                        # mean value of the coefficient field
sgfem_gamma = args.sgfem_gamma                      # Gamma used in adaptive SG FEM
rvtype = args.rvtype                                # used random variable type
init_mesh_dofs = args.init_mesh_dofs                # initial mesh refinement. -1 : ignore
thetax = args.thetax                                # factor of deterministic refinement (0 : no refinement)
thetay = args.thetay                                # factor of stochastic refinement (0 : no refinement)
n_coefficients = args.n_coefficients                # number of coefficients (aka M)
#   endregion
# region Observation operator parameters

n_samples_sqrt = args.n_samples_sqrt                # number of nodes squared
sample_coordinates = [[i/int(n_samples_sqrt+1), j/int(n_samples_sqrt+1)]
                      for i in range(1, n_samples_sqrt+1) for j in range(1, n_samples_sqrt+1)]
print("sample_coordinates: {}".format(sample_coordinates))

obs_precision = args.obs_precision                  # precision used for rounding in the observation operator
obs_rank = args.obs_rank                            # maximal rank to round to in the observation operator
# endregion
# region Inner Product parameter
inp_precision = args.inp_precision                  # precision used in the calculation of the inner product
inp_rank = args.inp_rank                            # maximal rank used in the calculation of the inner product
inp_gamma = args.inp_gamma                          # covariance constant value
# endregion
# region Euler Parameter
euler_precision = args.euler_precision              # precision to use in euler rounding
euler_rank = args.euler_rank                        # rank to round down in euler method
#                                                   # samples in every euler step
euler_local_mc_samples = args.euler_local_mc_samples
#                                                   # samples for the final result
euler_global_mc_samples = args.euler_global_mc_samples
euler_repetitions = args.euler_repetitions          # number of repetitions with half step size
euler_steps = args.euler_steps                      # start euler steps
#                                                   # global precision to reach after the euler scheme
euler_global_precision = args.euler_global_precision
euler_use_implicit = args.euler_use_implicit        # switch to use the implicit euler method
runge_method = args.runge_method                    # name of the runge kutta method to use if to used method explicit
runge_min_stepsize = args.runge_min_stepsize        # minimal step size to check in adaptive step size control setup
runge_max_stepsize = args.runge_max_stepsize        # maximal step size to check in adaptive step size control setup
if euler_use_implicit:                              # use implicit Euler method
    euler_method = ImplicitEuler(euler_steps, euler_precision, euler_rank, euler_global_mc_samples, euler_repetitions,
                                 euler_local_mc_samples)
else:
    if False:                                       # use Rank adaptive explicit Runge Kutta (works fairly well)
        euler_method = RankAdaptiveEmbeddedRungeKutta(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
                                                      euler_repetitions, euler_local_mc_samples)
        euler_method.method = runge_method
        euler_method.acc = euler_global_precision
        euler_method.min_stepsize = runge_min_stepsize
        euler_method.max_stepsize = runge_max_stepsize
        euler_method.start_rank = 6
        euler_method.max_rank = 30
        euler_method.rounding_safety = 10.0
    else:
        #                                           # use Embedded explicit Runge Kutta
        euler_method = EmbeddedRungeKutta(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
                                          euler_repetitions, euler_local_mc_samples)
        euler_method.method = runge_method
        euler_method.acc = euler_global_precision
        euler_method.min_stepsize = runge_min_stepsize
        euler_method.max_stepsize = runge_max_stepsize
# endregion
# region Density parameters
n_eval_grid = args.n_eval_grid                      # number of evaluation points for the density plots
eval_grid_points = np.linspace(-1, 1, n_eval_grid)  # grid for density plots
# endregion
# region setup collocation
Ny = args.Ny                                        # # of collocation points in each stochastic dimension
#                                                   # one dimensional stochastic grid of Chebyshev nodes
stochastic_grid = np.array(get_cheb_points([Ny])[0])
# endregion

# region Logging
logging.config.fileConfig('logging.conf')           # load logger configuration
log = logging.getLogger('test_bayes')               # get new logger object
# endregion
# region Path check and creation
file_path = args.file_path
try:
    if not os.path.isdir(file_path):
        os.mkdir(file_path)
except OSError:
    log.error("can not create file path: {}".format(file_path))
    exit()

bayes_path = args.bayes_path
try:
    if not os.path.isdir(bayes_path):
        os.mkdir(bayes_path)
except OSError:
    log.error("can not create bayes path: {}".format(bayes_path))
    exit()

plot_path = args.plot_path
try:
    if not os.path.isdir(plot_path):
        os.mkdir(plot_path)
except OSError:
    log.error("Can not create plot path: {}".format(plot_path))
    exit()
# endregion
# region special mean convergence parameter
z_mc_samples = args.z_mc_samples
n_cpu = args.n_cpu
sample_sol = args.sample_sol
sample_mean = args.sample_mean
sample_post_mean = args.sample_post_mean
sample_density = args.sample_density
sample_post_density = args.sample_post_den
export_setting = args.export_setting
do_plot = args.do_plot
# endregion
log_normal = args.log
# endregion
# endregion

# region Object Initialisation
bayes_obj = None
coeff_field = None
proj_basis = None
true_values = None
prior_densities = None

bayes_fem_list = []
solution_error_h1_fem = []
solution_error_l2_fem = []
solution_error_h1_rel_fem = []
solution_error_l2_rel_fem = []
solution_tt_dofs_fem = []
solution_refinement_fem = []

mean_error_h1_fem = []
mean_error_h1_rel_fem = []
mean_error_l2_fem = []
mean_error_l2_rel_fem = []
mean_tt_dofs_fem = []
mean_refinements_fem = []

file_list_x = []
file_list_y = []
file_list_Z = []

poly_degree_p1 = []
poly_degree_p2 = []
poly_degree_p3 = []

fem_degrees = [2]
used_item = -1

mc_runs = 10
mc_run_mean = None
# endregion

# region Class CacheItem
class CacheItem(object):
    pass
# endregion

# if sample_post_density:
    # print("number of coeff: {}".format(n_coefficients))
    # assert(n_coefficients == 2)

for fem_degree in fem_degrees:
    bayes_obj_list = []                             # list of Bayes objects due to multiple refinements in forward sol
    z_mc_sample_list = []                           # list of mc samples
    x_achses = []                                   # x-achses list
    dofs = []                                       # we use the coordinate sampling
    # region Init Bayes Object
    if not log_normal:
        bayes_obj = ASGFEMBayes.init_solver(dofs, sample_coordinates, true_values, obs_precision,
                                            obs_rank, inp_precision, inp_rank, inp_gamma, euler_method,
                                            euler_global_precision, eval_grid_points, prior_densities,
                                            stochastic_grid,
                                            iterations=iterations, refinements=refinements, max_dofs=max_dofs,
                                            thetax=thetax,
                                            thetay=thetay, srank=srank,  m=n_coefficients+1,
                                            femdegree=fem_degree, gpcdegree=gpcdegree, polysys=polysys,
                                            gamma=sgfem_gamma, rvtype=rvtype, domain=domain,
                                            mesh_dofs=init_mesh_dofs, problem=0,
                                            coeffield_type=coeffield_type,
                                            amp_type=amp_type, sol_index=-1, field_mean=field_mean
                                            )
    else:
        bayes_obj = LognormalSGFEMBayes.init_solver(dofs, sample_coordinates, true_values, obs_precision,
                                                    obs_rank, inp_precision, inp_rank, inp_gamma, euler_method,
                                                    euler_global_precision, eval_grid_points, prior_densities,
                                                    stochastic_grid, maxrank=15, n_coeff=10, femdegree=1, gpcdegree=2,
                                                    decayexp=2, gamma=0.9, domain="square", mesh_refine=1, mesh_dofs=-1,
                                                    sol_rank=5, freq_skip=0, freq_scale=1.0, scale=1.0, theta=0.5,
                                                    rho=1.0, hermite_degree=8, als_iterations=1000,
                                                    convergence=1e-12, coeffield_type="EF-square-cos", _print=False,
                                                    field_mean=1.0)
    cache = CacheItem()
    # endregion

    for lia in range(len(bayes_obj.forwardOp.SOL)):
        # for lia in range(used_item, used_item+1):
        bayes_obj.set_sol_index(lia)
        # if fem_degree == 3:
        #     print "file path: {}".format(hash(bayes_obj.file_name))
        if bayes_obj.import_bayes(bayes_path):
            bayes_obj_list.append(copy.copy(bayes_obj))
        else:
            # region Setup truth
            if not log_normal:
                log.info("Create {} true parameters".format(bayes_obj.forwardOp.coefficients))
                true_values = create_parameter_samples(bayes_obj.forwardOp.coefficients)
                true_values = [0.25, -0.4, 0, 0.5, -0.1, 0.1]
            else:
                log.info("Create {} true parameters".format(bayes_obj.forwardOp.n_coeff))
                true_values = create_parameter_samples(bayes_obj.forwardOp.n_coeff, lognormal=True, mu=0, sigma=1)
            bayes_obj.true_values = true_values
            # endregion
            # region Setup Gaussian Prior
            Gauss_prior = False
            if log_normal:
                # !!!!!!!!!!!!!!!!!!!!!!!!!!!!
                prior_densities = [lambda x: 0.5 * np.ones_like(x)] * bayes_obj.forwardOp.n_coeff
            else:
                prior_densities = [lambda x: 0.5 * np.ones_like(x)] * bayes_obj.forwardOp.coefficients

            bayes_obj.prior_densities = prior_densities
            # endregion
            # region Setup noisy measurements
            log.info("Create {} measurements".format(len(sample_coordinates)))
            measurements = np.array([bayes_obj.forwardOp.compute_direct_sample_solution(true_values,
                                                                                        cache=cache)(sample_coordinate)
                                     for sample_coordinate in sample_coordinates])
            log.info("  add noise to the measurements")
            for lic in range(len(measurements)):
                measurements[lic] += np.random.randn(1, 1)[0, 0] * inp_gamma

            bayes_obj.measurements = measurements
            # endregion
            # region Calculate the Bayesian Posterior and Mean
            print("start calculation for SOL[{}] / {} with P{}".format(lia+1, len(bayes_obj.forwardOp.SOL), fem_degree))
            bayes_obj.set_sol_index(lia)
            if sample_sol:
                bayes_obj.calculate_densities(stop_at_observation=True)
            else:
                bayes_obj.calculate_densities(stop_at_observation=False)
            bayes_obj_list.append(copy.copy(bayes_obj))
            log.info('export Bayes file %s', bayes_path + bayes_obj.file_name + '.dat')
            if not sample_sol:
                if not bayes_obj.export_bayes(bayes_path):
                    log.error("can not save Bayes object")
            print("P{} solution.n: {}".format(fem_degree, bayes_obj.forwardOp.SOL[-1]["V"].n))
            print("P{} solution.r: {}".format(fem_degree, bayes_obj.forwardOp.SOL[-1]["V"].r))
            print("P{} dofs: {}".format(fem_degree, bayes_obj.forwardOp.SOL[-1]["DOFS"]))
            # endregion

    # region Export Settings to file
    if export_setting:
        with open(plot_path + "poly_degree_P{}.dat".format(fem_degree+1 + 2), "w") as f:
            f.write("dim, poly\n")
            for lia in range(len(bayes_obj_list[-1].forwardOp.SOL[-1]["gpcps"])):
                f.write(repr(lia+1) + "," + repr(bayes_obj_list[-1].forwardOp.SOL[-1]["gpcps"][lia]) + "\n")
        cg = bayes_obj_list[-1].forwardOp.SOL[-1]["CG"]
        # inject additional solution data

        from alea.utils.plothelper import plot_mpl
        mesh = cg.mesh()
        # export mesh pdf
        if i == 0:
            plt.figure(1)
            plt.gca().set_aspect('equal')
            plt.gca().xaxis.set_ticklabels([])
            plt.gca().yaxis.set_ticklabels([])
            #                plt.show(block=False)
        plt.clf()
        plot_mpl(mesh)
        #            plt.title('iteration %i' % i)
        plt.draw()
        plt.savefig(plot_path + "-mesh-P{}.pdf".format(fem_degree+1 + 2), dpi=None, facecolor='w',
                    edgecolor='w', orientation='portrait', papertype=None, format='pdf',
                    transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None)
    # endregion
    # region Z' sampling old version
    if sample_post_mean:
        mean_error_h1 = []
        mean_error_l2 = []
        mean_error_h1_rel = []
        mean_error_l2_rel = []
        refine_list = []
        tt_dofs = []
        # for lic in range(len(bayes_obj_list)):
        #     print "sol_index={}".format(bayes_obj_list[lic].sol_index)
        #     print "Bayes {}: {}".format(lic,bayes_obj_list[lic].solution)
        #     print "solution {}: {}".format(lic,bayes_obj_list[lic].forwardOp.SOL[lic]["V"])
        for lic in range(len(bayes_obj_list)):
            print("start posterior solution mean sampling for SOL[{}] / {} with P{}".format(lic+1, len(bayes_obj_list),
                                                                                            fem_degree))
            refine_list.append(lic)
            tt_dofs.append(bayes_obj.forwardOp.SOL[lic]["DOFS"])
            _mean_error_l2_rel, \
                _mean_error_h1_rel = bayes_obj_list[lic].estimate_posterior_solution_error(n_xi_samples=z_mc_samples,
                                                                                           n_z_xi_samples=z_mc_samples,
                                                                                           sol_index=used_item,
                                                                                           sol_index_ref=used_item,
                                                                                           relative=True)
            # _mean_error_l2, \
            # _mean_error_h1 = bayes_obj_list[lic].estimate_posterior_solution_error(n_xi_samples=z_mc_samples,
            #                                                                        n_z_xi_samples=z_mc_samples,
            #                                                                        sol_index=lic, relative=False)
            _mean_error_l2 = 10
            _mean_error_h1 = 10
            mean_error_h1.append(_mean_error_h1)
            mean_error_l2.append(_mean_error_l2)
            mean_error_h1_rel.append(_mean_error_h1_rel)
            mean_error_l2_rel.append(_mean_error_l2_rel)
        mean_error_h1_fem.append(mean_error_h1)
        mean_error_h1_rel_fem.append(mean_error_h1_rel)
        mean_error_l2_fem.append(mean_error_l2)
        mean_error_l2_rel_fem.append(mean_error_l2_rel)
        mean_tt_dofs_fem.append(tt_dofs)
        mean_refinements_fem.append(refine_list)
    # endregion
    # region Sample density
    if sample_density:
        if len(bayes_obj_list) > 0:
            poly_mean_list = []
            poly_mean_orig_list = []
            true_value_list = []
            mc_mean_list = []
            dens_interp_list = []
            tt_dofs = []
            max_length = 0
            for lia, item in enumerate(bayes_obj_list):
                assert isinstance(item, ASGFEMBayes)
                tt_dofs.append(item.forwardOp.SOL[lia]["DOFS"])
                poly_mean = []
                poly_mean_orig = []
                true_value = []
                # region Create density interpolation
                for i, yi in enumerate(item.eval_densities):
                    scaled_density = []
                    scaled_domain = []
                    curr_mean = 0
                    curr_mean_counter = 0
                    max_value = max(yi)
                    left_bound = yi[0]
                    right_bound = yi[-1]
                    if False:                       # max_value - 0.5 < 0.01:
                        poly_mean.append(0)
                    else:
                        if left_bound > right_bound:
                                                    # at least a bit curved
                            if max_value - left_bound > 1e-16:
                                curr_mean_counter = 0
                                mean = 0
                                for lic, entry in enumerate(yi):
                                    mean += entry
                                    if entry - left_bound >= 0:
                                                            # store only positive values
                                        scaled_density.append(entry - left_bound)
                                        # print "left_concave add density: {}".format(entry-left_bound)
                                        scaled_domain.append(item.eval_grid_points[lic])
                                        curr_mean += entry - left_bound
                                        curr_mean_counter += 1
                                # print("Saved Mean: {}".format(mean))
                                if curr_mean_counter > 4:
                                    curr_mean *= curr_mean_counter**(-1)
                                    scaled_density = np.array(scaled_density) * curr_mean**(-1)
                                    I = interp1d(scaled_domain, scaled_density, kind='cubic')
                                    poly_mean.append(quad(lambda _x: _x*I(_x), scaled_domain[0], scaled_domain[-1])[0])

                                    # print("transformed mean: {}".format(quad(lambda _x: I(_x), scaled_domain[0],
                                    #                                      scaled_domain[-1])))
                                    I = interp1d(bayes_obj.eval_grid_points, yi, kind='cubic')
                                    poly_mean_orig.append(quad(lambda _x: _x*I(_x), item.eval_grid_points[0],
                                                               item.eval_grid_points[-1])[0])

                                    # print("orig. mean: {}".format(quad(lambda _x: I(_x),
                                    #                                    bayes_obj.eval_grid_points[0],
                                    #                                    bayes_obj.eval_grid_points[-1])))
                                else:
                                    scaled_density = yi
                                    scaled_domain = item.eval_grid_points
                                    I = interp1d(scaled_domain, scaled_density, kind='cubic')
                                    poly_mean.append(quad(lambda _x: _x*I(_x), scaled_domain[0], scaled_domain[-1])[0])

                                    poly_mean_orig.append(quad(lambda _x: _x*I(_x), scaled_domain[0],
                                                               scaled_domain[-1])[0])

                            else:                           # not that strongly curved, no scaling
                                scaled_density = yi
                                scaled_domain = item.eval_grid_points
                                I = interp1d(scaled_domain, scaled_density, kind='cubic')
                                poly_mean.append(quad(lambda _x: _x*I(_x), scaled_domain[0], scaled_domain[-1])[0])

                                poly_mean_orig.append(quad(lambda _x: _x*I(_x), scaled_domain[0], scaled_domain[-1])[0])

                                # print("orig. mean: {}".format(quad(lambda _x: I(_x), scaled_domain[0],
                                #                                    scaled_domain[-1])))
                        else:                               # right bigger than left
                            if max_value - right_bound > 1e-16:
                                curr_mean_counter = 0
                                for lic, entry in enumerate(yi):
                                    if entry - right_bound >= 0:
                                                            # store only positive values
                                        scaled_density.append(entry - right_bound)
                                        # print "right_concave add density: {}".format(entry-right_bound)
                                        scaled_domain.append(item.eval_grid_points[lic])
                                        curr_mean += entry - right_bound
                                        curr_mean_counter += 1
                                if curr_mean_counter > 4:
                                    curr_mean *= curr_mean_counter**(-1)
                                    scaled_density = np.array(scaled_density) * curr_mean**(-1)
                                    I = interp1d(scaled_domain, scaled_density, kind='cubic')
                                    poly_mean.append(quad(lambda _x: _x*I(_x), scaled_domain[0], scaled_domain[-1])[0])

                                    # print("transformed mean: {}".format(quad(lambda _x: I(_x), scaled_domain[0],
                                    #                                     scaled_domain[-1])))
                                    I = interp1d(item.eval_grid_points, yi, kind='cubic')
                                    poly_mean_orig.append(quad(lambda _x: _x*I(_x), item.eval_grid_points[0],
                                                               item.eval_grid_points[-1])[0])

                                    # print("orig. mean: {}".format(quad(lambda _x: I(_x),
                                    #                                   item.eval_grid_points[0],
                                    #                                   item.eval_grid_points[-1])))
                                else:
                                    scaled_density = yi
                                    scaled_domain = item.eval_grid_points
                                    I = interp1d(scaled_domain, scaled_density, kind='cubic')
                                    poly_mean.append(quad(lambda _x: _x*I(_x), scaled_domain[0], scaled_domain[-1])[0])

                                    poly_mean_orig.append(quad(lambda _x: _x*I(_x), scaled_domain[0],
                                                               scaled_domain[-1])[0])

                            else:
                                scaled_density = yi
                                scaled_domain = item.eval_grid_points
                                I = interp1d(scaled_domain, scaled_density, kind='cubic')
                                poly_mean.append(quad(lambda _x: _x*I(_x), scaled_domain[0], scaled_domain[-1])[0])

                                poly_mean_orig.append(quad(lambda _x: _x*I(_x), scaled_domain[0], scaled_domain[-1])[0])

                    print("poly_mean: {} vs. true_value: {} vs. poly_mean_orig: {}".format(poly_mean[-1],
                                                                                           item.true_values[i],
                                                                                           poly_mean_orig[-1]))
                    true_value.append(item.true_values[i])
                    if do_plot:
                        plt.plot(item.eval_grid_points, yi,
                                 label="y_{0} should {1}".format(i, item.true_values[i]))
                        plt.plot(scaled_domain, scaled_density, '--',
                                 label="scaled y_{0} should {1}".format(i, item.true_values[i]))
                        plt.xlim([-1, 1])
                        plt.vlines(item.true_values[i], 0, max(scaled_density))
                # endregion
                #                                   # store results of every density for this refinement in list
                poly_mean_list.append(poly_mean)
                poly_mean_orig_list.append(poly_mean_orig)
                true_value_list.append(true_value)
                if len(true_value) > max_length:
                    max_length = len(true_value)
                # region Create Samples in parameter space
                xi_samples = np.array([np.random.rand(item.forwardOp.coefficients+1, 1)[:, 0]*2 - 1
                                       for lic in range(z_mc_samples)])
                for lic in range(len(xi_samples)):
                    xi_samples[lic][0] = 0          # set first component to zero. Just for convenience
                # endregion
                # region sample Z with given parameter realisations

                def _sample_dens_z_parallel_tt(xi):
                    sol_buffer = item.forwardOp.sample_at_coeff(list(xi[1:]))
                    sol_measurements = np.array([sol_buffer(loc_dof) for loc_dof in item.sample_coordinates])
                    # print("     (O o G)(u) at {0} = {1}".format(bayes_obj.sample_coordinates, sol_measurements))
                    diff = np.array(item.measurements) - np.array(sol_measurements)
                    # print("     given measurements: {}".format(bayes_obj.measurements))
                    _Z = np.exp(-0.5 * np.sum((diff ** 2) * (inp_gamma ** (-1))))
                    return _Z
                print("start TT sampling for Z with {} MC samples".format(z_mc_samples))
                z = np.sum(Parallel(n_cpu)(delayed(_sample_dens_z_parallel_tt)(xi) for xi in xi_samples))
                z *= len(xi_samples)**(-1)
                z *= 0.5**(len(item.solution.n)-1)
                print("Sampled Z: {} vs calculated Z: {}".format(z, item.mean))
                # endregion

                # region Sample coefficients with interpolation
                dens_interp = []
                for lib in range(len(bayes_obj.solution.n)-1):
                    mc_mean_interp = []
                    for lic in range(n_eval_grid):
                        print("  ref {3}/{4}: sample coeff {0}/{1} for x={2}".format(lib, len(bayes_obj.solution.n)-1,
                                                                                     eval_grid_points[lic], lia,
                                                                                     len(bayes_obj_list)))

                        def _sample_dens_interp_parallel_tt(xi):
                            xi_test = np.array(xi)
                            xi_test[lib+1] = eval_grid_points[lic]
                            sol_buffer = item.forwardOp.sample_at_coeff(list(xi_test[1:]))
                            sol_measurements = np.array([sol_buffer(loc_dof)
                                                         for loc_dof in item.sample_coordinates])
                            diff = np.array(item.measurements) - np.array(sol_measurements)
                            _Z = np.exp(-0.5 * np.sum((diff ** 2) * (inp_gamma ** (-1))))
                            return _Z
                        mc_mean_interp.append(np.sum(Parallel(n_cpu)(delayed(_sample_dens_interp_parallel_tt)
                                                                            (xi) for xi in xi_samples)))
                        mc_mean_interp[-1] *= len(xi_samples)**(-1)
                        mc_mean_interp[-1] *= z**(-1)
                        mc_mean_interp[-1] *= 0.5**(len(item.solution.n)-2)
                    I = interp1d(eval_grid_points, np.array(mc_mean_interp), kind='cubic')
                    dens_interp.append(quad(lambda _x: _x*I(_x), eval_grid_points[0], eval_grid_points[-1])[0])
                dens_interp_list.append(dens_interp)
                # endregion
                # region Plot densities to file
                if do_plot:
                    plt.draw()
                    plt.savefig(file_path + "IND{}_".format(lia) +
                                str(hash(item.file_name)) + "_dens.pdf", dpi=None, facecolor='w', edgecolor='w',
                                orientation='portrait', papertype=None, format='pdf',
                                transparent=False, bbox_inches='tight', pad_inches=0.1,
                                frameon=None)
                    plt.clf()
                # endregion
            with open(plot_path + "dens_P{}".format(fem_degree), "w") as f:
                delimiter = ", "
                line = ["true_{0}, orig_{0}, transf_{0}, mc_{0}, errOrig_{0}, errTransf_{0}, errOrigMc_{0}, "
                        "errTransfMc_{0}".format(lic)
                        for lic in range(max_length)]
                header = delimiter.join(line)
                f.write(header + "\n")
                assert len(true_value_list) == len(poly_mean_list)
                assert len(true_value_list) == len(poly_mean_orig_list)
                # assert len(true_value_list) == len(mc_mean_list)
                assert len(true_value_list) == len(dens_interp_list)
                for lia in range(len(true_value_list)):
                    line = ""
                    for lic in range(max_length):
                        assert len(true_value_list[lia]) == len(poly_mean_list[lia])
                        assert len(true_value_list[lia]) == len(poly_mean_orig_list[lia])
                        # assert len(true_value_list[lia]) == len(mc_mean_list[lia])
                        assert len(true_value_list[lia]) == len(dens_interp_list[lia])
                        if lic < max_length - 1:
                            if lic < len(true_value_list[lia]):
                                line += "{}, {}, {}, {}, {}, {}, {}, {}, ".format(true_value_list[lia][lic],
                                                                                  poly_mean_orig_list[lia][lic],
                                                                                  poly_mean_list[lia][lic],
                                                                                  dens_interp_list[lia][lic],
                                                                                  np.abs(true_value_list[lia][lic] -
                                                                                         poly_mean_orig_list[lia][lic]),
                                                                                  np.abs(true_value_list[lia][lic] -
                                                                                         poly_mean_list[lia][lic]),
                                                                                  np.abs(poly_mean_orig_list[lia][lic] -
                                                                                         dens_interp_list[lia][lic]),
                                                                                  np.abs(poly_mean_list[lia][lic] -
                                                                                         dens_interp_list[lia][lic]))
                            else:
                                line += "{}, {}, {}, {}, {},  ".format(0, 0, 0, 0, 0)
                        else:
                            if lic < len(true_value_list[lia]):
                                line += "{}, {}, {}, {}, {}, {}, {}, {}\n".format(true_value_list[lia][lic],
                                                                                  poly_mean_orig_list[lia][lic],
                                                                                  poly_mean_list[lia][lic],
                                                                                  dens_interp_list[lia][lic],
                                                                                  np.abs(true_value_list[lia][lic] -
                                                                                         poly_mean_orig_list[lia][lic]),
                                                                                  np.abs(true_value_list[lia][lic] -
                                                                                         poly_mean_list[lia][lic]),
                                                                                  np.abs(poly_mean_orig_list[lia][lic] -
                                                                                         dens_interp_list[lia][lic]),
                                                                                  np.abs(poly_mean_list[lia][lic] -
                                                                                         dens_interp_list[lia][lic]))
                            else:
                                line += "{}, {}, {}, {}, {}, \n".format(0, 0, 0, 0, 0)
                    f.write(line)

    # endregion
    # region Sample posterior density
    if sample_post_density:
        if log_normal:
            raise NotImplemented("Posterior density sampling not implemented for log-normal case")
        nx = 15
        y_1 = np.linspace(-1., 1., nx)
        y_2 = np.linspace(-1., 1., nx)
        Y1, Y2 = np.meshgrid(y_1, y_2)

        # FE solution on design points
        post_den = np.zeros((Y1.shape[0], Y1.shape[1]))
        post_true_surrogate = np.zeros((Y1.shape[0], Y1.shape[1]))
        post_true = np.zeros((Y1.shape[0], Y1.shape[1]))
        from alea.application.bayes.Bayes_util.lib.bayes_lib import evaluate

        def true_post_surrogate(y):
            sol_buffer = bayes_obj.forwardOp.sample_at_coeff(list(y), sol_index=used_item)
            sol_measurements = np.array([sol_buffer(loc_dof) for loc_dof in bayes_obj.sample_coordinates])
            # print("     (O o G)(u) at {0} = {1}".format(bayes_obj.sample_coordinates, sol_measurements))
            diff = np.array(bayes_obj.measurements) - np.array(sol_measurements)
            # print("     given measurements: {}".format(bayes_obj.measurements))
            _Z = np.exp(-0.5 * np.sum((diff ** 2) * (bayes_obj.inp_gamma ** (-1))))
            return _Z

        def true_post(y):
            sol_buffer = bayes_obj.forwardOp.compute_direct_sample_solution(list(y), sol_index=used_item)
            sol_measurements = np.array([sol_buffer(loc_dof) for loc_dof in bayes_obj.sample_coordinates])
            # print("     (O o G)(u) at {0} = {1}".format(bayes_obj.sample_coordinates, sol_measurements))
            diff = np.array(bayes_obj.measurements) - np.array(sol_measurements)
            # print("     given measurements: {}".format(bayes_obj.measurements))
            _Z = np.exp(-0.5 * np.sum((diff ** 2) * (bayes_obj.inp_gamma ** (-1))))
            return _Z
        true_values = bayes_obj.true_values
        for (lia, _y_1) in enumerate(y_1):
            for (lic, _y_2) in enumerate(y_2):
                coordinate = [_y_1, _y_2] + [buf for buf in true_values[2:]]
                post_den[lia, lic] = evaluate(bayes_obj.solution, _poly='Lagrange', _samples=coordinate)
                post_true_surrogate[lia, lic] = true_post_surrogate(coordinate)
                post_true[lia, lic] = true_post(coordinate)
        from mpl_toolkits.mplot3d import Axes3D
        plt.figure()
        CS = plt.contour(Y1, Y2, post_den)
        plt.clabel(CS, inline=1, fontsize=10)
        plt.title("true values ({}, {})".format(true_values[0], true_values[1]))
        plt.xlabel("y_2")
        plt.ylabel("y_1")
        plt.savefig(plot_path + "y1_contour_K{}_gamma{}.png".format(len(sample_coordinates), inp_gamma))
        plt.figure()
        CS = plt.contour(Y1, Y2, post_true_surrogate)
        plt.clabel(CS, inline=1, fontsize=10)
        plt.title("true values ({}, {})".format(true_values[0], true_values[1]))
        plt.xlabel("y_2")
        plt.ylabel("y_1")
        plt.savefig(plot_path + "y1_surrogate_contour_K{}_gamma{}.png".format(len(sample_coordinates), inp_gamma))
        CS = plt.contour(Y1, Y2, post_true)
        plt.clabel(CS, inline=1, fontsize=10)
        plt.title("true values ({}, {})".format(true_values[0], true_values[1]))
        plt.xlabel("y_2")
        plt.ylabel("y_1")
        plt.savefig(plot_path + "y1_true_contour_K{}_gamma{}.png".format(len(sample_coordinates), inp_gamma))
        plt.figure()
        CS = plt.contour(Y1, Y2, post_true-post_den)
        plt.clabel(CS, inline=1, fontsize=10)
        plt.xlabel("y_2")
        plt.ylabel("y_1")
        plt.title("Error: {}".format(np.linalg.norm(post_true-post_den)))
        plt.savefig(plot_path + "y1_err_contour_K{}_gamma{}.png".format(len(sample_coordinates), inp_gamma))

        if len(true_values) < 4:
            exit()
        for (lia, _y_1) in enumerate(y_1):
            for (lic, _y_2) in enumerate(y_2):
                coordinate = [buf for buf in true_values[:2]] + [_y_1, _y_2] + [buf for buf in true_values[4:]]
                post_den[lia, lic] = evaluate(bayes_obj.solution, _poly='Lagrange', _samples=coordinate)
                post_true_surrogate[lia, lic] = true_post_surrogate(coordinate)
                post_true[lia, lic] = true_post(coordinate)
        plt.figure()
        CS = plt.contour(Y1, Y2, post_den)
        plt.clabel(CS, inline=1, fontsize=10)
        plt.title("true values ({}, {})".format(true_values[2], true_values[3]))
        plt.xlabel("y_4")
        plt.ylabel("y_3")
        plt.savefig(plot_path + "y3_contour_K{}_gamma{}.png".format(len(sample_coordinates), inp_gamma))
        plt.figure()
        CS = plt.contour(Y1, Y2, post_true_surrogate)
        plt.clabel(CS, inline=1, fontsize=10)
        plt.title("true values ({}, {})".format(true_values[2], true_values[3]))
        plt.xlabel("y_4")
        plt.ylabel("y_3")
        plt.savefig(plot_path + "y3_surrogate_contour_K{}_gamma{}.png".format(len(sample_coordinates), inp_gamma))
        CS = plt.contour(Y1, Y2, post_true)
        plt.clabel(CS, inline=1, fontsize=10)
        plt.title("true values ({}, {})".format(true_values[2], true_values[3]))
        plt.xlabel("y_4")
        plt.ylabel("y_3")
        plt.savefig(plot_path + "y3_true_contour_K{}_gamma{}.png".format(len(sample_coordinates), inp_gamma))
        plt.figure()
        CS = plt.contour(Y1, Y2, post_true - post_den)
        plt.clabel(CS, inline=1, fontsize=10)
        plt.xlabel("y_4")
        plt.ylabel("y_3")
        plt.title("Error: {}".format(np.linalg.norm(post_true - post_den)))
        plt.savefig(plot_path + "y3_err_contour_K{}_gamma{}.png".format(len(sample_coordinates), inp_gamma))
        if len(true_values) < 6:
            exit()
        for (lia, _y_1) in enumerate(y_1):
            for (lic, _y_2) in enumerate(y_2):
                coordinate = [buf for buf in true_values[:4]] + [_y_1, _y_2]
                post_den[lia, lic] = evaluate(bayes_obj.solution, _poly='Lagrange', _samples=coordinate)
                post_true_surrogate[lia, lic] = true_post_surrogate(coordinate)
                post_true[lia, lic] = true_post(coordinate)
        plt.figure()
        CS = plt.contour(Y1, Y2, post_den)
        plt.clabel(CS, inline=1, fontsize=10)
        plt.title("true values ({}, {})".format(true_values[4], true_values[5]))
        plt.xlabel("y_6")
        plt.ylabel("y_5")
        plt.savefig(plot_path + "y5_contour_K{}_gamma{}.png".format(len(sample_coordinates), inp_gamma))
        plt.figure()
        CS = plt.contour(Y1, Y2, post_true_surrogate)
        plt.clabel(CS, inline=1, fontsize=10)
        plt.title("true values ({}, {})".format(true_values[4], true_values[5]))
        plt.xlabel("y_6")
        plt.ylabel("y_5")
        plt.savefig(plot_path + "y5_surrogate_contour_K{}_gamma{}.png".format(len(sample_coordinates), inp_gamma))
        plt.figure()
        CS = plt.contour(Y1, Y2, post_true)
        plt.clabel(CS, inline=1, fontsize=10)
        plt.title("true values ({}, {})".format(true_values[4], true_values[5]))
        plt.xlabel("y_6")
        plt.ylabel("y_5")
        plt.savefig(plot_path + "y5_true_contour_K{}_gamma{}.png".format(len(sample_coordinates), inp_gamma))
        plt.figure()
        CS = plt.contour(Y1, Y2, post_true - post_den)
        plt.clabel(CS, inline=1, fontsize=10)
        plt.xlabel("y_6")
        plt.ylabel("y_5")
        plt.title("Error: {}".format(np.linalg.norm(post_true - post_den)))
        plt.savefig(plot_path + "y5_err_contour_K{}_gamma{}.png".format(len(sample_coordinates), inp_gamma))
    # endregion
    # region Sample current forward solution error
    if sample_sol:

        if len(bayes_obj.forwardOp.SOL) > 0:
            solution_error_h1 = []
            solution_error_l2 = []
            solution_error_h1_rel = []
            solution_error_l2_rel = []
            refine_list = []
            tt_dofs = []
            print("estimate solution error")
            for lic in range(len(bayes_obj.forwardOp.SOL)):
                print("sample SOL[{}] / {} for P{} FEM".format(lic, len(bayes_obj.forwardOp.SOL), fem_degree))
                refine_list.append(lic)
                tt_dofs.append(bayes_obj.forwardOp.SOL[lic]["DOFS"])
                # print("SOL.dofs: {}".format(bayes_obj.forwardOp.SOL[lic]["DOFS"]))
                if not log_normal:
                    _solution_error_l2, _solution_error_h1 = \
                        bayes_obj.forwardOp.estimate_sampling_error(sol_index=lic, relative=False,
                                                                    n_xi_samples=z_mc_samples)
                    _solution_error_l2_rel, _solution_error_h1_rel = \
                        bayes_obj.forwardOp.estimate_sampling_error(sol_index=lic, relative=True,
                                                                    n_xi_samples=z_mc_samples)
                else:
                    xi_samples = np.array([np.random.randn(bayes_obj.forwardOp.coefficients) for
                                           _ in range(z_mc_samples)])
                    _solution_error_l2, _solution_error_h1 = \
                        bayes_obj.forwardOp.estimate_sampling_error(sol_index=lic, relative=False,
                                                                    n_xi_samples=z_mc_samples, xi_samples=xi_samples)
                    _solution_error_l2_rel, _solution_error_h1_rel = \
                        bayes_obj.forwardOp.estimate_sampling_error(sol_index=lic, relative=True,
                                                                    n_xi_samples=z_mc_samples, xi_samples=xi_samples)

                __solution_error_l2 = 0
                __solution_error_h1 = 0
                __solution_error_l2_rel = 0
                __solution_error_h1_rel = 0
                for lix in range(len(_solution_error_h1)):
                    __solution_error_l2 += _solution_error_l2[lix]
                    __solution_error_h1 += _solution_error_h1[lix]
                    __solution_error_l2_rel += _solution_error_l2_rel[lix]
                    __solution_error_h1_rel += _solution_error_h1_rel[lix]
                __solution_error_l2 /= len(_solution_error_l2)
                __solution_error_h1 /= len(_solution_error_h1)
                __solution_error_l2_rel /= len(_solution_error_l2_rel)
                __solution_error_h1_rel /= len(_solution_error_h1_rel)
                solution_error_l2.append(__solution_error_l2)
                solution_error_h1.append(__solution_error_h1)
                solution_error_l2_rel.append(__solution_error_l2_rel)
                solution_error_h1_rel.append(__solution_error_h1_rel)

            solution_error_h1_fem.append(solution_error_h1)
            solution_error_l2_fem.append(solution_error_l2)
            solution_error_h1_rel_fem.append(solution_error_h1_rel)
            solution_error_l2_rel_fem.append(solution_error_l2_rel)
            solution_tt_dofs_fem.append(tt_dofs)
            solution_refinement_fem.append(refine_list)

    # endregion
    # region Sample error of normalization constant
    if sample_mean:
        mc_run_mean = []
        mc_run_err = []
        mc_run_err_rel = []
        # region Do the sampling
        for lib in range(mc_runs):
            print("start mean sampling for P{} with run {}/{}".format(fem_degree, lib+1, mc_runs))
            z_mc_sample_list = []
            if True:
                bayes_obj = copy.copy(bayes_obj_list[-1])
                print("P{} len(gpcdegrees): {}".format(fem_degree, len(bayes_obj.forwardOp.SOL[used_item]["gpcps"]
                                                                       [-1])))
                print("P{} gpcdegrees: {}".format(fem_degree, bayes_obj.forwardOp.SOL[used_item]["gpcps"][-1]))
                print("P{} solution.n: {}".format(fem_degree, bayes_obj.forwardOp.SOL[used_item]["V"].n))
                print("P{} solution.r: {}".format(fem_degree, bayes_obj.forwardOp.SOL[used_item]["V"].r))
                print("P{} dofs: {}".format(fem_degree, bayes_obj.forwardOp.SOL[used_item]["DOFS"]))
                print("coefficients from solution: {}".format(bayes_obj.solution.n))

                cg = bayes_obj.forwardOp.SOL[used_item]["CG"]
                # inject additional solution data

                from alea.utils.plothelper import plot_mpl
                mesh = cg.mesh()
                # export mesh pdf
                if i == 0:
                    plt.figure(1)
                    plt.gca().set_aspect('equal')
                    plt.gca().xaxis.set_ticklabels([])
                    plt.gca().yaxis.set_ticklabels([])
                    #                plt.show(block=False)
                plt.clf()
                plot_mpl(mesh)
                #            plt.title('iteration %i' % i)
                plt.draw()
                plt.savefig(plot_path + "mesh-P{}_item{}.pdf".format(fem_degree, used_item), dpi=None, facecolor='w',
                            edgecolor='w', orientation='portrait', papertype=None, format='pdf',
                            transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None)
                # continue

                from Bayes_util.lib.bayes_lib import evaluate_marginal
                print("recalculate mean")
                bayes_obj.mean = evaluate_marginal(
                    bayes_obj.solution, 0,
                    _samples=[0]*(len(bayes_obj.solution.n)-1))*0.5**(len(bayes_obj.solution.n)-1)
                print("recalculate mean Done")
            # region Create samples in parameter space
            xi_samples = np.array([np.array([np.random.rand(len(bayes_obj.solution.n)-1, 1)[:, 0]*2 - 1 for _
                                   in range(2**(lic+1))]) for lic in range(z_mc_samples)])
            # endregion
            # region sample Z

            def _sample_z_parallel_tt(xi):
                sol_buffer = bayes_obj.forwardOp.sample_at_coeff(list(xi), sol_index=used_item)
                sol_measurements = np.array([sol_buffer(loc_dof) for loc_dof in bayes_obj.sample_coordinates])
                # print("     (O o G)(u) at {0} = {1}".format(bayes_obj.sample_coordinates, sol_measurements))
                diff = np.array(bayes_obj.measurements) - np.array(sol_measurements)
                # print("     given measurements: {}".format(bayes_obj.measurements))
                _Z = np.exp(-0.5 * np.sum((diff ** 2) * (bayes_obj.inp_gamma ** (-1))))
                return _Z

            print("start TT sampling with {} steps".format(z_mc_samples))
            for lic in range(z_mc_samples):
                print("calculate TT-Z with {} samples".format(len(xi_samples[lic])))
                z_mc_sample_list.append(np.sum(Parallel(n_cpu)(delayed(_sample_z_parallel_tt)(xi)
                                                               for xi in xi_samples[lic])))
                # for lib in range(len(z_mc_sample_list[-1])):
                #    print("sample:{} yields {}".format(xi_samples[lic][lib], z_mc_sample_list[-1][lib]))
                # z_mc_sample_list[-1] = np.sum(z_mc_sample_list[-1])
                z_mc_sample_list[-1] *= len(xi_samples[lic])**(-1)
                x_achses.append(2**(lic+1))
            Z_poly = bayes_obj.mean
            # endregion
            # region Plot the results
            if do_plot:
                plt.plot(np.array(x_achses), np.array(z_mc_sample_list), label="MC P{}-TT".format(fem_degree))
                plt.plot(np.array(x_achses), [Z_poly]*len(x_achses), label="TT-Mean P{}".format(fem_degree))
                # plt.plot(np.array(x_achses), [Z_TT]*len(x_achses), label="TT-Mean Test")
                plt.title("Development of the Mean sampled with MC vs our TT-Mean.")
                plt.legend(ncol=3, loc=1)
                plt.xlabel("number of MC samples")
                plt.ylabel("Mean")
                plt.savefig(plot_path + str(hash(bayes_obj.file_name)) + "mean_P{}.png".format(fem_degree))
                plt.clf()
            # endregion
            bayes_fem_list.append(bayes_obj_list)
            file_list_x.append(x_achses)                # # of mc samples (list[# mc samples])
            file_list_y.append(z_mc_sample_list)        # sampled Z (list[# mc samples])
            file_list_Z.append(Z_poly)                  # calculated Z (float)
        # endregion
        mc_run_mean.append(file_list_y)
    # endregion
# region Export mean error to file
if sample_mean:
    iter_ = 0
    print("shape of y = {}, should be ({},{},{})".format(np.array(mc_run_mean).shape, 1, mc_runs, z_mc_samples))
    for fem_degree in fem_degrees:
        with open(plot_path + "mean_error_P{}_item{}_log2.dat".format(fem_degree, used_item), "w") as f:
            f.write("MC_samples, tt_mean")
            for lia in range(mc_runs):
                f.write(", sample_mean_{}, err_{}, err_rel_{}".format(lia, lia, lia))
            f.write("\n")
            # assert (len(file_list_x[iter]) == len(file_list_y[iter]))
            for lic in range(z_mc_samples):
                f.write(repr(file_list_x[iter_][lic]) + "," +
                        repr(file_list_Z[iter_]))
                for lia in range(mc_runs):
                    f.write("," + repr(mc_run_mean[iter_][lia][lic]) +
                            "," + repr(np.abs(mc_run_mean[iter_][lia][lic]-file_list_Z[iter_])) +
                            "," + repr(np.abs(mc_run_mean[iter_][lia][lic]-file_list_Z[iter_])/file_list_Z[iter_]))
                f.write("\n")
        iter += 1

# endregion

# region Export solution error to file
if sample_sol:
    # assert len(solution_refinement_fem) == 3
    # assert len(solution_tt_dofs_fem) == 3
    # assert len(solution_error_l2_rel_fem) == 3
    # assert len(solution_error_l2_fem) == 3
    # assert len(solution_error_h1_rel_fem) == 3
    # assert len(solution_error_h1_fem) == 3
    for fem_degree in range(1):
        with open(plot_path + "inter_sol_error_P{}.dat".format(fem_degree+1), "w") as f:
            f.write("refine, tt_dof, L2, H1, L2_rel, H1_rel\n")
            for lia in range(len(solution_refinement_fem[fem_degree])):
                assert len(solution_refinement_fem[fem_degree]) == len(solution_tt_dofs_fem[fem_degree])
                assert len(solution_refinement_fem[fem_degree]) == len(solution_error_l2_fem[fem_degree])
                assert len(solution_refinement_fem[fem_degree]) == len(solution_error_h1_fem[fem_degree])
                assert len(solution_refinement_fem[fem_degree]) == len(solution_error_l2_rel_fem[fem_degree])
                assert len(solution_refinement_fem[fem_degree]) == len(solution_error_h1_rel_fem[fem_degree])
                f.write(repr(solution_refinement_fem[fem_degree][lia]) + "," +
                        repr(solution_tt_dofs_fem[fem_degree][lia]) + "," +
                        repr(solution_error_l2_fem[fem_degree][lia]) + "," +
                        repr(solution_error_h1_fem[fem_degree][lia]) + "," +
                        repr(solution_error_l2_rel_fem[fem_degree][lia]) + "," +
                        repr(solution_error_h1_rel_fem[fem_degree][lia]) + "\n")
# endregion
# region Export posterior solution mean error to file
if sample_post_mean:
    # assert len(mean_refinements_fem) == 3
    # assert len(mean_tt_dofs_fem) == 3
    # assert len(mean_error_l2_rel_fem) == 3
    # assert len(mean_error_l2_fem) == 3
    # assert len(mean_error_h1_rel_fem) == 3
    # assert len(mean_error_h1_fem) == 3
    index = 0
    for fem_degree in fem_degrees:
        with open(plot_path + "post_mean_error_P{}.dat".format(fem_degree), "w") as f:
            f.write("refine, tt_dof, L2, H1, L2_rel, H1_rel\n")
            for lia in range(len(mean_refinements_fem[index])):
                assert len(mean_refinements_fem[index]) == len(mean_tt_dofs_fem[index])
                assert len(mean_refinements_fem[index]) == len(mean_error_l2_fem[index])
                assert len(mean_refinements_fem[index]) == len(mean_error_h1_fem[index])
                assert len(mean_refinements_fem[index]) == len(mean_error_l2_rel_fem[index])
                assert len(mean_refinements_fem[index]) == len(mean_error_h1_rel_fem[index])
                f.write(repr(mean_refinements_fem[index][lia]) + "," +
                        repr(mean_tt_dofs_fem[index][lia]) + "," +
                        repr(mean_error_l2_fem[index][lia]) + "," +
                        repr(mean_error_h1_fem[index][lia]) + "," +
                        repr(mean_error_l2_rel_fem[index][lia]) + "," +
                        repr(mean_error_h1_rel_fem[index][lia]) + "\n")
# endregion

# region Plot results
if do_plot:
    # region estimate reference solution by taking the last computed element
    def _sample_z_parallel(xi):
        sol_buffer = bayes_fem_list[-1][-1].forwardOp.compute_direct_sample_solution(list(xi[1:]), sol_index=-1)
        sol_measurements = np.array([sol_buffer(loc_dof) for loc_dof in bayes_obj.sample_coordinates])
        # print("     (O o G)(u) at {0} = {1}".format(bayes_obj.sample_coordinates, sol_measurements))
        diff = np.array(bayes_obj.measurements) - np.array(sol_measurements)
        # print("     given measurements: {}".format(bayes_obj.measurements))
        _Z = np.exp(-0.5 * np.sum((diff ** 2) * (inp_gamma ** (-1))))
        return _Z

    xi_samples = np.array([np.random.rand(bayes_obj.forwardOp.coefficients + 1, 1)[:, 0] * 2 - 1 for __
                           in range(z_mc_samples)])
    for lix in range(len(xi_samples)):
        xi_samples[lix][0] = 0

    z_true = np.sum(np.array(Parallel(n_cpu)(delayed(_sample_z_parallel)(xi) for xi in xi_samples)))
    z_true *= 0.5 ** (len(bayes_obj.solution.n) - 1)
    z_true *= len(xi_samples) ** (-1)
    # endregion

    # region Plot error of Z
    for lia in range(len(bayes_fem_list)):
        print("Bayes FEM {} / {}".format(lia, len(bayes_fem_list)))
        x_achses = []
        z_error = []
        for lic in range(len(bayes_fem_list[lia])):
            print("  Bayes Obj {} / {}".format(lic, len(bayes_fem_list[lia])))

            x_achses.append(bayes_fem_list[lia][lic].forwardOp.SOL[lic]["DOFS"])
            z_error.append(np.abs(z_true - bayes_fem_list[lia][lic].mean))
        plt.loglog(np.array(x_achses), np.array(z_error), label="P{}".format(lia))

    plt.title("Development of the Mean sampled with MC vs our TT-Mean.")
    plt.legend(ncol=3, loc=1)
    plt.xlabel("TT-Dofs")
    plt.ylabel("$\|Z-Z_h\|$")
    plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "mean_error.png")
    plt.clf()
    # endregion

    # region Plot solution errors against tt_dofs
    fem_marker = ["o", "^", "d"]

    for lia in range(3):                                # fem degrees P = 1,2,3
        plt.semilogy(solution_refinement_fem[lia], np.array(solution_error_l2_fem[lia]), "b", marker=fem_marker[lia],
                     label="$L^2$ error p{}" + str(lia+1))
        plt.semilogy(solution_refinement_fem[lia], np.array(solution_error_h1_fem[lia]), "r", marker=fem_marker[lia],
                     label="$H^1_0$ error p{}" + str(lia+1))
    plt.title("Sampled ERROR of the forward solution.")
    plt.legend(bbox_to_anchor=(0., -.202, 1., .102), loc=2,
               ncol=3, mode="expand", borderaxespad=0.)
    plt.xlabel("adaptive refinement step")
    plt.ylabel("Sampled ERROR of the forward solution object")
    plt.savefig(file_path + "sol_error_absolute_" + str(hash(bayes_obj.file_name)) + ".pdf",
                dpi=200, facecolor='w', edgecolor='w',
                orientation='portrait', papertype=None, format='pdf',
                transparent=False, bbox_inches='tight', pad_inches=0.1,
                frameon=None)
    plt.clf()
    for lia in range(3):                                # fem degrees P = 1,2,3
        plt.loglog(solution_tt_dofs_fem[lia], np.array(solution_error_l2_fem[lia]), "b", marker=fem_marker[lia],
                   label="$L^2$ error p{}".format(lia+1))
        plt.loglog(solution_tt_dofs_fem[lia], np.array(solution_error_h1_fem[lia]), "r", marker=fem_marker[lia],
                   label="$H^1_0$ error p{}".format(lia+1))
    plt.title("Sampled ERROR of the forward solution.")
    plt.legend(bbox_to_anchor=(0., -.202, 1., .102), loc=2,
               ncol=3, mode="expand", borderaxespad=0.)
    plt.xlabel("TT-dofs")
    plt.ylabel("Sampled ERROR of the forward solution object")
    plt.savefig(file_path + "sol_error_absolute_tt_" + str(hash(bayes_obj.file_name)) + ".pdf",
                dpi=200, facecolor='w', edgecolor='w',
                orientation='portrait', papertype=None, format='pdf',
                transparent=False, bbox_inches='tight', pad_inches=0.1,
                frameon=None)
    plt.clf()
    for lia in range(3):                                # fem degrees P = 1,2,3
        plt.semilogy(solution_refinement_fem[lia], np.array(solution_error_l2_rel_fem[lia]), "b",
                     marker=fem_marker[lia], label="$L^2$ error p{}".format(lia+1))
        plt.semilogy(solution_refinement_fem[lia], np.array(solution_error_h1_rel_fem[lia]), "r",
                     marker=fem_marker[lia], label="$H^1_0$ error p{}".format(lia+1))
    plt.title("Sampled relative ERROR of the forward solution.")
    plt.legend(bbox_to_anchor=(0., -.202, 1., .102), loc=2,
               ncol=3, mode="expand", borderaxespad=0.)
    plt.xlabel("adaptive refinement step")
    plt.ylabel("Sampled relative ERROR of the forward solution object")
    plt.savefig(file_path + "sol_error_relative_" + str(hash(bayes_obj.file_name)) + ".pdf",
                dpi=200, facecolor='w', edgecolor='w',
                orientation='portrait', papertype=None, format='pdf',
                transparent=False, bbox_inches='tight', pad_inches=0.1,
                frameon=None)
    plt.clf()
    for lia in range(3):                                # fem degrees P = 1,2,3
        plt.loglog(solution_tt_dofs_fem[lia], np.array(solution_error_l2_rel_fem[lia]), "b", marker=fem_marker[lia],
                   label="$L^2$ error p{}".format(lia+1))
        plt.loglog(solution_tt_dofs_fem[lia], np.array(solution_error_h1_rel_fem[lia]), "r", marker=fem_marker[lia],
                   label="$H^1_0$ error p{}".format(lia+1))
    plt.title("Sampled relative ERROR of the forward solution.")
    plt.legend(bbox_to_anchor=(0., -.202, 1., .102), loc=2,
               ncol=3, mode="expand", borderaxespad=0.)
    plt.xlabel("tt-dofs")
    plt.ylabel("Sampled relative ERROR of the forward solution object")
    plt.savefig(file_path + "sol_error_relative_tt_" + str(hash(bayes_obj.file_name)) + ".pdf",
                dpi=200, facecolor='w', edgecolor='w',
                orientation='portrait', papertype=None, format='pdf',
                transparent=False, bbox_inches='tight', pad_inches=0.1,
                frameon=None)
    plt.clf()
    # endregion
# endregion
