# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  sample TT-tensor Bayesian inversion results in view of normalization constant (mean), density and solution errors
"""
# region Imports
# region Standard library imports
from __future__ import division                     # use float division without dot
import os                                           # use system directory and file library
import argparse                                     # use command parameter parsing
import logging.config                               # use logging to file
import matplotlib                                   # use plot library
matplotlib.use('Agg')                               # use 'Agg' extension to use in cluster environment
import matplotlib.pyplot as plt                     # use python plotting library
from scipy.integrate import quad                    # use quadrature integration
from scipy.interpolate import interp1d              # use one dimensional interpolation
from joblib import Parallel, delayed                # use parallel computing and function delaying
import numpy as np                                  # use Matlab like array notation
# endregion
# region Bayes libraries
from Bayes_util.lib.bayes_lib import create_parameter_samples
from Bayes_util.exponential.implicit_euler import ImplicitEuler

from Bayes_util.asgfem_bayes import ASGFEMBayes
from Bayes_util.exponential.embedded_runge_kutta import EmbeddedRungeKutta
from Bayes_util.lib.interpolation_util import get_cheb_points
# endregion
# endregion
__author__ = "marschall"

# region Setup parameters
# region Parser Arguments
# region ASGFEM parameter
parser = argparse.ArgumentParser(description="estimate the MC convergence against the TT-mean")
parser.add_argument("-re", "--refinements", type=int, default="5",
                    help="# of refinements in ASGFEM")
parser.add_argument("-sr", "--srank", type=int, default="10",
                    help="start rank")
parser.add_argument("-gd", "--gpcdegree", type=int, default="10",
                    help="maximal # of gpc degrees")
parser.add_argument("-fd", "--femdegree", type=int, default="1", choices=range(1, 4),
                    help="femdegree used in ASGFEM")
parser.add_argument("-md", "--max_dofs", type=float, default="1e6",
                    help="maximal # of dofs in ASGFEM")
parser.add_argument("-it", "--iterations", type=int, default="1000",
                    help="maximal # of iterations")
parser.add_argument("-ps", "--polysys", type=str, default="L", choices=["L"],
                    help="system of polynomials used in ASGFEM")
parser.add_argument("-dom", "--domain", type=str, default="square", choices=["square"],
                    help="domain used in ASGFEM")
parser.add_argument("-ct", "--coeffield-type", type=str, default="cos", choices=["cos"],
                    help="coefficient type")
parser.add_argument("-at", "--amp_type", type=str, default="constant", choices=["constant", "decay-inf"],
                    help="amplification function type")
parser.add_argument("-de", "--decay_exp_rate", type=float, default=0,
                    help="rate of decay in the coefficient field")
parser.add_argument("-fm", "--field_mean", type=float, default=2.0,
                    help="mean of the coefficient field")
parser.add_argument("-sg", "--sgfem_gamma", type=float, default=0.9,
                    help="value of gamma in the ASGFEM coefficient field")
parser.add_argument("-rv", "--rvtype", type=str, default="uniform", choices=["uniform"],
                    help="random variable type used in algorithm")
parser.add_argument("-imd", "--init_mesh_dofs", type=int, default=-1,
                    help="# of initial physical mesh dofs (-1: ignore)")
parser.add_argument("-tx", "--thetax", type=float, default=0.5,
                    help="factor of deterministic refinement (0: no refinement)")
parser.add_argument("-ty", "--thetay", type=float, default=0.5,
                    help="factor of stochastic refinement (0: no refinement)")
parser.add_argument("-nc", "--n_coefficients", type=int, default=5,
                    help="# of coefficients in the stochastic field approximation (aka M)")
# endregion
# region Observation parameter
parser.add_argument("-ns", "--n_samples_sqrt", type=int, default=3,
                    help="# of sample points in physical domain. (ns --> ns^2 points)")
parser.add_argument("-op", "--obs_precision", type=float, default=1e-15,
                    help="precision used in TT.round in the observation process")
parser.add_argument("-or", "--obs_rank", type=int, default=500,
                    help="maximal rank used in TT.round in the observation process")
# endregion
# region Inner Product parameter
parser.add_argument("-ip", "--inp_precision", type=float, default=1e-15,
                    help="precision used in TT.round in the calculation of the inner product")
parser.add_argument("-ir", "--inp_rank", type=int, default=500,
                    help="maximal rank used in TT.round in the calculation of the inner product")
parser.add_argument("-ig", "--inp_gamma", type=float, default=1e-3,
                    help="gamma. constant value of covariance operator")
# endregion
# region Exponential calculation parameter
parser.add_argument("-ep", "--euler_precision", type=float, default=1e-10,
                    help="precision used in TT.round in the calculation of the exponential")
parser.add_argument("-er", "--euler_rank", type=int, default=50,
                    help="maximal rank used in TT.round in the calculation of the exponential")
parser.add_argument("-els", "--euler_local_mc_samples", type=int, default=1,
                    help="# of MC samples to check the convergence of each euler step (highly optional)")
parser.add_argument("-egs", "--euler_global_mc_samples", type=int, default=1000,
                    help="# of MC samples to check the convergence of the resulting exponential (highly optional)")
parser.add_argument("-erep", "--euler_repetitions", type=int, default=0,
                    help="# of repetitions of the euler procedure if the desired accuracy is not reached (only euler)")
parser.add_argument("-es", "--euler_steps", type=int, default=1000,
                    help="# of steps done in the iterative process. For adaptive only start value.")
parser.add_argument("-egp", "--euler_global_precision", type=float, default=1e-5,
                    help="desired accuracy to reach in the exponential approximation")
parser.add_argument("-eui", "--euler_use_implicit", action="store_true",
                    help="switch to use the implicit euler scheme for more stability.")
parser.add_argument("-rm", "--runge_method", type=str, default="forth", choices=["linear", "square", "forth"],
                    help="used embedded runge kutta method")
parser.add_argument("-rn", "--runge_min_stepsize", type=float, default=1e-19,
                    help="minimal step size to check in adaptive step size control")
parser.add_argument("-rx", "--runge_max_stepsize", type=float, default=1-1e-10,
                    help="maximal step size to check in adaptive step size control")
# endregion
# region Density parameter
parser.add_argument("-ng", "--n_eval_grid", type=int, default=50,
                    help="# of evaluation points to estimate densities (for plots only)")
# endregion
# region Stochastic collocation parameter
parser.add_argument("-ny", "--Ny", type=int, default=10,
                    help="# of collocation points in one stochastic dimension")
# endregion
# region File saving paths
parser.add_argument("-fp", "--file_path", type=str, default="results/paper/mean/",
                    help="path to store plots and results")
parser.add_argument("-bp", "--bayes_path", type=str, default="results/bayes/",
                    help="path to store the results of the bayesian procedure")
parser.add_argument("-pp", "--plot_path", type=str, default="results/bayes/",
                    help="path to store the plots of the bayesian procedure. incl the .dat for latex plots")
# endregion

# region special coef convergence parameter
parser.add_argument("-zmc", "--z_mc_samples", type=int, default=100,
                    help="# of mc_samples for the TT-Mean convergence")
parser.add_argument("-mref", "--m_ref", type=int, default=3,
                    help="# of quadrature refinements (2*(x+1))")
parser.add_argument("-cpu", "--n_cpu", type=int, default=40,
                    help="# CPUs used for parallel computation")
parser.add_argument("-plot", "--do_plot", action="store_true",
                    help="switch to create plots and store them in the plot directory")
# endregion
args = parser.parse_args()
print("========== PARAMETERS: {}".format(vars(args)))
# endregion
# region Store Parser Arguments
#   region Parameters to use in stochastic Galerkin for the forward solution operator
refinements = args.refinements 			            # # of refinement steps in sg
srank = args.srank					                # start ranks
gpcdegree = args.gpcdegree					        # maximal # of gpcdegrees
femdegree = args.femdegree  	                    # fem function degree
max_dofs = args.max_dofs                            # desired maximum of dofs to reach in refinement process
iterations = args.iterations                        # used iterations in tt ALS
polysys = args.polysys                              # used polynomial system
domain = args.domain                                # used domain, square, L-Shape, ...
coeffield_type = args.coeffield_type                # used coefficient field type
amp_type = args.amp_type                            # used amplification functions
decay_exp_rate = args.decay_exp_rate                # decay rate for non constant coefficient fields
field_mean = args.field_mean                        # mean value of the coefficient field
sgfem_gamma = args.sgfem_gamma                      # Gamma used in adaptive SG FEM
rvtype = args.rvtype                                # used random variable type
init_mesh_dofs = args.init_mesh_dofs                # initial mesh refinement. -1 : ignore
thetax = args.thetax                                # factor of deterministic refinement (0 : no refinement)
thetay = args.thetay                                # factor of stochastic refinement (0 : no refinement)
n_coefficients = args.n_coefficients                # number of coefficients (aka M)
#   endregion
# region Observation operator parameters

n_samples_sqrt = args.n_samples_sqrt                # number of nodes squared
sample_coordinates = [[i/int(n_samples_sqrt+1), j/int(n_samples_sqrt+1)]
                      for i in range(1, n_samples_sqrt+1) for j in range(1, n_samples_sqrt+1)]
print("sample_coordinates: {}".format(sample_coordinates))

obs_precision = args.obs_precision                  # precision used for rounding in the observation operator
obs_rank = args.obs_rank                            # maximal rank to round to in the observation operator
# endregion
# region Inner Product parameter
inp_precision = args.inp_precision                  # precision used in the calculation of the inner product
inp_rank = args.inp_rank                            # maximal rank used in the calculation of the inner product
inp_gamma = args.inp_gamma                          # covariance constant value
# endregion
# region Euler Parameter
euler_precision = args.euler_precision              # precision to use in euler rounding
euler_rank = args.euler_rank                        # rank to round down in euler method
#                                                   # samples in every euler step
euler_local_mc_samples = args.euler_local_mc_samples
#                                                   # samples for the final result
euler_global_mc_samples = args.euler_global_mc_samples
euler_repetitions = args.euler_repetitions          # number of repetitions with half step size
euler_steps = args.euler_steps                      # start euler steps
#                                                   # global precision to reach after the euler scheme
euler_global_precision = args.euler_global_precision
euler_use_implicit = args.euler_use_implicit        # switch to use the implicit euler method
runge_method = args.runge_method                    # name of the runge kutta method to use if to used method explicit
runge_min_stepsize = args.runge_min_stepsize        # minimal step size to check in adaptive step size control setup
runge_max_stepsize = args.runge_max_stepsize        # maximal step size to check in adaptive step size control setup
if euler_use_implicit:
    euler_method = ImplicitEuler(euler_steps, euler_precision, euler_rank, euler_global_mc_samples, euler_repetitions,
                                 euler_local_mc_samples)
else:
    euler_method = EmbeddedRungeKutta(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
                                      euler_repetitions, euler_local_mc_samples)
    euler_method.method = runge_method
    euler_method.acc = euler_global_precision
    euler_method.min_stepsize = runge_min_stepsize
    euler_method.max_stepsize = runge_max_stepsize
# endregion -
# region Density parameters
n_eval_grid = args.n_eval_grid                      # number of evaluation points for the density plots
eval_grid_points = np.linspace(-1, 1, n_eval_grid)  # grid for density plots
# endregion
# region setup collocation
Ny = args.Ny                                        # # of collocation points in each stochastic dimension
quadrature_refinements = args.m_ref
stochastic_grid = np.array([np.array(get_cheb_points([lia] * n_coefficients)[0])
                            for lia in range(2, quadrature_refinements+2)])
#                                                   # one dimensional stochastic grid of Chebyshev nodes
# endregion

# region Logging
logging.config.fileConfig('logging.conf')           # load logger configuration
log = logging.getLogger('test_bayes')               # get new logger object
# endregion
# region Path check and creation
file_path = args.file_path
try:
    if not os.path.isdir(file_path):
        os.mkdir(file_path)
except OSError:
    log.error("can not create file path: {}".format(file_path))
    exit()

bayes_path = args.bayes_path
try:
    if not os.path.isdir(bayes_path):
        os.mkdir(bayes_path)
except OSError:
    log.error("can not create bayes path: {}".format(bayes_path))
    exit()

plot_path = args.plot_path
try:
    if not os.path.isdir(plot_path):
        os.mkdir(plot_path)
except OSError:
    log.error("Can not create plot path: {}".format(plot_path))
    exit()
# endregion
# region special mean convergence parameter
z_mc_samples = args.z_mc_samples
n_cpu = args.n_cpu
do_plot = args.do_plot
# endregion
# endregion
# endregion

# region Object Initialisation
bayes_obj = None
coeff_field = None
proj_basis = None
prior_densities = None

bayes_fem_list = []
solution_error_h1_fem = []
solution_error_l2_fem = []
solution_error_h1_rel_fem = []
solution_error_l2_rel_fem = []
solution_tt_dofs_fem = []
solution_refinement_fem = []
file_list_x = []
file_list_y = []
file_list_Z = []
# endregion

log.info("Create {} true parameters".format(40))
true_values = create_parameter_samples(40)
for fem_degree in range(1, 2):
    bayes_obj_list = []                             # list of Bayes objects due to multiple refinements in forward sol
    for quad_refine in range(quadrature_refinements):
        dofs = []                                   # we use the coordinate sampling
        # region Init Bayes Object
        bayes_obj = ASGFEMBayes.init_solver(dofs, sample_coordinates, true_values, obs_precision,
                                            obs_rank, inp_precision, inp_rank, inp_gamma, euler_method,
                                            euler_global_precision, eval_grid_points, prior_densities,
                                            stochastic_grid[quad_refine],
                                            iterations=iterations, refinements=refinements, max_dofs=max_dofs,
                                            thetax=thetax,
                                            thetay=thetay, srank=srank,  m=n_coefficients+1,
                                            femdegree=fem_degree, gpcdegree=gpcdegree, polysys=polysys,
                                            gamma=sgfem_gamma, rvtype=rvtype, domain=domain,
                                            mesh_dofs=init_mesh_dofs, problem=0,
                                            coeffield_type=coeffield_type,
                                            amp_type=amp_type, sol_index=-1, field_mean=field_mean
                                            )
        # endregion

        if bayes_obj.import_bayes(bayes_path):
            bayes_obj_list.append(bayes_obj)
        else:
            # region Setup truth
            bayes_obj.true_values = true_values[:bayes_obj.forwardOp.coefficients]
            # endregion
            # region Setup Gaussian Prior
            Gauss_prior = False
            if Gauss_prior:
                raise NotImplemented
            else:
                prior_densities = [lambda x: 0.5 * np.ones_like(x)] * bayes_obj.forwardOp.coefficients

            bayes_obj.prior_densities = prior_densities
            # endregion
            # region Setup noisy measurements
            measurements = np.array([bayes_obj.forwardOp.compute_direct_sample_solution(true_values)
                                     (sample_coordinate) for sample_coordinate in sample_coordinates])
            for lic in range(len(measurements)):
                measurements[lic] += np.random.randn(1, 1)[0, 0] * inp_gamma

            bayes_obj.measurements = measurements
            # endregion
            bayes_obj.calculate_densities()
            bayes_obj_list.append(bayes_obj)
            log.info('export Bayes file %s', bayes_path + bayes_obj.file_name + '.dat')
            if not bayes_obj.export_bayes(bayes_path):
                log.error("can not save bayes object")

    print("Bayes obj loaded from file")
    # region Sample density
    poly_mean_list = []
    poly_mean_orig_list = []
    true_value_list = []
    mc_mean_list = []
    dens_interp_list = []
    tt_dofs = []
    max_length = 0
    for lia, item in enumerate(bayes_obj_list):
        assert isinstance(item, ASGFEMBayes)
        tt_dofs.append(len(item.stoch_grid)*(len(item.solution.n)-1))
        poly_mean = []
        poly_mean_orig = []
        true_value = []
        # region Create density interpolation
        for i, yi in enumerate(item.eval_densities):
            scaled_density = []
            scaled_domain = []
            curr_mean = 0
            curr_mean_counter = 0
            max_value = max(yi)
            left_bound = yi[0]
            right_bound = yi[-1]
            if False:                       # max_value - 0.5 < 0.01:
                poly_mean.append(0)
            else:
                if left_bound > right_bound:
                                            # at least a bit curved
                    if max_value - left_bound > 1e-16:
                        curr_mean_counter = 0
                        mean = 0
                        for lic, entry in enumerate(yi):
                            mean += entry
                            if entry - left_bound >= 0:
                                                    # store only positive values
                                scaled_density.append(entry - left_bound)
                                # print "left_concave add density: {}".format(entry-left_bound)
                                scaled_domain.append(item.eval_grid_points[lic])
                                curr_mean += entry - left_bound
                                curr_mean_counter += 1
                        # print("Saved Mean: {}".format(mean))
                        if curr_mean_counter > 4:
                            curr_mean *= curr_mean_counter**(-1)
                            scaled_density = np.array(scaled_density) * curr_mean**(-1)
                            I = interp1d(scaled_domain, scaled_density, kind='cubic')
                            poly_mean.append(quad(lambda _x: _x*I(_x), scaled_domain[0], scaled_domain[-1])[0])

                            # print("transformed mean: {}".format(quad(lambda _x: I(_x), scaled_domain[0],
                            #                                      scaled_domain[-1])))
                            I = interp1d(bayes_obj.eval_grid_points, yi, kind='cubic')
                            poly_mean_orig.append(quad(lambda _x: _x*I(_x), item.eval_grid_points[0],
                                                       item.eval_grid_points[-1])[0])

                            # print("orig. mean: {}".format(quad(lambda _x: I(_x),
                            #                                    bayes_obj.eval_grid_points[0],
                            #                                    bayes_obj.eval_grid_points[-1])))
                        else:
                            scaled_density = yi
                            scaled_domain = item.eval_grid_points
                            I = interp1d(scaled_domain, scaled_density, kind='cubic')
                            poly_mean.append(quad(lambda _x: _x*I(_x), scaled_domain[0], scaled_domain[-1])[0])

                            poly_mean_orig.append(quad(lambda _x: _x*I(_x), scaled_domain[0],
                                                       scaled_domain[-1])[0])

                    else:                           # not that strongly curved, no scaling
                        scaled_density = yi
                        scaled_domain = item.eval_grid_points
                        I = interp1d(scaled_domain, scaled_density, kind='cubic')
                        poly_mean.append(quad(lambda _x: _x*I(_x), scaled_domain[0], scaled_domain[-1])[0])

                        poly_mean_orig.append(quad(lambda _x: _x*I(_x), scaled_domain[0], scaled_domain[-1])[0])

                        # print("orig. mean: {}".format(quad(lambda _x: I(_x), scaled_domain[0],
                        #                                    scaled_domain[-1])))
                else:                               # right bigger than left
                    if max_value - right_bound > 1e-16:
                        curr_mean_counter = 0
                        for lic, entry in enumerate(yi):
                            if entry - right_bound >= 0:
                                                    # store only positive values
                                scaled_density.append(entry - right_bound)
                                # print "right_concave add density: {}".format(entry-right_bound)
                                scaled_domain.append(item.eval_grid_points[lic])
                                curr_mean += entry - right_bound
                                curr_mean_counter += 1
                        if curr_mean_counter > 4:
                            curr_mean *= curr_mean_counter**(-1)
                            scaled_density = np.array(scaled_density) * curr_mean**(-1)
                            I = interp1d(scaled_domain, scaled_density, kind='cubic')
                            poly_mean.append(quad(lambda _x: _x*I(_x), scaled_domain[0], scaled_domain[-1])[0])

                            # print("transformed mean: {}".format(quad(lambda _x: I(_x), scaled_domain[0],
                            #                                     scaled_domain[-1])))
                            I = interp1d(item.eval_grid_points, yi, kind='cubic')
                            poly_mean_orig.append(quad(lambda _x: _x*I(_x), item.eval_grid_points[0],
                                                       item.eval_grid_points[-1])[0])

                            # print("orig. mean: {}".format(quad(lambda _x: I(_x),
                            #                                   item.eval_grid_points[0],
                            #                                   item.eval_grid_points[-1])))
                        else:
                            scaled_density = yi
                            scaled_domain = item.eval_grid_points
                            I = interp1d(scaled_domain, scaled_density, kind='cubic')
                            poly_mean.append(quad(lambda _x: _x*I(_x), scaled_domain[0], scaled_domain[-1])[0])

                            poly_mean_orig.append(quad(lambda _x: _x*I(_x), scaled_domain[0],
                                                       scaled_domain[-1])[0])

                    else:
                        scaled_density = yi
                        scaled_domain = item.eval_grid_points
                        I = interp1d(scaled_domain, scaled_density, kind='cubic')
                        poly_mean.append(quad(lambda _x: _x*I(_x), scaled_domain[0], scaled_domain[-1])[0])

                        poly_mean_orig.append(quad(lambda _x: _x*I(_x), scaled_domain[0], scaled_domain[-1])[0])

            print("poly_mean: {} vs. true_value: {} vs. poly_mean_orig: {}".format(poly_mean[-1],
                                                                                   item.true_values[i],
                                                                                   poly_mean_orig[-1]))
            true_value.append(item.true_values[i])
            if do_plot:
                plt.plot(item.eval_grid_points, yi,
                         label="y_{0} should {1}".format(i, item.true_values[i]))
                plt.plot(scaled_domain, scaled_density, '--',
                         label="scaled y_{0} should {1}".format(i, item.true_values[i]))
                plt.xlim([-1, 1])
                plt.vlines(item.true_values[i], 0, max(scaled_density))
        # endregion
        #                                   # store results of every density for this refinement in list
        poly_mean_list.append(poly_mean)
        poly_mean_orig_list.append(poly_mean_orig)
        true_value_list.append(true_value)
        if len(true_value) > max_length:
            max_length = len(true_value)
        # region Create Samples in parameter space
        xi_samples = np.array([np.random.rand(item.forwardOp.coefficients+1, 1)[:, 0]*2 - 1
                               for lic in range(z_mc_samples)])
        for lic in range(len(xi_samples)):
            xi_samples[lic][0] = 0          # set first component to zero. Just for convenience
        # endregion
        # region sample Z with given parameter realisations

        def _sample_dens_z_parallel_tt(xi):
            sol_buffer = item.forwardOp.sample_at_coeff(list(xi[1:]))
            sol_measurements = np.array([sol_buffer(loc_dof) for loc_dof in item.sample_coordinates])
            # print("     (O o G)(u) at {0} = {1}".format(bayes_obj.sample_coordinates, sol_measurements))
            diff = np.array(item.measurements) - np.array(sol_measurements)
            # print("     given measurements: {}".format(bayes_obj.measurements))
            _Z = np.exp(-0.5 * np.sum((diff ** 2) * (inp_gamma ** (-1))))
            return _Z
        print("start TT sampling for Z with {} MC samples".format(z_mc_samples))
        z = np.sum(Parallel(n_cpu)(delayed(_sample_dens_z_parallel_tt)(xi) for xi in xi_samples))
        z *= len(xi_samples)**(-1)
        z *= 0.5**(len(item.solution.n)-1)
        print("Sampled Z: {} vs calculated Z: {}".format(z, item.mean))
        # endregion

        # region Sample coefficients with interpolation
        dens_interp = []
        for lib in range(len(bayes_obj.solution.n)-1):
            mc_mean_interp = []
            for lic in range(n_eval_grid):
                print("  ref {3}/{4}: sample coeff {0}/{1} for x={2}".format(lib+1, len(bayes_obj.solution.n)-1,
                                                                             eval_grid_points[lic], lia+1,
                                                                             len(bayes_obj_list)))

                def _sample_dens_interp_parallel_tt(xi):
                    xi_test = np.array(xi)
                    xi_test[lib+1] = eval_grid_points[lic]
                    sol_buffer = item.forwardOp.sample_at_coeff(list(xi_test[1:]))
                    sol_measurements = np.array([sol_buffer(loc_dof)
                                                 for loc_dof in item.sample_coordinates])
                    diff = np.array(item.measurements) - np.array(sol_measurements)
                    _Z = np.exp(-0.5 * np.sum((diff ** 2) * (inp_gamma ** (-1))))
                    return _Z
                mc_mean_interp.append(np.sum(Parallel(n_cpu)(delayed(_sample_dens_interp_parallel_tt)
                                                                    (xi) for xi in xi_samples)))
                mc_mean_interp[-1] *= len(xi_samples)**(-1)
                mc_mean_interp[-1] *= z**(-1)
                mc_mean_interp[-1] *= 0.5**(len(item.solution.n)-2)
            I = interp1d(eval_grid_points, np.array(mc_mean_interp), kind='cubic')
            dens_interp.append(quad(lambda _x: _x*I(_x), eval_grid_points[0], eval_grid_points[-1])[0])
        dens_interp_list.append(dens_interp)
        # endregion
        # region Plot densities to file
        if do_plot:
            plt.draw()
            plt.savefig(file_path + "IND{}_".format(lia) +
                        str(hash(item.file_name)) + "_dens.pdf", dpi=None, facecolor='w', edgecolor='w',
                        orientation='portrait', papertype=None, format='pdf',
                        transparent=False, bbox_inches='tight', pad_inches=0.1,
                        frameon=None)
            plt.clf()
        # endregion
    with open(plot_path + "col_error_dens_P{}.dat".format(fem_degree), "w") as f:
        delimiter = ", "
        line = ["tt_col_dofs_{0}, true_{0}, orig_{0}, transf_{0}, mc_{0}, errOrig_{0}, errTransf_{0}, errOrigMc_{0}, "
                "errTransfMc_{0}".format(lic)
                for lic in range(max_length)]
        header = delimiter.join(line)
        f.write(header + "\n")
        assert len(true_value_list) == len(poly_mean_list)
        assert len(true_value_list) == len(poly_mean_orig_list)
        # assert len(true_value_list) == len(mc_mean_list)
        assert len(true_value_list) == len(dens_interp_list)
        for lia in range(len(true_value_list)):
            line = ""
            for lic in range(max_length):
                assert len(true_value_list[lia]) == len(poly_mean_list[lia])
                assert len(true_value_list[lia]) == len(poly_mean_orig_list[lia])
                # assert len(true_value_list[lia]) == len(mc_mean_list[lia])
                assert len(true_value_list[lia]) == len(dens_interp_list[lia])
                if lic < max_length - 1:
                    if lic < len(true_value_list[lia]):
                        line += "{}, {}, {}, {}, {}, {}, {}, {}, {}, ".format(tt_dofs[lia],
                                                                              true_value_list[lia][lic],
                                                                              poly_mean_orig_list[lia][lic],
                                                                              poly_mean_list[lia][lic],
                                                                              dens_interp_list[lia][lic],
                                                                              np.abs(true_value_list[lia][lic] -
                                                                                     poly_mean_orig_list[lia][lic]),
                                                                              np.abs(true_value_list[lia][lic] -
                                                                                     poly_mean_list[lia][lic]),
                                                                              np.abs(poly_mean_orig_list[lia][lic] -
                                                                                     dens_interp_list[lia][lic]),
                                                                              np.abs(poly_mean_list[lia][lic] -
                                                                                     dens_interp_list[lia][lic]))
                    else:
                        line += "{}, {}, {}, {}, {}, {}, {}, {}, {}, ".format(0, 0, 0, 0, 0, 0)
                else:
                    if lic < len(true_value_list[lia]):
                        line += "{}, {}, {}, {}, {}, {}, {}, {}, {}\n".format(tt_dofs[lia],
                                                                              true_value_list[lia][lic],
                                                                              poly_mean_orig_list[lia][lic],
                                                                              poly_mean_list[lia][lic],
                                                                              dens_interp_list[lia][lic],
                                                                              np.abs(true_value_list[lia][lic] -
                                                                                     poly_mean_orig_list[lia][lic]),
                                                                              np.abs(true_value_list[lia][lic] -
                                                                                     poly_mean_list[lia][lic]),
                                                                              np.abs(poly_mean_orig_list[lia][lic] -
                                                                                     dens_interp_list[lia][lic]),
                                                                              np.abs(poly_mean_list[lia][lic] -
                                                                                     dens_interp_list[lia][lic]))
                    else:
                        line += "{}, {}, {}, {}, {}, {}, {}, {}, {}\n".format(0, 0, 0, 0, 0, 0)
            f.write(line)

        # endregion
