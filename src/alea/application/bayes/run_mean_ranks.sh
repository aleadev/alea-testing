#!/bin/bash
python paper_conv_mean_rank.py -re 1000 -md 150000 -sr 2 -gd 1 -nc 2 -at decay-inf -fm 2 -de 2 -fp results/paper/mean/decay-inf/alt3/ -imd 10 -pp ../paper-tt-bayesian-inversion/results/decay-inf/big/testMean/ -mean -zmc 1000 -cpu 50 -ny 8 #&
#python paper_conv_mean.py -re 1000 -md 50000 -sr 2 -gd 1 -nc 2 -at decay-inf -fm 2 -de 2 -fp results/paper/mean/decay-inf/alt/ -imd 10 -pp ../paper-tt-bayesian-inversion/results/decay-inf/big/testSol2/ -set -zmc 100 -cpu 1 -ir 10 -or 10 -er 10 -ny 8 #&
#python paper_conv_mean.py -re 20 -sr 2 -gd 1 -nc 2 -at decay-inf -fm 2 -de 2 -fp results/paper/mean/decay-inf/ -imd 10 -pp ../paper-tt-bayesian-inversion/results/decay-inf/big/ -dens -zmc 2000 -cpu 40

