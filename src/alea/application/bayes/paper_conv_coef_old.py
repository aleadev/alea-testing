# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  test script for the bayesian procedure using direct sampling of the Karhunen Loeve expansion
"""
# region Imports
from __future__ import division

from dolfin.fem.norms import errornorm
from dolfin.cpp.io import File

from Bayes_util.lib.bayes_lib import create_noisy_measurement_data_dofs
from Bayes_util.exponential.implicit_euler import ImplicitEuler
from scipy.integrate import quad
from scipy.interpolate import interp1d

from Bayes_util.asgfem_bayes import ASGFEMBayes
from Bayes_util.exponential.runge_kutta import RungeKutta
from Bayes_util.lib.bayes_lib_simple import create_unique_dofs
from Bayes_util.lib.interpolation_util import get_cheb_points

import logging.config
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
matplotlib.use('Agg')
# endregion

METHODS = ["quadrature", "measurements"]
METHOD = METHODS[1]
# region Setup parameters
#############################
#   Testsets
#   Set 1   for Method quadrature
#       refinements = 1
#       srank = 8
#       gpcdegree = 10
#       femdegree = 3
#       max_dofs = 1e5
#       iterations = 500
#       n_coefficients = 10
#       init_mesh_dofs = 1e4
#
#   Set 2   for method euler
#       refinements = 1
#       srank = 8
#       gpcdegree = 10
#       femdegree = 3
#       max_dofs = 1e5
#       iterations = 600
#       n_coefficients = 4
#       init_mesh_dofs = 1e4
#
#   Set 3
#       refinements = 10
#       srank = 8
#       gpcdegree = 10
#       femdegree = 3
#       max_dofs = 1e7
#       iterations = 500
#       n_coefficients = 10
#       init_mesh_dofs = 1e5
#
#   Set 4
#       refinements = 1
#       srank = 8
#       gpcdegree = 10
#       femdegree = 1
#       max_dofs = 1e5
#       iterations = 500
#       n_coefficients = 4
#       init_mesh_dofs = 1e4

#   region Parameters to use in stochastic Galerkin for the backward solution
refinements = 1 			                        # # of refinement steps in sg
srank = 8					                        # start ranks
gpcdegree = 10					                    # maximal # of gpcdegrees
femdegree = 1					                    # fem function degree
max_dofs = 1e5                                      # desired maximum of dofs to reach in refinement process
iterations = 500                                    # used iterations in tt ALS
polysys = "L"                                       # used polynomial system
domain = "square"                                   # used domain, square, L-Shape, ...
# coeffield_type = "EF-square-cos"
# coeffield_type = "EF-square-sin"
# coeffield_type = "monomial"
# coeffield_type = "linear"
# coeffield_type = "constant"                       # used coefficient field type
coeffield_type = "cos"
# amp_type = "constant"
amp_type = "decay-inf"
decay_exp_rate = 2                                  # decay rate for non constant coefficient fields
sgfem_gamma = 0.9                                   # Gamma used in adaptive SG FEM
rvtype = "uniform"
init_mesh_dofs = 1e4                                # initial mesh refinement. -1 : ignore
thetax = 0.5                                        # factor of deterministic refinement (0 : no refinement)
thetay = 0.5                                        # factor of stochastic refinement (0 : no refinement)
#   endregion

# region Solution parameter
n_coefficients = 2                                  # number of coefficients
USE_DOFS = False
n_dofs = 54                                         # number of dofs
n_dofs_list = [2**lia + 2 for lia in range(6)]      # list of increasing dofs

sample_coordinates = []
measurement_refinements = None
if METHOD == "measurements":
    measurement_refinements = 6                     # number of refinements, i.e. number of nodes squared start from 4
    for lia in range(measurement_refinements):
        sample_coordinates.append([[i/int(np.sqrt((lia+3)**2)+1), j/int(np.sqrt((lia+3)**2)+1)]
                                   for i in range(1, int(np.sqrt((lia+3)**2)))
                                  for j in range(1, int(np.sqrt((lia+3)**2)))])
else:
    # sample_coordinates = [[0.25, 0.25], [0.25, 0.5], [0.25, 0.75],
    #                      [0.5, 0.25],  [0.5, 0.5],  [0.5, 0.75],
    #                      [0.75, 0.25], [0.75, 0.5], [0.75, 0.75]]

    sample_coordinates = [[1/7, 1/7], [1/7, 2/7], [1/7, 3/7], [1/7, 4/7], [1/7, 5/7], [1/7, 6/7],
                          [2/7, 1/7], [2/7, 2/7], [2/7, 3/7], [2/7, 4/7], [2/7, 5/7], [2/7, 6/7],
                          [3/7, 1/7], [3/7, 2/7], [3/7, 3/7], [3/7, 4/7], [3/7, 5/7], [3/7, 6/7],
                          [4/7, 1/7], [4/7, 2/7], [4/7, 3/7], [4/7, 4/7], [4/7, 5/7], [4/7, 6/7],
                          [5/7, 1/7], [5/7, 2/7], [5/7, 3/7], [5/7, 4/7], [5/7, 5/7], [5/7, 6/7],
                          [6/7, 1/7], [6/7, 2/7], [6/7, 3/7], [6/7, 4/7], [6/7, 5/7], [6/7, 6/7]
                          ]
# endregion

# region Observation parameter
obs_precision = 1e-15
obs_rank = 100
# endregion

# region Inner Product parameter
inp_precision = 1e-15
inp_rank = 100
inp_gamma = 0.0001
# endregion

# region Euler Parameter
euler_precision = 1e-10                             # precision to use in euler rounding
euler_rank = 20                                     # rank to round down in euler method
euler_local_mc_samples = 1                          # samples in every euler step
euler_global_mc_samples = 1000                      # samples for the final result
euler_repetitions = 0                               # number of repetitions with halfed step size
euler_refinements = None
euler_steps = 1000                                  # start euler steps
euler_global_precision = 1e-5                       # global precision to reach after the euler scheme
euler_use_implicit = False
# endregion

# region Density parameters
n_eval_grid = 50
# endregion

# region setup discretisation
Nx, Ny = 10000, 20
dx, dy = 1/Nx, 1/Ny
eval_grid_points = np.linspace(-1, 1, n_eval_grid)
quadrature_refinements = None
if METHOD == "quadrature":
    quadrature_refinements = 5
    stochastic_grid = np.array([np.array(get_cheb_points([2*lia+2] * n_coefficients)[0])
                                for lia in range(1, quadrature_refinements)])
else:
    stochastic_grid = np.array(get_cheb_points([Ny] * n_coefficients)[0])
# endregion

# region MCMC parameter
n_walker = 50
burn_in = 10
n_samples = 100
covariance = 0.0001
# endregion

color_list = ['olive', 'gold', 'aqua', 'black', 'blue', 'brown', 'green', 'red', 'orange']

logging.config.fileConfig('logging.conf')           # load logger configuration
log = logging.getLogger('test_bayes')               # get new logger object

# endregion
if METHOD == "quadrature":
    file_path = 'results/paper/coef/quadrature/smallTenConstant/'
if METHOD == "measurements":
    file_path = 'results/paper/coef/measurements/bigTenDecayInf/'
bayes_obj = None
bayes_obj_list = []
unique_dofs = []
coeff_field = None
proj_basis = None
true_values = None
prior_densities = None
true_field = None
true_solution = None

field_error_h1 = []
field_error_l2 = []
solution_error_h1 = []
solution_error_l2 = []
if METHOD == "quadrature":

    log.info("Create exponential method")
    if euler_use_implicit:
        euler_method = ImplicitEuler(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
                                     euler_repetitions, euler_local_mc_samples)
    else:
        euler_method = RungeKutta(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
                                  euler_repetitions, euler_local_mc_samples)
        euler_method.method = "runge kutta"         # use the order 4 scheme
    for lia in range(1, quadrature_refinements):
        plt.clf()
        log.info("Create Bayes Object number {} with {} Chebyshev nodes".format(lia, len(stochastic_grid[lia-1])))
        dofs = []                                   # we use the coordinate sampling
        bayes_obj = ASGFEMBayes.init_solver(dofs, sample_coordinates, true_values, obs_precision,
                                            obs_rank, inp_precision, inp_rank, inp_gamma, euler_method,
                                            euler_global_precision, eval_grid_points, prior_densities,
                                            stochastic_grid[lia-1],
                                            iterations=iterations, refinements=refinements, max_dofs=max_dofs,
                                            thetax=thetax,
                                            thetay=thetay, srank=srank,  m=n_coefficients+1,
                                            femdegree=femdegree, gpcdegree=gpcdegree, polysys=polysys,
                                            gamma=sgfem_gamma, rvtype=rvtype, domain=domain,
                                            mesh_dofs=init_mesh_dofs, problem=0,
                                            coeffield_type=coeffield_type,
                                            amp_type=amp_type
                                            )

        if bayes_obj.import_bayes(file_path):
            bayes_obj_list.append(bayes_obj)
        else:
            log.info('start Bayesian procedure number {} with {} Chebyshev nodes'.format(lia,
                                                                                         len(stochastic_grid[lia-1])))
            # region  setup truth, noise and prior
            log.info("Create {} true parameters".format(bayes_obj.forwardOp.coefficients))
            # true_values = create_parameter_samples(bayes_obj.forwardOp.coefficients)
            # true_values = [0, 0.2, -0.2, 0.4]
            true_values = [0.2, -0.4, 0.5, -0.3, 0.4, 0, -0.1, -0.2, 0.3, 0.1, -0.5, 0.05, -0.05, 0.6, 0.7, 0.8, 0.9,
                           -0.6, -0.7, -0.8, -0.9, 0, 0.04]
            if USE_DOFS:
                unique_dofs = np.array(create_unique_dofs(n_dofs_list[-1], init_mesh_dofs))
            # endregion

            # region Setup Gaussian Prior
            Gauss_prior = False
            if Gauss_prior:
                raise NotImplemented
                # pi_y =
                # [lambda x: 1/(np.sqrt(2*np.pi)*sigma_i)*np.exp(-0.5*((x-mu_i)/sigma_i)**2) for mu_i, sigma_i in
                # zip(mu, sigma)]

            else:
                prior_densities = [lambda x: 0.5 * np.ones_like(x)] * bayes_obj.forwardOp.coefficients
            # endregion
            if USE_DOFS:
                measurements = create_noisy_measurement_data_dofs(bayes_obj.solution, true_values, dofs, inp_gamma)
            else:
                buffer_true_solution = bayes_obj.forwardOp.compute_direct_sample_solution(true_values)
                measurements = np.array([buffer_true_solution(sample_coordinate)
                                         for sample_coordinate in sample_coordinates])
            # measurements = create_noisy_measurement_data_dofs(bayes_obj.solution, true_values, dofs, inp_gamma)
            bayes_obj.measurements = measurements
            bayes_obj.true_values = true_values
            bayes_obj.prior_densities = prior_densities
            bayes_obj.calculate_densities()

            log.info('end Bayesian procedure')

            log.info('export Bayes file %s', file_path + bayes_obj.file_name + '.dat')
            if not bayes_obj.export_bayes(file_path):
                log.error("can not save bayes object")
            bayes_obj_list.append(bayes_obj)

        print("Bayes obj {} loaded from file".format(lia))
        poly_mean = []
        for i, yi in enumerate(bayes_obj.eval_densities):
            scaled_density = []
            scaled_domain = []
            curr_mean = 0
            curr_mean_counter = 0
            max_value = max(yi)
            left_bound = yi[0]
            right_bound = yi[-1]
            if False:  # max_value - 0.5 < 0.01:
                poly_mean.append(0)
            else:
                if left_bound > right_bound:        # left bigger than right
                                                    # at least a bit curved
                    if max_value - left_bound > 0.01:

                        for lic, entry in enumerate(yi):
                            if entry - left_bound >= 0:
                                                    # store only positive values
                                scaled_density.append(entry - left_bound)
                                # print "left_concave add density: {}".format(entry-left_bound)
                                scaled_domain.append(bayes_obj.eval_grid_points[lic])
                                curr_mean += entry - left_bound
                                curr_mean_counter += 1
                        if curr_mean_counter > 0:
                            curr_mean *= curr_mean_counter**(-1)
                            scaled_density = np.array(scaled_density) * curr_mean**(-1)
                            I = interp1d(scaled_domain, scaled_density, kind='cubic')
                            poly_mean.append(quad(lambda _x: _x*I(_x), scaled_domain[0], scaled_domain[-1])[0])
                        else:
                            raise TypeError("nothing positive")
                    else:                           # not that strongly curved, no scaling
                        scaled_density = yi
                        scaled_domain = bayes_obj.eval_grid_points
                        poly_mean.append(0)
                else:                               # right bigger than left
                    if max_value - right_bound > 0.01:
                        for lic, entry in enumerate(yi):
                            if entry - right_bound >= 0:
                                                    # store only positive values
                                scaled_density.append(entry - right_bound)
                                # print "right_concave add density: {}".format(entry-right_bound)
                                scaled_domain.append(bayes_obj.eval_grid_points[lic])
                                curr_mean += entry - right_bound
                                curr_mean_counter += 1
                        if curr_mean_counter > 0:
                            curr_mean *= curr_mean_counter**(-1)
                            scaled_density = np.array(scaled_density) * curr_mean**(-1)
                            I = interp1d(scaled_domain, scaled_density, kind='cubic')
                            poly_mean.append(quad(lambda _x: _x*I(_x), scaled_domain[0], scaled_domain[-1])[0])
                        else:
                            raise TypeError("nothing positive")
                    else:
                        scaled_density = yi
                        scaled_domain = bayes_obj.eval_grid_points
                        poly_mean.append(0)

            print("poly_mean: {} vs. true_value: {}".format(poly_mean[-1], bayes_obj.true_values[i]))
            plt.plot(bayes_obj.eval_grid_points, yi,
                     label="y_{0} should {1}".format(i, bayes_obj.true_values[i]),
                     color=color_list[i % len(color_list)])
            # print scaled_density
            # print scaled_domain
            plt.plot(scaled_domain, scaled_density, '--',
                     label="scaled y_{0} should {1}".format(i, bayes_obj.true_values[i]),
                     color=color_list[i % len(color_list)])
            plt.xlim([-1, 1])
            plt.vlines(bayes_obj.true_values[i], 0, max(yi),
                       color=color_list[i % len(color_list)])
        # plt.legend(ncol=len(bayes_obj.eval_densities)*2, loc=3)
        plt.draw()
        plt.savefig(file_path + "M{0}_g{1}_Dof{2}".format(len(bayes_obj.true_values), bayes_obj.inp_gamma,
                                                          len(bayes_obj.measurements)) + str(hash(bayes_obj.file_name))
                    + "_dens.pdf",
                    dpi=None, facecolor='w', edgecolor='w', orientation='portrait', papertype=None, format='pdf',
                    transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None)

        # from tt_als.alea.fem.fenics.fenics_vector import plot
        # print "mid: {}".format(poly_mean)
        # print "exact: {}".format(bayes_obj.true_values)
        # print "max: {}".format(poly_max_arg)
        # print str(hash(bayes_obj.file_name))
        # if True:
        #     exit()

        true_field = bayes_obj.get_true_coefficient_field()._fefunc
        approx_field = bayes_obj.get_parametric_coefficient_field(poly_mean)._fefunc

        approx_field_file = File(file_path + str(hash(bayes_obj.file_name)) +
                                 "M{}_approx_field.pvd".format(len(sample_coordinates[lia-1])))
        approx_field_file << approx_field

        field_error_l2.append(errornorm(true_field, approx_field, norm_type="l2", degree_rise=0))
        field_error_h1.append(errornorm(true_field, approx_field, norm_type="h1", degree_rise=0))

        true_solution = bayes_obj.forwardOp.compute_direct_sample_solution(bayes_obj.true_values)._fefunc
        approx_solution = bayes_obj.forwardOp.compute_direct_sample_solution(poly_mean)._fefunc

        approx_solution_file = File(file_path + str(hash(bayes_obj.file_name)) +
                                 "M{}_approx_solution.pvd".format(len(sample_coordinates[lia-1])))
        approx_solution_file << approx_solution

        solution_error_h1.append(errornorm(true_solution, approx_solution, norm_type='h1', degree_rise=0))
        solution_error_l2.append(errornorm(true_solution, approx_solution, norm_type='l2', degree_rise=0))
    plt.clf()
    plt.semilogy(np.array([lia*2 for lia in range(1, quadrature_refinements)]), np.array(field_error_l2),
                 '-or', label="L2 error")
    plt.semilogy(np.array([lia*2 for lia in range(1, quadrature_refinements)]), np.array(field_error_h1),
                 '-or', label="H1 error")
    plt.title("Error of the coefficient field.")
    plt.legend(ncol=2, loc=1)
    plt.xlabel("polynomial degree")
    plt.ylabel("ERROR of the coefficient field")
    plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "coef_error.png")
    plt.clf()
    plt.semilogy(np.array([2*lia for lia in range(1, quadrature_refinements)]), np.array(solution_error_l2),
                 '-or', label="L2 error")
    plt.semilogy(np.array([2*lia for lia in range(1, quadrature_refinements)]), np.array(solution_error_h1),
                 '-or', label="H1 error")
    plt.title("Error of the posterior forward solution.")
    plt.legend(ncol=2, loc=1)
    plt.xlabel("polynomial degree")
    plt.ylabel("ERROR of the posterior forward solution")
    plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "sol_error.png")
    true_field_file = File(file_path + str(hash(bayes_obj.file_name)) + "true_field.pvd")
    true_field_file << true_field
    true_solution_file = File(file_path + str(hash(bayes_obj.file_name)) + "true_solution.pvd")
    true_solution_file << true_solution

if METHOD == "measurements":

    log.info("Create exponential method")
    if euler_use_implicit:
        euler_method = ImplicitEuler(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
                                     euler_repetitions, euler_local_mc_samples)
    else:
        euler_method = RungeKutta(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
                                  euler_repetitions, euler_local_mc_samples)
        euler_method.method = "exp euler"
    for lia in range(1, measurement_refinements):
        plt.clf()
        log.info("Create Bayes Object number {} with {} measurement nodes".format(lia, len(sample_coordinates[lia-1])))
        dofs = []                                   # we use the coordinate sampling
        bayes_obj = ASGFEMBayes.init_solver(dofs, sample_coordinates[lia-1], true_values, obs_precision,
                                            obs_rank, inp_precision, inp_rank, inp_gamma, euler_method,
                                            euler_global_precision, eval_grid_points, prior_densities,
                                            stochastic_grid,
                                            iterations=iterations, refinements=refinements, max_dofs=max_dofs,
                                            thetax=thetax,
                                            thetay=thetay, srank=srank,  m=n_coefficients+1,
                                            femdegree=femdegree, gpcdegree=gpcdegree, polysys=polysys,
                                            gamma=sgfem_gamma, rvtype=rvtype, domain=domain,
                                            mesh_dofs=init_mesh_dofs, problem=0,
                                            coeffield_type=coeffield_type,
                                            amp_type=amp_type
                                            )

        if bayes_obj.import_bayes(file_path):
            bayes_obj_list.append(bayes_obj)
        else:
            log.info('start Bayesian procedure number {} with {} '
                     'measurement nodes'.format(lia, len(sample_coordinates[lia-1])))
            # region  setup truth, noise and prior
            log.info("Create {} true parameters".format(bayes_obj.forwardOp.coefficients))
            # true_values = create_parameter_samples(bayes_obj.forwardOp.coefficients)
            # true_values = [0, 0.2, -0.2, 0.4]
            true_values = [0.2, -0.4, 0, 0.3, 0.4, 0.5, -0.1, -0.2, -0.3, 0.1, -0.5, 0.05, -0.05, 0.6, 0.7, 0.8, 0.9,
                           -0.6, -0.7, -0.8, -0.9, 0, 0.04]
            if USE_DOFS:
                unique_dofs = np.array(create_unique_dofs(n_dofs_list[-1], init_mesh_dofs))
            # endregion

            # region Setup Gaussian Prior
            Gauss_prior = False
            if Gauss_prior:
                raise NotImplemented
                # pi_y =
                # [lambda x: 1/(np.sqrt(2*np.pi)*sigma_i)*np.exp(-0.5*((x-mu_i)/sigma_i)**2) for mu_i, sigma_i in
                # zip(mu, sigma)]

            else:
                prior_densities = [lambda x: 0.5 * np.ones_like(x)] * bayes_obj.forwardOp.coefficients
            # endregion
            if USE_DOFS:
                measurements = create_noisy_measurement_data_dofs(bayes_obj.solution, true_values, dofs, inp_gamma)
            else:
                buffer_true_solution = bayes_obj.forwardOp.compute_direct_sample_solution(true_values)
                measurements = np.array([buffer_true_solution(sample_coordinate)
                                         for sample_coordinate in sample_coordinates[lia-1]])
            # measurements = create_noisy_measurement_data_dofs(bayes_obj.solution, true_values, dofs, inp_gamma)
            bayes_obj.measurements = measurements
            bayes_obj.true_values = true_values
            bayes_obj.prior_densities = prior_densities
            bayes_obj.calculate_densities()

            log.info('end Bayesian procedure')

            log.info('export Bayes file %s', file_path + bayes_obj.file_name + '.dat')
            if not bayes_obj.export_bayes(file_path):
                log.error("can not save bayes object")
            bayes_obj_list.append(bayes_obj)

        print("Bayes obj {} loaded from file".format(lia))
        poly_mean = []
        for i, yi in enumerate(bayes_obj.eval_densities):
            scaled_density = []
            scaled_domain = []
            curr_mean = 0
            curr_mean_counter = 0
            max_value = max(yi)
            left_bound = yi[0]
            right_bound = yi[-1]
            if False:  # max_value - 0.5 < 0.01:
                poly_mean.append(0)
            else:
                if left_bound > right_bound:        # left bigger than right
                                                    # at least a bit curved
                    if max_value - left_bound > 0:
                        curr_mean_counter = 0
                        for lic, entry in enumerate(yi):
                            if entry - left_bound >= 0:
                                                    # store only positive values
                                scaled_density.append(entry - left_bound)
                                # print "left_concave add density: {}".format(entry-left_bound)
                                scaled_domain.append(bayes_obj.eval_grid_points[lic])
                                curr_mean += entry - left_bound
                                curr_mean_counter += 1
                        if curr_mean_counter > 4:
                            curr_mean *= curr_mean_counter**(-1)
                            scaled_density = np.array(scaled_density) * curr_mean**(-1)
                            I = interp1d(scaled_domain, scaled_density, kind='cubic')
                            poly_mean.append(quad(lambda _x: _x*I(_x), scaled_domain[0], scaled_domain[-1])[0])
                        else:
                            poly_mean.append(0)
                    else:                           # not that strongly curved, no scaling
                        scaled_density = yi
                        scaled_domain = bayes_obj.eval_grid_points
                        poly_mean.append(0)
                else:                               # right bigger than left
                    if max_value - right_bound > 0:
                        curr_mean_counter = 0
                        for lic, entry in enumerate(yi):
                            if entry - right_bound >= 0:
                                                    # store only positive values
                                scaled_density.append(entry - right_bound)
                                # print "right_concave add density: {}".format(entry-right_bound)
                                scaled_domain.append(bayes_obj.eval_grid_points[lic])
                                curr_mean += entry - right_bound
                                curr_mean_counter += 1
                        if curr_mean_counter > 4:
                            curr_mean *= curr_mean_counter**(-1)
                            scaled_density = np.array(scaled_density) * curr_mean**(-1)
                            I = interp1d(scaled_domain, scaled_density, kind='cubic')
                            poly_mean.append(quad(lambda _x: _x*I(_x), scaled_domain[0], scaled_domain[-1])[0])
                        else:
                            poly_mean.append(0)
                    else:
                        scaled_density = yi
                        scaled_domain = bayes_obj.eval_grid_points
                        poly_mean.append(0)
            print "poly_mean length: {} vs true_values length: {}".format(len(poly_mean), len(bayes_obj.true_values))
            print("poly_mean: {} vs. true_value: {}".format(poly_mean[-1], bayes_obj.true_values[i]))
            plt.plot(bayes_obj.eval_grid_points, yi,
                     label="y_{0} should {1}".format(i, bayes_obj.true_values[i]),
                     color=color_list[i % len(color_list)])
            # print scaled_density
            # print scaled_domain
            plt.plot(scaled_domain, scaled_density, '--',
                     label="scaled y_{0} should {1}".format(i, bayes_obj.true_values[i]),
                     color=color_list[i % len(color_list)])
            plt.xlim([-1, 1])
            plt.vlines(bayes_obj.true_values[i], 0, max(yi),
                       color=color_list[i % len(color_list)])
        # plt.legend(ncol=len(bayes_obj.eval_densities)*2, loc=3)
        plt.draw()
        plt.savefig(file_path + "M{0}_g{1}_Dof{2}".format(len(bayes_obj.true_values), bayes_obj.inp_gamma,
                                                          len(bayes_obj.measurements)) +
                    str(hash(bayes_obj.file_name)) + "_dens.pdf", dpi=None, facecolor='w', edgecolor='w',
                    orientation='portrait', papertype=None, format='pdf',
                    transparent=False, bbox_inches='tight', pad_inches=0.1,
                    frameon=None)

        # from tt_als.alea.fem.fenics.fenics_vector import plot
        # print "mid: {}".format(poly_mean)
        # print "exact: {}".format(bayes_obj.true_values)
        # print "max: {}".format(poly_max_arg)
        # print str(hash(bayes_obj.file_name))
        # if True:
        #     exit()
        true_field = bayes_obj.get_true_coefficient_field()._fefunc
        approx_field = bayes_obj.get_parametric_coefficient_field(poly_mean)._fefunc

        # true_field_plot = plot(true_field, interactive=False)
        # true_field_plot.write_png(file_path + str(hash(bayes_obj.file_name)) + "true_field.png")
        # plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "true_field.png")

        approx_field_file = File(file_path + str(hash(bayes_obj.file_name)) +
                                 "M{}_approx_field.pvd".format(len(sample_coordinates[lia-1])))
        approx_field_file << approx_field

        field_error_l2.append(errornorm(true_field, approx_field, norm_type="l2"))
        field_error_h1.append(errornorm(true_field, approx_field, norm_type="h1"))

        true_solution = bayes_obj.forwardOp.compute_direct_sample_solution(bayes_obj.true_values)._fefunc
        approx_solution = bayes_obj.forwardOp.compute_direct_sample_solution(poly_mean)._fefunc

        approx_solution_file = File(file_path + str(hash(bayes_obj.file_name)) +
                                 "M{}_approx_solution.pvd".format(len(sample_coordinates[lia-1])))
        approx_solution_file << approx_solution

        solution_error_h1.append(errornorm(true_solution, approx_solution, norm_type='h1'))
        solution_error_l2.append(errornorm(true_solution, approx_solution, norm_type='l2'))
    plt.clf()
    plt.semilogy(np.array([lia**2 for lia in range(1, measurement_refinements)]), np.array(field_error_l2),
                 '-or', label="L2 error")
    plt.semilogy(np.array([lia**2 for lia in range(1, measurement_refinements)]), np.array(field_error_h1),
                 '-or', label="H1 error")
    plt.title("Error of the coefficient field.")
    plt.legend(ncol=2, loc=1)
    plt.xlabel("polynomial degree")
    plt.ylabel("ERROR of the coefficient field")
    plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "coef_error.png")
    plt.clf()
    plt.semilogy(np.array([lia**2 for lia in range(1, measurement_refinements)]), np.array(solution_error_l2),
                 '-or', label="L2 error")
    plt.semilogy(np.array([lia**2 for lia in range(1, measurement_refinements)]), np.array(solution_error_h1),
                 '-or', label="H1 error")
    plt.title("Error of the posterior forward solution.")
    plt.legend(ncol=2, loc=1)
    plt.xlabel("polynomial degree")
    plt.ylabel("ERROR of the posterior forward solution")
    plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "sol_error.png")

    true_field_file = File(file_path + str(hash(bayes_obj.file_name)) + "true_field.pvd")
    true_field_file << true_field
    true_solution_file = File(file_path + str(hash(bayes_obj.file_name)) + "true_solution.pvd")
    true_solution_file << true_solution
