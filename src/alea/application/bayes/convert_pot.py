import numpy as np

convert_pot = False
convert_mean = True
if convert_pot:
    path = "../paper-tt-bayesian-inversion/results/decay-inf/big/testPot/"
    name = "pot_error_rank512_P2_gamma-4_index1.dat"
    ranks = []
    tt_dofs = []
    err = []
    refine = []
    with open(path + name, "r") as f:
        for lia, line in enumerate(f):
            if lia == 0:
                continue

            items = line.split(",")
            assert len(items) == 4
            items[3] = float(items[3])
            if lia == 1:
                ranks.append(items[0])
                tt_dofs.append(items[1])
                refine.append(items[2])
                err.append(items[3])
            else:
                counter = 0
                if int(tt_dofs[-1]) < int(items[1]):
                    if counter > 0:
                        err[-1] *= counter ** (-1)
                        counter = 0
                    ranks.append(items[0])
                    tt_dofs.append(items[1])
                    refine.append(items[2])
                    err.append(items[3])
                else:
                    err[-1] += items[3]

    with open(path + "new_" + name, "w") as f:
        f.write("r, tt_dofs, tt_refine, err_pot\n")
        for lia in range(len(ranks)):
            f.write(ranks[lia] + ", " + tt_dofs[lia] + ", " + refine[lia] + "," + str(err[lia]) + "\n")

if convert_mean:
    path = "../paper-tt-bayesian-inversion/results/"
    name = "mean_error_P1_item-1_log2.dat"
    mc_samples = []
    tt_mean = []
    sample_mean = []
    err = []
    err_rel = []
    with open(path + name, "r") as f:
        for lia, line in enumerate(f):
            loc_err = []
            loc_err_rel = []
            if lia == 0:
                continue

            items = line.split(",")
            # assert len(items) == 4
            for lic in range(len(items)):
                items[lic] = float(items[lic])
                if lic == 0:
                    mc_samples.append(items[lic])
                if lic == 1:
                    tt_mean.append(items[lic])
                if lic in (2, 5, 8, 11, 14, 17, 20, 23, 25, 28):
                    sample_mean.append(items[lic])
                if lic in (2+1, 5+1, 8+1, 11+1, 14+1, 17+1, 20+1, 23+1, 25+1, 28+1):
                    loc_err.append(items[3])
                if lic in (2+2, 5+2, 8+2, 11+2, 14+2, 17+2, 20+2, 23+2, 25+2, 28+2):
                    loc_err_rel.append(items[lic])
            err.append(np.sum(np.array(loc_err))/10)
            err_rel.append(np.sum(np.array(loc_err_rel))/10)
    with open(path + "new_" + name, "w") as f:
        f.write("MC_samples, tt_mean, sample_mean, err, err_rel\n")
        for lia in range(len(mc_samples)):
            f.write(str(mc_samples[lia]) + "," + str(tt_mean[lia]) + "," + str(sample_mean[lia]) + "," + str(err[lia]) + "," + str(err_rel[lia]) + "\n")
