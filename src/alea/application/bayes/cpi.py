from mpi4py import MPI
import numpy as np
from dolfin import inner, dx, assemble, nabla_grad

comm = MPI.Comm.Get_parent()
size = comm.Get_size()
rank = comm.Get_rank()

data = None
comm.bcast(data, root=0)
_coef_fun = data['coef1']
_coef_fun1 = data['coef2']
_sol_fun = data['u1']
_sol_fun1 = data['u2']
mesh = data['mesh']
R_T = inner(_coef_fun * nabla_grad(_sol_fun), _coef_fun1 * nabla_grad(_sol_fun1))
res = R_T * dx(mesh)
zeta_res = assemble(res, form_compiler_parameters={'quadrature_degree': -1})
zeta_res = np.array(zeta_res, dtype="d")

comm.Reduce([zeta_res, MPI.DOUBLE], None, root=0)

comm.Disconnect()

