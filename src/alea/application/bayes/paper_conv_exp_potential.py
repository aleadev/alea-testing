# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  test script for the bayesian procedure using direct sampling of the Karhunen Loeve expansion
"""
# region Imports
from __future__ import division

from Bayes_util.lib.bayes_lib import create_parameter_samples, create_noisy_measurement_data_dofs
from Bayes_util.exponential.implicit_euler import ImplicitEuler
# from scipy.integrate import quad
# from scipy.interpolate import interp1d

from Bayes_util.asgfem_bayes import ASGFEMBayes
# from Bayes_util.asgfem_mcmc_bayes import ASGFEMBayesMCMC
from Bayes_util.exponential.explicit_euler import ExplicitEuler
from Bayes_util.exponential.embedded_runge_kutta import EmbeddedRungeKutta
from Bayes_util.lib.bayes_lib_simple import create_unique_dofs
from Bayes_util.lib.interpolation_util import get_cheb_points

from joblib import Parallel, delayed
import logging.config
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
# endregion

METHODS = ["", "quadrature", "euler"]
METHOD = METHODS[1]
# region Setup parameters
#############################
#   Testsets
#   Set 1   for Method quadrature
#       refinements = 1
#       srank = 8
#       gpcdegree = 10
#       femdegree = 3
#       max_dofs = 1e5
#       iterations = 500
#       n_coefficients = 10
#       init_mesh_dofs = 1e4
#
#   Set 2   for method euler
#       refinements = 1
#       srank = 8
#       gpcdegree = 10
#       femdegree = 3
#       max_dofs = 1e5
#       iterations = 600
#       n_coefficients = 4
#       init_mesh_dofs = 1e4
#
#   Set 3
#       refinements = 10
#       srank = 8
#       gpcdegree = 10
#       femdegree = 3
#       max_dofs = 1e7
#       iterations = 500
#       n_coefficients = 10
#       init_mesh_dofs = 1e5
#
#   Set 4
#       refinements = 1
#       srank = 8
#       gpcdegree = 10
#       femdegree = 1
#       max_dofs = 1e5
#       iterations = 500
#       n_coefficients = 4
#       init_mesh_dofs = 1e4
#
#   Set 5
#       refinements = 1
#       srank = 8
#       gpcdegree = 10
#       femdegree = 3
#       max_dofs = 1e5
#       iterations = 500
#       n_coefficients = 10
#       init_mesh_dofs = 1e4
#       constant

#   region Parameters to use in stochastic Galerkin for the backward solution
refinements = 5 			                        # # of refinement steps in sg
srank = 10					                        # start ranks
gpcdegree = 10					                    # maximal # of gpcdegrees
femdegree = 1					                    # fem function degree
max_dofs = 1e8                                      # desired maximum of dofs to reach in refinement process
iterations = 501                                    # used iterations in tt ALS
polysys = "L"                                       # used polynomial system
domain = "square"                                   # used domain, square, L-Shape, ...
# coeffield_type = "EF-square-cos"
# coeffield_type = "EF-square-sin"
# coeffield_type = "monomial"
# coeffield_type = "linear"
# coeffield_type = "constant"                       # used coefficient field type
coeffield_type = "cos"
amp_type = "constant"
# amp_type = "decay-inf"
decay_exp_rate = 2
field_mean = 100                                    # decay rate for non constant coefficient fields
sgfem_gamma = 0.9                                   # Gamma used in adaptive SG FEM
rvtype = "uniform"
init_mesh_dofs = 1e2                                # initial mesh refinement. -1 : ignore
thetax = 0.5                                        # factor of deterministic refinement (0 : no refinement)
thetay = 0.5                                        # factor of stochastic refinement (0 : no refinement)
#   endregion

# region Solution parameter
n_coefficients = 5                                  # number of coefficients
USE_DOFS = False
n_dofs = 54                                         # number of dofs
n_dofs_list = [2**lia + 2 for lia in range(6)]      # list of increasing dofs

sample_coordinates = [[0.25, 0.25], [0.25, 0.5], [0.25, 0.75],
                      [0.5, 0.25],  [0.5, 0.5],  [0.5, 0.75],
                      [0.75, 0.25], [0.75, 0.5], [0.75, 0.75]]

# sample_coordinates = [[1/7, 1/7], [1/7, 2/7], [1/7, 3/7], [1/7, 4/7], [1/7, 5/7], [1/7, 6/7],
#                      [2 / 7, 1 / 7], [2 / 7, 2 / 7], [2 / 7, 3 / 7], [2 / 7, 4 / 7], [2 / 7, 5 / 7], [2 / 7, 6 / 7],
#                      [3 / 7, 1 / 7], [3 / 7, 2 / 7], [3 / 7, 3 / 7], [3 / 7, 4 / 7], [3 / 7, 5 / 7], [3 / 7, 6 / 7],
#                      [4 / 7, 1 / 7], [4 / 7, 2 / 7], [4 / 7, 3 / 7], [4 / 7, 4 / 7], [4 / 7, 5 / 7], [4 / 7, 6 / 7],
#                      [5 / 7, 1 / 7], [5 / 7, 2 / 7], [5 / 7, 3 / 7], [5 / 7, 4 / 7], [5 / 7, 5 / 7], [5 / 7, 6 / 7],
#                      [6 / 7, 1 / 7], [6 / 7, 2 / 7], [6 / 7, 3 / 7], [6 / 7, 4 / 7], [6 / 7, 5 / 7], [6 / 7, 6 / 7]
#                      ]
# endregion

# region Observation parameter
obs_precision = 1e-15
obs_rank = 500
# endregion

# region Inner Product parameter
inp_precision = 1e-15
inp_rank = 500
inp_gamma = 0.0001
# endregion

# region Euler Parameter
euler_precision = 1e-10                             # precision to use in euler rounding
euler_rank = 10                                     # rank to round down in euler method
euler_local_mc_samples = 1                          # samples in every euler step
euler_global_mc_samples = 1000                      # samples for the final result
euler_repetitions = 0                               # number of repetitions with half step size
euler_refinements = None
if METHOD == "euler":
    euler_refinements = 4
    euler_steps = [10**lia for lia in range(0, euler_refinements)]
else:
    euler_steps = 10                              # start euler steps
euler_global_precision = 1e-5                       # global precision to reach after the euler scheme
euler_use_implicit = True
# endregion

# region Density parameters
n_eval_grid = 50
# endregion

# region setup discretisation
Nx, Ny = 10000, 20
dx, dy = 1/Nx, 1/Ny
eval_grid_points = np.linspace(-1, 1, n_eval_grid)
quadrature_refinements = None
if METHOD == "quadrature":
    quadrature_refinements = 20
    stochastic_grid = np.array([np.array(get_cheb_points([2*lia] * n_coefficients)[0])
                                for lia in range(1, quadrature_refinements)])
else:
    stochastic_grid = np.array(get_cheb_points([Ny] * n_coefficients)[0])
# endregion

# region MCMC parameter
n_walker = 50
burn_in = 10
n_samples = 100
covariance = 0.0001
# endregion

logging.config.fileConfig('logging.conf')           # load logger configuration
log = logging.getLogger('test_bayes')               # get new logger object

# endregion

file_path = "results/paper/potential_exp/" + amp_type + "/"
if thetax == 1:
    file_path += "fix/"

bayes_obj = None
bayes_obj_list = []
unique_dofs = []
coeff_field = None
proj_basis = None
true_values = None
prior_densities = None

potential_mc_samples = 100                          # number of mc samples to estimate the error
potential_mc_sample = None                          # current mc samples
potential_mc_sample_list = []                       # list of mc samples of the potential
potential_x_achses = []

if METHOD == "quadrature":

    log.info("Create exponential method")
    if euler_use_implicit:
        euler_method = ImplicitEuler(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
                                     euler_repetitions, euler_local_mc_samples)
    else:
        if True:
            euler_method = EmbeddedRungeKutta(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
                                              euler_repetitions, euler_local_mc_samples)
            euler_method.method = "forth"
            euler_method.acc = euler_global_precision
            euler_method.min_stepsize = 1e-19
            euler_method.max_stepsize = 1.0 - 1e-10
        else:
            euler_method = ExplicitEuler(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
                                         euler_repetitions, euler_local_mc_samples)
    for lia in range(1, quadrature_refinements):
        log.info("Create Bayes Object number {} with {} Chebyshev nodes".format(lia, len(stochastic_grid[lia-1])))
        dofs = []                                   # we use the coordinate sampling
        bayes_obj = ASGFEMBayes.init_solver(dofs, sample_coordinates, true_values, obs_precision,
                                            obs_rank, inp_precision, inp_rank, inp_gamma, euler_method,
                                            euler_global_precision, eval_grid_points, prior_densities,
                                            stochastic_grid[lia-1],
                                            iterations=iterations, refinements=refinements, max_dofs=max_dofs,
                                            thetax=thetax,
                                            thetay=thetay, srank=srank,  m=n_coefficients+1,
                                            femdegree=femdegree, gpcdegree=gpcdegree, polysys=polysys,
                                            gamma=sgfem_gamma, rvtype=rvtype, domain=domain,
                                            mesh_dofs=init_mesh_dofs, problem=0,
                                            coeffield_type=coeffield_type,
                                            amp_type=amp_type
                                            )

        if bayes_obj.import_bayes(file_path):
            bayes_obj_list.append(bayes_obj)
        else:
            log.info('start Bayesian procedure number {} with {} Chebyshev '
                     'nodes'.format(lia, len(stochastic_grid[lia-1])))
            # region  setup truth, noise and prior
            log.info("Create {} true parameters".format(bayes_obj.forwardOp.coefficients))
            true_values = create_parameter_samples(bayes_obj.forwardOp.coefficients)
            if USE_DOFS:
                unique_dofs = np.array(create_unique_dofs(n_dofs_list[-1], init_mesh_dofs))
            # endregion

            # region Setup Gaussian Prior
            Gauss_prior = False
            if Gauss_prior:
                raise NotImplemented
                # pi_y =
                # [lambda x: 1/(np.sqrt(2*np.pi)*sigma_i)*np.exp(-0.5*((x-mu_i)/sigma_i)**2) for mu_i, sigma_i in
                # zip(mu, sigma)]

            else:
                prior_densities = [lambda x: 0.5 * np.ones_like(x)] * bayes_obj.forwardOp.coefficients
            # endregion
            if USE_DOFS:
                measurements = create_noisy_measurement_data_dofs(bayes_obj.solution, true_values, dofs, inp_gamma)
            else:
                measurements = np.array([bayes_obj.forwardOp.compute_direct_sample_solution(true_values)
                                         (sample_coordinate)
                                         for sample_coordinate in sample_coordinates])
            # measurements = create_noisy_measurement_data_dofs(bayes_obj.solution, true_values, dofs, inp_gamma)
            bayes_obj.measurements = measurements
            bayes_obj.true_values = true_values
            bayes_obj.prior_densities = prior_densities

            bayes_obj.calculate_densities()

            log.info('end Bayesian procedure')

            log.info('export Bayes file %s', file_path + bayes_obj.file_name + '.dat')
            if not bayes_obj.export_bayes(file_path):
                log.error("can not save bayes object")
            bayes_obj_list.append(bayes_obj)

        print("Bayes obj {} loaded from file".format(lia))

        lic = 0
        n_cpu = 40
        xi_samples = np.array([np.random.rand(bayes_obj.forwardOp.coefficients+1)*2 - 1 for _
                               in range(potential_mc_samples)])
        for lix in range(potential_mc_samples):
            xi_samples[lix][0] = 0
        # print xi_samples

        # test the measurements
        # sol_buffer = bayes_obj.forwardOp.compute_direct_sample_solution(bayes_obj.true_values)
        # sol_measurements = np.array([sol_buffer(loc_dof) for loc_dof in bayes_obj.sample_coordinates])
        # print sol_measurements - bayes_obj.measurements
        # print np.sum(bayes_obj.forwardOp.sampling_error_l2)/len(bayes_obj.forwardOp.sampling_error_l2)

        def _work_in_parallel(xi):

            # sol_buffer = bayes_obj.forwardOp.compute_direct_sample_solution(xi)
            sol_buffer = bayes_obj.forwardOp.sample_at_coeff(list(xi[1:]))
            sol_measurements = np.array([sol_buffer(loc_dof) for loc_dof in bayes_obj.sample_coordinates])
            # print("     (O o G)(u) at {0} = {1}".format(bayes_obj.sample_coordinates, sol_measurements))
            diff = np.array(bayes_obj.measurements) - np.array(sol_measurements)
            # print("     given measurements: {}".format(bayes_obj.measurements))
            sampled_potential = np.array(bayes_obj.sample_potential(list(xi)))
            direct_potential = np.exp(-0.5 * np.sum((diff ** 2) * (inp_gamma ** (-1))))
            potential_mc_sample_ = direct_potential - sampled_potential

            return potential_mc_sample_
        potential_mc_sample = np.sum(np.array(Parallel(n_cpu)(delayed(_work_in_parallel)(xi) for xi in xi_samples)))
        # potential_mc_sample = np.sum(np.array(Parallel(n_cpu)(delayed(_workInParallel)(xi) for
        # xi in zip(stochastic_grid[lia-1], stochastic_grid[lia-1], stochastic_grid[lia-1], stochastic_grid[lia-1]))))
        # potential_mc_sample /= len(stochastic_grid[lia-1])
        potential_mc_sample /= len(xi_samples)
        potential_mc_sample_list.append(potential_mc_sample)
        potential_x_achses.append(len(stochastic_grid[lia-1])*len(bayes_obj.true_values))
    print potential_mc_sample_list
    plt.loglog(np.array(potential_x_achses), np.array(potential_mc_sample_list),
               '-or', label="error sampling of the potential")
    plt.title("Error of the potential. MC sampled with the true potential.")
    plt.legend(ncol=2, loc=1)
    plt.xlabel("total number of collocation points")
    plt.ylabel("ERROR of the potential")
    plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "potential_exp_error.png")
    plt.clf()
# for step, n_dofs in enumerate(n_dofs_list):
#
#     if USE_DOFS:
#         log.info('Create %i unique random dofs', n_dofs)
#         dofs = unique_dofs[:n_dofs]
#     else:
#         dofs = []
#     log.info("Create Bayes Object")
#
#     bayes_obj = ASGFEMBayes.init_solver(dofs, sample_coordinates, true_values, obs_precision,
#                                         obs_rank, inp_precision, inp_rank, inp_gamma, euler_method,
#                                         euler_global_precision, eval_grid_points, prior_densities, stochastic_grid,
#                                         iterations=iterations, refinements=refinements, max_dofs=max_dofs,
#                                         thetax=thetax,
#                                         thetay=thetay, srank=srank,  m=n_coefficients+1,
#                                         femdegree=femdegree, gpcdegree=gpcdegree, polysys=polysys,
#                                         gamma=sgfem_gamma, rvtype=rvtype, domain=domain,
#                                         mesh_dofs=init_mesh_dofs, problem=0,
#                                         coeffield_type=coeffield_type,
#                                         amp_type=amp_type
#                                         )
#
#     # region Create Bayes solution either from pickling or calculation
#     fig_set = 0
#     fig = plt.figure()
#     bayes_obj.true_values = [0 for _ in range(23)]
#     if bayes_obj.import_bayes(file_path):
#         log.info('open Bayes file %s', file_path + bayes_obj.file_name + '.dat')
#         bayes_obj_list.append(bayes_obj)
#
#         # region results presentation area
#         plt.clf()
#         if fig_set != 0:
#             fig.clf()
#         try:
#             plt.clf()
#
#             # region Plot Solution Object for true coefficients
#             # from tt_als.alea.fem.fenics.fenics_vector import plot
#             # bayes_obj.forwardOp.sample_at_coeff(bayes_obj.true_values).plot(title="Approximated solution")
#             # bayes_obj.forwardOp.compute_direct_sample_solution(bayes_obj.true_values).plot(title="Direct solution")
#             # endregion
#
#             # region Plot ForwardOperator Error
#             if not bayes_obj.forwardOp.sampling_error_h1 or not bayes_obj.forwardOp.sampling_error_l2:
#                 xi_samples = np.array([np.random.rand(bayes_obj.forwardOp.coefficients) for _ in range(100)])
#                 log.info("Calculate forward operator sampling error")
#                 l2_error, h1_error = bayes_obj.forwardOp.estimate_sampling_error(list(xi_samples))
#                 l2_error_mean = 0
#                 h1_error_mean = 0
#                 for lia in range(len(xi_samples)):
#                     l2_error_mean += l2_error[lia]
#                     h1_error_mean += h1_error[lia]
#                 l2_error_mean /= len(xi_samples)
#                 h1_error_mean /= len(xi_samples)
#                 plt.plot(l2_error, '-r', label="L2-Error")
#                 plt.plot(list(np.arange(0, xi_samples.shape[0])),
#                          [l2_error_mean]*xi_samples.shape[0], '-b', label="Mean")
#                 plt.title("Error sampling of the forward solution with true coefficient values")
#                 plt.legend(ncol=2, loc=1)
#                 plt.xlabel("Sampling number")
#                 plt.ylabel("Error in L2 norm")
#                 plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "ASGFEM_L2_error.png")
#                 plt.clf()
#                 plt.plot(h1_error, '-r', label="H1-Error")
#                 plt.plot(list(np.arange(0, xi_samples.shape[0])),
#                          [l2_error_mean]*xi_samples.shape[0], '-b', label="Mean")
#                 plt.title("Error sampling of the forward solution with true coefficient values")
#                 plt.legend(ncol=2, loc=1)
#                 plt.xlabel("Sampling number")
#                 plt.ylabel("Error in H1 norm")
#                 plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "ASGFEM_H1_error.png")
#                 plt.clf()
#             else:
#                 l2_error_mean = 0
#                 h1_error_mean = 0
#                 for lia in range(len(bayes_obj.forwardOp.sampling_error_l2)):
#                     l2_error_mean += bayes_obj.forwardOp.sampling_error_l2[lia]
#                 l2_error_mean /= len(bayes_obj.forwardOp.sampling_error_l2)
#                 for lia in range(len(bayes_obj.forwardOp.sampling_error_h1)):
#                     h1_error_mean += bayes_obj.forwardOp.sampling_error_h1[lia]
#                 h1_error_mean /= len(bayes_obj.forwardOp.sampling_error_h1)
#                 plt.plot(bayes_obj.forwardOp.sampling_error_l2, '-r', label="L2-Error")
#                 plt.plot(np.arange(0, len(bayes_obj.forwardOp.sampling_error_l2)),
#                          [l2_error_mean]*len(bayes_obj.forwardOp.sampling_error_l2), '-b', label="Mean")
#                 plt.title("Error sampling of the forward solution with true coefficient values")
#                 plt.legend(ncol=2, loc=1)
#                 plt.xlabel("Sampling number")
#                 plt.ylabel("Error in L2 norm")
#                 plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "ASGFEM_L2_error.png")
#                 plt.clf()
#                 plt.plot(bayes_obj.forwardOp.sampling_error_h1, '-r', label="H1-Error")
#                 plt.plot(np.arange(0, len(bayes_obj.forwardOp.sampling_error_h1)),
#                          [h1_error_mean]*len(bayes_obj.forwardOp.sampling_error_h1), '-b', label="Mean")
#                 plt.title("Error sampling of the forward solution with true coefficient values")
#                 plt.legend(ncol=2, loc=1)
#                 plt.xlabel("Sampling number")
#                 plt.ylabel("Error in H1 norm")
#                 plt.savefig(file_path + str(hash(bayes_obj.file_name)) + "ASGFEM_H1_error.png")
#                 plt.clf()
#             # endregion
#
#             # region Plot euler results
#
#             fig, arr = plt.subplots(2, 2)
#             fig_set = 1
#             for lia in range(len(bayes_obj.exp_method.local_convergence)):
#                 arr[0, 0].plot(np.array([lic for lic in range(1,
#                                                               len(bayes_obj.exp_method.local_convergence[lia])+1)]),
#                                bayes_obj.exp_method.local_convergence[lia])
#                 arr[0, 0].set_title('error of euler method with different step sizes')
#                 # arr[0, 0].xlabel('# of steps')
#                 # arr[0, 0].ylabel('MC error')
#                 arr[1, 0].loglog(np.array([lic for lic in
#                                            range(1, len(bayes_obj.exp_method.local_convergence[lia])+1)]),
#                                  bayes_obj.exp_method.local_convergence[lia])
#                 arr[1, 0].set_title('logarithmic error development')
#                 # arr[0, 0].xlabel('# of steps')
#                 # arr[0, 0].ylabel('log MC error')
#                 arr[0, 1].plot(np.array([lic for lic in
#                                          range(1, len(bayes_obj.exp_method.local_convergence[-1][100:])+1)]),
#                                bayes_obj.exp_method.local_convergence[-1][100:])
#                 arr[0, 1].set_title('last steps of error size with smallest step size')
#                 # arr[0, 0].xlabel('# of steps')
#                 # arr[0, 0].ylabel('MC error')
#             arr[1, 1].plot(bayes_obj.exp_method.step_size_list)
#             arr[1, 1].set_title('step sizes used in Euler method')
#             # arr[0, 0].ylabel('step size')
#             plt.draw()
#             plt.savefig(file_path + "M{0}_g{1}_Dof{2}".format(len(bayes_obj.true_values), bayes_obj.inp_gamma,
#                                                               len(bayes_obj.measurements)) +
#                         str(hash(bayes_obj.file_name)) + "_euler.pdf", dpi=None, facecolor='w', edgecolor='w',
#                         orientation='portrait', papertype=None, format='pdf',
#                         transparent=False, bbox_inches='tight', pad_inches=0.1,
#                         frameon=None)
#             # plt.show()
#             # endregion
#
#             # region Plot densities to file
#             plt.clf()
#             poly_mean = []
#             poly_var = []
#             poly_max_arg = []
#             for i, yi in enumerate(bayes_obj.eval_densities):
#                 plt.plot(bayes_obj.eval_grid_points, yi/bayes_obj.mean/2,
#                          label="y_{0} should {1}".format(i, bayes_obj.true_values[i]),
#                          color=color_list[i % len(color_list)])
#                 plt.xlim([-1, 1])
#                 plt.vlines(bayes_obj.true_values[i], 0, max(yi/bayes_obj.mean/2),
#                           color=color_list[i % len(color_list)])
#                 I = interp1d(bayes_obj.eval_grid_points, yi, kind='cubic')
#                 poly_mean.append(quad(lambda _x: _x*I(_x)/bayes_obj.mean*0.5, -1,
#                                       bayes_obj.eval_grid_points[-1])[0])
#                 poly_var.append(quad(lambda _x2: (_x2 - poly_mean[-1])**2 * I(_x2) / bayes_obj.mean * 0.5, -1,
#                                      bayes_obj.eval_grid_points[-1])[0])
#
#                 poly_max_arg.append(bayes_obj.eval_grid_points[np.argmax(yi)])
#             plt.legend(ncol=len(bayes_obj.eval_densities), loc=3)
#             plt.draw()
#             plt.savefig(file_path + "M{0}_g{1}_Dof{2}".format(len(bayes_obj.true_values), bayes_obj.inp_gamma,
#                                                               len(bayes_obj.measurements)) +
#                         str(hash(bayes_obj.file_name)) + "_dens.pdf", dpi=None, facecolor='w', edgecolor='w',
#                         orientation='portrait', papertype=None, format='pdf',
#                         transparent=False, bbox_inches='tight', pad_inches=0.1,
#                         frameon=None)
#             # plt.show()
#             # endregion
#             # region Plot density means
#             plt.clf()
#             x_dim = np.linspace(1, len(poly_mean), num=len(poly_mean), endpoint=True)
#             plt.semilogy(x_dim, np.abs(np.array(poly_mean) - np.array(bayes_obj.true_values)), 'or',
#                          label='mean')
#             plt.semilogy(x_dim, np.abs(np.array(poly_max_arg) - np.array(bayes_obj.true_values)), 'ob',
#                          label='argmax')
#             plt.semilogy(x_dim, [np.linalg.norm(np.array(poly_mean) - np.array(bayes_obj.true_values))] *
#                          len(x_dim),
#                          label='l2 norm mean')
#             plt.semilogy(x_dim, [np.linalg.norm(np.array(poly_max_arg) - np.array(bayes_obj.true_values))] *
#                          len(x_dim),
#                          label='l2 norm argmax')
#             plt.legend(ncol=2, loc=1)
#             plt.xlim([0.9, x_dim[-1] + 0.1])
#             plt.title('error of estimated parameters to correct values')
#             plt.xlabel('Parameter')
#             plt.ylabel('Error')
#             plt.draw()
#             plt.savefig(file_path + "M{0}_g{1}_Dof{2}".format(len(bayes_obj.true_values), bayes_obj.inp_gamma,
#                                                               len(bayes_obj.measurements)) +
#                         str(hash(bayes_obj.file_name)) + "_error.pdf", dpi=None, facecolor='w', edgecolor='w',
#                         orientation='portrait', papertype=None, format='pdf',
#                         transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None)
#             # plt.show()
#
#             # endregion
#
#         except Exception as ex:
#             print "error while solution analysis " + ex.message
#         # endregion
#
#     else:
#         log.info('start Bayesian procedure')
#         # region  setup truth, noise and prior
#         log.info("Create %i true parameters", bayes_obj.forwardOp.coefficients)
#         true_values = create_parameter_samples(bayes_obj.forwardOp.coefficients)
#         if USE_DOFS:
#             unique_dofs = np.array(create_unique_dofs(n_dofs_list[-1], init_mesh_dofs))
#         # endregion
#
#         # region Setup Gaussian Prior
#         Gauss_prior = False
#         if Gauss_prior:
#             raise NotImplemented
#             # pi_y =
#             # [lambda x: 1/(np.sqrt(2*np.pi)*sigma_i)*np.exp(-0.5*((x-mu_i)/sigma_i)**2) for mu_i, sigma_i in
#             # zip(mu, sigma)]
#
#         else:
#             prior_densities = [lambda x: 0.5 * np.ones_like(x)] * bayes_obj.forwardOp.coefficients
#         # endregion
#         if USE_DOFS:
#             measurements = create_noisy_measurement_data_dofs(bayes_obj.solution, true_values, dofs, inp_gamma)
#         else:
#             measurements = np.array([bayes_obj.forwardOp.compute_direct_sample_solution(true_values)
#                                      (sample_coordinate)
#                                      for sample_coordinate in sample_coordinates])
#         # measurements = create_noisy_measurement_data_dofs(bayes_obj.solution, true_values, dofs, inp_gamma)
#         bayes_obj.measurements = measurements
#         bayes_obj.true_values = true_values
#         bayes_obj.prior_densities = prior_densities
#
#         bayes_obj.calculate_densities()
#
#         log.info('end Bayesian procedure')
#
#         log.info('export Bayes file %s', file_path + bayes_obj.file_name + '.dat')
#         if not bayes_obj.export_bayes(file_path):
#             log.error("can not save bayes object")
#         bayes_obj_list.append(bayes_obj)
#
#     if not USE_DOFS:
#         break
#     # endregion
#
# if USE_DOFS:
#     assert len(bayes_obj_list) == len(n_dofs_list)      # plot only if enough data is found
