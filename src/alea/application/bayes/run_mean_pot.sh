#!/bin/bash
python paper_conv_potential.py -re 1000 -md 150000 -sr 2 -gd 1 -nc 2 -at decay-inf -fm 2 -de 2 -bp results/paper/potential/decay-inf/new/ -imd 10 -pp ../paper-tt-bayesian-inversion/results/decay-inf/big/testPot/l2/ -mc 100 -cpu 50 -ny 8 #
