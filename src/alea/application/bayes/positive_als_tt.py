from __future__ import division
import numpy as np
from alea.utils.tictoc import TicToc

import matplotlib.pyplot as plt
from alea.application.tt_asgfem.tensorsolver.tt_util import tt_nmf_from_full, tt_round_qr
import tt


def create_scattered_data(sample_size, mean_list, variance_list):
    assert (len(mean_list) == len(variance_list))
    retval = np.zeros((sample_size, len(mean_list)))
    for lia in range(len(mean_list)):
        retval[:, lia] = [value if value >= 0 else mean_list[lia] for value in (np.random.randn(sample_size)*variance_list[lia] + mean_list[lia])]
    return retval



# region Test scattered data function
# test_mat = create_scattered_data(100, mean_list=[6, 8, 5], variance_list=[0.8, 1, 0.5])
# plt.scatter(test_mat[:, 0], test_mat[:, 1], color="blue")
# plt.scatter(test_mat[:, 1], test_mat[:, 2], color="orange")
# plt.scatter(test_mat[:, 0], test_mat[:, 2], color="red")
# plt.xlim([0, 15])
# plt.ylim([0, 15])
# plt.show()
# exit()

from PIL import Image

fname = 'example4.png'
black_and_white = False
image = None
if black_and_white is True:
    image = Image.open(fname).convert("L")
else:
    image = Image.open(fname)
arr_load = np.asarray(image)
print(arr_load.shape)

def rgb_to_id(rgb_arr, base=256):
    assert (len(rgb_arr) == 3)
    return base**2*rgb_arr[2] + base*rgb_arr[1] + rgb_arr[0]

def id_to_rgb(index):
    index = np.array(index, dtype=np.uint32)
    return index.view(np.uint8).reshape(index.shape+(4,))[..., :3]

arr = arr_load

# print("singular values of image: {}".format(np.linalg.svd(arr_load, compute_uv=False, full_matrices=False)))
# arr = arr_load*np.linalg.norm(arr_load)**(-0.7)
# factor = np.linalg.norm(arr)**(-0.7)
factor = 1

arr = arr * factor

# print("singular values of normalized image: {}".format(np.linalg.svd(arr, compute_uv=False, full_matrices=False)))
# print("rank of image: {}".format(np.linalg.matrix_rank(arr)))
# endregion

n = arr.shape[0]
m = arr.shape[1]
niter = []
# A = np.random.rand(n, m) + 1.1
# A = np.dot(A.T, A) + np.eye(A.shape[0], A.shape[1])
# A = arr
# np.real(arr).flatten('F'), self.n, self.d, eps, rmax)
#             else:
#                 self.r, self.ps = _tt_f90.tt_f90.dfull_to_tt(
#                     _np.real(a).flatten('F'), self.n, self.d, eps)
#             self.core = _tt_f90.tt_f90.core.copy()
#         _tt_f90.tt_f90.tt_dealloc()

rank = 5

tt_arr = tt.vector(arr, eps=1e-15)
orig_arr = max(tt_arr.r)
print("original full tensor hast specs: \n {}".format(tt_arr.full().shape))
print("original tensor has specifications: \n {}".format(tt_arr))
print("original image shape = {}".format(arr.shape))
print("difference: {}".format(np.linalg.norm(tt_arr.full() - arr)))
#for lia, core in enumerate(tt.vector.to_list(tt_arr)):
#    print("core {}: {}".format(lia, core))
# print("original image matrix rank = {}".format(np.linalg.matrix_rank(arr)))


# here non negative TT-HOSVD
tt_arr_round = tt.vector.round(tt_arr, 1e-15, rmax=rank)
tt_round_own, err = tt_round_qr(tt_arr, rank)
tt_round_own = tt.vector.from_list(tt_round_own)
tt_round_own_nmf = tt_nmf_from_full(arr, rank)
for lia in range(len(tt_round_own_nmf)):
    for lib in range(tt_round_own_nmf[lia].shape[0]):
        for lic in range(tt_round_own_nmf[lia].shape[1]):
            for lid in range(tt_round_own_nmf[lia].shape[2]):
                if tt_round_own_nmf[lia][lib, lic, lid] < 0:
                    print("negative entries in optimized tensor core ({}) at [{}, {}, {}]".format(lia, lib, lic, lid))
tt_round_own_nmf = tt.vector.from_list(tt_round_own_nmf)
# ########

print("new image rank = {}".format(rank))
print("difference own_round to osel_round: {}".format(tt.vector.norm(tt_arr_round - tt_round_own)))
print("difference nmf_round to osel_round: {}".format(tt.vector.norm(tt_arr_round - tt_round_own_nmf)))
plt.figure(1)
ax_top_0 = plt.subplot2grid((1, 4), (0, 0))
ax_top_1 = plt.subplot2grid((1, 4), (0, 1))
ax_top_2 = plt.subplot2grid((1, 4), (0, 2))
ax_top_3 = plt.subplot2grid((1, 4), (0, 3))

ax_top_0.imshow(np.array(arr, dtype=np.uint8))
ax_top_0.set_title("original rank={}".format(orig_arr))
# if black_and_white is True:
#     ax_top_1.imshow(np.dot(w_log, h_log) * factor ** (-1))
ax_top_1.imshow(np.array(tt_round_own.full(), dtype=np.uint8))
ax_top_1.set_title("tt own rank={}".format(max(tt_round_own.r)))

ax_top_2.imshow(np.array(tt_arr_round.full(), dtype=np.uint8))
ax_top_2.set_title("low-rank tt rank={}".format(max(tt_arr_round.r)))

ax_top_3.imshow(np.array(tt_round_own_nmf.full(), dtype=np.uint8))
ax_top_3.set_title("nmf tt rank={}".format(max(tt_round_own_nmf.r)))

plt.legend()
plt.show()
