# region Imports
from __future__ import division, print_function
import numpy as np
from scipy.optimize import minimize
from numpy.polynomial.hermite_e import hermegauss
import itertools
import h5py
from alea.utils.tictoc import TicToc
from alea.application.bayes.alea_util.configuration.asgfem_config import AsgfemConfig
from alea.application.bayes.alea_util.coefficient_field.tensor.tt_lognormal_semidiscrete_field import \
    (read_coef_from_config)
from alea.application.bayes.alea_util.coefficient_field.tensor.tt_lognormal_fullydiscrete_field import \
    TTLognormalFullyDiscreteField
from alea.application.bayes.Bayes_util.ForwardOperator.tt_lognormal_asgfem_operator_data import \
    TTLognormalAsgfemOperatorData
from alea.application.bayes.Bayes_util.ForwardOperator.tt_lognormal_asgfem_operator import (TTLognormalAsgfemOperator,
                                                                                            solution_cache)
from alea.application.bayes.Bayes_util.ForwardOperator.tt_forward_operator_util import calculate_stiffness_matrix
from alea.application.egsz.sample_problems2 import SampleProblem
from alea.application.egsz.sample_domains import SampleDomain

from alea.application.tt_asgfem.tensorsolver.tt_lognormal import sample_lognormal_tt

from alea.linalg.tensor.extended_tt import ExtendedTT, BasisType, get_base_change_tensor
from alea.linalg.tensor.extended_fem_tt import ExtendedFEMTT

from alea.polyquad.polynomials import StochasticHermitePolynomials

from alea.application.bayes.Bayes_util.lib.interpolation_util import get_hermite_points
from alea.application.bayes.Bayes_util.exponential.embedded_runge_kutta import EmbeddedRungeKutta
from alea.application.bayes.Bayes_util.exponential.extendedtt_embedded_rk import ExtendedTTEmbeddedRungeKutta
from dolfin import (FunctionSpace, Function, refine, interpolate)

import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

from itertools import product
from copy import deepcopy
import xerus as xe
import tt

import tarfile
from alea.utils.progress.bar import Bar
import cPickle as Pickle
np.random.seed(8190)
# endregion

problem_file = "../../alea_util/configuration/problems/" + "paper_lognormal_P3-sigma2-thy05-thx05.pro"
cache_file = "../../Bayes_util/ForwardOperator/tmp/" + "P3-sigma2.txt"

test_sol = False
test_obs = False
test_diff = False
test_noise = False
test_prod = False
test_pot = False
test_euler = False
test_reconstruction = False

do_reconstruction = False
do_euler = True


# region pre-computations and object creation
# region Read configuration from problem file
with TicToc(sec_key="Configuration", key="read configuration", do_print=False, active=True):
    config = AsgfemConfig(problem_file)
    if config.read_configuration() is False:
        exit()
# endregion

# region read coefficient from configuration
with TicToc(sec_key="Semi-Disc Coef", key="read from configuration", do_print=False, active=True):
    semi_discrete_coef = read_coef_from_config(config)
# endregion

# region try to read solution data from file
try:
    read_solutions = []
    if False:
        from os.path import isfile
        f = open(cache_file, 'rb')
        lines = f.readlines()
        f.close()
        with tarfile.open("P3-sigma2.tar", "w") as tar:
            for line in lines:
                sol = TTLognormalAsgfemOperatorData()
                line = line.strip()
                print("try to read file: {}".format(line))
                if len(line) <= 1:
                    continue
                if sol.load(line) is False:
                    print("can not read file")
                    read_solution = None
                    break
                tar.add(sol.path + "sol.dat")
                tar.add(sol.path + "sol-mesh.xml")
                tar.add(sol.path + "new-sol-mesh.xml")
                print("found solution: ")
                print("     hedgs: {}".format(sol.hdegs))
                print("     ranks: {}".format(sol.ranks))
                print("     m-dofs: {}".format(sol.m_dofs))
                read_solutions.append(sol)
    else:
        from os.path import isfile

        f = open(cache_file, 'rb')
        lines = f.readlines()
        f.close()
        bar = Bar("Load solutions", max=len(lines)-1)
        for line in lines:
            sol = TTLognormalAsgfemOperatorData()
            line = line.strip()
            # print("try to read file: {}".format(line))
            if len(line) <= 1:
                continue
            if sol.load(line) is False:
                print("can not read file")
                read_solution = None
                break
            if False:
                print("found solution: ")
                print("     hedgs: {}".format(sol.hdegs))
                print("     ranks: {}".format(sol.ranks))
                print("     m-dofs: {}".format(sol.m_dofs))
            read_solutions.append(sol)
            bar.next()
        bar.finish()
except Exception as ex:
    print(ex)
    read_solutions = None
    exit()
# endregion

# region create reference fs and forward operator to sample from
with TicToc(sec_key="reference function space", key="create", do_print=False, active=True):

    ref_mesh, boundaries, dim = SampleDomain.setupDomain(config.domain, initial_mesh_N=2)
    #                                               # refine mesh according to input
    ref_mesh = SampleProblem.setupMesh(ref_mesh, num_refine=1)
    #                                               # refinement if you want to have a specific amount of dofs

    while FunctionSpace(ref_mesh, 'CG', config.femdegree).dim() < 1000:
        ref_mesh = refine(ref_mesh)                 # uniformly refine reference mesh
    pde, _, _, _, _, _ = SampleProblem.setupPDE(2, config.domain, 0, boundaries, semi_discrete_coef)
    ref_fs = FunctionSpace(ref_mesh, "CG", 3)
    ref_operator = TTLognormalAsgfemOperator(semi_discrete_coef, ref_fs, semi_discrete_coef.hermite_degree,
                                             config.domain, empty=True)
# endregion
# endregion

# region measurement data
n_measurements = 200
noise = 0.01
with TicToc(sec_key="Create truth", key="true parameter", do_print=False, active=True):
    true_parameters = np.random.randn(config.reference_M)*10
# true_parameters[0] = 4.2
# true_parameters[1] = -6.3
# true_parameters[2] = 5.6
# endregion

grid = np.linspace(-10, 10, num=50, endpoint=True)
n_nodes = 10
noise_mat = np.diag([noise for _ in range(n_measurements)])
noise_mat = np.linalg.inv(noise_mat)

# region Plot prior density
if False:
    def prior_den(_x): return np.sqrt(2 * np.pi) ** (-1) * np.exp(-0.5 * _x ** 2)
    fig = plt.figure()
    plt.subplot(1, 3, 1)
    plt.plot(grid, [prior_den(x) for x in grid], '-b', label="prior")

    plt.legend()
    plt.title("Prior density 1D")
    marginal_pot_values = np.zeros((len(grid), len(grid)))

    prior_density_plot = np.zeros((len(grid), len(grid)))
    bar = Bar("Plot prior density", max=len(grid) ** 2)
    for lia, x in enumerate(grid):
        for lib, y in enumerate(grid):
            prior_density_plot[lib, lia] = prior_den(x)*prior_den(y)
            bar.next()
    bar.finish()
    X, Y = np.meshgrid(grid, grid)
    plt.subplot(1, 3, 2)
    levels = np.arange(0, 1.0, 0.01)
    cont = plt.contour(X, Y, prior_density_plot, levels=levels)
    plt.colorbar(cont)
    plt.title("Prior density contour")
    ax = plt.subplot(1, 3, 3, projection='3d')
    levels = np.arange(0, 1.5, 0.1)
    cont = ax.plot_surface(X, Y, prior_density_plot, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    plt.colorbar(cont)
    plt.title("Prior density 2D")

    fig.savefig("prior_density.png")
# endregion

# sol_of_interest = [40]
sol_of_interest = [15]

for sol_index in sol_of_interest:
    sol = read_solutions[sol_index]

    curr_fs = FunctionSpace(sol.mesh, 'CG', config.femdegree)
    # region Init Base and weights
    # TODO: automatic by creating the specific lognomral Bayes object
    base = [BasisType.points] + [BasisType.TransformedNormalisedHermite] * (len(sol.solution) - 1)
    xebase = [BasisType.points] + [BasisType.Hermite] * (len(sol.solution) - 1)
    bmax = semi_discrete_coef.affine_field.get_infty_norm(config.reference_M)
    weights = [[1] * sol.solution[0].shape[1]]
    for lia in range(len(sol.solution) - 1):
        weights.append([np.exp(-config.theta * config.rho * bmax[lia])] * 100)
    # endregion

    sol_ten = ExtendedFEMTT(sol.solution, base, curr_fs, weights)
    print("forward solution used: \n{}".format(sol_ten))
    # region Test Extended FEM TT
    if test_sol:                                    # test solution as Extended TT
        assert len(sol.solution) == sol_ten.dim     # dimensionality is correct
        for lia in range(len(sol.solution)):
            #                                       # the ranks and dimension are correctly read
            assert sol.solution[lia].shape[0] == sol_ten.r[lia]
            assert sol.solution[lia].shape[1] == sol_ten.n[lia]
        #                                           # the last rank is the same
        assert sol.solution[-1].shape[2] == sol_ten.r[-1]
        assert sol.solution[-1].shape[2] == 1       # and the last rank is equal to one

        samples = [np.random.randn(len(sol.solution)-1) for lia in range(config.n_samples)]
        bar = Bar("Test solution sampling", max=len(samples) - 1)
        for sample in samples:
            max_sol = sample_lognormal_tt(sol.solution, sample, bmax, theta=config.theta, rho=config.rho)
            man_sol = sol_ten.sample(sample)
            assert len(max_sol) == len(man_sol)
            assert np.linalg.norm(max_sol - man_sol) / np.linalg.norm(max_sol) <= 1e-10
            bar.next()
        bar.finish()
    # endregion

    # region compute potential
    path = 'potential_{}_{}_{}.dat'.format(sol_index, n_measurements, noise)

    potential_ten = ExtendedTT.load(path)

    if potential_ten is None:

        #                                               # create measurements
        measurement_nodes = curr_fs.tabulate_dof_coordinates().reshape(2, -1)
        measurements = []
        potential_ten = None
        bar = Bar("potential apply measurements", max=n_measurements)
        for lia in range(n_measurements):
            sample = deepcopy(true_parameters[:(sol_ten.dim - 1)])
            for lic in range(sol_ten.dim - 1):
                sample[lic] += np.random.randn(1)*noise
            measurements.append(sol_ten.sample(sample))

            #                                               # apply difference with measurements
            diff_ten = sol_ten * (-1)
            diff_ten = diff_ten.add_vec_to_component(measurements[-1], 0)

            rhs_ten = diff_ten.copy()
            stiff = calculate_stiffness_matrix(curr_fs)
            lhs_ten = diff_ten.multiply_comp_with_matrix_left(stiff, 0)
            #                                               # multiply lhs and rhs
            product_ten = lhs_ten.multiply_with_extendedTT(rhs_ten)

            #                                               # sum over first component
            if lia == 0:
                potential_ten = product_ten.sum_component(0) * (0.5) * noise**-1
            else:
                potential_ten += product_ten.sum_component(0) * (0.5) * noise**-1
            if max(potential_ten.r) > 1000:
                potential_ten.round(tol=1e-10)
            bar.next()
        bar.finish()
        print("potential prior to rounding:\n{}".format(potential_ten))
        potential_ten.round(tol=1e-10)
        print("potential post rounding:\n{}".format(potential_ten))
        print(potential_ten)
        if potential_ten.save(path) is False:
            raise IOError("can not save potential to : {}".format(path))

    # endregion

    # region Test potential result
    if test_pot:                                    # test potential

        def marginal_pot(_x):
            return potential_ten(list(_x))

        x0 = np.zeros(potential_ten.dim)
        res = minimize(marginal_pot, x0, method="BFGS")

        z_0 = res.x

        A = np.linalg.inv(res.hess_inv)
        sigma, v = np.linalg.eig(A)
        D_inv = np.sqrt(sigma)**-1

        def trafo(node):
            return np.dot(v.dot(np.diag(D_inv)), node) + z_0


        # region plot potential
        if True:
            marginal_pot_values = np.zeros((len(grid), len(grid)))

            bar = Bar("Plot potential values", max=len(grid) ** 2)
            for lia, x in enumerate(grid):
                for lib, y in enumerate(grid):
                    node = [x, y] + list(true_parameters[2:potential_ten.dim])
                    marginal_pot_values[lib, lia] = marginal_pot(node)
                    bar.next()
            bar.finish()
            X, Y = np.meshgrid(grid, grid)
            ax = plt.subplot(2, 2, 1, projection='3d')
            levels = np.arange(0, 100, 0.1)
            cont = ax.plot_surface(X, Y, marginal_pot_values, cmap=cm.coolwarm, linewidth=0, antialiased=False)
            plt.colorbar(cont)
            plt.title("Bayesian potential 3D")
            plt.subplot(2, 2, 2)
            cont = plt.contour(X, Y, marginal_pot_values, levels=levels)
            plt.plot(true_parameters[0], true_parameters[1], 'ro')
            plt.colorbar(cont)
            plt.title("Bayesian potential contour")

            plt.subplot(2, 2, 3)
            cont = plt.contour(X, Y, marginal_pot_values, levels=levels)
            plt.plot(true_parameters[0], true_parameters[1], 'ro')
            plt.colorbar(cont)
            plt.title("Gauss-Hermite nodes")

            nodes, weights = hermegauss(n_nodes)
            bar = Bar("Plot hermite nodes", max=len(nodes) ** 2)
            for node1 in nodes:
                for node2 in nodes:
                    plt.plot(node1, node2, 'bx')
                    bar.next()
            bar.finish()
            potential_ten_point = ExtendedTT(potential_ten.components, basis=[BasisType.points]*potential_ten.dim)
            # A, b, c = potential_ten_point.compute_order_2_best_fit_monomial_hermite()
            # z_0 = np.linalg.solve(A, b) * 2

            plt.subplot(2, 2, 4)
            cont = plt.contour(X, Y, marginal_pot_values, levels=levels)
            plt.plot(z_0[0], z_0[1], 'ro')
            plt.colorbar(cont)
            plt.title("transformed nodes")

            bar = Bar("Plot trafo values", max=len(nodes)**2)
            for node in itertools.product(*[nodes] * 2):
                # node = np.array([nodes[lia] for lib in range(len(sigma))])
                node = np.array([node[0], node[1]] + [0]*len(list(true_parameters[2:potential_ten.dim])))
                trafo_node = trafo(node)
                plt.plot(trafo_node[0], trafo_node[1], 'rx')
                bar.next()
            bar.finish()

            tensor_node = np.zeros((n_nodes, len(sigma)))
            for lia in range(n_nodes):
                node = np.array([nodes[lia] for lib in range(len(sigma))])
                tensor_node[lia, :] = np.dot(np.diag(D_inv), node) + z_0
            for lia, lib in itertools.product(range(n_nodes), range(n_nodes)):
                plt.plot(tensor_node[lia, 0], tensor_node[lib, 1], 'go')
            # endregion
            plt.tight_layout(w_pad=0.1)
            plt.show()
        grid = np.linspace(-1, 1, num=50)
        levels = np.arange(0, 10, 0.01)
        marginal_pot_values_trafo = np.zeros((len(grid), len(grid)))
        bar = Bar("Plot transformed potential values", max=len(grid) ** 2)
        for lia, x in enumerate(grid):
            for lib, y in enumerate(grid):
                node = np.array([x, y] + [0]*len(list(true_parameters[2:potential_ten.dim])))
                trafo_node = trafo(node)
                marginal_pot_values_trafo[lib, lia] = marginal_pot(list(trafo_node))
                bar.next()
        bar.finish()

        X, Y = np.meshgrid(grid, grid)
        fig = plt.figure()
        cont = plt.contour(X, Y, marginal_pot_values_trafo)
        plt.colorbar(cont)
        plt.title("transformed potential")
        plt.show()

    # endregion

    # region Nodes reweighting
    nodes, weights = hermegauss(n_nodes)
    x0 = np.zeros(potential_ten.dim)

    def marginal_pot(_x):
        return potential_ten(list(_x))
    res = minimize(marginal_pot, x0, method="BFGS")
    z_0 = res.x
    sigma, v = np.linalg.eig(np.linalg.inv(res.hess_inv))
    D_inv = np.sqrt(sigma) ** -1
    # endregion

    potential_ten = -0.5 * potential_ten

    poly_dim = potential_ten.n                      # polynomial degree to fit stochastic representation

    poly_dim = [10]*potential_ten.dim

    # region Tensor reconstruction
    if do_reconstruction:
        x_dim = 1                                   # nodes in physical space is only one, since it is a density
        target_eps = 1e-8                           # desired tolerance on test data set inside ADF
        max_itr = 10000                             # maximal number of iterations in ADF

        # TODO: play with this a bit. It is unclear what you need here. usually much more in exp than before

        basis = xe.PolynomBasis.Hermite             # define flag of used polynomials

        measurements = xe.UQMeasurementSet()        # create Xerus measurement Set
        #                                           # create samples
        n_samples = n_nodes**potential_ten.dim
        do_again = False
        recon_path = 'recon{}{}{}-1.dat'.format(n_samples, n_measurements, noise)
        try:
            raise IOError
            infile = open(recon_path, 'rb')
            exp_ten_recon = Pickle.load(infile)
            infile.close()
        except IOError as ex:
            print("IOERROR can not import from file " + recon_path + " err: " + ex.message)
            do_again = True
        except EOFError as ex:
            print("EOFERROR can not import from file " + recon_path + " err: " + ex.message)
            do_again = True
        if do_again:
            bar = Bar("create reconstruction samples", max=n_samples)
            y_list = []

            # for lia in range(n_samples):          # random sampling
            #     y_list.append(np.random.randn(potential_ten.dim))

            trafo_nodes = np.zeros(n_nodes ** len(sigma))
            for node in itertools.product(*[nodes] * len(sigma)):
                y_list.append(np.dot(v.dot(np.diag(D_inv)), node) + z_0)
                value = xe.Tensor(dim=[x_dim])      # create a one dimensional tensor of size of physical space (=1)
                #                                   # set value in tensor
                value[[0]] = np.exp(potential_ten(list(y_list[-1])))
                #                                   # add parameter and sample result to measurement list
                measurements.add(list(y_list[-1]), value)
                bar.next()
            bar.finish()
            dimension = [x_dim] + poly_dim          # define tensor dimensions

            #                                       # run adf algorithm
            result = xe.uq_ra_adf(measurements, basis, dimension, target_eps, max_itr)
            #                                       # create an extended TT out of the xerus TT
            exp_ten_recon = ExtendedTT.from_xerus_tt(result, xebase)

            # the xerus TT in this case has a one dimensional first component. we need to get rid of this
            components = exp_ten_recon.components
            components[1] = np.einsum('i, ijk->jk', components[0][0, 0, :],
                                      components[1]).reshape((1, components[1].shape[1], components[1].shape[2]),
                                                             order="F")
            #                                       # final result of the reconstruction in unnormalised Hermite poly
            exp_ten_recon = ExtendedTT(components[1:], xebase[1:])
            exp_ten_recon.normalise()               # Since xerus uses unnormalised Hermite poly -> normalise
            outfile = open(recon_path, 'wb')
            Pickle.dump(exp_ten_recon, outfile, Pickle.HIGHEST_PROTOCOL)
            outfile.close()
    # endregion

    # region do euler method
    if do_euler:
        if True:
            tensor_nodes = np.zeros((n_nodes, len(sigma)))
            for lia in range(n_nodes):
                node = np.array([nodes[lia] for lib in range(len(sigma))])
                tensor_nodes[lia, :] = np.dot(np.diag(D_inv), node) + z_0
            tensor_nodes = tensor_nodes.T

            max_rank = 50
            do_again = False
            euler_path = 'euler{}{}{}{}-1.dat'.format(sol_index, max_rank, n_measurements, noise)
            try:

                infile = open(euler_path, 'rb')
                exp_ten_euler = Pickle.load(infile)
                infile.close()
            except IOError as ex:
                print("IOERROR can not import from file " + euler_path + " err: " + ex.message)
                do_again = True
            except EOFError as ex:
                print("EOFERROR can not import from file " + euler_path + " err: " + ex.message)
                do_again = True
            if do_again:
                # nodes = get_hermite_points(poly_dim)
                potential_ten_eval = potential_ten.evaluate_at_grid(tensor_nodes)
                # euler_method = EmbeddedRungeKutta(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
                #                                   euler_repetitions, euler_local_mc_samples)
                # potential_ten = ExtendedTT(potential_ten.components, basis=[BasisType.NormalisedHermite]*potential_ten.dim)
                euler_method = EmbeddedRungeKutta(1000000, 1e-8, 50, 1, 1, 20)
                euler_method.method = "forth"
                euler_method.acc = 1e-8
                euler_method.min_stepsize = 1e-19
                euler_method.max_stepsize = 0.1
                potential_ten_tt = tt.vector.from_list(potential_ten_eval.components)
                exp_ten_euler, _ = euler_method.calculate_exponential_tt(potential_ten_tt, 3)
                exp_ten_euler = ExtendedTT.from_grid_to_hermite(tt.vector.to_list(exp_ten_euler), nodes=tensor_nodes)
                exp_ten_euler.round(1e-14)

                outfile = open(euler_path, 'wb')
                Pickle.dump(exp_ten_euler, outfile, Pickle.HIGHEST_PROTOCOL)
                outfile.close()

    # endregion

    exp_ten_euler = exp_ten_euler.multiply_with_extendedTT(exp_ten_euler)

    euler_mean = exp_ten_euler.mean()
    # recon_mean = exp_ten_recon.mean()

    # region Plot exp values
    if True:
        def prior_den(_x): return np.sqrt(2*np.pi)**(-1)*np.exp(-0.5*_x**2)

        fig = plt.figure()
        mean_list = np.zeros(exp_ten_euler.dim)
        for lia in range(exp_ten_euler.dim):
            # print("Euler ten: mean= {} \n {}".format(euler_mean, exp_ten_euler))
            # print("Recon ten: mean= {} \n {}".format(recon_mean, exp_ten_recon))
            marginal_euler = exp_ten_euler.marginalise_by_dimension(lia)

            mean_list[lia] = marginal_euler.components[0][0, 1, 0]*euler_mean**-1
            # marginal_recon = exp_ten_recon.marginalise_by_dimension(lia)
            plt.subplot(1, 3, lia+1)
            plt.plot(grid, [marginal_euler([x])*euler_mean**(-1)*prior_den(x) for x in grid], '-b',
                     label="$y_{}$".format(lia+1))
            plt.axvline(x=true_parameters[lia], color='r', linestyle="dotted", label="Truth")
            plt.axvline(x=mean_list[lia], color='g', linestyle="dotted", label="Approx Mean")
            plt.legend()
            # plt.plot(grid, [marginal_recon([x])*recon_mean**(-1)*prior_den(x) for x in grid], '-r')

        fig.savefig("marginals.png")

        euler_exp_values = np.zeros((len(grid), len(grid)))
        # recon_exp_values = np.zeros((len(grid), len(grid)))

        fig = plt.figure()
        fig.subplots_adjust(hspace=0.3)

        levels = np.arange(-0.01, 1000, 0.1)
        bar = Bar("Plot exp values", max=(len(grid))*exp_ten_euler.dim)
        plot_iter = 0
        X, Y = np.meshgrid(grid, grid)
        for d in range(exp_ten_euler.dim):
            plot_iter += 1
            euler_2d_ten = exp_ten_euler.marginalise_one_dimension(d)

            for lia, x in enumerate(grid):
                for lib, y in enumerate(grid):
                    euler_exp_values[lib, lia] = euler_2d_ten([x, y]) * euler_mean**-1 * prior_den(x) * prior_den(y)
                    # recon_exp_values[lib, lia] = exp_ten_recon([x, y, true_parameters[2]]) * recon_mean
                bar.next()
            plt.subplot(3, 2, plot_iter)
            plt.title("2D marginal {}".format(d))
            cont = plt.contour(X, Y, euler_exp_values)
            plt.colorbar(cont)

            plot_iter += 1
            ax = plt.subplot(3, 2, plot_iter, projection='3d')
            cont = ax.plot_surface(X, Y, euler_exp_values, cmap=cm.coolwarm, linewidth=0, antialiased=False)
            plt.colorbar(cont)
            plt.title("2D marginal {}".format(d))
        bar.finish()
        fig.tight_layout()
        fig.savefig("corner.png")
        exit()
    # endregion

    # #######################################
    # compute projection onto field
    # #######################################
    new_hdeg = [8]*10

    semi_discrete_coef.update_coefficient(new_hdeg, config.max_rank, add_order=4, new_dimension=2, add_dim=2,
                                          max_hdeg=config.max_hdegs_coef, same_length=(config.theta_y <= 0))

    discrete_coef = TTLognormalFullyDiscreteField(semi_discrete_coef, ref_fs, config.domain)

    cont_coef = ExtendedTT(discrete_coef.discrete_coef_cores[1:],
                           [BasisType.NormalisedHermite] * (len(discrete_coef.discrete_coef_cores[1:])))

    print(cont_coef)

    # region compute prior mean
    if False:
        print("compute coefficient mean w.r.t. prior")
        R = [1]
        cont_coef_prior_mean = cont_coef.copy()
        coef_cores = cont_coef_prior_mean.components
        bar = Bar("compute remainder (mean w.r.t. prior)", max=len(coef_cores))
        for lia in range(len(coef_cores)-1, -1, -1):
            #                                       # [r_k-1, 0, r_k] x [r_k] = [r_k-1]
            R = np.dot(coef_cores[lia][:, 0, :], R)
            bar.next()
        bar.finish()
        cmap = plt.cm.jet
        coordinates = ref_fs.tabulate_dof_coordinates().reshape((-1, ref_fs.mesh().geometry().dim()))

        alpha_mean_prior = discrete_coef.discrete_coef_cores[0][0, :, :].dot(R[:])
        _fig = plt.figure()
        ax = _fig.add_subplot(111, projection="3d")
        a = ax.plot_trisurf(coordinates[:, 0], coordinates[:, 1], alpha_mean_prior, cmap=cmap)
        ax.set_title("prior coefficient mean")
        _fig.colorbar(a)

        _fig.savefig("coef_mean_prior" + ".png")
        _fig.clf()
    # endregion

    # region compute prior variance
    if False:
        assert alpha_mean_prior is not None
        print("compute coefficient variance w.r.t. prior")
        path = 'coef_prod_{}_{}.dat'.format(n_measurements, noise)
        do_again = False
        try:
            newcores = []
            with h5py.File(path, "r") as outfile:
                for lia in range(int(outfile["len"][()])):
                    newcores.append(np.array(outfile["core{}".format(lia)][()]))
            cont_coef_prior_var = ExtendedTT(newcores, basis=[BasisType.NormalisedHermite] * len(newcores))
        except IOError as ex:
            print("IOERROR can not import from file " + path + " err: " + ex.message)
            do_again = True
        except EOFError as ex:
            print("EOFERROR can not import from file " + path + " err: " + ex.message)
            do_again = True
        except KeyError as ex:
            print("KEYERROR can not import from file " + path + " err: " + ex.message)
            do_again = True
        if do_again:
            base_change = get_base_change_tensor(max(cont_coef.n), max(cont_coef.n), BasisType.NormalisedHermite)
            base_change = base_change[:, 0, :].reshape((base_change.shape[0], 1, base_change.shape[2]), order="F")
            cont_coef_prior_var = cont_coef.multiply_with_extendedTT(cont_coef, progress=True, base_change=base_change)
            # outfile = open(path, 'wb')
            # Pickle.dump(coef_cores, outfile, Pickle.HIGHEST_PROTOCOL)
            # outfile.close()
            with h5py.File(path, "w") as outfile:
                for lia in range(len(cont_coef_prior_var.components)):
                    outfile["core{}".format(lia)] = cont_coef_prior_var.components[lia]
                outfile["len"] = len(cont_coef_prior_var.components)



        coef_cores = cont_coef_prior_var.components
        R = [1]
        for lia in range(len(coef_cores)-1, -1, -1):
            core = coef_cores[lia]
            R = np.dot(core[:, 0, :], R)
        ax = _fig.add_subplot(111, projection="3d")

        R = np.reshape(R, (discrete_coef.discrete_coef_cores[0].shape[2],
                           discrete_coef.discrete_coef_cores[0].shape[2]), order='F')

        alpha_var_prior = discrete_coef.discrete_coef_cores[0][0, :, :].dot(R)

        var_fun_vec = np.zeros(alpha_var_prior.shape[0])
        for lia in range(alpha_var_prior.shape[1]):
            var_fun_vec += alpha_var_prior[:, lia] * discrete_coef.discrete_coef_cores[0][0, :, lia]
        var_fun_vec += -1 * (alpha_mean_prior**2)
        a = ax.plot_trisurf(coordinates[:, 0], coordinates[:, 1], var_fun_vec, cmap=cmap)
        ax.set_title("prior coefficient variance")
        _fig.colorbar(a)

        _fig.savefig("coef_variance_prior" + ".png")
        _fig.clf()

    # endregion

    # region Compute and plot mean of coefficient w.r.t. posterior
    if False:
        print("compute coefficient mean w.r.t. posterior")
        coef_cores = cont_coef.components
        posterior = exp_ten_euler * exp_ten_euler.mean()**-1
        post_cores = posterior.components
        R = [1]
        bar = Bar("compute remainder", max=len(coef_cores))
        for lia in range(len(coef_cores)-1, -1, -1):
            if lia > len(post_cores)-1:
                #                                       # [r_k-1, 0, r_k] x [r_k] = [r_k-1]
                R = np.dot(coef_cores[lia][:, 0, :], R)
            elif lia == len(post_cores) - 1:
                mu = min(post_cores[lia].shape[1], coef_cores[lia].shape[1])
                #                                       # [r_k-1, mu_k, r_k] x [r_k] = [r_k-1, mu_k]
                R = np.einsum('ijk, k->ij', coef_cores[lia][:, :mu, :], R)
                U = post_cores[lia][:, :mu, 0]          # [r_k-1', mu_k, 1] = [r_k-1, mu_k]
                R = np.dot(R, U.T)                      # [r_k-1, mu_k] x [mu_k, r_k-1'] = [r_k-1, r_k-1']
            else:
                mu = min(post_cores[lia].shape[1], coef_cores[lia].shape[1])
                R = np.einsum('ijk, kl->ijl', coef_cores[lia][:, :mu, :], R)
                R = np.einsum('ajl, ijl->ia', post_cores[lia][:, :mu, :], R)
            bar.next()
        bar.finish()
        _fig = plt.figure()
        cmap = plt.cm.jet
        coordinates = ref_fs.tabulate_dof_coordinates().reshape((-1, ref_fs.mesh().geometry().dim()))

        ax = _fig.add_subplot(111, projection="3d")
        print("compute projection fem coefficient")
        alpha_mean_post = discrete_coef.discrete_coef_cores[0][0, :, :].dot(R[:, 0])
        a = ax.plot_trisurf(coordinates[:, 0], coordinates[:, 1], alpha_mean_post, cmap=cmap)
        ax.set_title("posterior coefficient mean")
        _fig.colorbar(a)
        _fig.savefig("coef_mean_post" + ".png")
        _fig.clf()
    # endregion

    # region Plot true coefficient field
    class coef_cache:
        def __init__(self):
            self.func_list = []
            self.fs = ref_fs


    cache = coef_cache()
    if False:


        print("compute true fem coefficient")
        cmap = plt.cm.jet
        coordinates = ref_fs.tabulate_dof_coordinates().reshape((-1, ref_fs.mesh().geometry().dim()))
        # alpha_true = semi_discrete_coef.sample_continuous_field(coordinates, list(true_parameters)[:3], cache=cache)
        alpha_true = semi_discrete_coef.sample_continuous_field(coordinates, list(true_parameters), cache=cache)
        _fig = plt.figure()
        ax = _fig.add_subplot(111, projection="3d")
        a = ax.plot_trisurf(coordinates[:, 0], coordinates[:, 1], alpha_true, cmap=cmap)
        ax.set_title("true coefficient at $y^*$")
        _fig.colorbar(a)
        _fig.savefig("coef_true" + ".png")
        _fig.clf()
    # endregion
    # region Plot cont coef at mean parameters
    if True:
        mean_list = np.zeros(exp_ten_euler.dim)
        for lia in range(exp_ten_euler.dim):
            # print("Euler ten: mean= {} \n {}".format(euler_mean, exp_ten_euler))
            # print("Recon ten: mean= {} \n {}".format(recon_mean, exp_ten_recon))
            marginal_euler = exp_ten_euler.marginalise_by_dimension(lia)

            mean_list[lia] = marginal_euler.components[0][0, 1, 0]*euler_mean**-1
        print("compute y* fem coefficient")
        cmap = plt.cm.jet
        coordinates = ref_fs.tabulate_dof_coordinates().reshape((-1, ref_fs.mesh().geometry().dim()))
        alpha_y = semi_discrete_coef.sample_continuous_field(coordinates, list(mean_list), cache=cache)
        _fig = plt.figure()
        ax = _fig.add_subplot(111, projection="3d")
        a = ax.plot_trisurf(coordinates[:, 0], coordinates[:, 1], alpha_y, cmap=cmap)
        ax.set_title("coefficient at mean of parameter")
        _fig.colorbar(a)
        _fig.savefig("coef_mean_para" + ".png")
        _fig.clf()
    # endregion

    # region Compute and plot variance of coefficient w.r.t. posterior
    if False:
        assert alpha_mean_post is not None
        print("compute coefficient variance w.r.t. posterior")


        coef_cores_var = cont_coef.copy()
        posterior = exp_ten_euler * exp_ten_euler.mean() ** -1
        path = 'coef_coef_post_prod_{}_{}.dat'.format(n_measurements, noise)
        do_again = False
        try:
            newcores = []
            with h5py.File(path, "r") as outfile:
                for lia in range(int(outfile["len"][()])):
                    newcores.append(np.array(outfile["core{}".format(lia)][()]))
            coef_post = ExtendedTT(newcores, basis=[BasisType.NormalisedHermite] * len(newcores))
        except IOError as ex:
            print("IOERROR can not import from file " + path + " err: " + ex.message)
            do_again = True
        except EOFError as ex:
            print("EOFERROR can not import from file " + path + " err: " + ex.message)
            do_again = True
        except KeyError as ex:
            print("KEYERROR can not import from file " + path + " err: " + ex.message)
            do_again = True
        if do_again:
            print("round coef tensor")
            coef_cores_var.round(1e-10)
            print("round post_tensor")
            posterior.round(1e-10)
            print("multiply coef with posterior")
            coef_post = coef_cores_var.multiply_with_extendedTT(posterior)
            print("round coef x posterior")
            coef_post.round(1e-10)
            print("multiply again with coefficient")
            base_change = get_base_change_tensor(max(coef_post.n), max(coef_post.n), BasisType.NormalisedHermite)
            base_change = base_change[:, 0, :].reshape((base_change.shape[0], 1, base_change.shape[2]), order="F")
            coef_post = coef_post.multiply_with_extendedTT(coef_cores_var, progress=True, base_change=base_change)
            with h5py.File(path, "w") as outfile:
                for lia in range(len(coef_post.components)):
                    outfile["core{}".format(lia)] = coef_post.components[lia]
                outfile["len"] = len(coef_post.components)



        print("start remainder computation")
        R = [1]
        for lia in range(len(coef_post.components)-1, -1, -1):
            #                                       # [r_k-1, 0, r_k] x [r_k] = [r_k-1]
            R = np.dot(coef_post.components[lia][:, 0, :], R)
        # coef_cores_var = coef_cores.components
        # post_cores = posterior.components
        # print("start remainder computation")
        # R = [1]
        # bar = Bar("compute remainder", max=len(coef_cores_var))
        # assert len(coef_cores_var) > len(post_cores)
        # for lia in range(len(coef_cores_var)-1, -1, -1):
        #
        #     if lia > len(post_cores)-1:
        #         #                                       # [r_k-1, 0, r_k] x [r_k] = [r_k-1]
        #         R = np.dot(coef_cores_var[lia][:, 0, :], R)
        #     elif lia == len(post_cores) - 1:
        #         mu = min(post_cores[lia].shape[1], coef_cores_var[lia].shape[1])
        #         #                                       # [r_k-1, mu_k, r_k] x [r_k] = [r_k-1, mu_k]
        #         R = np.einsum('ijk, k->ij', coef_cores_var[lia][:, :mu, :], R)
        #         assert post_cores[lia].shape[2] == 1
        #         U = post_cores[lia][:, :mu, 0]          # [r_k-1', mu_k, 1] = [r_k-1, mu_k]
        #         R = np.dot(R, U.T)                      # [r_k-1, mu_k] x [mu_k, r_k-1'] = [r_k-1, r_k-1']
        #     else:
        #         mu = min(post_cores[lia].shape[1], coef_cores_var[lia].shape[1])
        #         R = np.einsum('ijk, kl->ijl', coef_cores_var[lia][:, :mu, :], R)
        #         R = np.einsum('ajl, ijl->ia', post_cores[lia][:, :mu, :], R)
        #     bar.next()
        # bar.finish()

        print("compute projection fem coefficient")
        print(R.shape)
        print(discrete_coef.discrete_coef_cores[0].shape[2])
        R = np.reshape(R, (discrete_coef.discrete_coef_cores[0].shape[2],
                           discrete_coef.discrete_coef_cores[0].shape[2]), order='F')

        alpha_var = discrete_coef.discrete_coef_cores[0][0, :, :].dot(R)

        var_fun_vec = np.zeros(alpha_var.shape[0])
        for lia in range(alpha_var.shape[1]):
            var_fun_vec += alpha_var[:, lia]*discrete_coef.discrete_coef_cores[0][0, :, lia]
        var_fun_vec += -1 * (alpha_mean_post**2)

        _fig = plt.figure()
        cmap = plt.cm.jet
        coordinates = ref_fs.tabulate_dof_coordinates().reshape((-1, ref_fs.mesh().geometry().dim()))

        ax = _fig.add_subplot(111, projection="3d")
        a = ax.plot_trisurf(coordinates[:, 0], coordinates[:, 1], var_fun_vec, cmap=cmap)
        ax.set_title("posterior coefficient variance")
        _fig.colorbar(a)
        _fig.savefig("coef_variance_post" + ".png")
        _fig.clf()
    # endregion
