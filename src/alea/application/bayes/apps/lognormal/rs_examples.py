# region Imports
from __future__ import division, print_function
import numpy as np
from scipy.optimize import minimize
from numpy.polynomial.hermite_e import hermegauss
import itertools
import h5py
from alea.utils.tictoc import TicToc
from alea.application.bayes.alea_util.configuration.asgfem_config import AsgfemConfig
from alea.application.bayes.alea_util.coefficient_field.tensor.tt_lognormal_semidiscrete_field import \
    (read_coef_from_config)
from alea.application.bayes.alea_util.coefficient_field.tensor.tt_lognormal_fullydiscrete_field import \
    TTLognormalFullyDiscreteField
from alea.application.bayes.Bayes_util.ForwardOperator.tt_lognormal_asgfem_operator_data import \
    TTLognormalAsgfemOperatorData
from alea.application.bayes.Bayes_util.ForwardOperator.tt_lognormal_asgfem_operator import (TTLognormalAsgfemOperator,
                                                                                            solution_cache)
from alea.application.bayes.Bayes_util.ForwardOperator.tt_forward_operator_util import (calculate_stiffness_matrix,
                                                                                        calculate_mass_matrix)
from alea.application.egsz.sample_problems2 import SampleProblem
from alea.application.egsz.sample_domains import SampleDomain

from alea.application.tt_asgfem.tensorsolver.tt_lognormal import sample_lognormal_tt

from alea.linalg.tensor.extended_tt import ExtendedTT, BasisType, get_base_change_tensor
from alea.linalg.tensor.extended_fem_tt import ExtendedFEMTT

from alea.polyquad.polynomials import StochasticHermitePolynomials

from alea.application.bayes.Bayes_util.lib.interpolation_util import get_hermite_points
from alea.application.bayes.Bayes_util.exponential.embedded_runge_kutta import EmbeddedRungeKutta
from alea.application.bayes.Bayes_util.exponential.implicit_euler import ImplicitEuler
from alea.application.bayes.Bayes_util.exponential.extendedtt_embedded_rk import ExtendedTTEmbeddedRungeKutta
from dolfin import (FunctionSpace, Function, refine, interpolate)

import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

from itertools import product
from copy import deepcopy
import xerus as xe
import tt

import tarfile
from alea.utils.progress.bar import Bar
import cPickle as Pickle
np.random.seed(8190)
from_array = xe.Tensor.from_ndarray
# endregion

# region define the problem file
#                                                   # define the configuration used for the forward problem
problem_file = "../../alea_util/configuration/problems/"
small_example = True
if small_example:
    problem_file_name = "M10_d4_sigma2_P3"
else:
    problem_file_name = "M16_d8_sigma2_P3"

problem_file += problem_file_name + ".pro"
#                                                   # define the cache file containing the solution
cache_file = "../../Bayes_util/ForwardOperator/tmp/"
if small_example:
    cache_file_name = "M10_d4_sigma2_P3"
else:
    cache_file_name = "M16_d8_sigma2_P3"
cache_file += cache_file_name + ".txt"
if small_example:
    sol_index = 0                                       # For M16 use 1, for M10 use 0
else:
    sol_index = 1

# endregion

# region Flags
test_sol = False
test_obs = False
test_diff = False
test_noise = False
test_prod = False
test_pot = False
test_euler = False
test_reconstruction = False

do_reconstruction = False
do_euler = False
do_adf = True

print_found_solutions = True
plot_prior_density = False

measurement_norm = "H1"
# measurement_norm = "L2"
# measurement_norm = "l2"

use_transformation = True

recalc_pot = True
recalc_adf_start = True
recalc_exp = True
# endregion

# region pre-computations and object creation
# region Read configuration from problem file
with TicToc(sec_key="Configuration", key="read configuration", do_print=False, active=True):
    config = AsgfemConfig(problem_file)
    if config.read_configuration() is False:
        exit()
# endregion

# region read coefficient from configuration
with TicToc(sec_key="Semi-Disc Coef", key="read from configuration", do_print=False, active=True):
    semi_discrete_coef = read_coef_from_config(config)
# endregion

# region try to read solution data from file
try:
    read_solutions = []
    from os.path import isfile

    f = open(cache_file, 'rb')
    lines = f.readlines()
    f.close()
    bar = Bar("Load solutions", max=len(lines)-1)
    for line in lines:
        sol = TTLognormalAsgfemOperatorData()
        line = line.strip()
        # print("try to read file: {}".format(line))
        if len(line) <= 1:
            continue
        if sol.load(line) is False:
            print("can not read file")
            read_solution = None
            break
        if print_found_solutions:
            print("found solution: ")
            print("     hedgs: {}".format(sol.hdegs))
            print("     ranks: {}".format(sol.ranks))
            print("     m-dofs: {}".format(sol.m_dofs))
        read_solutions.append(sol)
        bar.next()
    bar.finish()
except Exception as ex:
    print(ex)
    # ############## Solve the problem
    curr_rank = [1] + [config.start_rank]*config.n_coefficients + [1]
    if isinstance(config.gpc_degree, list):
        curr_hdeg = config.gpc_degree
    else:
        curr_hdeg = [config.hermite_degree]*config.n_coefficients
    # region create fs
    with TicToc(sec_key="initial function space", key="create", do_print=False, active=True):
        if config.init_mesh_dofs > 0:  # mesh dofs are defined
            mesh_refine = 0  # there are no mesh refinements
        else:
            mesh_refine = config.mesh_refine  # use configured mesh refinements

        mesh, boundaries, dim = SampleDomain.setupDomain(config.domain, initial_mesh_N=2)
        #                                               # refine mesh according to input
        mesh = SampleProblem.setupMesh(mesh, num_refine=mesh_refine)
        #                                               # refinement if you want to have a specific amount of dofs

        print("soll mesh_dofs={} haben dofs={}".format(config.init_mesh_dofs,
                                                       FunctionSpace(mesh, 'CG', config.femdegree).dim()))
        while config.init_mesh_dofs > 0 and FunctionSpace(mesh, 'CG', config.femdegree).dim() < config.init_mesh_dofs:
            mesh = refine(mesh)  # uniformly refine mesh
        pde, _, _, _, _, _ = SampleProblem.setupPDE(2, config.domain, 0, boundaries, semi_discrete_coef)
        curr_fs = FunctionSpace(mesh, 'CG', config.femdegree)
    # endregion
    # region check coefficient field dimension
    semi_discrete_coef.update_coefficient(curr_hdeg, config.max_rank, add_order=4, new_dimension=2, add_dim=2,
                                          max_hdeg=config.max_hdegs_coef)
    # endregion
    # region create current forward operator
    with TicToc(sec_key="Forward Operator", key="create new operator", do_print=False, active=True):
        curr_forward_operator = TTLognormalAsgfemOperator(semi_discrete_coef, curr_fs, curr_hdeg, config.domain)
    # endregion
    # region solve the problem
    curr_forward_operator.solve(curr_rank, config.preconditioner, config.iterations, config.als_tol, [])
    # endregion
    refined_solution = curr_forward_operator.refine_adaptive(config.theta_x, config.theta_y,
                                                             config.resnorm_zeta_weight,
                                                             config.eta_zeta_weight,
                                                             config.rank_always, config.sol_rank,
                                                             config.start_rank, config.new_hdeg
                                                             )
    curr_forward_operator.save_to_hash(cache_file)
    print("found solution: ")
    sol = curr_forward_operator.solution
    print("     hedgs: {}".format(sol.hdegs))
    print("     ranks: {}".format(sol.ranks))
    print("     m-dofs: {}".format(sol.m_dofs))
    read_solutions = [sol]
    exit()
# endregion

# region create reference fs and forward operator to sample from
with TicToc(sec_key="reference function space", key="create", do_print=False, active=True):

    ref_mesh, boundaries, dim = SampleDomain.setupDomain(config.domain, initial_mesh_N=2)
    #                                               # refine mesh according to input
    ref_mesh = SampleProblem.setupMesh(ref_mesh, num_refine=1)
    #                                               # refinement if you want to have a specific amount of dofs

    while FunctionSpace(ref_mesh, 'CG', config.femdegree).dim() < 1000:
        ref_mesh = refine(ref_mesh)                 # uniformly refine reference mesh
    pde, _, _, _, _, _ = SampleProblem.setupPDE(2, config.domain, 0, boundaries, semi_discrete_coef)
    ref_fs = FunctionSpace(ref_mesh, "CG", 3)
    ref_operator = TTLognormalAsgfemOperator(semi_discrete_coef, ref_fs, semi_discrete_coef.hermite_degree,
                                             config.domain, empty=True)
# endregion
# endregion

# region measurement data
n_measurements = 100                                # number of measurements
noise = 0.01                                        # noise level
with TicToc(sec_key="Create truth", key="true parameter", do_print=False, active=True):
    true_parameters = np.random.randn(config.reference_M)*5
# true_parameters[0] = 0.2
# true_parameters[1] = -0.3
# true_parameters[2] = 5.6
# endregion

grid = np.linspace(-5, 5, num=50, endpoint=True)  # define the plot grid
n_nodes = 6                                       # number of polynomial nodes used for interpolation
noise_mat = np.diag([noise for _ in range(n_measurements)])
noise_mat = np.linalg.inv(noise_mat)

# region Plot prior density
if plot_prior_density:
    def prior_den(_x): return np.sqrt(2 * np.pi) ** (-1) * np.exp(-0.5 * _x ** 2)
    fig = plt.figure()
    plt.subplot(1, 3, 1)
    plt.plot(grid, [prior_den(x) for x in grid], '-b', label="prior")

    plt.legend()
    plt.title("Prior density 1D")
    marginal_pot_values = np.zeros((len(grid), len(grid)))

    prior_density_plot = np.zeros((len(grid), len(grid)))
    bar = Bar("Plot prior density", max=len(grid) ** 2)
    for lia, x in enumerate(grid):
        for lib, y in enumerate(grid):
            prior_density_plot[lib, lia] = prior_den(x)*prior_den(y)
            bar.next()
    bar.finish()
    X, Y = np.meshgrid(grid, grid)
    plt.subplot(1, 3, 2)
    levels = np.arange(0, 1.0, 0.01)
    cont = plt.contour(X, Y, prior_density_plot, levels=levels)
    plt.colorbar(cont)
    plt.title("Prior density contour")
    ax = plt.subplot(1, 3, 3, projection='3d')
    levels = np.arange(0, 1.5, 0.1)
    cont = ax.plot_surface(X, Y, prior_density_plot, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    plt.colorbar(cont)
    plt.title("Prior density 2D")

    fig.savefig("prior_density.png")
# endregion


sol = read_solutions[sol_index]

curr_fs = FunctionSpace(sol.mesh, 'CG', config.femdegree)
# region Init Base and weights
# TODO: automatic by creating the specific lognomral Bayes object
base = [BasisType.points] + [BasisType.TransformedNormalisedHermite] * (len(sol.solution) - 1)
xebase = [BasisType.points] + [BasisType.Hermite] * (len(sol.solution) - 1)
bmax = semi_discrete_coef.affine_field.get_infty_norm(config.reference_M)
ten_weights = [[1] * sol.solution[0].shape[1]]
for lia in range(len(sol.solution) - 1):
    ten_weights.append([np.exp(-config.theta * config.rho * bmax[lia])] * 1000)
# endregion

sol_ten = ExtendedFEMTT(sol.solution, base, curr_fs, ten_weights)
print("forward solution used: \n{}".format(sol_ten))
# region Test Extended FEM TT
if test_sol:                                    # test solution as Extended TT
    assert len(sol.solution) == sol_ten.dim     # dimensionality is correct
    for lia in range(len(sol.solution)):
        #                                       # the ranks and dimension are correctly read
        assert sol.solution[lia].shape[0] == sol_ten.r[lia]
        assert sol.solution[lia].shape[1] == sol_ten.n[lia]
    #                                           # the last rank is the same
    assert sol.solution[-1].shape[2] == sol_ten.r[-1]
    assert sol.solution[-1].shape[2] == 1       # and the last rank is equal to one

    samples = [np.random.randn(len(sol.solution)-1) for lia in range(config.n_samples)]
    bar = Bar("Test solution sampling", max=len(samples) - 1)
    for sample in samples:
        max_sol = sample_lognormal_tt(sol.solution, sample, bmax, theta=config.theta, rho=config.rho)
        man_sol = sol_ten.sample(sample)
        assert len(max_sol) == len(man_sol)
        assert np.linalg.norm(max_sol - man_sol) / np.linalg.norm(max_sol) <= 1e-10
        bar.next()
    bar.finish()
# endregion

# region compute potential
if small_example:
    path = 'short_sol_potential_{}_{}_{}_{}.dat'.format(sol_index, n_measurements, noise, measurement_norm)
else:
    path = 'long_sol_potential_{}_{}_{}_{}.dat'.format(sol_index, n_measurements, noise, measurement_norm)

if recalc_pot:
    potential_ten = None
else:
    potential_ten = ExtendedTT.load(path)

if potential_ten is None:

    #                                               # create measurements
    measurement_nodes = curr_fs.tabulate_dof_coordinates().reshape(2, -1)
    measurements = []
    potential_ten = None
    bar = Bar("potential apply measurements", max=n_measurements)
    for lia in range(n_measurements):
        if measurement_norm == "l2":
            raise NotImplementedError
        sample = deepcopy(true_parameters[:(sol_ten.dim - 1)])
        for lic in range(sol_ten.dim - 1):
            sample[lic] += np.random.randn(1)*noise
        measurements.append(sol_ten.sample(sample))

        #                                               # apply difference with measurements
        diff_ten = sol_ten * (-1)
        diff_ten = diff_ten.add_vec_to_component(measurements[-1], 0)

        rhs_ten = diff_ten.copy()
        if measurement_norm == "H1":
            noise_scale = calculate_stiffness_matrix(curr_fs)
        elif measurement_norm == "L2":
            noise_scale = calculate_mass_matrix(curr_fs)
        else:
            raise ValueError("unknown measurement norm type")
        lhs_ten = diff_ten.multiply_comp_with_matrix_left(noise_scale, 0)
        #                                               # multiply lhs and rhs
        product_ten = lhs_ten.multiply_with_extendedTT(rhs_ten)

        #                                               # sum over first component
        if lia == 0:
            potential_ten = product_ten.sum_component(0) * noise**-1
        else:
            potential_ten += product_ten.sum_component(0) * noise**-1
        if max(potential_ten.r) > 150:
            potential_ten.round(tol=1e-16)
        bar.next()
    bar.finish()
    print("potential prior to rounding:\n{}".format(potential_ten))
    potential_ten.round(tol=1e-15)
    print("potential post rounding:\n{}".format(potential_ten))
    print(potential_ten)
    if potential_ten.save(path) is False:
        raise IOError("can not save potential to : {}".format(path))

# endregion

# region Test potential result
if test_pot:                                    # test potential

    def marginal_pot(_x):
        return potential_ten(list(_x))


    print("  compute jacobian")
    jacobian = potential_ten.get_jacobian()
    print("  compute hessian")
    hessian = potential_ten.get_hessian()


    def tt_jac(_x, args=None):
        return np.array([_ten(_x) for _ten in jacobian])


    def tt_hes(_x, args=None):
        retval = np.zeros((potential_ten.dim, potential_ten.dim))
        for _lia in range(retval.shape[0]):
            for _lib in range(retval.shape[1]):
                retval[_lia, _lib] = hessian[_lia][_lib](_x)
        return retval

    if use_transformation:

        x0 = np.zeros(potential_ten.dim)
        res = minimize(marginal_pot, x0, method="Newton-CG", jac=tt_jac, hess=tt_hes)
        print(res)
        z_0 = res.x
        A = tt_hes(z_0)
        sigma, v = np.linalg.eig(A)
        D_inv = np.sqrt(sigma)**-1
    else:
        z_0 = true_parameters[:potential_ten.dim]

        grad = tt_jac(z_0)
        print("jacobian should be close to 0: \n{}".format(grad))
        A = tt_hes(z_0)

        sigma, v = np.linalg.eig(A)
        D_inv = np.sqrt(sigma) ** -1

    def trafo(node):
        return np.dot(v.dot(np.diag(D_inv)), node) + z_0


    # region plot potential

    fig = plt.figure()
    potential_mean = potential_ten.mean()
    max_n = 4
    for lia in range(max_n):
        for lib in range(lia+1):
            if lia == lib:
                marginal_euler = potential_ten.marginalise_by_dimension(lia)

                curr_mean = marginal_euler.components[0][0, 1, 0] * potential_mean ** -1
                # marginal_recon = exp_ten_recon.marginalise_by_dimension(lia)
                ax = plt.subplot(max_n, max_n, (lia * max_n) + (lia + 1))
                plt.plot(grid, [marginal_euler([x]) * potential_mean ** -1 for x in grid], '-b',
                         label="$y_{}$".format(lia + 1))
                plt.axvline(x=true_parameters[lia], color='r', linestyle="dotted", label="Truth")
                plt.axvline(x=curr_mean, color='g', linestyle="dotted", label="Approx Mean")
                ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
                continue
            print("marginalize dimension {}".format((lia, lib)))
            marginal_pot_ten = potential_ten.copy()

            marginal_pot_ten = marginal_pot_ten.marginalise_all_but_two_dimension(lia, lib)
            print(marginal_pot_ten)
            assert marginal_pot_ten.dim == 2

            marginal_pot_values = np.zeros((len(grid), len(grid)))

            # bar = Bar("Plot potential values", max=len(grid) * potential_ten.dim)
            for i, x in enumerate(grid):
                for j, y in enumerate(grid):
                    node = [x, y]
                    marginal_pot_values[j, i] = marginal_pot_ten(node)
                    # bar.next()
            # bar.finish()
            X, Y = np.meshgrid(grid, grid)
            ax = plt.subplot(max_n, max_n, (lia*max_n) + (lib+1))
            levels = None #np.linspace(np.min(marginal_pot_values), np.min(marginal_pot_values)+500, num=20)
            extend = None #'max'
            cont = plt.contour(X, Y, marginal_pot_values, levels=levels, extend=extend)
            plt.plot(true_parameters[lia], true_parameters[lib], 'ro')
            # plt.colorbar(cont)
            # plt.title("Bayesian potential contour")
    plt.tight_layout()
    fig.savefig("potential_1-4.png")

    fig = plt.figure()
    for lia in range(max_n):
        for lib in range(lia+1):
            if lia == lib:
                marginal_euler = potential_ten.marginalise_by_dimension(lia)

                curr_mean = marginal_euler.components[0][0, 1, 0] * potential_mean ** -1
                # marginal_recon = exp_ten_recon.marginalise_by_dimension(lia)
                plt.subplot(max_n, max_n, (lia * max_n) + (lia + 1))
                values = np.zeros(len(grid))
                for i in range(len(grid)):
                    node = np.array(list(true_parameters[:lia]) + list([grid[i]]) + list(true_parameters[(lia+1):potential_ten.dim]))

                    values[i] = marginal_euler([trafo(node)[lia]])
                plt.plot(grid, values * potential_mean ** -1 , '-b',
                         label="$y_{}$".format(lia + 1))
                plt.axvline(x=true_parameters[lia], color='r', linestyle="dotted", label="Truth")
                plt.axvline(x=curr_mean, color='g', linestyle="dotted", label="Approx Mean")
                continue
            print("marginalize dimension {}".format((lia, lib)))
            marginal_pot_ten = potential_ten.copy()

            marginal_pot_ten = marginal_pot_ten.marginalise_all_but_two_dimension(lia, lib)
            print(marginal_pot_ten)
            assert marginal_pot_ten.dim == 2

            marginal_pot_values = np.zeros((len(grid), len(grid)))

            # bar = Bar("Plot potential values", max=len(grid) * potential_ten.dim)
            for i, x in enumerate(grid):
                for j, y in enumerate(grid):
                    node = list(true_parameters[:lib]) + list([x]) + list(true_parameters[(lib+1):lia]) + list([y]) + list(true_parameters[(lia+1):potential_ten.dim])
                    trafo_node = trafo(np.array(node))
                    trafo_node = list([trafo_node[lia], trafo_node[lib]])
                    marginal_pot_values[j, i] = marginal_pot_ten(trafo_node)
                    # bar.next()
            # bar.finish()
            X, Y = np.meshgrid(grid, grid)
            ax = plt.subplot(max_n, max_n, (lia*max_n) + (lib+1))
            levels = None #np.linspace(np.min(marginal_pot_values), np.min(marginal_pot_values)+500, num=20)
            extend = None #'max'
            cont = plt.contour(X, Y, marginal_pot_values, levels=levels, extend=extend)
            plt.plot(true_parameters[lia], true_parameters[lib], 'ro')
            # plt.colorbar(cont)
            # plt.title("Bayesian potential contour")
    plt.tight_layout()
    fig.savefig("trafo_potential_1-4.png")


    fig = plt.figure()
    potential_mean = potential_ten.mean()
    max_n = 4
    for lia in range(max_n, max_n+4):
        for lib in range(max_n, lia+1):
            if lia == lib:
                marginal_euler = potential_ten.marginalise_by_dimension(lia)

                curr_mean = marginal_euler.components[0][0, 1, 0] * potential_mean ** -1
                # marginal_recon = exp_ten_recon.marginalise_by_dimension(lia)
                plt.subplot(max_n, max_n, ((lia-max_n) * max_n) + ((lia-max_n) + 1))
                plt.plot(grid, [marginal_euler([x]) * potential_mean ** -1 for x in grid], '-b',
                         label="$y_{}$".format(max_n+lia + 1))
                plt.axvline(x=true_parameters[lia], color='r', linestyle="dotted", label="Truth")
                plt.axvline(x=curr_mean, color='g', linestyle="dotted", label="Approx Mean")
                continue
            print("marginalize dimension {}".format((max_n+lia, max_n+lib)))
            marginal_pot_ten = potential_ten.copy()

            marginal_pot_ten = marginal_pot_ten.marginalise_all_but_two_dimension(lia, lib)
            print(marginal_pot_ten)
            assert marginal_pot_ten.dim == 2

            marginal_pot_values = np.zeros((len(grid), len(grid)))

            # bar = Bar("Plot potential values", max=len(grid) * potential_ten.dim)
            for i, x in enumerate(grid):
                for j, y in enumerate(grid):
                    node = [x, y]
                    marginal_pot_values[j, i] = marginal_pot_ten(node)
                    # bar.next()
            # bar.finish()
            X, Y = np.meshgrid(grid, grid)
            ax = plt.subplot(max_n, max_n, ((lia-max_n)*max_n) + ((lib-max_n)+1))
            levels = None  # np.linspace(np.min(marginal_pot_values), np.min(marginal_pot_values)+500, num=20)
            extend = None  # 'max'
            cont = plt.contour(X, Y, marginal_pot_values, levels=levels, extend=extend)
            plt.plot(true_parameters[lia], true_parameters[lib], 'ro')
            # plt.colorbar(cont)
            # plt.title("Bayesian potential contour")
    plt.tight_layout()
    fig.savefig("potential_5-9.png")
    # endregion

# endregion

# region Nodes reweighting
nodes, weights = hermegauss(n_nodes)
x0 = np.zeros(potential_ten.dim)


def marginal_pot(_x):
    return potential_ten(list(_x))


if use_transformation:
    print("start minimization")
    print("  compute jacobian")
    jacobian = potential_ten.get_jacobian()
    print("  compute hessian")
    hessian = potential_ten.get_hessian()

    def tt_jac(_x, args=None):
        return np.array([_ten(_x) for _ten in jacobian])

    def tt_hes(_x, args=None):
        retval = np.zeros((potential_ten.dim, potential_ten.dim))
        for _lia in range(retval.shape[0]):
            for _lib in range(retval.shape[1]):
                retval[_lia, _lib] = hessian[_lia][_lib](_x)
        return retval
    print("  begin minimize")
    glob_min = np.inf
    glob_argmin = None
    # x0 = true_parameters[:potential_ten.dim]
    # x0 = true_parameters[:potential_ten.dim] + np.random.randn(potential_ten.dim)
    for lia in range(1):
        res = minimize(marginal_pot, x0, jac=tt_jac, hess=tt_hes, method="Newton-CG", tol=1e-5, options={'disp': True})
        # print(res)
        if not res.success:
            print("no termination -> start again with current value")
            x0 = res.x
            continue
        #  x0 = true_parameters[:potential_ten.dim] + np.random.randn(potential_ten.dim)
        curr_argmin = res.x
        curr_min = marginal_pot(curr_argmin)
        if curr_min < glob_min or True:
            print("new minimum {} > {} at {}".format(glob_min, curr_min, curr_argmin))
            print("true parameter: {}".format(true_parameters[:potential_ten.dim]))
            glob_min = curr_min
            glob_argmin = curr_argmin
    # z_0 = res.x
    z_0 = glob_argmin
    # print(res)
    A = tt_hes(z_0)
    sigma, v = np.linalg.eig(A)
    D_inv = np.sqrt(sigma) ** -1
else:
    z_0 = x0
    D_inv = np.eye(potential_ten.dim, potential_ten.dim)
    sigma = np.eye(potential_ten.dim, potential_ten.dim)
    v = np.eye(potential_ten.dim, potential_ten.dim)


def trafo(_node):
    return np.dot(v.dot(np.diag(D_inv)), _node) + z_0


# endregion

potential_ten = -0.5 * potential_ten

# region Tensor reconstruction
if do_reconstruction:
    # poly_dim = potential_ten.n                      # polynomial degree to fit stochastic representation
    poly_dim = [10]*potential_ten.dim
    x_dim = 1                                   # nodes in physical space is only one, since it is a density
    target_eps = 1e-8                           # desired tolerance on test data set inside ADF
    max_itr = 10000                             # maximal number of iterations in ADF

    # TODO: play with this a bit. It is unclear what you need here. usually much more in exp than before

    basis = xe.PolynomBasis.Hermite             # define flag of used polynomials

    measurements = xe.UQMeasurementSet()        # create Xerus measurement Set
    #                                           # create samples
    n_samples = n_nodes**potential_ten.dim
    do_again = False
    recon_path = 'recon{}{}{}-1.dat'.format(n_samples, n_measurements, noise)
    try:
        raise IOError
        infile = open(recon_path, 'rb')
        exp_ten_recon = Pickle.load(infile)
        infile.close()
    except IOError as ex:
        print("IOERROR can not import from file " + recon_path + " err: " + ex.message)
        do_again = True
    except EOFError as ex:
        print("EOFERROR can not import from file " + recon_path + " err: " + ex.message)
        do_again = True
    if do_again:
        bar = Bar("create reconstruction samples", max=n_samples)
        y_list = []

        # for lia in range(n_samples):          # random sampling
        #     y_list.append(np.random.randn(potential_ten.dim))

        trafo_nodes = np.zeros(n_nodes ** len(sigma))
        for node in itertools.product(*[nodes] * len(sigma)):
            y_list.append(np.dot(v.dot(np.diag(D_inv)), node) + z_0)
            value = xe.Tensor(dim=[x_dim])      # create a one dimensional tensor of size of physical space (=1)
            #                                   # set value in tensor
            value[[0]] = np.exp(potential_ten(list(y_list[-1])))
            #                                   # add parameter and sample result to measurement list
            measurements.add(list(y_list[-1]), value)
            bar.next()
        bar.finish()
        dimension = [x_dim] + poly_dim          # define tensor dimensions

        #                                       # run adf algorithm
        result = xe.uq_ra_adf(measurements, basis, dimension, target_eps, max_itr)
        #                                       # create an extended TT out of the xerus TT
        exp_ten_recon = ExtendedTT.from_xerus_tt(result, xebase)

        # the xerus TT in this case has a one dimensional first component. we need to get rid of this
        components = exp_ten_recon.components
        components[1] = np.einsum('i, ijk->jk', components[0][0, 0, :],
                                  components[1]).reshape((1, components[1].shape[1], components[1].shape[2]),
                                                         order="F")
        #                                       # final result of the reconstruction in unnormalised Hermite poly
        exp_ten_recon = ExtendedTT(components[1:], xebase[1:])
        exp_ten_recon.normalise()               # Since xerus uses unnormalised Hermite poly -> normalise
        outfile = open(recon_path, 'wb')
        Pickle.dump(exp_ten_recon, outfile, Pickle.HIGHEST_PROTOCOL)
        outfile.close()
# endregion

# region ADF as reconstruction algorithm
if do_adf:

    y_list = []
    measurements = xe.RankOneMeasurementSet()
    nodes, weights = hermegauss(n_nodes)
    nodelist = np.tile(nodes, potential_ten.dim).reshape(potential_ten.dim, -1)

    from alea.polyquad.polynomials import StochasticHermitePolynomials
    polys = StochasticHermitePolynomials(normalised=True)

    import SpectralToolbox as ST
    ADF = xe.ADFVariant(100, 1e-12, 0.99999)  # maxIterations = 1000
    # targetResidualNorm = 1e-8
    # minimalResidualNormDecrease = 0.999
    pd = xe.PerformanceData(True)  # print = True
    #                                                   # run adf algorithm
    maxRanks = [100] * (len(potential_ten.r)-2)

    print("Create start tensor: ")

    # region create ADF start tensor
    if recalc_adf_start:
        exp_ten_euler = None
    else:
        exp_ten_euler = ExtendedTT.load("adf_starttensor" + problem_file_name + ".dat")

    if exp_ten_euler is None:
        exp_component_list = []
        exp_basis_list = []
        exp_weight_list = []
        for lia in range(potential_ten.dim):
            curr_comp = np.zeros((1, potential_ten.n[lia], 1))
            curr_comp[0, 0, 0] = 1
            exp_component_list.append(curr_comp)
            exp_basis_list.append(BasisType.TransformedNormalisedHermite)
            exp_weight_list.append(ten_weights[lia+1])

        print("    1")
        exp_ten_euler = ExtendedTT(exp_component_list, exp_basis_list, exp_weight_list)
        print(exp_ten_euler)

        print("    1 + X")
        exp_ten_euler = exp_ten_euler + potential_ten
        exp_ten_euler.canonicalize_left()
        exp_ten_euler.round(1e-10)
        print(exp_ten_euler)

        print("    1 + X + 0.5 X^2")
        print("      multiply")
        potential_ten_sq = potential_ten.multiply_with_extendedTT(potential_ten, progress=True)
        # potential_ten_sq.round(1e-6)
        print("      add")
        exp_ten_euler = exp_ten_euler + potential_ten_sq * 0.5
        print("      canonicalize")
        exp_ten_euler.canonicalize_left()
        print("      round")
        exp_ten_euler.round(1e-10)
        print(exp_ten_euler)
        if False:
            print("    1 + X + 1/2 X^2 + 1/6 X^3")
            potential_ten_cu = potential_ten.multiply_with_extendedTT(potential_ten_sq, progress=True)
            potential_ten_cu.round(1e-6)
            exp_ten_euler = exp_ten_euler + potential_ten_cu * (1/6)
            exp_ten_euler.round(1e-6)
            print("    1 + X + 1/2 X^2 + 1/6 X^3 + 1/24 X^4")
            potential_ten_fo = potential_ten.multiply_with_extendedTT(potential_ten_cu, progress=True)
            potential_ten_fo.round(1e-6)
            exp_ten_euler = exp_ten_euler + potential_ten_fo * (1 / 24)
            exp_ten_euler.round(1e-6)
        exp_ten_euler.save("adf_starttensor" + problem_file_name + ".dat")
    # endregion
    # region Sample start tensor
    if False:
        mc_err = 0
        mc_norm = 0
        bar = Bar("sample exponential initial value", max=100)
        for lia in range(100):
            y = np.random.randn(exp_ten_euler.dim)
            true_value = np.exp(potential_ten(y))
            mc_norm += true_value
            approx_value = exp_ten_euler(y)
            print("true = {} vs approx = {}".format(true_value, approx_value))
            mc_err += np.abs(true_value - approx_value)
            bar.next()
        bar.finish()
        print("exponential MC error of initial value: {}".format(mc_err / mc_norm))
    # endregion
    if recalc_exp:
        exp_ten_euler_load = None # ExtendedTT.load("adf_solution.dat")
    else:
        exp_ten_euler_load = ExtendedTT.load("adf_solution.dat")

    exp_ten_euler = exp_ten_euler.xe_round(tol=1e-6, max_r=500)
    if exp_ten_euler_load is None:
        print("init sg")
        dim = exp_ten_euler.dim
        level = 7 # max(potential_ten.n)
        sg = ST.SparseGrids.SparseGrid(ST.SparseGrids.GQN, dim, level, None)
        print("generate points of dimension {} and level {}".format(dim, level))
        (nodelist, weights) = sg.sparseGrid()

        print(nodelist.shape)
        bar = Bar("Create reconstruction samples", max=nodelist.shape[0])

        def _eval(_node):
            return [[polys.eval(_lib, __node*ten_weights[_lia][_lib], all_degrees=False)
                     for _lib in range(exp_ten_euler.n[_lia])] for (_lia, __node) in enumerate(_node)]

        for lia in range(nodelist.shape[0]):
            bar.next()
            node = trafo(nodelist[lia, :])

            val = np.exp(potential_ten(node))
            if val < 1e-6:
                continue
            # print("use value= {} at node {}".format(val, node))
            pos = _eval(node)
            ten_list = [from_array(ten) for ten in pos]
            measurements.add(ten_list, val)
        bar.finish()
        print("    convert to xerus TT")
        exp_ten_euler = exp_ten_euler.to_xerus_tt()
        print(exp_ten_euler.ranks())
        print(exp_ten_euler.dimensions)
        print("    Start ADF")
        ADF(exp_ten_euler, measurements, maxRanks, pd)
        exp_ten_euler = ExtendedTT.from_xerus_tt(exp_ten_euler,
                                                 basis=[BasisType.TransformedNormalisedHermite]*potential_ten.dim,
                                                 weights=ten_weights[1:])
        print("round solution")
        exp_ten_euler = exp_ten_euler.cut(50)
        exp_ten_euler = exp_ten_euler.xe_round(tol=1e-6)
        print(exp_ten_euler)
        print("multiply with itself")
        exp_ten_euler = exp_ten_euler.multiply_with_extendedTT(exp_ten_euler)
        print("save solution")
        exp_ten_euler.save("adf_solution.dat")
    else:
        exp_ten_euler = exp_ten_euler_load
    print("exponential result: {}".format(exp_ten_euler))

# endregion

# region do euler method
if do_euler:
    if True:
        tensor_nodes = np.zeros((n_nodes, len(sigma)))
        for lia in range(n_nodes):
            node = np.array([nodes[lia] for lib in range(len(sigma))])
            tensor_nodes[lia, :] = np.dot(np.diag(D_inv), node) + z_0
        tensor_nodes = tensor_nodes.T

        max_rank = 50
        do_again = False
        euler_path = 'euler{}{}{}{}-1.dat'.format(sol_index, max_rank, n_measurements, noise)
        try:
            raise IOError
            infile = open(euler_path, 'rb')
            exp_ten_euler = Pickle.load(infile)
            infile.close()
        except IOError as ex:
            print("IOERROR can not import from file " + euler_path + " err: " + ex.message)
            do_again = True
        except EOFError as ex:
            print("EOFERROR can not import from file " + euler_path + " err: " + ex.message)
            do_again = True
        if do_again:
            # nodes = get_hermite_points(poly_dim)
            potential_ten_eval = potential_ten.evaluate_at_grid(tensor_nodes)
            potential_ten_eval.round(1e-14)
            # euler_method = EmbeddedRungeKutta(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
            #                                   euler_repetitions, euler_local_mc_samples)
            # potential_ten = ExtendedTT(potential_ten.components, basis=[BasisType.NormalisedHermite]*potential_ten.dim)
            euler_method = EmbeddedRungeKutta(1000000, 1e-8, 50, 1, 1, 20)
            euler_method = ImplicitEuler(3, 1e-6, 50, 1, 10, 20)
            euler_method.method = "forth"
            euler_method.acc = 1e-8
            euler_method.min_stepsize = 1e-19
            euler_method.max_stepsize = 0.1
            potential_ten_tt = tt.vector.from_list(potential_ten_eval.components)
            # exp_ten_euler, _ = euler_method.calculate_exponential_tt(potential_ten_tt, 3)
            exp_ten_euler, _ = euler_method.calculate_exponential_tt(potential_ten_tt)
            exp_ten_euler = ExtendedTT.from_grid_to_hermite(tt.vector.to_list(exp_ten_euler), _nodes=tensor_nodes)
            exp_ten_euler.round(1e-14)

            outfile = open(euler_path, 'wb')
            Pickle.dump(exp_ten_euler, outfile, Pickle.HIGHEST_PROTOCOL)
            outfile.close()

        exp_ten_euler = exp_ten_euler.multiply_with_extendedTT(exp_ten_euler)
# endregion

euler_mean = exp_ten_euler.mean()
# recon_mean = exp_ten_recon.mean()

# region Plot exp values
if True:
    def prior_den(_x): return np.sqrt(2*np.pi)**(-1)*np.exp(-0.5*_x**2)

    fig = plt.figure()
    mean_list = np.zeros(exp_ten_euler.dim)
    max_n = 4
    for lia in range(max_n):
        for lib in range(lia + 1):
            if lia == lib:
                marginal_euler = exp_ten_euler.marginalise_by_dimension(lia)

                curr_mean = marginal_euler.components[0][0, 1, 0] * euler_mean ** -1
                mean_list[lia] = curr_mean
                # marginal_recon = exp_ten_recon.marginalise_by_dimension(lia)
                ax = plt.subplot(max_n, max_n, (lia * max_n) + (lia + 1))
                plt.plot(grid, [marginal_euler([x]) * euler_mean ** -1 * prior_den(x) for x in grid], '-b',
                         label="$y_{}$".format(lia + 1))
                plt.axvline(x=true_parameters[lia], color='r', linestyle="dotted", label="Truth")
                plt.axvline(x=curr_mean, color='g', linestyle="dotted", label="Approx Mean")
                ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
                continue
            print("marginalize dimension {}".format((lia, lib)))
            marginal_pot_ten = exp_ten_euler.copy()

            marginal_pot_ten = marginal_pot_ten.marginalise_all_but_two_dimension(lia, lib)
            print(marginal_pot_ten)
            assert marginal_pot_ten.dim == 2

            marginal_pot_values = np.zeros((len(grid), len(grid)))

            # bar = Bar("Plot potential values", max=len(grid) * potential_ten.dim)
            for i, x in enumerate(grid):
                for j, y in enumerate(grid):
                    node = [x, y]
                    marginal_pot_values[j, i] = marginal_pot_ten(node) * prior_den(x) * prior_den(y)
                    # bar.next()
            # bar.finish()
            X, Y = np.meshgrid(grid, grid)
            ax = plt.subplot(max_n, max_n, (lia * max_n) + (lib + 1))
            levels = None  # np.linspace(np.min(marginal_pot_values), np.min(marginal_pot_values)+500, num=20)
            extend = None  # 'max'
            cont = plt.contour(X, Y, marginal_pot_values, levels=levels, extend=extend)
            plt.plot(true_parameters[lia], true_parameters[lib], 'ro')
            # plt.colorbar(cont)
            # plt.title("Bayesian potential contour")
    plt.tight_layout()
    fig.savefig("exponential_1-4.png")
    fig = plt.figure()
    for lia in range(max_n, max_n+max_n):
        for lib in range(max_n, lia + 1):
            if lia == lib:
                marginal_euler = exp_ten_euler.marginalise_by_dimension(lia)

                curr_mean = marginal_euler.components[0][0, 1, 0] * euler_mean ** -1
                mean_list[lia] = curr_mean
                # marginal_recon = exp_ten_recon.marginalise_by_dimension(lia)
                ax = plt.subplot(max_n, max_n, ((lia-max_n) * max_n) + ((lia-max_n) + 1))
                plt.plot(grid, [marginal_euler([x]) * euler_mean ** -1 * prior_den(x) for x in grid], '-b',
                         label="$y_{}$".format(lia + 1))
                plt.axvline(x=true_parameters[lia], color='r', linestyle="dotted", label="Truth")
                plt.axvline(x=curr_mean, color='g', linestyle="dotted", label="Approx Mean")
                ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
                continue
            print("marginalize dimension {}".format((lia, lib)))
            marginal_pot_ten = exp_ten_euler.copy()

            marginal_pot_ten = marginal_pot_ten.marginalise_all_but_two_dimension(lia, lib)
            print(marginal_pot_ten)
            assert marginal_pot_ten.dim == 2

            marginal_pot_values = np.zeros((len(grid), len(grid)))

            # bar = Bar("Plot potential values", max=len(grid) * potential_ten.dim)
            for i, x in enumerate(grid):
                for j, y in enumerate(grid):
                    node = [x, y]
                    marginal_pot_values[j, i] = marginal_pot_ten(node) * prior_den(x) * prior_den(y)
                    # bar.next()
            # bar.finish()
            X, Y = np.meshgrid(grid, grid)
            ax = plt.subplot(max_n, max_n, ((lia-max_n) * max_n) + ((lib-max_n) + 1))
            levels = None  # np.linspace(np.min(marginal_pot_values), np.min(marginal_pot_values)+500, num=20)
            extend = None  # 'max'
            cont = plt.contour(X, Y, marginal_pot_values, levels=levels, extend=extend)
            plt.plot(true_parameters[lia], true_parameters[lib], 'ro')
            # plt.colorbar(cont)
            # plt.title("Bayesian potential contour")
    plt.tight_layout()
    fig.savefig("exponential_5-8.png")
    euler_exp_values = np.zeros((len(grid), len(grid)))
    # recon_exp_values = np.zeros((len(grid), len(grid)))
    print("approx mean={}".format(mean_list))
    print("true mean=  {}".format(true_parameters[:potential_ten.dim]))
    if False:
        fig = plt.figure()
        fig.subplots_adjust(hspace=0.3)

        levels = np.arange(-0.01, 1000, 0.1)
        bar = Bar("Plot exp values", max=(len(grid))*exp_ten_euler.dim)
        plot_iter = 0
        X, Y = np.meshgrid(grid, grid)
        for d in range(exp_ten_euler.dim):
            plot_iter += 1
            # euler_2d_ten = exp_ten_euler.marginalise_one_dimension(d)
            euler_2d_ten = exp_ten_euler.copy()
            for lia, x in enumerate(grid):
                for lib, y in enumerate(grid):
                    euler_exp_values[lib, lia] = euler_2d_ten([x, y]) * euler_mean**-1 * prior_den(x) * prior_den(y)
                    # recon_exp_values[lib, lia] = exp_ten_recon([x, y, true_parameters[2]]) * recon_mean
                bar.next()
            plt.subplot(3, 2, plot_iter)
            plt.title("2D marginal {}".format(d))
            cont = plt.contour(X, Y, euler_exp_values)
            plt.colorbar(cont)

            plot_iter += 1
            ax = plt.subplot(3, 2, plot_iter, projection='3d')
            cont = ax.plot_surface(X, Y, euler_exp_values, cmap=cm.coolwarm, linewidth=0, antialiased=False)
            plt.colorbar(cont)
            plt.title("2D marginal {}".format(d))
        bar.finish()
        fig.tight_layout()
        fig.savefig("corner.png")

# endregion

# #######################################
# compute projection onto field
# #######################################
new_hdeg = [8]*10

semi_discrete_coef.update_coefficient(new_hdeg, config.max_rank, add_order=4, new_dimension=2, add_dim=2,
                                      max_hdeg=config.max_hdegs_coef, same_length=(config.theta_y <= 0))

discrete_coef = TTLognormalFullyDiscreteField(semi_discrete_coef, ref_fs, config.domain)

cont_coef = ExtendedTT(discrete_coef.discrete_coef_cores[1:],
                       [BasisType.NormalisedHermite] * (len(discrete_coef.discrete_coef_cores[1:])))

print(cont_coef)

# region compute prior mean
if False:
    print("compute coefficient mean w.r.t. prior")
    R = [1]
    cont_coef_prior_mean = cont_coef.copy()
    coef_cores = cont_coef_prior_mean.components
    bar = Bar("compute remainder (mean w.r.t. prior)", max=len(coef_cores))
    for lia in range(len(coef_cores)-1, -1, -1):
        #                                       # [r_k-1, 0, r_k] x [r_k] = [r_k-1]
        R = np.dot(coef_cores[lia][:, 0, :], R)
        bar.next()
    bar.finish()
    cmap = plt.cm.jet
    coordinates = ref_fs.tabulate_dof_coordinates().reshape((-1, ref_fs.mesh().geometry().dim()))

    alpha_mean_prior = discrete_coef.discrete_coef_cores[0][0, :, :].dot(R[:])
    _fig = plt.figure()
    ax = _fig.add_subplot(111, projection="3d")
    a = ax.plot_trisurf(coordinates[:, 0], coordinates[:, 1], alpha_mean_prior, cmap=cmap)
    ax.set_title("prior coefficient mean")
    _fig.colorbar(a)

    _fig.savefig("coef_mean_prior" + ".png")
    _fig.clf()
# endregion

# region compute prior variance
if False:
    assert alpha_mean_prior is not None
    print("compute coefficient variance w.r.t. prior")
    path = 'coef_prod_{}_{}.dat'.format(n_measurements, noise)
    do_again = False
    try:
        newcores = []
        with h5py.File(path, "r") as outfile:
            for lia in range(int(outfile["len"][()])):
                newcores.append(np.array(outfile["core{}".format(lia)][()]))
        cont_coef_prior_var = ExtendedTT(newcores, basis=[BasisType.NormalisedHermite] * len(newcores))
    except IOError as ex:
        print("IOERROR can not import from file " + path + " err: " + ex.message)
        do_again = True
    except EOFError as ex:
        print("EOFERROR can not import from file " + path + " err: " + ex.message)
        do_again = True
    except KeyError as ex:
        print("KEYERROR can not import from file " + path + " err: " + ex.message)
        do_again = True
    if do_again:
        base_change = get_base_change_tensor(max(cont_coef.n), max(cont_coef.n), BasisType.NormalisedHermite)
        base_change = base_change[:, 0, :].reshape((base_change.shape[0], 1, base_change.shape[2]), order="F")
        cont_coef_prior_var = cont_coef.multiply_with_extendedTT(cont_coef, progress=True, base_change=base_change)
        # outfile = open(path, 'wb')
        # Pickle.dump(coef_cores, outfile, Pickle.HIGHEST_PROTOCOL)
        # outfile.close()
        with h5py.File(path, "w") as outfile:
            for lia in range(len(cont_coef_prior_var.components)):
                outfile["core{}".format(lia)] = cont_coef_prior_var.components[lia]
            outfile["len"] = len(cont_coef_prior_var.components)



    coef_cores = cont_coef_prior_var.components
    R = [1]
    for lia in range(len(coef_cores)-1, -1, -1):
        core = coef_cores[lia]
        R = np.dot(core[:, 0, :], R)
    ax = _fig.add_subplot(111, projection="3d")

    R = np.reshape(R, (discrete_coef.discrete_coef_cores[0].shape[2],
                       discrete_coef.discrete_coef_cores[0].shape[2]), order='F')

    alpha_var_prior = discrete_coef.discrete_coef_cores[0][0, :, :].dot(R)

    var_fun_vec = np.zeros(alpha_var_prior.shape[0])
    for lia in range(alpha_var_prior.shape[1]):
        var_fun_vec += alpha_var_prior[:, lia] * discrete_coef.discrete_coef_cores[0][0, :, lia]
    var_fun_vec += -1 * (alpha_mean_prior**2)
    a = ax.plot_trisurf(coordinates[:, 0], coordinates[:, 1], var_fun_vec, cmap=cmap)
    ax.set_title("prior coefficient variance")
    _fig.colorbar(a)

    _fig.savefig("coef_variance_prior" + ".png")
    _fig.clf()

# endregion

# region Compute and plot mean of coefficient w.r.t. posterior
if True:
    print("compute coefficient mean w.r.t. posterior")
    coef_cores = cont_coef.components
    posterior = exp_ten_euler * exp_ten_euler.mean()**-1
    post_cores = posterior.components
    R = [1]
    bar = Bar("compute remainder", max=len(coef_cores))
    for lia in range(len(coef_cores)-1, -1, -1):
        if lia > len(post_cores)-1:
            #                                       # [r_k-1, 0, r_k] x [r_k] = [r_k-1]
            R = np.dot(coef_cores[lia][:, 0, :], R)
        elif lia == len(post_cores) - 1:
            mu = min(post_cores[lia].shape[1], coef_cores[lia].shape[1])
            #                                       # [r_k-1, mu_k, r_k] x [r_k] = [r_k-1, mu_k]
            R = np.einsum('ijk, k->ij', coef_cores[lia][:, :mu, :], R)
            U = post_cores[lia][:, :mu, 0]          # [r_k-1', mu_k, 1] = [r_k-1, mu_k]
            R = np.dot(R, U.T)                      # [r_k-1, mu_k] x [mu_k, r_k-1'] = [r_k-1, r_k-1']
        else:
            mu = min(post_cores[lia].shape[1], coef_cores[lia].shape[1])
            R = np.einsum('ijk, kl->ijl', coef_cores[lia][:, :mu, :], R)
            R = np.einsum('ajl, ijl->ia', post_cores[lia][:, :mu, :], R)
        bar.next()
    bar.finish()
    _fig = plt.figure()
    cmap = plt.cm.jet
    coordinates = ref_fs.tabulate_dof_coordinates().reshape((-1, ref_fs.mesh().geometry().dim()))

    ax = _fig.add_subplot(111, projection="3d")
    print("compute projection fem coefficient")
    alpha_mean_post = discrete_coef.discrete_coef_cores[0][0, :, :].dot(R[:, 0])
    a = ax.plot_trisurf(coordinates[:, 0], coordinates[:, 1], alpha_mean_post, cmap=cmap)
    ax.set_title("posterior coefficient mean")
    _fig.colorbar(a)
    _fig.savefig("coef_mean_post" + ".png")
    _fig.clf()
# endregion

# region Plot true coefficient field
class coef_cache:
    def __init__(self):
        self.func_list = []
        self.fs = ref_fs


cache = coef_cache()
if True:


    print("compute true fem coefficient")
    cmap = plt.cm.jet
    coordinates = ref_fs.tabulate_dof_coordinates().reshape((-1, ref_fs.mesh().geometry().dim()))
    # alpha_true = semi_discrete_coef.sample_continuous_field(coordinates, list(true_parameters)[:3], cache=cache)
    alpha_true = semi_discrete_coef.sample_continuous_field(coordinates, list(true_parameters), cache=cache)
    _fig = plt.figure()
    ax = _fig.add_subplot(111, projection="3d")
    a = ax.plot_trisurf(coordinates[:, 0], coordinates[:, 1], alpha_true, cmap=cmap)
    ax.set_title("true coefficient at $y^*$")
    _fig.colorbar(a)
    _fig.savefig("coef_true" + ".png")
    _fig.clf()
# endregion
# region Plot cont coef at mean parameters
if True:
    mean_list = np.zeros(exp_ten_euler.dim)
    for lia in range(exp_ten_euler.dim):
        # print("Euler ten: mean= {} \n {}".format(euler_mean, exp_ten_euler))
        # print("Recon ten: mean= {} \n {}".format(recon_mean, exp_ten_recon))
        marginal_euler = exp_ten_euler.marginalise_by_dimension(lia)

        mean_list[lia] = marginal_euler.components[0][0, 1, 0]*euler_mean**-1
    print("compute y* fem coefficient")
    cmap = plt.cm.jet
    coordinates = ref_fs.tabulate_dof_coordinates().reshape((-1, ref_fs.mesh().geometry().dim()))
    alpha_y = semi_discrete_coef.sample_continuous_field(coordinates, list(mean_list), cache=cache)
    _fig = plt.figure()
    ax = _fig.add_subplot(111, projection="3d")
    a = ax.plot_trisurf(coordinates[:, 0], coordinates[:, 1], alpha_y, cmap=cmap)
    ax.set_title("coefficient at mean of parameter")
    _fig.colorbar(a)
    _fig.savefig("coef_mean_para" + ".png")
    _fig.clf()
# endregion

# region Compute and plot variance of coefficient w.r.t. posterior
if False:
    assert alpha_mean_post is not None
    print("compute coefficient variance w.r.t. posterior")


    coef_cores_var = cont_coef.copy()
    posterior = exp_ten_euler * exp_ten_euler.mean() ** -1
    path = 'coef_coef_post_prod_{}_{}.dat'.format(n_measurements, noise)
    do_again = False
    try:
        newcores = []
        with h5py.File(path, "r") as outfile:
            for lia in range(int(outfile["len"][()])):
                newcores.append(np.array(outfile["core{}".format(lia)][()]))
        coef_post = ExtendedTT(newcores, basis=[BasisType.NormalisedHermite] * len(newcores))
    except IOError as ex:
        print("IOERROR can not import from file " + path + " err: " + ex.message)
        do_again = True
    except EOFError as ex:
        print("EOFERROR can not import from file " + path + " err: " + ex.message)
        do_again = True
    except KeyError as ex:
        print("KEYERROR can not import from file " + path + " err: " + ex.message)
        do_again = True
    if do_again:
        print("round coef tensor")
        coef_cores_var.round(1e-10)
        print("round post_tensor")
        posterior.round(1e-10)
        print("multiply coef with posterior")
        coef_post = coef_cores_var.multiply_with_extendedTT(posterior)
        print("round coef x posterior")
        coef_post.round(1e-10)
        print("multiply again with coefficient")
        base_change = get_base_change_tensor(max(coef_post.n), max(coef_post.n), BasisType.NormalisedHermite)
        base_change = base_change[:, 0, :].reshape((base_change.shape[0], 1, base_change.shape[2]), order="F")
        coef_post = coef_post.multiply_with_extendedTT(coef_cores_var, progress=True, base_change=base_change)
        with h5py.File(path, "w") as outfile:
            for lia in range(len(coef_post.components)):
                outfile["core{}".format(lia)] = coef_post.components[lia]
            outfile["len"] = len(coef_post.components)



    print("start remainder computation")
    R = [1]
    for lia in range(len(coef_post.components)-1, -1, -1):
        #                                       # [r_k-1, 0, r_k] x [r_k] = [r_k-1]
        R = np.dot(coef_post.components[lia][:, 0, :], R)
    # coef_cores_var = coef_cores.components
    # post_cores = posterior.components
    # print("start remainder computation")
    # R = [1]
    # bar = Bar("compute remainder", max=len(coef_cores_var))
    # assert len(coef_cores_var) > len(post_cores)
    # for lia in range(len(coef_cores_var)-1, -1, -1):
    #
    #     if lia > len(post_cores)-1:
    #         #                                       # [r_k-1, 0, r_k] x [r_k] = [r_k-1]
    #         R = np.dot(coef_cores_var[lia][:, 0, :], R)
    #     elif lia == len(post_cores) - 1:
    #         mu = min(post_cores[lia].shape[1], coef_cores_var[lia].shape[1])
    #         #                                       # [r_k-1, mu_k, r_k] x [r_k] = [r_k-1, mu_k]
    #         R = np.einsum('ijk, k->ij', coef_cores_var[lia][:, :mu, :], R)
    #         assert post_cores[lia].shape[2] == 1
    #         U = post_cores[lia][:, :mu, 0]          # [r_k-1', mu_k, 1] = [r_k-1, mu_k]
    #         R = np.dot(R, U.T)                      # [r_k-1, mu_k] x [mu_k, r_k-1'] = [r_k-1, r_k-1']
    #     else:
    #         mu = min(post_cores[lia].shape[1], coef_cores_var[lia].shape[1])
    #         R = np.einsum('ijk, kl->ijl', coef_cores_var[lia][:, :mu, :], R)
    #         R = np.einsum('ajl, ijl->ia', post_cores[lia][:, :mu, :], R)
    #     bar.next()
    # bar.finish()

    print("compute projection fem coefficient")
    print(R.shape)
    print(discrete_coef.discrete_coef_cores[0].shape[2])
    R = np.reshape(R, (discrete_coef.discrete_coef_cores[0].shape[2],
                       discrete_coef.discrete_coef_cores[0].shape[2]), order='F')

    alpha_var = discrete_coef.discrete_coef_cores[0][0, :, :].dot(R)

    var_fun_vec = np.zeros(alpha_var.shape[0])
    for lia in range(alpha_var.shape[1]):
        var_fun_vec += alpha_var[:, lia]*discrete_coef.discrete_coef_cores[0][0, :, lia]
    var_fun_vec += -1 * (alpha_mean_post**2)

    _fig = plt.figure()
    cmap = plt.cm.jet
    coordinates = ref_fs.tabulate_dof_coordinates().reshape((-1, ref_fs.mesh().geometry().dim()))

    ax = _fig.add_subplot(111, projection="3d")
    a = ax.plot_trisurf(coordinates[:, 0], coordinates[:, 1], var_fun_vec, cmap=cmap)
    ax.set_title("posterior coefficient variance")
    _fig.colorbar(a)
    _fig.savefig("coef_variance_post" + ".png")
    _fig.clf()
# endregion
