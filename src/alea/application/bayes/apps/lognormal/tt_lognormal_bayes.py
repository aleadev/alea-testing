# region Imports
from __future__ import division, print_function
import numpy as np

from alea.utils.tictoc import TicToc
from alea.application.bayes.alea_util.configuration.asgfem_config import AsgfemConfig
from alea.application.bayes.alea_util.coefficient_field.tensor.tt_lognormal_semidiscrete_field import \
    (read_coef_from_config)
from alea.application.bayes.Bayes_util.ForwardOperator.tt_lognormal_asgfem_operator_data import \
    TTLognormalAsgfemOperatorData
from alea.application.bayes.Bayes_util.ForwardOperator.tt_lognormal_asgfem_operator import (TTLognormalAsgfemOperator,
                                                                                            solution_cache)
from alea.application.egsz.sample_problems2 import SampleProblem
from alea.application.egsz.sample_domains import SampleDomain

from alea.application.tt_asgfem.tensorsolver.tt_lognormal import sample_lognormal_tt

from alea.linalg.tensor.extended_tt import ExtendedTT, BasisType
from alea.linalg.tensor.extended_fem_tt import ExtendedFEMTT

from alea.polyquad.polynomials import StochasticHermitePolynomials

from alea.application.bayes.Bayes_util.lib.interpolation_util import get_hermite_points
from alea.application.bayes.Bayes_util.exponential.embedded_runge_kutta import EmbeddedRungeKutta
from dolfin import (FunctionSpace, Function, refine, interpolate)

import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

from itertools import product
import cmath
import xerus as xe
import tt

import tarfile
from alea.utils.progress.bar import Bar
import cPickle as Pickle
np.random.seed(8190)
# endregion

problem_file = "../../alea_util/configuration/problems/" + "paper_lognormal_P3-sigma2-thy05-thx05.pro"
cache_file = "../../Bayes_util/ForwardOperator/tmp/" + "P3-sigma2.txt"

test_sol = False
test_obs = False
test_diff = False
test_noise = False
test_prod = False
test_pot = True
test_euler = False
test_reconstruction = False

do_reconstruction = True
do_euler = True


# region pre-computations and object creation
# region Read configuration from problem file
with TicToc(sec_key="Configuration", key="read configuration", do_print=False, active=True):
    config = AsgfemConfig(problem_file)
    if config.read_configuration() is False:
        exit()
# endregion

# region read coefficient from configuration
with TicToc(sec_key="Semi-Disc Coef", key="read from configuration", do_print=False, active=True):
    semi_discrete_coef = read_coef_from_config(config)
# endregion

# region try to read solution data from file
try:
    read_solutions = []
    if False:
        from os.path import isfile
        f = open(cache_file, 'rb')
        lines = f.readlines()
        f.close()
        with tarfile.open("P3-sigma2.tar", "w") as tar:
            for line in lines:
                sol = TTLognormalAsgfemOperatorData()
                line = line.strip()
                print("try to read file: {}".format(line))
                if len(line) <= 1:
                    continue
                if sol.load(line) is False:
                    print("can not read file")
                    read_solution = None
                    break
                tar.add(sol.path + "sol.dat")
                tar.add(sol.path + "sol-mesh.xml")
                tar.add(sol.path + "new-sol-mesh.xml")
                print("found solution: ")
                print("     hedgs: {}".format(sol.hdegs))
                print("     ranks: {}".format(sol.ranks))
                print("     m-dofs: {}".format(sol.m_dofs))
                read_solutions.append(sol)
    else:
        from os.path import isfile

        f = open(cache_file, 'rb')
        lines = f.readlines()
        f.close()
        bar = Bar("Load solutions", max=len(lines)-1)
        for line in lines:
            sol = TTLognormalAsgfemOperatorData()
            line = line.strip()
            # print("try to read file: {}".format(line))
            if len(line) <= 1:
                continue
            if sol.load(line) is False:
                print("can not read file")
                read_solution = None
                break
            if False:
                print("found solution: ")
                print("     hedgs: {}".format(sol.hdegs))
                print("     ranks: {}".format(sol.ranks))
                print("     m-dofs: {}".format(sol.m_dofs))
            read_solutions.append(sol)
            bar.next()
        bar.finish()
except Exception as ex:
    print(ex)
    read_solutions = None
    exit()
# endregion

# region create reference fs and forward operator to sample from
with TicToc(sec_key="reference function space", key="create", do_print=False, active=True):

    ref_mesh, boundaries, dim = SampleDomain.setupDomain(config.domain, initial_mesh_N=2)
    #                                               # refine mesh according to input
    ref_mesh = SampleProblem.setupMesh(ref_mesh, num_refine=1)
    #                                               # refinement if you want to have a specific amount of dofs

    while FunctionSpace(ref_mesh, 'CG', config.femdegree).dim() < 10000:
        ref_mesh = refine(ref_mesh)                 # uniformly refine reference mesh
    pde, _, _, _, _, _ = SampleProblem.setupPDE(2, config.domain, 0, boundaries, semi_discrete_coef)
    ref_fs = FunctionSpace(ref_mesh, "CG", 3)
    ref_operator = TTLognormalAsgfemOperator(semi_discrete_coef, ref_fs, semi_discrete_coef.hermite_degree,
                                             config.domain, empty=True)
# endregion
# endregion

# region measurement data
n_measurements = 20
noise = 0.001
with TicToc(sec_key="Create truth", key="true parameter", do_print=False, active=True):
    true_parameters = np.random.randn(config.reference_M)*10
with TicToc(sec_key="Create truth", key="measurement nodes", do_print=False, active=True):
    measurement_nodes = [np.random.rand(2) for lia in range(n_measurements)]
with TicToc(sec_key="Create truth", key="true solution", do_print=False, active=True):
    retval = ref_operator.sample_analytic_solution(true_parameters, pde, cache=None, reference_m=config.reference_M,
                                                   expression_degree=config.reference_expression_degree, ref_fs=ref_fs)
    true_solution, true_coefficient = retval        # sol is of type numpy array, coef is a compiled expression
    true_solution_fun = Function(ref_fs)
    true_solution_fun.vector()[:] = true_solution
    true_coefficient = interpolate(true_coefficient, ref_fs)
with TicToc(sec_key="Create truth", key="measurement values", do_print=False, active=True):
    measurement_values = [true_solution_fun(coord) for coord in measurement_nodes]
# endregion

grid = np.linspace(-100, 100, num=100, endpoint=True)

noise_mat = np.diag([noise for _ in range(len(measurement_nodes))])
noise_mat = np.linalg.inv(noise_mat)

sol_of_interest = [15]

for sol_index in sol_of_interest:
    sol = read_solutions[sol_index]

    curr_fs = FunctionSpace(sol.mesh, 'CG', config.femdegree)
    # region Init Base and weights
    # TODO: automatic by creating the specific lognomral Bayes object
    base = [BasisType.points] + [BasisType.TransformedNormalisedHermite] * (len(sol.solution) - 1)
    xebase = [BasisType.points] + [BasisType.Hermite] * (len(sol.solution) - 1)
    bmax = semi_discrete_coef.affine_field.get_infty_norm(config.reference_M)
    weights = [[1] * sol.solution[0].shape[1]]
    for lia in range(len(sol.solution) - 1):
        weights.append([np.exp(-config.theta * config.rho * bmax[lia])] * 100)
    # endregion

    sol_ten = ExtendedFEMTT(sol.solution, base, curr_fs, weights)

    # region Test Extended FEM TT
    if test_sol:                                    # test solution as Extended TT
        assert len(sol.solution) == sol_ten.dim     # dimensionality is correct
        for lia in range(len(sol.solution)):
            #                                       # the ranks and dimension are correctly read
            assert sol.solution[lia].shape[0] == sol_ten.r[lia]
            assert sol.solution[lia].shape[1] == sol_ten.n[lia]
        #                                           # the last rank is the same
        assert sol.solution[-1].shape[2] == sol_ten.r[-1]
        assert sol.solution[-1].shape[2] == 1       # and the last rank is equal to one

        samples = [np.random.randn(len(sol.solution)-1) for lia in range(config.n_samples)]
        bar = Bar("Test solution sampling", max=len(samples) - 1)
        for sample in samples:
            max_sol = sample_lognormal_tt(sol.solution, sample, bmax, theta=config.theta, rho=config.rho)
            man_sol = sol_ten.sample(sample)
            assert len(max_sol) == len(man_sol)
            assert np.linalg.norm(max_sol - man_sol) / np.linalg.norm(max_sol) <= 1e-10
            bar.next()
        bar.finish()
    # endregion

    #                                               # apply observation
    sol_ten = sol_ten.eval_first_comp(measurement_nodes)

    # region Test application of observation
    if test_obs:                                    # test observation
        max_sol_fun = Function(curr_fs)
        bar = Bar("Test measurement application", max=len(measurement_nodes) - 1)
        for lia in range(len(measurement_nodes)):   # for every measurement node
            #                                       # create samples of the length of the stochastic space
            samples = [np.random.randn(len(sol.solution) - 1) for lib in range(config.n_samples)]
            for sample in samples:
                max_sol = sample_lognormal_tt(sol.solution, sample, bmax, theta=config.theta, rho=config.rho)
                max_sol_fun.vector()[:] = max_sol
                max_obs = max_sol_fun(measurement_nodes[lia])
                man_obs = sol_ten([lia] + list(sample))
                assert np.linalg.norm(max_obs - man_obs) / np.linalg.norm(max_obs) <= 1e-10
            bar.next()
        bar.finish()
    # endregion

    #                                               # apply difference with measurements
    diff_ten = sol_ten * (-1)
    assert isinstance(diff_ten, ExtendedTT)
    diff_ten = diff_ten.add_vec_to_component(measurement_values, 0)

    # region Test difference application
    if test_diff:                                   # test observation
        max_dif_fun = Function(curr_fs)
        bar = Bar("Test difference application", max=len(measurement_nodes) - 1)
        for lia in range(len(measurement_nodes)):   # for every measurement node
            #                                       # create samples of the length of the stochastic space
            samples = [np.random.randn(len(sol.solution) - 1) for lib in range(config.n_samples)]
            for sample in samples:
                max_dif = sample_lognormal_tt(sol.solution, sample, bmax, theta=config.theta, rho=config.rho)
                max_dif_fun.vector()[:] = max_dif
                max_dif = measurement_values[lia] - max_dif_fun(measurement_nodes[lia])
                man_dif = diff_ten([lia] + list(sample))
                if np.abs(max_dif - man_dif) / np.abs(max_dif) > 1e-7:
                    print("node: {} sample={}, max={}, man={}".format(measurement_nodes[lia], sample, max_dif, man_dif))
                    exit()
            bar.next()
        bar.finish()
    # endregion

    rhs_ten = diff_ten.copy()
    lhs_ten = diff_ten.multiply_comp_with_matrix_left(noise_mat, 0)

    # region Test measurement noise application
    if test_noise:                                  # test measurement noise application
        max_noi_fun = Function(curr_fs)
        bar = Bar("Test noise application", max=len(measurement_nodes) - 1)
        for lia in range(len(measurement_nodes)):   # for every measurement node
            #                                       # create samples of the length of the stochastic space
            samples = [np.random.randn(len(sol.solution) - 1) for lib in range(config.n_samples)]
            for sample in samples:
                max_noi = sample_lognormal_tt(sol.solution, sample, bmax, theta=config.theta, rho=config.rho)
                max_noi_fun.vector()[:] = max_noi
                max_noi = noise**(-1) * (measurement_values[lia] - max_noi_fun(measurement_nodes[lia]))
                man_noi = lhs_ten([lia] + list(sample))
                assert np.abs(max_noi - man_noi) / np.abs(max_noi) <= 1e-10
            bar.next()
        bar.finish()
    # endregion
    #                                               # multiply lhs and rhs
    product_ten = lhs_ten.multiply_with_extendedTT(rhs_ten)

    # region Test product application
    if test_prod:                                   # test product application
        max_prod_fun = Function(curr_fs)
        bar = Bar("Test product application", max=len(measurement_nodes) - 1)
        for lia in range(len(measurement_nodes)):   # for every measurement node
            #                                       # create samples of the length of the stochastic space
            samples = [np.random.randn(len(sol.solution) - 1) for lib in range(config.n_samples)]
            for sample in samples:
                max_prod = sample_lognormal_tt(sol.solution, sample, bmax, theta=config.theta, rho=config.rho)
                max_prod_fun.vector()[:] = max_prod
                max_prod = noise ** (-1) * (measurement_values[lia] - max_prod_fun(measurement_nodes[lia]))**2
                man_prod = product_ten([lia] + list(sample))
                # print("{}/{} sample={}: max={}, man={}".format(lia+1, len(measurement_nodes),
                #                                                sample, max_prod, man_prod))
                assert np.abs(max_prod - man_prod) / np.abs(max_prod) <= 1e-7
            bar.next()
        bar.finish()
    # endregion

    #                                               # sum over first component
    potential_ten = product_ten.sum_component(0) * (-0.5)
    # region Test potential result
    if test_pot:                                    # test potential
        if False:
            # region sampling of error
            max_pot_fun = Function(curr_fs)
            #                                       # create samples of the length of the stochastic space
            samples = [np.random.randn(len(sol.solution) - 1) for lib in range(config.n_samples)]

            bar = Bar("Test potential", max=len(samples) - 1)
            for sample in samples:
                max_pot = sample_lognormal_tt(sol.solution, sample, bmax, theta=config.theta, rho=config.rho)
                max_pot_fun.vector()[:] = max_pot
                max_pot_value = 0
                for lia in range(len(measurement_nodes)):
                    max_pot_value += noise ** (-1) * (measurement_values[lia] - max_pot_fun(measurement_nodes[lia]))**2
                max_pot_value *= (-0.5)
                assert isinstance(potential_ten, ExtendedTT)
                man_pot_value = potential_ten(list(sample))
                # print("{}/{} sample={}: max={}, man={}".format(lia+1, len(measurement_nodes),
                #                                                sample, max_pot_value, man_pot_value))
                assert np.abs(max_pot_value - man_pot_value) / np.abs(max_pot_value) <= 1e-7
                bar.next()
            bar.finish()
            # endregion
        # region plot potential

        marginal_pot_values = np.zeros((len(grid), len(grid)))

        def marginal_pot(_x):
            return potential_ten(list(_x))

        bar = Bar("Plot potential values", max=len(grid) ** 2)
        for lia, x in enumerate(grid):
            for lib, y in enumerate(grid):
                marginal_pot_values[lia, lib] = marginal_pot([x, y, true_parameters[2]])
                bar.next()
        bar.finish()
        X, Y = np.meshgrid(grid, grid)
        ax = plt.subplot(1, 3, 1, projection='3d')
        levels = np.arange(-1000, 5, 5)
        cont = ax.plot_surface(X, Y, marginal_pot_values, cmap=cm.coolwarm, linewidth=0, antialiased=False)
        # cont = plt.contour(X, Y, marginal_pot_values, levels=levels)
        plt.colorbar(cont)
        plt.title("Bayesian potential")

        # endregion

        print("true values: {}".format(true_parameters))
        # region compute and plot monomial best approximation
        potential_ten_mon = ExtendedTT(potential_ten.components, [BasisType.points]*potential_ten.dim)
        A, b, c = potential_ten_mon.compute_order_2_best_fit_monomial_hermite()

        def P2(_y):
            _y = np.array(_y)
            return _y.T.dot(A.dot(_y)) + b.T.dot(_y) + c

        marginal_pot2_values = np.zeros((len(grid), len(grid)))
        bar = Bar("Plot potential 2-approx values", max=len(grid) ** 2)
        for lia, x in enumerate(grid):
            for lib, y in enumerate(grid):
                marginal_pot2_values[lia, lib] = P2([x, y, true_parameters[2]])
                bar.next()
        bar.finish()
        ax = plt.subplot(1, 3, 2, projection='3d')
        cont = ax.plot_surface(X, Y, marginal_pot2_values, cmap=cm.coolwarm, linewidth=0, antialiased=False)
        # cont = plt.contour(X, Y, marginal_pot2_values, levels=levels)
        plt.colorbar(cont)
        plt.title("total degree 2 monomial best fit")
        # endregion

        # region compute shift and normalisation
        z_0 = np.linalg.solve(A, b)*0.5
        sigma, v = np.linalg.eig(A)
        print("eigenvalues: {}".format(sigma))
        D = np.ones(len(sigma))
        D_ = np.ones(len(sigma))
        for lia in range(len(sigma_inv)):
            if sigma[lia] < 0:
                D_[lia] = sigma[lia]**-1
            else:
                D[lia] = sigma[lia]**-1
        print("inv sqrt eigenvalues: {}".format(sigma_inv))
        print("product eigenvalues: {}".format(sigma_inv*sigma*sigma_inv))
        z = np.dot(v, np.diag(sigma_inv))

        marginal_shift_values = np.zeros((len(grid), len(grid)))
        bar = Bar("Plot potential 3-normed-shifted values", max=len(grid) ** 2)
        for lia, x in enumerate(grid):
            for lib, y in enumerate(grid):
                y = np.array([x, y, true_parameters[2]])
                y = np.dot(z, y) - z_0
                marginal_shift_values[lia, lib] = P2(y)
                bar.next()
        bar.finish()

        ax = plt.subplot(1, 3, 3, projection='3d')
        cont = ax.plot_surface(X, Y, marginal_shift_values, cmap=cm.coolwarm, linewidth=0, antialiased=False)
        # cont = plt.contour(X, Y, marginal_shift_values, levels=levels)
        plt.colorbar(cont)
        plt.title("shifted and normalized potential")
        plt.show()
        # endregion
        exit()
    # endregion

    poly_dim = potential_ten.n                      # polynomial degree to fit stochastic representation

    # region Tensor reconstruction
    if do_reconstruction:
        x_dim = 1                                   # nodes in physical space is only one, since it is a density
        target_eps = 1e-8                           # desired tolerance on test data set inside ADF
        max_itr = 10000                             # maximal number of iterations in ADF

        # TODO: play with this a bit. It is unclear what you need here. usually much more in exp than before

        basis = xe.PolynomBasis.Hermite             # define flag of used polynomials

        measurements = xe.UQMeasurementSet()        # create Xerus measurement Set
        #                                           # create samples
        n_samples = 1000
        do_again = False
        recon_path = 'recon{}{}{}-1.dat'.format(n_samples, n_measurements, noise)
        try:
            infile = open(recon_path, 'rb')
            exp_ten_recon = Pickle.load(infile)
            infile.close()
        except IOError as ex:
            print("IOERROR can not import from file " + recon_path + " err: " + ex.message)
            do_again = True
        except EOFError as ex:
            print("EOFERROR can not import from file " + recon_path + " err: " + ex.message)
            do_again = True
        if do_again:
            bar = Bar("create reconstruction samples", max=n_samples)
            y_list = []
            for lia in range(n_samples):
                y_list.append(np.random.randn(potential_ten.dim))
                value = xe.Tensor(dim=[x_dim])      # create a one dimensional tensor of size of physical space (=1)
                #                                   # set value in tensor
                value[[0]] = np.exp(potential_ten(list(y_list[-1])))
                #                                   # add parameter and sample result to measurement list
                measurements.add(list(y_list[-1]), value)
                bar.next()
            bar.finish()
            dimension = [x_dim] + poly_dim          # define tensor dimensions

            #                                       # run adf algorithm
            result = xe.uq_ra_adf(measurements, basis, dimension, target_eps, max_itr)
            #                                       # create an extended TT out of the xerus TT
            exp_ten_recon = ExtendedTT.from_xerus_tt(result, xebase)

            # the xerus TT in this case has a one dimensional first component. we need to get rid of this
            components = exp_ten_recon.components
            components[1] = np.einsum('i, ijk->jk', components[0][0, 0, :],
                                      components[1]).reshape((1, components[1].shape[1], components[1].shape[2]),
                                                             order="F")
            #                                       # final result of the reconstruction in unnormalised Hermite poly
            exp_ten_recon = ExtendedTT(components[1:], xebase[1:])
            exp_ten_recon.normalise()               # Since xerus uses unnormalised Hermite poly -> normalise
            outfile = open(recon_path, 'wb')
            Pickle.dump(exp_ten_recon, outfile, Pickle.HIGHEST_PROTOCOL)
            outfile.close()
    # endregion

    # region do euler method
    if do_euler:
        max_rank = 50
        do_again = False
        euler_path = 'euler{}{}{}-1.dat'.format(max_rank, n_measurements, noise)
        try:
            infile = open(euler_path, 'rb')
            exp_ten_euler = Pickle.load(infile)
            infile.close()
        except IOError as ex:
            print("IOERROR can not import from file " + euler_path + " err: " + ex.message)
            do_again = True
        except EOFError as ex:
            print("EOFERROR can not import from file " + euler_path + " err: " + ex.message)
            do_again = True
        if do_again:
            nodes = get_hermite_points(poly_dim)
            potential_ten_eval = potential_ten.evaluate_at_grid(nodes)
            # euler_method = EmbeddedRungeKutta(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
            #                                   euler_repetitions, euler_local_mc_samples)
            euler_method = EmbeddedRungeKutta(100, 1e-8, 50, 1, 1, 20)
            euler_method.method = "forth"
            euler_method.acc = 1e-8
            euler_method.min_stepsize = 1e-19
            euler_method.max_stepsize = 1.0 - 1e-10
            potential_ten_tt = tt.vector.from_list(potential_ten_eval.components)
            exp_ten_tt, _ = euler_method.calculate_exponential_tt(potential_ten_tt, 3)
            exp_ten_tt = tt.vector.round(exp_ten_tt, 1e-14)
            exp_ten_tt = tt.vector.to_list(exp_ten_tt)
            exp_ten_euler = ExtendedTT.from_grid_to_hermite(exp_ten_tt)

            outfile = open(euler_path, 'wb')
            Pickle.dump(exp_ten_euler, outfile, Pickle.HIGHEST_PROTOCOL)
            outfile.close()
    # endregion

    # region Test Euler and reconstruction
    # region create test set for exp results
    if test_reconstruction or test_euler:

        n_samples = 100
        samples = [np.random.randn(len(poly_dim)) for _ in range(n_samples)]
        bar = Bar("sample reference results", max=n_samples)
        max_exp_values = np.zeros(n_samples)
        max_exp_fun = Function(curr_fs)
        for lia, sample in enumerate(samples):
            max_exp = sample_lognormal_tt(sol.solution, sample, bmax, theta=config.theta, rho=config.rho)
            max_exp_fun.vector()[:] = max_exp
            max_exp_value = 0
            for lib in range(len(measurement_nodes)):
                max_exp_value += noise ** (-1) * (measurement_values[lib] - max_exp_fun(measurement_nodes[lib])) ** 2
            max_exp_value *= (-0.5)
            max_exp_values[lia] = np.exp(max_exp_value)
            bar.next()
        bar.finish()
    # endregion

    # region test reconstruction
    if test_reconstruction:
        mc_recon = 0
        bar = Bar("sample reconstruction result", max=n_samples)
        for lia, sample in enumerate(samples):
            man_exp_value = exp_ten_recon(list(sample))
            # print("max: {} = {} :reconstruction".format(max_exp_values[lia], man_exp_value))
            mc_recon += (max_exp_values[lia] - man_exp_value)**2/max_exp_values[lia]
            bar.next()
        bar.finish()
        mc_recon *= n_samples ** (-1)
        print("reconstruction relative sampling error: {}".format(mc_recon))
    # endregion

    # region test euler
    if test_euler:
        mc_euler = 0
        bar = Bar("sample euler result", max=n_samples)
        for lia, sample in enumerate(samples):
            man_exp_value = exp_ten_euler(sample)
            # print("max: {} = {} :euler".format(max_exp_values[lia], man_exp_value))
            mc_euler += (max_exp_values[lia] - man_exp_value)**2 / max_exp_values[lia]
            bar.next()
        bar.finish()
        mc_euler *= n_samples**(-1)
        print("euler relative sampling error: {}".format(mc_euler))
    # endregion
    # endregion

    euler_mean = exp_ten_euler.mean()
    recon_mean = exp_ten_recon.mean()

    if True:
        def prior_den(_x): return np.sqrt(2*np.pi)**(-1)*np.exp(-0.5*_x**2)

        fig = plt.figure()

        for lia in range(exp_ten_euler.dim):
            print("Euler ten: mean= {} \n {}".format(euler_mean, exp_ten_euler))
            print("Recon ten: mean= {} \n {}".format(recon_mean, exp_ten_recon))
            marginal_euler = exp_ten_euler.marginalise_by_dimension(lia)
            marginal_recon = exp_ten_recon.marginalise_by_dimension(lia)
            plt.subplot(1, 3, lia+1)
            plt.plot(grid, [marginal_euler([x])*euler_mean**(-1)*prior_den(x) for x in grid], '-s')
            plt.axvline(x=true_parameters[lia])
            plt.plot(grid, [marginal_recon([x])*recon_mean**(-1)*prior_den(x) for x in grid], '-r')
        plt.show()

        euler_exp_values = np.zeros((len(grid), len(grid)))
        recon_exp_values = np.zeros((len(grid), len(grid)))

        bar = Bar("Plot exp values", max=len(grid) ** 2)
        for lia, x in enumerate(grid):
            for lib, y in enumerate(grid):
                euler_exp_values[lia, lib] = exp_ten_euler([x, y, true_parameters[2]]) * euler_mean
                recon_exp_values[lia, lib] = exp_ten_recon([x, y, true_parameters[2]]) * recon_mean
                bar.next()
        bar.finish()
        X, Y = np.meshgrid(grid, grid)

        levels = np.arange(-0.01, 1000, 0.1)

        fig = plt.figure()
        fig.subplots_adjust(hspace=0.3)

        plt.subplot(2, 1, 1)
        plt.title("Euler")
        cont = plt.contour(X, Y, euler_exp_values)
        plt.colorbar(cont)
        plt.subplot(2, 1, 2)
        plt.title("Reconstruction")
        cont = plt.contour(X, Y, recon_exp_values)
        # plt.colorbar(cont)
        print(true_parameters[:2])
        plt.show()

    # #######################################
    # second solution step
    # #######################################
    curr_hdeg = sol.hdegs
    curr_rank = sol.ranks

    semi_discrete_coef.update_coefficient(curr_hdeg, config.max_rank, add_order=4, new_dimension=2, add_dim=2,
                                          max_hdeg=config.max_hdegs_coef, same_length=(config.theta_y <= 0))

    curr_forward_operator = TTLognormalAsgfemOperator(semi_discrete_coef, curr_fs, curr_hdeg, config.domain)

    cont_coef = ExtendedTT(curr_forward_operator.discrete_coef.discrete_coef_cores[1:],
                           [BasisType.NormalisedHermite] * (len(curr_forward_operator.discrete_coef.discrete_coef_cores[1:])))
    print(cont_coef)
    # TODO: base change of exp_ten_euler to transformed hermite polynomials with weight of solution
    cont_coef = cont_coef.multiply_with_extendedTT(exp_ten_euler * euler_mean)
    curr_forward_operator.discrete_coef.discrete_coef_cores[1:] = cont_coef.components
    curr_forward_operator.discrete_coef.hermite_degree[1:] = cont_coef.n
    curr_forward_operator.discrete_coef.ranks[1:] = cont_coef.r

    curr_forward_operator.solve(curr_rank, config.preconditioner, config.iterations, config.als_tol, sol.solution)

    sol_ten = ExtendedFEMTT(curr_forward_operator.solution, base, curr_fs, weights)

    #                                               # apply observation
    sol_ten = sol_ten.eval_first_comp(measurement_nodes)

    #                                               # apply difference with measurements
    diff_ten = sol_ten * (-1)
    diff_ten = diff_ten.add_vec_to_component(measurement_values, 0)

    rhs_ten = diff_ten.copy()
    lhs_ten = diff_ten.multiply_comp_with_matrix_left(noise_mat, 0)

    #                                               # multiply lhs and rhs
    product_ten = lhs_ten.multiply_with_extendedTT(rhs_ten)

    #                                               # sum over first component
    potential_ten = product_ten.sum_component(0) * (-0.5)
    # region Test potential result
    if test_pot:  # test potential
        marginal_pot_values = np.zeros((len(grid), len(grid)))

        def marginal_pot(_x):
            return potential_ten(list(_x))

        bar = Bar("Plot potential values", max=len(grid) ** 2)
        for lia, x in enumerate(grid):
            for lib, y in enumerate(grid):
                marginal_pot_values[lia, lib] = marginal_pot([x, y, true_parameters[2]])
                bar.next()
        bar.finish()
        X, Y = np.meshgrid(grid, grid)

        fig = plt.figure()
        levels = np.arange(-10, 0.01, 0.1)
        cont = plt.contour(X, Y, marginal_pot_values, levels=levels)
        plt.colorbar(cont)
        print(true_parameters[:2])
        plt.show()
        exit()
        print("potential tensor: {}".format(potential_ten))
    # endregion

    poly_dim = potential_ten.n  # polynomial degree to fit stochastic representation

    # region Tensor reconstruction
    if do_reconstruction:
        x_dim = 1  # nodes in physical space is only one, since it is a density
        target_eps = 1e-8  # desired tolerance on test data set inside ADF
        max_itr = 10000  # maximal number of iterations in ADF

        # TODO: play with this a bit. It is unclear what you need here. usually much more in exp than before

        basis = xe.PolynomBasis.Hermite  # define flag of used polynomials

        measurements = xe.UQMeasurementSet()  # create Xerus measurement Set
        #                                           # create samples
        n_samples = 1000
        do_again = False
        recon_path = 'recon{}{}{}-2.dat'.format(n_samples, n_measurements, noise)
        try:
            infile = open(recon_path, 'rb')
            exp_ten_recon = Pickle.load(infile)
            infile.close()
        except IOError as ex:
            print("IOERROR can not import from file " + recon_path + " err: " + ex.message)
            do_again = True
        except EOFError as ex:
            print("EOFERROR can not import from file " + recon_path + " err: " + ex.message)
            do_again = True
        if do_again:
            bar = Bar("create reconstruction samples", max=n_samples)
            y_list = []
            for lia in range(n_samples):
                y_list.append(np.random.randn(potential_ten.dim))
                value = xe.Tensor(dim=[x_dim])  # create a one dimensional tensor of size of physical space (=1)
                #                                   # set value in tensor
                value[[0]] = np.exp(potential_ten(list(y_list[-1])))
                #                                   # add parameter and sample result to measurement list
                measurements.add(list(y_list[-1]), value)
                bar.next()
            bar.finish()
            dimension = [x_dim] + poly_dim  # define tensor dimensions

            #                                       # run adf algorithm
            result = xe.uq_ra_adf(measurements, basis, dimension, target_eps, max_itr)
            #                                       # create an extended TT out of the xerus TT
            exp_ten_recon = ExtendedTT.from_xerus_tt(result, xebase)

            # the xerus TT in this case has a one dimensional first component. we need to get rid of this
            components = exp_ten_recon.components
            components[1] = np.einsum('i, ijk->jk', components[0][0, 0, :],
                                      components[1]).reshape((1, components[1].shape[1], components[1].shape[2]),
                                                             order="F")
            #                                       # final result of the reconstruction in unnormalised Hermite poly
            exp_ten_recon = ExtendedTT(components[1:], xebase[1:])
            exp_ten_recon.normalise()  # Since xerus uses unnormalised Hermite poly -> normalise
            outfile = open(recon_path, 'wb')
            Pickle.dump(exp_ten_recon, outfile, Pickle.HIGHEST_PROTOCOL)
            outfile.close()
    # endregion

    # region do euler method
    if do_euler:
        max_rank = 50
        do_again = False
        euler_path = 'euler{}{}{}-2.dat'.format(max_rank, n_measurements, noise)
        try:
            infile = open(euler_path, 'rb')
            exp_ten_euler = Pickle.load(infile)
            infile.close()
        except IOError as ex:
            print("IOERROR can not import from file " + euler_path + " err: " + ex.message)
            do_again = True
        except EOFError as ex:
            print("EOFERROR can not import from file " + euler_path + " err: " + ex.message)
            do_again = True
        if do_again:
            nodes = get_hermite_points(poly_dim)
            potential_ten_eval = potential_ten.evaluate_at_grid(nodes)
            # euler_method = EmbeddedRungeKutta(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
            #                                   euler_repetitions, euler_local_mc_samples)
            euler_method = EmbeddedRungeKutta(100, 1e-8, 50, 1, 1, 20)
            euler_method.method = "forth"
            euler_method.acc = 1e-8
            euler_method.min_stepsize = 1e-19
            euler_method.max_stepsize = 1.0 - 1e-10
            potential_ten_tt = tt.vector.from_list(potential_ten_eval.components)
            exp_ten_tt, _ = euler_method.calculate_exponential_tt(potential_ten_tt, 3)
            exp_ten_tt = tt.vector.round(exp_ten_tt, 1e-14)
            exp_ten_tt = tt.vector.to_list(exp_ten_tt)
            exp_ten_euler = ExtendedTT.from_grid_to_hermite(exp_ten_tt)

            outfile = open(euler_path, 'wb')
            Pickle.dump(exp_ten_euler, outfile, Pickle.HIGHEST_PROTOCOL)
            outfile.close()
    # endregion

    euler_mean = exp_ten_euler.mean()
    recon_mean = exp_ten_recon.mean()

    if True:
        def prior_den(_x):
            return np.sqrt(2 * np.pi) ** (-1) * np.exp(-0.5 * _x ** 2)


        fig = plt.figure()

        for lia in range(exp_ten_euler.dim):
            print("Euler ten: mean= {} \n {}".format(euler_mean, exp_ten_euler))
            print("Recon ten: mean= {} \n {}".format(recon_mean, exp_ten_recon))
            marginal_euler = exp_ten_euler.marginalise_by_dimension(lia)
            marginal_recon = exp_ten_recon.marginalise_by_dimension(lia)
            plt.subplot(1, 3, lia + 1)
            plt.plot(grid, [marginal_euler([x]) * euler_mean ** (-1) * prior_den(x) for x in grid], '-s')
            plt.axvline(x=true_parameters[lia])
            plt.plot(grid, [marginal_recon([x]) * recon_mean ** (-1) * prior_den(x) for x in grid], '-r')
        plt.show()

        euler_exp_values = np.zeros((len(grid), len(grid)))
        recon_exp_values = np.zeros((len(grid), len(grid)))

        bar = Bar("Plot exp values", max=len(grid) ** 2)
        for lia, x in enumerate(grid):
            for lib, y in enumerate(grid):
                euler_exp_values[lia, lib] = exp_ten_euler([x, y, true_parameters[2]]) * euler_mean
                recon_exp_values[lia, lib] = exp_ten_recon([x, y, true_parameters[2]]) * recon_mean
                bar.next()
        bar.finish()
        X, Y = np.meshgrid(grid, grid)

        levels = np.arange(-0.01, 10, 0.1)

        fig = plt.figure()
        fig.subplots_adjust(hspace=0.3)

        plt.subplot(2, 1, 1)
        plt.title("Euler")
        cont = plt.contour(X, Y, euler_exp_values)
        plt.colorbar(cont)
        plt.subplot(2, 1, 2)
        plt.title("Reconstruction")
        cont = plt.contour(X, Y, recon_exp_values)
        # plt.colorbar(cont)
        print(true_parameters[:2])
        plt.show()
