"""
Simple test script to validate the numerical example from H.Matthies https://arxiv.org/pdf/1806.03234.pdf
which states, that the conditioned mean of the potential, ist the value of choice for Bayesian inversion.
Better than current Polynomial Filter methods.
"""
# region Imports
from __future__ import division, print_function
import numpy as np
import xerus as xe
import TensorToolbox as DT
from alea.linalg.tensor.extended_tt import ExtendedTT, BasisType
from alea.application.bayes.Bayes_util.exponential.rank_adaptive_embedded_runge_kutta import RankAdaptiveEmbeddedRungeKutta
from alea.application.bayes.Bayes_util.exponential.runge_kutta import RungeKutta
from alea.application.bayes.Bayes_util.exponential.embedded_runge_kutta import EmbeddedRungeKutta
from alea.application.bayes.Bayes_util.exponential.extendedtt_embedded_rk import ExtendedTTEmbeddedRungeKutta
from alea.application.bayes.Bayes_util.exponential.implicit_euler import ImplicitEuler
from alea.utils.progress.bar import Bar
import matplotlib.pyplot as plt
from matplotlib import cm
from numpy.polynomial.hermite_e import hermegauss
from numpy.polynomial.legendre import leggauss
from scipy.optimize import minimize
import tt
from mpl_toolkits.mplot3d import Axes3D
from itertools import product

np.random.seed(8011990)
# endregion

# region Function to represent observation
def measurement_operator(
                         _q,                        # type: np.array
                         params=None
                        ):
    """
    given measurement op from example in Figure 4 of paper
    :param _q: input - tuple (q_1, q_2)
    :return: float
    """
    q1, q2 = _q[0], _q[1]
    q1 *= 2
    q2 *= 2
    return q1*(q1 + 3/2)*(q1 - 3/2) + q2*(q2 + 3/2)*(q2 - 3/2) - q1*q2


def noise():
    """
    define the level of noise
    :return: float
    """
    return np.sqrt(0.4)

# endregion

use_legendre = False
use_trafo = False
use_trafo2 = False
if use_trafo2:
    assert use_trafo

if use_legendre:
    gauss = leggauss
else:
    gauss = hermegauss

n_nodes = 20

noise_value = noise()*np.random.randn(1)[0]
n_samples = 10000                                   # number of samples used for reconstruction
x_dim = 1                                           # nodes in physical space
target_eps = 1e-14                                  # desired tolerance on test data set inside ADF
max_itr = 10000                                     # maximal number of iterations in ADF

poly_dim = [15, 15]                                 # polynomial degree to fit one dimensional stochastic representation
if use_legendre:
    basis = xe.PolynomBasis.Legendre                # define flag of used polynomials in xerus
    ex_basis = [BasisType.points] + 2 * [BasisType.Legendre]
else:
    basis = xe.PolynomBasis.Hermite                 # define flag of used polynomials in xerus
    ex_basis = [BasisType.points] + 2*[BasisType.Hermite]

#                                                   # create samples according to prior measure
if use_legendre:
    y_list = [np.random.rand(len(poly_dim))*2 -1 for _ in range(n_samples)]
else:
    y_list = [np.random.randn(len(poly_dim)) for _ in range(n_samples)]

dimension = [x_dim] + poly_dim                      # define tensor dimensions


if True:                                            # potential cross interpolation
    nodes, weights = gauss(6)                      # polynomial of degree 5 are fine
    domain = [nodes] + [nodes]

    def involved_measurement_operator(node, params=None):
        return noise_value - measurement_operator(node)

    TW = DT.TensorWrapper(involved_measurement_operator, domain)
    approx = DT.TTvec(TW)
    print("build potential using cross interpolation")
    approx.build(eps=1e-10, method='ttdmrgcross', rs=None, fix_rank=False, Jinit=None, delta=1e-4, maxit=100, mv_eps=1e-6,
                 mv_maxit=100, max_ranks=None, kickrank=None)
    core_list = approx.TT
    if use_legendre:
        result = ExtendedTT.from_grid_to_legendre(core_list)
    else:
        result = ExtendedTT.from_grid_to_hermite(core_list)

if False:                                           # potential reconstruction
    # region create measurement set
    measurements = xe.UQMeasurementSet()                # create Xerus measurement Set


    for node1, node2 in product(nodes, nodes):
        sol = xe.Tensor(dim=[x_dim])  # create a one dimensional tensor of size of physical space
        sol[[0]] = measurement_operator([node1, node2]) + noise_value
        measurements.add([node1, node2], sol)  # add parameter and sample result to measurement list
    # for y in y_list:                                    # for every sample
    #     sol = xe.Tensor(dim=[x_dim])                    # create a one dimensional tensor of size of physical space
    #     sol[[0]] = measurement_operator(y) + noise_value
    #     measurements.add(y, sol)                        # add parameter and sample result to measurement list
    # endregion

    #                                                   # run adf algorithm
    res = xe.uq_ra_adf(measurements, basis, dimension, target_eps, max_itr)
    result = ExtendedTT.from_xerus_tt(res, ex_basis)    # Store as extended TT
    components = result.components                      # copy components
    components[1] = np.einsum('i, ijk->jk', components[0][0, 0, :],
                              components[1]).reshape((1, components[1].shape[1], components[1].shape[2]), order="F")
    #                                                   # final result of the reconstruction in unnormalised Hermite poly
    result = ExtendedTT(components[1:], ex_basis[1:])

result = result * (-1)
result.normalise()                                  # normalise to obtain better numerical behaviour
result.round(1e-16)
result = result.multiply_with_extendedTT(result)

potential = result * noise()**(-1)*(-0.5)


def marginal_pot(_x):
    return potential(list(_x))

# region Nodes reweighting, minima 1
nodes, weights = gauss(n_nodes)
if use_trafo:

    x0 = np.array([1, 1])

    res = minimize(marginal_pot, x0, method="BFGS") #, tol=1e-8, options={'gtol': 1e-8, 'maxiter': None})
    z_0 = res.x
    # hes_inv = np.array(res.hess_inv.todense())
    hes_inv = res.hess_inv
    sigma, v = np.linalg.eig(np.linalg.inv(hes_inv))
    D_inv = np.sqrt(sigma) ** -1
else:
    z_0 = np.zeros(potential.dim)
    D_inv = np.eye(potential.dim, potential.dim)
    sigma = np.eye(potential.dim, potential.dim)
    v = np.eye(potential.dim, potential.dim)

def trafo(node):
    return np.dot(v.dot(np.diag(D_inv)), node) + z_0

orig_tensor_nodes = np.zeros((n_nodes, len(sigma)))
orig_tensor_weights = np.zeros(n_nodes)
tensor_nodes = np.zeros((n_nodes, len(sigma)))
tensor_weights = np.zeros(n_nodes)
for lia in range(n_nodes):
    node = np.array([nodes[lia] for lib in range(len(sigma))])
    tensor_nodes[lia, :] = np.dot(np.diag(D_inv), node) + z_0
    tensor_weights[lia] = weights[lia]
    orig_tensor_nodes[lia, :] = node
    orig_tensor_weights[lia] = weights[lia]
tensor_nodes = tensor_nodes.T
# endregion

# region Nodes reweighting, minima 2
if use_trafo2:
    x0 = np.array([-1, -1])
    res = minimize(marginal_pot, x0, method="BFGS", bounds=[(0.5, 1.5), (0.5, 1.5)]) #, tol=1e-8, options={'gtol': 1e-8, 'maxiter': None})
    z_1 = res.x
    # hes_inv = np.array(res.hess_inv.todense())
    hes_inv = res.hess_inv
    sigma_2, v_2 = np.linalg.eig(np.linalg.inv(hes_inv))
    D_inv_2 = np.sqrt(sigma_2) ** -1
else:
    z_1 = np.zeros(potential.dim)
    D_inv_2 = np.eye(potential.dim, potential.dim)
    sigma_2 = np.eye(potential.dim, potential.dim)
    v_2 = np.eye(potential.dim, potential.dim)


def trafo_2(node):
    return np.dot(v_2.dot(np.diag(D_inv_2)), node) + z_1


tensor_nodes_2 = np.zeros((n_nodes, len(sigma_2)))
tensor_weights_2 = np.zeros(n_nodes)
for lia in range(n_nodes):
    node = np.array([nodes[lia] for lib in range(len(sigma_2))])
    tensor_nodes_2[lia, :] = np.dot(np.diag(D_inv_2), node) + z_1
    tensor_weights_2[lia] = weights[lia]
    # tensor_nodes[lia, :] = node
tensor_nodes_2 = tensor_nodes_2.T

tensor_nodes_join = np.zeros((2, 2*n_nodes))
tensor_nodes_join[:, :n_nodes] = tensor_nodes
tensor_nodes_join[:, n_nodes:] = tensor_nodes_2
tensor_weights_join = np.zeros(2*n_nodes)
tensor_weights_join[:n_nodes] = tensor_weights
tensor_weights_join[n_nodes:] = tensor_weights_2
# endregion

if False:                                            # reconstruction
    measurements = xe.UQMeasurementSet()  # create Xerus measurement Set
    nodes, weights = hermegauss(20)
    if False:
        for node1, node2 in product(nodes, nodes):
            sol = xe.Tensor(dim=[x_dim])  # create a one dimensional tensor of size of physical space
            trafo_node = trafo([node1, node2])
            sol[[0]] = marginal_pot(trafo_node)
            measurements.add(trafo_node, sol)  # add parameter and sample result to measurement list

        for node1, node2 in product(nodes, nodes):
            sol = xe.Tensor(dim=[x_dim])  # create a one dimensional tensor of size of physical space
            trafo_node = trafo_2([node1, node2])
            sol[[0]] = marginal_pot(trafo_node)
            measurements.add(trafo_node, sol)  # add parameter and sample result to measurement list
    else:
        for lic in range(1000):
            sol = xe.Tensor(dim=[x_dim])  # create a one dimensional tensor of size of physical space
            trafo_node = np.random.randn(2)*4
            sol[[0]] = marginal_pot(trafo_node)
            measurements.add(trafo_node, sol)  # add parameter and sample result to measurement list
    res = xe.uq_ra_adf(measurements, basis, dimension, target_eps, max_itr)
    exp_tt = ExtendedTT.from_xerus_tt(res, ex_basis)    # Store as extended TT
    components = exp_tt.components                      # copy components
    components[1] = np.einsum('i, ijk->jk', components[0][0, 0, :],
                              components[1]).reshape((1, components[1].shape[1], components[1].shape[2]), order="F")
    #                                                   # final result of the reconstruction in unnormalised Hermite poly
    exp_ten_euler = ExtendedTT(components[1:], ex_basis[1:])
    exp_ten_euler.normalise()
    exp_ten_euler = exp_ten_euler.multiply_with_extendedTT(exp_ten_euler)
    euler_mean = exp_ten_euler.mean()
    exp_ten_euler = exp_ten_euler * euler_mean ** (-1)

if True:                                           # Euler
    potential_ten_eval = potential.evaluate_at_grid(orig_tensor_nodes.T)
    # euler_method = EmbeddedRungeKutta(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
    #                                   euler_repetitions, euler_local_mc_samples)
    # potential_ten = ExtendedTT(potential_ten.components, basis=[BasisType.NormalisedHermite]*potential_ten.dim)
    # euler_method = EmbeddedRungeKutta(1000000, 1e-6, 50, 1, 1, 20)
    # euler_method = RankAdaptiveEmbeddedRungeKutta(1000000, 1e-6, 50, 1, 1, 20)
    # euler_method = RungeKutta(1000, 1e-8, 100, 1, 1, 20)
    euler_method = ImplicitEuler(100, 1e-8, 200, 10, 1, 20)
    # euler_method = ExtendedTTEmbeddedRungeKutta(10000, 1e-8, 20, 1, 1, 20)
    euler_method.method = "forth"
    # euler_method.method = "3rk"
    euler_method.acc = 1e-5
    euler_method.min_stepsize = 1e-19
    euler_method.max_stepsize = 0.888
    potential_ten_tt = tt.vector.from_list(potential_ten_eval.components)
    # potential.round(tol=1e-14)
    print(potential)
    exp_ten_euler, _ = euler_method.calculate_exponential_tt(potential_ten_tt)
    if use_legendre:
        exp_ten_euler = ExtendedTT.from_grid_to_legendre(tt.vector.to_list(exp_ten_euler))
    else:
        exp_ten_euler = ExtendedTT.from_grid_to_hermite(tt.vector.to_list(exp_ten_euler))
    print(exp_ten_euler)
    exp_ten_euler.round(1e-16)
    print(exp_ten_euler)
    exp_ten_euler = exp_ten_euler.multiply_with_extendedTT(exp_ten_euler)
    euler_mean = exp_ten_euler.mean()
    print("euler_mean = {}".format(euler_mean))
    exp_ten_euler = exp_ten_euler * euler_mean ** (-1)

if False:                                           # interpolation using 2 means

    def euler_call(x, params=None):
        _retval = np.exp(marginal_pot(x)*(1))
        # print("x= {} -> {}".format(x, _retval))
        return _retval


    # nodes, weights = hermegauss(5)
    # domain = [nodes] + [nodes]
    domain = tensor_nodes
    domain_weight = tensor_weights

    TW = DT.TensorWrapper(euler_call, domain)
    approx = DT.TTvec(TW)
    print("build exponential using cross-interpolation")
    approx.build(eps=1e-10, method='ttdmrgcross', rs=None, fix_rank=False, Jinit=None, delta=1e-4, maxit=1000,
                 mv_eps=1e-6, mv_maxit=1000, max_ranks=None, kickrank=None)
    core_list = approx.TT
    exp_ten_euler1 = ExtendedTT.from_grid_to_hermite(core_list, _nodes=domain, _weights=domain_weight)
    # exp_ten_euler1.round(1e-16)
    exp_ten_euler1.normalise()
    exp_ten_euler1 = exp_ten_euler1.multiply_with_extendedTT(exp_ten_euler1)

    euler_mean1 = exp_ten_euler1.mean()
    exp_ten_euler1 = exp_ten_euler1 * euler_mean1**(-1)

    domain = tensor_nodes_2
    domain_weight = tensor_weights_2

    TW = DT.TensorWrapper(euler_call, domain)
    approx = DT.TTvec(TW)
    print("build exponential using cross-interpolation")
    approx.build(eps=1e-10, method='ttdmrgcross', rs=None, fix_rank=False, Jinit=None, delta=1e-4, maxit=1000,
                 mv_eps=1e-6, mv_maxit=1000, max_ranks=None, kickrank=None)
    core_list = approx.TT
    exp_ten_euler2 = ExtendedTT.from_grid_to_hermite(core_list, _nodes=domain, _weights=domain_weight)
    exp_ten_euler2.round(1e-16)
    exp_ten_euler2.normalise()
    exp_ten_euler2 = exp_ten_euler2.multiply_with_extendedTT(exp_ten_euler2)

    euler_mean2 = exp_ten_euler2.mean()
    exp_ten_euler2 = exp_ten_euler2 * euler_mean2**(-1)

    exp_ten_euler = exp_ten_euler1 + exp_ten_euler2

    # exp_ten_euler.cut(15)
    euler_mean = exp_ten_euler.mean()
    exp_ten_euler = exp_ten_euler * euler_mean**(-1)
    print("exponential tensor: {}".format(exp_ten_euler))

if False:                                            # interpolation only standard nodes
    def euler_call(x, params=None):
        return np.exp((noise_value - measurement_operator(list(x)))**2 * noise()**(-1)*(-0.5))


    orig_tensor_nodes = np.zeros((n_nodes, len(sigma)))
    orig_tensor_weights = np.zeros(n_nodes)
    tensor_nodes = np.zeros((n_nodes, len(sigma)))
    tensor_weights = np.zeros(n_nodes)

    for lia in range(n_nodes):
        node = np.array([nodes[lia] for lib in range(len(sigma))])
        tensor_nodes[lia, :] = np.dot(np.diag(D_inv), node) + z_0
        tensor_weights[lia] = weights[lia]
        orig_tensor_nodes[lia, :] = node
        orig_tensor_weights[lia] = weights[lia]

    domain = tensor_nodes.T
    domain_weight = tensor_weights.T

    TW = DT.TensorWrapper(euler_call, domain)
    approx = DT.TTvec(TW)
    print("build exponential using cross-interpolation")
    approx.build(eps=1e-10, method='ttdmrgcross', rs=None, fix_rank=False, Jinit=None, delta=1e-4, maxit=1000,
                 mv_eps=1e-6, mv_maxit=1000, max_ranks=None, kickrank=None)
    core_list = approx.TT
    exp_ten_euler = ExtendedTT.from_grid_to_hermite(core_list, _nodes=domain, _weights=domain_weight)
    exp_ten_euler.round(1e-16)
    exp_ten_euler = exp_ten_euler.multiply_with_extendedTT(exp_ten_euler)
    exp_ten_euler = exp_ten_euler * (exp_ten_euler.mean())**(-1)

exp_ten_euler.round(1e-14)
print(exp_ten_euler)
exit()
# region Plot result
grid = np.linspace(-1, 1, num=30, endpoint=True)
pot_values_tt = np.zeros((len(grid), len(grid)))
pot_values = np.zeros((len(grid), len(grid)))
exp_values_tt = np.zeros((len(grid), len(grid)))
exp_values = np.zeros((len(grid), len(grid)))

if use_legendre:
    def prior_den(_x): return 0.5
else:
    def prior_den(_x): return np.sqrt(2*np.pi)**(-1)*np.exp(-0.5*_x**2)


def posterior(node):
    return np.exp((-1)*(noise_value-measurement_operator(node))**2 * noise()**(-1))


bar = Bar("Plot potential values", max=len(grid) ** 2)
for lia, x in enumerate(grid):
    for lib, y in enumerate(grid):
        node = [x, y]
        pot_values_tt[lib, lia] = potential(node) * (-1)
        pot_values[lib, lia] = (noise_value - measurement_operator(node))**2 * noise()**(-1)*(0.5)
        exp_values_tt[lib, lia] = exp_ten_euler(node) * prior_den(x) * prior_den(y)
        exp_values[lib, lia] = posterior(node) * prior_den(x) * prior_den(y)
        bar.next()
bar.finish()


if use_legendre:
    mc_mean = np.sum([posterior(np.random.rand(2)*2-1) for lib in range(10000)]) / 10000
else:
    mc_mean = np.sum([posterior(np.random.randn(2)) for lib in range(10000)]) / 10000
print("mc mean={}".format(mc_mean))
print("euler mean={}".format(euler_mean))
X, Y = np.meshgrid(grid, grid)
levels = np.arange(-10, 10, 0.1)
# levels = [-7, -5, -3, -1, 0, 1, 3, 5, 7, 10]
# levels = [0.0, 0.03, 0.1, 0.2, 0.3, 0.4, 0.5, 0.55]
ax = plt.subplot(2, 2, 1, projection="3d")
cont = ax.plot_surface(X, Y, pot_values, cmap=cm.coolwarm, linewidth=0, antialiased=False)
ax.clabel(cont, inline=1, fontsize=5)
plt.colorbar(cont)
plt.title("True potential Surf")

ax = plt.subplot(2, 2, 2)
cont = plt.contour(X, Y, pot_values, levels=levels)
plt.plot(z_0[0], z_0[1], 'xr', label="min 1")
plt.plot(z_1[0], z_1[1], 'xr', label="min 2")
ax.clabel(cont, inline=1, fontsize=5)
plt.colorbar(cont)
plt.title("True potential contour")

ax = plt.subplot(2, 2, 3, projection="3d")
cont = ax.plot_surface(X, Y, pot_values_tt, cmap=cm.coolwarm, linewidth=0, antialiased=False)
plt.colorbar(cont)

plt.title("TT potential Surf")

ax = plt.subplot(2, 2, 4)
cont = plt.contour(X, Y, pot_values_tt, levels=levels)
plt.plot(z_0[0], z_0[1], 'xr', label="min 1")
plt.plot(z_1[0], z_1[1], 'xr', label="min 2")
plt.colorbar(cont)

plt.title("TT potential contour")
plt.tight_layout()

plt.figure()
ax = plt.subplot(2, 2, 1, projection="3d")
# cont = plt.contour(X, Y, marginal_pot_values_trafo)
plt.title("True posterior surf")

levels = [0.005, 0.02, 0.05, 0.08]
# plt.plot(z_0[0], z_0[1], 'xr', label="min 1")
# plt.plot(z_1[0], z_1[1], 'xr', label="min 2")
# cont = plt.contour(X, Y, exp_values * mc_mean**(-1), levels=levels)
cont = ax.plot_surface(X, Y, exp_values*mc_mean**(-1), cmap=cm.coolwarm, linewidth=0, antialiased=False)
ax.clabel(cont, inline=1, fontsize=5)
# plt.colorbar(cont)

ax = plt.subplot(2, 2, 2)
plt.title("True posterior contour")

levels = [0.005, 0.02, 0.05, 0.08]
plt.plot(z_0[0], z_0[1], 'xr', label="min 1")
plt.plot(z_1[0], z_1[1], 'xr', label="min 2")
cont = plt.contour(X, Y, exp_values * mc_mean**(-1), levels=levels)
ax.clabel(cont, inline=1, fontsize=5)
# plt.colorbar(cont)

ax = plt.subplot(2, 2, 3, projection="3d")
cont = ax.plot_surface(X, Y, exp_values_tt, cmap=cm.coolwarm, linewidth=0, antialiased=False, vmax=1)
plt.colorbar(cont)
ax.clabel(cont, inline=1, fontsize=10)
plt.title("TT posterior surf")

# levels = np.arange(0, 10, 0.1)
ax = plt.subplot(2, 2, 4)
cont = plt.contour(X, Y, exp_values_tt)
plt.plot(z_0[0], z_0[1], 'xr', label="min 1")
plt.plot(z_1[0], z_1[1], 'xr', label="min 2")
# plt.colorbar(cont)
ax.clabel(cont, inline=1, fontsize=10)
plt.title("TT posterior contour")
plt.tight_layout()

cond_expectation = exp_ten_euler.components
cond_expectation = ExtendedTT(cond_expectation, basis=[BasisType.points]*2)
print(exp_ten_euler)
print(cond_expectation([1, 0]))
print(cond_expectation([0, 1]))
print("mean: {}".format(cond_expectation([0, 0])))

plt.figure()
ax = plt.subplot(2, 2, 1)
levels = np.arange(0, 20, 0.01)
plt.plot(z_0[0], z_0[1], 'or', label="min 1")
plt.plot(z_1[0], z_1[1], 'or', label="min 2")
cont = plt.contour(X, Y, exp_values*mc_mean**(-1), levels=levels)
for lia in range(tensor_nodes.shape[1]):
    for lib in range(tensor_nodes.shape[1]):
        plt.plot(tensor_nodes[0, lia], tensor_nodes[1, lib], "ob")
        plt.plot(tensor_nodes_2[0, lia], tensor_nodes_2[1, lib], "xr")
# plt.colorbar(cont)

ax = plt.subplot(2, 2, 2, projection="3d")
cont = ax.plot_surface(X, Y, exp_values*mc_mean**(-1), cmap=cm.coolwarm, linewidth=0, antialiased=False)
plt.colorbar(cont)
# nodes, weights = hermegauss(n_nodes)
# for node in nodes:
#     for node2 in nodes:
#         trafo_node = trafo([node, node2])
#         plt.plot(trafo_node[0], trafo_node[1], "ob")

# marginal_pot_values_trafo = np.zeros((len(grid), len(grid)))
# bar = Bar("Plot transformed potential values", max=len(grid) ** 2)
# for lia, x in enumerate(grid):
#     for lib, y in enumerate(grid):
#         node = np.array([x, y])
#         trafo_node = trafo(node)
#         marginal_pot_values_trafo[lib, lia] = marginal_pot(list(trafo_node))
#         bar.next()
# bar.finish()


plt.show()

# endregion
