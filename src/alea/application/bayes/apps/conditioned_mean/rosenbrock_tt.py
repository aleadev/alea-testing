# -*- coding: utf-8 -*-
from __future__ import division, print_function
import numpy as np
import TensorToolbox as DT
from alea.linalg.tensor.extended_tt import ExtendedTT, BasisType
from alea.application.bayes.Bayes_util.exponential.implicit_euler import ImplicitEuler
from alea.application.bayes.Bayes_util.exponential.embedded_runge_kutta import EmbeddedRungeKutta
from numpy.polynomial.hermite_e import hermegauss
from numpy.polynomial.legendre import leggauss
import tt
from alea.utils.progress.bar import Bar
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
np.random.seed(8011990)
if False:
    X = Y = np.linspace(-1, 1, 1000)
    X, Y = np.meshgrid(X, Y)
    plt.subplot(2, 2, 1)
    plt.contourf(X, Y, np.load('leg_moon.npz')['orig'])
    plt.colorbar()
    plt.title("Orig")

    plt.subplot(2, 2, 2)
    plt.contourf(X, Y, np.load('leg_moon.npz')['appr'])
    plt.colorbar()
    plt.title("appr")

    plt.subplot(2, 2, 3)
    plt.contourf(X, Y, np.load('leg_moon.npz')['reco'])
    plt.colorbar()
    plt.title("reco")
    plt.tight_layout()
    # plt.show()

import xerus as xe
from_array = xe.Tensor.from_ndarray


def measurement_operator(x, y):
    # parameters
    a = 0.5
    b = 100
    # normalization constants for fixed parameters
    d = 5.5/2
    e = 6-d
    # rescaled variables
    xt = (2*x+1)
    yt = (e*y+d)
    r = (a-xt)**2 + b*(yt-xt**2)**2
    # r is the Rosenbrock function
    return r*0.01


def noise():
    """
    define the level of noise
    :return: float
    """
    return np.sqrt(0.4)


use_legendre = True
n_nodes = 80

if use_legendre:
    gauss = leggauss
else:
    gauss = hermegauss


nodes, weights = gauss(6)                           # polynomial of degree 5 are fine
domain = [nodes] + [nodes]
noise_value = noise()*np.random.randn(1)[0]


def involved_measurement_operator(node, params=None):
    return noise_value - measurement_operator(node[0], node[1])
    # return (-1)*measurement_operator(node[0], node[1])


TW = DT.TensorWrapper(involved_measurement_operator, domain)
approx = DT.TTvec(TW)
print("build potential using cross interpolation")
approx.build(eps=1e-10, method='ttdmrgcross', rs=None, fix_rank=False, Jinit=None, delta=1e-4, maxit=100, mv_eps=1e-6,
             mv_maxit=100, max_ranks=None, kickrank=None)
core_list = approx.TT
if use_legendre:
    result = ExtendedTT.from_grid_to_legendre(core_list)
else:
    result = ExtendedTT.from_grid_to_hermite(core_list)

result = result * (-1)
result.normalise()                                  # normalise to obtain better numerical behaviour
result.round(1e-16)
result = result.multiply_with_extendedTT(result)

potential = result * noise()**(-1)*(-0.5)

nodes, _ = gauss(n_nodes)
orig_tensor_nodes = np.zeros((n_nodes, 2))
for lia in range(n_nodes):
    node = np.array([nodes[lia] for lib in range(2)])
    orig_tensor_nodes[lia, :] = node

potential_ten_eval = potential.evaluate_at_grid(orig_tensor_nodes.T)
# euler_method = EmbeddedRungeKutta(euler_steps, euler_precision, euler_rank, euler_global_mc_samples,
#                                   euler_repetitions, euler_local_mc_samples)
# potential_ten = ExtendedTT(potential_ten.components, basis=[BasisType.NormalisedHermite]*potential_ten.dim)
# euler_method = EmbeddedRungeKutta(1000000, 1e-6, 50, 1, 1, 20)
# euler_method = RankAdaptiveEmbeddedRungeKutta(1000000, 1e-6, 50, 1, 1, 20)
# euler_method = RungeKutta(1000, 1e-8, 100, 1, 1, 20)
euler_method = ImplicitEuler(100, 1e-8, 200, 10, 1, 20)
# euler_method = ExtendedTTEmbeddedRungeKutta(10000, 1e-8, 20, 1, 1, 20)
euler_method.method = "forth"
# euler_method.method = "3rk"
euler_method.acc = 1e-5
euler_method.min_stepsize = 1e-19
euler_method.max_stepsize = 0.888
potential_ten_tt = tt.vector.from_list(potential_ten_eval.components)
# potential.round(tol=1e-14)
print(potential)
exp_ten_euler, _ = euler_method.calculate_exponential_tt(potential_ten_tt)
if use_legendre:
    exp_ten_euler = ExtendedTT.from_grid_to_legendre(tt.vector.to_list(exp_ten_euler))
else:
    exp_ten_euler = ExtendedTT.from_grid_to_hermite(tt.vector.to_list(exp_ten_euler))
print(exp_ten_euler)
exp_ten_euler.round(1e-16)
print(exp_ten_euler)
exp_ten_euler = exp_ten_euler.multiply_with_extendedTT(exp_ten_euler)
euler_mean = exp_ten_euler.mean()
exp_ten_euler = exp_ten_euler * euler_mean ** (-1)

grid = np.linspace(-1, 1, num=30, endpoint=True)
pot_values_tt = np.zeros((len(grid), len(grid)))
pot_values = np.zeros((len(grid), len(grid)))
exp_values_tt = np.zeros((len(grid), len(grid)))
exp_values = np.zeros((len(grid), len(grid)))

if use_legendre:
    def prior_den(_x): return 0.5
else:
    def prior_den(_x): return np.sqrt(2*np.pi)**(-1)*np.exp(-0.5*_x**2)


def posterior(node):
    # return np.exp((-1)*(noise_value-measurement_operator(node[0], node[1]))**2 * noise()**(-1))
    return np.exp((-1)*(measurement_operator(node[0], node[1])) ** 2 * noise() ** (-1))


bar = Bar("Plot potential values", max=len(grid) ** 2)
for lia, x in enumerate(grid):
    for lib, y in enumerate(grid):
        node = [x, y]
        pot_values_tt[lib, lia] = potential(node)
        # pot_values[lib, lia] = (noise_value - measurement_operator(node[0], node[1]))**2 * noise()**(-1)*(0.5)
        pot_values[lib, lia] = (measurement_operator(node[0], node[1])) ** 2 * noise() ** (-1) * (-0.5)
        exp_values_tt[lib, lia] = exp_ten_euler(node) * prior_den(x) * prior_den(y)
        exp_values[lib, lia] = posterior(node) * prior_den(x) * prior_den(y)
        bar.next()
bar.finish()


if use_legendre:
    mc_mean = np.sum([posterior(np.random.rand(2)*2-1) for lib in range(10000)]) / 10000
else:
    mc_mean = np.sum([posterior(np.random.randn(2)) for lib in range(10000)]) / 10000

print("mc mean={}".format(mc_mean))
print("euler mean={}".format(euler_mean))
X, Y = np.meshgrid(grid, grid)
levels = np.arange(-10, 10, 0.1)
# levels = [-7, -5, -3, -1, 0, 1, 3, 5, 7, 10]
# levels = [0.0, 0.03, 0.1, 0.2, 0.3, 0.4, 0.5, 0.55]
ax = plt.subplot(2, 2, 1, projection="3d")
cont = ax.plot_surface(X, Y, pot_values, cmap=cm.coolwarm, linewidth=0, antialiased=False)
ax.clabel(cont, inline=1, fontsize=5)
plt.colorbar(cont)
plt.title("True potential Surf")

ax = plt.subplot(2, 2, 2)
cont = plt.contour(X, Y, pot_values, levels=levels)
ax.clabel(cont, inline=1, fontsize=5)
plt.colorbar(cont)
plt.title("True potential contour")

ax = plt.subplot(2, 2, 3, projection="3d")
cont = ax.plot_surface(X, Y, pot_values_tt, cmap=cm.coolwarm, linewidth=0, antialiased=False)
plt.colorbar(cont)

plt.title("TT potential Surf")

ax = plt.subplot(2, 2, 4)
cont = plt.contour(X, Y, pot_values_tt, levels=levels)
plt.colorbar(cont)

plt.title("TT potential contour")
plt.tight_layout()

plt.figure()
ax = plt.subplot(2, 2, 1, projection="3d")
plt.title("True posterior surf")
levels = [0.005, 0.02, 0.05, 0.08]
cont = ax.plot_surface(X, Y, exp_values*mc_mean**(-1), cmap=cm.coolwarm, linewidth=0, antialiased=False)
ax.clabel(cont, inline=1, fontsize=5)
plt.colorbar(cont)

ax = plt.subplot(2, 2, 2)
plt.title("True posterior contour")

levels = [0.005, 0.02, 0.05, 0.08]
cont = plt.contour(X, Y, exp_values * mc_mean**(-1), levels=levels)
ax.clabel(cont, inline=1, fontsize=5)
# plt.colorbar(cont)

ax = plt.subplot(2, 2, 3, projection="3d")
cont = ax.plot_surface(X, Y, exp_values_tt, cmap=cm.coolwarm, linewidth=0, antialiased=False, vmax=1)
plt.colorbar(cont)
ax.clabel(cont, inline=1, fontsize=10)
plt.title("TT posterior surf")

# levels = np.arange(0, 10, 0.1)
ax = plt.subplot(2, 2, 4)
cont = plt.contour(X, Y, exp_values_tt)
# plt.colorbar(cont)
ax.clabel(cont, inline=1, fontsize=10)
plt.title("TT posterior contour")
plt.tight_layout()

cond_expectation = exp_ten_euler.components
cond_expectation = ExtendedTT(cond_expectation, basis=[BasisType.points]*2)
print(exp_ten_euler)
print(cond_expectation([1, 0]))
print(cond_expectation([0, 1]))
print("mean: {}".format(cond_expectation([0, 0])))



plt.show()

# endregion
