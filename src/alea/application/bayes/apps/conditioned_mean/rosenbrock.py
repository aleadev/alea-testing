# -*- coding: utf-8 -*-
from __future__ import division, print_function
import numpy as np
import scipy.sparse as sps
import scipy.sparse.linalg as spsl
import matplotlib.pyplot as plt

X = Y = np.linspace(-1, 1, 1000)
X, Y = np.meshgrid(X, Y)
plt.subplot(2, 2, 1)
plt.contourf(X, Y, np.load('leg_moon.npz')['orig'])
plt.colorbar()
plt.title("Orig")

plt.subplot(2, 2, 2)
plt.contourf(X, Y, np.load('leg_moon.npz')['appr'])
plt.colorbar()
plt.title("appr")

plt.subplot(2, 2, 3)
plt.contourf(X, Y, np.load('leg_moon.npz')['reco'])
plt.colorbar()
plt.title("reco")
plt.tight_layout()
plt.show()
exit()

import xerus as xe
from_array = xe.Tensor.from_ndarray


N = 1000
deg_p = 100


def f(x,y):
    # parameters
    a = 1
    b = 100
    # normalization constants for fixed parameters
    d = 5.5/2
    e = 6-d
    # rescaled variables
    xt = (2*x+1)
    yt = (e*y+d)
    r = (a-xt)**2 + b*(yt-xt**2)**2
    # r is the Rosenbrock function
    return np.exp(-r)

X = Y = np.linspace(-1, 1, N)
F = f(X[:,None], Y[None,:])
F[F<2e-1] = 0


# # P1-Basis
# u,s,vt = spsl.svds(F, deg_p) # deg_p largest singular values
# _F = (u*s).dot(vt)
# # Complete Basis
# H = np.diag(s)
# def eval(x,y):
#     e0 = np.zeros(N); e0[0] = 1
#     e = lambda i: np.roll(e0, i)
#     idx = lambda x: np.round((N-1)*(x+1)/2).astype(int)
#     return e(idx(x)).dot(u), vt.dot(e(idx(y)))
# __F = lambda H: u.dot(H).dot(vt)
# # Reduced Basis
# H = _F
# def eval(x,y):
#     e0 = np.zeros(N); e0[0] = 1
#     e = lambda i: np.roll(e0, i)
#     idx = lambda x: np.round((N-1)*(x+1)/2).astype(int)
#     return e(idx(x)), e(idx(y))
# __F = lambda H: H


# Legendre-Basis
from numpy.polynomial.legendre import legval
fn = np.sqrt(2/(2*np.arange(deg_p)+1))
ps = legval(X, np.eye(deg_p)) / fn[:, None]
H = ps.dot(F).dot(ps.T) # projection of F onto the polynomial basis
_F = ps.T.dot(H).dot(ps)
def eval(x,y):
    return (legval([x,y], np.eye(deg_p)) / fn[:, None]).T
__F = lambda H: ps.T.dot(H).dot(ps)


# # Hermite-Basis
# from numpy.polynomial.hermite_e import hermeval
# from scipy.special import factorial
# fn = (factorial(np.arange(deg_p), exact=True)**(1/2)).astype(float)
# ps = hermeval(X, np.eye(deg_p)) / fn[:, None]
# H = ps.dot(F).dot(ps.T) # projection of F onto the polynomial basis
# _F = ps.T.dot(H).dot(ps)
# def eval(x,y):
#     return (hermeval([x,y], np.eye(deg_p)) / fn[:, None]).T
# __F = lambda H: ps.T.dot(H).dot(ps)



# Z = 2*np.random.rand(N*int(np.log(N)),2)-1
# Z = 2*np.random.rand(N**2,2)-1
Zx,Zy = np.meshgrid(X,Y)
Z = np.vstack([Zx.ravel(), Zy.ravel()]).T
Ze = np.array([f(*Zi) for Zi in Z])

measurements = xe.RankOneMeasurementSet()
for i,(pos,val) in enumerate(zip(Z,Ze)):
    print('\r[{0:2d}%] Creating Measurement Set'.format(int(100*i/len(Z))), end='')
    px,py = eval(*pos)
    pos = [from_array(px), from_array(py)]
    measurements.add(pos, val)
print('\r[done] Creating Measurement Set')

result = xe.TTTensor(from_array(H))

ADF = xe.ADFVariant(100, 1e-12, 0.99999)            # maxIterations = 1000
                                                    # targetResidualNorm = 1e-8
                                                    # minimalResidualNormDecrease = 0.999
pd = xe.PerformanceData(True)                       # print = True
#                                                   # run adf algorithm
maxRanks = [100]
ADF(result, measurements, maxRanks, pd)

_H = xe.Tensor(result).to_ndarray()
__F = __F(_H)

# L2-norm: sqrt(vec(_F-F) @ M @ vec(_F-F)) where M = 4*diag(1/N**2)
print(np.linalg.norm(_F - F, 'fro')*2/N)
print(np.linalg.norm(__F - F, 'fro')*2/N)

np.savez_compressed('moon', orig=F, appr=_F, reco=__F)
