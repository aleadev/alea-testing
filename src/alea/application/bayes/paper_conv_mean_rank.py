# !usr/bin/env python
# -*- coding: latin-1 -*-
"""
  sample TT-tensor Bayesian inversion results in view of normalization constant (mean), density and solution errors
"""
# region Imports
from __future__ import division                     # use float division without dot

# region Bayes libraries
from Bayes_util.lib.bayes_lib import create_parameter_samples
from Bayes_util.exponential.implicit_euler import ImplicitEuler

from Bayes_util.asgfem_bayes import ASGFEMBayes
from Bayes_util.exponential.embedded_runge_kutta import EmbeddedRungeKutta
from Bayes_util.lib.interpolation_util import get_cheb_points
# endregion

# region Standard library imports
from joblib import Parallel, delayed                # use parallel computing and function delaying
import os                                           # use system directory and file library
import argparse                                     # use command parameter parsing
import logging.config                               # use logging to file
import matplotlib                                   # use plot library
import matplotlib.pyplot as plt
import numpy as np                                  # use Matlab like array notation
import copy
matplotlib.use('Agg')                               # use 'Agg' extension to use in cluster environment
# endregion
# endregion
__author__ = "marschall"

# region Setup parameters
# region Parser Arguments
# region ASGFEM parameter
parser = argparse.ArgumentParser(description="estimate the MC convergence against the TT-mean")
parser.add_argument("-re", "--refinements", type=int, default="5",
                    help="# of refinements in ASGFEM")
parser.add_argument("-sr", "--srank", type=int, default="10",
                    help="start rank")
parser.add_argument("-gd", "--gpcdegree", type=int, default="10",
                    help="maximal # of gpc degrees")
parser.add_argument("-fd", "--femdegree", type=int, default="1", choices=range(1, 4),
                    help="femdegree used in ASGFEM")
parser.add_argument("-md", "--max_dofs", type=float, default="1e6",
                    help="maximal # of dofs in ASGFEM")
parser.add_argument("-it", "--iterations", type=int, default="1000",
                    help="maximal # of iterations")
parser.add_argument("-ps", "--polysys", type=str, default="L", choices=["L"],
                    help="system of polynomials used in ASGFEM")
parser.add_argument("-dom", "--domain", type=str, default="square", choices=["square"],
                    help="domain used in ASGFEM")
parser.add_argument("-ct", "--coeffield-type", type=str, default="cos", choices=["cos"],
                    help="coefficient type")
parser.add_argument("-at", "--amp_type", type=str, default="constant", choices=["constant", "decay-inf"],
                    help="amplification function type")
parser.add_argument("-de", "--decay_exp_rate", type=float, default=0,
                    help="rate of decay in the coefficient field")
parser.add_argument("-fm", "--field_mean", type=float, default=2.0,
                    help="mean of the coefficient field")
parser.add_argument("-sg", "--sgfem_gamma", type=float, default=0.9,
                    help="value of gamma in the ASGFEM coefficient field")
parser.add_argument("-rv", "--rvtype", type=str, default="uniform", choices=["uniform"],
                    help="random variable type used in algorithm")
parser.add_argument("-imd", "--init_mesh_dofs", type=int, default=-1,
                    help="# of initial physical mesh dofs (-1: ignore)")
parser.add_argument("-tx", "--thetax", type=float, default=0.5,
                    help="factor of deterministic refinement (0: no refinement)")
parser.add_argument("-ty", "--thetay", type=float, default=0.5,
                    help="factor of stochastic refinement (0: no refinement)")
parser.add_argument("-nc", "--n_coefficients", type=int, default=5,
                    help="# of coefficients in the stochastic field approximation (aka M)")
# endregion
# region Observation parameter
parser.add_argument("-ns", "--n_samples_sqrt", type=int, default=3,
                    help="# of sample points in physical domain. (ns --> ns^2 points)")
parser.add_argument("-op", "--obs_precision", type=float, default=0.0,
                    help="precision used in TT.round in the observation process")
parser.add_argument("-or", "--obs_rank", type=int, default=500,
                    help="maximal rank used in TT.round in the observation process")
# endregion
# region Inner Product parameter
parser.add_argument("-ip", "--inp_precision", type=float, default=0.0,
                    help="precision used in TT.round in the calculation of the inner product")
parser.add_argument("-ir", "--inp_rank", type=int, default=500,
                    help="maximal rank used in TT.round in the calculation of the inner product")
parser.add_argument("-ig", "--inp_gamma", type=float, default=1e-1,
                    help="gamma. constant value of covariance operator")
# endregion
# region Exponential calculation parameter
parser.add_argument("-ep", "--euler_precision", type=float, default=0.0,
                    help="precision used in TT.round in the calculation of the exponential")
parser.add_argument("-er", "--euler_rank", type=int, default=50,
                    help="maximal rank used in TT.round in the calculation of the exponential")
parser.add_argument("-els", "--euler_local_mc_samples", type=int, default=1,
                    help="# of MC samples to check the convergence of each euler step (highly optional)")
parser.add_argument("-egs", "--euler_global_mc_samples", type=int, default=1000,
                    help="# of MC samples to check the convergence of the resulting exponential (highly optional)")
parser.add_argument("-erep", "--euler_repetitions", type=int, default=0,
                    help="# of repetitions of the euler procedure if the desired accuracy is not reached (only euler)")
parser.add_argument("-es", "--euler_steps", type=int, default=1000,
                    help="# of steps done in the iterative process. For adaptive only start value.")
parser.add_argument("-egp", "--euler_global_precision", type=float, default=1e-10,
                    help="desired accuracy to reach in the exponential approximation")
parser.add_argument("-eui", "--euler_use_implicit", action="store_true",
                    help="switch to use the implicit euler scheme for more stability.")
parser.add_argument("-rm", "--runge_method", type=str, default="forth", choices=["linear", "square", "forth"],
                    help="used embedded runge kutta method")
parser.add_argument("-rn", "--runge_min_stepsize", type=float, default=1e-19,
                    help="minimal step size to check in adaptive step size control")
parser.add_argument("-rx", "--runge_max_stepsize", type=float, default=1-1e-10,
                    help="maximal step size to check in adaptive step size control")
# endregion
# region Density parameter
parser.add_argument("-ng", "--n_eval_grid", type=int, default=5,
                    help="# of evaluation points to estimate densities (for plots only)")
# endregion
# region Stochastic collocation parameter
parser.add_argument("-ny", "--Ny", type=int, default=10,
                    help="# of collocation points in one stochastic dimension")
# endregion
# region File saving paths
parser.add_argument("-fp", "--file_path", type=str, default="results/paper/mean/",
                    help="path to store plots and results")
parser.add_argument("-bp", "--bayes_path", type=str, default="results/bayes/",
                    help="path to store the results of the bayesian procedure")
parser.add_argument("-pp", "--plot_path", type=str, default="results/bayes/",
                    help="path to store the plots of the bayesian procedure. incl the .dat for latex plots")
# endregion

# region special mean convergence parameter
parser.add_argument("-zmc", "--z_mc_samples", type=int, default=100,
                    help="# of mc_samples for the TT-Mean convergence")
parser.add_argument("-cpu", "--n_cpu", type=int, default=40,
                    help="# CPUs used for parallel computation")
parser.add_argument("-mean", "--sample_mean", action="store_true",
                    help="switch to use the mean sampling with -zmc samples.")
parser.add_argument("-post", "--sample_post_mean", action="store_true",
                    help="switch to use the mean sampling of the solution w.r.t to the posterior with -zmc samples.")
parser.add_argument("-cont", "--plot_contour", action="store_true",
                    help="switch to plot the resulting bayesian measure for coeff 1 and 2.")
# endregion
args = parser.parse_args()
print("========== PARAMETERS: {}".format(vars(args)))
# endregion
# region Store Parser Arguments
#   region Parameters to use in stochastic Galerkin for the forward solution operator
refinements = args.refinements 			            # # of refinement steps in sg
srank = args.srank					                # start ranks
gpcdegree = args.gpcdegree					        # maximal # of gpcdegrees
femdegree = args.femdegree  	                    # fem function degree
max_dofs = args.max_dofs                            # desired maximum of dofs to reach in refinement process
iterations = args.iterations                        # used iterations in tt ALS
polysys = args.polysys                              # used polynomial system
domain = args.domain                                # used domain, square, L-Shape, ...
coeffield_type = args.coeffield_type                # used coefficient field type
amp_type = args.amp_type                            # used amplification functions
decay_exp_rate = args.decay_exp_rate                # decay rate for non constant coefficient fields
field_mean = args.field_mean                        # mean value of the coefficient field
sgfem_gamma = args.sgfem_gamma                      # Gamma used in adaptive SG FEM
rvtype = args.rvtype                                # used random variable type
init_mesh_dofs = args.init_mesh_dofs                # initial mesh refinement. -1 : ignore
thetax = args.thetax                                # factor of deterministic refinement (0 : no refinement)
thetay = args.thetay                                # factor of stochastic refinement (0 : no refinement)
n_coefficients = args.n_coefficients                # number of coefficients (aka M)
#   endregion
# region Observation operator parameters

n_samples_sqrt = args.n_samples_sqrt                # number of nodes squared
sample_coordinates = [[i/int(n_samples_sqrt+1), j/int(n_samples_sqrt+1)]
                      for i in range(1, n_samples_sqrt+1) for j in range(1, n_samples_sqrt+1)]
print("sample_coordinates: {}".format(sample_coordinates))

obs_precision = args.obs_precision                  # precision used for rounding in the observation operator
obs_rank = args.obs_rank                            # maximal rank to round to in the observation operator
# endregion
# region Inner Product parameter
inp_precision = args.inp_precision                  # precision used in the calculation of the inner product
inp_rank = args.inp_rank                            # maximal rank used in the calculation of the inner product
inp_gamma = args.inp_gamma                          # covariance constant value
# endregion
# region Euler Parameter
euler_precision = args.euler_precision              # precision to use in euler rounding
euler_rank = args.euler_rank                        # rank to round down in euler method
#                                                   # samples in every euler step
euler_local_mc_samples = args.euler_local_mc_samples
#                                                   # samples for the final result
euler_global_mc_samples = args.euler_global_mc_samples
euler_repetitions = args.euler_repetitions          # number of repetitions with half step size
euler_steps = args.euler_steps                      # start euler steps
#                                                   # global precision to reach after the euler scheme
euler_global_precision = args.euler_global_precision
euler_use_implicit = args.euler_use_implicit        # switch to use the implicit euler method
runge_method = args.runge_method                    # name of the runge kutta method to use if to used method explicit
runge_min_stepsize = args.runge_min_stepsize        # minimal step size to check in adaptive step size control setup
runge_max_stepsize = args.runge_max_stepsize        # maximal step size to check in adaptive step size control setup
# endregion
# region Density parameters
n_eval_grid = args.n_eval_grid                      # number of evaluation points for the density plots
eval_grid_points = np.linspace(-1, 1, n_eval_grid)  # grid for density plots
# endregion
# region setup collocation
Ny = args.Ny                                        # # of collocation points in each stochastic dimension
#                                                   # one dimensional stochastic grid of Chebyshev nodes
stochastic_grid = np.array(get_cheb_points([Ny])[0])
# endregion

# region Logging
logging.config.fileConfig('logging.conf')           # load logger configuration
log = logging.getLogger('test_bayes')               # get new logger object
# endregion
# region Path check and creation
file_path = args.file_path
try:
    if not os.path.isdir(file_path):
        os.mkdir(file_path)
except OSError:
    log.error("can not create file path: {}".format(file_path))
    exit()

bayes_path = args.bayes_path
try:
    if not os.path.isdir(bayes_path):
        os.mkdir(bayes_path)
except OSError:
    log.error("can not create bayes path: {}".format(bayes_path))
    exit()

plot_path = args.plot_path
try:
    if not os.path.isdir(plot_path):
        os.mkdir(plot_path)
except OSError:
    log.error("Can not create plot path: {}".format(plot_path))
    exit()
# endregion
# region special mean convergence parameter
z_mc_samples = args.z_mc_samples
n_cpu = args.n_cpu
sample_mean = args.sample_mean
sample_post_mean = args.sample_post_mean
plot_contour = args.plot_contour
# endregion
# endregion
# endregion

# region Object Initialisation
bayes_obj = None
coeff_field = None
proj_basis = None
true_values = None
measurements = None
prior_densities = None

bayes_fem_list = []

mean_error_h1_fem = []
mean_error_h1_rel_fem = []
mean_error_l2_fem = []
mean_error_l2_rel_fem = []
mean_tt_dofs_fem = []
mean_refinements_fem = []

poly_degree_p1 = []
poly_degree_p2 = []
poly_degree_p3 = []


bayes_obj_list = []                                 # list of Bayes objects due to multiple refinements in forward sol

ranks = [2, 4, 18, 36]   # , 48, 64, 96]# , 128, 256, 512, 1028]

# used_items = [4, 20, 30]
# used_items = range(44)
# used_items = [lia for lia in range(45)]             # P3
used_items = [lia for lia in range(36)]           # P2
# used_items = [lia for lia in range(17)]           # P1
used_fem_degree = 2
# endregion


class CacheItem(object):
    pass
# Currently used to calculate mean with different ranks. Only P1
for test_rank in ranks:
    for fem_degree in range(used_fem_degree, used_fem_degree+1):
        z_mc_sample_list = []                           # list of mc samples
        x_achses = []                                   # x-achses list
        dofs = []                                       # we use the coordinate sampling
        for used_item in used_items:
            inp_rank = test_rank
            obs_rank = test_rank

            if euler_use_implicit:
                euler_method = ImplicitEuler(euler_steps, euler_precision, test_rank, euler_global_mc_samples,
                                             euler_repetitions, euler_local_mc_samples)
            else:
                euler_method = EmbeddedRungeKutta(euler_steps, euler_precision, test_rank, euler_global_mc_samples,
                                                  euler_repetitions, euler_local_mc_samples)
                euler_method.method = runge_method
                euler_method.acc = euler_global_precision
                euler_method.min_stepsize = runge_min_stepsize
                euler_method.max_stepsize = runge_max_stepsize
            # region Init Bayes Object
            bayes_obj = ASGFEMBayes.init_solver(dofs, sample_coordinates, true_values, obs_precision,
                                                obs_rank, inp_precision, inp_rank, inp_gamma, euler_method,
                                                euler_global_precision, eval_grid_points, prior_densities,
                                                stochastic_grid,
                                                iterations=iterations, refinements=refinements, max_dofs=max_dofs,
                                                thetax=thetax,
                                                thetay=thetay, srank=srank,  m=n_coefficients+1,
                                                femdegree=fem_degree, gpcdegree=gpcdegree, polysys=polysys,
                                                gamma=sgfem_gamma, rvtype=rvtype, domain=domain,
                                                mesh_dofs=init_mesh_dofs, problem=0,
                                                coeffield_type=coeffield_type,
                                                amp_type=amp_type, sol_index=-1, field_mean=field_mean
                                                )
            cache = CacheItem()
            # endregion
            if true_values is None:
                log.info("Create {} true parameters".format(len(bayes_obj.forwardOp.SOL[-1]["V"].n)-1))
                true_values = create_parameter_samples(len(bayes_obj.forwardOp.SOL[-1]["V"].n)-1)
                true_values[0] = 0.25
                true_values[1] = -1/3
            if measurements is None:
                log.info("Create {} measurements".format(len(sample_coordinates)))
                measurements = np.array([bayes_obj.forwardOp.compute_direct_sample_solution(true_values, sol_index=-1,
                                                                                            cache=cache)
                                         (sample_coordinate)
                                         for sample_coordinate in sample_coordinates])
                log.info("  add noise to the measurements")
                for lic in range(len(measurements)):
                    measurements[lic] += np.random.randn(1, 1)[0, 0] * inp_gamma
            # currently only the last solution (or rather just the 10th)
            # for lia in range(len(bayes_obj.forwardOp.SOL)-2, len(bayes_obj.forwardOp.SOL)-1):
            for lia in range(used_item, used_item+1):
                bayes_obj.set_sol_index(lia)
                # if fem_degree == 3:
                #     print "file path: {}".format(hash(bayes_obj.file_name))
                if bayes_obj.import_bayes(bayes_path):
                    bayes_obj_list.append(copy.copy(bayes_obj))
                    # works only since we use the same forward solution but different ranks !!!
                    measurements = bayes_obj.measurements
                    true_values = bayes_obj.true_values
                else:
                    # region Setup truth
                    bayes_obj.true_values = true_values[:len(bayes_obj.forwardOp.SOL[lia]["V"].n)-1]
                    # endregion
                    # region Setup Gaussian Prior
                    Gauss_prior = False
                    if Gauss_prior:
                        raise NotImplemented
                    else:
                        prior_densities = [lambda x: 0.5 * np.ones_like(x)] * bayes_obj.forwardOp.coefficients

                    bayes_obj.prior_densities = prior_densities
                    # endregion
                    # region Setup noisy measurements
                    bayes_obj.measurements = measurements
                    # endregion
                    print("start calculation for SOL[{}] / {} with P{}".format(lia+1, len(bayes_obj.forwardOp.SOL),
                                                                               fem_degree))
                    bayes_obj.set_sol_index(lia)
                    bayes_obj.calculate_densities()
                    bayes_obj_list.append(copy.copy(bayes_obj))
                    log.info('export Bayes file %s', bayes_path + bayes_obj.file_name + '.dat')
                    if not bayes_obj.export_bayes(bayes_path):
                        log.error("can not save Bayes object")
                    print("P{} solution.n: {}".format(fem_degree, bayes_obj.forwardOp.SOL[-1]["V"].n))
                    print("P{} solution.r: {}".format(fem_degree, bayes_obj.forwardOp.SOL[-1]["V"].r))
                    print("P{} dofs: {}".format(fem_degree, bayes_obj.forwardOp.SOL[-1]["DOFS"]))
                if plot_contour:
                    Z = np.zeros((len(bayes_obj.eval_grid_points), len(bayes_obj.eval_grid_points)))
                    Z_MC_coeff = np.zeros((len(bayes_obj.eval_grid_points), len(bayes_obj.eval_grid_points)))
                    Z_MC_direct = np.zeros((len(bayes_obj.eval_grid_points), len(bayes_obj.eval_grid_points)))
                    from Bayes_util.lib.bayes_lib import evaluate
                    X, Y = np.meshgrid(eval_grid_points, eval_grid_points)
                    max_value = 0
                    max_lix = 0
                    max_lic = 0
                    for lix in range(len(eval_grid_points)):
                        for lic in range(len(eval_grid_points)):
                            Z[lix, lic] = evaluate(
                                bayes_obj.solution,
                                _poly='Lagrange',
                                _samples=[0]+[eval_grid_points[lix]]+[eval_grid_points[lic]]+[0]*(len(bayes_obj.solution
                                                                                                      .n)-3))
                            print("({}, {})".format(lix, lic))
                            true_solution = bayes_obj.forwardOp.sample_at_coeff([eval_grid_points[lix],
                                                                                 eval_grid_points[lic]] +
                                                                                [0]*(len(bayes_obj.solution.n)-3),
                                                                                sol_index=used_item)
                            Z_MC_coeff[lix, lic] = np.exp(inp_gamma**(-1)*(-0.5*np.sum([(bayes_obj.measurements[lib] -
                                                                                         (true_solution(
                                                                                             sample_coordinates[lib])))
                                                                                        ** 2 for lib in
                                                                                        range(len(sample_coordinates))])
                                                                           )
                                                          )
                            true_solution_direct = bayes_obj.\
                                forwardOp.compute_direct_sample_solution([eval_grid_points[lix],
                                                                          eval_grid_points[lic]] +
                                                                         [0]*(len(bayes_obj.solution.n)-3),
                                                                         sol_index=used_item)
                            Z_MC_direct[lix, lic] = np.exp(inp_gamma**(-1)*(-0.5*np.sum([(bayes_obj.measurements[lib] -
                                                                                          (true_solution_direct(
                                                                                              sample_coordinates[lib])))
                                                                                         ** 2 for lib in
                                                                                         range(len(sample_coordinates))]
                                                                                        )
                                                                            )
                                                           )
                            if Z[lix, lic] > max_value:
                                max_value = Z[lix, lic]
                                max_lix = lix
                                max_lic = lic

                    plt.contour(X, Y, Z)
                    plt.plot(bayes_obj.true_values[1], bayes_obj.true_values[0], 'xb', label="Truth")
                    print("max at ({},{}) vs truth at ({},{})".format(eval_grid_points[max_lix],
                                                                      eval_grid_points[max_lic],
                                                                      bayes_obj.true_values[0],
                                                                      bayes_obj.true_values[1]))
                    plt.plot(eval_grid_points[max_lic], eval_grid_points[max_lix], 'oc', label="max")
                    plt.ylabel("coeff 2")
                    plt.ylabel("coeff 1")
                    plt.xlim([-1, 1])
                    plt.ylim([-1, 1])
                    plt.legend()
                    if not os.path.exists(plot_path + "/P{}_R{}_TT{}/".format(fem_degree, test_rank, used_item)):
                        os.mkdir(plot_path + "/P{}_R{}_TT{}/".format(fem_degree, test_rank, used_item))
                    plt.savefig(plot_path + "/P{}_R{}_TT{}/".format(fem_degree, test_rank, used_item) +
                                "contour_Measure{}".format(len(bayes_obj.measurements)) +
                                str(hash(bayes_obj.file_name)) + ".png")
                    plt.clf()
                    plt.contour(X, Y, Z_MC_coeff)
                    plt.plot(bayes_obj.true_values[1], bayes_obj.true_values[0], 'xb', label="Truth")
                    plt.xlim([-1, 1])
                    plt.ylim([-1, 1])
                    plt.legend()
                    plt.savefig(plot_path + "/P{}_R{}_TT{}/".format(fem_degree, test_rank, used_item) +
                                "coeff_contour_Measure{}".format(len(bayes_obj.measurements)) +
                                str(hash(bayes_obj.file_name)) + ".png")
                    plt.clf()
                    plt.contour(X, Y, Z_MC_direct)
                    plt.plot(bayes_obj.true_values[1], bayes_obj.true_values[0], 'xb', label="Truth")
                    plt.xlim([-1, 1])
                    plt.ylim([-1, 1])
                    plt.legend()
                    plt.savefig(plot_path + "/P{}_R{}_TT{}/".format(fem_degree, test_rank, used_item) +
                                "direct_contour_Measure{}".format(len(bayes_obj.measurements)) +
                                str(hash(bayes_obj.file_name)) + ".png")
                    plt.clf()
# region Z' sampling
if sample_post_mean:
    mean_error_h1 = []
    mean_error_l2 = []
    mean_error_h1_rel = []
    mean_error_l2_rel = []
    refine_list = []
    tt_dofs = []
    # for lic in range(len(bayes_obj_list)):
    #     print "sol_index={}".format(bayes_obj_list[lic].sol_index)
    #     print "Bayes {}: {}".format(lic,bayes_obj_list[lic].solution)
    #     print "solution {}: {}".format(lic,bayes_obj_list[lic].forwardOp.SOL[lic]["V"])
    for lic in range(len(bayes_obj_list)):
        print("start posterior solution mean sampling for SOL[{}] / {} ".format(lic+1, len(bayes_obj_list)))
        refine_list.append(lic)
        tt_dofs.append(bayes_obj.forwardOp.SOL[lic]["DOFS"])
        _mean_error_l2_rel, \
            _mean_error_h1_rel = bayes_obj_list[lic].estimate_posterior_solution_error(n_xi_samples=z_mc_samples,
                                                                                       n_z_xi_samples=z_mc_samples,
                                                                                       sol_index=lic, relative=True)
        # _mean_error_l2, \
        # _mean_error_h1 = bayes_obj_list[lic].estimate_posterior_solution_error(n_xi_samples=z_mc_samples,
        #                                                                        n_z_xi_samples=z_mc_samples,
        #                                                                        sol_index=lic, relative=False)
        _mean_error_l2 = 10
        _mean_error_h1 = 10
        mean_error_h1.append(_mean_error_h1)
        mean_error_l2.append(_mean_error_l2)
        mean_error_h1_rel.append(_mean_error_h1_rel)
        mean_error_l2_rel.append(_mean_error_l2_rel)

# endregion

# region Sample error of normalization constant
if sample_mean:
    print("start mean sampling")
    from Bayes_util.lib.bayes_lib import evaluate_marginal
    from alea.application.tt_asgfem.tensorsolver.tt_sgfem_util import tt_dofs

    # if True:
    #     print("len(gpcdegrees): {}".format(len(bayes_obj.forwardOp.SOL[-1]["gpcps"][-1])))
    #     print("gpcdegrees: {}".format(bayes_obj.forwardOp.SOL[-1]["gpcps"][-1]))
    #     print("solution.n: {}".format(bayes_obj.forwardOp.SOL[-1]["V"].n))
    #     print("solution.r: {}".format(bayes_obj.forwardOp.SOL[-1]["V"].r))
    #     print("dofs: {}".format(bayes_obj.forwardOp.SOL[-1]["DOFS"]))
    #     print("inverse ranks: {}".format(bayes_obj.solution.r))
    # region Create samples in parameter space
    xi_samples = np.array([np.random.rand(bayes_obj_list[-1].forwardOp.coefficients, 1)[:, 0]*2 - 1
                           for lic in range(z_mc_samples)])
    # endregion
    # region sample reference Z from finest list element

    def _sample_z_parallel_tt(xi):
        sol_buffer = bayes_obj_list[-1].forwardOp.compute_direct_sample_solution(list(xi))
        sol_measurements = np.array([sol_buffer(loc_dof) for loc_dof in bayes_obj_list[-1].sample_coordinates])
        # print("     (O o G)(u) at {0} = {1}".format(bayes_obj.sample_coordinates, sol_measurements))
        diff = np.array(bayes_obj_list[-1].measurements) - np.array(sol_measurements)
        # print("     given measurements: {}".format(bayes_obj.measurements))
        _Z = np.exp(-0.5 * np.sum((np.array(diff) ** 2) * (bayes_obj.inp_gamma ** (-1))))
        return _Z

    print("start Z sampling with {} steps on {} threads".format(z_mc_samples, n_cpu))
    z_mc_sample = np.sum(Parallel(n_cpu)(delayed(_sample_z_parallel_tt)(xi) for xi in xi_samples))
    z_mc_sample *= len(xi_samples)**(-1)
    # endregion
    for lia in range(len(ranks)):
        # region Export Z
        file_list_x = []
        file_tt_dofs = []
        file_tt_refine = []
        file_list_y = []
        file_list_Z = []
        # region Recalculate mean
        for lic, used_item in enumerate(used_items):
            file_tt_refine.append(used_item)
            x_achses.append(ranks[lia])

            bayes_obj = copy.copy(bayes_obj_list[lic + lia*(len(used_items))])
            file_tt_dofs.append(tt_dofs(bayes_obj.solution))
            if True:
                print("recalculate mean for item {} / {}".format(lic + lia*(len(used_items)), len(bayes_obj_list)))
                bayes_obj.mean = evaluate_marginal(bayes_obj.solution, 0, _samples=[0]*(len(bayes_obj.solution.n)-1))
                bayes_obj.mean *= 0.5**(len(bayes_obj.solution.n)-1)
                print("recalculate mean Done")
            Z_poly = bayes_obj.mean
            bayes_fem_list.append(bayes_obj_list)
            file_list_x.append(x_achses)                # # of mc samples (list[# mc samples])
            file_list_y.append(z_mc_sample)             # sampled Z (list[# mc samples])
            file_list_Z.append(Z_poly)                  # calculated Z (float)
        # endregion
        # region Export mean error to file
        with open(plot_path + "mean_error_rank{}_P{}.dat".format(ranks[lia], used_fem_degree), "w") as f:
            f.write("r, tt_dofs, tt_refine, MC_Z, BI_Z, diff, diff_rel\n")
            assert len(file_list_x) == len(used_items)
            assert len(file_list_y) == len(used_items)
            assert len(file_list_Z) == len(used_items)
            assert len(file_tt_dofs) == len(used_items)
            assert len(file_tt_refine) == len(used_items)
            for lic in range(len(used_items)):
                f.write(repr(ranks[lia]) + "," +
                        repr(file_tt_dofs[lic]) + "," +
                        repr(file_tt_refine[lic]) + "," +
                        repr(file_list_y[lic]) + "," +
                        repr(file_list_Z[lic]) + "," +
                        repr(np.abs(file_list_y[lic]-file_list_Z[lic])) + "," +
                        repr(np.abs(file_list_y[lic]-file_list_Z[lic])/file_list_Z[lic]) +
                        "\n")
        # endregion
    # endregion
    # region Export posterior solution mean error to file
    if sample_post_mean:
        assert len(mean_refinements_fem) == 3
        assert len(mean_tt_dofs_fem) == 3
        assert len(mean_error_l2_rel_fem) == 3
        assert len(mean_error_l2_fem) == 3
        assert len(mean_error_h1_rel_fem) == 3
        assert len(mean_error_h1_fem) == 3
        for fem_degree in range(3):
            with open(plot_path + "post_mean_error_P{}.dat".format(fem_degree+1), "w") as f:
                f.write("refine, tt_dof, L2, H1, L2_rel, H1_rel\n")
                for lia in range(len(mean_refinements_fem[fem_degree])):
                    assert len(mean_refinements_fem[fem_degree]) == len(mean_tt_dofs_fem[fem_degree])
                    assert len(mean_refinements_fem[fem_degree]) == len(mean_error_l2_fem[fem_degree])
                    assert len(mean_refinements_fem[fem_degree]) == len(mean_error_h1_fem[fem_degree])
                    assert len(mean_refinements_fem[fem_degree]) == len(mean_error_l2_rel_fem[fem_degree])
                    assert len(mean_refinements_fem[fem_degree]) == len(mean_error_h1_rel_fem[fem_degree])
                    f.write(repr(mean_refinements_fem[fem_degree][lia]) + "," +
                            repr(mean_tt_dofs_fem[fem_degree][lia]) + "," +
                            repr(mean_error_l2_fem[fem_degree][lia]) + "," +
                            repr(mean_error_h1_fem[fem_degree][lia]) + "," +
                            repr(mean_error_l2_rel_fem[fem_degree][lia]) + "," +
                            repr(mean_error_h1_rel_fem[fem_degree][lia]) + "\n")
    # endregion
