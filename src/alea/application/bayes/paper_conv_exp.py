from __future__ import division
import numpy as np
import tt
import tt.amen
import matplotlib.pyplot as plt

import os, sys
sys.path.insert(0,os.path.pardir)
from Bayes_util.exponential.explicit_euler import ExplicitEuler
from Bayes_util.exponential.implicit_euler import ImplicitEuler
from Bayes_util.exponential.runge_kutta import RungeKutta

import argparse                                     # use command parameter parsing

import time
parser = argparse.ArgumentParser(description="Exponential test")
parser.add_argument("-pp", "--plot_path", type=str, default="results/bayes/",
                    help="path to store the plots of the bayesian procedure. incl the .dat for latex plots")
#parser.add_argument("-steps", "--steps", type=float, default=100)

args = parser.parse_args()
plot_path = args.plot_path

dimensions = [1,3,5,2,7]           # dimensions of the random tensor
ranks = [1,8,6,7,5,1]              # ranks of the random tensor

euler_global_mc_samples = 100
euler_repetitions = 0
euler_local_mc_samples = 100

exp_euler_steps = 10             # number of steps to do in euler method
exp_euler_round_rank = 50          # maximal rank to start rounding at
exp_euler_precision = 1e-5         # precision to round tensor to

imp_euler_steps = 10                # number of steps to do in euler method
imp_euler_round_rank = 50          # maximal rank to start rounding at
imp_euler_precision = 1e-5         # precision to round tensor to
imp_euler_restarts = 50
                                   # number of sample points for the monte carlos sampling
MC_steps = int(np.prod(dimensions)/4)

y = [[np.random.random_integers(0, high=dimensions[lic]-1) for lic in range(len(dimensions))] for lia in range(MC_steps)]



# create a random tensor of given dimensions and ranks
A = tt.rand(dimensions, len(dimensions), ranks)
A = -0.005*A*A
steps = np.arange(1, 1000, 10)
diff_imp = []
diff_exp = []
diff_3rk = []
diff_heun = []
diff_runge = []
time_imp = []
time_exp = []
time_3rk = []
time_heun = []
time_runge = []

step_size = []
for step in steps:
    print "start exp method with {} steps".format(step)
    step_size.append(1/step)
    start = time.time()
    euler_explicit = ExplicitEuler(step, imp_euler_precision, imp_euler_round_rank, euler_global_mc_samples,
                                   euler_repetitions, euler_local_mc_samples)
    euler_explicit.calculate_exponential_tt_precision(tt.vector.copy(A), exp_euler_precision)
    exp_A_explicit = euler_explicit.solution
    time_exp.append(time.time() - start)

    start = time.time()
    euler_implicit = ImplicitEuler(step, exp_euler_precision, exp_euler_round_rank, euler_global_mc_samples,
                                   euler_repetitions, euler_local_mc_samples)
    euler_implicit.calculate_exponential_tt_precision(tt.vector.copy(A), imp_euler_precision)
    exp_A_implicit = euler_implicit.solution
    time_imp.append(time.time() - start)

    start = time.time()
    rungekutta_3rk = RungeKutta(step, imp_euler_precision, imp_euler_round_rank, euler_global_mc_samples,
                                euler_repetitions, euler_local_mc_samples)
    rungekutta_3rk.method = "3rk"
    rungekutta_3rk.calculate_exponential_tt_precision(tt.vector.copy(A), exp_euler_precision)
    exp_A_rungekutta_3rk = rungekutta_3rk.solution
    time_3rk.append(time.time() - start)

    start = time.time()
    rungekutta_heun = RungeKutta(step, imp_euler_precision, imp_euler_round_rank, euler_global_mc_samples,
                                euler_repetitions, euler_local_mc_samples)
    rungekutta_heun.method = "heun"
    rungekutta_heun.calculate_exponential_tt_precision(tt.vector.copy(A), exp_euler_precision)
    exp_A_rungekutta_heun = rungekutta_heun.solution
    time_heun.append(time.time() - start)

    start = time.time()
    rungekutta_runge = RungeKutta(step, imp_euler_precision, imp_euler_round_rank, euler_global_mc_samples,
                                euler_repetitions, euler_local_mc_samples)
    rungekutta_runge.method = "runge kutta"
    rungekutta_runge.calculate_exponential_tt_precision(tt.vector.copy(A), exp_euler_precision)
    exp_A_rungekutta_runge = rungekutta_runge.solution
    time_runge.append(time.time() - start)
    #print exp_A_rungekutta
    #print "diff {}".format(tt.vector.norm(A-exp_A_rungekutta))

    MC_mean_imp = 0
    MC_mean_exp = 0
    MC_mean_3rk = 0
    MC_mean_heun = 0
    MC_mean_runge = 0
    for lia, y_k in enumerate(y):
        #print "A(y_k)={2}, exp(A(y_k))={0}, e^A(y_k)_T={1}".format(np.exp(A[y_k]),exp_A[y_k],A[y_k])
        MC_mean_imp += np.abs(exp_A_implicit[y_k] - np.exp(A[y_k]))
        MC_mean_exp += np.abs(exp_A_explicit[y_k] - np.exp(A[y_k]))
        MC_mean_3rk += np.abs(exp_A_rungekutta_3rk[y_k] - np.exp(A[y_k]))
        MC_mean_heun += np.abs(exp_A_rungekutta_heun[y_k] - np.exp(A[y_k]))
        MC_mean_runge += np.abs(exp_A_rungekutta_runge[y_k] - np.exp(A[y_k]))
    MC_mean_imp /= MC_steps
    MC_mean_exp /= MC_steps
    MC_mean_3rk /= MC_steps
    MC_mean_heun /= MC_steps
    MC_mean_runge /= MC_steps
    diff_imp.append(MC_mean_imp)
    diff_exp.append(np.minimum(MC_mean_exp,1))
    diff_3rk.append(np.minimum(MC_mean_3rk,1))
    diff_heun.append(np.minimum(MC_mean_heun,1))
    diff_runge.append(np.minimum(MC_mean_runge,1))

with open(plot_path + "exp_error.dat", "w") as f:
    f.write("step_size, imp, exp, 3rk, heun, runge, t_imp, t_exp, t_3rk, t_heun, t_runge\n")
    for lia in range(len(step_size)):
        f.write(repr(step_size[lia]) + "," +
                repr(diff_imp[lia]) + "," +
                repr(diff_exp[lia]) + "," +
                repr(diff_3rk[lia]) + "," +
                repr(diff_heun[lia]) + "," +
                repr(diff_runge[lia]) + "," +
                repr(time_imp[lia]) + "," +
                repr(time_exp[lia]) + "," +
                repr(time_3rk[lia]) + "," +
                repr(time_heun[lia]) + "," +
                repr(time_runge[lia]) + "\n")
# plt.figure()
# plt.loglog(step_size,diff_imp, '-r', label="impl. Euler")
# plt.loglog(step_size,diff_exp, '.-r', label="expl. Euler")
# plt.loglog(step_size,diff_3rk, '-g', label="runge kutta 3")
# plt.loglog(step_size,diff_heun, '-b', label="heun")
# plt.loglog(step_size,diff_runge, '-s', label="runge kutta")
# plt.loglog(step_size,np.array(step_size)**1,'--r', label="linear")
# plt.loglog(step_size,np.array(step_size)**2,'--b', label="sqaure")
# plt.loglog(step_size,np.array(step_size)**3,'--g', label="cubic")
# plt.legend(ncol =2, loc=0)
# plt.title("Convergence rate")
# plt.figure()
# plt.loglog(step_size,time_imp, label="impl. Euler")
# plt.loglog(step_size,time_exp, label="expl. Euler")
# plt.loglog(step_size,time_3rk, label="runge kutta 3")
# plt.loglog(step_size,time_heun, label="heun")
# plt.loglog(step_size,time_runge, label="runge kutta")
# plt.title("time consumption")
# plt.show()