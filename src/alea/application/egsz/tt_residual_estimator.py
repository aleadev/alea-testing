from __future__ import division

import numpy as np
from dolfin import Function, FunctionSpace
from alea.fem.fenics.residual_estimator import evaluate_residual_main_component, evaluate_residual_neighbour_components
from alea.application.tt_asgfem.tensorsolver.tt_param_pde import (ttCanonicalFEMEstimator, ttParamProdFirstVec,
                                                                  ttCollapseTailOnly)

parallel_w1 = None
parallel_w2 = None
parallel_coeff_field_mean_func = None
parallel_w2vec = None
parallel_wlist = None
parallel_a_m = None
parallel_V = None

from copy import copy
from gc import collect

def do_parallel(k, l, _assemble_quad_degree, _with_volume, _with_edge):
        # print(" (i={},j={})".format(i,j))
        _w1 = copy(parallel_w1)
        _w2 = copy(parallel_w2)
        _coeff_field_mean_func = copy(parallel_coeff_field_mean_func)
        _w2vec = copy(parallel_w2vec)
        _wlist = copy(parallel_wlist)
        _a_m = copy(parallel_a_m)
        _V = copy(parallel_V)
        _tail = ttCollapseTailOnly(_wlist[k], _wlist[l])
        _ww = _w2vec.dot(_tail.T)
        _curr_item = 0
        for k1 in range(_w2vec.shape[1]):
            # print("    k1 = {} / {}".format(k1, _w2vec.shape[1]))
            _w2.vector()[:] = _w2vec[:,k1]
            # print("parallel ({},{}) k1={} _w2vec[:,k1]= {}".format(i, j, k1, _w2vec[:,k1]))
            _w1.vector()[:] = np.ascontiguousarray(_ww[:,k1])
            _eta_res_local2 = evaluate_residual_neighbour_components(_V, _coeff_field_mean_func, _w1, _w2, _a_m[k], _a_m[l],
                                                  assemble_quad_degree=_assemble_quad_degree,
                                                  with_volume=_with_volume, with_edge=_with_edge)
            _curr_item += _eta_res_local2

        collect()
        #if k==0 and l==0:
        #    print("parallel ({},{}) curr_item= {}".format(k, l, _curr_item))
        return _curr_item

def ttEvaluateResidualEstimator(U, D, V, coeff_field, f, assemble_quad_degree=-1, with_volume=True, with_edge=True,
                                PH=None, ncpu=1):
    # eta_local = []
    eta_local = 0
    eta_local_parallel = 0
    M = U.d
    # compute a_i
    #---------------------------
    a_m = [coeff_field._mean_func]
    for i in range(M-1):
        a_m.append(coeff_field[i][0])

    # compute first part
    # --------------------------
    w0 = ttParamProdFirstVec(U, D)
    eta_res_local2_ = evaluate_residual_main_component(V, a_m, w0, f, assemble_quad_degree=assemble_quad_degree, with_volume=with_volume)
    eta_local = eta_res_local2_

    # compute second part
    # -----------------------
    w1, w2 = Function(V), Function(V)
    wlist, w2vec = ttCanonicalFEMEstimator(U, D)

    if ncpu > 1:                                    # use parallel computing
        from joblib import Parallel, delayed        # import parallel computing and function delaying

        global parallel_w2                          # define global variables should not be pickled
        global parallel_w1
        global parallel_coeff_field_mean_func
        global parallel_w2vec
        global parallel_wlist
        global parallel_a_m
        global parallel_V

        parallel_w1 = Function(V)                   # assign values to global variables
        parallel_w2 = Function(V)
        parallel_coeff_field_mean_func = coeff_field._mean_func
        parallel_w2vec = w2vec
        parallel_wlist = wlist
        parallel_a_m = a_m
        parallel_V = V
        #                                           # create double index list of tuples (i,j) from 0, to M-1
        lis = [(lia, lic) for lia in range(M) for lic in range(M)]
        eta_local_parallel = copy(eta_local)        # copy result from main component
        #                                           # run once due to fenics bug?!
        _ = do_parallel(0, 0, assemble_quad_degree, with_volume, with_edge)
        #                                           # estimate all residuals parallel
        eta_local_parallel_buf = np.array(np.array(Parallel(ncpu)(delayed(do_parallel)(lia, lic, assemble_quad_degree,
                                                                                       with_volume, with_edge)
                                                                  for (lia, lic) in lis)))
        #                                           # sum over all residuals
        eta_local_parallel += np.sum(eta_local_parallel_buf, axis=0)
        eta_local = copy(eta_local_parallel)        # copy again
    else:
        curr_item = 0
        for i in range(M):
            for j in range(M):
                tail = ttCollapseTailOnly(wlist[i],wlist[j])
                ww = w2vec.dot(tail.T)
                for k1 in range(w2vec.shape[1]):
                    w2.vector()[:] = w2vec[:,k1]
                    if PH is not None:
                        PH["w2"].plot(w2, interactive=False)
                    w1.vector()[:] = np.ascontiguousarray(ww[:,k1])
                    if PH is not None:
                        PH["w1"].plot(w1, interactive=False)

                    eta_res_local2_ = evaluate_residual_neighbour_components(V, coeff_field._mean_func, w1, w2, a_m[i], a_m[j],
                                                                            assemble_quad_degree=assemble_quad_degree,
                                                                            with_volume=with_volume, with_edge=with_edge)
                    curr_item += eta_res_local2_
        eta_local += curr_item

    # sum up contributions
    eta_global = np.sqrt(sum([eta2 for eta2 in eta_local]))
    eta_local = np.sqrt(eta_local)
    #print eta_global
    #print eta_local

    return eta_local, eta_global



# ===================================================
# reference code for TT residual estimator validation
# ===================================================

from alea.math_utils.multiindex_set import MultiindexSet
from alea.math_utils.multiindex import Multiindex
from alea.application.tt_asgfem.tensorsolver.tt_util import ttGetVec
from dolfin import (FacetNormal, nabla_grad, nabla_div, inner, TestFunction, CellSize, dx, dS, avg,
                        jump, assemble, cells, Constant, Expression)

def _evaluateResidualEstimatorMode(mu, Lambda, U, V, coeff_field, f, assemble_quad_degree=15):
    def get_wmu(U, V, mu):
        # TODO: cache w[mu]
        wmu = Function(V)
        wmu.vector()[:] = ttGetVec(U, mu)
        return wmu

    M = U.d
    mesh = V.mesh()
    nu = FacetNormal(mesh)
    a0_f = coeff_field._mean_func

    # (A) initialise sigma_mu, volume and edge residuals with mu component
    wmu = get_wmu(U, V, mu)
    sigma_mu = a0_f * nabla_grad(wmu)

    # (B) iterate neighbours of mu
    if len(coeff_field) < M:
        logger.warning("insufficient length of coefficient field for MultiVector (%i < %i)", len(coeff_field), M)
        M = len(coeff_field)
        #        assert coeff_field.length >= M        # ensure coeff_field expansion is sufficiently long
    for m in range(M):
        am_f, am_rv = coeff_field[m]

        # prepare polynom coefficients
        beta = am_rv.orth_polys.get_beta(mu[m])

        # mu (NOTE: this term vanishes for symmetric measures)
        sigma_mu -= am_f * beta[0] * nabla_grad(wmu)

        # mu+1
        mu1 = mu.inc(m)
        if mu1 in Lambda:
            w_mu1 = get_wmu(U, V, mu1)
            sigma_mu += am_f * beta[1] * nabla_grad(w_mu1)

        # mu-1
        mu2 = mu.dec(m)
        if mu2 in Lambda:
            w_mu2 = get_wmu(U, V, mu2)
            sigma_mu += am_f * beta[-1] * nabla_grad(w_mu2)

    # setup volume and edge terms
    R_T = nabla_div(sigma_mu)
    if not mu:
        R_T += f
    R_E = jump(sigma_mu, nu)

    # prepare more FEM variables for residual assembly
    DG = FunctionSpace(mesh, "DG", 0)
    s = TestFunction(DG)
    h = CellSize(mesh)

    # scaling of residual terms and definition of residual form
    res_form = (h ** 2 * (1 / a0_f) * R_T ** 2 * s * dx
                + avg(h) * (1 / avg(a0_f)) * R_E ** 2 * 2 * avg(s) * dS)

    # map DG dofs to cell indices
    dofs = [DG.dofmap().cell_dofs(c.index())[0] for c in cells(mesh)]
    # FEM evaluate residual estimator on mesh
    eta = assemble(res_form, form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
    eta_local = np.sqrt(np.array(eta))[dofs]
    eta = np.sqrt(sum(eta))
    return eta_local, eta


def evaluateResidualEstimator(U, D, V, coeff_field, f, gpcp, assemble_quad_degree=-1, with_volume=True, with_edge=True, PH=None):
    def get_Lambda(gpcp):
        assert len(gpcp) <= 15, "Lambda set might get too large"
        Lambda = MultiindexSet.createFullTensorSet(len(gpcp), max(gpcp))
        idx = np.all(Lambda <= np.array(gpcp), axis=1)
        Lambda = [Multiindex(mi) for mi in Lambda[idx]]
        return Lambda

    Lambda = get_Lambda(gpcp)
    eta, eta_local = {}, {}
    for mu in Lambda:
        eta_local[mu], eta[mu] = _evaluateResidualEstimatorMode(mu, Lambda, U, V, coeff_field, f, assemble_quad_degree)
    eta_global = np.sqrt(sum([v ** 2 for v in eta.values()]))
    eta_local = np.sqrt(sum([v ** 2 for v in eta_local.values()]))
    return eta_local, eta_global
