from __future__ import division
import numpy as np

from alea.application.egsz.sample_problems2 import SampleProblem


class AffineField(object):
    def __init__(self, coeff_type, amptype, decayexp, gamma, freqscale, freqskip, scale, coef_mean=0.0,
                 rv_type="normal"):
        # setup coeff field
        self.coeff_field = SampleProblem.setupCF(coeff_type, decayexp=decayexp, gamma=gamma, freqscale=freqscale,
                                                 freqskip=freqskip, rvtype=rv_type, scale=scale, coeff_mean=coef_mean)
        # decay coefficient function
        self.ampfunc = SampleProblem.get_ampfunc(amptype, decayexp, gamma)
        self.coeff_type = coeff_type
        self.amptype = amptype
        self.decayexp = decayexp
        self.gamma = gamma
        self.scale = scale
        self.freqscale = freqscale
        self.freqskip = freqskip
        self.coef_mean = coef_mean
        self.rv_type = "normal"

    def __getitem__(self, i):
        if i > 0:
            return self.coeff_field[i - 1], self.ampfunc(i)
        else:
            return (self.coeff_field.mean_func, None), 1

    def evaluate_basis(self, x, M):
        assert M > 0
        N = x.shape[0]
        V = np.zeros((N, M + 1))
        # evaluate all basis functions
        for i, p in enumerate(x):
            V[i, :] = [self[m][0][0](p) for m in range(M + 1)]
        return V

    def __call__(self, x, y, cache=None):
        assert(isinstance(y, list))
        y = [1] + y
        M = len(y)
        N = x.shape[0]
        if cache is not None:
            if cache.func_list is not None:
                if len(cache.func_list) < M:
                    assert cache.fs is not None
                    cache.func_list = []
                    from dolfin import interpolate
                    for m in range(M):
                        fun = interpolate(self[m][0][0], cache.fs)
                        cache.func_list.append(fun.vector()[:])
                    cache.func_mat = np.array(cache.func_list)
                y_ = np.array(y)
                W = np.dot(y_, cache.func_mat)
                return W
                # V = sum([y[m] * cache.func_list[m] for m in range(M)])
                # assert np.allclose(W, V)
                # return V
        V = np.zeros(N)
        # evaluate sum of weighted basis functions at y
        for i, p in enumerate(x):
            # print("a({},{})={}".format(p, y, sum([y[m] * self[m][0][0](p) for m in range(M)])))
            V[i] = sum([y[m] * self[m][0][0](p) for m in range(M)])
        return V

    def sample_rvs(self, M):
        return [self.rvs(m).sample(1)[0] for m in range(M)]

    @property
    def rvs(self):
        return self.coeff_field.rvs

    def get_infty_norm(self, M):
        if self.coeff_type == 'EF-square-cos':  # using a decay that defines a convergent series in the affine part
            from scipy.special import zeta  # get zeta function
            #                                           # get decay start value from affine field
            start = SampleProblem.get_decay_start(self.decayexp, self.gamma)
            amp = self.gamma / zeta(self.decayexp, start)  # get corresponding amplification
            #                                           # \|a_m\|_\infty = scale*gamma/zeta * (m+start)^{-sigma}
            bxmax_m = [self.scale * amp * (i + start) ** (-self.decayexp) for i in range(M)]
            bxmax = np.array(bxmax_m)  # use np.arrays
        elif self.coeff_type == 'EF-square-cos-algebraic':
            #                                           # special case using algebraic decay
            # start = SampleProblem.get_decay_start(decayexp, gamma)
            amp = self.gamma
            start = 1
            bxmax_m = [self.scale * amp * (i + start) ** (-self.decayexp) for i in range(M)]
            # print bxmax_M
            bxmax = np.array(bxmax_m)
        else:
            raise NotImplementedError("other coefficient field types are not implemented")
        return bxmax

    def get_filename(self):
        filename = "AffineField"
        filename += "coeff_type:{}".format(self.coeff_type)
        filename += "amptype:{}".format(self.amptype)
        filename += "decayexp:{}".format(self.decayexp)
        filename += "gamma:{}".format(self.gamma)
        filename += "scale:{}".format(self.scale)
        filename += "freqscale:{}".format(self.freqscale)
        filename += "freqskip:{}".format(self.freqskip)
        filename += "coef_mean:{}".format(self.coef_mean)
        filename += "rv_type:{}".format(self.rv_type)
        return filename

    def get_hash(self):
        return hash(self.get_filename())
