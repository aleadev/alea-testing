from __future__ import division
from functools import partial
import logging

import numpy as np
import scipy as sp
from scipy.sparse.linalg.isolve import bicg, bicgstab, gmres, lgmres, lsqr, cgs
from scipy.sparse.linalg import LinearOperator

from alea.application.egsz.pcg import pcg
from alea.application.egsz.multi_operator2 import PreconditioningOperator, prepare_FEM
from alea.application.egsz.fem_discretisation import FEMDiscretisation, zero_function
from alea.fem.fenics.fenics_operator import FEniCSOperator, FEniCSSolveOperator
from alea.math_utils.multiindex import Multiindex
from alea.linalg.vector import FlatVector
from alea.fem.fenics.fenics_utils import error_norm

from dolfin import Constant, Expression, project, exp
import dolfin as df
import dolfin as df
import collections


# retrieve logger
logger = logging.getLogger(__name__)


# noinspection PyProtectedMember
def prepare_rhs(A, w, coeff_field, pde, fv=None):
    # incorporate boundary conditions into rhs, see [EGSZ 2014]
    b = 0 * w
    zero = Multiindex()
    if fv is None:
        b[zero].coeffs = pde.assemble_rhs(basis=b[zero].basis, coeff=coeff_field.mean_func,
                                          withNeumannBC=True)
    else:
        for mu, fvmu in fv.iteritems():
            assert mu in b.active_indices()
            b[mu].coeffs = pde.assemble_rhs(basis=b[zero].basis, coeff=coeff_field.mean_func,
                                            withNeumannBC=True, f=fvmu)
    # setup appropriate zero function
    if pde.f.value_rank() == 0:
        zero_func = Constant(0.0)
    else:
        zero_func = Constant((0.0,) * pde.f.value_size())
    # noinspection PyProtectedMember
    zero_func = zero_function(b[zero].basis._fefs)

    for m in range(w.max_order):
        eps_m = zero.inc(m)
        am_f, am_rv = coeff_field[m]
        beta = am_rv.orth_polys.get_beta(0)

        if eps_m in b.active_indices():
            g0 = b[eps_m].copy()
            g0.coeffs = pde.assemble_rhs(basis=b[eps_m].basis, coeff=am_f, withNeumannBC=False, f=zero_func)  # this equates to homogeneous Neumann bc
            pde.set_dirichlet_bc_entries(g0, homogeneous=True)
            b[eps_m] += beta[1] * g0

        g0 = b[zero].copy()
        g0.coeffs = pde.assemble_rhs(basis=b[zero].basis, coeff=am_f, f=zero_func)
        pde.set_dirichlet_bc_entries(g0, homogeneous=True)
        b[zero] += beta[0] * g0
    return b


def prepare_rhs_plain(w, coeff_field, pde, fv):
    # incorporate boundary conditions into rhs (non-symmetric)

    def make_list(x, length=None):
        """Make a sequence type out of some item if it not already is one"""
        import collections
        if not isinstance(x, collections.Sequence):
            x = [x]
        if length is not None:
            if len(x) == 1:
                x = x * length
            assert len(x) == length
        return x

    def create_dirichlet_bcs(V, uD, boundary):
        """create list of FEniCS boundary condition objects."""
        boundary = make_list(boundary)
        uD = make_list(uD, len(boundary))
        bcs = [df.DirichletBC(V, cuD, cDb) for cuD, cDb in zip(uD, boundary)]
        return bcs

    # noinspection PyProtectedMember
    def prepare_FEM(w, coeff_field, pde):
        FEM = {}
        FEM["pde"] = pde
        FEM["f"] = pde.f
        FEM["dirichlet_boundary"] = pde.dirichlet_boundary
        FEM["uD"] = pde.uD
        FEM["neumann_boundary"] = pde.neumann_boundary
        FEM["g"] = pde.g
        FEM["coeff"] = coeff_field
        FEM["coeff_mean"] = coeff_field.mean_func
        FEM["expa0"] = df.exp(FEM["coeff_mean"])
        FEM["basis"] = w.basis.basis
        # noinspection PyProtectedMember
        V = w.basis.basis._fefs
        FEM["V"] = V
        FEM["u"], FEM["v"] = df.TrialFunction(V), df.TestFunction(V)
        FEM["BC"] = create_dirichlet_bcs(V, FEM["uD"], FEM["dirichlet_boundary"])
        return FEM

    def assembleF(FEM, fvmu, with_Dirichlet_BC=True):
        v = FEM["v"]
        Lmu = df.assemble(fvmu * v * df.dx)
        if with_Dirichlet_BC:
            for bc in FEM["BC"]:
                bc.apply(Lmu)
        return Lmu

    # setup FEM variables for assembly
    FEM = prepare_FEM(w, coeff_field, pde)

    b = 0 * w
    for mu, fvmu in fv.iteritems():
        # print "----RRRRRRRRRRRRHS assembling rhs for mu", mu, fvmu((0.3,0.7))
        assert mu in b.active_indices()
        b[mu].coeffs = assembleF(FEM, fvmu, True)

    # TODO: move to better place
    # construct preconditioner
    u, v = FEM["u"], FEM["v"]
    P0 = df.assemble(df.exp(FEM["coeff_mean"]) * df.inner(df.nabla_grad(u), df.nabla_grad(v)) * df.dx)
    for bc in FEM["BC"]:
        bc.apply(P0)
    P = FEniCSSolveOperator(P0, w.basis.basis)
    return b, P


# noinspection PyProtectedMember,PyProtectedMember
def pcg_solve(A, w, coeff_field, pde, stats, pcg_eps, pcg_maxiter):
    b = prepare_rhs(A, w, coeff_field, pde)
    P = PreconditioningOperator(coeff_field.mean_func,
                                pde.assemble_solve_operator)

    w, zeta, numit = pcg(A, b, P, w0=w, eps=pcg_eps, maxiter=pcg_maxiter)
    logger.info("PCG finished with zeta=%f after %i iterations", zeta, numit)

    b2 = A * w
    stats["RESIDUAL-L2"] = error_norm(b, b2, "L2")
    stats["RESIDUAL-H1A"] = error_norm(b, b2, pde.energy_norm)
    # noinspection PyProtectedMember
    stats["DOFS"] = sum([b[mu]._fefunc.function_space().dim() for mu in b.keys()])
    # noinspection PyProtectedMember
    stats["CELLS"] = sum([b[mu]._fefunc.function_space().mesh().num_cells() for mu in b.keys()])
    logger.info("[pcg] Residual = [%s (L2)] [%s (H1A)] with [%s dofs] and [%s cells]", stats["RESIDUAL-L2"], stats["RESIDUAL-H1A"], stats["DOFS"], stats["CELLS"])
    return w, zeta


# noinspection PyProtectedMember,PyProtectedMember,PyProtectedMember
def gmres_solve(A, w, coeff_field, pde, stats, gmres_eps, gmres_maxiter, fv=None, plain=True):
    # prepare rhs for logtransformed problem
    if plain:
        b, P = prepare_rhs_plain(w, coeff_field, pde, fv)
    else:
        b = prepare_rhs(A, w, coeff_field, pde, fv=fv)
        P = PreconditioningOperator(coeff_field.mean_func,
                                    pde.assemble_solve_operator)

    # get to and from Euclidian vector conversion
    M2E = w.to_euclidian_operator
    E2M = w.from_euclidian_operator

    # operator application with vector format conversion
    def apply_A(v):
        r = E2M(FlatVector(v))          # to multivector
        r2 = A * r                      # operator action
        return M2E(r2).coeffs           # to Euclidian

    # # preconditioner computing P^-1 * x
    P = PreconditioningOperator(coeff_field.mean_func,
                                pde.assemble_solve_operator)
    def apply_P(v):
        r = E2M(FlatVector(v))          # to multivector
        r2 = P * r                      # operator action
        return M2E(r2).coeffs           # to Euclidian

    # prepare operators and rhs for scipy solver
    # noinspection PyProtectedMember
    N = M2E._dimsum
    P1 = LinearOperator((N,N), matvec=apply_P, dtype=np.float64)
    A1 = LinearOperator((N,N), matvec=apply_A, dtype=np.float64)
    b1 = M2E(b)
    # print N, b1.dim, type(b1)

    # callback generator for solver status tracking
    gmres_info = dict(counter=0, residuals=[])
    def make_callback():
        def callback(residuals):
            gmres_info["counter"] += 1
            gmres_info["residuals"].append(residuals)
            print "\t iteration", gmres_info["counter"], ":\tresidual =", residuals
        return callback

    # call solver
    x1, info = gmres(A1, b1.coeffs, tol=gmres_eps, maxiter=gmres_maxiter, callback=make_callback(), M=P1)
    w = E2M(FlatVector(x1))
    if info == 0:
        logger.info("GMRES finished with zeta=%f after %i iterations", gmres_info['residuals'][-1], gmres_info['counter'])
    else:
        logger.warning("GMRES did not converge!")

    # compute residual
    b2 = A * w
    stats["RESIDUAL-L2"] = error_norm(b, b2, "L2")
    stats["RESIDUAL-H1A"] = error_norm(b, b2, pde.energy_norm)
    # noinspection PyProtectedMember
    stats["DOFS"] = sum([b[mu]._fefunc.function_space().dim() for mu in b.keys()])
    # noinspection PyProtectedMember
    stats["CELLS"] = sum([b[mu]._fefunc.function_space().mesh().num_cells() for mu in b.keys()])
    logger.info("[pcg] Residual = [%s (L2)] [%s (H1A)] with [%s dofs] and [%s cells]", stats["RESIDUAL-L2"], stats["RESIDUAL-H1A"], stats["DOFS"], stats["CELLS"])

    # DEBUG---
    # df.plot(w[Multiindex()]._fefunc, "solution", interactive=True)
    # DEBUG---

    return w, gmres_info['residuals'][-1]


def prepare_tildef(coeff_field, f, Lambda, a_maxm=-1, V=None, normalised=False, ex_degree=5):
    # a_maxm: length of sum for first factor constant factor in expansion of \tilde{f}
    print "++++++++++ tildef: Lambda %s \tmaxm %i" %(str(Lambda), a_maxm)
    assert a_maxm >= 0

    # prepare expansion of rhs
    def amu(coeff_field, mu):
        def getcpp2(coeff_exp, p):
            if p == 0:
                return "1.0"
            else:
                cpp = coeff_exp.cppcode
                cpp = cpp.replace("A", str(coeff_exp.A)).replace("B", str(coeff_exp.B)).replace("freq", str(
                    coeff_exp.freq)).replace("m", str(coeff_exp.m)).replace("n", str(coeff_exp.n))
                return "pow(" + cpp + ",%i)" %p if p > 1 else cpp

        if len(mu) == 0:
            exp_a = Constant(1)
        else:
            a_ex_m = "*".join([getcpp2(coeff_field(m), mum) for m, mum in enumerate(mu)])
            a_ex_m = a_ex_m.replace("1.0*", "")

            # print "[solver_utils amu]", mu
            # print "=" * 80
            # print a_ex_m
            # print "=" * 80

            # cell_str = ',' if a_maxm > 0 else ''
            cell_str = " cell='triangle'"
            eval_m = "Expression('%s', %s, degree=%s)" % (a_ex_m, cell_str, ex_degree)
            exp_a = eval(eval_m)
        return exp_a

    if True:
        # NOTE: this variant creates a single large expression and so requires just one assembly with exp_a
        # should be much faster than the second variant below
        # NOTE: ampfunc is already included in coeff_field
        def getcpp1(coeff_exp):
            cpp = coeff_exp.cppcode
            return cpp.replace("A", str(coeff_exp.A)).replace("B", str(coeff_exp.B)).replace("freq", str(
                coeff_exp.freq)).replace("m", str(coeff_exp.m)).replace("n", str(coeff_exp.n))

        a_ex_m = "+".join([" (" + c_str + ")*(" + c_str + ") " for c_str in
                           [getcpp1(coeff_field(m)) for m in range(a_maxm)]])
        # a_ex_m = "+".join(["2.0*(" + str(a_0.A) + ")"] +
        #                   [" (" + c_str + ")*(" + c_str + ") " for c_str in
        #                    [getcpp1(coeff_field(m)) for m in range(a_maxm)]])
        a_ex_m = "exp(0.5 * (" + a_ex_m + "))".replace('1.0*', '')
        a_ex_m = a_ex_m.replace('1.0*', '')

        # print "[solver_utils tildef] squared coefficient"
        # print "=" * 80
        # print a_ex_m
        # print "=" * 80

        # cell_str = ',' if a_maxm > 0 else ''
        cell_str = " cell='triangle'"
        eval_m = "Expression('%s', %s, degree=%s)" % (a_ex_m, cell_str, ex_degree)
        exp_a = eval(eval_m)
    else:
        # NOTE: this was replaced by the above code
        a = 0     # mean a_0 is not part of the expansion
        for m in range(a_maxm):
            a += coeff_field(m) ** 2
        exp_a = exp(0.5 * a)

    normfac = lambda n: 1.0 / np.sqrt(sp.math.factorial(n)) if normalised else 1.0
    fv = {}
    for i, muv in enumerate(Lambda):
        print "--> mu %i/%i\t%s" %(i, len(Lambda), str(muv))
        mu = muv.as_array
        if len(mu) == 0:
            mu = [0]
        a_sgn = -1 if muv.order % 2 else 1
        a_mu = amu(lambda m: coeff_field(m), mu)
        fac_mu = np.prod([sp.math.factorial(mum) * normfac(mum) for mum in mu])
        fvmu = f * exp_a * a_sgn * a_mu / fac_mu
        fv[muv] = fvmu if V is None else project(fvmu, V)
    return fv
