"""EGSZ discrete operator A.

According to the representation of orthogonal polynomials in alea, the operator A defined in EGSZ (2.6) has the more general form

.. math:: A(u_N) := \overline{A}u_{N,\mu} + \sum_{m=1}^infty A_m\left(\alpha^m_{\mu_m+1}u_{N,\mu+e_m} - \alpha^m_{\mu_m}u_{N,\mu} + \alpha^m_{\mu_m-1}u_{N,\mu-e_m}\right)

where the coefficients :math:`(\alpha^m_{n-1},\alpha^m_n,\alpha^m_{n+1})` are obtained from the the three recurrence coefficients :math:`(a_n,b_n,c_n)` of the orthogonal polynomial by

.. math::
        \alpha_{n-1} &:= c_n/b_n \\
        \alpha_n &:= a_n/b_n \\
        \alpha_{n+1} &:= 1/b_n
"""

from alea.linalg.basis import Basis
from alea.linalg.operator import Operator
from alea.utils.type_check import takes, anything, optional
from alea.application.egsz.coefficient_field import CoefficientField
from alea.application.egsz.multi_vector import MultiVector, MultiVectorSharedBasis
from alea.fem.fenics.fenics_operator import FEniCSOperator

import dolfin as df
import collections

import logging
logger = logging.getLogger(__name__)


class MultiOperator(Operator):
    """Discrete operator according to EGSZ (2.6) but with just a single spatial grid, generalised for alea orthonormal polynomials."""

    @takes(anything, CoefficientField, callable, optional(callable), optional(Basis), optional(Basis))
    def __init__(self, coeff_field, assemble_0, assemble_m=None, domain=None, codomain=None):
        """Initialise discrete operator with FEM discretisation and coefficient field."""
        self._assemble_0 = assemble_0
        self._assemble_m = assemble_m or assemble_0
        self._coeff_field = coeff_field
        self._domain = domain
        self._codomain = codomain

    @takes(any, MultiVector)
    def apply(self, w):
        """Apply operator to vector which has to live in the same domain."""

        v = 0 * w
        Lambda = w.active_indices()
        maxm = w.max_order
        if len(self._coeff_field) < maxm:
            logger.warning("insufficient length of coefficient field for MultiVector (%i instead of %i", len(self._coeff_field), maxm)
            maxm = len(self._coeff_field)
        # assert self._coeff_field.length >= maxm        # ensure coeff_field expansion is sufficiently long

        V = w.basis.basis
        for mu in Lambda:
            # compute deterministic part
            a0_f = self._coeff_field.mean_func
            A0 = self._assemble_0(V, a0_f)
            cur_v = A0 * w[mu]

            # iterate related multiindices
            for m in range(maxm):
                logger.debug("with m = %i (mu = %s)", m, mu)
                # assemble A for \mu and a_m
                am_f, am_rv = self._coeff_field[m]
                Am = self._assemble_m(V, am_f)

                # prepare polynom coefficients
                beta = am_rv.orth_polys.get_beta(mu[m])

                # mu
                cur_w = -beta[0] * w[mu]

                # mu+1
                mu1 = mu.inc(m)
                if mu1 in Lambda:
                    cur_w += beta[1] * w[mu1]

                # mu-1
                mu2 = mu.dec(m)
                if mu2 in Lambda:
                    cur_w += beta[-1] * w[mu2]

                # apply discrete operator
                cur_v += Am * cur_w

                # import numpy as np
                # print "##################", mu, m, ":", np.linalg.norm((Am * cur_w)._fefunc.vector().array()), np.linalg.norm(np.dot(Am._matrix.array(), cur_w._fefunc.vector().array()))
                # print type(Am), type(cur_w)

            v[mu] = cur_v
        return v

    @property
    def domain(self):
        """Return the basis of the domain."""
        return self._domain

    @property
    def codomain(self):
        """Return the basis of the codomain."""
        return self._codomain


class LogTransformed_MultiOperator(Operator):
    """Discrete logtransformed operator according to EG (x.x) generalised for ALEA orthonormal polynomials."""

    @takes(anything, CoefficientField, callable, optional(callable), optional(Basis), optional(Basis))
    def __init__(self, coeff_field, assemble_0, assemble_m=None, domain=None, codomain=None):
        """Initialise discrete operator with FEM discretisation and coefficient field."""
        self._assemble_0 = assemble_0
        self._assemble_m = assemble_m if assemble_m is not None else assemble_0
        self._coeff_field = coeff_field
        self._domain = domain
        self._codomain = codomain

    @takes(any, MultiVector)
    def apply(self, w):
        """Apply operator to vector which has to live in the same domain."""

        v = 0 * w
        Lambda = w.active_indices()
        maxm = w.max_order
        if len(self._coeff_field) < maxm:
            logger.warning("insufficient length of coefficient field for MultiVector (%i instead of %i", len(self._coeff_field), maxm)
            maxm = len(self._coeff_field)
        # assert self._coeff_field.length >= maxm             # ensure coeff_field expansion is sufficiently long

        # compute deterministic part once
        V = w.basis.basis
        a0_f = self._coeff_field.mean_func
        a0_f.m = 0                                              # inject function index for CD discretisation
        A0 = self._assemble_0(V, a0_f)
        for mu in Lambda:
            cur_v = A0 * w[mu] # if mu.order == 0 else 0 * w[mu]

            # iterate related multiindices
            for m in range(maxm):
                logger.debug("with m = %i (mu = %s)", m, mu)
                # assemble A for \mu and a_m
                am_f, am_rv = self._coeff_field[m]
                am_f.m = m                                      # inject function index for CD discretisation
                Am = self._assemble_m(V, am_f)

                # prepare polynom coefficients
                beta = am_rv.orth_polys.get_beta(mu[m])

                # mu
                cur_w = -beta[0] * w[mu]

                # mu+1
                mu1 = mu.inc(m)
                if mu1 in Lambda:
                    cur_w += beta[1] * w[mu1]

                # mu-1
                mu2 = mu.dec(m)
                if mu2 in Lambda:
                    cur_w += beta[-1] * w[mu2]

                # apply discrete operator
                cur_v += Am * cur_w

            v[mu] = cur_v
        return v

    @property
    def domain(self):
        """Return the basis of the domain."""
        return self._domain

    @property
    def codomain(self):
        """Return the basis of the codomain."""
        return self._codomain


class LogTransformed_MultiOperator_plain(Operator):
    """Discrete logtransformed operator according to EG (x.x) generalised for ALEA orthonormal polynomials."""

    def __init__(self, coeff_field, pde, domain=None, codomain=None):
        """Initialise discrete operator with FEM discretisation and coefficient field."""

        self._coeff_field = coeff_field
        self._pde = pde
        self._domain = domain
        self._codomain = codomain

    @takes(any, MultiVector)
    def apply(self, w):
        """Apply operator to vector which has to live in the same domain."""

        def assembleA(FEM, m, with_Dirichlet_BC=True, project_coeff=True):
            # assemble operator
            u, v = FEM["u"], FEM["v"]
            # NOTE: mean scaling is done in tildef
            # expa0 = FEM["expa0"]
            if m < 0:
                # A = expa0 * df.inner(df.nabla_grad(u), df.nabla_grad(v))
                # print("--- assembleA -1")
                A = df.inner(df.nabla_grad(u), df.nabla_grad(v))
            else:
                a0 = FEM["coeff_mean"]
                am, am_rv = FEM["coeff"][m]
                if project_coeff:
                    a0 = df.interpolate(a0, FEM["V"])
                    am = df.interpolate(am, FEM["V"])
                # print("--- assembleA", m, "\t", a0((0.3,0.7)), am((0.3,0.7)))
                # A = -expa0 * df.inner(df.nabla_grad(a0 + am), df.nabla_grad(u)) * v
                A = -df.inner(df.nabla_grad(a0 + am), df.nabla_grad(u)) * v
            Am = df.assemble(A * df.dx)
            if with_Dirichlet_BC:
                for bc in FEM["BC"]:
                    bc.apply(Am)
            Am = FEniCSOperator(Am, FEM["basis"])

            if m < 0:
                return Am
            else:
                return Am, am_rv

        # setup FEM variables for assembly
        FEM = prepare_FEM(w, self._coeff_field, self._pde)

        v = 0 * w
        Lambda = w.active_indices()
        maxm = w.max_order
        if len(self._coeff_field) < maxm:
            logger.warning("insufficient length of coefficient field for MultiVector (%i instead of %i", len(self._coeff_field), maxm)
            maxm = len(self._coeff_field)
        # assert self._coeff_field.length >= maxm             # ensure coeff_field expansion is sufficiently long

        # compute deterministic part
        A0 = assembleA(FEM, -1, True)
        for mu in Lambda:
            logger.debug("plain CD operator for mu = %s", mu)
            # print "MultiOperator apply A0", A0._matrix.array()
            cur_v = A0 * w[mu]

            # iterate related multiindices
            for m in range(maxm):
                logger.debug("\twith m = %i (mu = %s)", m, mu)
                # assemble A for \mu and a_m
                Am, am_rv = assembleA(FEM, m, True)

                # prepare polynom coefficients
                beta = am_rv.orth_polys.get_beta(mu[m])

                # mu
                cur_w = -beta[0] * w[mu]

                # mu+1
                mu1 = mu.inc(m)
                if mu1 in Lambda:
                    cur_w += beta[1] * w[mu1]

                # mu-1
                mu2 = mu.dec(m)
                if mu2 in Lambda:
                    cur_w += beta[-1] * w[mu2]

                # apply discrete operator
                cur_v += Am * cur_w

            v[mu] = cur_v
        return v

    @property
    def domain(self):
        """Return the basis of the domain."""
        return self._domain

    @property
    def codomain(self):
        """Return the basis of the codomain."""
        return self._codomain


class PreconditioningOperator(Operator):
    """Preconditioning operator according to EGSZ section 7.1."""

    @takes(anything, anything, callable, optional(Basis), optional(Basis))
    def __init__(self, mean_func, assemble_solver, domain=None, codomain=None):
        """Initialise operator with FEM discretisation and
        mean diffusion coefficient"""
        self._assemble_solver = assemble_solver
        self._mean_func = mean_func
        self._domain = domain
        self._codomain = codomain

    @takes(any, MultiVector)
    def apply(self, w):
        """Apply operator to vector which has to live in the same domain."""
        v = 0 * w
        Delta = w.active_indices()

        for mu in Delta:
            a0_f = self._mean_func
            A0 = self._assemble_solver(w[mu].basis, a0_f)
            # if False:
            #     mat = A0._matrix
            #     M = mat.array()
            #     M2 = 0.5 * (M + M.T)
            v[mu] = A0 * w[mu]
        return v

    @property
    def domain(self):
        """Return the basis of the domain."""
        return self._domain

    @property
    def codomain(self):
        """Return the basis of the codomain."""
        return self._codomain


# TODO: the following should be moved to solver_utils which unfortunately is not possible directly due to circular imports

def make_list(x, length=None):
    """Make a sequence type out of some item if it not already is one"""
    if not isinstance(x, collections.Sequence):
        x = [x]
    if length is not None:
        if len(x) == 1:
            x = x * length
        assert len(x) == length
    return x


def create_dirichlet_bcs(V, uD, boundary):
    """create list of FEniCS boundary condition objects."""
    boundary = make_list(boundary)
    uD = make_list(uD, len(boundary))
    bcs = [df.DirichletBC(V, cuD, cDb) for cuD, cDb in zip(uD, boundary)]
    return bcs


# noinspection PyProtectedMember
def prepare_FEM(w, coeff_field, pde):
    FEM = {}
    FEM["pde"] = pde
    FEM["f"] = pde.f
    FEM["dirichlet_boundary"] = pde.dirichlet_boundary
    FEM["uD"] = pde.uD
    FEM["neumann_boundary"] = pde.neumann_boundary
    FEM["g"] = pde.g
    FEM["coeff"] = coeff_field
    FEM["coeff_mean"] = coeff_field.mean_func
    # FEM["expa0"] = df.exp(FEM["coeff_mean"])
    # noinspection PyProtectedMember
    V = w.basis.basis._fefs
    FEM["basis"] = w.basis.basis
    FEM["V"] = V
    FEM["u"], FEM["v"] = df.TrialFunction(V), df.TestFunction(V)
    FEM["BC"] = create_dirichlet_bcs(V, FEM["uD"], FEM["dirichlet_boundary"])
    return FEM
