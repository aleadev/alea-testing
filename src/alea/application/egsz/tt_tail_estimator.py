from __future__ import division
import numpy as np
import scipy.sparse as sps
# import itertools as iter
from operator import itemgetter
from dolfin import assemble, inner, dx, TrialFunction, TestFunction, parameters, nabla_grad, as_backend_type

from alea.fem.fenics.fenics_vector import FEniCSVector
# from alea.application.egsz.multi_vector import supp
# from alea.math_utils.multiindex import Multiindex
from alea.utils.decorators import cache
from alea.application.tt_asgfem.tensorsolver.tt_param_pde import ttComponentGPCEstimator
from alea.application.tt_asgfem.tensorsolver.tt_util import ravel

import logging
logger = logging.getLogger(__name__)

# try:
#     new_dolfin = float(dolfin_version()[:2]) >= 1.4
#     print "dolfin version", dolfin_version(), new_dolfin
# except:
#     new_dolfin = False


def ttEvaluateUpperTailBound(W, gpcp, V, coeff_field, pde, maxh=1 / 10, add_maxm=1000):
    """Estimate upper tail bounds."""

    # def determine_Lambda(gpcp, maxp=0):
    #     P = [range(p+1) for p in gpcp]
    #     Lambda = iter.product(*P)
    #     if maxp > 0:
    #         Lambda = [l for l in Lambda if sum(l) <= maxp]
    #     return [Multiindex(np.array(l)) for l in Lambda]

    @cache
    def get_ainfty(m, V):
        a0_f = coeff_field.mean_func
        if isinstance(a0_f, tuple):
            a0_f = a0_f[0]
        # determine min \overline{a} on D (approximately)
        f1 = FEniCSVector.from_basis(V, sub_spaces=0)
        f1.interpolate(a0_f)
        am_f, _ = coeff_field[m]
        if isinstance(am_f, tuple):
            am_f = am_f[0]

        # determine ||a_m/\overline{a}||_{L\infty(D)} (approximately)
        f2 = FEniCSVector.from_basis(V, sub_spaces=0)
        f2.interpolate(am_f)
        f3 = np.divide(f2.array, f1.array)      # TODO: revert...
        # print f1.array,f2.array
        ainftym = max(f3)
        # print ">>>>>>>>>>", max_am, min_a0, ainftym
        assert isinstance(ainftym, float)  # and ainftym > 0
        return ainftym

    # def LambdaBoundary(Lambda):
    #     suppLambda = supp(Lambda)
    #
    #     for mu in Lambda:
    #         for m in suppLambda:
    #             mu1 = mu.inc(m)
    #             if mu1 not in Lambda:
    #                 yield mu1, m
    #             mu2 = mu.dec(m)
    #             if mu2 not in Lambda and mu2 is not None:
    #                 yield mu2, m

    # prepare some variables
    # Lambda = determine_Lambda(gpcp, maxp=maxp)
    # suppLambda = supp(Lambda)
    # M = min(len(gpcp) + add_maxm, len(coeff_field))

    # retrieve (sufficiently fine) function space for maximum norm evaluation
    # V = V.refine_maxh(maxh)[0]

    # # evaluate estimator contributions of (3.16)
    # from collections import defaultdict
    # # === zeta ===
    # zeta, zeta_ym = defaultdict(int), defaultdict(int)

    # assembly stiffness matrix
    u, v = TrialFunction(V._fefs), TestFunction(V._fefs)
    # use uBLAS backend for conversion to scipy sparse array
    backend_backup = parameters.linear_algebra_backend
    try:
        parameters.linear_algebra_backend = "Eigen"
    except:
        parameters.linear_algebra_backend = "uBLAS"
    A = assemble(inner(nabla_grad(u), nabla_grad(v)) * dx)
    # A = assemble(inner(pde.weak_form.flux(u, pde.coeff), pde.weak_form.differential_op(v)) * dx)
    parameters.linear_algebra_backend = backend_backup
    # convert to scipy
    try:
        rows, cols, values = as_backend_type(A).data()
    except:
        rows, cols, values = A.data()
    A = sps.csr_matrix((values, cols, rows)).tocoo()

    # evaluate zeta_m estimators
    M = W.d
    AWmleft, AWmright = None, None
    zeta_ym, zeta_ym_tail = {}, []
    for m, p in enumerate(gpcp + [0] * (add_maxm - M)):
        _, am_rv = coeff_field[m]
        beta = am_rv.orth_polys.get_beta(p)
        ainfty = get_ainfty(m, V)
        if m <= M:
            w0, right = ttComponentGPCEstimator(W, m+1)
            AWmleft = ravel(A.dot(w0))
            AWmright = ravel(w0.dot(right.T))
        zeta2_ym = (ainfty * beta[1]) ** 2 * np.dot(AWmleft, AWmright)
        logger.debug("zeta2 %d", zeta2_ym)
        if m <= len(gpcp):
            zeta_ym[m] = np.sqrt(zeta2_ym)
            # print "WWWW m %i p %i" %(m, p)
        else:       # tail extension
            zeta_ym_tail.append((m, np.sqrt(zeta2_ym)))
    global_zeta = np.sqrt(sum(map(lambda x: x**2, zeta_ym.values())))
    return global_zeta, zeta_ym, zeta_ym_tail


def ttMark_y(zeta_ym_, zeta_ym_tail, theta_y, global_zeta_ym, longtail_zeta_marking=True):
    """Carry out Doerfler marking by activation of new indices."""
    max_y_dim = zeta_ym_tail[0][0] - 1
    zeta_ym = zeta_ym_

    # bulk marking
    # ============
    new_dim = []
    marked_zeta = 0.0
    update = True
    while True:
        if update:
            sorted_zeta_ym = sorted(zeta_ym.items(), key=itemgetter(1))
            logger.debug("SORTED ZETA_YM %s", sorted_zeta_ym)
            # print("SORTED ZETA_YM {}".format(sorted_zeta_ym))
            update = False
        # break if sufficiently many new degrees/dimensions are selected
        if theta_y * global_zeta_ym <= marked_zeta or len(sorted_zeta_ym) == 0:
            if len(sorted_zeta_ym) == 0:
                logger.warn("NO MORE Y-DIM TO MARK!")
            break
        # get largest y dimension
        new_zeta_ym = sorted_zeta_ym.pop(-1)
        ym = new_zeta_ym[0]
        zeta_ym.pop(ym)
        logger.debug("ADDING %s to new_dim %s", ym, new_dim)
        # print("ADDING {} to new_dim {}".format(ym, new_dim))
        new_dim.append(ym)
        marked_zeta = np.sqrt(marked_zeta ** 2 + new_zeta_ym[1] ** 2)
        # print "TailTester",global_zeta_ym,marked_zeta
        # print("longtail: {}, ym={}, max_y_dim={}, zeta_ym_tail={}".format(longtail_zeta_marking,
        #                                                                     ym, max_y_dim, zeta_ym_tail))
        # if new dimension was added, add next tail bound to set of possible y-refinements
        if not longtail_zeta_marking and ym == max_y_dim and len(zeta_ym_tail) > 0:
            new_tail_zeta = zeta_ym_tail.pop(0)
            zeta_ym[new_tail_zeta[0]] = new_tail_zeta[1]
            global_zeta_ym = np.sqrt(global_zeta_ym**2 + new_tail_zeta[1]**2)
            max_y_dim += 1
            # print "EXTENDING zeta set by", new_tail_zeta, max_y_dim, global_zeta_ym
            update = True

    if len(new_dim) > 0:
        print "SELECTED NEW DIMENSIONS %s" % new_dim
        logger.info("SELECTED NEW DIMENSIONS %s", new_dim)
    else:
        logger.info("NO NEW DIMENSIONS SELECTED")

    if longtail_zeta_marking:
        # transform into non-delta data again (not required...)
        zeta_ym = {k: zeta_ym_[k] for k in [k for k in zeta_ym.keys() if k not in new_dim]}
    return new_dim, marked_zeta, zeta_ym
