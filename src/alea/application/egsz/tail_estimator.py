from __future__ import division
import numpy as np
import itertools as iter
from dolfin import Function
from operator import itemgetter

from alea.fem.fenics.fenics_vector import FEniCSVector
from alea.application.egsz.multi_vector import supp
from alea.math_utils.multiindex import Multiindex
from alea.utils.decorators import cache
from alea.application.tt_asgfem.tensorsolver.tt_util import ttGetVec

import logging
logger = logging.getLogger(__name__)


def evaluateUpperTailBound(W, gpcp, V, coeff_field, pde, maxh=1 / 10, add_maxm=10, maxp=0):
    """Estimate upper tail bounds."""

    def determine_Lambda(gpcp, maxp=0):
        P = [range(p+1) for p in gpcp]
        Lambda = iter.product(*P)
        if maxp > 0:
            Lambda = [l for l in Lambda if sum(l) <= maxp]
        return [Multiindex(np.array(l)) for l in Lambda]

    @cache
    def get_ainfty(m, V):
        a0_f = coeff_field.mean_func
        if isinstance(a0_f, tuple):
            a0_f = a0_f[0]
        # determine min \overline{a} on D (approximately)
        f = FEniCSVector.from_basis(V, sub_spaces=0)
        f.interpolate(a0_f)
        min_a0 = f.min_val
        am_f, _ = coeff_field[m]
        if isinstance(am_f, tuple):
            am_f = am_f[0]

        # determine ||a_m/\overline{a}||_{L\infty(D)} (approximately)
        try:
            # use exact bounds if defined
            max_am = am_f.max_val
        except:
            # otherwise interpolate
            f.interpolate(am_f)
            max_am = f.max_val
        ainftym = max_am / min_a0
        # print ">>>>>>>>>>", max_am, min_a0, ainftym
        assert isinstance(ainftym, float)  # and ainftym > 0
        return ainftym

    def prepare_norm_w(energynorm, W, Lambda, V):
        F = Function(V)
        normw = {}
        for mu in Lambda:
            wmu = ttGetVec(W, mu)
            F.vector()[:] = wmu
            normw[mu] = energynorm(F)
        return normw

    def LambdaBoundary(Lambda):
        suppLambda = supp(Lambda)

        for mu in Lambda:
            for m in suppLambda:
                mu1 = mu.inc(m)
                if mu1 not in Lambda:
                    yield mu1, m
                mu2 = mu.dec(m)
                if mu2 not in Lambda and mu2 is not None:
                    yield mu2, m

    # evaluate (3.15)
    def eval_zeta_bar(mu, suppLambda, coeff_field, normw, V, M):
        assert mu in normw.keys()
        zz = 0
#            print "====zeta bar Z1", mu, M
        for m in range(M):
            if m in suppLambda:
                continue
            _, am_rv = coeff_field[m]
            beta = am_rv.orth_polys.get_beta(mu[m])
            ainfty = get_ainfty(m, V)
            zz += (beta[1] * ainfty) ** 2
        return normw[mu] * np.sqrt(zz)

    # evaluate (3.11)
    def eval_zeta(mu, Lambda, coeff_field, normw, V, M=None, this_m=None):
        z = 0
        if this_m is None:
            for m in range(M):
                _, am_rv = coeff_field[m]
                beta = am_rv.orth_polys.get_beta(mu[m])
                ainfty = get_ainfty(m, V)
                mu1 = mu.inc(m)
                if mu1 in Lambda:
                    # print "====beta", [beta[i-1] for i in range(3)]
                    # print "====zeta Z1", ainfty, beta[1], normw[mu1], " == ", ainfty * beta[1] * normw[mu1]
                    z += ainfty * beta[1] * normw[mu1]
                mu2 = mu.dec(m)
                if mu2 in Lambda:
                    # print "====beta", [beta[i-1] for i in range(3)]
                    # print "====zeta Z2", ainfty, beta[-1], normw[mu2], " == ", ainfty * beta[-1] * normw[mu2]
                    z += ainfty * beta[-1] * normw[mu2]
            return z
        else:
                m = this_m
                _, am_rv = coeff_field[m]
                beta = am_rv.orth_polys.get_beta(mu[m])
                ainfty = get_ainfty(m, V)
                # print "====beta", [beta[i-1] for i in range(3)]
                # print "====zeta Z3", m, ainfty, beta[1], normw[mu], " == ", ainfty * beta[1] * normw[mu]
                return ainfty * beta[1] * normw[mu]

    # prepare some variables
    energynorm = pde.energy_norm
    Lambda = determine_Lambda(gpcp, maxp=maxp)
    suppLambda = supp(Lambda)
    M = min(len(gpcp) + add_maxm, len(coeff_field))
    normw = prepare_norm_w(energynorm, W, Lambda, V._fefs)
    # retrieve (sufficiently fine) function space for maximum norm evaluation
    V = V.refine_maxh(maxh)[0]
    # evaluate estimator contributions of (3.16)
    from collections import defaultdict
    # === (a) zeta ===
    zeta, zeta_ym = defaultdict(int), defaultdict(int)
    # iterate multiindex extensions
    print "===A1 Lambda", Lambda
    for nu, m in LambdaBoundary(Lambda):
        assert nu not in Lambda
        print "===A2 boundary nu", nu
        zetanu = eval_zeta(nu, Lambda, coeff_field, normw, V, M)
        zeta[nu] += zetanu
        zeta_ym[m] += zetanu
    # === (b) zeta_bar ===
    zeta_bar = {}
    # iterate over active indices
    for mu in Lambda:
        zeta_bar[mu] = eval_zeta_bar(mu, suppLambda, coeff_field, normw, V, M)

    # evaluate summed estimator (3.16)
    global_zeta = np.sqrt(sum([v ** 2 for v in zeta.values()]) + sum([v ** 2 for v in zeta_bar.values()]))
    # also return zeta evaluation for single m (needed for refinement algorithm)
    eval_zeta_m = lambda mu, m: eval_zeta(mu=mu, Lambda=Lambda, coeff_field=coeff_field, normw=normw, V=V, M=M, this_m=m)
    logger.debug("=== ZETA  %s --- %s --- %s", global_zeta, zeta, zeta_bar)
    return global_zeta, zeta, zeta_ym, zeta_bar, eval_zeta_m, Lambda

def mark_y(Lambda, zeta, zeta_ym_, eval_zeta_m, theta_y, max_new_dim=100):
    """Carry out Doerfler marking by activation of new indices."""
    zeta_ym = zeta_ym_
    global_zeta_ym = np.sqrt(sum([z ** 2 for z in zeta_ym_.values()]))
    suppLambda = supp(Lambda)
    maxm = max(suppLambda)
    logger.debug("---- SUPPORT Lambda %s   maxm %s   Lambda %s ", suppLambda, maxm, Lambda)
    # modified paper marking
    # ========================
    new_dim = []
    marked_zeta_ym = 0.0
    while True:
        # break if sufficiently many new degrees/dimensions are selected
        if theta_y * global_zeta_ym <= marked_zeta_ym or len(new_dim) >= max_new_dim or len(zeta_ym) == 0:
            if len(new_dim) >= max_new_dim:
                logger.warn("max new_dim reached (%i) WITHOUT sufficient share of global zeta_ym!" % len(new_dim))
            if len(zeta_ym) == 0:
                logger.warn("NO MORE Y-DIM TO MARK!")
            break
        sorted_zeta_ym = sorted(zeta_ym.items(), key=itemgetter(1))
        logger.debug("SORTED ZETA_YM %s", sorted_zeta_ym)
        new_zeta_ym = sorted_zeta_ym[-1]
        ym = new_zeta_ym[0]
        zeta_ym.pop(ym)
        logger.debug("ADDING %s to new_dim %s", ym, new_dim)
        new_dim.append(ym)
        marked_zeta_ym = np.sqrt(marked_zeta_ym ** 2 + new_zeta_ym[1] ** 2)
        possible_new_mu = []
        minm = min(set(range(1, maxm + 2)).difference(set(suppLambda)))         # find min(N\setminus supp\Lambda)
        for mu2 in Lambda:
            new_mu = mu2.inc(minm)
            if new_mu not in zeta.keys():
                logger.debug("extending multiindex candidates by %s (y dimension %i) since it is at the boundary of Lambda (reachable from %s), minm: %s", new_mu, minm, mu2, minm)
                possible_new_mu += [new_mu]
                zetamu2 = eval_zeta_m(mu2, minm)
                zeta[new_mu] = zetamu2
                zeta_ym[minm] += zetamu2
                # update global zeta
                global_zeta = np.sqrt(global_zeta_ym ** 2 + zeta[new_mu] ** 2)
                logger.debug("new global_zeta is %f", global_zeta)
        else:
            logger.debug("no further extension of multiindex candidates required")
            if len(new_dim) >= max_new_dim:
                logger.debug("maximal number new y dimensions reached!")
            elif len(zeta) == 0:
                logger.debug("no more new dimensions available!")
        logger.info("possible new dimension considered %i (mi %s)" % (minm, possible_new_mu))

    if len(zeta) == 0:
        if theta_y * global_zeta > marked_zeta:
            logger.warning("list of mi candidates is empty and reduction goal NOT REACHED, %f > %f!", theta_y * global_zeta, marked_zeta)

    if len(new_dim) > 0:
        logger.info("SELECTED NEW DIMENSIONS %s", new_dim)
    else:
        logger.info("NO NEW DIMENSIONS SELECTED")

    return new_dim

def refine_y(w, new_mi):
    V = w.basis
    for mu in new_mi:
        w[mu] = V.basis.new_vector()
