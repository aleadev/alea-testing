from __future__ import division
import sys
import os
from glob import glob
import pickle
import csv
import logging
import numpy as np
from dolfin import set_log_level

if len(sys.argv) != 2:
    print "arguments: base-dir"
else:
    basedir = sys.argv[1]

# FEniCS logging
set_log_level(logging.WARNING)
fenics_logger = logging.getLogger("FFC")
fenics_logger.setLevel(logging.WARNING)
fenics_logger = logging.getLogger("UFL")
fenics_logger.setLevel(logging.WARNING)

# load simulation data
FNAME = 'SFEM2-SOLUTIONS-P*.pkl'
LOAD_SIM_FN = os.path.join(basedir, FNAME)
print "trying to load", LOAD_SIM_FN
SIM_STATS = {}
for fname in glob(LOAD_SIM_FN):
    P = int(fname[fname.find("-P") + 2:fname.find(".pkl")])
    print "loading P{0} solutions from {1}".format(P, fname)
    with open(fname, 'rb') as fin:
        w_history = pickle.load(fin)
    print "w_history has %s iterations" % len(w_history)

    SIM_STATS[P] = []
    w_ref = w_history[-1]
    for i, w in enumerate(w_history):
            # nw = np.square(sum([v.norm("L2") for v in w.values()]))
            nw = []
            for mu in w_ref.active_indices():
                f = w_ref[mu].copy()
                if mu in w.active_indices():
                    # noinspection PyProtectedMember,PyProtectedMember
                    f._fefunc.vector()[:] = f.array - f.interpolate(w[mu]._fefunc).array
                nw.append(f.norm("H1")**2)
            SIM_STATS[P].append(np.sqrt(sum(nw)))
print ">>>", SIM_STATS

# load export data and add new columns
FNAME = 'SIMDATA-P?.dat'
LOAD_DATA_FN = os.path.join(basedir, FNAME)
print "trying to load", LOAD_DATA_FN
for fname in glob(LOAD_DATA_FN):
    P = int(fname[fname.find("-P") + 2:fname.find(".dat")])
    print "loading P{0} data from {1}".format(P, fname)
    with open(fname, 'rb') as csvfile:
        csvreader = csv.reader(csvfile, delimiter='\t')
        data = [row for row in csvreader]
    N = len(data[0])
    assert N == 10
    print type(data[0]), data[0]
    data[0].extend(["L2err", "normeq"])
    data[1:] = [map(float, row) for row in data[1:]]
    data[1:] = [[int(row[0])] + row[1:] + [l2e, row[2]/l2e] for row, l2e in zip(data[1:], SIM_STATS[P])]

    # write out data
    fname = fname.replace(".dat", "-neq.dat")
    print "writing file", fname
    with open(fname, 'wb') as csvfile:
        csvwriter = csv.writer(csvfile, delimiter='\t',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for row in data:
            csvwriter.writerow(row)
