from __future__ import division
import os
import logging
from math import sqrt
from collections import defaultdict

from alea.application.egsz.multi_operator2 import MultiOperator, LogTransformed_MultiOperator, LogTransformed_MultiOperator_plain
from alea.application.egsz.sample_problems2 import SampleProblem
from alea.application.egsz.sample_domains import SampleDomain
from alea.application.egsz.mc_error_sampling import sample_error_mc
from alea.application.egsz.generate_reference_setup import generate_reference_setup
from alea.utils.plot.plotter import Plotter
try:
    from dolfin import (Function, FunctionSpace, Mesh, Constant, UnitSquareMesh,
                        plot, interactive, set_log_level, set_log_active, refine)
    from alea.application.egsz.egsz_utils import setup_logging
except:
    import traceback
    print traceback.format_exc()
    print "FEniCS has to be available"
    os.sys.exit(1)

# ------------------------------------------------------------
logger = logging.getLogger(__name__)

def run_MC(opts, conf):
    # propagate config values
    _G = globals()
    for sec in conf.keys():
        if sec == "LOGGING":
            continue
        secconf = conf[sec]
        for key, val in secconf.iteritems():
            print "CONF_" + key + "= secconf['" + key + "'] =", secconf[key]
            _G["CONF_" + key] = secconf[key]

    # setup logging
    _G["LOG_LEVEL"] = eval("logging." + conf["LOGGING"]["level"])
    print "LOG_LEVEL = logging." + conf["LOGGING"]["level"]    
    setup_logging(LOG_LEVEL, logfile=CONF_experiment_name + "_MC-P{0}".format(CONF_FEM_degree))
    
    # determine path of this module
    path = os.path.dirname(__file__)


    # ============================================================
    # PART A: Setup Problem
    # ============================================================
    
    # get boundaries
    mesh0, boundaries, dim = SampleDomain.setupDomain(CONF_domain, initial_mesh_N=CONF_initial_mesh_N)

    # define coefficient field
    coeff_types = ("EF-square-cos", "EF-square-sin", "monomials", "constant", "deterministic")
    from itertools import count
    if CONF_mu is not None:
        muparam = (CONF_mu, (0 for _ in count()))
    else:
        muparam = None

    # set lognormal flag
    lognormal = CONF_problem_type == 2
    rvtype = "normal" if lognormal else "uniform"

    coeff_field = SampleProblem.setupCF(coeff_types[CONF_coeff_type], decayexp=CONF_decay_exp, gamma=CONF_gamma,
                                    freqscale=CONF_freq_scale, freqskip=CONF_freq_skip, rvtype=rvtype,
                                    scale=CONF_coeff_scale, secondparam=muparam, coeff_mean=CONF_coeff_mean)

    # setup boundary conditions and pde
#    initial_mesh_N = CONF_initial_mesh_N
    pde, Dirichlet_boundary, uD, Neumann_boundary, g, f = SampleProblem.setupPDE(CONF_boundary_type, CONF_domain, CONF_problem_type, boundaries, coeff_field)
    ref_pde = None
    if lognormal:
        ref_pde, _, _, _, _, _ = SampleProblem.setupPDE(CONF_boundary_type, CONF_domain, 0, boundaries, coeff_field)

    # define multioperator
    # if CONF_problem_type <= 1:
    if not lognormal:
        # Poisson and Navier-Lame
        A = MultiOperator(coeff_field, pde.assemble_operator, pde.assemble_operator_inner_dofs)
        logger.info("Using standard operator.")
    # elif CONF_problem_type == 2:
    else:
        # convection diffusion (logtransformed Poisson)
        #        A = LogTransformed_MultiOperator(coeff_field, pde.assemble_operator, pde.assemble_operator_inner_dofs)
        A = LogTransformed_MultiOperator_plain(coeff_field, pde)
        logger.info("Using convection-diffusion operator.")

    
    # ============================================================
    # PART B: Import Solution
    # ============================================================
    import pickle
    PATH_SOLUTION = os.path.join(opts.basedir, CONF_experiment_name)
    FILE_SOLUTION = 'SFEM2-SOLUTIONS-P{0}.pkl'.format(CONF_FEM_degree)
    FILE_STATS = 'SIM2-STATS-P{0}.pkl'.format(CONF_FEM_degree)

    print "LOADING solutions from %s" % os.path.join(PATH_SOLUTION, FILE_SOLUTION)
    logger.info("LOADING solutions from %s" % os.path.join(PATH_SOLUTION, FILE_SOLUTION))
    # load solutions
    with open(os.path.join(PATH_SOLUTION, FILE_SOLUTION), 'rb') as fin:
        w_history = pickle.load(fin)
    # load simulation data
    logger.info("LOADING statistics from %s" % os.path.join(PATH_SOLUTION, FILE_STATS))
    with open(os.path.join(PATH_SOLUTION, FILE_STATS), 'rb') as fin:
        sim_stats = pickle.load(fin)

    logger.info("active indices of w after initialisation: %s", w_history[-1].active_indices())

    
    # ============================================================
    # PART C: MC Error Sampling
    # ============================================================

    # determine reference setting
    ref_mesh, ref_Lambda, ref_fem_degree = generate_reference_setup(PATH_SOLUTION)
    if CONF_ref_fem_degree > 0:
        logger.info("overriding ref_fem_degree %i -> %i", ref_fem_degree, CONF_ref_fem_degree)
        ref_fem_degree = CONF_ref_fem_degree

    MC_N = CONF_N
    MC_HMAX = CONF_maxh
    if CONF_runs > 0:
        # determine reference mesh
        w = w_history[-1]
        # ref_mesh = w.basis.basis.mesh
        for _ in range(CONF_ref_mesh_refine):
            ref_mesh = refine(ref_mesh)
        assert CONF_sampling_order <= 0 or CONF_sampling_order >= max(len(mu) for mu in ref_Lambda)
        ref_maxm = CONF_sampling_order if CONF_sampling_order > 0 else max(len(mu) for mu in ref_Lambda) + CONF_sampling_order_increase
        stored_rv_samples = []
        for i, w in enumerate(w_history):
#            if i == 0:
#                continue

            # memory usage info
            import resource
            logger.info("\n======================================\nMEMORY USED: " + str(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss) + "\n======================================\n")
            logger.info("================>>> MC error sampling for w[%i] (of %i) on %i cells with maxm %i <<<================" % (i, len(w_history), ref_mesh.num_cells(), ref_maxm))
            print("================>>> MC error sampling for w[%i] (of %i) on %i cells with maxm %i <<<================" % (i, len(w_history), ref_mesh.num_cells(), ref_maxm))

            MC_start = 0
            old_stats = sim_stats[i]
            if opts.continueMC:
                try:
                    MC_start = sim_stats[i]["MC-N"]
                    logger.info("CONTINUING MC of %s for solution (iteration) %s of %s", PATH_SOLUTION, i, len(w_history))
                except:
                    logger.info("STARTING MC of %s for solution (iteration) %s of %s", PATH_SOLUTION, i, len(w_history))
            if MC_start <= 0:
                    sim_stats[i]["MC-N"] = 0
                    sim_stats[i]["MC-ERROR-L2"] = 0
                    sim_stats[i]["MC-ERROR-H1A"] = 0
                    sim_stats[i]["MC-ERROR-L2_a0"] = 0
                    sim_stats[i]["MC-ERROR-H1A_a0"] = 0
            
            MC_RUNS = max(CONF_runs - MC_start, 0)
            if MC_RUNS > 0:
                logger.info("STARTING %s MC RUNS", MC_RUNS)
#                L2err, H1err, L2err_a0, H1err_a0, N = sample_error_mc(w, pde, A, coeff_field, mesh0, ref_maxm, MC_RUNS, MC_N, MC_HMAX)
                L2err, H1err, L2err_a0, H1err_a0, N = sample_error_mc(w, pde, A, coeff_field, ref_mesh, ref_maxm, MC_RUNS, MC_N, MC_HMAX, stored_rv_samples,
                                                                      CONF_quadrature_degree, fem_degree=ref_fem_degree, lognormal=lognormal, ref_pde=ref_pde)
                # combine current and previous results
                sim_stats[i]["MC-N"] = N + old_stats["MC-N"]
                sim_stats[i]["MC-ERROR-L2"] = (L2err * N + old_stats["MC-ERROR-L2"]) / sim_stats[i]["MC-N"]
                sim_stats[i]["MC-ERROR-H1A"] = (H1err * N + old_stats["MC-ERROR-H1A"]) / sim_stats[i]["MC-N"]
                sim_stats[i]["MC-ERROR-L2_a0"] = (L2err_a0 * N + old_stats["MC-ERROR-L2_a0"]) / sim_stats[i]["MC-N"]
                sim_stats[i]["MC-ERROR-H1A_a0"] = (H1err_a0 * N + old_stats["MC-ERROR-H1A_a0"]) / sim_stats[i]["MC-N"]
                print "MC-ERROR-H1A (N:%i) = %f" % (sim_stats[i]["MC-N"], sim_stats[i]["MC-ERROR-H1A"])
            else:
                logger.info("SKIPPING MC RUN since sufficiently many samples are available")
    
    # ============================================================
    # PART D: Export Updated Data
    # ============================================================
    # save updated data
    if opts.saveData:
        # save updated statistics
        print "SAVING statistics into %s" % os.path.join(PATH_SOLUTION, FILE_STATS)
        print sim_stats[-1].keys()
        logger.info("SAVING statistics into %s" % os.path.join(PATH_SOLUTION, FILE_STATS))
        with open(os.path.join(PATH_SOLUTION, FILE_STATS), 'wb') as fout:
            pickle.dump(sim_stats, fout)
