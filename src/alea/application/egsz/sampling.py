from __future__ import division
import os
import logging
import numpy as np
from alea.utils.timing import timing
from alea.application.egsz.multi_vector import MultiVector
from alea.stochastics.random_variable import UniformRV, NormalRV

from alea.application.tt_asgfem.tensorsolver.tt_param_pde import ttRightHalfdot
from alea.application.tt_asgfem.tensorsolver.tt_lognormal import sample_lognormal_tt

try:
    from dolfin import (Function, VectorFunctionSpace, FunctionSpace, refine, exp,
                        solve, project) #, plot, interactive, , errornorm, Expression, Constant
    from alea.fem.fenics.fenics_vector import FEniCSVector
    from alea.fem.fenics.fenics_basis import FEniCSBasis
except Exception, e:
    import traceback
    print traceback.format_exc()
    print "FEniCS has to be available"
    os.sys.exit(1)

# module logger
logger = logging.getLogger(__name__)

# create reference mesh and function space
def get_projection_basis(mesh0, mesh_refinements=None, maxh=None, degree=1, sub_spaces=None, family='CG'):
    if mesh_refinements is not None:
        mesh = mesh0
        for _ in range(mesh_refinements):
            mesh = refine(mesh)
        if sub_spaces is None or sub_spaces == 0:
            V = FunctionSpace(mesh, family, degree)
        else:
            V = VectorFunctionSpace(mesh, family, degree)
            assert V.num_sub_spaces() == sub_spaces
        return FEniCSBasis(V)
    else:
        assert maxh is not None
        if sub_spaces is None or sub_spaces == 0:
            V = FunctionSpace(mesh0, family, degree)
        else:
            V = VectorFunctionSpace(mesh0, family, degree)
            assert V.num_sub_spaces() == sub_spaces
        B = FEniCSBasis(V)
        return B.refine_maxh(maxh, True)[0]


def get_projected_solution(w, mu, proj_basis):
    # TODO: obfuscated method call since project is not obvious in the interface of MultiVector! This should be separated more clearly!
#    print "sampling.get_projected_solution"
#    print w[mu].num_sub_spaces
#    print proj_basis.num_sub_spaces
    with timing(msg="get_projected_solution (%s --- %i)" % (str(mu), w[mu].dim), logfunc=logger.debug):
        w_proj = w.project(w[mu], proj_basis)
    return w_proj


def prepare_w_projections(w, proj_basis):
    return {mu:get_projected_solution(w, mu, proj_basis) for mu in w.active_indices()}

def compute_parametric_sample_solution_TT(RV_samples, w, proj_basis, sigma, lognormal=False, no_proj=False):
    #with timing(msg="parametric_sample_sol_TT", logfunc=logger.info):
    assert not isinstance(w, MultiVector)

    # get polys
    if w.polysys == 'L':
        rv = UniformRV()
    elif w.polysys == 'H':
        rv = NormalRV()
    else:
        raise TypeError("unsupported type")
    M = len(w.r) - 2
    samples = [np.array(RV_sample[:M]) for RV_sample in RV_samples]
    # logger.debug("sample len %i" % len(samples))
    # logger.debug("Lambda len %i" % len(Lambda))

    # evaluate stochastic polynomials for sample
    poly = rv.orth_polys
    # print("samples= {}".format(samples))
    # print("gpcps={}".format(w.gpcps))
    # print("gpcps[0]={}".format(w.gpcps[0]))
    Pxs = [np.array(poly.eval(max(w.gpcps), sigma*sample, all_degrees=True)) for lia, sample in enumerate(samples)]

    # sum up realisation vector
    # produce linear combination of fem_basis corresponding to Px
    # construct FEM functions
    F = []
    for Px in Pxs:
        # print("Px={}".format(Px))
        # print(w)
        sample_sol = ttRightHalfdot(w, Px)
        # print M
        # if M % 2 == 0:
        #     sample_sol = sample_sol * (-1)
        if no_proj:
            F.append(sample_sol)
        else:
            f_sample_sol = Function(w.V)
            f_sample_sol.vector()[:] = sample_sol
            F.append(proj_basis.project_onto(FEniCSVector(f_sample_sol)))
    return F

def compute_parametric_sample_solution(RV_samples, coeff_field, w, proj_basis, cache=None):
    with timing(msg="parametric_sample_sol", logfunc=logger.info):
        Lambda = w.active_indices()
        sample_map, _ = coeff_field.sample_realization(Lambda, RV_samples)
        # sum up (stochastic) solution vector on reference function space wrt samples

        if cache is None:
            sample_sol = sum(get_projected_solution(w, mu, proj_basis) * sample_map[mu] for mu in Lambda)
        else:
            try:
                projected_sol = cache.projected_sol
            except AttributeError:
                projected_sol = {mu: get_projected_solution(w, mu, proj_basis) for mu in Lambda}
                cache.projected_sol = projected_sol
            sample_sol = sum(projected_sol[mu] * sample_map[mu] for mu in Lambda)
    return sample_sol


# noinspection PyProtectedMember
def compute_solution_variance(coeff_field, w, proj_basis):
    Lambda = w.active_indices()
    # sum up (stochastic) solution vector on reference function space wrt samples

    sample_sol = 0
    for mu in Lambda:
        if mu.order == 0:
            continue
        w_mu = get_projected_solution(w, mu, proj_basis)
        # noinspection PyProtectedMember
        f = w_mu._fefunc
        #print "1>" + str(list(f.vector().array()))
        f.vector()[:] = (f.vector().array()) ** 2
        #print "2>" + str(list(f.vector().array()))
        #f2 = project(f*f, w_mu.basis._fefs)
        #sample_sol += FEniCSVector(f2)
        sample_sol += FEniCSVector(f)
    return sample_sol


# noinspection PyProtectedMember,PyProtectedMember,PyProtectedMember,PyProtectedMember,PyProtectedMember
def compute_direct_sample_solution_lognormal(pde, RV_samples, coeff_field, maxm, proj_basis, piecewise_coeffs_data=None):
    with timing(msg="direct_sample_sol: construct coefficient", logfunc=logger.info):
        a_0 = coeff_field.mean_func
        if True:
            # NOTE: this variant creates a single large expression and so requires just one assembly with exp_a
            # should be much faster than the second variant below
            from dolfin import Expression
            def getcpp(coeff_exp):
                cpp = coeff_exp.cppcode
                return cpp.replace("A", str(coeff_exp.A)).replace("B", str(coeff_exp.B)).replace("freq", str(coeff_exp.freq)).replace("m", str(coeff_exp.m)).replace("n", str(coeff_exp.n))

            a_m = "+".join([a_0.cppcode.replace("A", str(a_0.A)).replace("B", str(a_0.B))]
                           + ["A"+str(m)+"*"+getcpp(coeff_field[m][0]) for m in range(maxm)])
            a_A_m = ",".join(["A%i=0" % m for m in range(maxm)])
            a_ex_m = "exp(" + a_m + ")"

            # print "=" * 80
            # print a_ex_m
            # print "=" * 80

            cell_str = ',' if maxm > 0 else ''
            cell_str += " cell='triangle', degree=5"
            eval_m = "Expression('%s', %s)" % (a_ex_m, a_A_m + cell_str)
            exp_a = eval(eval_m)
            just_a = eval("Expression('%s', %s)" % (a_m, a_A_m + cell_str))     # DEBUG
            for m in range(maxm):
                Am = "A%i" % m
                setattr(exp_a, Am, RV_samples[m])
                setattr(just_a, Am, RV_samples[m])                              # DEBUG
        else:
            # NOTE: this was replaced by the above code
            a = a_0
            for m in range(maxm):
                a += RV_samples[m] * coeff_field[m][0]
            exp_a = exp(a)

    if piecewise_coeffs_data is not None:
        # approximation by piecewise function
        if piecewise_coeffs_data['p'] == 0:
            c0 = Function(piecewise_coeffs_data['DG'])
            u0 = np.array([exp_a(p) for p in piecewise_coeffs_data['mp']])
            c0.vector()[:] = u0[piecewise_coeffs_data['c2d']]
            exp_a = c0
        else:
            exp_a = project(exp_a, piecewise_coeffs_data['DG'])

    f = pde.f

    with timing(msg="direct_sample_sol: assemble and apply BCs", logfunc=logger.info):
        b = pde.assemble_rhs(basis=proj_basis, coeff=exp_a, withDirichletBC=False)
        A = pde.assemble_lhs(basis=proj_basis, coeff=exp_a, withDirichletBC=False)
        # noinspection PyProtectedMember
        A, b = pde.apply_dirichlet_bc(proj_basis._fefs, A, b)

    with timing(msg="direct_sample_sol: solve linear system", logfunc=logger.info):
        X = 0 * b
        logger.info("compute_direct_sample_solution with %i dofs" % b.size())
        solve(A, X, b)

    # --- DEBUG
    if False:
        # compare standard poisson with lognormal coefficient with convection-diffusion formulation
        from dolfin import inner, nabla_grad, dx
        import dolfin as df
        # noinspection PyProtectedMember
        V = proj_basis._fefs
        #mesh = V.mesh()
        u, v = df.TrialFunction(V), df.TestFunction(V)
        bcs = pde.create_dirichlet_bcs(V, pde.uD, pde.dirichlet_boundary)
        a = (inner(nabla_grad(u), nabla_grad(v)) - inner(nabla_grad(just_a), nabla_grad(u)) * v) * dx
        L = exp(-just_a) * f * v * dx
        A0, b0 = df.assemble_system(a, L, bcs)
        x0 = 0 * b
        df.solve(A0, x0, b0)
        #x0f = Function(proj_basis._fefs, x0)
        # df.plot(just_a, title='just_a', mesh=mesh)
        # noinspection PyProtectedMember
        err = Function(proj_basis._fefs, x0-X)
        # noinspection PyProtectedMember
        print "XXXXX error", df.norm(err, 'L2'), df.norm(err, 'L2')/df.norm(Function(proj_basis._fefs, X), 'L2')
        # df.plot(err, title="error", interactive=True)
        # df.plot(x0f, title='x0', interactive=True)
        X = x0
    # --- DEBUG

    # noinspection PyProtectedMember
    return FEniCSVector(Function(proj_basis._fefs, X))


# noinspection PyProtectedMember,PyProtectedMember
def compute_direct_sample_solution(pde, RV_samples, coeff_field, A, maxm, proj_basis, cache=None, low_memory=True,
                                   lognormal=False, piecewise_coeffs_data=None):
    # branch for the lognormal case
    if lognormal:
        return compute_direct_sample_solution_lognormal(pde, RV_samples, coeff_field, maxm, proj_basis,
                                                        piecewise_coeffs_data=piecewise_coeffs_data)

    # the following is for the affine case
    assert piecewise_coeffs_data == None
    try:
        A0 = cache.A
        A_m = cache.A_m
        b = cache.b
        logger.debug("compute_direct_sample_solution: CACHE USED")
        print "CACHE USED"
    except AttributeError:
        with timing(msg="direct_sample_sol: compute A_0, b", logfunc=logger.info):
            a = coeff_field.mean_func
            A0 = pde.assemble_lhs(basis=proj_basis, coeff=a, withDirichletBC=False)
            b = pde.assemble_rhs(basis=proj_basis, coeff=a, withDirichletBC=False)
            A_m = [None] * maxm
            logger.debug("compute_direct_sample_solution: CACHE NOT USED")
            print "CACHE NOT USED"
        if cache is not None:
            cache.A = A0
            cache.A_m = A_m
            cache.b = b

    with timing(msg="direct_sample_sol: compute A_m", logfunc=logger.info):
        A = A0.copy()
        if low_memory and maxm > 0:
            # NOTE: this variant creates a single large expression and so requires just one assembly of A_m
            def getcpp(coeff_exp):
                cpp = coeff_exp.cppcode
                return cpp.replace("A", str(coeff_exp.A)).replace("B", str(coeff_exp.B)).replace("freq", str(coeff_exp.freq)).replace("m", str(coeff_exp.m)).replace("n", str(coeff_exp.n))
            a_ex_m = "+".join(["A"+str(m)+"*"+getcpp(coeff_field[m][0]) for m in range(maxm)])
            a_A_m = ",".join(["A%i=0" % m for m in range(maxm)])
            eval_m = "Expression('%s', %s)" % (a_ex_m, a_A_m)
            a_m = eval(eval_m)
            for m in range(maxm):
                Am = "A%i" % m
                setattr(a_m, Am, RV_samples[m])
            A += pde.assemble_lhs(basis=proj_basis, coeff=a_m, withDirichletBC=False)
        else:
            for m in range(maxm):
                if A_m[m] is None:
                    a_m = coeff_field[m][0]
                    A_m[m] = pde.assemble_lhs(basis=proj_basis, coeff=a_m, withDirichletBC=False)
                A += RV_samples[m] * A_m[m]

    with timing(msg="direct_sample_sol: apply BCs", logfunc=logger.info):
        # noinspection PyProtectedMember
        A, b = pde.apply_dirichlet_bc(proj_basis._fefs, A, b)

    with timing(msg="direct_sample_sol: solve linear system", logfunc=logger.info):
        X = 0 * b
        logger.info("compute_direct_sample_solution with %i dofs" % b.size())
        solve(A, X, b)
    # noinspection PyProtectedMember
    return FEniCSVector(Function(proj_basis._fefs, X))


# noinspection PyProtectedMember,PyProtectedMember,PyProtectedMember,PyProtectedMember
def compute_solution_flux(pde, RV_samples, coeff_field, u, maxm, proj_basis, vec_proj_basis):
    a = coeff_field.mean_func
    for m in range(maxm):
        a_m = RV_samples[m] * coeff_field[m][0]
        a = a + a_m
    print type(a)
    # noinspection PyProtectedMember
    uN = u._fefunc
    # noinspection PyProtectedMember,PyProtectedMember
    uN = project(u._fefunc, proj_basis._fefs)
    s = pde.weak_form.flux(uN, a)
    # noinspection PyProtectedMember
    return FEniCSVector(project(s, vec_proj_basis._fefs))


# def compute_direct_sample_solution_old(pde, RV_samples, coeff_field, A, maxm, proj_basis):
#     a = coeff_field.mean_func
#     for m in range(maxm):
#         a_m = RV_samples[m] * coeff_field[m][0]
#         a = a + a_m
#
#     A = pde.assemble_lhs(basis=proj_basis, coeff=a)
#     b = pde.assemble_rhs(basis=proj_basis, coeff=a)
#     X = 0 * b
#     solve(A, X, b)
#     return FEniCSVector(Function(proj_basis._fefs, X)), a


# noinspection PyProtectedMember
def get_coeff_realisation(RV_samples, coeff_field, maxm=None, proj_basis=None):
    if maxm is None:
        maxm = len(RV_samples)
    a = coeff_field.mean_func
    for m in range(maxm):
        a_m = RV_samples[m] * coeff_field[m][0]
        a = a + a_m
    # noinspection PyProtectedMember
    return a if proj_basis is None else FEniCSVector(project(a, proj_basis._fefs))
