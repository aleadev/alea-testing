"""EG a posteriori residual estimator (FEniCS centric implementation)"""

from __future__ import division
import numpy as np
from dolfin import (assemble, dot, nabla_grad, div, dx, avg, jump, dS, sqrt, cells,
                    FunctionSpace, TestFunction, CellSize, FacetNormal, parameters, inner, interpolate, exp)

from alea.fem.fenics.fenics_vector import FEniCSVector
from alea.application.egsz.coefficient_field import CoefficientField
from alea.application.egsz.multi_vector import MultiVector, MultiVectorSharedBasis, supp
from alea.application.egsz.solver_utils import prepare_tildef
#from alea.fem.fenics.fenics_utils import weighted_H1_norm
from alea.linalg.vector import FlatVector
from alea.math_utils.multiindex import Multiindex
from alea.utils.decorators import cache

from collections import defaultdict

import logging
logger = logging.getLogger(__name__)

def element_degree(u):
    """Returns degree of ufl_element of u"""
    return u.function_space().ufl_element().degree()


class ResidualEstimator_plain(object):
    """Evaluation of the residual error estimator which consists of volume/edge terms and the upper tail bound."""

    @classmethod
    def evaluateResidualEstimator(cls, w, coeff_field, pde, f, quadrature_degree=-1, f_maxm=30):
        """Evaluate residual estimator for all active mu of w."""
        assert pde.typestr == "convectiondiffusion"

        # evaluate residual estimator for all multiindices
        eta_local = MultiVector()
        eta = {}
        M = np.max(supp(w.active_indices()))
        if len(M) == 0:
            M = 1
        else:
            M = tuple(M)[0] + 1
        M += f_maxm
        fv = prepare_tildef(lambda m: coeff_field[m][0], pde.f, w.active_indices(), a_maxm=M, normalised=True)
        # scale coefficients with exp(-mean)
        scaled_fv = {mu: exp(-coeff_field.mean_func) * fvi for mu, fvi in fv.iteritems()}
        for mu in w.active_indices():
            eta[mu], eta_local[mu] = cls._evaluateResidualEstimator(mu, w, coeff_field, pde, scaled_fv[mu], quadrature_degree, f_maxm)
        global_eta = sqrt(sum([v ** 2 for v in eta.values()]))
        return global_eta, eta, eta_local


    @classmethod
    def _evaluateResidualEstimator(cls, mu, w, coeff_field, pde, hatf, quadrature_degree, f_maxm):
        logger.debug("residual quadrature order = " + str(quadrature_degree))

        # prepare some FEM variables
        V = w[mu]._fefunc.function_space()
        mesh = V.mesh()
        nu = FacetNormal(mesh)

        # TODO: depending on used norm, weighting by 1/a0 may be necessary
        # get mean field of coefficient and discretise
        # a0_f = coeff_field.mean_func
        # a0_f_h = interpolate(a0_f, V)

        # initialise volume and edge residual with deterministic part
        R_T = hatf + div(nabla_grad(w[mu]._fefunc))
        R_E = nabla_grad(w[mu]._fefunc)

        # iterate m
        Lambda = w.active_indices()
        maxm = w.max_order
        if len(coeff_field) < maxm:
            logger.warning("insufficient length of coefficient field for MultiVector (%i < %i)", len(coeff_field),
                           maxm)
            maxm = len(coeff_field)
            #        assert coeff_field.length >= maxm        # ensure coeff_field expansion is sufficiently long

        for m in range(maxm):
            # discretise coefficient
            am_f, am_rv = coeff_field[m]
            am_f_h = interpolate(am_f, V)

            # prepare polynom coefficients
            beta = am_rv.orth_polys.get_beta(mu[m])

            # mu
            r_T = -beta[0] * w[mu]

            # mu+1
            mu1 = mu.inc(m)
            if mu1 in Lambda:
                w_mu1 = w[mu1]
                r_T += beta[1] * w_mu1

            # mu-1
            mu2 = mu.dec(m)
            if mu2 in Lambda:
                w_mu2 = w[mu2]
                r_T += beta[-1] * w_mu2

            # add volume contribution for m
            r_T = inner(nabla_grad(am_f_h), nabla_grad(r_T._fefunc))
            R_T = R_T + r_T

        # prepare more FEM variables for residual assembly
        DG0 = FunctionSpace(mesh, "DG", 0)
        dg0 = TestFunction(DG0)
        h = CellSize(mesh)

        # scaling of residual terms and definition of residual form
        res_form = (h ** 2 * inner(R_T, R_T) * dg0 * dx
                    + avg(h) * jump(R_E, nu) ** 2 * 2 * avg(dg0) * dS)

        # FEM evaluate residual estimator on mesh
        eta = assemble(res_form, form_compiler_parameters={'quadrature_degree': quadrature_degree})
        eta_indicator = np.array([sqrt(e) for e in eta])
        # map DG dofs to cell indices
        dofs = [DG0.dofmap().cell_dofs(c.index())[0] for c in cells(mesh)]
        eta_indicator = eta_indicator[dofs]
        global_error = sqrt(sum(e for e in eta))

        # debug ---
        print "==========CD RESIDUAL ESTIMATOR============"
        #        print "eta", eta.array()
        #        print "eta_indicator", eta_indicator
        print "global =", global_error
        # ---debug

        return global_error, FlatVector(eta_indicator)


    @classmethod
    def evaluateUpperTailBound(cls, w, coeff_field, pde, maxh=1/10, add_maxm=10, f_maxm=30):
        """Estimate upper tail bounds."""
        
        @cache
        def get_ainfty(m, V):
            a0_f = coeff_field.mean_func
            if isinstance(a0_f, tuple):
                a0_f = a0_f[0]
            # determine min \overline{a} on D (approximately)
            f = FEniCSVector.from_basis(V, sub_spaces=0)
            f.interpolate(a0_f)
            min_a0 = f.min_val
            am_f, _ = coeff_field[m]
            if isinstance(am_f, tuple):
                am_f = am_f[0]

            # determine ||a_m/\overline{a}||_{L\infty(D)} (approximately)
            try:
                # use exact bounds if defined
                max_am = am_f.max_val
            except:
                # otherwise interpolate
                f.interpolate(am_f)
                max_am = f.max_val
            # TODO: adjust weights to used norm!
            ainftym = max_am / np.max([min_a0, 1.0])
            # print ">>>>>>>>>>", max_am, min_a0, ainftym
            assert isinstance(ainftym, float) # and ainftym > 0
            return ainftym
        
        def prepare_norm_w(energynorm, w):
            normw = {}
            for mu in w.active_indices():
                normw[mu] = energynorm(w[mu]._fefunc)        
            return normw
        
        def LambdaBoundary(Lambda):
            suppLambda = supp(Lambda)

            for mu in Lambda:
                for m in suppLambda:
                    mu1 = mu.inc(m)
                    if mu1 not in Lambda:
                        yield mu1

                    mu2 = mu.dec(m)
                    if mu2 not in Lambda and mu2 is not None:
                        yield mu2

        # evaluate (3.15)
        def eval_zeta_bar(mu, suppLambda, coeff_field, normw, V, M):
            assert mu in normw.keys()
            zz = 0
#            print "====zeta bar Z1", mu, M
            for m in range(M):
                if m in suppLambda:
                    continue
                _, am_rv = coeff_field[m]
                beta = am_rv.orth_polys.get_beta(mu[m])
                ainfty = get_ainfty(m, V)
                zz += (beta[1] * ainfty) ** 2
            return normw[mu] * sqrt(zz)
        
        # evaluate (3.11)
        def eval_zeta(mu, Lambda, coeff_field, normw, V, M=None, this_m=None):
            z = 0
            if this_m is None:
                for m in range(M):
                    _, am_rv = coeff_field[m]
                    beta = am_rv.orth_polys.get_beta(mu[m])
                    ainfty = get_ainfty(m, V)
                    mu1 = mu.inc(m)
                    if mu1 in Lambda:
                        # print "====beta", [beta[i-1] for i in range(3)]
                        # print "====zeta Z1", ainfty, beta[1], normw[mu1], " == ", ainfty * beta[1] * normw[mu1]
                        z += ainfty * beta[1] * normw[mu1]
                    mu2 = mu.dec(m)
                    if mu2 in Lambda:
                        # print "====beta", [beta[i-1] for i in range(3)]
                        # print "====zeta Z2", ainfty, beta[-1], normw[mu2], " == ", ainfty * beta[-1] * normw[mu2]
                        z += ainfty * beta[-1] * normw[mu2]
                return z
            else:
                    m = this_m
                    _, am_rv = coeff_field[m]
                    beta = am_rv.orth_polys.get_beta(mu[m])
                    ainfty = get_ainfty(m, V)
                    # print "====beta", [beta[i-1] for i in range(3)]
                    # print "====zeta Z3", m, ainfty, beta[1], normw[mu], " == ", ainfty * beta[1] * normw[mu]
                    return ainfty * beta[1] * normw[mu]

        # prepare some variables
        energynorm = pde.energy_norm
        Lambda = w.active_indices()
        suppLambda = supp(w.active_indices())
        M = min(w.max_order + add_maxm, len(coeff_field))
        normw = prepare_norm_w(energynorm, w)
        # retrieve (sufficiently fine) function space for maximum norm evaluation
        V = w[Multiindex()].basis.refine_maxh(maxh)[0]
        # evaluate estimator contributions of (3.16)
        # === (a) zeta ===
        zeta = defaultdict(int)
        # iterate multiindex extensions
#        print "===A1 Lambda", Lambda
        for nu in LambdaBoundary(Lambda):
            assert nu not in Lambda
#            print "===A2 boundary nu", nu
            zeta[nu] += eval_zeta(nu, Lambda, coeff_field, normw, V, M)
        # === (b) zeta_bar ===
        zeta_bar = {}
        # iterate over active indices
        for mu in Lambda:
            zeta_bar[mu] = eval_zeta_bar(mu, suppLambda, coeff_field, normw, V, M)

        # evaluate summed estimator (3.16)
        global_zeta = sqrt(sum([v ** 2 for v in zeta.values()]) + sum([v ** 2 for v in zeta_bar.values()]))
        # also return zeta evaluation for single m (needed for refinement algorithm)
        eval_zeta_m = lambda mu, m: eval_zeta(mu=mu, Lambda=Lambda, coeff_field=coeff_field, normw=normw, V=V, M=M, this_m=m)
        logger.debug("=== ZETA  %s --- %s --- %s", global_zeta, zeta, zeta_bar)
        return global_zeta, zeta, zeta_bar, eval_zeta_m
