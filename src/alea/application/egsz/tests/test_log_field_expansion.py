# test expansion of exp(-a(x,y))

from __future__ import division
import numpy as np
import scipy as sp
import argparse
import matplotlib.pyplot as plt

# alea imports
from alea.application.egsz.sample_problems2 import SampleProblem
from alea.application.egsz.sample_domains import SampleDomain
from alea.math_utils.multiindex_set import MultiindexSet
from alea.polyquad.polynomials import StochasticHermitePolynomials
from alea.application.egsz.sampling import get_coeff_realisation
from alea.fem.fenics.fenics_basis import FEniCSBasis
# from alea.utils.plothelper import PlotHelper

from scipy.integrate import quad
from scipy.special import eval_hermite, eval_hermitenorm

from dolfin import plot, refine, Function, FunctionSpace, Expression, set_log_level, WARNING, INFO, ERROR, project
import logging

# set FEniCS logging
set_log_level(logging.WARNING)
fenics_logger = logging.getLogger("FFC")
fenics_logger.setLevel(logging.WARNING)
fenics_logger = logging.getLogger("UFL")
fenics_logger.setLevel(logging.WARNING)

parser = argparse.ArgumentParser()
parser.add_argument("-ct", "--coeff-type", type=str, default="cos",
                    help="coefficient type")
parser.add_argument("-at", "--amp-type", type=str, default="decay-inf",
                    help="amplification type")
parser.add_argument("-N", type=int, default=30,
                    help="number of terms for amp-type constant")
parser.add_argument("-M", type=int, default=2,
                    help="terms in realisation")
parser.add_argument("-de", "--decay-exp", type=float, default=2.0,
                    help="decay exponent")
parser.add_argument("-g", "--gamma", type=float, default=0.9,
                    help="gamma")
parser.add_argument("-fk", "--freq-skip", type=int, default=0,
                    help="frequency skip")
parser.add_argument("-fs", "--freq-scale", type=float, default=1.0,
                    help="frequency scaling")
parser.add_argument("-s", "--scale", type=float, default=1.0,
                    help="scaling")
parser.add_argument("-rv", "--rv-type", type=str, default="normal",
                    help="RV type")
parser.add_argument("-pt", "--problem-type", type=int, default=2,
                    help="problem type")
parser.add_argument("-d", "--domain", type=str, default="square",
                    help="domain")
parser.add_argument("-ref", "--mesh-refinements", type=int, default=0,
                    help="initial mesh refinements")
args = parser.parse_args()
print "========== PARAMETERS:", vars(args)


# -------------------------------------
# A setup field a(x,y) and polynomials H
# -------------------------------------

# setup boundary conditions and pde
mesh, boundaries, dim = SampleDomain.setupDomain(args.domain, initial_mesh_N=50)
mesh = SampleProblem.setupMesh(mesh, num_refine=args.mesh_refinements)

# define coefficient field
coeff_field = SampleProblem.setupCF2(args.coeff_type, amptype=args.amp_type, decayexp=args.decay_exp, gamma=args.gamma, N=args.N,
                                     freqscale=args.freq_scale, freqskip=args.freq_skip, rvtype=args.rv_type, scale=args.scale)

# --- probabilist ---
h = StochasticHermitePolynomials(normalised=False)
# ...normalised probabilist
nh = StochasticHermitePolynomials(normalised=True)

# -------------------------------------
# B expand exp(-a(x,y))
# -------------------------------------

def exp_a(x, y, coeff_field, Lambda, H):
    def Hmu(y, mu):
        return np.prod([H(mum, ym) for mum, ym in zip(mu, y)])
    def amu(x, mu):
        return np.prod([a[m](x)**mum for m, mum in enumerate(mu)])
    a = [coeff_field[m][0] for m in range(len(y))]
    s = np.exp(0.5 * np.sum([am(x)**2 for am in a]))
    return s * np.sum([(-1)**np.sum(mu) * amu(x, mu) * Hmu(y, mu) / np.prod([sp.math.factorial(mum) for mum in mu]) for mu in Lambda])


# -------------------------------------
# C sample error
# -------------------------------------

def evaluate_coeff_realisation(sample, coeff_field, x=None):
    def getcpp(coeff_exp):
        cpp = coeff_exp.cppcode
        try:
            return cpp.replace("A", str(coeff_exp.A)).replace("B", str(coeff_exp.B)).replace("freq", str(coeff_exp.freq)).replace("m", str(coeff_exp.m)).replace("n", str(coeff_exp.n))
        except:
            return cpp.replace("B", str(coeff_exp.B))
    M = len(sample)
    a_mean = coeff_field.mean_func
    # a_ex_m = "+".join([getcpp(a_mean)] + ["A"+str(m)+"*"+getcpp(coeff_field[m][0]) for m in range(M)])
    a_ex_m = "+".join(["A"+str(m)+"*"+getcpp(coeff_field[m][0]) for m in range(M)])
    a_A_m = ",".join(["A%i=0" % m for m in range(M)])
    eval_m = "Expression('%s', %s)" % (a_ex_m, a_A_m + ", cell='triangle'")
    a_m = eval(eval_m)
    for m in range(M):
        Am = "A%i" % m
        setattr(a_m, Am, sample[m])
    return a_m if x is None else a_m(x)

M, P = args.M, 10
Lambda_complete = MultiindexSet.createCompleteOrderSet(M, P)
# print Lambda_complete
Lambda_full = MultiindexSet.createFullTensorSet(M, P)
# print Lambda_full
Lambda = [Lambda_complete, Lambda_full][1]
# print Lambda

nr_samples = 10
sample_rvs = coeff_field.sample_rvs()
RV_samples = [[sample_rvs[j + n*M] for j in range(M)] for n in range(nr_samples)]

# sample_map = {}
# for mu in Lambda:
#     univariate_vals = list(self[m][1].orth_polys[mu[m]](RV_samples[m]) for m in range(len(mu)))
#     sample_map[mu] = prod(univariate_vals)

V = FunctionSpace(mesh, 'CG', 1)
x0 = (0.1, 0.4)
print [exp_a(x0, sample, coeff_field, Lambda, lambda n, y: h.eval(n, y)) for sample in RV_samples]
print [np.exp(-evaluate_coeff_realisation(sample, coeff_field, x=x0)) for sample in RV_samples]
# print [get_coeff_realisation(sample, coeff_field, proj_basis=FEniCSBasis(V))._fefunc(0,0) for sample in RV_samples]
