from dolfin import mesh, set_log_active, set_log_level
from alea.fem.fenics.fenics_utils import create_joint_mesh

import pickle
import os
import glob
import logging


# noinspection PyProtectedMember
def generate_reference_setup(basedir, SOLUTIONFN='SFEM2-SOLUTIONS-P?.pkl'):
    # ============================================================
    # import solutions and determine reference setting
    # ============================================================
    for i, fn in enumerate(glob.glob(os.path.join(basedir, SOLUTIONFN))):
        print "--------> loading data from", fn
        with open(fn, 'rb') as fin:
            w_history = pickle.load(fin)
            V = w_history[-1].basis.basis
            # noinspection PyProtectedMember
            degree = V._fefs.ufl_element().degree()
            mesh0 = V.mesh
            Lambda0 = w_history[-1].active_indices()
            print "\tmesh has %i dofs in P%i, %i cells after %i iterations with Lambda %s" % (V.dim, degree, mesh0.num_cells(), len(w_history), Lambda0)
            if i == 0:
                mesh, Lambda, ref_fem_degree = mesh0, set(Lambda0), degree
            else:
                # mesh, _ = create_joint_mesh([mesh0, mesh])
                if mesh.num_cells() < mesh0.num_cells():
                    mesh = mesh0
                if ref_fem_degree < degree:
                    ref_fem_degree = degree
                Lambda = Lambda.union(set(Lambda0))
    print "=== FINAL mesh has %i cells and len(Lambda) is %i ===" % (mesh.num_cells(), len(Lambda))
    return mesh, Lambda, ref_fem_degree

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

if __name__ == "__main__":
    # FEniCS logging
    set_log_active(True)
    set_log_level(logging.WARNING)
    fenics_logger = logging.getLogger("FFC")
    fenics_logger.setLevel(logging.WARNING)
    fenics_logger = logging.getLogger("UFL")
    fenics_logger.setLevel(logging.WARNING)

    generate_reference_setup('.')
