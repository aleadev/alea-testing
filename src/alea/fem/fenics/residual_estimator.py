from __future__ import division
import numpy as np
import scipy.sparse as sps
from dolfin import (nabla_grad, nabla_div, dot, inner, FacetNormal, TestFunction, TrialFunction, CellSize, assemble, Function, FunctionSpace,
                        dx, dS, avg, jump, project, interpolate, parameters, cells, Expression, Constant)

# def evaluate_residual_estimator(V, u_h, kappa0, kappa, f, assemble_quad_degree):
#     # setup assembly
#     mesh = V.mesh()
#     nu = FacetNormal(mesh)
#     h = CellSize(mesh)
#     DG0 = FunctionSpace(mesh, 'DG', 0)
#     DG0_dofs = dict([(c.index(),DG0.dofmap().cell_dofs(c.index())[0]) for c in cells(mesh)])
#     dg0 = TestFunction(DG0)
#
#     # discretise kappa
#     V2 = FunctionSpace(V.mesh(), V.ufl_element().family(), V.ufl_element().degree())
#     kappa_h = [project(k, V2) for k in kappa]
#
#     # define forms and assemble
#     # R_T = f + div(kappa_h[0] * grad(u_h[0]))
#     R_T = (f + sum([div(k * grad(u)) for k, u in zip(kappa_h, u_h)]))
#     # R_dT = dot(kappa_h[0] * grad(u_h[0]), nu)
#     R_dT = sum([dot(k * grad(u), nu) for k, u in zip(kappa, u_h)])
#     indicator = h ** 2 * (1 / kappa0) * R_T ** 2 * dg0 * dx + avg(h) * avg(1 / kappa0) * jump(R_dT) ** 2 * 2 * avg(dg0) * dS
#
#     # prepare indicators
#     eta_res_local = assemble(indicator, form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
#
#     if not True:
#         from dolfin import Function, plot
#         F = Function(DG0)
#         F.vector()[:] = eta_res_local
#         plot(kappa0, title="kappa0", mesh=mesh)
#         plot(kappa_h[0], title="kappa")
#         plot(u_h[0], title="u")
#         plot(F, title="error indicator", interactive=True)
#
#     # print min(eta_res_local.array()), max(eta_res_local.array())
#     # assert min(eta_res_local.array()) > 0
#     eta_res_local = np.sqrt(eta_res_local.array())[DG0_dofs.values()]
#     eta_res_global = np.sqrt(np.sum(i ** 2 for i in eta_res_local))
#
#     return eta_res_local, eta_res_global


def evaluate_residual_main_component(V, a_m, w0, f, assemble_quad_degree=-1, with_volume=True):
    # setup assembly
    mesh = V.mesh()
    h = CellSize(mesh)

    # setup localisation
    DG0 = FunctionSpace(mesh, 'DG', 0)
    DG0_dofs = dict([(c.index(), DG0.dofmap().cell_dofs(c.index())[0]) for c in cells(mesh)])
    dg0 = TestFunction(DG0)

    # calculate sigma0
    M = len(w0)
    ww = Function(V)
    # ww.vector()[:] = w0[0]
    ww.vector().set_local(w0[0])
    a0 = a_m[0]
    aa = nabla_div(a0 * nabla_grad(ww))
    for i in range(1, M):
        ww = Function(V)
        # ww.vector()[:] = w0[i]
        ww.vector().set_local(w0[i])
        aa += nabla_div(a_m[i] * nabla_grad(ww))

    # setup residual estimator forms
    R_T = f ** 2 + 2 * f * aa
    if with_volume:
        res = h ** 2 * (1 / a0) * R_T * dg0 * dx
    else:
        res = Expression("0") * dg0 * dx

    # assemble and reorder indicator
    eta_res_local2 = assemble(res, form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
    eta_res_local2 = eta_res_local2.array()[DG0_dofs.values()]

    return eta_res_local2


def evaluate_residual_neighbour_components(V, a0, w_m1, w_m2, a_m1, a_m2, assemble_quad_degree=-1, with_volume=True, with_edge=True):
    assert with_volume or with_edge

    # setup assembly
    mesh = V.mesh()
    nu = FacetNormal(mesh)
    h = CellSize(mesh)

    # setup localisation
    DG0 = FunctionSpace(mesh, 'DG', 0)
    DG0_dofs = dict([(c.index(),DG0.dofmap().cell_dofs(c.index())[0]) for c in cells(mesh)])
    dg0 = TestFunction(DG0)

    # setup residual estimator forms
    if with_volume:
        R_T = inner(nabla_div(a_m1 * nabla_grad(w_m1)), nabla_div(a_m2 * nabla_grad(w_m2)))
        res = h ** 2 * (1 / a0) * R_T * dg0 * dx
    else:
        res = Expression("0") * dg0 * dx
    if with_edge:
        R_dT = inner(jump(a_m1 * nabla_grad(w_m1), nu), jump(a_m2 * nabla_grad(w_m2), nu))
        res += avg(h) * avg(1 / a0) * R_dT * 2 * avg(dg0) * dS

    # assemble and reorder indicator
    eta_res_local2 = assemble(res, form_compiler_parameters={'quadrature_degree': assemble_quad_degree})
    eta_res_local2 = eta_res_local2.array()[DG0_dofs.values()]

    return eta_res_local2
