from extended_tt import ExtendedTT, BasisType
from extended_fem_tt import ExtendedFEMTT
from dolfin import FunctionSpace, project, Function, interpolate
import xerus as xe
import numpy as np


class MLExtendedFEMTT(object):
    def __init__(self,
                 cores                              # type: list
                 ):
        """
        Init for an Multi Level extended FEM TT having a list of extended FEM TT
        :param cores: list of extended FEM TT
        """

        assert isinstance(cores, list)
        assert len(cores) > 0
        self.L = len(cores)
        for lia in range(self.L):
            assert isinstance(cores[lia], ExtendedFEMTT)
        self.cores = cores

    # region def sample
    def sample(self, x):
        """
        defines the call operator by evaluating using every basis element
        :param x: point to evaluate the corresponding basis at
        :return: fem representation of the corresponding sample
        """

        def inner_project(values, old_fs, new_fs):
            fun = Function(old_fs)
            fun.vector()[:] = values
            fun = interpolate(fun, new_fs)
            return fun.vector()[:]

        retval = self.cores[0].sample(x)
        for lia in range(1, self.L):
            retval = self.cores[lia].sample(x) + inner_project(retval, self.cores[lia-1].first_comp_fs,
                                                               self.cores[lia].first_comp_fs)
        return retval
    # endregion

    # region def overwrite normalise
    def normalise(self):
        for lia in range(self.L):
            self.cores[lia].normalize()
    # endregion

    # region def from xerus reconstruction
    @staticmethod
    def from_reconstruction(list_fs,                # type: list
                            fun,                    # type: function
                            list_samples,           # type: list
                            list_dimensions,        # type: list
                            poly_basis="L",         # type: str
                            target_eps=1e-8,        # type: float
                            maxitr=10000,           # type: int
                            progress=False,         # type: bool
                            only_var_estimation=False
                            ):
        """
        Creates the individual level of update tensors by iterative application of the reconstruction scheme
        from the xerus library.
        :param list_fs: list of (dolfin) function spaces
        :param fun: function to evaluate the sample at fun(y, fs)
        :param list_samples: list of list of samples $([y_j^l]_{j=1}^{N_l})_{l=0}^L$
        :param list_dimensions: list of list of dimensions to use in reconstruction for every level
        :param poly_basis: Used polynomial basis in xerus. Either Legendre or Hermite
        :param target_eps: tolerance to reach in the rank adaptive scheme of eigenvalue threshold
        :param maxitr: maximal number of iterations used in the rank adaptive adf (aka. reconstruction algorithm)
        :return: MLExtendedFEMTT
        """
        assert isinstance(list_fs, list)            # check input: list of function spaces
        for lia in range(len(list_fs)):
            assert isinstance(list_fs[lia], FunctionSpace)

        assert isinstance(list_samples, list)       # check input: list of list of samples
        for lia in range(len(list_samples)):
            assert isinstance(list_samples[lia], list)

        assert isinstance(list_dimensions, list)    # check input: list of list of dimensions
        for lia in range(len(list_dimensions)):
            for dim in list_dimensions[lia]:
                assert dim > 1

        retval = []
        for level in range(len(list_fs)):           # for every level
            if progress:
                print("Start computation on level: {}".format(level))
            fs = list_fs[level]                     # get the current function space
            fs_dim = fs.dim()                       # its physical space dimensionality
            dimension = list_dimensions[level]      # and number of "stochastic" dimensions
            #                                       # define polynomial flag
            if poly_basis == "L":
                xebasis = xe.PolynomBasis.Legendre
                exbasis = [BasisType.points] + [BasisType.Legendre] * (len(dimension))
            else:
                xebasis = xe.PolynomBasis.Hermite
                exbasis = [BasisType.points] + [BasisType.Hermite] * (len(dimension))

            measurements = xe.UQMeasurementSet()    # create Xerus measurement Set

            samples = list_samples[level]
            mean_estimator = 0
            var_estimator = 0
            value_list = []
            for lib, sample in enumerate(samples):
                if progress and lib % 10 == 0:
                    print("compute sample: {}/{}".format(lib, len(samples)))
                if level == 0:                      # in the first level take the value itself
                    value_list.append(list(fun(sample, fs).vector()[:]))
                else:                               # remaining level: take the projected difference as value
                    value_list.append(list(fun(sample, fs).vector()[:] -
                                           project(fun(sample, list_fs[level-1]), fs).vector()[:]))
                mean_estimator += np.array(value_list[-1])

            mean_estimator *= len(samples)**(-1)

            for lib, sample in enumerate(samples):  # for every sample
                if progress and lib % 10 == 0:
                    print("process sample: {}/{}".format(lib, len(samples)))
                value = value_list[lib]
                var_estimator += (np.array(value) - np.array(mean_estimator))**2
                #                                   # create a one dimensional xerus tensor
                value_ten = xe.Tensor(dim=[fs_dim])
                for lia in range(fs_dim):           # and add the values to it

                    value_ten[[lia]] = value[lia]
                #                                   # enrich the measurement set with the sample and corr. value tensor
                measurements.add(list(sample), value_ten)
            print("!" * 50)
            print("level: {} variance estimation: {}".format(level,
                                                             np.linalg.norm(var_estimator*((len(samples)-1)**(-1)))))
            print("!" * 50)
            if only_var_estimation:
                continue
            dimension = [fs_dim] + dimension
            #                                       # do reconstruction algorithm
            xe_tt = xe.uq_ra_adf(measurements, xebasis, dimension, target_eps, maxitr)
            #                                       # create extended TT from result
            ex_tt = ExtendedTT.from_xerus_tt(xe_tt, exbasis)
            #                                       # create an extended FEM tensor from result
            ex_tt = ExtendedFEMTT(ex_tt.components, ex_tt.basis, fs)
            retval.append(ex_tt.copy())
        if only_var_estimation:
            return None
        return MLExtendedFEMTT(retval)
    # endregion

    def save(self, path):
        retval = False
        for lia in range(self.L):
            retval = ExtendedFEMTT.save(self.cores[lia], path + "{}".format(lia))
            if retval is False:
                return False
        return retval

    @staticmethod
    def load(path, level):
        cores = []
        for lia in range(level):
            cores.append(ExtendedFEMTT.load(path + "{}".format(lia)))
        return MLExtendedFEMTT(cores)
