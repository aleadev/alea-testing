from __future__ import division
import unittest
from alea.linalg.tensor.extended_tt import ExtendedTT, BasisType, get_base_change_tensor, leg_base_change_constant
import tt
from alea.polyquad.polynomials import StochasticHermitePolynomials, LegendrePolynomials
from alea.application.tt_asgfem.tensorsolver.tt_lognormal import sample_lognormal_tt
import itertools as _iter
import xerus as xe
import time
import numpy as np
from functools import partial
np.random.seed(80190)

class TestExtendedTT(unittest.TestCase):

    def setUp(self):
        self.dimension = [4, 6, 8]
        self.ranks = [1, 8, 5, 1]
        self.dimension2 = [2, 9, 4, 3]
        self.ranks2 = [1, 15, 4, 2, 1]
        self.osel_tt = tt.rand(self.dimension, d=len(self.dimension), r=self.ranks)
        self.osel_tt = tt.vector.to_list(self.osel_tt)
        self.osel_tt2 = tt.rand(self.dimension2, d=len(self.dimension2), r=self.ranks2)
        self.osel_tt2 = tt.vector.to_list(self.osel_tt2)
        self.startTime = time.time()
        self.weights = []
        for lia in range(max(len(self.dimension2), len(self.dimension))):
            if lia < len(self.dimension) and lia < len(self.dimension2):
                self.weights.append([np.random.rand(1)] * (self.dimension2[lia]+self.dimension[lia]))
            elif lia < len(self.dimension):
                self.weights.append([np.random.rand(1)] * (self.dimension[lia]))
            elif lia < len(self.dimension2):
                self.weights.append([np.random.rand(1)] * (self.dimension2[lia]))
        self.weights_small = self.weights[:len(self.dimension)]

    def tearDown(self):
        t = time.time() - self.startTime
        print("{}: {}".format(self.id(), t))

    def test_creation_flag(self):
        for basistype in BasisType:
            if basistype == BasisType.TransformedHermite or basistype == BasisType.TransformedNormalisedHermite:
                continue
            base = [BasisType.points] + [basistype] * (len(self.dimension) - 1)
            ten = ExtendedTT(self.osel_tt, base)
            self.assertEqual(self.dimension, ten.n, "dimension do not coincide")
            self.assertEqual(self.ranks, ten.r, "ranks do not coincide")

    def test_creation(self):
        polybase = StochasticHermitePolynomials(normalised=True)
        base = []
        for lia in range(len(self.dimension)):
            base.append([])
            for lib in range(self.dimension[lia]):
                base[lia].append(lambda x: polybase.eval(lib, x, all_degrees=False))
        ten = ExtendedTT(self.osel_tt, base)
        self.assertEqual(self.dimension, ten.n, "dimension do not coincide")
        self.assertEqual(self.ranks, ten.r, "ranks do not coincide")

    def test_eval_point_wise_own_base(self):
        base = []

        for lia in range(len(self.dimension)):
            base.append([])
            for lib in range(self.dimension[lia]):
                #                                   # weird way of creating a delta function using the index variable
                def f(x, _i=lib): return 1 if x == _i else 0
                base[lia].append(f)
        ex_tt = ExtendedTT(self.osel_tt, base)
        osel_tt = tt.vector.from_list(self.osel_tt)
        for (i, j, k) in _iter.product(range(self.dimension[0]), range(self.dimension[1]), range(self.dimension[2])):
            self.assertAlmostEqual(ex_tt([i, j, k]), osel_tt[i, j, k])

    def test_eval_point_wise(self):
        base = [BasisType.points] * len(self.dimension)

        ex_tt = ExtendedTT(self.osel_tt, base)
        osel_tt = tt.vector.from_list(self.osel_tt)
        for (i, j, k) in _iter.product(range(self.dimension[0]), range(self.dimension[1]), range(self.dimension[2])):
            self.assertAlmostEqual(ex_tt([i, j, k]), osel_tt[i, j, k])

    def test_eval_hermite(self):
        base = [BasisType.points] + [BasisType.Hermite] * (len(self.dimension)-1)
        extt = ExtendedTT(self.osel_tt, base)
        xebase = xe.PolynomBasis.Hermite
        xett = xe.TTTensor(self.dimension)
        for lia in range(len(self.dimension)):
            loc_xett = xe.Tensor.from_function(self.osel_tt[lia].shape,
                                               lambda idx: self.osel_tt[lia][idx[0], idx[1], idx[2]])
            xett.set_component(lia, loc_xett)
        for (i, j, k) in _iter.product(range(self.dimension[0]), range(self.dimension[1]), range(self.dimension[2])):

            loc_xett = xe.uq_tt_evaluate(xett, (j, k), xebase)
            loc_xett = loc_xett[[i]]
            loc_extt = extt([i, j, k])
            self.assertLessEqual(np.abs((loc_extt - loc_xett)/loc_extt), 1e-10)

    def test_eval_transformed_hermite(self):

        base = [BasisType.points] + [BasisType.TransformedHermite] * (len(self.dimension)-1)
        weights = [[1] * self.dimension[lia] for lia in range(len(self.dimension))]

        extt = ExtendedTT(self.osel_tt, base, weights=weights)
        xebase = xe.PolynomBasis.Hermite
        xett = xe.TTTensor(self.dimension)
        for lia in range(len(self.dimension)):
            loc_xett = xe.Tensor.from_function(self.osel_tt[lia].shape,
                                               lambda idx: self.osel_tt[lia][idx[0], idx[1], idx[2]])
            xett.set_component(lia, loc_xett)

        xett_list = []
        extt_list = []

        for (j, k) in _iter.product(range(self.dimension[1]), range(self.dimension[2])):
            xett_ten = xe.uq_tt_evaluate(xett, (j, k), xebase)
            loc_extt = np.zeros(self.dimension[0])
            loc_xett = np.zeros(self.dimension[0])
            for i in range(self.dimension[0]):
                loc_extt[i] = extt([i, j, k])
                loc_xett[i] = xett_ten[i]
            extt_list.append(loc_extt)
            xett_list.append(loc_xett)

        for lia in range(len(xett_list)):
            self.assertLessEqual(np.linalg.norm(extt_list[lia] - xett_list[lia])/np.linalg.norm(extt_list[lia]),
                                 1e-10)

    
    def test_eval_transformed_normalised_hermite(self):

        base = [BasisType.points] + [BasisType.TransformedNormalisedHermite] * (len(self.dimension)-1)
        theta = 0.5
        rho = 1
        bmax = [np.random.rand(1) for _ in range(len(self.dimension) - 1)]
        weights = [[1] * self.dimension[0]]
        for lia in range(len(self.dimension) - 1):
            weights.append([np.exp(-theta * rho * bmax[lia])] * self.dimension[lia + 1])

        extt = ExtendedTT(self.osel_tt, base, weights=weights)
        extt_list = []
        maxtt_list = []

        for (j, k) in _iter.product(range(self.dimension[1]), range(self.dimension[2])):
            loc_extt = np.zeros(self.dimension[0])
            for i in range(self.dimension[0]):
                loc_extt[i] = extt([i, j, k])
            extt_list.append(loc_extt)
            maxtt_list.append(sample_lognormal_tt(extt.components, [j, k], bmax, theta=theta, rho=rho).reshape(-1))

        for lia in range(len(extt_list)):
            self.assertLessEqual(np.linalg.norm(extt_list[lia] - maxtt_list[lia]) / np.linalg.norm(extt_list[lia]),
                                 1e-10)

    
    def test_eval_legendre(self):
        base = [BasisType.points] + [BasisType.Legendre] * (len(self.dimension)-1)
        extt = ExtendedTT(self.osel_tt, base)
        xebase = xe.PolynomBasis.Legendre
        xett = xe.TTTensor(self.dimension)
        for lia in range(len(self.dimension)):
            loc_xett = xe.Tensor.from_function(self.osel_tt[lia].shape,
                                               lambda idx: self.osel_tt[lia][idx[0], idx[1], idx[2]])
            xett.set_component(lia, loc_xett)
        for (i, j, k) in _iter.product(range(self.dimension[0]), range(self.dimension[1]), range(self.dimension[2])):

            loc_xett = xe.uq_tt_evaluate(xett, (j**(-1) if j > 0 else j, k**(-1) if k > 0 else k), xebase)
            loc_xett = loc_xett[[i]]
            loc_extt = extt([i, j**(-1) if j > 0 else j, k**(-1) if k > 0 else k])
            self.assertLessEqual(np.abs((loc_extt - loc_xett)/loc_extt), 1e-10)

    
    def test_eval_norm_hermite(self):
        # this will not work, since the polynomials in xerus are not normalized
        base = [BasisType.points] + [BasisType.NormalisedHermite] * (len(self.dimension)-1)
        extt = ExtendedTT(self.osel_tt, base)
        xebase = xe.PolynomBasis.Hermite
        xett = xe.TTTensor(self.dimension)
        for lia in range(len(self.dimension)):
            loc_xett = xe.Tensor.from_function(self.osel_tt[lia].shape,
                                               lambda idx: self.osel_tt[lia][idx[0], idx[1], idx[2]])
            xett.set_component(lia, loc_xett)
        for (i, j, k) in _iter.product(range(self.dimension[0]), range(self.dimension[1]), range(self.dimension[2])):

            loc_xett = xe.uq_tt_evaluate(xett, (j, k), xebase)
            loc_xett = loc_xett[[i]]
            loc_extt = extt([i, j, k])
            # self.assertLessEqual((loc_extt - loc_xett)/loc_extt, 1e-10)

    
    def test_eval_norm_legendre(self):
        # This will not work since the polynomials in xerus are not normalized
        base = [BasisType.points] + [BasisType.NormalisedLegendre] * (len(self.dimension)-1)
        extt = ExtendedTT(self.osel_tt, base)
        xebase = xe.PolynomBasis.Legendre
        xett = xe.TTTensor(self.dimension)
        for lia in range(len(self.dimension)):
            loc_xett = xe.Tensor.from_function(self.osel_tt[lia].shape,
                                               lambda idx: self.osel_tt[lia][idx[0], idx[1], idx[2]])
            xett.set_component(lia, loc_xett)
        for (i, j, k) in _iter.product(range(self.dimension[0]), range(self.dimension[1]), range(self.dimension[2])):

            loc_xett = xe.uq_tt_evaluate(xett, (j**(-1) if j > 0 else j, k**(-1) if k > 0 else k), xebase)
            loc_xett = loc_xett[[i]]
            loc_extt = extt([i, j**(-1) if j > 0 else j, k**(-1) if k > 0 else k])

            self.assertLessEqual((loc_extt - loc_xett)/loc_extt, 1e-10)

    
    def test_from_osel_tt(self):
        osel_tt = tt.rand(self.dimension, len(self.dimension), self.ranks)
        basis = [BasisType.points] * len(self.dimension)
        ex_tt = ExtendedTT.from_osel_tt(osel_tt, basis)

        for (i, j, k) in _iter.product(range(self.dimension[0]), range(self.dimension[1]), range(self.dimension[2])):
            self.assertLessEqual(np.abs((ex_tt([i, j, k]) - osel_tt[i, j, k])/ex_tt([i, j, k])), 1e-10)

        osel_tt = tt.vector.to_list(osel_tt)
        self.assertEqual(len(osel_tt), len(ex_tt.components))
        for lia in range(len(osel_tt)):
            comp_osel = osel_tt[lia]
            comp_ex = ex_tt.components[lia]
            self.assertEqual(comp_osel.shape[0], comp_ex.shape[0])
            self.assertEqual(comp_osel.shape[1], comp_ex.shape[1])
            self.assertEqual(comp_osel.shape[2], comp_ex.shape[2])
            for (i, j, k) in _iter.product(range(comp_ex.shape[0]), range(comp_ex.shape[1]), range(comp_ex.shape[2])):
                self.assertLessEqual(np.abs((comp_osel[i, j, k] - comp_ex[i, j, k])/comp_osel[i, j, k]), 1e-10)

    
    def test_from_xerus_tt(self):
        #                                           # You can not create a xerus tt tensor of given rank vector. only sc
        xe_tt = xe.TTTensor.random(self.dimension, self.ranks[1:-1])
        basis = [BasisType.points] * len(self.dimension)
        ex_tt = ExtendedTT.from_xerus_tt(xe_tt, basis)

        for (i, j, k) in _iter.product(range(self.dimension[0]), range(self.dimension[1]), range(self.dimension[2])):
            self.assertLessEqual(np.abs((ex_tt([i, j, k]) - xe_tt[[i, j, k]])/ex_tt([i, j, k])), 1e-10)

        self.assertEqual(xe_tt.degree(), len(ex_tt.components))
        for lia in range(xe_tt.degree()):
            comp_xe = xe_tt.get_component(lia)
            comp_ex = ex_tt.components[lia]
            xe_dim = comp_xe.dimensions
            self.assertEqual(xe_dim[0], comp_ex.shape[0])
            self.assertEqual(xe_dim[1], comp_ex.shape[1])
            self.assertEqual(xe_dim[2], comp_ex.shape[2])
            for (i, j, k) in _iter.product(range(comp_ex.shape[0]), range(comp_ex.shape[1]), range(comp_ex.shape[2])):
                self.assertLessEqual(np.abs((comp_xe[[i, j, k]] - comp_ex[i, j, k])/comp_ex[i, j, k]), 1e-10)

    
    def test_from_xerus_tt_linear(self):
        #                                           # You can not create a xerus tt tensor of given rank vector. only sc
        xe_tt = xe.TTTensor.random(self.dimension, self.ranks[1:-1])
        basis = [BasisType.points] * len(self.dimension)
        ex_tt = ExtendedTT.from_xerus_tt(xe_tt, basis)

        for lia, (i, j, k) in enumerate(_iter.product(range(self.dimension[0]), range(self.dimension[1]),
                                                      range(self.dimension[2]))):
            self.assertLessEqual(np.abs((ex_tt([i, j, k]) - xe_tt[lia])/ex_tt([i, j, k])), 1e-10)

        self.assertEqual(xe_tt.degree(), len(ex_tt.components))
        for lia in range(xe_tt.degree()):
            comp_xe = xe_tt.get_component(lia)
            comp_ex = ex_tt.components[lia]
            xe_dim = comp_xe.dimensions
            self.assertEqual(xe_dim[0], comp_ex.shape[0])
            self.assertEqual(xe_dim[1], comp_ex.shape[1])
            self.assertEqual(xe_dim[2], comp_ex.shape[2])
            for lic, (i, j, k) in enumerate(_iter.product(range(comp_ex.shape[0]), range(comp_ex.shape[1]),
                                                          range(comp_ex.shape[2]))):
                self.assertLessEqual(np.abs((comp_xe[lic] - comp_ex[i, j, k])/comp_ex[i, j, k]), 1e-10)

    
    def test_neg(self):
        basis = [BasisType.points]*len(self.dimension)
        extt = ExtendedTT(self.osel_tt, basis)
        neg_extt = -extt
        for (i, j, k) in _iter.product(range(self.dimension[0]), range(self.dimension[1]), range(self.dimension[2])):
            self.assertLessEqual(np.abs((extt([i, j, k]) + neg_extt([i, j, k])) / extt([i, j, k])), 1e-10)

    
    def test_mul(self):
        basis = [BasisType.points]*len(self.dimension)
        extt = ExtendedTT(self.osel_tt, basis)
        mul_extt = extt * 5
        for (i, j, k) in _iter.product(range(self.dimension[0]), range(self.dimension[1]), range(self.dimension[2])):
            self.assertLessEqual(np.abs((5*extt([i, j, k]) - mul_extt([i, j, k])) / extt([i, j, k])), 1e-10)

    
    def test_add(self):
        for basistype in BasisType:
            if basistype == BasisType.TransformedHermite or basistype == BasisType.TransformedNormalisedHermite:
                continue

            basis = [basistype]*len(self.dimension)
            extt = ExtendedTT(self.osel_tt, basis)
            add_extt = extt + extt
            self.assertEqual(len(extt.r), len(add_extt.r))
            self.assertEqual(len(extt.n), len(add_extt.n))
            self.assertEqual(extt.dim, add_extt.dim)
            for lia in range(extt.dim+1):
                if lia == 0 or lia == extt.dim:
                    self.assertEqual(add_extt.r[lia], 1)
                    continue
                self.assertEqual(2*extt.r[lia], add_extt.r[lia])

            for (i, j, k) in _iter.product(range(self.dimension[0]), range(self.dimension[1]),
                                           range(self.dimension[2])):
                self.assertLessEqual(np.abs((2*extt([i, j, k]) - add_extt([i, j, k])) / extt([i, j, k])), 1e-10)

    
    def test_add_vec(self):
        for basistype in BasisType:
            if basistype == BasisType.TransformedHermite or basistype == BasisType.TransformedNormalisedHermite:
                continue
            basis = [basistype] * len(self.dimension)
            for lia in range(len(self.dimension)):
                basis[lia] = BasisType.points
                extt = ExtendedTT(self.osel_tt, basis)
                vec = np.random.randn(extt.n[lia])
                add_extt = extt.add_vec_to_component(vec, lia)
                for (i, j, k) in _iter.product(range(self.dimension[0]), range(self.dimension[1]),
                                               range(self.dimension[2])):

                    if lia == 0:
                        # print vec[i]
                        self.assertLessEqual(np.abs(((extt([i, j, k]) + vec[i]) - add_extt([i, j, k])) /
                                                    add_extt([i, j, k])),
                                             1e-10)
                    elif lia == 1:
                        # print vec[j]
                        self.assertLessEqual(np.abs(((extt([i, j, k]) + vec[j]) - add_extt([i, j, k])) /
                                                    add_extt([i, j, k])),
                                             1e-10)
                    elif lia == 2:
                        # print vec[k]
                        self.assertLessEqual(np.abs(((extt([i, j, k]) + vec[k]) - add_extt([i, j, k])) /
                                                    add_extt([i, j, k])),
                                             1e-10)
                    else:
                        raise ValueError

    
    def test_multiply_comp_with_mat_right(self):
        from alea.polyquad.polynomials import LegendrePolynomials
        leg = LegendrePolynomials(normalised=False)
        leg_norm = LegendrePolynomials(normalised=True)
        x = -0.5

        basis = [BasisType.Legendre] * len(self.dimension)
        extt = ExtendedTT(self.osel_tt, basis)
        for lia in range(len(self.dimension)-1, -1, -1):
            _basis = [BasisType.Legendre] * len(self.dimension)
            _basis[lia] = BasisType.NormalisedLegendre
            extt_norm = ExtendedTT(self.osel_tt, _basis)
            mat = np.diag([np.sqrt((2*lib) + 1) for lib in range(self.dimension[lia])])
            # print mat
            # mat = np.linalg.inv(mat)
            extt_own_norm = extt.multiply_comp_with_matrix_right(mat, lia)
            for i in range(len(self.dimension)-1, -1, -1):
                if i == lia:
                    _leg = np.array(leg.eval(self.dimension[i]-1, x, all_degrees=True)).reshape(-1)
                    _leg_norm = np.array(leg_norm.eval(self.dimension[i] - 1, x, all_degrees=True)).reshape(-1)

                    for (k1, k2) in _iter.product(range(self.ranks[i]), range(self.ranks[i+1])):
                        curr_leg_norm = extt_norm.components[i][k1, :, k2].dot(_leg_norm)
                        curr_leg_own = extt_own_norm.components[i][k1, :, k2].dot(_leg)
                        self.assertLessEqual(np.abs(curr_leg_norm - curr_leg_own)/np.abs(curr_leg_norm), 1e-10)

                else:
                    self.assertLessEqual(np.linalg.norm(extt_norm.components[i] - extt_own_norm.components[i]) /
                                         np.linalg.norm(extt_own_norm.components[i]), 1e-10)
            for (i, j, k) in _iter.product(range(self.dimension[0]), range(self.dimension[1]),
                                           range(self.dimension[2])):
                self.assertLessEqual(np.abs((extt_own_norm([i, j, k]) - extt_norm([i, j, k])) / extt_norm([i, j, k])),
                                     1e-10)

    def test_Legendre_polynomials(self):
        from alea.polyquad.polynomials import LegendrePolynomials
        x = [np.random.rand(1)*2 - 1 for _ in range(5)]
        poly = LegendrePolynomials(normalised=False)
        poly_norm = LegendrePolynomials(normalised=True)
        for _x in x:
            leg = poly.eval(10, _x, all_degrees=True)
            leg_norm = np.array(poly_norm.eval(10, _x, all_degrees=True)).reshape(-1)
            mat = np.diag([np.sqrt(((2*lib) + 1)) for lib in range(11)])
            new = np.dot(np.array(leg).T, mat)[0]
            self.assertLessEqual(np.linalg.norm(leg_norm - new)/np.linalg.norm(new), 1e-10)

    def test_base_change_tensor(self):
        np.random.seed(80190)
        for basistype in BasisType:
            if basistype == BasisType.points or \
                            basistype == BasisType.TransformedHermite or \
                            basistype == BasisType.TransformedNormalisedHermite:
                continue

            deg1 = 10
            deg2 = 8
            if basistype == BasisType.Legendre:
                poly = LegendrePolynomials(normalised=False)
            elif basistype == BasisType.NormalisedLegendre:
                poly = LegendrePolynomials(normalised=True)
            elif basistype == BasisType.Hermite:
                poly = StochasticHermitePolynomials(normalised=False)
            elif basistype == BasisType.NormalisedHermite:
                poly = StochasticHermitePolynomials(normalised=True)
            else:
                raise ValueError
            poly_eval = partial(poly.eval, all_degrees=False)
            samples = np.random.rand(20)
            for (lia, lib) in _iter.product(range(1, deg1), range(1, deg2)):
                mid = get_base_change_tensor(deg1, deg2, basistype)
                for sample in samples:
                    lhs = poly_eval(lia, sample) * poly_eval(lib, sample)
                    rhs = sum([mid[lia, lic, lib]*poly_eval(lic, sample) for lic in range(lia + lib + 1)])
                    self.assertLessEqual(np.abs(lhs - rhs)/np.abs(lhs), 1e-7, msg="basistype: {}".format(basistype))


    def test_product_two_extendedTT(self):
        for basistype in BasisType:
            if basistype == BasisType.points:
                continue

            base = [basistype] * (len(self.dimension))
            base2 = [basistype] * (len(self.dimension2))
            ten1 = ExtendedTT(self.osel_tt, base, self.weights_small)
            ten2 = ExtendedTT(self.osel_tt2, base2, self.weights)

            prod = ten1.multiply_with_extendedTT(ten2)
            samples = [np.random.rand(4) for _ in range(20)]
            for (i, j, k, l) in samples:
                self.assertLessEqual(np.abs((ten1([i, j, k])*ten2([i, j, k, l]) - prod([i, j, k, l])) /
                                            prod([i, j, k, l])), 1e-10,
                                     msg="failed for ({}, {}, {}, {}) and basis: {}".format(i, j, k, l, basistype))

    def test_tensor_point_evaluation(self):
        for basistype in BasisType:
            if basistype == BasisType.points:
                continue
        # TODO

    def test_normalisation_legendre(self):
        base = [BasisType.Legendre] * (len(self.dimension))
        ten1 = ExtendedTT(self.osel_tt, base, self.weights_small)
        ten2 = ten1.copy()
        ten1.normalise()
        samples = [np.random.rand(3) for _ in range(20)]
        for (i, j, k) in samples:
            self.assertLessEqual(np.abs(ten1([i, j, k]) - ten2([i, j, k])) / ten2([i, j, k]), 1e-10,
                                 msg="failed for ({}, {}, {}) and basis: Legendre".format(i, j, k))

    def test_normalisation_hermite(self):
        base = [BasisType.Hermite] * (len(self.dimension))
        ten1 = ExtendedTT(self.osel_tt, base, self.weights_small)
        ten2 = ten1.copy()
        ten1.normalise()
        samples = [np.random.rand(3) for _ in range(20)]
        for (i, j, k) in samples:
            self.assertLessEqual(np.abs(ten1([i, j, k]) - ten2([i, j, k])) / ten2([i, j, k]), 1e-10,
                                 msg="failed for ({}, {}, {}) and basis: Hermite".format(i, j, k))

    def test_normalisation_transformed_hermite(self):
        base = [BasisType.TransformedNormalisedHermite] * (len(self.dimension))
        ten1 = ExtendedTT(self.osel_tt, base, self.weights_small)
        ten2 = ten1.copy()
        ten1.normalise()
        samples = [np.random.rand(3) for _ in range(20)]
        for (i, j, k) in samples:
            self.assertLessEqual(np.abs(ten1([i, j, k]) - ten2([i, j, k])) / ten2([i, j, k]), 1e-10,
                                 msg="failed for ({}, {}, {}) and basis: Transformed Hermite".format(i, j, k))


if __name__ == '__main__':
    print("#"*20)
    suite = unittest.TestLoader().loadTestsFromTestCase(TestExtendedTT)
    unittest.TextTestRunner(verbosity=0).run(suite)

