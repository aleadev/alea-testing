from __future__ import division
import unittest
from alea.linalg.tensor.extended_tt import ExtendedTT, BasisType
from alea.linalg.tensor.extended_fem_tt import ExtendedFEMTT
import tt
from alea.polyquad.polynomials import StochasticHermitePolynomials
from alea.application.tt_asgfem.tensorsolver.tt_lognormal import sample_lognormal_tt
import itertools as _iter
import xerus as xe
import time
import numpy as np
from dolfin import UnitSquareMesh, FunctionSpace, Function


class TestExtendedFEMTT(unittest.TestCase):

    def setUp(self):
        mesh = UnitSquareMesh(10, 10)
        self.fs = FunctionSpace(mesh, 'CG', 1)
        self.dimension = [self.fs.dim(), 6, 8]
        self.ranks = [1, 8, 5, 1]
        self.weights = [1, [1]*self.dimension[1], [1]*self.dimension[2]]
        self.osel_tt = tt.rand(self.dimension, d=len(self.dimension), r=self.ranks)
        self.osel_tt = tt.vector.to_list(self.osel_tt)
        self.startTime = time.time()

    def tearDown(self):
        t = time.time() - self.startTime
        print("{}: {}".format(self.id(), t))

    def test_creation_flag(self):
        for basistype in BasisType:
            base = [BasisType.points] + [basistype] * (len(self.dimension) - 1)
            ten = ExtendedFEMTT(self.osel_tt, base, self.fs, weights=self.weights)
            self.assertEqual(self.dimension, ten.n, "dimension do not coincide")
            self.assertEqual(self.ranks, ten.r, "ranks do not coincide")

    def test_creation_own(self):
        polybase = StochasticHermitePolynomials(normalised=True)
        base = []
        for lia in range(len(self.dimension)):
            if lia == 0:
                base.append(BasisType.points)
                continue
            base.append([])
            for lib in range(self.dimension[lia]):
                base[lia].append(lambda x: polybase.eval(lib, x, all_degrees=False))
        tt = ExtendedTT(self.osel_tt, base)
        self.assertEqual(self.dimension, tt.n, "dimension do not coincide")
        self.assertEqual(self.ranks, tt.r, "ranks do not coincide")

    def test_eval(self):
        base = [BasisType.points] + [BasisType.NormalisedHermite] * (len(self.dimension) - 1)
        extt = ExtendedFEMTT(self.osel_tt, base, self.fs)
        x = [np.random.rand(2) for _ in range(10)]
        extt_eval = extt.eval_first_comp(list(x))
        extt_list = []
        maxtt_list = []
        loc_max_fun = Function(self.fs)
        for (j, k) in _iter.product(range(self.dimension[1]), range(self.dimension[2])):
            loc_extt = np.zeros(self.dimension[0])
            loc_maxtt = np.zeros(self.dimension[0])

            loc_max_value = sample_lognormal_tt(extt.components, [j, k], [1]*(len(self.dimension)-1), theta=0, rho=1)
            loc_max_fun.vector()[:] = loc_max_value

            for i in range(len(x)):
                loc_extt[i] = extt_eval([i, j, k])
                loc_maxtt[i] = loc_max_fun(x[i])
            extt_list.append(loc_extt)
            maxtt_list.append(loc_maxtt)
        for lia in range(len(extt_list)):
            self.assertLessEqual(np.linalg.norm(extt_list[lia] - maxtt_list[lia]) / np.linalg.norm(extt_list[lia]),
                                 1e-10)


if __name__ == '__main__':
    print("#"*20)
    suite = unittest.TestLoader().loadTestsFromTestCase(TestExtendedFEMTT)
    unittest.TextTestRunner(verbosity=0).run(suite)

