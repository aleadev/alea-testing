from extended_tt import ExtendedTT, BasisType
from alea.polyquad.polynomials import LegendrePolynomials, StochasticHermitePolynomials
from dolfin import FunctionSpace, Function, File, Mesh
import itertools as _iter

import numpy as np
import xerus as xe
import h5py


class ExtendedFEMTT(ExtendedTT):
    def __init__(self,
                 components,                        # type: list
                 basis,                             # type: list
                 fs,                                # type: FunctionSpace
                 weights=None                       # type: list
                 ):
        """
        Init for an extended TT having the first component a finite element basis given by a function space.
        We assume the basis list has the same length as the components list and the first element of basis should be
        a point-wise evaluation, later overwritten by the fs basis functions.
        :param components: list of component tensors
        :param basis: list of basis functions
        :param fs: finite element function space (dolfin)
        """

        assert basis[0] == BasisType.points
        ExtendedTT.__init__(self, components, basis, weights=weights)
        assert isinstance(fs, FunctionSpace)
        self.weights = weights
        self.first_comp_fs = fs

    # region def sample
    def sample(self, x, project_result=False):
        """
        defines the call operator by evaluating using every basis element
        :param x: point to evaluate the corresponding basis at
        :param project_result: flag to project the result onto to function space to obtain a cont. function
        :return: fem representation of the corresponding sample
        """
        assert len(x) == len(self.components)-1
        retval = 1
        for lia in range(len(self.components) - 1, 0, -1):  # for every component, starting at the end
            comp = self.components[lia]
            basis = self.basis[lia]

            values = None

            if basis == BasisType.points:  # current basis type is point-wise evaluation
                values = np.zeros(comp.shape[1])
                values[x[lia-1]] = 1
            elif basis == BasisType.Legendre:  # current basis type is Legendre polynomials
                base = LegendrePolynomials(normalised=False)
            elif basis == BasisType.NormalisedLegendre:
                base = LegendrePolynomials(normalised=True)
                #                                   # current basis type is Hermite polynomials or transformed Hermite
            elif basis == BasisType.Hermite:
                base = StochasticHermitePolynomials(0, 1, normalised=False)
            elif basis == BasisType.TransformedHermite:
                assert self.weights is not None
                assert len(self.weights[lia]) >= comp.shape[1]
                base = StochasticHermitePolynomials(0, 1, normalised=False)
            elif basis == BasisType.NormalisedHermite:
                base = StochasticHermitePolynomials(0, 1, normalised=True)
            elif basis == BasisType.TransformedNormalisedHermite:
                assert self.weights is not None
                assert len(self.weights[lia]) >= comp.shape[1]
                base = StochasticHermitePolynomials(0, 1, normalised=True)
            else:  # alternatively. use a self defined, callable basis
                assert len(basis) >= comp.shape[1]
                for lib in range(len(basis)):  # for every item in the current basis list assert callable
                    assert callable(basis[lib])
                values = np.array([basis[lib](x[lia-1]) for lib in range(comp.shape[1])])

            if values is None:
                values = np.zeros(comp.shape[1])
                for lib in range(comp.shape[1]):
                    if basis == BasisType.TransformedHermite or basis == BasisType.TransformedNormalisedHermite:
                        values[lib] = base.eval(lib, x[lia-1] * self.weights[lia][lib], all_degrees=False)

                    else:
                        values[lib] = base.eval(lib, x[lia-1], all_degrees=False)
            if lia == len(self.components) - 1:  # if it is the last component, just make a dot product
                retval = np.dot(comp[:, :, 0], values)
                continue

            _comp = comp.transpose(0, 2, 1)  # else, transpose [k_m, n, k_m+1] -> [k_m, k_m+1, n]
            #                                       # reshape -> [k_m*k_m+1, n]
            _comp = _comp.reshape((comp.shape[0] * comp.shape[2], comp.shape[1]), order="F")
            _comp = _comp.dot(values)  # dot -> [k_m*k_m+1]
            #                                       # reshape -> [k_m, k_m+1]
            _comp = _comp.reshape((comp.shape[0], comp.shape[2]), order="F")
            retval = _comp.dot(retval)  # dot -> [k_m]
        assert retval.shape[0] == self.components[0].shape[2]
        retval = self.components[0][0, :, :].dot(retval)
        if project_result:
            ret = Function(self.first_comp_fs)
            ret.vector()[:] = np.ascontiguousarray(retval)
            return ret
        return retval
    # endregion

    # region def eval first component
    def eval_first_comp(self,
                        x                           # type: list
                        ):
        """
        Takes a coordinate or a list of coordinates and evaluates the first component at those given points.
        The result is a new tensor train in functional representation having the same basis in all but the first
        component, but a point-wise evaluation in the first component. Hence, a usual ExtendedTT
        :param x: (list of) coordinate
        :return: Extended TT
        """
        if not isinstance(x, list):
            x = [x]
        first_comp = self.components[0][0, :, :]
        first_fun = Function(self.first_comp_fs)
        new_first_comp = np.zeros((1, len(x), first_comp.shape[1]))
        for lia, coord in enumerate(x):
            for k in range(first_comp.shape[1]):
                first_fun.vector()[:] = first_comp[:, k]
                new_first_comp[0, lia, k] = first_fun(coord)
        new_components = []
        for lia in range(len(self.components)):
            if lia == 0:
                new_components.append(new_first_comp)
                continue
            new_components.append(self.components[lia])
        retval = ExtendedTT(new_components, self.basis, weights=self.weights)
        return retval

    def multiply_with_extendedTT(self,
                                 other              # type: ExtendedTT
                                 ):
        raise NotImplementedError

    # endregion

    # region def overwrite normalise
    def normalise(self):
        components = self.components[1:]            # take stochastic components
        basis = self.basis[1:]                      # and basis
        if self.weights is None:                    # and weights
            weights = None
        else:
            weights = self.weights[1:]
        #                                           # to create an extended tensor train
        intermediate_tt = ExtendedTT(components, basis, weights)
        intermediate_tt.normalise()                 # that can be normalized
        #                                           # and we keep the components
        self.components[1:] = intermediate_tt.components
    # endregion

    # region def save
    def save(self, path):
        mesh_file = File(path + "-mesh.xml")
        mesh_file << self.first_comp_fs.mesh()
        try:
            with h5py.File(path + "-femdata.dat", "w") as outfile:
                outfile["family"] = self.first_comp_fs.ufl_element().family()
                outfile["degree"] = self.first_comp_fs.ufl_element().degree()
        except Exception:
            return False
        ExtendedTT.save(self, path + "-ten.dat")
        return True
    # endregion

    # region def load
    @staticmethod
    def load(path):
        mesh = Mesh(path + "-mesh.xml")
        try:
            with h5py.File(path + "-femdata.dat", "r") as outfile:
                family = str(outfile["family"][()])
                degree = int(outfile["degree"][()])
        except IOError as ex:
            print("IOERROR can not import from file " + path + " err: " + ex.message)
            return None
        except EOFError as ex:
            print("EOFERROR can not import from file " + path + " err: " + ex.message)
            return None
        except KeyError as ex:
            print("KEYERROR can not import from file " + path + " err: " + ex.message)
            return None
        retval = ExtendedTT.load(path + "-ten.dat")
        fs = FunctionSpace(mesh, family, degree)
        retval = ExtendedFEMTT(retval.components, retval.basis, fs, retval.weights)
        return retval
    # endregion

    # region def copy
    def copy(self):
        """ Creates a copy of the Extended TT Object"""
        from copy import deepcopy
        _components = deepcopy(self.components)
        _basis = deepcopy(self.basis)
        _weights = deepcopy(self.weights)
        fs = FunctionSpace(self.first_comp_fs.mesh(), self.first_comp_fs.ufl_element().family(),
                           self.first_comp_fs.ufl_element().degree())
        retval = ExtendedFEMTT(_components, _basis, fs, _weights)
        return retval
    # endregion

    # region def from xerus tt
    @staticmethod
    def from_xerus_tt(xerus_tt,
                      basis,
                      fs,
                      weights=None
                      ):
        """
        Creates an extended FEM tensor train from an Oseledets TT Tensor.
        By default the basis is set to point-wise. otherwise pass a list of basis types or callable functions.
        :param xerus_tt: tensor train in xerus TTTensor format
        :param basis: list of basis types or callable functions
        :param weights: list of weights
        :return: ExtendedTT
        """
        components = []
        for lia in range(xerus_tt.degree()):
            comp = xerus_tt.get_component(lia)
            # TODO: for loop over all entries, assert len == 3
            dim = comp.dimensions
            assert len(dim) == 3
            curr_component = np.zeros(dim)
            for (i, j, k) in _iter.product(range(dim[0]), range(dim[1]), range(dim[2])):
                curr_component[i, j, k] = comp[[i, j, k]]
            components.append(curr_component)
        if basis is not None:
            assert len(basis) == len(components)
            _basis = basis
        else:
            _basis = [BasisType.points] * len(components)

        return ExtendedFEMTT(components, _basis, fs, weights)

    # endregion

    # region def from_reconstruction
    @staticmethod
    def from_reconstruction(fs,                     # type: FunctionSpace
                            fun,                    # type: function
                            samples,                # type: list
                            dimensions,             # type: list
                            poly_basis="L",         # type: str
                            target_eps=1e-8,        # type: float
                            maxitr=10000,           # type: int
                            progress=False          # type: bool
                            ):
        """
        starts the reconstruction algorithm by invoking the xerus library with the given parameters
        :param fs: dolfin FunctionSpace to obtain the dofs from
        :param fun: function to create function values from ala a(y, fs)
        :param samples: list of sample points
        :param dimensions: list of dimensions meaning the tensor architecture
        :param poly_basis: L - Legendre, H - Hermite
        :param target_eps: target accuracy
        :param maxitr: maximal number of iteration in reconstruction algorithm
        :param progress: flag to show how many samples are created atm
        :return: ExtendedFEMTT
        """
        assert isinstance(fs, FunctionSpace)        # check input: function spaces
        assert isinstance(samples, list)            # check input: list of samples
        assert isinstance(dimensions, list)         # check input: list of dimensions
        for lia in range(len(dimensions)):
            assert dimensions[lia] > 1

        fs_dim = fs.dim()                           # the physical space dimensionality
        #                                           # define polynomial flag
        if poly_basis == "L":
            xebasis = xe.PolynomBasis.Legendre
            exbasis = [BasisType.points] + [BasisType.Legendre] * (len(dimensions))
        else:
            xebasis = xe.PolynomBasis.Hermite
            exbasis = [BasisType.points] + [BasisType.Hermite] * (len(dimensions))

        measurements = xe.UQMeasurementSet()        # create Xerus measurement Set

        for lia, sample in enumerate(samples):      # for every sample
            if progress and lia % 10 == 0:
                print("samples created: {}/{}".format(lia, len(samples)))
            #                                       # evaluate function on current level
            value = list(fun(sample, fs).vector()[:])
            #                                       # create a one dimensional xerus tensor
            value_ten = xe.Tensor(dim=[fs_dim])
            for lia in range(fs_dim):               # and add the values to it
                value_ten[[lia]] = value[lia]
            #                                       # enrich the measurement set with the sample and corr. value tensor
            measurements.add(list(sample), value_ten)
        dimension = [fs_dim] + dimensions
        #                                          # do reconstruction algorithm
        xe_tt = xe.uq_ra_adf(measurements, xebasis, dimension, target_eps, maxitr)
        #                                           # create extended TT from result
        ex_tt = ExtendedTT.from_xerus_tt(xe_tt, exbasis)
        #                                           # create an extended FEM tensor from result
        ex_tt = ExtendedFEMTT(ex_tt.components, ex_tt.basis, fs)

        return ex_tt

    # endregion
