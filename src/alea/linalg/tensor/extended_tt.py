from __future__ import division
import numpy as np
from enum import Enum
from numpy.polynomial.hermite_e import hermegauss
from numpy.polynomial.legendre import leggauss
from alea.polyquad.polynomials import (StochasticHermitePolynomials, LegendrePolynomials)
import tt
from functools import partial
import itertools as _iter
from math import factorial as fac
from decimal import Decimal as dec
from decimal import getcontext
from alea.utils.progress.bar import Bar
import h5py
import xerus as xe
getcontext().prec = 100


class BasisType(Enum):
    points = 1
    Legendre = 2
    NormalisedLegendre = 3
    Hermite = 4
    NormalisedHermite = 5
    TransformedHermite = 6
    TransformedNormalisedHermite = 7


def frac_fac(a, b):
    a = int(a)
    b = int(b)
    if a > b:
        return np.prod([lia for lia in range(b + 1, a + 1)], dtype=np.float64)
    elif a < b:
        return np.prod([lia ** (-1) for lia in range(a + 1, b + 1)], dtype=np.float64)
    else:
        return 1


def herm_base_change_constant(n, m, k, normalised=False):
    if not normalised:
        ret1 = dec(frac_fac(n, n - k))
        ret2 = dec(frac_fac(m, m - k))
        ret3 = dec(fac(k)) ** (-1)
    else:
        ret1 = dec.sqrt(dec(fac(n))) * dec(fac(n - k)) ** (-1)
        ret2 = dec.sqrt(dec(fac(m))) * dec(fac(m - k)) ** (-1)
        ret3 = dec.sqrt(dec(fac(n + m - 2 * k))) * dec(fac(k)) ** (-1)

    return ret1 * ret2 * ret3


def leg_base_change_constant(m1, m2, m3, normalised=False):
    if (m1 + m2 + m3) % 2 != 0:
        return 0
    _k = (m1 + m2 + m3) * 0.5
    if m1 > _k or m2 > _k or m3 > _k:
        return 0
    if not normalised:
        ret1 = (2 * m3 + 1) * (2 * _k + 1) ** (-1)
    else:
        ret1 = np.sqrt((2 * m1 + 1) * (2 * m2 + 1) * (2 * m3 + 1)) * (2 * _k + 1) ** (-1)
    ret2 = fac(_k) ** 2 * fac((2 * _k - 2 * m2)) * fac(2 * _k - 2 * m1) * fac(2 * _k - 2 * m3)
    ret3 = fac(2 * _k) * fac(_k - m2) ** 2 * fac(_k - m1) ** 2 * fac(_k - m3) ** 2
    ret_f_1 = frac_fac(_k, 2 * _k) * fac(_k)
    ret_f_2 = frac_fac((2 * _k - 2 * m2), _k - m2) * fac(_k - m2) ** (-1)
    ret_f_3 = frac_fac(2 * _k - 2 * m1, _k - m1) * fac(_k - m1) ** (-1)
    ret_f_4 = frac_fac(2 * _k - 2 * m3, _k - m3) * fac(_k - m3) ** (-1)
    # return ret1 * ret2 * (ret3 ** (-1))
    return ret1 * ret_f_1 * ret_f_2 * ret_f_3 * ret_f_4


def get_base_change_tensor(deg1, deg2, basis_type):
    assert deg1 > 0
    assert deg2 > 0

    retval = np.zeros((deg1, deg1 + deg2, deg2))

    if basis_type == BasisType.Legendre:
        for lia, lib in _iter.product(range(deg1), range(deg2)):
            for lic in range(lia+lib+1):
                retval[lia, lic, lib] = leg_base_change_constant(lia, lib, lic, normalised=False)
    elif basis_type == BasisType.NormalisedLegendre:
        for lia, lib in _iter.product(range(deg1), range(deg2)):
            for lic in range(lia+lib+1):
                retval[lia, lic, lib] = leg_base_change_constant(lia, lib, lic, normalised=True)
    elif basis_type == BasisType.Hermite or basis_type == BasisType.TransformedHermite:
        for lia, lib in _iter.product(range(deg1), range(deg2)):
            for lic in range(min(lia, lib)+1):
                retval[lia, lia + lib - 2*lic, lib] = herm_base_change_constant(lia, lib, lic, normalised=False)
    elif basis_type == BasisType.NormalisedHermite or basis_type == BasisType.TransformedNormalisedHermite:
        for lia, lib in _iter.product(range(deg1), range(deg2)):
            for lic in range(min(lia, lib)+1):
                retval[lia, lia + lib - 2*lic, lib] = herm_base_change_constant(lia, lib, lic, normalised=True)
    else:
        raise ValueError("unknown basis type: {}".format(basis_type))

    return retval


class ExtendedTT:
    # region def init
    def __init__(self, components, basis, weights=None):
        """
        constructor for an extended tensor train
        :param components: list of order 3 component tensor
        :param basis: list of basis functions
        :param weights: list of weights to transform the polynomial basis
        """
        assert len(components) == len(basis)        # the amount of components and basis types must coincide
        if weights is not None:
            assert len(components) == len(weights)  # even the list of weights should have the same length if set
        self.components = []                        # define list of components (core tensors)
        self.basis = []                             # define list of basis
        self.dim = len(components)                  # set dimension to length of components (convenient?)
        self.n = []                                 # init dimension list
        self.r = []                                 # init rank list
        self.weights = None                         # init weights as None
        if weights is not None:
            self.weights = weights
        for lia in range(len(components)):          # for every component in the tensor train of order 3
            assert len(components[lia].shape) == 3
            # if lia == 0:                            # if it is the first dimension -> rank[0] must be 1
            #     assert components[lia].shape[0] == 1
            if lia == len(components)-1:            # if it is the last dimension -> rank[-1] must be 1
                assert components[lia].shape[2] == 1
            #                                       # append dimension, rank and component to instance
            self.n.append(components[lia].shape[1])
            self.r.append(components[lia].shape[0])
            self.components.append(components[lia])
            self.basis.append(basis[lia])

        self.r.append(components[-1].shape[2])
    # endregion

    # region def call
    def __call__(self, x):
        """
        defines the call operator by evaluating using every basis element
        :param x: point to evaluate the corresponding basis at
        :return: value
        """
        assert len(x) == len(self.components)
        retval = 1
        for lia in range(len(x)-1, -1, -1):         # for every component, starting at the end
            comp = self.components[lia]
            basis = self.basis[lia]

            values = None
            base = None
            if basis == BasisType.points:           # current basis type is point-wise evaluation
                values = np.zeros(comp.shape[1], dtype=np.int)
                values[int(x[lia])] = 1
            elif basis == BasisType.Legendre:       # current basis type is Legendre polynomials
                base = LegendrePolynomials(normalised=False)
            elif basis == BasisType.NormalisedLegendre:
                base = LegendrePolynomials(normalised=True)
                #                                   # current basis type is Hermite polynomials or transformed Hermite
            elif basis == BasisType.Hermite:
                base = StochasticHermitePolynomials(0, 1, normalised=False)
            elif basis == BasisType.TransformedHermite:
                assert self.weights is not None
                assert len(self.weights[lia]) >= comp.shape[1]
                base = StochasticHermitePolynomials(0, 1, normalised=False)
            elif basis == BasisType.NormalisedHermite:
                base = StochasticHermitePolynomials(0, 1, normalised=True)
            elif basis == BasisType.TransformedNormalisedHermite:
                assert self.weights is not None
                assert len(self.weights[lia]) >= comp.shape[1]
                base = StochasticHermitePolynomials(0, 1, normalised=True)
            else:                                   # alternatively. use a self defined, callable basis
                assert len(basis) >= comp.shape[1]
                for lib in range(len(basis)):       # for every item in the current basis list assert callable
                    assert callable(basis[lib])
                values = np.array([basis[lib](x[lia]) for lib in range(comp.shape[1])])

            if values is None:
                values = np.zeros(comp.shape[1])
                for lib in range(comp.shape[1]):
                    if basis == BasisType.TransformedHermite or basis == BasisType.TransformedNormalisedHermite:
                        values[lib] = base.eval(lib, x[lia]*self.weights[lia][lib], all_degrees=False)

                    else:
                        values[lib] = base.eval(lib, x[lia], all_degrees=False)
            if lia == len(x)-1:                     # if it is the last component, just make a dot product
                retval = np.dot(comp[:, :, 0], values)
                continue

            _comp = comp.transpose(0, 2, 1)         # else, transpose [k_m, n, k_m+1] -> [k_m, k_m+1, n]
            #                                       # reshape -> [k_m*k_m+1, n]
            _comp = _comp.reshape((comp.shape[0]*comp.shape[2], comp.shape[1]), order="F")
            _comp = _comp.dot(values)               # dot -> [k_m*k_m+1]
            #                                       # reshape -> [k_m, k_m+1]
            _comp = _comp.reshape((comp.shape[0], comp.shape[2]), order="F")
            retval = _comp.dot(retval)              # dot -> [k_m]
        if retval.shape[0] == 1:
            return retval[0]
        return retval                               # return remaining vector
    # endregion

    # region def str
    def __str__(self):
        return "Extended Tensor Train: \n dimension: {} \n ranks: {} \n basis: {}".format(self.n, self.r,
                                                                                          self.basis)
    # endregion

    # region def repr
    def __repr__(self):
        return self.__str__()
    # endregion

    # region def add
    def __add__(self, other):
        if other is None:
            return self
        assert isinstance(other, ExtendedTT)
        assert len(other.components) == len(self.components)
        assert len(other.basis) == len(self.basis)
        for lia in range(len(other.basis)):
            assert self.basis[lia] == other.basis[lia]
            # assert self.components[lia].shape[1] == other.components[lia].shape[1]
            # TODO: Test if this is okay
        retval = self.copy()

        newcomponents = []
        for lia in range(retval.dim):
            if lia == 0:
                mu = max(retval.n[lia], other.n[lia])
                newcomp = np.zeros((1, mu, retval.r[1] + other.r[1]))
                newcomp[0, :retval.n[lia], :retval.r[1]] = retval.components[0]
                newcomp[0, :other.n[lia], retval.r[1]:] = other.components[0]
                newcomponents.append(newcomp)
                continue
            retval.r[lia] = retval.r[lia] + other.r[lia]
            if lia < retval.dim - 1:
                mu = max(retval.n[lia], other.n[lia])
                newcomp = np.zeros([retval.r[lia], mu, self.r[lia + 1] + other.r[lia + 1]])
                newcomp[:self.r[lia], :retval.n[lia], :self.r[lia + 1]] = retval.components[lia]
                newcomp[self.r[lia]:retval.r[lia], :other.n[lia],
                        self.r[lia + 1]:(self.r[lia + 1] + other.r[lia + 1])] = other.components[lia]
            else:
                mu = max(retval.n[lia], other.n[lia])
                newcomp = np.zeros([retval.r[self.dim - 1], mu, self.r[self.dim]])
                newcomp[:self.r[self.dim - 1], :retval.n[lia], :self.r[self.dim]] = retval.components[self.dim - 1]
                newcomp[self.r[self.dim - 1]:retval.r[self.dim - 1], :other.n[lia],
                        :self.r[self.dim]] = other.components[self.dim - 1]
            newcomponents.append(newcomp)

        return ExtendedTT(newcomponents, self.basis, self.weights)
    # endregion

    # region def sub
    def __sub__(self, other):
        c = self + (-other)
        return c
    # endregion

    # region def neg
    def __neg__(self):
        retval = self.copy()
        retval.components[0] *= -1
        return retval
    # endregion

    # region def matmul
    def __matmul__(self, other):
        raise NotImplementedError
    # endregion

    # region def rmul
    def __rmul__(self, other):
        return self.__mul__(other)
    # endregion

    # region def mul
    def __mul__(self, other):
        retval = self.copy()
        retval.components[0] *= other
        return retval
    # endregion

    # region def copy
    def copy(self):
        """ Creates a copy of the Extended TT Object"""
        from copy import deepcopy
        _components = deepcopy(self.components)
        _basis = deepcopy(self.basis)
        _weights = deepcopy(self.weights)
        retval = ExtendedTT(_components, _basis, _weights)
        return retval
    # endregion

    # region def add vector to component
    def add_vec_to_component(self, vec, index):
        assert 0 <= index < self.dim
        assert len(vec) == self.components[index].shape[1]

        measurement_ten = []
        for lia in range(self.dim):
            curr_core = np.zeros((1, self.n[lia], 1))
            if lia == index:
                assert self.basis[lia] == BasisType.points
                curr_core[0, :, 0] = vec
                measurement_ten.append(curr_core)
                continue
            if self.basis[lia] == BasisType.points:
                curr_core[0, :, 0] = np.ones(self.n[lia])
            else:
                curr_core[0, 0, 0] = 1.0
            measurement_ten.append(curr_core)
        measurement_ten = ExtendedTT(measurement_ten, self.basis)
        retval = self.__add__(measurement_ten)
        return retval
    # endregion

    # region multiply component by matrix (right)
    def multiply_comp_with_matrix_right(self, mat, index):
        assert 0 <= index < self.dim
        assert len(mat.shape) == 2
        assert mat.shape[0] == self.n[index]
        retval = self.copy()
        for (k1, k2) in _iter.product(range(retval.components[index].shape[0]),
                                      range(retval.components[index].shape[2])):
            # print("shape alpha: {}".format(retval.components[index][k1, :, k2].shape))
            # print("alpha: {}".format(retval.components[index][k1, :, k2]))
            # print("shape mat: {}".format(mat.shape))
            # print("mat: {}".format(mat))
            retval.components[index][k1, :, k2] = np.dot(retval.components[index][k1, :, k2].T, mat)
            # print("shape bar alpha: {}".format(retval.components[index][k1, :, k2].shape))
            # print("bar alpha: {}".format(retval.components[index][k1, :, k2]))
        retval.n[index] = mat.shape[1]
        return retval

    # endregion
    # region multiply component by matrix (left)
    def multiply_comp_with_matrix_left(self, mat, index):
        assert 0 <= index < self.dim
        assert len(mat.shape) == 2
        assert mat.shape[1] == self.n[index]
        retval = self.copy()
        for (k1, k2) in _iter.product(range(retval.components[index].shape[0]),
                                      range(retval.components[index].shape[2])):
            # print("shape alpha: {}".format(retval.components[index][k1, :, k2].shape))
            # print("alpha: {}".format(retval.components[index][k1, :, k2]))
            # print("shape mat: {}".format(mat.shape))
            # print("mat: {}".format(mat))
            retval.components[index][k1, :, k2] = mat.dot(retval.components[index][k1, :, k2])
            # print("shape bar alpha: {}".format(retval.components[index][k1, :, k2].shape))
            # print("bar alpha: {}".format(retval.components[index][k1, :, k2]))
        retval.n[index] = mat.shape[0]
        return retval

    # endregion

    # region def multiply with extended TT
    def multiply_with_extendedTT(self,
                                 other,             # type: ExtendedTT
                                 progress=False,    # type: bool
                                 base_change=None   # type: np.array
                                 ):
        assert isinstance(other, ExtendedTT)
        new_components = []
        if self.dim >= other.dim:
            right = self.components
            left = other.components
            new_basis = self.basis
            new_weights = self.weights
        else:
            right = other.components
            left = self.components
            new_basis = other.basis
            new_weights = other.weights
        if progress:
            bar = Bar("compute new components", max=len(left))
        for lia in range(len(left)):                # loop over first M components
            #                                       # both must have the same basis.
            #                                       # TODO: accept up to normalisation
            assert self.basis[lia] == other.basis[lia]
            # TODO: Weights are not clearly defined. Some are lists, some are values?
            # if self.weights is not None:
                # assert self.weights[lia] == other.weights[lia]

            l_core = left[lia]
            r_core = right[lia]
            if self.basis[lia] == BasisType.points:
                if lia > 0:
                    raise NotImplementedError
                assert l_core.shape[1] == r_core.shape[1]
                comp = np.zeros((1, l_core.shape[1], r_core.shape[2], l_core.shape[2]))
                for (k, l, m) in _iter.product(
                                               range(l_core.shape[1]),
                                               range(r_core.shape[2]),
                                               range(l_core.shape[2])
                                                     ):
                    comp[0, k, l, m] = l_core[0, k, l] * r_core[0, k, m]
                comp = comp.reshape(1, l_core.shape[1], l_core.shape[2]*r_core.shape[2], order="F")
                new_components.append(comp)
                continue
            if base_change is None:
                mid = get_base_change_tensor(l_core.shape[1], r_core.shape[1], self.basis[lia])
            else:
                mid = base_change[:l_core.shape[1], :, :r_core.shape[1]]
            assert mid.shape[0] == l_core.shape[1]
            assert mid.shape[2] == r_core.shape[1]
            # assert mid.shape[1] == l_core.shape[1] + r_core.shape[1]

            #                                       # [r_k-1, l, r_k] x [l', l'', l] -> [r_k-1, l', l'', r_k]
            comp = np.einsum('ijk, lmj->ilmk', r_core, mid)
            #                                       # [r_k-1', l', r_k'] x [r_k-1, l', l'', r_k] ->
            #                                       #                            [r_k-1, r_k-1', l'', r_k, r_k']
            comp = np.einsum('ijk, ajmc->aimck', l_core, comp)
            #                                       # [r_k-1, r_k-1', l'', r_k, r_k'] -> [s_k-1, l'', s_k]
            comp = comp.reshape(l_core.shape[0]*r_core.shape[0], comp.shape[2], l_core.shape[2]*r_core.shape[2],
                                order="F")
            if self.basis[lia] == BasisType.TransformedHermite or \
               self.basis[lia] == BasisType.TransformedNormalisedHermite:
                assert len(self.weights[lia]) >= comp.shape[1]
            new_components.append(comp)
            if progress:
                bar.next()
        if progress:
            bar.finish()
        for lia in range(len(left), len(right)):
            new_components.append(right[lia])
        return ExtendedTT(new_components, new_basis, new_weights)

    # endregion

    # region def multiply with extended TT and evaluate result at some point
    def multandeval(self,
                    other,  # type: ExtendedTT
                    nodes,  # type: list
                    progress=False  # type: bool
                    ):
        """
        DOES NOT TAKE CARE OF BASIS. Evaluation means pointwise.
        Can be used to compute integrals over the tensor with respect to the measure, orthonormal w.r.t the basis
        :param other: ExtendedTT
        :param nodes: list of nodes
        :param progress: flag to show progress bar
        :return:
        """
        assert isinstance(other, ExtendedTT)
        assert len(nodes) >= other.dim
        assert len(nodes) >= self.dim

        if self.dim >= other.dim:
            right = self.components  # chose right to be the long tensor
            left = other.components
            new_basis = self.basis
            new_weights = self.weights
        else:
            right = other.components
            left = self.components
            new_basis = other.basis
            new_weights = other.weights
        retval = [1]
        bar = None
        if progress:
            bar = Bar("contract cores", max=len(right) - 1)
        #                                           # loop over the cores which are to long
        for lia in range(len(right) - 1, len(left) - 1, -1):
            if lia == len(right) - 1:
                assert right[lia].shape[2] == 1
                retval = right[lia][:, nodes[lia], 0]
                continue
            retval = np.dot(right[lia][:, nodes[lia], :], retval)
            if progress:
                bar.next()
        # result up to here : vec[k_M]
        for lia in range(len(left) - 1, -1, -1):  # loop through the remaining cores
            #                                       # both must have the same basis.
            #                                       # TODO: accept up to normalisation
            assert self.basis[lia] == other.basis[lia]
            if self.weights is not None:
                assert self.weights[lia] == other.weights[lia]

            l_core = left[lia]
            r_core = right[lia]
            if self.basis[lia] == BasisType.points:
                raise NotImplementedError

            if lia == len(left) - 1:
                r_core = np.einsum('ijk, k->ij', r_core, retval)
                mid = get_base_change_tensor(l_core.shape[1], r_core.shape[1], self.basis[lia])
                assert mid.shape[0] == l_core.shape[1]
                assert mid.shape[2] == r_core.shape[1]
                assert l_core.shape[2] == 1
                mid = mid[:, :(nodes[lia] + 1), :]
                # assert mid.shape[1] == l_core.shape[1] + r_core.shape[1]

                #                                       # [r_k-1, l] x [l', l'', l] -> [r_k-1, l', l'']
                comp = np.einsum('ij, lmj->ilm', r_core, mid)
                #                                       # [r_k-1', l', r_k'] x [r_k-1, l', l''] ->
                #                                       #                            [r_k-1, r_k-1', l'']
                comp = np.einsum('ij, ajm->aim', l_core[:, :, 0], comp)
                retval = comp[:, :, nodes[lia]]  # [r_k-1, r_k-1']
                continue
            # r_core = np.einsum('ijk, kl->ijl', r_core, retval)
            r_, n, _r = r_core.shape[0], r_core.shape[1], retval.shape[1]
            r_core = r_core.reshape((r_ * n, r_core.shape[2]), order="F")
            r_core = np.dot(r_core, retval)
            r_core = r_core.reshape((r_, n, _r))
            mid = get_base_change_tensor(l_core.shape[1], r_core.shape[1], self.basis[lia])
            assert mid.shape[0] == l_core.shape[1]
            assert mid.shape[2] == r_core.shape[1]
            mid = mid[:, :(nodes[lia] + 1), :]
            #                                       # [r_k-1, l, r_k'] x [l', l'', l] -> [r_k-1, l', l'', r_k']
            comp = np.einsum('ijk, lmj->ilmk', r_core, mid)
            #                                       # [r_k-1', l', r_k'] x [r_k-1, l', l'', r_k'] ->
            #                                       #                                       [r_k-1, r_k-1', l'']
            comp = np.einsum('ijk, ajlk->ail', l_core, comp)
            retval = comp[:, :, nodes[lia]]
            if progress:
                bar.next()
        if progress:
            bar.finish()
        return np.ravel(retval, order="F")
    # endregion

    # region def from osel tt
    @staticmethod
    def from_osel_tt(osel_tt,                       # type: tt.vector
                     basis=None                     # type: None or list
                     ):
        """
        Creates an extended tensor train from an Oseledets TT Tensor.
        By default the basis is set to point-wise. otherwise pass a list of basis types or callable functions.
        :param osel_tt: tensor train in tt.vector format
        :param basis: list of basis types or callable functions
        :return: ExtendedTT
        """
        assert isinstance(osel_tt, tt.vector)
        components = tt.vector.to_list(osel_tt)
        if basis is not None:
            assert len(basis) == len(components)
            _basis = basis
        else:
            _basis = [BasisType.points]*len(components)
        return ExtendedTT(components, _basis)
    # endregion

    # region def from xerus tt
    @staticmethod
    def from_xerus_tt(xerus_tt,
                      basis=None,
                      weights=None
                      ):
        """
        Creates an extended tensor train from an Oseledets TT Tensor.
        By default the basis is set to point-wise. otherwise pass a list of basis types or callable functions.
        :param xerus_tt: tensor train in xerus TTTensor format
        :param basis: list of basis types or callable functions
        :param weights: list of weights
        :return: ExtendedTT
        """
        components = []
        for lia in range(xerus_tt.degree()):
            comp = xerus_tt.get_component(lia)
            # TODO: for loop over all entries, assert len == 3
            dim = comp.dimensions
            assert len(dim) == 3
            curr_component = np.zeros(dim)
            for (i, j, k) in _iter.product(range(dim[0]), range(dim[1]), range(dim[2])):
                curr_component[i, j, k] = comp[[i, j, k]]
            components.append(curr_component)
        if basis is not None:
            assert len(basis) == len(components)
            _basis = basis
        else:
            _basis = [BasisType.points]*len(components)

        return ExtendedTT(components, _basis, weights)

    # endregion

    # region def to osel tt
    def to_osel_tt(self):
        """
        WARNING: This does not make use of the basis representation. Only components are given as tt object
        :return: tt.vector
        """
        return tt.vector.from_list(self.components)
    # endregion

    # region def to xerus tt
    def to_xerus_tt(self):
        retval = xe.TTTensor(self.n)
        for lia in range(self.dim):
            # TODO: do we need pointwise tensors?
            # assert self.basis[lia] == BasisType.points
            curr_comp = self.components[lia]
            curr_xe_comp = xe.Tensor(dim=(self.r[lia], self.n[lia], self.r[lia+1]))
            # TODO: use from_array if it is fixed in your version
            for k1, n, k2 in _iter.product(range(self.r[lia]),
                                           range(self.n[lia]),
                                           range(self.r[lia+1])):
                curr_xe_comp[k1, n, k2] = curr_comp[k1, n, k2]
            retval.set_component(lia, curr_xe_comp)
        retval.canonicalize_left()
        return retval
    # endregion

    # region sum first component
    def sum_component(self, index):
        # TODO: make this possible for other components than the first
        assert index == 0                           # not tested for other components
        # TODO: make this possible for other basis types
        assert self.basis[index] == BasisType.points

        if self.dim == 1:  # only one component -> has no ranks
            return float(np.sum(self.components[0][0, :, 0]))

        new_components = []
        new_basis = []
        new_weights = None if self.weights is None else []
        next_comp = False
        new_comp = 1
        for lia in range(self.dim):
            if next_comp:
                assert lia > 0
                right = np.reshape(self.components[lia], (self.components[lia].shape[0], -1), order="F")
                new_comp = np.dot(new_comp, right).reshape((self.components[lia-1].shape[0],
                                                           self.components[lia].shape[1],
                                                           self.components[lia].shape[2]), order="F")
                new_components.append(new_comp)
                new_basis.append(self.basis[lia])
                if self.weights is not None:
                    new_weights.append(self.weights[lia])
                next_comp = False
                continue
            if lia == index:
                new_comp = np.sum(self.components[index], axis=1)
                next_comp = True
                if lia == self.dim - 1:
                    left = np.reshape(self.components[lia-1], (-1, self.components[lia-1].shape[2]), order="F")
                    new_comp = np.dot(left, new_comp)
                    new_comp = np.reshape(new_comp, (self.components[lia-1].shape[0], self.components[lia-1].shape[2],
                                                     1), order="F")
                    new_components[-1] = new_comp
            else:
                new_components.append(self.components[lia])
                new_basis.append(self.basis[lia])
                if self.weights is not None:
                    new_weights.append(self.weights[lia])
        return ExtendedTT(new_components, new_basis, new_weights)

    # endregion

    # region def evaluate at grid
    def evaluate_at_grid(self, nodes):

        assert self.dim == len(nodes)
        for lia in range(len(nodes)):
            assert len(nodes[lia]) > 0

        new_comp_list = []
        for lia in range(self.dim):
            new_comp = None
            basis = self.basis[lia]
            comp = self.components[lia]
            base = None
            if basis == BasisType.points:           # current basis type is point-wise evaluation
                assert len(nodes[lia]) <= comp.shape[1]
                new_comp = self.components[lia][:, nodes[lia], :]
            elif basis == BasisType.Legendre:       # current basis type is Legendre polynomials
                base = LegendrePolynomials(normalised=False)
            elif basis == BasisType.NormalisedLegendre:
                base = LegendrePolynomials(normalised=True)
                #                                   # current basis type is Hermite polynomials or transformed Hermite
            elif basis == BasisType.Hermite:
                base = StochasticHermitePolynomials(0, 1, normalised=False)
            elif basis == BasisType.TransformedHermite:
                assert self.weights is not None
                assert len(self.weights[lia]) >= comp.shape[1]
                base = StochasticHermitePolynomials(0, 1, normalised=False)
            elif basis == BasisType.NormalisedHermite:
                base = StochasticHermitePolynomials(0, 1, normalised=True)
            elif basis == BasisType.TransformedNormalisedHermite:
                assert self.weights is not None
                assert len(self.weights[lia]) >= comp.shape[1]
                base = StochasticHermitePolynomials(0, 1, normalised=True)
            else:                                   # alternatively. use a self defined, callable basis
                assert len(basis) >= comp.shape[1]
                for lib in range(len(basis)):       # for every item in the current basis list assert callable
                    assert callable(basis[lib])
                new_comp = np.zeros([comp[lia].shape[0], len(nodes[lia]), comp[lia].shape[2]])
                for lib in range(len(nodes[lia])):
                    new_comp[:, lib, :] = sum(comp[:, lic, :] * basis[lic](nodes[lia][lib])
                                              for lic in range(comp.shape[1]))

            if new_comp is None:
                new_comp = np.zeros([comp.shape[0], len(nodes[lia]), comp.shape[2]])
                for lib in range(len(nodes[lia])):
                    if basis == BasisType.TransformedHermite or basis == BasisType.TransformedNormalisedHermite:
                        for lic in range(comp.shape[1]):
                            new_comp[:, lib, :] += comp[:, lic, :] * \
                                                   base.eval(lic, nodes[lia][lib] * self.weights[lia][lic],
                                                             all_degrees=False)

                    else:
                        new_comp[:, lib, :] = sum(comp[:, lic, :] * base.eval(lic, nodes[lia][lib], all_degrees=False)
                                                  for lic in range(comp.shape[1]))
            new_comp_list.append(new_comp)

        new_basis = [BasisType.points] * self.dim
        return ExtendedTT(new_comp_list, new_basis)

    # endregion

    # region marginalise all but one dimension
    def marginalise_by_dimension(self, idx):
        shift_comp = [1]
        retval = self.copy()
        retval.normalise()
        #                                           # loop backward in hope of smaller ranks, hence better performance
        for lia in range(retval.dim-1, -1, -1):
            assert not retval.basis[lia] == BasisType.points
            if lia == idx:
                comp = retval.components[lia]
            else:
                comp = retval.components[lia][:, 0, :]
            shift_comp = np.dot(comp, shift_comp)

        shift_comp = np.reshape(shift_comp, (1, retval.components[idx].shape[1], 1), order="F")
        new_component_list = [shift_comp]
        new_basis_list = [retval.basis[idx]]
        if retval.weights is not None:
            new_weight_list = [retval.weights[idx]]
            return ExtendedTT(new_component_list, new_basis_list, weights=new_weight_list)
        return ExtendedTT(new_component_list, new_basis_list)
    # endregion

    # region marginalise all but two dimension
    def marginalise_all_but_two_dimension(self, idx1, idx2):
        retval = self.copy()
        retval.normalise()
        comp1 = [1]
        comp2 = [1]
        new_component_list = []
        new_basis_list = []
        if self.weights is None:
            new_weight_list = None
        else:
            new_weight_list = []
        first = min(idx1, idx2)
        last = max(idx1, idx2)

        assert retval.components[-1].shape[2] == 1
        #                                           # loop backward in hope of smaller ranks, hence better performance
        for lia in range(retval.dim - 1, last, -1):
            assert not retval.basis[lia] == BasisType.points
            comp1 = np.dot(retval.components[lia][:, 0, :], comp1)

        if last == retval.dim - 1:
            comp1 = retval.components[-1][:, :, 0]
        else:
            comp1 = np.einsum('ijk, k->ij', retval.components[last], comp1)
        for lia in range(last-1, first, -1):
            comp1 = np.einsum('ik, kl->il', retval.components[lia][:, 0, :], comp1)

        for lia in range(first):
            comp2 = np.dot(comp2, retval.components[lia][:, 0, :])

        if first == 0:
            comp2 = retval.components[0][0, :, :]
        else:
            comp2 = np.einsum('k, klm->lm', comp2, retval.components[first])

        new_component_list.append(comp2.reshape((1, comp2.shape[0], comp2.shape[1]), order="F"))
        new_component_list.append(comp1.reshape((comp1.shape[0], comp1.shape[1], 1), order="F"))

        new_basis_list.append(retval.basis[first])
        new_basis_list.append(retval.basis[last])
        if new_weight_list is not None:
            new_weight_list.append(retval.weights[first])
            new_weight_list.append(retval.weights[last])

        return ExtendedTT(new_component_list, new_basis_list, new_weight_list)
    # endregion

    # region marginalise one dimension only
    def marginalise_one_dimension(self, idx):
        retval = self.copy()
        retval.normalise()
        comp = None
        new_component_list = []
        new_basis_list = []
        if self.weights is None:
            new_weight_list = None
        else:
            new_weight_list = []
        #                                           # loop backward in hope of smaller ranks, hence better performance
        for lia in range(retval.dim-1, -1, -1):
            assert not retval.basis[lia] == BasisType.points
            if lia == idx:
                comp = retval.components[lia][:, 0, :]
                if lia == 0:
                    new_component_list[lia] = np.einsum('ai, ijk->ajk', comp, new_component_list[0])
            else:
                if comp is not None:
                    new_component_list.insert(0, np.einsum('ijk, kl->ijl', self.components[lia], comp))
                    comp = None
                else:
                    new_component_list.insert(0, self.components[lia])
                new_basis_list.insert(0, self.basis[lia])
                if self.weights is not None:
                    new_weight_list.insert(0, self.weights[lia])

        return ExtendedTT(new_component_list, new_basis_list, new_weight_list)
    # endregion

    # region compute mean
    def mean(self):
        shift_comp = [1]
        #                                           # loop backward in hope of smaller ranks, hence better performance
        for lia in range(self.dim - 1, -1, -1):
            assert self.basis[lia] == BasisType.NormalisedHermite or self.basis[lia] == BasisType.NormalisedLegendre \
                   or self.basis[lia] == BasisType.TransformedNormalisedHermite
            # TODO: this can be done for all other non normalised versions too. But beware the norm multiplication
            comp = self.components[lia][:, 0, :]
            shift_comp = np.dot(comp, shift_comp)
        retval = float(shift_comp)
        return retval
    # endregion

    # region normalise polynomials
    def normalise(self):
        leg_poly = LegendrePolynomials(normalised=False)
        for lia in range(self.dim):
            if self.basis[lia] == BasisType.NormalisedLegendre or self.basis[lia] == BasisType.NormalisedHermite \
                    or self.basis[lia] == BasisType.TransformedNormalisedHermite:
                continue
            assert self.basis[lia] == BasisType.Hermite or self.basis[lia] == BasisType.Legendre \
                or self.basis[lia] == BasisType.TransformedHermite
            if self.basis[lia] == BasisType.Hermite or self.basis[lia] == BasisType.TransformedHermite:
                for lib in range(self.components[lia].shape[1]):
                    self.components[lia][:, lib, :] *= np.sqrt(np.math.factorial(lib))
                if self.basis[lia] == BasisType.Hermite:
                    self.basis[lia] = BasisType.NormalisedHermite
                elif self.basis[lia] == BasisType.TransformedHermite:
                    self.basis[lia] = BasisType.TransformedNormalisedHermite
            elif self.basis[lia] == BasisType.Legendre:
                for lib in range(self.components[lia].shape[1]):
                    self.components[lia][:, lib, :] *= leg_poly.norm(lib)
                self.basis[lia] = BasisType.NormalisedLegendre
            else:
                raise ValueError("unknown polynomials to normalise: {}".format(self.basis[lia]))
    # endregion

    # region def from grid evaluation
    @staticmethod
    def from_grid_to_hermite(components, _nodes=None, _weights=None):
        polys = [StochasticHermitePolynomials(normalised=True)] * len(components)
        basis = [BasisType.NormalisedHermite] * len(components)
        if _nodes is None:
            _nodes = []
            for lia in range(len(components)):
                nodes, _ = hermegauss(components[lia].shape[1])
                _nodes.append(nodes)
        if _weights is None:
            _weights = []
            for lia in range(len(components)):
                _, weights = hermegauss(components[lia].shape[1])
                _weights.append(weights)
        return ExtendedTT.from_grid_to_extended(components, polys, _nodes, _weights, basis)


    @staticmethod
    def from_grid_to_legendre(components, _nodes=None, _weights=None):
        polys = [LegendrePolynomials(normalised=True)] * len(components)
        basis = [BasisType.NormalisedLegendre] * len(components)
        if _nodes is None:
            _nodes = []
            for lia in range(len(components)):
                nodes, _ = leggauss(components[lia].shape[1])
                _nodes.append(nodes)
        if _weights is None:
            _weights = []
            for lia in range(len(components)):
                _, weights = leggauss(components[lia].shape[1])
                _weights.append(weights)

        return ExtendedTT.from_grid_to_extended(components, polys, _nodes, _weights, basis)


    @staticmethod
    def from_grid_to_extended(components, polys, _nodes, _weights, basis):
        new_component_list = []
        new_basis_list = []
        for d, comp in enumerate(components):
            poly = polys[d]
            poly_eval = partial(poly.eval, all_degrees=False)
            new_comp = np.zeros(comp.shape)
            dim = comp.shape[1]
            nodes = _nodes[d]
            weights = _weights[d]
            weights *= weights.sum()**(-1)
            for lia in range(dim):
                # if len(nodes.shape) == 2:
                #     new_comp[:, lia, :] = sum(weights[lib] * comp[:, lib, :] * poly_eval(lia, nodes[d][lib])
                #                               for lib in range(dim))
                # else:
                new_comp[:, lia, :] = sum(weights[lib]*comp[:, lib, :]*poly_eval(lia, nodes[lib])
                                          for lib in range(dim))
            new_component_list.append(new_comp)

            new_basis_list.append(basis[d])
        return ExtendedTT(new_component_list, new_basis_list)

    # endregion

    # region compute order 2 best fit in monomial basis
    def compute_order_2_best_fit_monomial_hermite(self):
        for lia in range(self.dim):
            assert self.basis[lia] == BasisType.points
        n2 = StochasticHermitePolynomials(normalised=False).norm(2)
        Q = np.zeros((self.dim, self.dim))
        for i in range(self.dim):
            for j in range(self.dim):
                idx = np.zeros(self.dim, dtype=np.int)
                if i == j:
                    idx[i] = 2
                    Q[i, j] = self(idx) * n2 ** -1
                else:
                    idx[i] = 1
                    idx[j] = 1
                    Q[i, j] = 0.5 * self(idx)
        b = np.zeros(self.dim)
        for i in range(self.dim):
            idx = np.zeros(self.dim, dtype=np.int)
            idx[i] = 1
            b[i] = self(idx)
        c = self(np.zeros(self.dim, dtype=np.int))
        for i in range(self.dim):
            idx = np.zeros(self.dim, dtype=np.int)
            idx[i] = 2
            c += -1 * self(idx) * n2 ** -1
        return Q, b, c
    # endregion

    # region round (Warning: we only round the component tensors without taking care of the basis)
    def round(self, tol=1e-15, max_r=10000):
        self.components = tt.vector.to_list(tt.vector.round(tt.vector.from_list(self.components), eps=tol, rmax=max_r))
        self.r = []                                 # init rank list
        for lia in range(self.dim):                 # for every component in the tensor train of order 3
            self.r.append(self.components[lia].shape[0])
        self.r.append(self.components[-1].shape[2])
    # endregion

    # region round (Warning: we only round the component tensors without taking care of the basis)
    def xe_round(self, tol=1e-15, max_r=10000):
        retval = self.to_xerus_tt()
        retval.round(tol)
        return ExtendedTT.from_xerus_tt(retval, basis=self.basis, weights=self.weights)

    # endregion

    # region def canonicalize left
    def canonicalize_left(self):
        retval = self.to_xerus_tt()
        retval.canonicalize_left()
        return ExtendedTT.from_xerus_tt(retval, basis=self.basis, weights=self.basis)
    # endregion
    # region cut_dimension (Warning: we only cut the dimension here. No control applied)
    def cut(self, maxdim):
        assert maxdim >= 0
        for lia in range(self.dim):
            self.components[lia] = self.components[lia][:, :maxdim, :]
        return ExtendedTT(self.components, self.basis, self.weights)
    # endregion

    # region save
    def save(self, path):
        try:
            with h5py.File(path, "w") as outfile:
                if self.weights is not None:
                    outfile["weights"] = True
                else:
                    outfile["weights"] = False
                for lia in range(len(self.components)):
                    outfile["core{}".format(lia)] = self.components[lia]
                    outfile["basis{}".format(lia)] = self.basis[lia].value
                    if self.weights is not None:
                        outfile["weight{}".format(lia)] = self.weights[lia]
                outfile["len"] = len(self.components)
        except Exception as ex:
            print(ex)
            return False
        return True
    # endregion

    # region load
    @staticmethod
    def load(path):
        newcores = []
        newbasis = []
        newweights = []
        use_weight = False
        try:
            with h5py.File(path, "r") as outfile:
                print(outfile)
                if bool(outfile["weights"]) is True:
                    use_weight = True
                else:
                    newweights = None
                for lia in range(int(outfile["len"][()])):
                    newcores.append(np.array(outfile["core{}".format(lia)][()]))
                    newbasis.append(BasisType(int(outfile["basis{}".format(lia)][()])))
                    if use_weight is True and newweights is not None:
                        try:
                            newweights.append(np.array(outfile["weight{}".format(lia)][()]))
                        except KeyError as ex:
                            newweights = None
                            continue
        except IOError as ex:
            print("IOERROR can not import from file " + path + " err: " + ex.message)
            return None
        except EOFError as ex:
            print("EOFERROR can not import from file " + path + " err: " + ex.message)
            return None
        except KeyError as ex:
            print("KEYERROR can not import from file " + path + " err: " + ex.message)
            return None

        return ExtendedTT(newcores, basis=newbasis, weights=newweights)
    # endregion

    # region def partial derivative
    def partial_derivative(self, i, check_quotient=False):
        herm = StochasticHermitePolynomials(normalised=False)

        new_ten = self.copy()
        new_components = new_ten.components
        new_basis = new_ten.basis
        new_weights = new_ten.weights
        if new_basis[i] == BasisType.points:
            raise NotImplementedError("Can not derive for point basis")
        if new_basis[i] == BasisType.Legendre or new_basis[i] == BasisType.NormalisedLegendre:
            # TODO
            raise NotImplementedError("Can not derive legendre basis atm")

        assert new_basis[i] == BasisType.Hermite or new_basis[i] == BasisType.NormalisedHermite or \
            new_basis[i] == BasisType.TransformedNormalisedHermite or new_basis[i] == BasisType.TransformedHermite

        newcomp = np.zeros((new_components[i].shape[0], new_components[i].shape[1]-1, new_components[i].shape[2]))
        for lia in range(new_components[i].shape[1]-1):
            newcomp[:, lia, :] = new_components[i][:, lia+1, :]
            newcomp[:, lia, :] *= (lia+1)

            if new_weights is not None:
                newcomp[:, lia, :] *= new_weights[i][lia+1]

            if new_basis[i] == BasisType.NormalisedHermite or new_basis[i] == BasisType.TransformedNormalisedHermite:
                newcomp[:, lia, :] *= herm.norm(lia)
                newcomp[:, lia, :] *= herm.norm(lia+1)**(-1)

        new_components[i] = newcomp
        retval = ExtendedTT(new_components, new_basis, new_weights)
        if check_quotient:
            print("check difference quotient")
            x = np.random.randn(retval.dim)
            val = np.inf
            for lia in range(25):
                h = 2**-lia
                _x = x.copy()
                _x0 = x.copy()
                _x0[i] += h
                diff = (self(_x0) - self(_x))/h
                deri = retval(_x)
                val = np.abs(diff - deri)
                print("  difference to quotient with h={0:12f}: {1}".format(h, val))
            assert val < 1e-4
        return retval

    # endregion

    # region def Jacobian
    def get_jacobian(self):
        retval = []
        for lia in range(self.dim):
            retval.append(self.partial_derivative(lia))
        return retval

    # endregion

    # region der Hessian
    def get_hessian(self):
        retval = []
        jac = self.get_jacobian()
        for lia in range(self.dim):
            retval.append(jac[lia].get_jacobian())
        return retval

    # endregion
