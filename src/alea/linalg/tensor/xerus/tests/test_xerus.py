import unittest
import xerus as xe
import itertools as _iter
import numpy as np


class TestLognormalField(unittest.TestCase):
    def setUp(self):
        self.dimension = [10, 15, 20]
        self.ranks = [1, 8, 4, 1]

    def test_tensor_addition(self):
        ten_a = xe.Tensor.ones(self.dimension)
        ten_b = xe.Tensor.ones(self.dimension) * 4
        ten_c = ten_a + ten_b
        for (i, j, k) in _iter.product(range(self.dimension[0]), range(self.dimension[1]), range(self.dimension[2])):
            self.assertAlmostEqual(ten_c[i, j, k], 5, "values at ({}, {}, {}) do not coincide with 5".format(i, j, k))

    def test_tt_get_ndarray(self):
        ten = xe.TTTensor.random(self.dimension, self.ranks[1:-1])
        for lia in range(ten.degree()):
            comp_ten = ten.get_component(lia)
            comp_dim = comp_ten.dimensions
            comp_for = np.zeros(comp_dim)
            for (i, j, k) in _iter.product(range(comp_dim[0]), range(comp_dim[1]), range(comp_dim[2])):
                comp_for[i, j, k] = comp_ten[[i, j, k]]
            comp_nd = comp_ten.to_ndarray()         # TODO: raises segmentation fault
            self.assertLessEqual((np.linalg.norm(comp_for - comp_nd)/np.linalg.norm(comp_for)), 1e-10)

if __name__ == '__main__':
    unittest.main()

