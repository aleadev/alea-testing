from __future__ import division

import xerus as xe
import numpy as np


def function_to_approx(_x, _y):
    """
    some function to test the algorithm
    :param _x: one dimensional physical point
    :param _y: one dimensional stochastic point
    :return: value
    """
    assert len(_y) > 0
    return np.sin(2*np.pi*_x)*_y[0] + np.sin(2*np.pi*_x)*0.1*_y[0]

n_samples = 10000                                    # number of samples used for reconstruction
x_dim = 100                                         # nodes in physical space
target_eps = 1e-8                                   # desired tolerance on test data set inside ADF
max_itr = 10000                                      # maximal number of iterations in ADF

poly_dim = [5, 5]                                   # polynomial degree to fit one dimensional stochastic representation
basis = xe.PolynomBasis.Legendre                    # define flag of used polynomials

#                                                   # create samples
y_list = [np.random.rand(len(poly_dim)) for _ in range(n_samples)]
x = np.linspace(0, 1, num=x_dim, endpoint=False)     # create physical space
assert len(x) == x_dim


dimension = [x_dim] + poly_dim                      # define tensor dimensions

# region create measurement set
measurements = xe.UQMeasurementSet()                # create Xerus measurement Set
for y in y_list:                                    # for every sample
    sol = xe.Tensor(dim=[x_dim])                    # create a one dimensional tensor of size of physical space
    for lia in range(x_dim):                        # for every physical mesh node (since c++, that iss the way to go)
        #                                           # set value in tensor
        sol[lia] = function_to_approx(x[lia], y)
    measurements.add(y, sol)                  # add parameter and sample result to measurement list
# endregion

#                                                   # run adf algorithm
result = xe.uq_ra_adf(measurements, basis, dimension, target_eps, max_itr)

n_test_samples = 100                                # define new set of test samples
test_samples = [np.random.rand(len(poly_dim)) for __ in range(n_test_samples)]

reconstruction_results = []
true_values = []
for lia in range(len(test_samples)):                # for every test sample
    #                                               # evaluate tensor at given sample point
    eval_result = xe.uq_tt_evaluate(result, test_samples[lia], basis)
    loc_eval_result = np.zeros(x_dim)
    for lib in range(len(loc_eval_result)):         # for every physical mesh node
        loc_eval_result[lib] = eval_result[[lib]]   # obtain value in tensor
    reconstruction_results.append(loc_eval_result)  # save in list of arrays
    #                                               # save true values
    true_values.append(function_to_approx(x, test_samples[lia]))

reconstruction_results = np.array(reconstruction_results)
true_values = np.array(true_values)
#                                                   # compute l2 norm of results
res_norm = np.linalg.norm(reconstruction_results - true_values) / np.linalg.norm(true_values)

print("relative l2 error: {}".format(res_norm))
