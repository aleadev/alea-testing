===========
 Reference
===========


Foo
===

.. xxautoclass:: alea.polynomials.polynomials.NormalisedPolynomialFamily


Bar
===


.. currentmodule:: alea

.. fooautosummary::
   :xtoctree: generated/
   
   linalg.basis
   linalg.basis.Basis
   linalg.vector.EuclideanBasis
   linalg.operator
   polynomials.polynomials
   polynomials._polynomials


.. autosummary::
   :toctree: generated/

   polynomials.polynomials.NormalisedPolynomialFamily
   polynomials.polynomials.LegendrePolynomials
   polynomials.polynomials.StochasticHermitePolynomials


polynomials.polynomials.PolynomialFamily
polynomials.polynomials.OrthogonalPolynomialFamily

